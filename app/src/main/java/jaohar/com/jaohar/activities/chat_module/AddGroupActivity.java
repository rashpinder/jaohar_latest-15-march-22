package jaohar.com.jaohar.activities.chat_module;

import static android.view.View.GONE;
import static jaohar.com.jaohar.utils.RealPathUtil.getRealPathFromURI_API19;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.chat_module.SelectedUsersAdapter;
import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;
import jaohar.com.jaohar.interfaces.chat_module.RemoveParticipantInterface;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class AddGroupActivity extends BaseActivity {
    // Permissions Value
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    /**
     * Initialize the Activity
     */
    private final Activity mActivity = AddGroupActivity.this;
    // Permissions
    private final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private final String CAMERA = Manifest.permission.CAMERA;
    /*
     * Getting the Class Name
     * */
    String TAG = AddGroupActivity.this.getClass().getSimpleName();
    /**
     * Widgets
     */
    @BindView(R.id.txtCancelTV)
    TextView txtCancelTV;
    @BindView(R.id.txtNextTV)
    TextView txtNextTV;
    @BindView(R.id.NoOfParticipantsTV)
    TextView NoOfParticipantsTV;
    @BindView(R.id.selectedUsersRV)
    RecyclerView selectedUsersRV;
    @BindView(R.id.groupNameET)
    EditText groupNameET;
    @BindView(R.id.addImageRL)
    RelativeLayout addImageRL;
    @BindView(R.id.imgImageIV)
    ImageView imgImageIV;
    SelectedUsersAdapter mSelectedUsersAdapter;
    ArrayList<ChatUsersModel> mSelectedArrayList = new ArrayList<>();
    ArrayList<String> mSelectedist = new ArrayList<>();
    String currentPhotoPath = "";
    /*
     * Some Variable and data Types
     * */
    String mCurrentPhotoPath, mStoragePath = "", strBase64 = "";
    Bitmap thumb = null;
    RemoveParticipantInterface removeParticipantInterface = new RemoveParticipantInterface() {
        @Override
        public void mRemoveParticipantInterface(int position, ArrayList<ChatUsersModel> modelArrayList) {

        }
    };
    String cameraType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set status bar
        setStatusBar();
        setContentView(R.layout.activity_add_group);
        ButterKnife.bind(this);
        if (JaoharSingleton.getInstance().getmSelectedGroupUsersList() != null) {
            mSelectedArrayList = JaoharSingleton.getInstance().getmSelectedGroupUsersList();
            setSelectedUsersAdapter(mSelectedArrayList, "2");

            for (int i = 0; i < mSelectedArrayList.size(); i++) {
                mSelectedist.add(mSelectedArrayList.get(i).getId());
            }
        }
        NoOfParticipantsTV.setText(String.valueOf(mSelectedArrayList.size()));
    }

    @OnClick({R.id.txtCancelTV, R.id.txtNextTV, R.id.imgImageIV, R.id.addImageRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtCancelTV:
                onBackPressed();
                break;
            case R.id.txtNextTV:
                if (isValidate()) {
                    executeAddChatGroupAPI();
                }
                break;
            case R.id.imgImageIV:
                mToGrantPermission();
                break;
            case R.id.addImageRL:
                mToGrantPermission();
                break;
        }
    }

    private Boolean isValidate() {
        boolean flag = true;

        if (groupNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_group_name));
            flag = false;
        }
        return flag;
    }

    /**
     * /
     * API executation
     */
    Map<String, String> mParams1() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        params.put("group_name", groupNameET.getText().toString().trim());
        params.put("user_list", String.valueOf(mSelectedist));
        params.put("photo", strBase64);
        Log.e(TAG, "**PARAMS**" + params);
        return params;
    }

    public void executeAddChatGroupAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.addChatGroup1(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), groupNameET.getText().toString().trim(),
                String.valueOf(mSelectedist), strBase64);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    AlertDialogManager.hideProgressDialog();
                    String status = String.valueOf(jsonObject.getInt("status"));
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
                        showAddGroupAlertDialog(mActivity, getString(R.string.app_name), "" + getString(R.string.group_created_successfully));
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void setSelectedUsersAdapter(ArrayList<ChatUsersModel> selectedArrayList, String type) {
        selectedUsersRV.setNestedScrollingEnabled(false);
        selectedUsersRV.setLayoutManager(new LinearLayoutManager(mActivity, RecyclerView.HORIZONTAL, false));
        mSelectedUsersAdapter = new SelectedUsersAdapter(mActivity, selectedArrayList, type, removeParticipantInterface);
        selectedUsersRV.setAdapter(mSelectedUsersAdapter);
    }

    public void mToGrantPermission() {
        if (mCheckPermission()) {
            openCameraGalleryDialog();
        } else {
            mRequestPermission();
        }
    }

    private boolean mCheckPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int read_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int write_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return camera == PackageManager.PERMISSION_GRANTED && read_external_storage == PackageManager.PERMISSION_GRANTED && write_external_storage == PackageManager.PERMISSION_GRANTED;
    }

    private void mRequestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, 100);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            //All Permissions Granted
//                            openCameraGalleryDialog();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            Toast.makeText(mActivity, getString(R.string.goto_settings), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);
        TextView tittleTV = (TextView) dialog.findViewById(R.id.tittleTV);
        TextView text_camra = (TextView) dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = (TextView) dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_files.setVisibility(View.GONE);
        tittleTV.setVisibility(View.GONE);
        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });

        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });

        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openFiles();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        Log.e("error is occured", dialog.toString());
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 505);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void openInternalStorage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf|text/plain");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 606);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void openCamera() {
        cameraType = "camera";
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                String authorities = getApplicationContext().getPackageName() + "com.dharmaniapps.fileprovider";
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            }
        }
        startActivityForResult(takePictureIntent, CAMERA_REQUEST);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void openGallery() {
        cameraType = "gallery";
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == GALLERY_REQUEST && data != null) {
            Uri uri = data.getData();
            File file = null; // 2
            try {
                file = createImageFile();
            } catch (IOException e) {
                Toast.makeText(mActivity, "EXCEPTION", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            Uri destinationUri = Uri.fromFile(file);
            // Uri destinationUri = Uri.parse(mCurrentPhotoPath);
            openCropActivityForProfilePic(uri, destinationUri);
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK) {
            Uri uri = UCrop.getOutput(data);
            if (cameraType.equals("gallery")) {
                try {
                    try {
                        Bitmap bitmap;
                        //    File finalFile = new File(getRealPathFromURI(uri));
                        File finalFile = new File(getRealPathFromURI_API19(mActivity, uri));
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 0;

                        bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                          For COnvert And rotate image
                        ExifInterface exifInterface = null;
                        try {
                            exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                        Matrix matrix = new Matrix();
                        switch (orientation) {
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                matrix.setRotate(90);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                matrix.setRotate(180);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                matrix.setRotate(270);
                                break;
                            case ExifInterface.ORIENTATION_NORMAL:
                            default:
                        }
                        JaoharConstants.IS_camera_Click = false;
                        thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

                        addImageRL.setVisibility(GONE);
                        imgImageIV.setVisibility(View.VISIBLE);

//                            imgImageIV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        imgImageIV.setImageBitmap(thumb);
                        strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } finally {
                }
            }
            if (cameraType.equals("camera")) {
                try {
                    JaoharConstants.IS_camera_Click = true;
                    thumb = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 0;

                    addImageRL.setVisibility(GONE);
                    imgImageIV.setVisibility(View.VISIBLE);

//                    imgImageIV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                    imgImageIV.setImageBitmap(thumb);
                    strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {


            Uri uri = Uri.parse(mCurrentPhotoPath);
//            Uri sourceUri = data.getData(); // 1
            openCropActivityForProfilePic(uri, uri);

        }
    }

    private void openCropActivityForProfilePic(Uri sourceUri, Uri destinationUri) {

        UCrop.Options options = new UCrop.Options();
        UCrop.of(sourceUri, destinationUri)
                .useSourceImageAspectRatio()
                .withMaxResultSize(10000, 10000)
                .withOptions(options)
                .start(mActivity);
    }

    /*
     * Show Alert Dailog Box
     * */
    public void showAddGroupAlertDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                Intent mIntent = new Intent(mActivity, ChatUsersListActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mIntent);

            }
        });
        alertDialog.show();
    }
}