package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.Utilities;

public class Subtract_ValueDiscountInvoiceActivity extends BaseActivity {
    Activity mActivity = Subtract_ValueDiscountInvoiceActivity.this;
    String TAG = Subtract_ValueDiscountInvoiceActivity.this.getClass().getSimpleName(), strItemID,strItem,strQuantity,strPrice;
    LinearLayout llLeftLL;
    TextView txtSubTotalTV,txtQuantityTV,txtVatPriceTV,txtTotalTV;
    EditText AddValueTV,txtDescriptionTV;
    Button btnSubmitB;
    InvoiceAddItemModel mModel, pModel;
    int mPosition;
    double totalPrice;
    ArrayList<DiscountModel> mDiscountModelArrayList = new ArrayList<DiscountModel>();
    private  long parcentValue;
    DiscountModel mDiscountModel =new DiscountModel();
    private long sumData;
    double totalPercent ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subtract__value_discount_invoice);
        if (getIntent() != null) {
            mModel = (InvoiceAddItemModel) getIntent().getSerializableExtra("Model");
            mDiscountModelArrayList =mModel.getmDiscountModelArrayList();
            mPosition = getIntent().getIntExtra("Position", 0);
            strItemID = mModel.getItemID();
        }
    }
    @Override
    protected void setViewsIDs() {
        llLeftLL=(LinearLayout)findViewById(R.id.llLeftLL);
        txtSubTotalTV=(TextView)findViewById(R.id.txtSubTotalTV);
        txtQuantityTV=(TextView)findViewById(R.id.txtQuantityTV);
        txtDescriptionTV=(EditText)findViewById(R.id.txtDescriptionTV);
        txtVatPriceTV=(TextView)findViewById(R.id.txtVatPriceTV);
        txtTotalTV=(TextView)findViewById(R.id.txtTotalTV);
        AddValueTV=(EditText) findViewById(R.id.AddValueTV);
        btnSubmitB=(Button) findViewById(R.id.btnSubmitB);

        totalPrice = Double.parseDouble(mModel.getStrTotalAmountUnit());;
        String strTotal= String.valueOf(totalPrice);
        strItem = mModel.getItem();

        strQuantity = String.valueOf(mModel.getQuantity());
        strPrice = String.valueOf(mModel.getUnitprice());







        txtSubTotalTV.setText(strItem);
        Double  strTotal12 = Utilities.getRoundOff2Decimal(Double.parseDouble(strTotal));
        txtVatPriceTV.setText(""+Utilities.convertEvalueToNormal(Double.parseDouble(mModel.getStrTotalAmountUnit())));
//        txtVatPriceTV.setText(strPrice);
//        txtDescriptionTV.setText(strDescription);
        txtQuantityTV.setText(strQuantity);

    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        btnSubmitB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtDescriptionTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_description));
                } else if (AddValueTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_value));
                }else {
                    mDiscountModel.setSubtract_description(txtDescriptionTV.getText().toString());
                    mDiscountModel.setSubtract_items(strItem);
                    mDiscountModel.setSubtract_quantity(strQuantity);
                    mDiscountModel.setSubtract_unitPrice(String.valueOf(totalPrice));
                    mDiscountModel.setSubtract_Value(AddValueTV.getText().toString());
                    mDiscountModel.setType("SUBTRACT_VALUE");
                    mDiscountModel.setAddAndSubtractTotal(txtTotalTV.getText().toString());
                    mDiscountModel.setStrtotalPercentValue(String.valueOf(totalPercent));
                    mDiscountModelArrayList.add(mDiscountModel);
//                mModel.setmDiscountModelArrayList(mDiscountModelArrayList);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("pModel", mDiscountModelArrayList);
                    returnIntent.putExtra("Position", mPosition);
                    setResult(810, returnIntent);
                    onBackPressed();
                    finish();
                }
            }
        });


        AddValueTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double basePrice=0;
                double sum;
                String str = AddValueTV.getText().toString();
                try
                {
                    basePrice = Double.parseDouble(mModel.getStrTotalAmountUnit());//   Integer.parseInt()
                    totalPercent = Double.parseDouble(str);

//                    totalPercent=  basePrice * percentValue;
//                    totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                    sum =basePrice-totalPercent;

                    txtTotalTV.setText(""+Utilities.convertEvalueToNormal(sum));
                }
                catch (Exception ex)
                {

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

