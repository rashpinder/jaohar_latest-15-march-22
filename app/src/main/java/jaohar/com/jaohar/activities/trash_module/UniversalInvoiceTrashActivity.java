package jaohar.com.jaohar.activities.trash_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.trash_module_adapter.InvoiceTrashAdapter;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.beans.PaymentModel;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.INVtrashRecoverDeleteInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteInvoiceTrashActivity;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class UniversalInvoiceTrashActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private final Activity mActivity = UniversalInvoiceTrashActivity.this;
    /*
     * Getting the Class Name
     * */

    String TAG = UniversalInvoiceTrashActivity.this.getClass().getSimpleName();
    /**
     * Widgets
     */

    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;
    private int page_no = 1;
    SwipeRefreshLayout swipeToRefresh;
    RecyclerView dataRV;
    ProgressBar progressBottomPB;
    ImageView recoverIV, deleteIV, emtyTrashIV;
    InVoicesModel mModel;


    /* *
     * Initialize the String and ArrayList
     * */
    private String strLastPage = "FALSE", strInvoiceID = "";
    private boolean isSwipeRefresh = false;
    String strIsClickFrom = "";
    ArrayList<InVoicesModel> modelArrayList = new ArrayList<InVoicesModel>();
    ArrayList<InVoicesModel> mLoadMore = new ArrayList<InVoicesModel>();
    ArrayList<InvoiceAddItemModel> mInvoiceItemArrayList = new ArrayList<InvoiceAddItemModel>();
    ArrayList<String> mRecordID = new ArrayList<>();
    InvoiceTrashAdapter mAdapter;
    private final ArrayList<InVoicesModel> multiSelectArrayList = new ArrayList<InVoicesModel>();


    /**
     * To get Selected and unSelected Vessel Interface
     */
    RecoverDeleteInvoiceTrashActivity mRecoverDeleteSelected = new RecoverDeleteInvoiceTrashActivity() {
        @Override
        public void mRecoverDeleteInvoice(InVoicesModel mModel, boolean IsSelected) {
            if (mModel != null) {
                if (!IsSelected) {
                    multiSelectArrayList.add(mModel);
                    String strID = mModel.getInvoice_id();
                    mRecordID.add(mModel.getInvoice_id());
                } else {
                    multiSelectArrayList.remove(mModel);
                    String strID = mModel.getInvoice_id();
                    mRecordID.remove(mModel.getInvoice_id());
                }
            }
        }
    };


    /*
     * Implement Pagination On Recycler View
     * */
    paginationforVesselsInterface mPagination = new paginationforVesselsInterface() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                progressBottomPB.setVisibility(View.VISIBLE);
                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            executeAPI(page_no);
                        } else {
                            progressBottomPB.setVisibility(View.GONE);
                        }
                    }
                }, 1000);

            }
        }
    };


    /*
     * To Recover and Delete Interface
     * */
    INVtrashRecoverDeleteInterface mDeleteInterface = new INVtrashRecoverDeleteInterface() {
        @Override
        public void mInvTrashRecover(InVoicesModel mModel, String strType) {
            strInvoiceID = mModel.getInvoice_id();
            if (strType.equals("recover")) {
                deleteConfirmDialog(getString(R.string.recover_trash_invoice), "recover");
            } else {
                deleteConfirmDialog(getString(R.string.are_you_sure_want_to_delete_invoice_), "delete");
            }
        }
    };


    /**
     * Activity @Override method
     * #onCreate
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_universal_invoice_trash);
    }

    /**
     * Set Widget IDS
     */

    @Override
    protected void setViewsIDs() {
        imgBack = findViewById(R.id.imgBack);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        dataRV = findViewById(R.id.dataRV);
        recoverIV = findViewById(R.id.recoverIV);
        deleteIV = findViewById(R.id.deleteIV);
        emtyTrashIV = findViewById(R.id.emtyTrashIV);
        progressBottomPB = findViewById(R.id.progressBottomPB);
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        txtCenter.setText(getResources().getString(R.string.invoice_trash_screen_));
        swipeToRefresh = findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                mLoadMore.clear();
                modelArrayList.clear();
                mInvoiceItemArrayList.clear();
                page_no = 1;
                executeAPI(page_no);
            }
        });
        //Get Clicked Value
        if (getIntent() != null) {
            if (getIntent().getStringExtra("isClick").equals("manageUtilities")) {
                strIsClickFrom = getIntent().getStringExtra("isClick");
            }
        }
    }


    /**
     * Set Widget TextChangedListener & ClickListener
     */

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        emtyTrashIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (modelArrayList.size() > 0) {
                    deleteConfirmDialog(getString(R.string.clear_trash), "empty");
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_vessel_found_in_trash));

                }
            }
        });

        recoverIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (modelArrayList.size() > 0) {

                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {

                        if (multiSelectArrayList.size() > 0) {
                            executeRecoverSelectedTrash();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.there_is_no_selected_item));

                        }
                    }
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_vessel_found_in_trash));

                }
            }
        });

        deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (modelArrayList.size() > 0) {

                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {

                        if (multiSelectArrayList.size() > 0) {
                            executeDeleteSelectedTrash();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.there_is_no_selected_item));

                        }
                    }
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_vessel_found_in_trash));

                }
            }
        });

    }


    /*
     * Arrange Selected Array list and Get ID from that
     * */
    private ArrayList<String> getMultiVesselDetailsData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();
        Log.e(TAG, "ArrayLIST_DATA1============= " + mRecordID.size());
        for (int i = 0; i < mRecordID.size(); i++) {
            strData.add(mRecordID.get(i));
        }

        return strData;
    }

    /**
     * Activity @Override method
     * #onBackPressed
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent = new Intent(mActivity, TrashActivity.class);
        mIntent.putExtra("isClick", strIsClickFrom);
        startActivity(mIntent);
        finish();
    }

    /**
     * Activity @Override method
     * #onResume
     */

    @Override
    public void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            if (strLastPage.equals("FALSE")) {
                page_no = 1;
                mLoadMore.clear();
                modelArrayList.clear();
                mInvoiceItemArrayList.clear();
                executeAPI(page_no);
            }
        }
    }


    /**
     * Call GetAllUniversalInvoiceTrash API for getting
     * list of all universal invoices
     *
     * @param
     * @page_no
     * @user_id
     **/
//    public void executeAPI(int page_no) {
//        String strUrl = JaoharConstants.GetAllUniversalInvoiceTrash + "?page_no=" + page_no + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
//        if (page_no > 1) {
//            swipeToRefresh.setRefreshing(false);
//            progressBottomPB.setVisibility(View.VISIBLE);
//        }
//        if (page_no == 1) {
//            swipeToRefresh.setRefreshing(false);
//            progressBottomPB.setVisibility(View.GONE);
//            AlertDialogManager.showProgressDialog(mActivity);
//        }
//        if (!isSwipeRefresh) {
//            progressBottomPB.setVisibility(View.GONE);
//        }
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******ResponseTrash*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        parseResponse(response);
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                progressBottomPB.setVisibility(View.GONE);
//                if (isSwipeRefresh) {
//                    swipeToRefresh.setRefreshing(false);
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
    public void executeAPI(int page_no) {
//        String strUrl = JaoharConstants.GetAllUniversalInvoiceTrash + "?page_no=" + page_no + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
        if (page_no > 1) {
            swipeToRefresh.setRefreshing(false);
            progressBottomPB.setVisibility(View.VISIBLE);
        }
        if (page_no == 1) {
            swipeToRefresh.setRefreshing(false);
            progressBottomPB.setVisibility(View.GONE);
            AlertDialogManager.showProgressDialog(mActivity);
        }
        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllUniversalInvoiceTrash(String.valueOf(page_no), JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "******ResponseTrash*****" + response);
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    String strMessage = mJsonObject.getString("message");
                    if (strStatus.equals("1")) {
                        parseResponse(response.body().toString());
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressBottomPB.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse(String response) {
        mLoadMore.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            JSONObject mDataObject = mJsonObject.getJSONObject("data");
            JSONArray mJsonArray = mDataObject.getJSONArray("all_invoices");
            strLastPage = mDataObject.getString("last_page");
            for (int i = 0; i < mJsonArray.length(); i++) {
                JSONObject mJson = mJsonArray.getJSONObject(i);
                mModel = new InVoicesModel();
                JSONObject mAllDataObj = mJson.getJSONObject("all_data");
                if (!mAllDataObj.isNull("invoice_id"))
                    mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                if (!mAllDataObj.getString("pdf").equals("")) {
                    mModel.setPdf(mAllDataObj.getString("pdf"));
                }
                if (!mAllDataObj.getString("pdf_name").equals("")) {
                    mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
                }
                if (!mAllDataObj.isNull("invoice_no"))
                    mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
                if (!mAllDataObj.isNull("invoice_date"))
                    mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
                if (!mAllDataObj.isNull("term_days"))
                    mModel.setTerm_days(mAllDataObj.getString("term_days"));
                if (!mAllDataObj.isNull("currency"))
                    mModel.setCurrency(mAllDataObj.getString("currency"));
                if (!mAllDataObj.isNull("status"))
                    mModel.setStatus(mAllDataObj.getString("status"));
                if (!mAllDataObj.isNull("reference"))
                    mModel.setRefrence1(mAllDataObj.getString("reference"));
                if (!mAllDataObj.isNull("reference1"))
                    mModel.setRefrence2(mAllDataObj.getString("reference1"));
                if (!mAllDataObj.isNull("deleted_by"))
                    mModel.setDeleted_by(mAllDataObj.getString("deleted_by"));
                if (!mAllDataObj.isNull("reference2"))
                    mModel.setRefrence3(mAllDataObj.getString("reference2"));
                if (!mAllDataObj.isNull("payment_id"))
                    mModel.setPayment_id(mAllDataObj.getString("payment_id"));
                if (!mJson.getString("sign_data").equals("")) {
                    JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                    SignatureModel mSignModel = new SignatureModel();
                    if (!mSignDataObj.isNull("sign_id"))
                        mSignModel.setId(mSignDataObj.getString("sign_id"));
                    if (!mSignDataObj.isNull("sign_name"))
                        mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                    if (!mSignDataObj.isNull("sign_image"))
                        mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                    mModel.setmSignatureModel(mSignModel);
                }

                if (!mJson.getString("stamp_data").equals("")) {
                    JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                    StampsModel mStampsModel = new StampsModel();
                    if (!mStampDataObj.isNull("stamp_id"))
                        mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                    if (!mStampDataObj.isNull("stamp_name"))
                        mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                    if (!mStampDataObj.isNull("stamp_image"))
                        mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                    mModel.setmStampsModel(mStampsModel);
                }
                if (!mJson.getString("bank_data").equals("")) {
                    JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                    BankModel mBankModel = new BankModel();
                    if (!mBankDataObj.isNull("bank_id"))
                        mBankModel.setId(mBankDataObj.getString("bank_id"));
                    if (!mBankDataObj.isNull("beneficiary"))
                        mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                    if (!mBankDataObj.isNull("bank_name"))
                        mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                    if (!mBankDataObj.isNull("address1"))
                        mBankModel.setAddress1(mBankDataObj.getString("address1"));
                    if (!mBankDataObj.isNull("address2"))
                        mBankModel.setAddress2(mBankDataObj.getString("address2"));
                    if (!mBankDataObj.isNull("iban_ron"))
                        mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                    if (!mBankDataObj.isNull("iban_usd"))
                        mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                    if (!mBankDataObj.isNull("iban_eur"))
                        mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                    if (!mBankDataObj.isNull("swift"))
                        mBankModel.setSwift(mBankDataObj.getString("swift"));
                    mModel.setmBankModel(mBankModel);
                }
                if (!mJson.getString("search_vessel_data").equals("")) {
                    JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                    VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                    if (!mSearchVesselObj.isNull("vessel_id"))
                        mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                    if (!mSearchVesselObj.isNull("vessel_name"))
                        mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                    if (!mSearchVesselObj.isNull("IMO_no"))
                        mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                    if (!mSearchVesselObj.isNull("flag"))
                        mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                    mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                }
                if (!mJson.getString("search_company_data").equals("")) {
                    JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                    CompaniesModel mCompaniesModel = new CompaniesModel();
                    if (!mSearchCompanyObj.isNull("id"))
                        mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                    if (!mSearchCompanyObj.isNull("company_name"))
                        mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                    if (!mSearchCompanyObj.isNull("Address1"))
                        mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                    if (!mSearchCompanyObj.isNull("Address2"))
                        mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                    if (!mSearchCompanyObj.isNull("Address3"))
                        mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                    if (!mSearchCompanyObj.isNull("Address4"))
                        mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                    if (!mSearchCompanyObj.isNull("Address5"))
                        mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                    mModel.setmCompaniesModel(mCompaniesModel);
                }
                if (!mJson.getString("payment_data").equals("")) {
                    JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                    PaymentModel mPaymentModel = new PaymentModel();
                    if (!mPaymentObject.isNull("payment_id")) {
                        mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                    }
                    if (!mPaymentObject.isNull("sub_total")) {
                        mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                    }
                    if (!mPaymentObject.isNull("VAT")) {
                        mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                    }
                    if (!mPaymentObject.isNull("vat_price")) {
                        mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                    }
                    if (!mPaymentObject.isNull("total")) {
                        mPaymentModel.setTotal(mPaymentObject.getString("total"));
                    }
                    if (!mPaymentObject.isNull("paid")) {
                        mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                    }
                    if (!mPaymentObject.isNull("due")) {
                        mPaymentModel.setPaid(mPaymentObject.getString("due"));
                    }
                    mModel.setmPaymentModel(mPaymentModel);
                }
                if (!mJson.getString("items_data").equals("")) {
                    JSONArray mItemArray = mJson.getJSONArray("items_data");
                    for (int k = 0; k < mItemArray.length(); k++) {
                        JSONObject mItemObj = mItemArray.getJSONObject(k);
                        InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                        if (!mItemObj.isNull("item_id"))
                            mItemModel.setItemID(mItemObj.getString("item_id"));
                        if (!mItemObj.isNull("item_serial_no"))
                            mItemModel.setItem(mItemObj.getString("item_serial_no"));
                        if (!mItemObj.isNull("quantity"))
                            mItemModel.setQuantity(mItemObj.getInt("quantity"));
                        if (!mItemObj.isNull("price"))
                            mItemModel.setUnitprice(mItemObj.getString("price"));
                        if (!mItemObj.isNull("description"))
                            mItemModel.setDescription(mItemObj.getString("description"));
                        mInvoiceItemArrayList.add(mItemModel);
                    }
                    mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                }

                if (page_no == 1) {
                    modelArrayList.add(mModel);
                } else if (page_no > 1) {
                    mLoadMore.add(mModel);
                }
            }
            if (mLoadMore.size() > 0) {
                modelArrayList.addAll(mLoadMore);
            }
            if (page_no == 1) {
                setAdapter();
            } else {
                mAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setAdapter() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        dataRV.setLayoutManager(layoutManager);
        mAdapter = new InvoiceTrashAdapter(mActivity, modelArrayList, mPagination, mDeleteInterface, mRecoverDeleteSelected);
        dataRV.setAdapter(mAdapter);
    }


    /*
     * Setting Up custom Dialog box to show pop Up
     * */
    public void deleteConfirmDialog(String strMessage, final String strType) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                if (strType.equals("empty")) {
                    /*Execute EMPTY API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeEmptyTrash();
                    }
                } else if (strType.equals("delete")) {
                    /*Execute Delete API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeDeleteTrash();
                    }
                } else {
                    /*Execute Recover API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeRecoverTrash();
                    }
                }
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }


    /**
     * Call RecoverSingleUniversalInvoiceTrash API for recover Single Invoice
     * user access token
     *
     * @param
     * @invoice_id
     * @user_id
     */
    private void executeRecoverTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.recoverSingleUniversalInvoiceTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""), strInvoiceID).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            mLoadMore.clear();
                            modelArrayList.clear();
                            mInvoiceItemArrayList.clear();
                            executeAPI(1);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }


//    public void executeRecoverTrash() {
//        String strUrl = JaoharConstants.RecoverSingleUniversalInvoiceTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&invoice_id=" + strInvoiceID;// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mLoadMore.clear();
//                        modelArrayList.clear();
//                        mInvoiceItemArrayList.clear();
//                        executeAPI(1);
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    /**
     * Call DeleteSingleUniversalInvoiceTrash API for delete Single Invoice
     * user access token
     *
     * @param
     * @invoice_id
     * @user_id
     */
    private void executeDeleteTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteSingleUniversalInvoiceTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") , strInvoiceID).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            mLoadMore.clear();
                            modelArrayList.clear();
                            mInvoiceItemArrayList.clear();
                            executeAPI(1);
                            mAdapter.notifyDataSetChanged();
                        } else {
                          AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                 }}

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }

//    public void executeDeleteTrash() {
//        String strUrl = JaoharConstants.DeleteSingleUniversalInvoiceTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&invoice_id=" + strInvoiceID;// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mLoadMore.clear();
//                        modelArrayList.clear();
//                        mInvoiceItemArrayList.clear();
//                        executeAPI(1);
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Call EmptyUnivesalInvoiceTrash API for Empty whole Invoice
     * user access token
     *
     * @param
     * @user_id
     */
    private void executeEmptyTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.emptyUniversalInvoiceTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "")).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mLoadMore.clear();
                    modelArrayList.clear();
                    mInvoiceItemArrayList.clear();
                    executeAPI(1);
                    mAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


//    public void executeEmptyTrash() {
//        String strUrl = JaoharConstants.EmptyUnivesalInvoiceTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
////                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                        mLoadMore.clear();
//                        modelArrayList.clear();
//                        mInvoiceItemArrayList.clear();
//                        executeAPI(1);
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Call RecoverSelectedUniversalInvoiceTrash API for recover selected  Invoice
     * user access token
     *
     * @param
     * @user_id
     * @invoice_ids
     */
    private Map<String, String> mRecoverParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        params.put("invoice_ids", String.valueOf(getMultiVesselDetailsData()));
        Log.e("**PARAMS**",params.toString());
        return params;
    }

    public void executeRecoverSelectedTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.recoverSelectedUniversalInvoiceTrash(mRecoverParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    mLoadMore.clear();
                    mRecordID.clear();
                    modelArrayList.clear();
                    mInvoiceItemArrayList.clear();
                    executeAPI(1);
                    mAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" +t.getMessage());

            }
        });
    }

//    public void executeRecoverSelectedTrash() {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.RecoverSelectedUniversalInvoiceTrash;
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("invoice_ids", getMultiVesselDetailsData());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.e("test", "******response*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String strStatus = jsonObject.getString("status");
//                    String strMessage = jsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mLoadMore.clear();
//                        mRecordID.clear();
//                        modelArrayList.clear();
//                        mInvoiceItemArrayList.clear();
//                        executeAPI(1);
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("test", "******response*****" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Call DeleteSelectedUniversalInvoiceTrash API for delete selected  Invoice
     * user access token
     *
     * @param
     * @user_id
     * @invoice_ids
     */
    private Map<String, String> mDeleteParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        params.put("vessel_ids", String.valueOf(getMultiVesselDetailsData()));
        Log.e("**PARAMS**", params.toString());
        return params;
    }

    public void executeDeleteSelectedTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteSelectedUniversalInvoiceTrash(mDeleteParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mLoadMore.clear();
                    mRecordID.clear();
                    modelArrayList.clear();
                    mInvoiceItemArrayList.clear();
                    executeAPI(1);
                    mAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeDeleteSelectedTrash() {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.DeleteSelectedUniversalInvoiceTrash;
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("vessel_ids", getMultiVesselDetailsData());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.e("test", "******response*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String strStatus = jsonObject.getString("status");
//                    String strMessage = jsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mLoadMore.clear();
//                        mRecordID.clear();
//                        modelArrayList.clear();
//                        mInvoiceItemArrayList.clear();
//                        executeAPI(1);
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("test", "******response*****" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

}
