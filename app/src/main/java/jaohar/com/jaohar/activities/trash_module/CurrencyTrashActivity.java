package jaohar.com.jaohar.activities.trash_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteCurrencySelectedInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteSingleTarshInterface;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.trash_module_adapter.CurrencytrashAdapter;
import jaohar.com.jaohar.models.CurrenciesTrashModel;
import jaohar.com.jaohar.models.CurrencyData;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class CurrencyTrashActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = CurrencyTrashActivity.this;
    /*
     * Getting the Class Name
     * */

    String TAG = CurrencyTrashActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */

    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;

    RecyclerView dataRV;

    ImageView recoverIV, deleteIV, emtyTrashIV;
    CurrencytrashAdapter mAdapter;
    String strIsClickFrom = "";
    /* *
     * Initialize the String and ArrayList
     * */
    private String strLastPage = "FALSE", strLISTID = "";

//    ArrayList<CurrenciesModel> mArrayList = new ArrayList<CurrenciesModel>();
//    ArrayList<CurrenciesModel> multiSelectArrayList = new ArrayList<CurrenciesModel>();
    ArrayList<CurrencyData> mArrayList = new ArrayList<CurrencyData>();
    ArrayList<CurrencyData> multiSelectArrayList = new ArrayList<CurrencyData>();
    ArrayList<String> mRecordID = new ArrayList<String>();

    /*
     * To Recover and Delete Interface
     * */
    RecoverDeleteSingleTarshInterface mrecoverDelete= new RecoverDeleteSingleTarshInterface() {
        @Override
        public void mRecoverDeleteSingleTrash(CurrencyData mModel, String strScreenType) {
            strLISTID = mModel.getId();
            if (strScreenType.equals("recover")) {
                deleteConfirmDialog(getString(R.string.recover_trash_currency_), "recover");
            } else {
                deleteConfirmDialog(getString(R.string.delete_trash_currency_), "delete");
            }
        }
    };
    /**
     * To get Selected and unSelected Vessel Interface
     */
    RecoverDeleteCurrencySelectedInterface mSelectedInterface= new RecoverDeleteCurrencySelectedInterface() {
        @Override
        public void mRecoverSelected(CurrencyData mModel, boolean isSelected) {
            if (mModel != null) {
                if (!isSelected) {
                    multiSelectArrayList.add(mModel);
                    String strID = mModel.getId();
                    mRecordID.add(mModel.getId());
                } else {
                    multiSelectArrayList.remove(mModel);
                    String strID = mModel.getId();
                    mRecordID.remove(mModel.getId());
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_trash);
    }


    /**
     * Set Widget IDS
     **/

    @Override
    protected void setViewsIDs() {
        imgBack = findViewById(R.id.imgBack);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        dataRV = findViewById(R.id.dataRV);
        recoverIV = findViewById(R.id.recoverIV);
        deleteIV = findViewById(R.id.deleteIV);
        emtyTrashIV = findViewById(R.id.emtyTrashIV);
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        txtCenter.setText(getResources().getString(R.string.currency_trash_screen));

        //Get Clicked Value
        if (getIntent() != null) {
            if (getIntent().getStringExtra("isClick").equals("manageUtilities")) {
                strIsClickFrom = getIntent().getStringExtra("isClick");
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {

            executeAPI();

        }
    }

    /**
     * Set Widget TextChangedListener & ClickListener
     */
    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        emtyTrashIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mArrayList.size() > 0) {
                    if (mArrayList.size() > 0) {
                        deleteConfirmDialog(getString(R.string.clear_trash), "empty");
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_currency_found_in_trash));

                    }
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_currency_found_in_trash));

                }
            }
        });

        recoverIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mArrayList.size() > 0) {

                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        if(multiSelectArrayList.size()>0){
                            executeRecoverSelectedTrash();
                        }else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_currency_found_in_trash));

                        }

                    }
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_currency_found_in_trash));

                }
            }
        });

        deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mArrayList.size() > 0) {

                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        if(multiSelectArrayList.size()>0){
                            executeDeleteSelectedTrash();
                        }else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.there_is_no_selected_item));

                        }

                    }
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_currency_found_in_trash));

                }
            }
        });

    }

    /**
     * Call GetAllCurrenciesTrash API for getting
     * list of all deleted companies
     * @param
     * @user_id
     */
//    public void executeAPI() {
//        String strUrl = JaoharConstants.GetAllCurrenciesTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
//
//        AlertDialogManager.showProgressDialog(mActivity);
//
//
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******ResponseCurrencyTrash*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        parseResponse(response);
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
    public void executeAPI() {
//        String strUrl = JaoharConstants.GetAllCurrenciesTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);

        AlertDialogManager.showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<CurrenciesTrashModel> call1 = mApiInterface.getAllCurrenciesTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<CurrenciesTrashModel>() {
            @Override
            public void onResponse(Call<CurrenciesTrashModel> call, retrofit2.Response<CurrenciesTrashModel> response) {
                Log.e(TAG, "******ResponseCurrencyTrash*****" + response);
                AlertDialogManager.hideProgressDialog();
                mArrayList.clear();
                CurrenciesTrashModel mModel = response.body();
                    if (mModel.getStatus().equals("1")) {
                        mArrayList=mModel.getData();
                /*SetAdapter*/
                setAdapter();
            } else if (mModel.getStatus().equals("100")) {
                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());

            }}
        @Override
        public void onFailure(Call<CurrenciesTrashModel> call, Throwable t) {
            AlertDialogManager.hideProgressDialog();
            Log.e(TAG, "******error*****" + t.getMessage());
        }
    });}

//    private void parseResponse(String response) {
//        mArrayList.clear();
//        try {
//            JSONObject mJsonObject = new JSONObject(response);
//            String strStatus = mJsonObject.getString("status");
//            if (strStatus.equals("1")) {
//                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
//                for (int i = 0; i < mJsonArray.length(); i++) {
//                    JSONObject mJson = mJsonArray.getJSONObject(i);
//                    CurrenciesModel mModel = new CurrenciesModel();
//
//                    if (!mJson.isNull("id")) {
//                        mModel.setId(mJson.getString("id"));
//                    }
//                    if (!mJson.isNull("currency_name")) {
//                        mModel.setCurrency_name(mJson.getString("currency_name"));
//                    }
//                    if (!mJson.isNull("alias_name")) {
//                        mModel.setAlias_name(mJson.getString("alias_name"));
//                    }if (!mJson.isNull("added_by")) {
//                        mModel.setAdded_by(mJson.getString("added_by"));
//                    }
//                    mArrayList.add(mModel);
//                }
//
//                /*SetAdapter*/
//                setAdapter();
//            } else if (mJsonObject.getString("status").equals("100")) {
//                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void setAdapter() {
//        CompanyTrashAdapter
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        dataRV.setLayoutManager(layoutManager);
        mAdapter = new CurrencytrashAdapter(mActivity, mArrayList,mSelectedInterface,mrecoverDelete);
        dataRV.setAdapter(mAdapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent = new Intent(mActivity, TrashActivity.class);
        mIntent.putExtra("isClick", strIsClickFrom);
        startActivity(mIntent);
        finish();
    }

    /*
     * Arrange Selected Array list and Get ID from that
     * */
    private ArrayList<String> getMultiVesselDetailsData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();
        Log.e(TAG, "ArrayLIST_DATA1============= " + mRecordID.size());
        for (int i = 0; i < mRecordID.size(); i++) {
            strData.add(mRecordID.get(i));
        }

        return strData;
    }


    /**
     * Implement API for Recover Selected Company
     */


    private Map<String, String> mParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        params.put("currencies", String.valueOf(getMultiVesselDetailsData()));
        Log.e("**PARAMS**",params.toString());
        return params;
    }

    public void executeRecoverSelectedTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.recoverSelectedCurrencyTrash(mParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    mArrayList.clear();
                    mRecordID.clear();
                    executeAPI();
                    mAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" +t.getMessage());

            }
        });
    }


//    public void executeRecoverSelectedTrash() {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.RecoverSelectedCurrencyTrash;
//
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("currencies", getMultiVesselDetailsData());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.e("test", "******responseRecover*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//
//                    String strStatus = jsonObject.getString("status");
//                    String strMessage = jsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//
//                        mArrayList.clear();
//                        mRecordID.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("test", "******response*****" + error.toString());
//            }
//        }) {
//            /* *
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Implement API for Delete Selected Company
     */
    private Map<String, String> mDeleteParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        params.put("currencies", String.valueOf(getMultiVesselDetailsData()));
        Log.e("**PARAMS**", params.toString());
        return params;
    }

    public void executeDeleteSelectedTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteSelectedCurrencyTrash(mDeleteParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mArrayList.clear();
                    mRecordID.clear();
                    executeAPI();
                    mAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


//    public void executeDeleteSelectedTrash() {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.DeleteSelectedCurrencyTrash;
//
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("currencies", getMultiVesselDetailsData());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.e("test", "******responseDelete*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//
//                    String strStatus = jsonObject.getString("status");
//                    String strMessage = jsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//
//                        mArrayList.clear();
//                        mRecordID.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("test", "******response*****" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
    /**
     * Implement API for Clear All Trash
     **/

    private void executeEmptyTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.emptyCurrencyTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "")).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mArrayList.clear();
                    executeAPI();
                    mAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeEmptyTrash() {
//        String strUrl = JaoharConstants.EmptyCurrencyTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mArrayList.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void deleteConfirmDialog(String strMessage, final String strType) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                if (strType.equals("empty")) {
                    /*Execute EMPTY API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeEmptyTrash();
                    }
                } else if (strType.equals("delete")) {
                    /*Execute Delete API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeDeleteTrash();
                    }
                } else {
                    /*Execute Recover API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeRecoverTrash();
                    }
                }
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }


    /**
     * Implement API for Delete Invoice Currency
     */
    private void executeDeleteTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteSingleCurrencyTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""), strLISTID).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            mArrayList.clear();
                            executeAPI();
                            mAdapter.notifyDataSetChanged();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }

//    public void executeDeleteTrash() {
//        String strUrl = JaoharConstants.DeleteSingleCurrencyTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&currency_id=" + strLISTID;// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******ResponseDeleteSingle*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mArrayList.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Implement API for Recover Invoice Currency
     */
    private void executeRecoverTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.recoverSingleCurrencyTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""), strLISTID).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            mArrayList.clear();
                            mRecordID.clear();
                            executeAPI();
                            mAdapter.notifyDataSetChanged();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }


//    public void executeRecoverTrash() {
//        String strUrl = JaoharConstants.RecoverSingleCurrencyTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&currency_id=" + strLISTID;// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******ResponseDeleteSingle*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mArrayList.clear();
//                        mRecordID.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


}
