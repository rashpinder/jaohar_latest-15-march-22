package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.EditCurrencyActivity;
import jaohar.com.jaohar.adapters.CurrenciesAdapter;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.interfaces.DeleteCurrencyInterface;
import jaohar.com.jaohar.interfaces.EditCurrencyInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class SearchInvoiceCurrencyActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = SearchInvoiceCurrencyActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = SearchInvoiceCurrencyActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.cancelTV)
    TextView cancelTV;
    @BindView(R.id.invoicesRV)
    RecyclerView rvCurrencyRV;

    ArrayList<CurrenciesModel> mArrayList = new ArrayList<CurrenciesModel>();
    ArrayList<CurrenciesModel> filteredList = new ArrayList<CurrenciesModel>();
    CurrenciesAdapter mCurrenciesAdapter;

    EditCurrencyInterface mEditCurrencyInterface = new EditCurrencyInterface() {
        @Override
        public void editCurrency(CurrenciesModel mCurrenciesModel) {
            Intent mIntent = new Intent(mActivity, EditCurrencyActivity.class);
            mIntent.putExtra("Model", String.valueOf(mCurrenciesModel));
            mActivity.startActivity(mIntent);
        }
    };

    DeleteCurrencyInterface mDeleteCurrencyInterface = new DeleteCurrencyInterface() {
        @Override
        public void deleteCurrency(CurrenciesModel mModel, int position) {
            deleteConfirmDialog(mModel, position);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_invoice_currency);

        setStatusBar();

        getIntentData();

        ButterKnife.bind(this);
    }

    private void getIntentData() {
        mArrayList = (ArrayList<CurrenciesModel>) getIntent().getSerializableExtra("QuestionListExtra");
    }

    protected void setClickListner() {
        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                charSequence = charSequence.toString().toLowerCase();
                filteredList.clear();
                for (int k = 0; k < mArrayList.size(); k++) {
                    final String text = mArrayList.get(k).getAlias_name().toLowerCase();
                    if (text.contains(charSequence)) {
                        filteredList.add(mArrayList.get(k));
                    }
                }
                rvCurrencyRV.setLayoutManager(new LinearLayoutManager(mActivity));
                mCurrenciesAdapter = new CurrenciesAdapter(mActivity, filteredList, mDeleteCurrencyInterface, mEditCurrencyInterface);
                rvCurrencyRV.setAdapter(mCurrenciesAdapter);
                mCurrenciesAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void deleteConfirmDialog(final CurrenciesModel mCurrenciesModel, int position) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_currency));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mCurrenciesModel, position);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    /*Execute DeleteUser API*/
    private void executeDeleteAPI(CurrenciesModel mCurrenciesModel, int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteCurrencyRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), mCurrenciesModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {

                    mArrayList.remove(position);
                    filteredList.remove(position);

                    if (mCurrenciesAdapter != null)
                        mCurrenciesAdapter.notifyDataSetChanged();

                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

}