package jaohar.com.jaohar.activities.chat_module;

import static android.view.View.GONE;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.chat_module.AllChatParticipantsAdapter;
import jaohar.com.jaohar.adapters.chat_module.SelectedUsersInfoAdapter;
import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;
import jaohar.com.jaohar.interfaces.chat_module.ChatUsersListInterface;
import jaohar.com.jaohar.interfaces.chat_module.ClickChatUsersInterface;
import jaohar.com.jaohar.interfaces.chat_module.RemoveParticipantInterface;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class AddNewParticipantsActivity extends BaseActivity {
    /**
     * set Activity
     **/
    Activity mActivity = AddNewParticipantsActivity.this;

    /**
     * set Activity TAG
     **/
    String TAG = AddNewParticipantsActivity.this.getClass().getSimpleName();

    /**
     * Widgets AND Adapters,Strings, Array List,Boolean and Integers
     **/
    @BindView(R.id.allUsersRV)
    RecyclerView allUsersRV;
    @BindView(R.id.selectedUsersRV)
    RecyclerView selectedUsersRV;
    @BindView(R.id.txtBackTV)
    TextView txtBackTV;
    @BindView(R.id.txtAddTV)
    TextView txtAddTV;
    @BindView(R.id.progressBottomPB)
    ProgressBar progressBottomPB;

    AllChatParticipantsAdapter mChatUsersListAdapter;
    SelectedUsersInfoAdapter mSelectedUsersInfoAdapter;
    ArrayList<ChatUsersModel> modelArrayList = new ArrayList<>();
    ArrayList<ChatUsersModel> mSelectedArrayList = new ArrayList<>();
    ArrayList<String> mSelectedist = new ArrayList<>();

    String strLastPage = "TRUE", strGroupId = "", strGroupName = "", strGroupImage = "";
    int page_no = 1;


    RemoveParticipantInterface removeParticipantInterface = new RemoveParticipantInterface() {
        @Override
        public void mRemoveParticipantInterface(int position, ArrayList<ChatUsersModel> modelArrayList) {
            if (mSelectedist != null) {
                mSelectedist.clear();

                for (int k = 0; k < modelArrayList.size(); k++) {
                    mSelectedist.add(modelArrayList.get(k).getId());
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        setContentView(R.layout.activity_add_new_participants);
        ButterKnife.bind(this);
        getIntentData();
        executeUsersAPI();
    }

    private void getIntentData() {
        if (getIntent().getExtras() != null) {
            Intent i = getIntent();
            mSelectedArrayList = (ArrayList<ChatUsersModel>) i.getSerializableExtra(JaoharConstants.GROUP_PARTICIPANTS);
            strGroupId = getIntent().getStringExtra(JaoharConstants.GROUP_ID);
            strGroupName = getIntent().getStringExtra(JaoharConstants.GROUP_NAME);
            if (getIntent().getStringExtra(JaoharConstants.GROUP_IMAGE) != null && !getIntent().getStringExtra(JaoharConstants.GROUP_IMAGE).equals("")){
                strGroupImage = ImageUtils.getInstant().getBase64FromBitmap(getBitmapFromURL(getIntent().getStringExtra(JaoharConstants.GROUP_IMAGE)));
            }
            for (int k = 0; k < mSelectedArrayList.size(); k++) {
                mSelectedist.add(mSelectedArrayList.get(k).getId());
            }
            setSelectedAdapter(mSelectedArrayList);
        }
    }

    private void setSelectedAdapter(ArrayList<ChatUsersModel> chatUsersModelList) {
        selectedUsersRV.setLayoutManager(new LinearLayoutManager(mActivity, RecyclerView.HORIZONTAL, false));
        mSelectedUsersInfoAdapter = new SelectedUsersInfoAdapter(mActivity, chatUsersModelList, removeParticipantInterface);
        selectedUsersRV.setAdapter(mSelectedUsersInfoAdapter);
    }

    @OnClick({R.id.txtAddTV, R.id.txtBackTV})
    public void onViewCLicked(View view) {

        switch (view.getId()) {
            case R.id.txtAddTV:
                performAddClick();
                break;

            case R.id.txtBackTV:
                onBackPressed();
                break;
        }
    }

    private void performAddClick() {
        executeEditChatGroupAPI();
    }

    /* *
     * Execute API for getting Users list
     * @param
     * @user_id
     * */
    public void executeUsersAPI() {
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_no == 1) {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllChatUsers(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), page_no);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });

    }

    void parseResponse(String response) {
        try {
            JSONObject mJSonObject = new JSONObject(response);
            String strStatus = String.valueOf(mJSonObject.getInt("status"));
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_users");
                if (mjsonArrayData != null) {
                    ArrayList<ChatUsersModel> mTempraryList = new ArrayList<>();
                    mTempraryList.clear();
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        ChatUsersModel mModel = new ChatUsersModel();
                        if (!mJsonDATA.getString("id").equals("")) {
                            mModel.setId(mJsonDATA.getString("id"));
                        }
                        if (!mJsonDATA.getString("email").equals("")) {
                            mModel.setEmail(mJsonDATA.getString("email"));
                        }
                        if (!mJsonDATA.getString("role").equals("")) {
                            mModel.setRole(mJsonDATA.getString("role"));
                        }
                        if (!mJsonDATA.getString("company_name").equals("")) {
                            mModel.setCompany_name(mJsonDATA.getString("company_name"));
                        }
                        if (!mJsonDATA.getString("first_name").equals("")) {
                            mModel.setFirst_name(mJsonDATA.getString("first_name"));
                        }
                        if (!mJsonDATA.getString("last_name").equals("")) {
                            mModel.setLast_name(mJsonDATA.getString("last_name"));
                        }
                        if (!mJsonDATA.getString("job").equals("")) {
                            mModel.setJob(mJsonDATA.getString("job"));
                        }
                        if (!mJsonDATA.getString("image").equals("")) {
                            mModel.setImage(mJsonDATA.getString("image"));
                        }
                        if (!mJsonDATA.getString("chat_status").equals("")) {
                            mModel.setChat_status(mJsonDATA.getString("chat_status"));
                        }
                        if (!mJsonDATA.getString("unread_chat").equals("")) {
                            mModel.setUnread_chat(mJsonDATA.getString("unread_chat"));
                        }
                        if (!mJsonDATA.getString("online_state").equals("")) {
                            mModel.setOnline_state(mJsonDATA.getString("online_state"));
                        }
                        if (!mJsonDATA.getString("message_time").equals("")) {
                            mModel.setMessage_time(mJsonDATA.getString("message_time"));
                        }
                        if (!mJsonDATA.getString("message").equals("")) {
                            mModel.setMessage(mJsonDATA.getString("message"));
                        }
                        if (!mJsonDATA.getString("room_id").equals("")) {
                            mModel.setRoom_id(mJsonDATA.getString("room_id"));
                        }
                        mTempraryList.add(mModel);
                    }
                    modelArrayList.addAll(mTempraryList);
                    if (page_no == 1) {
                        setAdapter();
                    } else {
                        mChatUsersListAdapter.notifyDataSetChanged();
                    }
                }
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setAdapter() {
        allUsersRV.setNestedScrollingEnabled(false);
        allUsersRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mChatUsersListAdapter = new AllChatParticipantsAdapter(mActivity, modelArrayList, mPaginationInterFace, mClickChatUsersInterface);
        allUsersRV.setAdapter(mChatUsersListAdapter);
    }

    /**
     * Recycler View Pagination Adapter Interface
     **/
    ChatUsersListInterface mPaginationInterFace = new ChatUsersListInterface() {
        @Override
        public void mChatUsersListInterface(boolean isLastScroll) {
            if (isLastScroll == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            progressBottomPB.setVisibility(View.VISIBLE);
                            ++page_no;
                            executeUsersAPI();
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                        }
                    }
                }, 1500);
            }
        }
    };

    ClickChatUsersInterface mClickChatUsersInterface = new ClickChatUsersInterface() {
        @Override
        public void mChatUsersListInterface(int position, ArrayList<ChatUsersModel> mModelArrayList) {
            if (!mSelectedist.contains(mModelArrayList.get(position).getId())) {
                mSelectedArrayList.add(mModelArrayList.get(position));
                mSelectedist.add(mModelArrayList.get(position).getId());
                setSelectedAdapter(mSelectedArrayList);
            }
        }
    };

    /*
    Chat listing
     */
    Map<String, String> mParams() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        params.put("group_id", strGroupId);
        params.put("group_name", strGroupName);
        params.put("user_list", String.valueOf(mSelectedist));
        params.put("photo", strGroupImage);
        Log.e(TAG, "**PARAMS**" + params);
        return params;
    }

    public void executeEditChatGroupAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        String mApiUrl = JaoharConstants.EditChatGroup;
        Log.e(TAG, "**Api Url**" + mApiUrl);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.editChatGroup1(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strGroupId, strGroupName,
                String.valueOf(mSelectedist), strGroupImage);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    AlertDialogManager.hideProgressDialog();
                    String status = String.valueOf(jsonObject.getInt("status"));
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
                        showAddGroupAlertDialog(mActivity, getString(R.string.app_name), "" + getString(R.string.group_updated_successfully));
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
            }
        });
    }

    /*
     * Show Alert Dailog Box
     * */
    public void showAddGroupAlertDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                finish();
            }
        });
        alertDialog.show();
    }

}