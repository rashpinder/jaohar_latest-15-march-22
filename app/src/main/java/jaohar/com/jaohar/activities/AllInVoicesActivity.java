package jaohar.com.jaohar.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.InVoicesAdapter;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.beans.PaymentModel;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.DeleteInvoiceInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.PDFdownloadInterface;
import jaohar.com.jaohar.interfaces.SendMultipleInvoiceMAIlInterface;
import jaohar.com.jaohar.interfaces.SinglemailInvoiceInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.FileDownloader;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AllInVoicesActivity extends BaseActivity {

    Activity mActivity = AllInVoicesActivity.this;
    String TAG = AllInVoicesActivity.this.getClass().getSimpleName();

    LinearLayout llLeftLL;
    RelativeLayout imgRightLL, downArrowRL, resetRL1;
    TextView txtCenter, txtRight, txtMailTV, mailAllInvoices;
    ImageView imgBack, imgRight;
    RecyclerView inVoicesRV;
    InVoicesModel mInVoicesModel;
    InVoicesAdapter mInVoicesAdapter;
    NestedScrollView invoiceNestedScroll;
    SwipeRefreshLayout swipeToRefresh;
    EditText editSearchET;
    ProgressBar progressBottomPB;
    Boolean isScroolling = false;
    boolean isSwipeRefresh = false;
    ArrayList<InVoicesModel> modelArrayList = new ArrayList<InVoicesModel>();
    ArrayList<InVoicesModel> mLoadMore = new ArrayList<InVoicesModel>();
    ArrayList<InvoiceAddItemModel> mInvoiceItemArrayList = new ArrayList<InvoiceAddItemModel>();
    ArrayList<String> mArrayListStamps = new ArrayList<String>();
    ArrayList<String> mArrayListCompanys = new ArrayList<String>();
    ArrayList<String> mArrayListBankDetails = new ArrayList<String>();
    ArrayList<String> vesselArrayList = new ArrayList<String>();
    ArrayList<String> mArrayListCurrency = new ArrayList<String>();
    ArrayList<String> mArrayListSignDATA = new ArrayList<String>();
    ArrayList<String> mInvoiceID = new ArrayList<String>();
    boolean isNormalSearch = false, isAdvanceSearch = false;
    String strInvoiceNum, strInvoiceDateFrom, strInvoiceDateTo, strInvoiceCompany, strInvoiceVessel, strInvoiceCurrency, strInvoiceStamp, strInvoiceSign, strInvoiceStatus, strInvoiceBank;
    String arrayVesselsType[], arrayStatus[], arrayCurrency[];
    private static int page_no = 1;
    private String strLastPage = "FALSE", strInvoiceNumber, strInvoiceVesselName;
    private DownloadManager downloadManager;
    private long downloadReference;
    static int IsInvoiceActive = 0;

    DeleteInvoiceInterface mDeleteInvoiceInterface = new DeleteInvoiceInterface() {
        @Override
        public void deleteInvoice(InVoicesModel mInVoicesModel) {
            deleteConfirmDialog(mInVoicesModel);
        }
    };

    private ArrayList<InVoicesModel> multiSelectArrayList = new ArrayList<InVoicesModel>();

    SendMultipleInvoiceMAIlInterface mMultimaIlInterface = new SendMultipleInvoiceMAIlInterface() {
        @Override
        public void mSendMutliInvoice(InVoicesModel mInVoivce, boolean mDelete) {
            if (mInVoivce != null) {
                if (!mDelete) {

                    IsInvoiceActive = IsInvoiceActive + 1;
                    multiSelectArrayList.add(mInVoivce);
                    String strID = mInVoivce.getInvoice_number();
                    mInvoiceID.add(mInVoivce.getInvoice_number());

                } else {
                    IsInvoiceActive = IsInvoiceActive - 1;
                    multiSelectArrayList.remove(mInVoivce);
                    String strID = mInVoivce.getInvoice_number();

                    mInvoiceID.remove(mInVoivce.getInvoice_number());
                }
                if (IsInvoiceActive > 0) {
                    txtMailTV.setVisibility(View.VISIBLE);
                } else {
                    txtMailTV.setVisibility(View.GONE);
                }
            }
        }
    };


    SinglemailInvoiceInterface msinglemailInvoiceInterface = new SinglemailInvoiceInterface() {
        @Override
        public void mSinglemailInvoice(InVoicesModel mInvoiceModel) {
            strInvoiceNumber = "JAORO" + mInvoiceModel.getInvoice_number();
            strInvoiceVesselName = "   " + mInvoiceModel.getmVesselSearchInvoiceModel().getVessel_name();
            /*Send Email*/
            Intent mIntent = new Intent(mActivity, SendingInvoiceEmailActivity.class);
            mIntent.putExtra("SubjectName", "Invoice    " + strInvoiceNumber + strInvoiceVesselName);
            mIntent.putExtra("vesselID", mInvoiceModel.getInvoice_id());
            startActivity(mIntent);
//              finish();
        }
    };

    PDFdownloadInterface mPdfDownloader = new PDFdownloadInterface() {
        @Override
        public void pdfDownloadInterface(final String strPDF, final String strPDFNAme) {
//          new DownloadFile().execute("http://maven.apache.org/maven-1.x/maven.pdf", "maven.pdf");
            if (!strPDF.equals("")) {

                // your code
                downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse(strPDF);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                //Restrict the types of networks over which this download may proceed.
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                //Set whether this download may proceed over a roaming connection.
                request.setAllowedOverRoaming(false);
                //Set the title of this download, to be displayed in notifications (if enabled).
                request.setTitle(strPDFNAme);
                //Set a description of this download, to be displayed in notifications (if enabled)
                request.setDescription("" + "Invoice PDF is Download Please Wait...");
                //Set the local destination for the downloaded file to a path within the application's external files directory
                request.setDestinationInExternalFilesDir(mActivity, Environment.DIRECTORY_DOWNLOADS, "InvoicePDFList.pdf");
                //Enqueue a new download and same the referenceId
                downloadReference = downloadManager.enqueue(request);


            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), "Their is no PDF");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_in_voices);

        setStatusBar();
    }

    @Override
    protected void setViewsIDs() {
        progressBottomPB = findViewById(R.id.progressBottomPB);
        editSearchET = (EditText) findViewById(R.id.editSearchET);

        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtMailTV = (TextView) findViewById(R.id.txtMailTV);
        mailAllInvoices = (TextView) findViewById(R.id.mailAllInvoices);
        txtMailTV.setVisibility(View.GONE);
//        txtRight = (TextView) findViewById(R.id.txtRight);
//        txtRight.setVisibility(View.GONE);
        txtCenter.setText(getString(R.string.all_invoices));
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        downArrowRL = (RelativeLayout) findViewById(R.id.downArrowRL);
        resetRL1 = (RelativeLayout) findViewById(R.id.resetRL1);
        imgRightLL.setVisibility(View.VISIBLE);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgRight.setImageResource(R.drawable.plus_symbol);
        inVoicesRV = (RecyclerView) findViewById(R.id.inVoicesRV);
        invoiceNestedScroll = (NestedScrollView) findViewById(R.id.invoiceNestedScroll);
        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                isNormalSearch = false;
                isAdvanceSearch = false;
                modelArrayList.clear();
                mLoadMore.clear();
                page_no = 1;
                executeAPI();
            }
        });

        invoiceNestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
//                    modelArrayList.clear();
//                    mLoadMore.clear();
                    if (isAdvanceSearch == false) {
                        if (isNormalSearch == false) {
                            isScroolling = true;
                            ++page_no;
                            progressBottomPB.setVisibility(View.VISIBLE);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (strLastPage.equals("FALSE")) {
                                        executeAPI();
                                    } else {
                                        progressBottomPB.setVisibility(View.GONE);
                                    }
                                }
                            }, 500);
                        }
                    }
                    /* else {
                    progressPB.setVisibility(View.GONE);
                }*/
                }
            }
        });


    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransitionExit();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strLastPage = "FALSE";
                Intent mIntent = new Intent(mActivity, AddInvoiceActivity.class);
                mIntent.putExtra("Title", "Add Invoice");
                mActivity.startActivity(mIntent);
                overridePendingTransitionEnter();
            }
        });

        downArrowRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeStampSAPI();
                advancedSearchView();
            }
        });

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    executeNormalSearch();
                    return true;
                }
                return false;
            }
        });
        txtMailTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Send Email*/
                Intent mIntent = new Intent(mActivity, SendingInvoiceEmailActivity.class);
                mIntent.putExtra("vessalName1", "Your Requested list of Invoices from Jaohar UK Limited");
                mIntent.putExtra("recordIDArray", getMultiVesselDetailsData());
                startActivity(mIntent);
//                finish();


            }
        });
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    resetRL1.setVisibility(View.VISIBLE);
                } else {
                    resetRL1.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        resetRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IsInvoiceActive = 0;
                isAdvanceSearch = false;
                isNormalSearch = false;
                editSearchET.setText("");
                txtMailTV.setVisibility(View.GONE);
                //*Execute Vesseles API*//*
                modelArrayList.clear();
                mLoadMore.clear();
                page_no = 1;
                executeAPI();

            }
        });
        mailAllInvoices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Send All Inoice*/
                /*Send Email*/
                Intent mIntent = new Intent(mActivity, SendingInvoiceEmailActivity.class);
                mIntent.putExtra("vessalName2", "Your Requested list of Invoices from Jaohar UK Limited");
                mIntent.putExtra("recordIDArray", getMultiVesselDetailsData());
                startActivity(mIntent);
//                finish();

            }
        });

    }

    private ArrayList<String> getMultiVesselDetailsData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();
        Log.e(TAG, "ArrayLIST_DATA1============= " + mInvoiceID.size());
        for (int i = 0; i < mInvoiceID.size(); i++) {
            strData.add(mInvoiceID.get(i));
        }
        mInvoiceID.clear();
        return strData;
    }

    @Override
    protected void onResume() {
        super.onResume();
        IsInvoiceActive = 0;
        txtMailTV.setVisibility(View.GONE);
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute Vesseles API*//*
            modelArrayList.clear();
            mLoadMore.clear();
            page_no = 1;
            if (strLastPage.equals("FALSE")) {
                executeAPI();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent mIntent = new Intent(mActivity, HomeActivity.class);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
//        mActivity.startActivity(mIntent);
//        mActivity.finish();
    }

    public void executeAPI() {
//        modelArrayList.clear();
//        + "&page_no=" + page_no
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);

        }
        if (page_no == 1) {
            isSwipeRefresh = false;
            swipeToRefresh.setRefreshing(false);
            progressBottomPB.setVisibility(View.GONE);
            AlertDialogManager.showProgressDialog(mActivity);

        }

        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);

        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllInvoiceRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), String.valueOf(page_no));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                resetRL1.setVisibility(View.GONE);
                AlertDialogManager.hideProgressDialog();
                progressBottomPB.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }
                Log.e(TAG, "*****ResponseALLk****" + response.body());
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void executeNormalSearch() {
        modelArrayList.clear();
        resetRL1.setVisibility(View.VISIBLE);
//        + "&page_no=" + page_no
//        String strUrl = JaoharConstants.Simple_Search_Invoice + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&search=" + editSearchET.getText().toString() + "&role=staff";

//        strUrl = strUrl.replace(" ", "%20");
//        Log.e(TAG, "***URL***" + strUrl);
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.searchInvoiceRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), editSearchET.getText().toString(), "staff");
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                editSearchET.setText("");
                isNormalSearch = true;
                Log.e(TAG, "*****Response****" + response);
                try {
                    JSONObject mJsonObject111 = new JSONObject(response.body().toString());
                    if (mJsonObject111.getString("status").equals("1")) {
                        AlertDialogManager.hideProgressDialog();
                        JSONArray mJsonArray = mJsonObject111.getJSONArray("all_searched_invoices");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJson = mJsonArray.getJSONObject(i);
                            InVoicesModel mModel = new InVoicesModel();
                            JSONObject mAllDataObj = mJson.getJSONObject("all_data");
                            if (!mAllDataObj.getString("pdf").equals("")) {
                                mModel.setPdf(mAllDataObj.getString("pdf"));
                            }
                            if (!mAllDataObj.getString("pdf_name").equals("")) {
                                mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
                            }
                            if (!mAllDataObj.isNull("invoice_id"))
                                mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                            if (!mAllDataObj.isNull("invoice_no"))
                                mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
                            if (!mAllDataObj.isNull("invoice_date"))
                                mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
                            if (!mAllDataObj.isNull("term_days"))
                                mModel.setTerm_days(mAllDataObj.getString("term_days"));
                            if (!mAllDataObj.isNull("currency"))
                                mModel.setCurrency(mAllDataObj.getString("currency"));
                            if (!mAllDataObj.isNull("status"))
                                mModel.setStatus(mAllDataObj.getString("status"));
                            if (!mAllDataObj.isNull("reference"))
                                mModel.setRefrence1(mAllDataObj.getString("reference"));
                            if (!mAllDataObj.isNull("reference1"))
                                mModel.setRefrence2(mAllDataObj.getString("reference1"));
                            if (!mAllDataObj.isNull("reference2"))
                                mModel.setRefrence3(mAllDataObj.getString("reference2"));
                            if (!mAllDataObj.isNull("payment_id"))
                                mModel.setPayment_id(mAllDataObj.getString("payment_id"));
                            if (!mJson.getString("sign_data").equals("")) {
                                JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                                SignatureModel mSignModel = new SignatureModel();
                                if (!mSignDataObj.isNull("sign_id"))
                                    mSignModel.setId(mSignDataObj.getString("sign_id"));
                                if (!mSignDataObj.isNull("sign_name"))
                                    mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                                if (!mSignDataObj.isNull("sign_image"))
                                    mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                                mModel.setmSignatureModel(mSignModel);
                            }
                            if (!mJson.getString("stamp_data").equals("")) {
                                JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                                StampsModel mStampsModel = new StampsModel();
                                if (!mStampDataObj.isNull("stamp_id"))
                                    mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                                if (!mStampDataObj.isNull("stamp_name"))
                                    mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                                if (!mStampDataObj.isNull("stamp_image"))
                                    mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                                mModel.setmStampsModel(mStampsModel);
                            }
                            if (!mJson.getString("bank_data").equals("")) {
                                JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                                BankModel mBankModel = new BankModel();
                                if (!mBankDataObj.isNull("bank_id"))
                                    mBankModel.setId(mBankDataObj.getString("bank_id"));
                                if (!mBankDataObj.isNull("beneficiary"))
                                    mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                                if (!mBankDataObj.isNull("bank_name"))
                                    mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                                if (!mBankDataObj.isNull("address1"))
                                    mBankModel.setAddress1(mBankDataObj.getString("address1"));
                                if (!mBankDataObj.isNull("address2"))
                                    mBankModel.setAddress2(mBankDataObj.getString("address2"));
                                if (!mBankDataObj.isNull("iban_ron"))
                                    mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                                if (!mBankDataObj.isNull("iban_usd"))
                                    mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                                if (!mBankDataObj.isNull("iban_eur"))
                                    mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                                if (!mBankDataObj.isNull("swift"))
                                    mBankModel.setSwift(mBankDataObj.getString("swift"));
                                mModel.setmBankModel(mBankModel);
                            }
                            if (!mJson.getString("search_vessel_data").equals("")) {
                                JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                                VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                                if (!mSearchVesselObj.isNull("vessel_id"))
                                    mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                                if (!mSearchVesselObj.isNull("vessel_name"))
                                    mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                                if (!mSearchVesselObj.isNull("IMO_no"))
                                    mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                                if (!mSearchVesselObj.isNull("flag"))
                                    mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                                mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                            }
                            if (!mJson.getString("search_company_data").equals("")) {
                                JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                                CompaniesModel mCompaniesModel = new CompaniesModel();
                                if (!mSearchCompanyObj.isNull("id"))
                                    mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                                if (!mSearchCompanyObj.isNull("company_name"))
                                    mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                                if (!mSearchCompanyObj.isNull("Address1"))
                                    mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                                if (!mSearchCompanyObj.isNull("Address2"))
                                    mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                                if (!mSearchCompanyObj.isNull("Address3"))
                                    mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                                if (!mSearchCompanyObj.isNull("Address4"))
                                    mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                                if (!mSearchCompanyObj.isNull("Address5"))
                                    mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                                mModel.setmCompaniesModel(mCompaniesModel);
                            }
                            if (!mJson.getString("payment_data").equals("")) {
                                JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                                PaymentModel mPaymentModel = new PaymentModel();
                                if (!mPaymentObject.isNull("payment_id")) {
                                    mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                                }
                                if (!mPaymentObject.isNull("sub_total")) {
                                    mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                                }
                                if (!mPaymentObject.isNull("VAT")) {
                                    mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                                }
                                if (!mPaymentObject.isNull("vat_price")) {
                                    mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                                }
                                if (!mPaymentObject.isNull("total")) {
                                    mPaymentModel.setTotal(mPaymentObject.getString("total"));
                                }
                                if (!mPaymentObject.isNull("paid")) {
                                    mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                                }
                                if (!mPaymentObject.isNull("due")) {
                                    mPaymentModel.setPaid(mPaymentObject.getString("due"));
                                }
                                mModel.setmPaymentModel(mPaymentModel);
                            }
                            if (!mJson.getString("items_data").equals("")) {
                                JSONArray mItemArray = mJson.getJSONArray("items_data");
                                for (int k = 0; k < mItemArray.length(); k++) {
                                    JSONObject mItemObj = mItemArray.getJSONObject(k);
                                    InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                                    if (!mItemObj.isNull("item_id"))
                                        mItemModel.setItemID(mItemObj.getString("item_id"));
                                    if (!mItemObj.isNull("item_serial_no"))
                                        mItemModel.setItem(mItemObj.getString("item_serial_no"));
                                    if (!mItemObj.isNull("quantity"))
                                        mItemModel.setQuantity(mItemObj.getInt("quantity"));
                                    if (!mItemObj.isNull("price"))
                                        mItemModel.setUnitprice(mItemObj.getString("price"));
                                    if (!mItemObj.isNull("description"))
                                        mItemModel.setDescription(mItemObj.getString("description"));
                                    mInvoiceItemArrayList.add(mItemModel);
                                }
                                mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                            }

                            modelArrayList.add(mModel);


                        }

                        /*Set Adapter*/
                        setAdapter();


                    } else if (mJsonObject111.getString("status").equals("100")) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                    } else {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
//                        page_no = 1;
//                        executeAPI();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                parseResponse(response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    private void parseResponse(String response) {
        mLoadMore.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            if (mJsonObject.getString("status").equals("1")) {
                JSONObject mDataObject = mJsonObject.getJSONObject("data");
                JSONArray mJsonArray = mDataObject.getJSONArray("all_invoices");
                strLastPage = mDataObject.getString("last_page");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    InVoicesModel mModel = new InVoicesModel();
                    JSONObject mAllDataObj = mJson.getJSONObject("all_data");
                    if (!mAllDataObj.isNull("invoice_id"))
                        mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                    if (!mAllDataObj.getString("pdf").equals("")) {
                        mModel.setPdf(mAllDataObj.getString("pdf"));
                    }
                    if (!mAllDataObj.getString("pdf_name").equals("")) {
                        mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
                    }
                    if (!mAllDataObj.isNull("invoice_no"))
                        mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
                    if (!mAllDataObj.isNull("invoice_date"))
                        mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
                    if (!mAllDataObj.isNull("term_days"))
                        mModel.setTerm_days(mAllDataObj.getString("term_days"));
                    if (!mAllDataObj.isNull("currency"))
                        mModel.setCurrency(mAllDataObj.getString("currency"));
                    if (!mAllDataObj.isNull("status"))
                        mModel.setStatus(mAllDataObj.getString("status"));
                    if (!mAllDataObj.isNull("reference"))
                        mModel.setRefrence1(mAllDataObj.getString("reference"));
                    if (!mAllDataObj.isNull("reference1"))
                        mModel.setRefrence2(mAllDataObj.getString("reference1"));
                    if (!mAllDataObj.isNull("reference2"))
                        mModel.setRefrence3(mAllDataObj.getString("reference2"));
                    if (!mAllDataObj.isNull("payment_id"))
                        mModel.setPayment_id(mAllDataObj.getString("payment_id"));
                    if (!mJson.getString("sign_data").equals("")) {
                        JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                        SignatureModel mSignModel = new SignatureModel();
                        if (!mSignDataObj.isNull("sign_id"))
                            mSignModel.setId(mSignDataObj.getString("sign_id"));
                        if (!mSignDataObj.isNull("sign_name"))
                            mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                        if (!mSignDataObj.isNull("sign_image"))
                            mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                        mModel.setmSignatureModel(mSignModel);
                    }
                    if (!mJson.getString("stamp_data").equals("")) {
                        JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                        StampsModel mStampsModel = new StampsModel();
                        if (!mStampDataObj.isNull("stamp_id"))
                            mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                        if (!mStampDataObj.isNull("stamp_name"))
                            mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                        if (!mStampDataObj.isNull("stamp_image"))
                            mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                        mModel.setmStampsModel(mStampsModel);
                    }
                    if (!mJson.getString("bank_data").equals("")) {
                        JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                        BankModel mBankModel = new BankModel();
                        if (!mBankDataObj.isNull("bank_id"))
                            mBankModel.setId(mBankDataObj.getString("bank_id"));
                        if (!mBankDataObj.isNull("beneficiary"))
                            mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                        if (!mBankDataObj.isNull("bank_name"))
                            mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                        if (!mBankDataObj.isNull("address1"))
                            mBankModel.setAddress1(mBankDataObj.getString("address1"));
                        if (!mBankDataObj.isNull("address2"))
                            mBankModel.setAddress2(mBankDataObj.getString("address2"));
                        if (!mBankDataObj.isNull("iban_ron"))
                            mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                        if (!mBankDataObj.isNull("iban_usd"))
                            mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                        if (!mBankDataObj.isNull("iban_eur"))
                            mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                        if (!mBankDataObj.isNull("swift"))
                            mBankModel.setSwift(mBankDataObj.getString("swift"));
                        mModel.setmBankModel(mBankModel);
                    }
                    if (!mJson.getString("search_vessel_data").equals("")) {
                        JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                        VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                        if (!mSearchVesselObj.isNull("vessel_id"))
                            mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                        if (!mSearchVesselObj.isNull("vessel_name"))
                            mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                        if (!mSearchVesselObj.isNull("IMO_no"))
                            mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                        if (!mSearchVesselObj.isNull("flag"))
                            mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                        mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                    }
                    if (!mJson.getString("search_company_data").equals("")) {
                        JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                        CompaniesModel mCompaniesModel = new CompaniesModel();
                        if (!mSearchCompanyObj.isNull("id"))
                            mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                        if (!mSearchCompanyObj.isNull("company_name"))
                            mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                        if (!mSearchCompanyObj.isNull("Address1"))
                            mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                        if (!mSearchCompanyObj.isNull("Address2"))
                            mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                        if (!mSearchCompanyObj.isNull("Address3"))
                            mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                        if (!mSearchCompanyObj.isNull("Address4"))
                            mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                        if (!mSearchCompanyObj.isNull("Address5"))
                            mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                        mModel.setmCompaniesModel(mCompaniesModel);
                    }
                    if (!mJson.getString("payment_data").equals("")) {
                        JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                        PaymentModel mPaymentModel = new PaymentModel();
                        if (!mPaymentObject.isNull("payment_id")) {
                            mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                        }
                        if (!mPaymentObject.isNull("sub_total")) {
                            mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                        }
                        if (!mPaymentObject.isNull("VAT")) {
                            mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                        }
                        if (!mPaymentObject.isNull("vat_price")) {
                            mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                        }
                        if (!mPaymentObject.isNull("total")) {
                            mPaymentModel.setTotal(mPaymentObject.getString("total"));
                        }
                        if (!mPaymentObject.isNull("paid")) {
                            mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                        }
                        if (!mPaymentObject.isNull("due")) {
                            mPaymentModel.setPaid(mPaymentObject.getString("due"));
                        }
                        mModel.setmPaymentModel(mPaymentModel);
                    }
                    if (!mJson.getString("items_data").equals("")) {
                        JSONArray mItemArray = mJson.getJSONArray("items_data");
                        for (int k = 0; k < mItemArray.length(); k++) {
                            JSONObject mItemObj = mItemArray.getJSONObject(k);
                            InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                            if (!mItemObj.isNull("item_id"))
                                mItemModel.setItemID(mItemObj.getString("item_id"));
                            if (!mItemObj.isNull("item_serial_no"))
                                mItemModel.setItem(mItemObj.getString("item_serial_no"));
                            if (!mItemObj.isNull("quantity"))
                                mItemModel.setQuantity(mItemObj.getInt("quantity"));
                            if (!mItemObj.isNull("price"))
                                mItemModel.setUnitprice(mItemObj.getString("price"));
                            if (!mItemObj.isNull("description"))
                                mItemModel.setDescription(mItemObj.getString("description"));
                            mInvoiceItemArrayList.add(mItemModel);
                        }
                        mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                    }
                    if (page_no == 1) {
                        modelArrayList.add(mModel);
                    } else if (page_no > 1) {
                        mLoadMore.add(mModel);
                    }
                }
                if (mLoadMore.size() > 0) {
                    modelArrayList.addAll(mLoadMore);
                }
                /*Set Adapter*/
                setAdapter();
            } else if (mJsonObject.getString("status").equals("100")) {
                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, "*exception*" + e.toString());
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        inVoicesRV.setNestedScrollingEnabled(false);
        inVoicesRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mInVoicesAdapter = new InVoicesAdapter(mActivity, modelArrayList, mDeleteInvoiceInterface,
                mPdfDownloader, msinglemailInvoiceInterface, mMultimaIlInterface, mOnClickInterface);
        inVoicesRV.setAdapter(mInVoicesAdapter);
//        mInVoicesAdapter.notifyDataSetChanged();
    }

    OnClickInterface mOnClickInterface = new OnClickInterface() {
        @Override
        public void mOnClickInterface(int position) {

        }
    };

    public void deleteConfirmDialog(final InVoicesModel mInVoicesModel) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_invoice));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mInVoicesModel);
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }

    public void executeDeleteAPI(InVoicesModel mInVoicesModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteInvoiceRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), mInVoicesModel.getInvoice_id());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    //Toast.makeText(mActivity,""+mJsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    modelArrayList.clear();
                    executeAPI();
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    public void advancedSearchView() {
        final Dialog searchDialog = new Dialog(mActivity);
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_advance_search_invoice);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        // set the custom dialog components - text, image and button
        Button btn_search = (Button) searchDialog.findViewById(R.id.btn_search);
        ImageView imgCloseIV = (ImageView) searchDialog.findViewById(R.id.imgCloseIV);
        final EditText editInvoiceET = (EditText) searchDialog.findViewById(R.id.editInvoiceET);
        final EditText editDateFromET = (EditText) searchDialog.findViewById(R.id.editDateFromET);
        final EditText editDateToET = (EditText) searchDialog.findViewById(R.id.editDateToET);
        final EditText editCompanyET = (EditText) searchDialog.findViewById(R.id.editCompanyET);
//        final Spinner typeSpinner = (Spinner) searchDialog.findViewById(R.id.typeSpinner);
        final EditText editVesselET = (EditText) searchDialog.findViewById(R.id.editVesselET);
        final EditText editCurrencyET = (EditText) searchDialog.findViewById(R.id.editCurrencyET);
        final EditText editStampET = (EditText) searchDialog.findViewById(R.id.editStampET);
        final EditText editSignET = (EditText) searchDialog.findViewById(R.id.editSignET);
        final EditText editStatusET = (EditText) searchDialog.findViewById(R.id.editStatusET);
        final EditText editBankET = (EditText) searchDialog.findViewById(R.id.editBankET);
        final EditText editNameET = (EditText) searchDialog.findViewById(R.id.editNameET);

        arrayStatus = mActivity.getResources().getStringArray(R.array.status_array);
        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });
        editDateFromET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    int mYear, mMonth, mDay, mHour, mMinute;
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    int intMonth = monthOfYear + 1;
//                                  String actualFormatDate = ""+dayOfMonth +" " + Utilities.getMonthNameFromNumber(Utilities.getFormatedString("" + intMonth)) + " " + ""+year;
                                    editDateFromET.setText(year + "/" + Utilities.getFormatedString("" + intMonth) + "/" + Utilities.getFormatedString("" + dayOfMonth));
                                    //mEditText.setText(actualFormatDate);
                                }
                            }, mYear, mMonth, mDay);


                    datePickerDialog.show();
                    return true;
                }
                return false;
            }
        });

        editDateToET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final int mYear;
                    final int mMonth;
                    final int mDay;
                    int mHour;
                    int mMinute;
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    int intMonth = monthOfYear + 1;

//                        String actualFormatDate = ""+dayOfMonth +" " + Utilities.getMonthNameFromNumber(Utilities.getFormatedString("" + intMonth)) + " " + ""+year;
                                    editDateToET.setText(year + "/" + Utilities.getFormatedString("" + intMonth) + "/" + Utilities.getFormatedString("" + dayOfMonth));
                                    //mEditText.setText(actualFormatDate);
                                }
                            }, mYear, mMonth, mDay);


                    datePickerDialog.show();
                    return true;
                }
                return false;
            }
        });


        editStampET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Stamps");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListStamps);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                            strType = mArrayListStamps.get(position);
                            editStampET.setText(mArrayListStamps.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editCompanyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Company");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListCompanys);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                            strType = mArrayListStamps.get(position);
                            editCompanyET.setText(mArrayListCompanys.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editBankET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Bank Details");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListBankDetails);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                            strType = mArrayListStamps.get(position);
                            editBankET.setText(mArrayListBankDetails.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editVesselET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessels");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, vesselArrayList);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                            strType = mArrayListStamps.get(position);
                            editVesselET.setText(vesselArrayList.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });
        editCurrencyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Currency");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListCurrency);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                            strType = mArrayListStamps.get(position);
                            editCurrencyET.setText(mArrayListCurrency.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });
        editSignET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Signatures");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListSignDATA);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                            strType = mArrayListStamps.get(position);
                            editSignET.setText(mArrayListSignDATA.get(position));
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDialog.dismiss();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strInvoiceNum = editInvoiceET.getText().toString();
                strInvoiceDateFrom = editDateFromET.getText().toString();
                strInvoiceDateTo = editDateToET.getText().toString();
                strInvoiceCompany = editCompanyET.getText().toString();
                strInvoiceVessel = editVesselET.getText().toString();
                strInvoiceCurrency = editCurrencyET.getText().toString();
                strInvoiceStamp = editStampET.getText().toString();
                strInvoiceSign = editSignET.getText().toString();
                strInvoiceStatus = editStatusET.getText().toString();
                strInvoiceBank = editBankET.getText().toString();
                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    /*Execute Vesseles API*/
                    executeAdvanceSearchAPI(searchDialog);
                }

            }
        });
        searchDialog.show();

    }

    public void executeAdvanceSearchAPI(final Dialog mDialog) {
        modelArrayList.clear();
        resetRL1.setVisibility(View.VISIBLE);
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.advancedSearchInvoiceRequest(strInvoiceNum, "staff", strInvoiceDateFrom, strInvoiceDateTo,
                strInvoiceCompany, strInvoiceVessel, strInvoiceCurrency, strInvoiceStamp, strInvoiceSign, strInvoiceStatus, strInvoiceBank, JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();

//                String strUrl = JaoharConstants.ADVANCED_SEARCH_INVOICE + "?invoice_no=" + strInvoiceNum +
//                "&role=" + "staff" +
//                "&inv_date_from=" + strInvoiceDateFrom +
//                "&inv_date_to=" + strInvoiceDateTo +
//                "&search_company=" + strInvoiceCompany +
//                "&invoice_vessel=" + strInvoiceVessel +
//                "&invoice_currency=" + strInvoiceCurrency +
//                "&invoice_stamp=" + strInvoiceStamp +
//                "&invoice_sign=" + strInvoiceSign +
//                "&invoice_status=" + strInvoiceStatus +
//                "&invoice_bank=" + strInvoiceBank +
//                "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        strUrl = strUrl.replace(" ", "%20");
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                try {
                    JSONObject mJsonObject111 = new JSONObject(response.body().toString());
                    if (mJsonObject111.getString("status").equals("1")) {
                        isAdvanceSearch = true;
                        JSONArray mJsonArray = mJsonObject111.getJSONArray("all_searched_invoices");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJson = mJsonArray.getJSONObject(i);
                            InVoicesModel mModel = new InVoicesModel();
                            JSONObject mAllDataObj = mJson.getJSONObject("all_data");
                            if (!mAllDataObj.isNull("invoice_id"))
                                mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                            if (!mAllDataObj.getString("pdf").equals("")) {
                                mModel.setPdf(mAllDataObj.getString("pdf"));
                            }
                            if (!mAllDataObj.getString("pdf_name").equals("")) {
                                mModel.setPdf_name(mAllDataObj.getString("pdf_name"));
                            }
                            if (!mAllDataObj.isNull("invoice_no"))
                                mModel.setInvoice_number("JAORO" + mAllDataObj.getString("invoice_no"));
                            if (!mAllDataObj.isNull("invoice_date"))
                                mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
                            if (!mAllDataObj.isNull("term_days"))
                                mModel.setTerm_days(mAllDataObj.getString("term_days"));
                            if (!mAllDataObj.isNull("currency"))
                                mModel.setCurrency(mAllDataObj.getString("currency"));
                            if (!mAllDataObj.isNull("status"))
                                mModel.setStatus(mAllDataObj.getString("status"));
                            if (!mAllDataObj.isNull("reference"))
                                mModel.setRefrence1(mAllDataObj.getString("reference"));
                            if (!mAllDataObj.isNull("reference1"))
                                mModel.setRefrence2(mAllDataObj.getString("reference1"));
                            if (!mAllDataObj.isNull("reference2"))
                                mModel.setRefrence3(mAllDataObj.getString("reference2"));
                            if (!mAllDataObj.isNull("payment_id"))
                                mModel.setPayment_id(mAllDataObj.getString("payment_id"));
                            if (!mJson.getString("sign_data").equals("")) {
                                JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                                SignatureModel mSignModel = new SignatureModel();
                                if (!mSignDataObj.isNull("sign_id"))
                                    mSignModel.setId(mSignDataObj.getString("sign_id"));
                                if (!mSignDataObj.isNull("sign_name"))
                                    mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                                if (!mSignDataObj.isNull("sign_image"))
                                    mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                                mModel.setmSignatureModel(mSignModel);
                            }
                            if (!mJson.getString("stamp_data").equals("")) {
                                JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                                StampsModel mStampsModel = new StampsModel();
                                if (!mStampDataObj.isNull("stamp_id"))
                                    mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                                if (!mStampDataObj.isNull("stamp_name"))
                                    mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                                if (!mStampDataObj.isNull("stamp_image"))
                                    mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                                mModel.setmStampsModel(mStampsModel);
                            }
                            if (!mJson.getString("bank_data").equals("")) {
                                JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                                BankModel mBankModel = new BankModel();
                                if (!mBankDataObj.isNull("bank_id"))
                                    mBankModel.setId(mBankDataObj.getString("bank_id"));
                                if (!mBankDataObj.isNull("beneficiary"))
                                    mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                                if (!mBankDataObj.isNull("bank_name"))
                                    mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                                if (!mBankDataObj.isNull("address1"))
                                    mBankModel.setAddress1(mBankDataObj.getString("address1"));
                                if (!mBankDataObj.isNull("address2"))
                                    mBankModel.setAddress2(mBankDataObj.getString("address2"));
                                if (!mBankDataObj.isNull("iban_ron"))
                                    mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                                if (!mBankDataObj.isNull("iban_usd"))
                                    mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                                if (!mBankDataObj.isNull("iban_eur"))
                                    mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                                if (!mBankDataObj.isNull("swift"))
                                    mBankModel.setSwift(mBankDataObj.getString("swift"));
                                mModel.setmBankModel(mBankModel);
                            }
                            if (!mJson.getString("search_vessel_data").equals("")) {
                                JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                                VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                                if (!mSearchVesselObj.isNull("vessel_id"))
                                    mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                                if (!mSearchVesselObj.isNull("vessel_name"))
                                    mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                                if (!mSearchVesselObj.isNull("IMO_no"))
                                    mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                                if (!mSearchVesselObj.isNull("flag"))
                                    mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                                mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                            }
                            if (!mJson.getString("search_company_data").equals("")) {
                                JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                                CompaniesModel mCompaniesModel = new CompaniesModel();
                                if (!mSearchCompanyObj.isNull("id"))
                                    mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                                if (!mSearchCompanyObj.isNull("company_name"))
                                    mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                                if (!mSearchCompanyObj.isNull("Address1"))
                                    mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                                if (!mSearchCompanyObj.isNull("Address2"))
                                    mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                                if (!mSearchCompanyObj.isNull("Address3"))
                                    mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                                if (!mSearchCompanyObj.isNull("Address4"))
                                    mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                                if (!mSearchCompanyObj.isNull("Address5"))
                                    mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                                mModel.setmCompaniesModel(mCompaniesModel);
                            }
                            if (!mJson.getString("payment_data").equals("")) {
                                JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                                PaymentModel mPaymentModel = new PaymentModel();
                                if (!mPaymentObject.isNull("payment_id")) {
                                    mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                                }
                                if (!mPaymentObject.isNull("sub_total")) {
                                    mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                                }
                                if (!mPaymentObject.isNull("VAT")) {
                                    mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                                }
                                if (!mPaymentObject.isNull("vat_price")) {
                                    mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                                }
                                if (!mPaymentObject.isNull("total")) {
                                    mPaymentModel.setTotal(mPaymentObject.getString("total"));
                                }
                                if (!mPaymentObject.isNull("paid")) {
                                    mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                                }
                                if (!mPaymentObject.isNull("due")) {
                                    mPaymentModel.setPaid(mPaymentObject.getString("due"));
                                }
                                mModel.setmPaymentModel(mPaymentModel);
                            }
                            if (!mJson.getString("items_data").equals("")) {
                                JSONArray mItemArray = mJson.getJSONArray("items_data");
                                for (int k = 0; k < mItemArray.length(); k++) {
                                    JSONObject mItemObj = mItemArray.getJSONObject(k);
                                    InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                                    if (!mItemObj.isNull("item_id"))
                                        mItemModel.setItemID(mItemObj.getString("item_id"));
                                    if (!mItemObj.isNull("item_serial_no"))
                                        mItemModel.setItem(mItemObj.getString("item_serial_no"));
                                    if (!mItemObj.isNull("quantity"))
                                        mItemModel.setQuantity(mItemObj.getInt("quantity"));
                                    if (!mItemObj.isNull("price"))
                                        mItemModel.setUnitprice(mItemObj.getString("price"));
                                    if (!mItemObj.isNull("description"))
                                        mItemModel.setDescription(mItemObj.getString("description"));
                                    mInvoiceItemArrayList.add(mItemModel);
                                }
                                mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                            }

                            modelArrayList.add(mModel);


                        }

                        /*Set Adapter*/
                        setAdapter();


                    } else if (mJsonObject111.getString("status").equals("100")) {
                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject111.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*Paras Search Data*/
//            isAdvanceSearch = true;
//            parseSearchResponse(response, mDialog);
                mDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    public void executeStampSAPI() {
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAlStampsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
//                JaoharConstants.IS_STAMP_EDIT = false;
//                AlertDialogManager.hideProgressDialog();
                parseResponse1(response.body().toString());
                executeCompaniesAPI();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse1(String response) {

        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    StampsModel mModel = new StampsModel();
                    if (!mJson.isNull("stamp_id"))
                        mModel.setId(mJson.getString("stamp_id"));
                    if (!mJson.isNull("stamp_image"))
                        mModel.setStamp_image(mJson.getString("stamp_image"));
                    if (!mJson.isNull("stamp_name"))
                        mModel.setStamp_name(mJson.getString("stamp_name"));
                    mArrayListStamps.add(mJson.getString("stamp_name"));
                }


            }
//            else if (mJsonObject.getString("status").equals("100")) {
//                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeCompaniesAPI() {
        mArrayListCompanys.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAlCompaniesRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                parseResponse12(response.body().toString());
                executeBankDetailAPI();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse12(String response) {

        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    CompaniesModel mModel = new CompaniesModel();

                    if (!mJson.isNull("id")) {
                        mModel.setId(mJson.getString("id"));
                    }
                    if (!mJson.isNull("company_name")) {
                        mModel.setCompany_name(mJson.getString("company_name"));
                        mArrayListCompanys.add(mJson.getString("company_name"));
                    }
                    if (!mJson.isNull("Address1")) {
                        mModel.setAddress1(mJson.getString("Address1"));
                    }
                    if (!mJson.isNull("Address2")) {
                        mModel.setAddress2(mJson.getString("Address2"));
                    }
                    if (!mJson.isNull("Address3")) {
                        mModel.setAddress3(mJson.getString("Address3"));
                    }
                    if (!mJson.isNull("Address4")) {
                        mModel.setAddress4(mJson.getString("Address4"));
                    }
                    if (!mJson.isNull("Address5")) {
                        mModel.setAddress5(mJson.getString("Address5"));
                    }


                }


            }
//            else if (mJsonObject.getString("status").equals("100")) {
//                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeBankDetailAPI() {
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllBankDetailsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "******Response*****" + response);
                JaoharConstants.IS_BANK_DETAILS_EDIT = false;
                AlertDialogManager.hideProgressDialog();
                parseResponse13(response.body().toString());
                executeGetAllVesselsAPI();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse13(String response) {
        mArrayListBankDetails.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    BankModel mModel = new BankModel();
                    mModel.setId(mJson.getString("bank_id"));
                    mModel.setBenificiary(mJson.getString("beneficiary"));
                    mModel.setBankName(mJson.getString("bank_name"));
                    mArrayListBankDetails.add(mJson.getString("bank_name"));
                    mModel.setAddress1(mJson.getString("address1"));
                    mModel.setAddress2(mJson.getString("address2"));
                    mModel.setIbanRON(mJson.getString("iban_ron"));
                    mModel.setIbanUSD(mJson.getString("iban_usd"));
                    mModel.setIbanEUR(mJson.getString("iban_eur"));
                    mModel.setSwift(mJson.getString("swift"));


                }


            }
//            else if (mJsonObject.getString("status").equals("100")) {
//                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executeGetAllVesselsAPI() {
        vesselArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllInvoiceVesselsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    if (mJsonObject.getString("status").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJson = mJsonArray.getJSONObject(i);
                            VesselSearchInvoiceModel mModel = new VesselSearchInvoiceModel();
                            mModel.setVessel_id(mJson.getString("vessel_id"));
                            mModel.setVessel_name(mJson.getString("vessel_name"));
                            vesselArrayList.add(mJson.getString("vessel_name"));
                            mModel.setIMO_no(mJson.getString("IMO_no"));
                            mModel.setFlag(mJson.getString("flag"));

                        }

                        /*Set Adapter*/
                        executeCurrencyAPI();

                    }
//                    else if (mJsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else if (mJsonObject.getString("status").equals("0")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void executeCurrencyAPI() {
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllCurrenciesRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();

                JaoharConstants.IS_CURRENCY_EDIT = false;
                //AlertDialogManager.hideProgressDialog();
                parseResponse14(response.body().toString());
                executeSignDataAPI();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse14(String response) {
        mArrayListCurrency.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    CurrenciesModel mModel = new CurrenciesModel();

                    if (!mJson.isNull("id")) {
                        mModel.setId(mJson.getString("id"));
                    }
                    if (!mJson.isNull("currency_name")) {
                        mModel.setCurrency_name(mJson.getString("currency_name"));
                        mArrayListCurrency.add(mJson.getString("currency_name"));
                    }
                    if (!mJson.isNull("alias_name")) {
                        mModel.setAlias_name(mJson.getString("alias_name"));
                    }

                }


            }
//            else if (mJsonObject.getString("status").equals("100")) {
//                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeSignDataAPI() {
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllSignsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******Response*****" + response);
                JaoharConstants.IS_SIGNATURE_EDIT = false;
                AlertDialogManager.hideProgressDialog();
                parseResponse15(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse15(String response) {
        mArrayListSignDATA.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    SignatureModel mModel = new SignatureModel();
                    if (!mJson.isNull("sign_id"))
                        mModel.setId(mJson.getString("sign_id"));
                    if (!mJson.isNull("sign_name"))
                        mModel.setSignature_name(mJson.getString("sign_name"));
                    mArrayListSignDATA.add(mJson.getString("sign_name"));
                    if (!mJson.isNull("sign_image"))
                        mModel.setSignature_image(mJson.getString("sign_image"));


                }


            }
//            else if (mJsonObject.getString("status").equals("100")) {
//                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
