package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.VesselesModel;

public class DocumentWebViewActivity extends BaseActivity {
    Activity mActivity = DocumentWebViewActivity.this;
    String TAG = DocumentWebViewActivity.this.getClass().getSimpleName();
    //Widgets
    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;
    WebView mWebView;
    ProgressBar mProgressBar;
    VesselesModel mVesselesModel;
    String strUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_web_view);
        if (getIntent() != null) {
            strUrl = getIntent().getStringExtra("Url");
            Log.e(TAG,"****URL****"+strUrl);
        }
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtCenter.setText("View Document");
        mWebView = (WebView) findViewById(R.id.mWebView);
        mProgressBar = (ProgressBar) findViewById(R.id.mProgressBar);

       /*mWebView.setWebViewClient(new WebViewClient() {
           // This method will be triggered when the Page Started Loading
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                mProgressBar.setVisibility(View.VISIBLE);
                super.onPageStarted(view, url, favicon);
            }
            // This method will be triggered when the Page loading is completed

            public void onPageFinished(WebView view, String url) {
                mProgressBar.setVisibility(View.GONE);
                super.onPageFinished(view, url);
            }
            // This method will be triggered when error page appear
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                mProgressBar.setVisibility(View.GONE);
                // You can redirect to your own page instead getting the default
                // error page
                Toast.makeText(mActivity,
                        "The Requested Page Does Not Exist", Toast.LENGTH_LONG).show();
                mWebView.loadUrl(strUrl);
                super.onReceivedError(view, errorCode, description, failingUrl);
            }
        });
        mWebView.loadUrl(strUrl);//http://arunimmanuel.blogspot.in
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setJavaScriptEnabled(true);*/

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(strUrl);
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }
}
