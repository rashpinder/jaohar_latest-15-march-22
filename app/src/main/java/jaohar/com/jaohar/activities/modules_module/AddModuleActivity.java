package jaohar.com.jaohar.activities.modules_module;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AddModuleActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = AddModuleActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = AddModuleActivity.this.getClass().getSimpleName();

    /*
     * Some Variable and data Types
     * */
    String mCurrentPhotoPath, mStoragePath = "", strBase64 = "";
    Bitmap thumb = null;

    /**
     * Widgets
     */
    LinearLayout llLeftLL;
    TextView txtCenter;
    ImageView imgImageIV, imgBack;
    Button btnSendMAILB;
    EditText editSubjectET;


    // Permissions Value
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    // Permissions
    private String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String CAMERA = Manifest.permission.CAMERA;
    String strType = "", strForumType = "";


    /*
     * @OnCreate
     * @Default Activity method
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_add_module);
    }

    /**
     * Set Widget IDS
     **/
    @Override
    protected void setViewsIDs() {
        imgBack = findViewById(R.id.imgBack);
        imgImageIV = findViewById(R.id.imgImageIV);
        llLeftLL = findViewById(R.id.llLeftLL);
        editSubjectET = findViewById(R.id.editSubjectET);
        btnSendMAILB = findViewById(R.id.btnSendMAILB);
        txtCenter = findViewById(R.id.txtCenter);

        /* set top bar */
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        txtCenter.setText(getResources().getString(R.string.add_module));
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgImageIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mToGrantPermission();
            }
        });

        btnSendMAILB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editSubjectET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_module_name));
                } else {
                    /**
                     *Implement API to Submit Data
                     **/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeADDAPI();
                    }
                }
            }
        });
    }

    /*******
     * PERMISSIONS
     * **********/
    public void mToGrantPermission() {
        if (mCheckPermission()) {
            openCameraGalleryDialog();
        } else {
            mRequestPermission();
        }
    }

    private boolean mCheckPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int read_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int write_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return camera == PackageManager.PERMISSION_GRANTED && read_external_storage == PackageManager.PERMISSION_GRANTED && write_external_storage == PackageManager.PERMISSION_GRANTED;
    }

    private void mRequestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, 100);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            //All Permissions Granted
//                            openCameraGalleryDialog();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            Toast.makeText(mActivity, getString(R.string.goto_settings), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            default:
                break;
        }
    }


    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);
        TextView text_camra = (TextView) dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = (TextView) dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_files.setVisibility(View.GONE);
        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });

        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });

        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openFiles();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        Log.e("error is occured", dialog.toString());
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 505);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }


    private void openInternalStorage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf|text/plain");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 606);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                String authorities = getApplicationContext().getPackageName() + "com.dharmaniapps.fileprovider";
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            }
        }
        startActivityForResult(takePictureIntent, CAMERA_REQUEST);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            Bitmap bitmap;
                            Uri uri = data.getData();
                            File finalFile = new File(getRealPathFromURI(uri));

                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 0;

                            bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                          For COnvert And rotate image
                            ExifInterface exifInterface = null;
                            try {
                                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                            Matrix matrix = new Matrix();
                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(270);
                                    break;
                                case ExifInterface.ORIENTATION_NORMAL:
                                default:
                            }
                            JaoharConstants.IS_camera_Click = false;
                            thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                            imgImageIV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            imgImageIV.setImageBitmap(thumb);
                            strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "*****No Picture Selected*****");
                        return;
                    }
                } finally {
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                try {
                    JaoharConstants.IS_camera_Click = true;
                    thumb = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 0;
                    imgImageIV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    imgImageIV.setImageBitmap(thumb);
                    strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    /**
     * Implemented API to Add Module data in Server @AddModule
     *
     * @params
     * @user_id
     * @name
     * @link
     * @image
     * @app_image
     **/
    private void executeADDAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.addModule(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""),editSubjectET.getText().toString(),strBase64);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLADDResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }


//    private void executeADDAPI() {
//        String strAPIUrl = "";
//        AlertDialogManager.showProgressDialog(mActivity);
//
//        strAPIUrl = JaoharConstants.AddModule;
//
//
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "==Response==" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "***Error**" + error.toString());
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//                params.put("name", editSubjectET.getText().toString());
//                params.put("app_image", strBase64);
//
//                return params;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }


}

