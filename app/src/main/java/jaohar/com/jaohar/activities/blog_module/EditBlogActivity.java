package jaohar.com.jaohar.activities.blog_module;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.beans.blog_module.Blog_CategoriesModel;
import jaohar.com.jaohar.beans.blog_module.Blog_List_Model;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.AddBlogModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class EditBlogActivity extends BaseActivity {
    /**
     * set Activity
     **/
    Activity mActivity = EditBlogActivity.this;

    /**
     * set Activity TAG
     **/
    String TAG = EditBlogActivity.this.getClass().getSimpleName();

    /**
     * Widgets AND Adapters,Strings, Array List,Boolean and Integers
     **/
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.uploadImgTV)
    TextView uploadImgTV;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.layoutTakeImageLL)
    LinearLayout layoutTakeImageLL;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgRight)
    ImageView imgRight;
    @BindView(R.id.addImageData)
    ImageView addImageData;
    @BindView(R.id.imgRightLL)
    RelativeLayout imgRightLL;
    @BindView(R.id.blog_TitleET)
    EditText blog_TitleET;
    @BindView(R.id.blog_TagsET)
    EditText blog_TagsET;
    @BindView(R.id.blog_ContentsET)
    EditText blog_ContentsET;
    @BindView(R.id.spinnerData)
    Spinner spinnerData;
    @BindView(R.id.btnAdd)
    Button btnAdd;
    ArrayList<Blog_CategoriesModel> modelArrayList = new ArrayList<>();
    Blog_List_Model mModel;

    //permissions
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private String cameraStr = Manifest.permission.CAMERA;
    private String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE, strBlogCategory, strBase64, strBlogId = "";

    /**
     * Persist URI image to crop URI if specific permissions are required
     */
    private Uri mCropImageUri;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        setContentView(R.layout.activity_edit_blog);
        ButterKnife.bind(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setViewIDs();
    }

    /*
     *SetUp IDs to Views
     **/

    protected void setViewIDs() {

        /* set tool bar */
        txtCenter.setText(getString(R.string.edit_blog));
        imgBack.setImageResource(R.drawable.back);

        /*Retrieve Data From Previous Activity*/
        if (getIntent().getSerializableExtra("model") != null) {
            mModel = (Blog_List_Model) getIntent().getSerializableExtra("model");
            strBlogId = mModel.getBlog_id();
            blog_TitleET.setText(mModel.getTitle());
            blog_TagsET.setText(mModel.getTag());
            blog_ContentsET.setText(mModel.getContent());
            strBlogCategory = mModel.getCategory();
            strBase64 = mModel.getImage();
            Log.e("Check",mModel.getImage());
            if (!mModel.getImage().equals("") && mModel.getImage().contains("http")) {
                uploadImgTV.setVisibility(View.GONE);
              //  Glide.with(mActivity).load(mModel.getImage()).into(addImageData);
                Glide.with(mActivity)
                        .load(mModel.getImage())
//  .transition(DrawableTransitionOptions.withCrossFade(400)) //Optional
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE)
                                .error(R.drawable.img_bg_forum))
                        .dontAnimate()
                        .into(addImageData);
                addImageData.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            }
        }
        gettingBlogDATA();
    }

    /*
     *SetUp Clicks to Views
     **/
    @OnClick({R.id.llLeftLL,R.id.addImageData,R.id.btnAdd,R.id.layoutTakeImageLL})
    public  void  onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.addImageData:
                performImagePick();
                break;
            case R.id.btnAdd:
                performAdd();
                break;
            case  R.id.layoutTakeImageLL:
                performImagePick();
                break;

        }
    }



    private void performImagePick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private void performAdd() {
        if (blog_TitleET.getText().toString().trim().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_blog_title));
        } else if (blog_ContentsET.getText().toString().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_blog_content));
        } else {
            // Execute API
            executeUpdateAPI();
        }
    }
    private Map<String, String> mParams() {
        Map<String, String> params = new HashMap<>();
        String strTag = "";
        if (strBlogCategory.equals("Select a blog category")) {
            strTag = "";
        } else {
            strTag = strBlogCategory;
        }
        params.put("blog_title", blog_TitleET.getText().toString());
        params.put("blog_tags", blog_TagsET.getText().toString());
        params.put("content", blog_ContentsET.getText().toString());
        params.put("blog_image", strBase64);
        params.put("blog_category", strTag);
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));

        Log.e("**PARAMS**",params.toString());
        return params;
    }

    /*
     * Execute API to Update Blogs
     * @param (raw)
     * @user_id
     * @blog_title
     * @blog_category
     * @blog_tags
     * @blog_image (base64)
     * @content
     * @blog_id
     * */
    private void executeUpdateAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editBlogs(mParams()).enqueue(new Callback<AddBlogModel>() {
            @Override
            public void onResponse(Call<AddBlogModel> call, retrofit2.Response<AddBlogModel> response) {
                AlertDialogManager.hideProgressDialog();
                AddBlogModel addBlogModel=response.body();
                Log.e(TAG, "******response*****" + response.body().toString());
                if (addBlogModel.getStatus()==1) {
                    showAlertDialog(mActivity, getString(R.string.app_name), "" + addBlogModel.getMessage());
                } else if (addBlogModel.getStatus()==100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + addBlogModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + addBlogModel.getMessage());
                }


            }

            @Override
            public void onFailure(Call<AddBlogModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                Log.e(TAG, "******error*****" +t.getMessage());
            }
        });

           }


    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick() {
        CropImage.startPickImageActivity(this);
    }


    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
//                .setGuidelines(CropImageView.Guidelines.OFF).setAspectRatio(1,1)
//                .setFixAspectRatio(true)

                .setMultiTouchEnabled(false)
                .start(this);
    }


    /*********
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/

    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }


    void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                            onSelectImageClick();
//                            startCropImageActivity(mCropImageUri);
//                            startActivity(new Intent(SplashSlidesActivity.this, SelectionActivity.class));
//                            finish();
//                            Toast.makeText(context,"on",Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            requestPermission();
//                            permissionAccepted = false;
                        }
                    }
                }
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Bitmap bitmap = null;
                try {

                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//                        String encodedImage = encodeImage(selectedImage);

                    //                    mImage.setImageURI(result.getUri());
                    addImageData.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    addImageData.setImageBitmap(selectedImage);
                    strBase64 = ImageUtils.getInstant().getBase64FromBitmap(selectedImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }


    /*
     * API to Get All Blog list Using API
     * */
    private void gettingBlogDATA() {

        AlertDialogManager.showProgressDialog(mActivity);


        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        Call<JsonObject> call1 = mApiInterface.getAllBlogCategories(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {

                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                try {

                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    if (strStatus.equals("1")) {
                        parseResponce(response.body().toString());
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mJsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
            }
        });
       }

    private void parseResponce(String response) {
        JSONObject mJSonObject = null;
        try {
            mJSonObject = new JSONObject(response);

            JSONArray mjsonArrayData = mJSonObject.getJSONArray("data");
            Blog_CategoriesModel mModel1 = new Blog_CategoriesModel();
            mModel1.setCategory("Select Blog Category");
            mModel1.setId("0");
            mModel1.setCreation_date("");
            modelArrayList.add(mModel1);
            for (int i = 0; i < mjsonArrayData.length(); i++) {
                JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                Blog_CategoriesModel mModel = new Blog_CategoriesModel();
                if (!mJsonDATA.getString("id").equals("")) {
                    mModel.setId(mJsonDATA.getString("id"));
                }
                if (!mJsonDATA.getString("category").equals("")) {
                    mModel.setCategory(mJsonDATA.getString("category"));
                }
                if (!mJsonDATA.getString("creation_date").equals("")) {
                    mModel.setCreation_date(mJsonDATA.getString("creation_date"));
                }
                modelArrayList.add(mModel);
            }

            /*
             * SetUp Adapter
             * */
            setAdapter();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setAdapter() {
        final Typeface font = Typeface.createFromAsset(mActivity.getAssets(), "Poppins-Regular.ttf");
        Log.e(TAG, "TEST====" + modelArrayList.size());
        ArrayAdapter<Blog_CategoriesModel> dataAdapter = new ArrayAdapter<Blog_CategoriesModel>(mActivity, android.R.layout.simple_spinner_item, modelArrayList) {
            @SuppressLint("InflateParams")
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }
                TextView itemTextView = v.findViewById(R.id.itemTextView);
                Blog_CategoriesModel mModel = modelArrayList.get(position);
                itemTextView.setText(mModel.getCategory());
                return v;
            }
        };
        spinnerData.setAdapter(dataAdapter);

        /*Company Spinner Click Listener*/
        spinnerData.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Blog_CategoriesModel mModel = modelArrayList.get(position);


                strBlogCategory = mModel.getCategory();

                ((TextView) view).setText(strBlogCategory);
                ((TextView) view).setTextSize(13);
                ((TextView) view).setTextColor(Color.BLACK); //Change selected text color
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (!mModel.getCategory().equals("")) {
            for (int i = 0; i < modelArrayList.size(); i++) {
                Blog_CategoriesModel mModel1 = modelArrayList.get(i);
                strBlogCategory = mModel.getCategory();
                if (mModel.getCategory().equals("")) {
                    spinnerData.setSelection(0);
                } else {
                    if (mModel.getCategory().equals(mModel1.getCategory())) {
                        spinnerData.setSelection(i);
                    }
                }


            }
        }
    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
