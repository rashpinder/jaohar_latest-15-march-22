package jaohar.com.jaohar.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.PreviewItemsAdapter;
import jaohar.com.jaohar.beans.AddTotalInvoiceDiscountModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.beans.PriviewItemModel;
import jaohar.com.jaohar.beans.PriviewModelItems;
import jaohar.com.jaohar.utils.Utilities;
import jaohar.com.jaohar.views.ZoomLayout;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class PreviewActivity extends BaseActivity {
    private static String FILE = "Jaohar/Invoice.pdf"; // add permission in your manifest...
    public final int REQUEST_PERMISSIONS = 1;
    Activity mActivity = PreviewActivity.this;
    String TAG = PreviewActivity.this.getClass().getSimpleName();
    LinearLayout rootLinearLayoutLL, llLeftLL, mainLL1,swiftLL,gbpLL;
    NestedScrollView mNestedScrollView;
    RelativeLayout imgRightLL;
    Bitmap mGeneratedPDFBitmap = null;
    String strPDFPath = "";
    String strInvoiceVesselName = "";
    String strCompanyName = "";
    String strInvoiceNumber = "";
    boolean boolean_permission;
    boolean boolean_save;
    ZoomLayout mZoomLayout;
    ProgressBar progress1;
    PriviewItemModel priviewModel = new PriviewItemModel();
    ArrayList<PriviewModelItems> mArraypriviewModelPrievw = new ArrayList<PriviewModelItems>();
    ArrayList<PriviewModelItems> mArraypriviewModeNEWPRIVIEW = new ArrayList<PriviewModelItems>();
    //    ZoomableRelativeLayout mZoomLayoutRelativeLayout;
    /*PDF Widgets*/
    TextView txtCompanyName, txtCompanyAddress, txtCompanyAddress2, txtCompanyAddress3, txtCompanyAddress4, txtCompanyAddress5, txtInVoiceNumberValueTV, txtDateValueTV, txtDueValueTV, txtRefrenceValue,
            txtCurrencyValueTV, txtSubTotalValueTV, txtVatValueTV, txtVATPercentTV, txtTotalValueTV, txtPaidValueTV, txtBalanceDueValueTV,
            txtBenificiaryValueTV, txtBankNameTV,txtCurrenCyGBPTV, txtAddress1TV, txtCurrencyRONTV, txtCurrencyUSDTV, txtCurrencyEURTV, txtSwiftCodeTV, txtRefrence1, txtRefrence2, txtRefrence3, txtAddress2TV;
    LinearLayout usdLL, eurLL, ronLL, mainLL, mailLl;
    ImageView imgTopStatusIV, imgSigneIV;
    ImageView imgStampIV;
    RecyclerView rvItemsRowsRV;
    /*Invoice Model*/
    InVoicesModel mInVoicesModel;
    /*Adapter*/
    PreviewItemsAdapter mPreviewItemsAdapter;
    //Invoice Items ArrayList
    ArrayList<InvoiceAddItemModel> mInvoiceItemArrayList = new ArrayList<InvoiceAddItemModel>();
    //PDF File Path
    String mStoragePath = "";
    boolean Is_Edit = false;
    ArrayList<AddTotalInvoiceDiscountModel> mArrayTotalDiscount = new ArrayList<AddTotalInvoiceDiscountModel>();
    private String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_preview);

        setStatusBar();
        if (checkPermission()) {
            boolean_permission = true;
        } else {
            requestPermission();
        }
        setViewss();
    }

    protected void setViewss() {
        mNestedScrollView = (NestedScrollView) findViewById(R.id.mNestedScrollView);
        rootLinearLayoutLL = (LinearLayout) findViewById(R.id.rootLinearLayoutLL);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        mainLL1 = (LinearLayout) findViewById(R.id.mainLL1);
        swiftLL = (LinearLayout) findViewById(R.id.swiftLL);
        gbpLL = (LinearLayout) findViewById(R.id.gbpLL);


        mainLL = (LinearLayout) findViewById(R.id.mainLL);
//        mailLl = (LinearLayout) findViewById(R.id.mailLl);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        txtCompanyName = (TextView) findViewById(R.id.txtCompanyName);
        txtCompanyAddress = (TextView) findViewById(R.id.txtCompanyAddress);
        txtCompanyAddress2 = (TextView) findViewById(R.id.txtCompanyAddress2);
        txtCompanyAddress3 = (TextView) findViewById(R.id.txtCompanyAddress3);
        txtCompanyAddress4 = (TextView) findViewById(R.id.txtCompanyAddress4);
        txtCompanyAddress5 = (TextView) findViewById(R.id.txtCompanyAddress5);
        txtInVoiceNumberValueTV = (TextView) findViewById(R.id.txtInVoiceNumberValueTV);
        txtDateValueTV = (TextView) findViewById(R.id.txtDateValueTV);
        txtDueValueTV = (TextView) findViewById(R.id.txtDueValueTV);
        txtRefrenceValue = (TextView) findViewById(R.id.txtRefrenceValue);
        txtCurrencyValueTV = (TextView) findViewById(R.id.txtCurrencyValueTV);
        txtBenificiaryValueTV = (TextView) findViewById(R.id.txtBenificiaryValueTV);
        txtBankNameTV = (TextView) findViewById(R.id.txtBankNameTV);
        txtAddress1TV = (TextView) findViewById(R.id.txtAddress1TV);
        txtAddress2TV = (TextView) findViewById(R.id.txtAddress2TV);
        txtCurrenCyGBPTV = (TextView) findViewById(R.id.txtCurrenCyGBPTV);

        txtCurrencyRONTV = (TextView) findViewById(R.id.txtCurrencyRONTV);
        txtCurrencyUSDTV = (TextView) findViewById(R.id.txtCurrencyUSDTV);
        txtCurrencyEURTV = (TextView) findViewById(R.id.txtCurrencyEURTV);
        txtSwiftCodeTV = (TextView) findViewById(R.id.txtSwiftCodeTV);
        txtSubTotalValueTV = (TextView) findViewById(R.id.txtSubTotalValueTV);
        txtVatValueTV = (TextView) findViewById(R.id.txtVatValueTV);
        txtVATPercentTV = (TextView) findViewById(R.id.txtVATPercentTV);
        txtTotalValueTV = (TextView) findViewById(R.id.txtTotalValueTV);
        txtPaidValueTV = (TextView) findViewById(R.id.txtPaidValueTV);
        txtBalanceDueValueTV = (TextView) findViewById(R.id.txtBalanceDueValueTV);
        imgTopStatusIV = (ImageView) findViewById(R.id.imgTopStatusIV);
        imgStampIV = (ImageView) findViewById(R.id.imgStampIV);
        imgSigneIV = (ImageView) findViewById(R.id.imgSigneIV);

        mZoomLayout = new ZoomLayout(mActivity);
        usdLL = (LinearLayout) findViewById(R.id.usdLL);
        eurLL = (LinearLayout) findViewById(R.id.eurLL);
        ronLL = (LinearLayout) findViewById(R.id.ronLL);

        txtRefrence1 = (TextView) findViewById(R.id.txtRefrence1);
        txtRefrence2 = (TextView) findViewById(R.id.txtRefrence2);
        txtRefrence3 = (TextView) findViewById(R.id.txtRefrence3);

//      rvItemsRowsRV = (RecyclerView) findViewById(R.id.rvItemsRowsRV);
        progress1 = (ProgressBar) findViewById(R.id.progress1);
//      rvItemsRowsRV.setVisibility(View.GONE);

        if (getIntent() != null) {
            mInVoicesModel = (InVoicesModel) getIntent().getSerializableExtra("Model");
            if ((ArrayList<PriviewModelItems>) getIntent().getSerializableExtra("mPriviewModelArray") != null) {
                mArraypriviewModelPrievw = (ArrayList<PriviewModelItems>) getIntent().getSerializableExtra("mPriviewModelArray");
            }

            if (getIntent().getExtras().getBoolean("IsEdit") == true) {
                Is_Edit = getIntent().getExtras().getBoolean("IsEdit");
            }

            setDataOnWidgets(mInVoicesModel);

            System.out.println("ITEAM===============SIZE" + mInVoicesModel.getmItemModelArrayList().size());

        }

    }

    private void setDataOnWidgets(InVoicesModel mInVoicesModel) {
        try {
            if (mInVoicesModel.getmCompaniesModel() != null) {
                txtCompanyName.setText(mInVoicesModel.getmCompaniesModel().getCompany_name());
                strCompanyName = "-" + mInVoicesModel.getmCompaniesModel().getCompany_name();
                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0)
                    txtCompanyAddress.setText(mInVoicesModel.getmCompaniesModel().getAddress1());
                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0)
                    txtCompanyAddress2.setText(mInVoicesModel.getmCompaniesModel().getAddress2());
                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress3().length() > 0)
                    txtCompanyAddress3.setText(mInVoicesModel.getmCompaniesModel().getAddress3());
                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress3().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress4().length() > 0)
                    txtCompanyAddress4.setText(mInVoicesModel.getmCompaniesModel().getAddress4());
                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress3().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress4().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress5().length() > 0)
                    txtCompanyAddress5.setText(mInVoicesModel.getmCompaniesModel().getAddress5());
            }

            if (mInVoicesModel.getInvoice_number().length() > 0)
                txtInVoiceNumberValueTV.setText("JAORO" + mInVoicesModel.getInvoice_number());
            strInvoiceNumber = "JAORO" + mInVoicesModel.getInvoice_number();
            if (mInVoicesModel.getInvoice_date().length() > 0)
                txtDateValueTV.setText(Utilities.gettingFormatTime(mInVoicesModel.getInvoice_date()));

            String strDueDate = Utilities.getNewDateFormatWhenAddDays(mInVoicesModel.getInvoice_date(), Integer.parseInt(mInVoicesModel.getTerm_days()));
            txtDueValueTV.setText(Utilities.gettingFormatTime(strDueDate));
            if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
                String strFlag = mInVoicesModel.getmVesselSearchInvoiceModel().getFlag();
                txtRefrenceValue.setText("M/V " + mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_name() + ", IMO " + mInVoicesModel.getmVesselSearchInvoiceModel().getIMO_no() + ", Flag " + strFlag);
                strInvoiceVesselName = "-" + mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_name();
            }

            txtRefrence1.setText(mInVoicesModel.getRefrence1());
            txtRefrence2.setText(mInVoicesModel.getRefrence2());
            txtRefrence3.setText(mInVoicesModel.getRefrence3());
            if (mInVoicesModel.getCurrency().equals("USD")) {
                txtCurrencyValueTV.setText(mInVoicesModel.getCurrency());
                txtCurrencyValueTV.setTextColor(getResources().getColor(R.color.colorAccentRed));
            } else {
                txtCurrencyValueTV.setText(mInVoicesModel.getCurrency());
                txtCurrencyValueTV.setTextColor(getResources().getColor(R.color.colorAccentRed));
            }

            if (mInVoicesModel.getmBankModel() != null) {
                txtBenificiaryValueTV.setText(mInVoicesModel.getmBankModel().getBenificiary());
                txtBankNameTV.setText(mInVoicesModel.getmBankModel().getBankName());
                txtAddress1TV.setText(mInVoicesModel.getmBankModel().getAddress1());
                txtAddress2TV.setText(mInVoicesModel.getmBankModel().getAddress2());
                if (mInVoicesModel.getmBankModel().getIban_gbp() != null && mInVoicesModel.getmBankModel().getIban_gbp().length() > 0){
                    txtCurrenCyGBPTV.setText(mInVoicesModel.getmBankModel().getIban_gbp());
                }else {
                    gbpLL.setVisibility(View.GONE);
                }


            }

            if (mInVoicesModel.getmBankModel() != null) {
                if (mInVoicesModel.getmBankModel().getIbanUSD().length() > 0) {
                    usdLL.setVisibility(View.VISIBLE);
                    txtCurrencyUSDTV.setText(mInVoicesModel.getmBankModel().getIbanUSD());
                } else {
                    usdLL.setVisibility(View.GONE);
                }
            } else {
                usdLL.setVisibility(View.GONE);
            }
            if (mInVoicesModel.getmBankModel() != null) {
                if (mInVoicesModel.getmBankModel().getIbanEUR().length() > 0) {
                    eurLL.setVisibility(View.VISIBLE);
                    txtCurrencyEURTV.setText(mInVoicesModel.getmBankModel().getIbanEUR());
                } else {
                    eurLL.setVisibility(View.GONE);
                }
            } else {
                eurLL.setVisibility(View.GONE);
            }
            if (mInVoicesModel.getmBankModel() != null) {
                if (mInVoicesModel.getmBankModel().getIbanRON().length() > 0) {
                    ronLL.setVisibility(View.VISIBLE);
                    txtCurrencyRONTV.setText(mInVoicesModel.getmBankModel().getIbanRON());
                } else {
                    ronLL.setVisibility(View.GONE);
                }
                if (mInVoicesModel.getmBankModel().getSwift() != null && mInVoicesModel.getmBankModel().getSwift().length() > 0){
                    txtSwiftCodeTV.setText(mInVoicesModel.getmBankModel().getSwift());
                }else {
                    swiftLL.setVisibility(View.GONE);
                }

            } else {
                ronLL.setVisibility(View.GONE);
            }

            if (mInVoicesModel.getmItemModelArrayList().size() > 0)
                Log.e(TAG, "*****SIZE****" + mInVoicesModel.getmItemModelArrayList().size());

            mArraypriviewModeNEWPRIVIEW = Utilities.gettingFormatedItems(mArraypriviewModelPrievw);
            Log.e(TAG, "*****SIZEsfasf****" + mArraypriviewModeNEWPRIVIEW.size());
            if (mInVoicesModel.getmPaymentModel() != null) {
                if (mInVoicesModel.getmPaymentModel().getSubTotal().contains(".")) {
                    String strDWT2 = "00";
                    DecimalFormat df = new DecimalFormat("#.##");
                    Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getSubTotal());
                    df.setRoundingMode(RoundingMode.DOWN);
                    String strNumDWT = df.format(f1);
                    String[] separated = strNumDWT.split("\\.");
                    String strDWT = separated[0]; // this will contain "Fruit"
                    if (strNumDWT.contains(".")) {
                        strDWT2 = separated[1];
                    }
                    long numberGrain = Long.parseLong(strDWT);
                    String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                    txtSubTotalValueTV.setText(strNuumGrain + "." + strDWT2 + "  " + mInVoicesModel.getCurrency());
                } else {
                    long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getSubTotal());
                    String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                    txtSubTotalValueTV.setText(strNumGrossTonage + "  " + mInVoicesModel.getCurrency());
//                  GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                }
//                  txtSubTotalValueTV.setText("" + mInVoicesModel.getmPaymentModel().getSubTotal() + "  " + mInVoicesModel.getCurrency());
                if (!mInVoicesModel.getmPaymentModel().getVAT().equals("0")) {
                    if (!mInVoicesModel.getmPaymentModel().getVAT().equals("")) {
                        if (mInVoicesModel.getmPaymentModel().getVAT().contains(".")) {
                            String strDWT2 = "00";
                            DecimalFormat df = new DecimalFormat("#.##");
                            Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getVAT());
                            df.setRoundingMode(RoundingMode.DOWN);
                            String strNumDWT = df.format(f1);
                            String[] separated = strNumDWT.split("\\.");
                            String strDWT = separated[0]; // this will contain "Fruit"
                            if (strNumDWT.contains(".")) {
                                strDWT2 = separated[1];
                            }
                            long numberGrain = Long.parseLong(strDWT);
                            String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                            txtVATPercentTV.setText("V.A.T(" + strNuumGrain + "." + strDWT2 + "%):");

                        } else {
                            long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getVAT());
                            String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                            txtVATPercentTV.setText("V.A.T(" + strNumGrossTonage + "%):");
//                  GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
//                    txtVATPercentTV.setText("V.A.T(12345.12%):");
                        }
                    } else {
                        txtVATPercentTV.setText("V.A.T(" + "0" + "%):");
                    }

                }

                if (!mInVoicesModel.getmPaymentModel().getVATPrice().equals("")) {
                    if (mInVoicesModel.getmPaymentModel().getVATPrice().contains(".")) {
                        String strDWT2 = "00";
                        DecimalFormat df = new DecimalFormat("#.##");
                        Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getVATPrice());
                        df.setRoundingMode(RoundingMode.DOWN);
                        String strNumDWT = df.format(f1);
                        String[] separated = strNumDWT.split("\\.");
                        String strDWT = separated[0]; // this will contain "Fruit"
                        if (strNumDWT.contains(".")) {
                            strDWT2 = separated[1];
                        }
                        long numberGrain = Long.parseLong(strDWT);
                        String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                        txtVatValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mInVoicesModel.getCurrency());
                    } else {
                        long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getVATPrice());
                        String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                        txtVatValueTV.setText(" " + strNumGrossTonage + "  " + mInVoicesModel.getCurrency());
//                  GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                    }
                } else {
                    txtVatValueTV.setText(" 0 " + mInVoicesModel.getCurrency());
                }

                if (!mInVoicesModel.getmPaymentModel().getTotal().equals("")) {
                    if (mInVoicesModel.getmPaymentModel().getTotal().contains(".")) {
                        String strDWT2 = "00";
                        DecimalFormat df = new DecimalFormat("#.##");
                        Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getTotal());
                        df.setRoundingMode(RoundingMode.DOWN);
                        String strNumDWT = df.format(f1);
                        String[] separated = strNumDWT.split("\\.");
                        String strDWT = separated[0]; // this will contain "Fruit"
                        if (strNumDWT.contains(".")) {
                            strDWT2 = separated[1];
                        }
                        long numberGrain = Long.parseLong(strDWT);
                        String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                        txtTotalValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mInVoicesModel.getCurrency());
                    } else {
                        long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getTotal());
                        String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                        txtTotalValueTV.setText(" " + strNumGrossTonage + "  " + mInVoicesModel.getCurrency());
//                         GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                    }
                } else {
                    txtTotalValueTV.setText(" 0 " + mInVoicesModel.getCurrency());
                }

                if (!mInVoicesModel.getmPaymentModel().getPaid().equals("")) {
                    if (mInVoicesModel.getmPaymentModel().getPaid().contains(".")) {
                        String strDWT2 = "00";
                        DecimalFormat df = new DecimalFormat("#.##");
                        Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getPaid());
                        df.setRoundingMode(RoundingMode.DOWN);
                        String strNumDWT = df.format(f1);
                        String[] separated = strNumDWT.split("\\.");
                        String strDWT = separated[0]; // this will contain "Fruit"
                        if (strNumDWT.contains(".")) {
                            strDWT2 = separated[1];
                        }
                        long numberGrain = Long.parseLong(strDWT);
                        String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                        txtPaidValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mInVoicesModel.getCurrency());
                    } else {
                        long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getPaid());
                        String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                        txtPaidValueTV.setText(" " + strNumGrossTonage + "  " + mInVoicesModel.getCurrency());
                    }
                } else {
                    txtPaidValueTV.setText(" 0 " + mInVoicesModel.getCurrency());
                }

                if (!mInVoicesModel.getmPaymentModel().getBalanceDue().equals(""))
                    if (mInVoicesModel.getmPaymentModel().getBalanceDue().contains(".")) {
                        String strDWT2 = "00";
                        DecimalFormat df = new DecimalFormat("#.##");
                        Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getBalanceDue());
                        df.setRoundingMode(RoundingMode.DOWN);
                        String strNumDWT = df.format(f1);
                        String[] separated = strNumDWT.split("\\.");
                        String strDWT = separated[0]; // this will contain "Fruit"
                        if (strNumDWT.contains(".")) {
                            strDWT2 = separated[1];
                        }
                        long numberGrain = Long.parseLong(strDWT);
                        String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                        txtBalanceDueValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mInVoicesModel.getCurrency());
                    } else {
                        long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getBalanceDue());
                        String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                        txtBalanceDueValueTV.setText(" " + strNumGrossTonage + "  " + mInVoicesModel.getCurrency());
                    }
            } else {
                txtBalanceDueValueTV.setText(" 0 " + mInVoicesModel.getCurrency());
            }

            imgTopStatusIV.setImageResource(Utilities.getStatusImage(mInVoicesModel.getStatus()));

            if (mInVoicesModel.getmStampsModel() != null) {
                Log.e("STAMPIMG",""+mInVoicesModel.getmStampsModel().getStamp_image());
                if (mInVoicesModel.getmStampsModel().getStamp_image().length() > 0) {

                    progress1.setVisibility(View.GONE);
                    Picasso.get().load(mInVoicesModel.getmStampsModel().getStamp_image()).into(imgStampIV);
                }
            } else {
                progress1.setVisibility(View.GONE);
            }
            if (mInVoicesModel.getmSignatureModel() != null) {
                if (mInVoicesModel.getmSignatureModel().getSignature_image().length() > 0) {
                    Log.e(TAG,"Image++  "+mInVoicesModel.getmSignatureModel().getSignature_image());
                    Picasso.get().load(mInVoicesModel.getmSignatureModel().getSignature_image())

                            .into(imgSigneIV);
                }
            }


            if (mArraypriviewModeNEWPRIVIEW.size() != 0) {

                for (int i = 0; i < mArraypriviewModeNEWPRIVIEW.size(); i++) {

                    LayoutInflater layoutInflater =
                            (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(R.layout.item_pdf_item_layout, null);

                    TextView item_idTV = (TextView) addView.findViewById(R.id.item_idTV);
                    LinearLayout itemParentLL = (LinearLayout) addView.findViewById(R.id.itemParentLL);
                    TextView item_DescriptionTV = (TextView) addView.findViewById(R.id.item_DescriptionTV);
                    TextView item_QuantityTV = (TextView) addView.findViewById(R.id.item_QuantityTV);
                    TextView item_UnitPriceTV = (TextView) addView.findViewById(R.id.item_UnitPriceTV);
                    TextView item_AmountTV = (TextView) addView.findViewById(R.id.item_AmountTV);
                    PriviewModelItems tempValue = mArraypriviewModeNEWPRIVIEW.get(i);
                    int strCOUNT = i + 1;
                    if (!tempValue.getStrDescription().equals("")) {
                        item_idTV.setText("" + strCOUNT);
                    }
                    if (!tempValue.getStrDescription().equals("")) {
                        int strCharcter = tempValue.getStrDescription().length();
                        if (strCharcter <= 60) {
                            itemParentLL.setMinimumHeight(40);
                        }
                        item_DescriptionTV.setText(tempValue.getStrDescription());
                    } else {
                        itemParentLL.setMinimumHeight(40);
                    }
                    if (!tempValue.getStrUnitPrice().equals("")) {
                        if (tempValue.getStrUnitPrice().contains(".")) {
                            String strDWT2 = "00";
                            DecimalFormat df = new DecimalFormat("#.##");
                            if (tempValue.getStrUnitPrice().contains("+-")) {
                                String strVAlue =tempValue.getStrUnitPrice();
                                strVAlue.replace("+-","");
                                Double f1 = Double.parseDouble(strVAlue);
                                df.setRoundingMode(RoundingMode.DOWN);
                                String strNumDWT = df.format(f1);
                                String[] separated = strNumDWT.split("\\.");
                                String strDWT = separated[0]; // this will contain "Fruit"
                                if (strNumDWT.contains(".")) {
                                    strDWT2 = separated[1];
                                }
                                long numberGrain = Long.parseLong(strDWT);
                                String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                                item_UnitPriceTV.setText(strNuumGrain + "." + strDWT2);

                            } else {
                                Double f1 = Double.parseDouble(tempValue.getStrUnitPrice());
                                df.setRoundingMode(RoundingMode.DOWN);
                                String strNumDWT = df.format(f1);
                                String[] separated = strNumDWT.split("\\.");
                                String strDWT = separated[0]; // this will contain "Fruit"
                                if (strNumDWT.contains(".")) {
                                    strDWT2 = separated[1];
                                }
                                long numberGrain = Long.parseLong(strDWT);
                                String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                                item_UnitPriceTV.setText(strNuumGrain + "." + strDWT2);

                            }
                        } else {
                            long numberGrossTonage = Long.parseLong(tempValue.getStrUnitPrice());
                            String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                            item_UnitPriceTV.setText(strNumGrossTonage);
                        }
                    }
                    if (!tempValue.getStrAmount().equals("")) {
                        if (tempValue.getStrAmount().contains(".")) {
                            if (tempValue.getStrAmount().contains("+")) {
                                String strDWT2 = "00";
                                DecimalFormat df = new DecimalFormat("##.##");

                                Double f1 = Double.parseDouble(tempValue.getStrAmount());
                                df.setRoundingMode(RoundingMode.DOWN);
                                String strNumDWT = df.format(f1);
                                String[] separated = strNumDWT.split("\\.");
                                String strDWT = separated[0]; // this will contain "Fruit"
                                if (strNumDWT.contains(".")) {
                                    strDWT2 = separated[1];
                                }
                                long numberGrain = Long.parseLong(strDWT);
                                String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                                item_AmountTV.setText(strNuumGrain + "." + strDWT2);
                            } else if (tempValue.getStrAmount().contains("-")) {
                                String strDWT2 = "00";
                                DecimalFormat df = new DecimalFormat("#.##");
                                Double f1 = Double.parseDouble(tempValue.getStrAmount());
                                df.setRoundingMode(RoundingMode.DOWN);
                                String strNumDWT = df.format(f1);
                                String[] separated = strNumDWT.split("\\.");
                                String strDWT = separated[0]; // this will contain "Fruit"
                                if (strNumDWT.contains(".")) {
                                    strDWT2 = separated[1];
                                }
                                long numberGrain = Long.parseLong(strDWT);
                                String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                                if (strNuumGrain.contains("-")) {
                                    item_AmountTV.setText(strNuumGrain + "." + strDWT2);
                                } else {
                                    item_AmountTV.setText("-" + strNuumGrain + "." + strDWT2);
                                }
                            } else {
                                String strDWT2 = "00";
                                DecimalFormat df = new DecimalFormat("#.##");
                                Double f1 = Double.parseDouble(tempValue.getStrAmount());
                                df.setRoundingMode(RoundingMode.DOWN);
                                String strNumDWT = df.format(f1);
                                String[] separated = strNumDWT.split("\\.");
                                String strDWT = separated[0]; // this will contain "Fruit"
                                if (strNumDWT.contains(".")) {
                                    strDWT2 = separated[1];
                                }
                                long numberGrain = Long.parseLong(strDWT);
                                String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                                item_AmountTV.setText(strNuumGrain + "." + strDWT2);
                            }
                        } else {
                            long numberGrossTonage = Long.parseLong(tempValue.getStrAmount());
                            String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                            item_AmountTV.setText(strNumGrossTonage);
                        }
                    }
                    if (!tempValue.getStrQuantity().equals("")) {
                        item_QuantityTV.setText("" + tempValue.getStrQuantity());
                    }
                    if (i % 2 == 0) {
                        itemParentLL.setBackgroundResource(R.drawable.pdf_row_odd);
                    } else {
                        itemParentLL.setBackgroundResource(R.drawable.pdf_row_even);
                    }
                    mainLL1.addView(addView);
                    ViewTreeObserver observer = mainLL1.getViewTreeObserver();
                    observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            // TODO Auto-generated method stub
                            init();
                            mainLL1.getViewTreeObserver().removeGlobalOnLayoutListener(
                                    this);
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void init() {
        int a = mainLL1.getHeight();
        int b = mainLL1.getWidth();
        if (a < 490) {

        } else {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mainLL1.getLayoutParams();
//        Changes the height and width to the specified *pixels*
            params.height = 490;
            mainLL1.setLayoutParams(params);
        }
//        Toast.makeText(mActivity,""+a+" "+b,Toast.LENGTH_LONG).show();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (boolean_permission) {
//                    rootLinearLayoutLL.setDrawingCacheEnabled(true);
                    mGeneratedPDFBitmap = loadBitmapFromView(rootLinearLayoutLL, rootLinearLayoutLL.getWidth(), rootLinearLayoutLL.getHeight());
                    createPdf(mGeneratedPDFBitmap);
                }
            }
        });


    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public Bitmap loadBitmapFromView(View v, int width, int height) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
//      Paint paint = new Paint();
//      paint.setStyle(Paint.Style.FILL);
//      paint.setAntiAlias(true);
//      paint.setTypeface(Typeface.MONOSPACE);
//      c.drawText("0", 0, 16, paint);
        v.draw(c);
        return b;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void createPdf(Bitmap mBitmap) {
        PdfDocument document = new PdfDocument();
//        Document document11 = new Document(PageSize.A4, 0, 0, 0, 0);
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(mBitmap.getWidth(), mBitmap.getHeight(), 1).create();
//        PdfDocument.PageInfo pageInfo1 = new PdfDocument.PageInfo.Builder(mBitmap.getWidth(), mBitmap.getHeight(), 2).create();
        PdfDocument.Page page1 = document.startPage(pageInfo);
//      page1 = document.startPage(pageInfo1);
        Canvas canvas = page1.getCanvas();
        Paint paint = new Paint();
        canvas.drawPaint(paint);
        paint.setColor(Color.WHITE);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(mBitmap, 0, 0, null);
//        canvas.setDensity(72);
//      page1.
        document.finishPage(page1);
        // write the document content

//        try {
//            File mGeneratedFile = createImageFile();
//            Log.e(TAG, "******Generated File Path*******" + mGeneratedFile.getAbsolutePath());
//            document.writeTo(new FileOutputStream(mGeneratedFile));
//            strPDFPath = mGeneratedFile.getAbsolutePath();
//            Toast.makeText(this, "PDF PATH : " + strPDFPath, Toast.LENGTH_LONG).show();
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
//        }
//        // close the document
//        document.close();
//        sendEmail(strPDFPath);


        // write the document content
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            try {
                String imageFileName = "INV" + strInvoiceNumber + strInvoiceVesselName + strCompanyName;

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();
                ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);

                Uri savedFileUri = savePDFFile(mActivity, bs, "files/pdf", imageFileName, "jaohar");
//                Uri savedFileUri = savePDFFile(mActivity, bs, "pdf", imageFileName, "jaohar");
                Log.d("URI: ", savedFileUri.toString());
                strPDFPath = getRealPathFromURI(mActivity, savedFileUri);
                Log.d(TAG, "File Path: " + strPDFPath);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            File mGeneratedFile = null;
            try {
                mGeneratedFile = createImageFile();
                Log.e(TAG, "******Generated File Path*******" + mGeneratedFile.getAbsolutePath());
                document.writeTo(new FileOutputStream(mGeneratedFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
            strPDFPath = mGeneratedFile.getAbsolutePath();
        }

        // close the document
        document.close();
        sendEmail(strPDFPath);

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "INV" + strInvoiceNumber + strInvoiceVesselName + strCompanyName;
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File mFile = File.createTempFile(imageFileName, /* prefix */
                ".pdf", /* suffix */
                storageDir /* directory */
        );
        File from = new File(storageDir, mFile.getName());
        File to = new File(storageDir, imageFileName + ".pdf");
        from.renameTo(to);
        Log.i("Directory is", storageDir.toString());
        Log.i("Default path is", storageDir.toString());
        Log.i("From path is", from.toString());
        Log.i("To path is", to.toString());
        // Save a file: path for use with ACTION_VIEW intents
        String path = new File(imageFileName).getParent();
        mStoragePath = to.getAbsolutePath();
        return to;
    }

    private void sendEmail(String strFilePath) {
        Uri imageURI = null;
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", new File(strFilePath));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            imageURI = Uri.fromFile(new File(strFilePath));
        }

//        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
//                "mailto", "abc@gmail.com", null));
//        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "This is my subject text");
//        context.startActivity(Intent.createChooser(emailIntent, null));

        Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:")); // it's not ACTION_SEND
        intent.setType("png/image");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Invoice Details: ");
//      intent.putExtra(Intent.EXTRA_TEXT, "Ops (Jaohar UK Limited)\n"+"Invoice Details PDF:\n"+"Hello Please find the attached pdf:");
        intent.putExtra(Intent.EXTRA_TEXT, "Please find attached requested invoice");
        intent.putExtra(Intent.EXTRA_STREAM, imageURI);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent);
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(PreviewActivity.this, new String[]{
                READ_EXTERNAL_STORAGE,
                WRITE_EXTERNAL_STORAGE
        },REQUEST_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (grantResults.length > 0) {
                    boolean writeStorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean readStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (writeStorage && readStorage) {
                        boolean_permission = true;
                    } else {
                        Toast.makeText(PreviewActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                        requestPermission();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }


}



