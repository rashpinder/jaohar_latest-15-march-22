package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.ContactByIdModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class EditAddressBookActivity extends BaseActivity {
    private Activity mActivity = EditAddressBookActivity.this;
    private  String strIsADDClick="",strUSERID="",strContactID="";
    private TextView textHeadingTV;
    private RelativeLayout clossRL;
    private EditText nameET,company_nameET,email1ET,email2ET,email3ET,phone1ET,phone2ET,phone3ET;
    private Button btnCancel,btnSend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address_book);
        if(JaoharConstants.is_Staff_FragmentClick==true){
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        }else {
            strUSERID =JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
        }
    }
    @Override
    protected void setViewsIDs() {
        nameET= (EditText) findViewById(R.id.nameET);
        company_nameET= (EditText) findViewById(R.id.company_nameET);
        email1ET= (EditText) findViewById(R.id.email1ET);
        email2ET= (EditText) findViewById(R.id.email2ET);
        email3ET= (EditText) findViewById(R.id.email3ET);
        phone1ET= (EditText) findViewById(R.id.phone1ET);
        phone2ET= (EditText) findViewById(R.id.phone2ET);
        phone3ET= (EditText) findViewById(R.id.phone3ET);
        textHeadingTV=findViewById(R.id.textHeadingTV);
        clossRL=findViewById(R.id.clossRL);
        btnCancel=findViewById(R.id.btnCancel);
        btnSend=findViewById(R.id.btnSend);
        if (getIntent() != null) {
            if (getIntent().getStringExtra("isAddClick") != null) {
                strIsADDClick = getIntent().getStringExtra("isAddClick");
                if (strIsADDClick.equals("true")) {
                    textHeadingTV.setText("Contact Adding Form");
                } else if (strIsADDClick.equals("false")) {
                    textHeadingTV.setText("Edit Contact");
                }
                if(getIntent().getStringExtra("contactID")!=null){
                    strContactID = getIntent().getStringExtra("contactID");
                }
            }
        }
    }

    @Override
    protected void setClickListner() {
        clossRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nameET.getText().toString().trim().equals("")){
                    AlertDialogManager. showAlertDialog(mActivity,getResources().getString(R.string.app_name),getResources().getString(R.string.please_enter_name));
                }
                else if(company_nameET.getText().toString().trim().equals("")){
                    AlertDialogManager.showAlertDialog(mActivity,getResources().getString(R.string.app_name),getResources().getString(R.string.please_enter_company_name));
                }
                else if(email1ET.getText().toString().trim().equals("")){
                    AlertDialogManager.showAlertDialog(mActivity,getResources().getString(R.string.app_name),getResources().getString(R.string.please_enter_valid_email1));
                }
                else if(phone1ET.getText().toString().trim().equals("")){
                    AlertDialogManager.showAlertDialog(mActivity,getResources().getString(R.string.app_name),getResources().getString(R.string.please_enter_phone1));
                }
                else if (Utilities.isValidEmaillId(email1ET.getText().toString()) != true) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                }
                else {
                    if(!email2ET.getText().toString().trim().equals("")){
                        if (Utilities.isValidEmaillId(email2ET.getText().toString()) != true) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                        }else {
                            if(email1ET.getText().toString().trim().equals(email2ET.getText().toString().trim())){
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_emailone_emailtwo));
                            }else if(!email3ET.getText().toString().trim().equals("")){
                                if (Utilities.isValidEmaillId(email3ET.getText().toString()) != true) {
                                    AlertDialogManager. showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                                }else {
                                    if(email2ET.getText().toString().trim().equals(email3ET.getText().toString().trim())){
                                        AlertDialogManager. showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_emailtwo_emailthree));
                                    }
                                    else if(email1ET.getText().toString().trim().equals(email3ET.getText().toString().trim())){
                                        AlertDialogManager. showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_emailone_emailthree));
                                    } else if(!phone2ET.getText().toString().trim().equals("")){
                                        if(phone2ET.getText().toString().trim().equals(phone1ET.getText().toString().trim())){
                                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_phoneone_phonetwo));
                                        }
                                        else if(!phone3ET.getText().toString().trim().equals("")){
                                            if(phone2ET.getText().toString().trim().equals(phone3ET.getText().toString().trim())){
                                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_phonetwo_phonethree));
                                            }
                                            else if(phone1ET.getText().toString().trim().equals(phone3ET.getText().toString().trim())){
                                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_phoneone_phonethree));
                                            }else {
                                                if (Utilities.isNetworkAvailable(mActivity) == false) {
                                                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                                                } else {
                                                    executeAPIforSaving();
                                                }
//                                                Toast.makeText(mActivity,"test",Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        else {
                                            if (Utilities.isNetworkAvailable(mActivity) == false) {
                                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                                            } else {
                                                executeAPIforSaving();
                                            }
//                                            Toast.makeText(mActivity,"test",Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    else {
                                        if (Utilities.isNetworkAvailable(mActivity) == false) {
                                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                                        } else {
                                            executeAPIforSaving();
                                        }
//                                        Toast.makeText(mActivity,"test",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else {
                                if (Utilities.isNetworkAvailable(mActivity) == false) {
                                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                                } else {
                                    executeAPIforSaving();
                                }
//                                Toast.makeText(mActivity,"test",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else if(!email3ET.getText().toString().trim().equals("")){
                        if (Utilities.isValidEmaillId(email3ET.getText().toString()) != true) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                        }else {
                            if(email2ET.getText().toString().trim().equals(email3ET.getText().toString().trim())){
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_emailtwo_emailthree));
                            }
                            else if(email1ET.getText().toString().trim().equals(email3ET.getText().toString().trim())){
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_emailone_emailthree));
                            } else if(!phone2ET.getText().toString().trim().equals("")){
                                if(phone2ET.getText().toString().trim().equals(phone1ET.getText().toString().trim())){
                                    AlertDialogManager. showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_phoneone_phonetwo));
                                }
                                else if(!phone3ET.getText().toString().trim().equals("")){
                                    if(phone2ET.getText().toString().trim().equals(phone3ET.getText().toString().trim())){
                                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_phonetwo_phonethree));
                                    }
                                    else if(phone1ET.getText().toString().trim().equals(phone3ET.getText().toString().trim())){
                                        AlertDialogManager. showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_phoneone_phonethree));
                                    }else {
                                        if (Utilities.isNetworkAvailable(mActivity) == false) {
                                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                                        } else {
                                            executeAPIforSaving();
                                        }
//                                        Toast.makeText(mActivity,"test",Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else {
                                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                                    } else {
                                        executeAPIforSaving();
                                    }
//                                    Toast.makeText(mActivity,"test",Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                    else if(!phone2ET.getText().toString().trim().equals("")){
                        if(phone2ET.getText().toString().trim().equals(phone1ET.getText().toString().trim())){
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_phoneone_phonetwo));
                        }
                        else if(!phone3ET.getText().toString().trim().equals("")){
                            if(phone2ET.getText().toString().trim().equals(phone3ET.getText().toString().trim())){
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_phonetwo_phonethree));
                            }
                            else if(phone1ET.getText().toString().trim().equals(phone3ET.getText().toString().trim())){
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_phoneone_phonethree));
                            }else {
                                if (Utilities.isNetworkAvailable(mActivity) == false) {
                                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                                } else {
                                    executeAPIforSaving();
                                }
//                                Toast.makeText(mActivity,"test",Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            if (Utilities.isNetworkAvailable(mActivity) == false) {
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                            } else {
                                executeAPIforSaving();
                            }
//                            Toast.makeText(mActivity,"test",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else if(!phone3ET.getText().toString().trim().equals("")){
                        if(phone2ET.getText().toString().trim().equals(phone3ET.getText().toString().trim())){
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_phonetwo_phonethree));
                        }
                        else if(phone1ET.getText().toString().trim().equals(phone3ET.getText().toString().trim())){
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_diff_phoneone_phonethree));
                        }else {
                            if (Utilities.isNetworkAvailable(mActivity) == false) {
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                            } else {
                                executeAPIforSaving();
                            }
//                                Toast.makeText(mActivity,"test",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (Utilities.isNetworkAvailable(mActivity) == false) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            executeAPIforSaving();
                        }

//                        Toast.makeText(mActivity,"test",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    private void executeAPIforSaving() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.editAddressBookContactRequest(strUSERID,nameET.getText().toString().trim(),company_nameET.getText().toString().trim(),email1ET.getText().toString().trim(),email2ET.getText().toString().trim(),email3ET.getText().toString().trim(),phone1ET.getText().toString().trim(),phone2ET.getText().toString().trim(),phone3ET.getText().toString().trim(),strContactID);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e("TAG", "******error*****" + t.getMessage());

            }
        });
    }

//    private  void executeAPIforSaving(){
//        String strUrl = null;
////        https://root.jaohar.com/Staging/JaoharWebServicesNew/AddAddressBookContact.php
////        user_id , name , company_name , email1 , email2 , email3 , phone1 , phone2 and phone3
////        https://root.jaohar.com/Staging/JaoharWebServicesNew/EditAddressBookContact.php
////        params - user_id , contact_id , name , company_name , email1 , email2 , email3 , phone1 , phone2 and phone3
//
//            strUrl = JaoharConstants.EditAddressBookContact + "?user_id=" + strUSERID + "&name=" + nameET.getText().toString().trim() + "&company_name=" + company_nameET.getText().toString().trim() + "&email1=" + email1ET.getText().toString().trim() + "&email2=" + email2ET.getText().toString().trim() + "&email3=" + email3ET.getText().toString().trim() + "&phone1=" + phone1ET.getText().toString().trim()+ "&phone2=" + phone2ET.getText().toString().trim()+ "&phone3=" + phone3ET.getText().toString().trim()+ "&contact_id=" + strContactID ;// + "?page_no=" + page_no ;
//
//
//        strUrl=strUrl.replace(" ","%20");
//
//        Log.e("TAG", "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e("TAG", "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonData = new JSONObject(response);
//                    if (mJsonData.getString("status").equals("1")) {
//                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("TAG", "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            executeAPIforGetting();
        }
    }

    private void executeAPIforGetting() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ContactByIdModel> call1 = mApiInterface.getAddressBookByRequest(strUSERID, strContactID);
        call1.enqueue(new Callback<ContactByIdModel>() {
            @Override
            public void onResponse(Call<ContactByIdModel> call, retrofit2.Response<ContactByIdModel> response) {
                AlertDialogManager.hideProgressDialog();
                ContactByIdModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    nameET.setText(mModel.getData().getName());
                    company_nameET.setText(mModel.getData().getCompanyName());
                    email1ET.setText(mModel.getData().getEmail1());
                    email2ET.setText(mModel.getData().getEmail2());
                    email3ET.setText(mModel.getData().getEmail3());
                    phone1ET.setText(mModel.getData().getPhone1());
                    phone2ET.setText(mModel.getData().getPhone2());
                    phone3ET.setText(mModel.getData().getPhone3());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());

                }
            }

            @Override
            public void onFailure(Call<ContactByIdModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    private  void executeAPIforGetting(){
//        String strUrl = null;
////        https://root.jaohar.com/Staging/JaoharWebServicesNew/AddAddressBookContact.php
////        user_id , name , company_name , email1 , email2 , email3 , phone1 , phone2 and phone3
////        https://root.jaohar.com/Staging/JaoharWebServicesNew/EditAddressBookContact.php
////        params - user_id , contact_id , name , company_name , email1 , email2 , email3 , phone1 , phone2 and phone3
////        https://root.jaohar.com/Staging/JaoharWebServicesNew/GetAddressBookById.php
////        params - user_id , contact_id
//            strUrl = JaoharConstants.GetAddressBookById + "?user_id=" + strUSERID + "&contact_id=" + strContactID ;// + "?page_no=" + page_no ;
//
//
//        strUrl=strUrl.replace(" ","%20");
//
//        Log.e("TAG", "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e("TAG", "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonData = new JSONObject(response);
//                    if (mJsonData.getString("status").equals("1")) {
//
//                        JSONObject mjsonDATA = mJsonData.getJSONObject("data");
//                        if(!mjsonDATA.getString("name").equals("")){
//                            nameET.setText(mjsonDATA.getString("name"));
//                        }
//                        if(!mjsonDATA.getString("company_name").equals("")){
//                            company_nameET.setText(mjsonDATA.getString("company_name"));
//                        }
//                        if(!mjsonDATA.getString("email1").equals("")){
//                            email1ET.setText(mjsonDATA.getString("email1"));
//                        }
//                        if(!mjsonDATA.getString("email2").equals("")){
//                            email2ET.setText(mjsonDATA.getString("email2"));
//                        }
//                        if(!mjsonDATA.getString("email3").equals("")){
//                            email3ET.setText(mjsonDATA.getString("email3"));
//                        }
//                        if(!mjsonDATA.getString("phone1").equals("")){
//                            phone1ET.setText(mjsonDATA.getString("phone1"));
//                        } if(!mjsonDATA.getString("phone2").equals("")){
//                            phone2ET.setText(mjsonDATA.getString("phone2"));
//                        }
//                        if(!mjsonDATA.getString("phone3").equals("")){
//                            phone3ET.setText(mjsonDATA.getString("phone3"));
//                        }
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("TAG", "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public  void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
