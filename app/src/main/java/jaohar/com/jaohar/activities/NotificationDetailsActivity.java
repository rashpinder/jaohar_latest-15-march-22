package jaohar.com.jaohar.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.chat_module.ChatMessagesListActivity;

public class NotificationDetailsActivity extends AppCompatActivity {

    /**
     * set Activity
     **/
    Activity mActivity = NotificationDetailsActivity.this;

    /**
     * set Activity TAG
     **/
    String TAG = NotificationDetailsActivity.this.getClass().getSimpleName();

    /**
     * Widgets AND Adapters,Strings, Array List,Boolean and Integers
     **/
    TextView txtNotification;
    LinearLayout llLeftLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_notification_details);

        setIds();

        getIntentData();

        ClickListeners();
    }

    private void ClickListeners() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getIntentData() {
        if (getIntent() != null) {

            if (getIntent().getStringExtra("Notification") != null) {
                String str_Notification = getIntent().getStringExtra("Notification");

                txtNotification.setText(str_Notification);
            }
        }
    }

    private void setIds() {
        txtNotification = findViewById(R.id.txtNotification);
        llLeftLL = findViewById(R.id.llLeftLL);
    }
}
