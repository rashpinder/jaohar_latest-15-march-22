package jaohar.com.jaohar.activities.educational_blogs_module;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.utils.JaoharConstants;

public class OpenEducationBlogActivity extends AppCompatActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = OpenEducationBlogActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = OpenEducationBlogActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.closeLL)
    LinearLayout closeLL;
    @BindView(R.id.mWebView)
    public WebView mWebView;
    @BindView(R.id.circlePB)
    ProgressBar circlePB;

    String strTitle = "";
    String strLink = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_education_blog);
        ButterKnife.bind(this);
        getData();

    }



    /**
     * CLicks
     */
    @OnClick({R.id.closeLL})
    public  void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.closeLL:
                onBackPressed();
                break;
        }
    }


    private void getData() {
        strLink = getIntent().getStringExtra(JaoharConstants.BLOG_LINK);

        setLinks();
    }


    private void setLinks() {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                circlePB.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                circlePB.setVisibility(View.GONE);
            }
        });
        mWebView.loadUrl(strLink);
    }

}
