package jaohar.com.jaohar.activities.chat_module;

import static android.view.View.GONE;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.chat_module.AllChatParticipantsAdapter;
import jaohar.com.jaohar.adapters.chat_module.SelectedUsersAdapter;
import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;
import jaohar.com.jaohar.interfaces.chat_module.ChatUsersListInterface;
import jaohar.com.jaohar.interfaces.chat_module.ClickChatUsersInterface;
import jaohar.com.jaohar.interfaces.chat_module.RemoveParticipantInterface;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class AddParticipantActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = AddParticipantActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = AddParticipantActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.selectedUsersRV)
    RecyclerView selectedUsersRV;
    @BindView(R.id.allUsersRV)
    RecyclerView allUsersRV;
    @BindView(R.id.progressBottomPB)
    ProgressBar progressBottomPB;
    @BindView(R.id.txtNextTV)
    TextView txtNextTV;
    @BindView(R.id.txtCancelTV)
    TextView txtCancelTV;
    @BindView(R.id.editSearchET)
    EditText editSearchET;

    public static View DifView;

    int page_no = 1;
    String strLastPage = "TRUE";

    /*
     * Setting Up Array List
     * */
    ArrayList<ChatUsersModel> mLoadMore = new ArrayList<>();
    ArrayList<ChatUsersModel> modelArrayList = new ArrayList<>();

    ArrayList<ChatUsersModel> mSelectedArrayList = new ArrayList<>();

    /*
     * Setting Up Adapter
     * */
    AllChatParticipantsAdapter mChatUsersListAdapter;
    SelectedUsersAdapter mSelectedUsersAdapter;

    /**
     * Recycler View Pagination Adapter Interface
     **/
    ChatUsersListInterface mPaginationInterFace = new ChatUsersListInterface() {
        @Override
        public void mChatUsersListInterface(boolean isLastScroll) {
            if (isLastScroll == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            progressBottomPB.setVisibility(View.VISIBLE);
                            ++page_no;
                            executeAPI();
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                        }
                    }
                }, 1500);
            }
        }
    };

    ClickChatUsersInterface mClickChatUsersInterface = new ClickChatUsersInterface() {
        @Override
        public void mChatUsersListInterface(int position, ArrayList<ChatUsersModel> modelArrayList) {
            if (!mSelectedArrayList.contains(modelArrayList.get(position))) {
                ArrayList<String> ids = new ArrayList<>();

                if (mSelectedArrayList.size() > 0) {
                    for (int i = 0; i < mSelectedArrayList.size(); i++) {
                        ids.add(mSelectedArrayList.get(i).getId());
                    }
                    if (!ids.contains(modelArrayList.get(position).getId())) {
                        mSelectedArrayList.add(modelArrayList.get(position));
                        JaoharSingleton.getInstance().setmSelectedGroupUsersList(mSelectedArrayList);
                        setSelectedUsersAdapter(mSelectedArrayList, "1");
                    }
                } else {
                    mSelectedArrayList.add(modelArrayList.get(position));
                    JaoharSingleton.getInstance().setmSelectedGroupUsersList(mSelectedArrayList);
                    setSelectedUsersAdapter(mSelectedArrayList, "1");
                }
            }

            if (mSelectedArrayList.size() > 0) {
                DifView.setVisibility(View.VISIBLE);
            } else {
                DifView.setVisibility(GONE);
            }
        }
    };

    RemoveParticipantInterface removeParticipantInterface = new RemoveParticipantInterface() {
        @Override
        public void mRemoveParticipantInterface(int position, ArrayList<ChatUsersModel> modelArrayList) {

            if (modelArrayList.size() > 0) {
                DifView.setVisibility(View.VISIBLE);
            } else {
                DifView.setVisibility(GONE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        setContentView(R.layout.activity_add_participant);
        ButterKnife.bind(this);
        setIds();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (modelArrayList != null)
            modelArrayList.clear();

        if (editSearchET.getText().toString().length() == 0) {
            page_no = 1;
            executeAPI();
        } else {
            executeChatUsersSearch(editSearchET.getText().toString());
        }
    }


    @OnClick({R.id.txtCancelTV, R.id.txtNextTV})
    public  void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.txtCancelTV:
                onBackPressed();
                break;
            case R.id.txtNextTV:
                performNextClick();
                break;

        }
    }

    private void setIds() {
        DifView=findViewById(R.id.DifView);
        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event == null || event.getAction() != KeyEvent.ACTION_DOWN) {
                    //do something
                    if (modelArrayList != null)
                        modelArrayList.clear();

                    executeChatUsersSearch(editSearchET.getText().toString());
                }
                return false;
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() == 0) {
                    if (modelArrayList != null)
                        modelArrayList.clear();

                    executeAPI();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void performNextClick() {
        if (mSelectedArrayList.size() > 0) {
            startActivity(new Intent(mActivity, AddGroupActivity.class));
        } else {
            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_choose_participants));
        }
    }

    /* *
     * Execute API for getting Users list
     * @param
     * @user_id
     * */
    public void executeAPI() {
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_no == 1) {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllChatUsers(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),page_no);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {

                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
         }

    void parseResponse(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            String strStatus = String.valueOf(mJSonObject.getInt("status"));
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_users");
                if (mjsonArrayData != null) {
                    ArrayList<ChatUsersModel> mTempraryList = new ArrayList<>();
                    mTempraryList.clear();
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        ChatUsersModel mModel = new ChatUsersModel();
                        if (!mJsonDATA.getString("id").equals("")) {
                            mModel.setId(mJsonDATA.getString("id"));
                        }
                        if (!mJsonDATA.getString("email").equals("")) {
                            mModel.setEmail(mJsonDATA.getString("email"));
                        }
                        if (!mJsonDATA.getString("role").equals("")) {
                            mModel.setRole(mJsonDATA.getString("role"));
                        }
                        if (!mJsonDATA.getString("company_name").equals("")) {
                            mModel.setCompany_name(mJsonDATA.getString("company_name"));
                        }
                        if (!mJsonDATA.getString("first_name").equals("")) {
                            mModel.setFirst_name(mJsonDATA.getString("first_name"));
                        }
                        if (!mJsonDATA.getString("last_name").equals("")) {
                            mModel.setLast_name(mJsonDATA.getString("last_name"));
                        }
                        if (!mJsonDATA.getString("job").equals("")) {
                            mModel.setJob(mJsonDATA.getString("job"));
                        }
                        if (!mJsonDATA.getString("image").equals("")) {
                            mModel.setImage(mJsonDATA.getString("image"));
                        }
                        if (!mJsonDATA.getString("chat_status").equals("")) {
                            mModel.setChat_status(mJsonDATA.getString("chat_status"));
                        }
                        if (!mJsonDATA.getString("unread_chat").equals("")) {
                            mModel.setUnread_chat(mJsonDATA.getString("unread_chat"));
                        }
                        if (!mJsonDATA.getString("online_state").equals("")) {
                            mModel.setOnline_state(mJsonDATA.getString("online_state"));
                        }
                        if (!mJsonDATA.getString("message_time").equals("")) {
                            mModel.setMessage_time(mJsonDATA.getString("message_time"));
                        }
                        if (!mJsonDATA.getString("message").equals("")) {
                            mModel.setMessage(mJsonDATA.getString("message"));
                        }
                        if (!mJsonDATA.getString("room_id").equals("")) {
                            mModel.setRoom_id(mJsonDATA.getString("room_id"));
                        }

                        mTempraryList.add(mModel);
                    }
                    modelArrayList.addAll(mTempraryList);
                    if (page_no == 1) {
                        setAdapter(modelArrayList);
                    } else {
                        mChatUsersListAdapter.notifyDataSetChanged();
                    }
                }
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setAdapter(ArrayList<ChatUsersModel> modelArrayList) {
        allUsersRV.setNestedScrollingEnabled(false);
        allUsersRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mChatUsersListAdapter = new AllChatParticipantsAdapter(mActivity, modelArrayList, mPaginationInterFace, mClickChatUsersInterface);
        allUsersRV.setAdapter(mChatUsersListAdapter);
    }

    public void setSelectedUsersAdapter(ArrayList<ChatUsersModel> selectedArrayList, String type) {
        selectedUsersRV.setNestedScrollingEnabled(false);
        selectedUsersRV.setLayoutManager(new LinearLayoutManager(mActivity, RecyclerView.HORIZONTAL, false));
        mSelectedUsersAdapter = new SelectedUsersAdapter(mActivity, selectedArrayList, type, removeParticipantInterface);
        selectedUsersRV.setAdapter(mSelectedUsersAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    /* *
     * Execute API for Search chat users
     * */
    private void executeChatUsersSearch(String searchText) {
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_no == 1) {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }
        String strUrl = JaoharConstants.SearchChatUsers + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&input_search_value=" + searchText;
        Log.e(TAG, "***URL***" + strUrl);
        AlertDialogManager.showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.searchChatUsers(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),searchText);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });

    }
}