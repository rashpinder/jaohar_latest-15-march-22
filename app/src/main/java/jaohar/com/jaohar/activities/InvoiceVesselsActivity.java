package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.invoices_module.SearchInvoiceVesselsActivity;
import jaohar.com.jaohar.adapters.InvoiceVesselsAdapter;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.InvoiceVesselDeleteInterface;
import jaohar.com.jaohar.interfaces.InvoiceVesselEditInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class InvoiceVesselsActivity extends BaseActivity {

    Activity mActivity = InvoiceVesselsActivity.this;
    String TAG = InvoiceVesselsActivity.this.getClass().getSimpleName();
    /*Toolbar*/
    ImageView imgBack;
    LinearLayout llLeftLL;
    TextView txtCenter;
    ImageView imgSearch, imgAdd;
    RelativeLayout imgRightLL;

    EditText editSearchET;

    RecyclerView vesselsRV;
    ArrayList<VesselSearchInvoiceModel> vesselArrayList = new ArrayList<VesselSearchInvoiceModel>();
    InvoiceVesselsAdapter mInvoiceVesselsAdapter;
    ArrayList<VesselSearchInvoiceModel> filteredList = new ArrayList<VesselSearchInvoiceModel>();

    InvoiceVesselDeleteInterface mInvoiceVesselDeleteInterface = new InvoiceVesselDeleteInterface() {
        @Override
        public void invoiceVesselDelete(VesselSearchInvoiceModel mModel, int position) {
            deleteConfirmDialog(mModel, position);
        }
    };

    InvoiceVesselEditInterface mInvoiceVesselEditInterface = new InvoiceVesselEditInterface() {
        @Override
        public void invoiceVesselEdit(VesselSearchInvoiceModel mModel) {
            Intent mIntent = new Intent(mActivity, EditInvoiceVesselActivity.class);
            mIntent.putExtra("Model", mModel);
            mActivity.startActivity(mIntent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_vessels);

        setStatusBar();
    }

    @Override
    protected void setViewsIDs() {
        /*Toolbar*/
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        txtCenter.setText("Invoice Vessels");

        imgRightLL = findViewById(R.id.imgRightLL);
        imgAdd = findViewById(R.id.imgAdd);
        imgSearch = findViewById(R.id.imgSearch);

        editSearchET = findViewById(R.id.editSearchET);
        vesselsRV = findViewById(R.id.vesselsRV);
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AddInvoiceVesselActivity.class));
                overridePendingTransitionEnter();
            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, SearchInvoiceVesselsActivity.class);
                intent.putExtra("QuestionListExtra", vesselArrayList);
                startActivityForResult(intent, 666);
                overridePendingTransitionEnter();
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                s = s.toString().toLowerCase();
                filteredList.clear();
                for (int k = 0; k < vesselArrayList.size(); k++) {
                    final String text = vesselArrayList.get(k).getVessel_name().toLowerCase();
                    if (text.contains(s)) {
                        filteredList.add(vesselArrayList.get(k));
                    }
                }
                vesselsRV.setLayoutManager(new LinearLayoutManager(mActivity));
                mInvoiceVesselsAdapter = new InvoiceVesselsAdapter(mActivity, filteredList, mInvoiceVesselDeleteInterface, mInvoiceVesselEditInterface);
                vesselsRV.setAdapter(mInvoiceVesselsAdapter);
                mInvoiceVesselsAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            executeGetAllVesselsAPI();
        }
    }

    private void executeGetAllVesselsAPI() {
        vesselArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllInvoiceVesselsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    if (mJsonObject.getString("status").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            JSONObject mJson = mJsonArray.getJSONObject(i);
                            VesselSearchInvoiceModel mModel = new VesselSearchInvoiceModel();
                            mModel.setVessel_id(mJson.getString("vessel_id"));
                            mModel.setVessel_name(mJson.getString("vessel_name"));
                            mModel.setIMO_no(mJson.getString("IMO_no"));
                            mModel.setFlag(mJson.getString("flag"));

                            vesselArrayList.add(mModel);
                        }

                        /*Set Adapter*/
                        setAdapter();

                    } else if (mJsonObject.getString("status").equals("100")) {
                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
                    } else if (mJsonObject.getString("status").equals("0")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        Log.e(TAG, "VesselsAdapter: " + vesselArrayList.size());
        vesselsRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mInvoiceVesselsAdapter = new InvoiceVesselsAdapter(mActivity, vesselArrayList, mInvoiceVesselDeleteInterface, mInvoiceVesselEditInterface);
        vesselsRV.setAdapter(mInvoiceVesselsAdapter);
    }

    public void deleteConfirmDialog(final VesselSearchInvoiceModel mModel, int position) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_vessel));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mModel, position);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    public void executeDeleteAPI(VesselSearchInvoiceModel model, int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteInvoiceVesselsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), model.getVessel_id());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
//                    executeGetAllVesselsAPI();
                    vesselArrayList.remove(position);

                    if (mInvoiceVesselsAdapter != null)
                        mInvoiceVesselsAdapter.notifyDataSetChanged();

                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check that it is the Activity with an OK result
        if (requestCode == 666) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", data.getSerializableExtra("Model"));
                mActivity.setResult(666, returnIntent);
                mActivity.finish();
            }
        }
    }
}
