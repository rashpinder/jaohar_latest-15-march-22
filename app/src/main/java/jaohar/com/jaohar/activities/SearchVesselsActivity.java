package jaohar.com.jaohar.activities;

import static android.view.View.GONE;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.mail_list_all_activities.SendingMailToLIst_Vessels;
import jaohar.com.jaohar.activities.vessels_module.CopyVesselActivity;
import jaohar.com.jaohar.adapters.VesselRecentSearchAdapter;
import jaohar.com.jaohar.adapters.VesselsAdapter;
import jaohar.com.jaohar.beans.VessalClassModel;
import jaohar.com.jaohar.beans.VesselTypesModel;
import jaohar.com.jaohar.interfaces.DeleteVesselsInterface;
import jaohar.com.jaohar.interfaces.FavoriteVesselInterface;
import jaohar.com.jaohar.interfaces.MailSelectedVesselsInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.OpenPopUpVesselInterface;
import jaohar.com.jaohar.interfaces.SendEmailInterface;
import jaohar.com.jaohar.interfaces.SendMultiVesselDataInterface;
import jaohar.com.jaohar.interfaces.SendingMailToVesselInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.models.GetVesselClassData;
import jaohar.com.jaohar.models.GetVesselClassModel;
import jaohar.com.jaohar.models.GetVesselTypeData;
import jaohar.com.jaohar.models.GetVesselTypeModel;
import jaohar.com.jaohar.models.RecentSearchModel;
import jaohar.com.jaohar.models.SearchModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class SearchVesselsActivity extends AppCompatActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = SearchVesselsActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = SearchVesselsActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    EditText editSearchET;
    TextView cancelTV, ad_search_tv, mailSelectedTV, recentTV;
    RecyclerView recentRV;
    private RecyclerView vesselsRV;
    private LinearLayout mailSelectedLL;

    Dialog searchDialog;

    private String arrayStatus[], arrayGeared[];
    public String strCapacityFrom, strCapacityTo, strName, strLoaFrom, strLoaTo, strType, strGrainFrom,
            strGrainTo, strClass, strYearofBuildFrom, strYearofBuildTo, strBaleFrom, strBaleTo, strPlaceBuild,
            strRecordID, strGeared = "", strStatus, strTEUFrom, strTEUto, strPassangerFrom, strPassangerTo, strDraftsFrom,
            strDraftsTo, strLiquidFrom, strLiquidTo, strTextNEWS = "", SearchType = "", strOffMarket = "";
    VesselRecentSearchAdapter mVesselRecentSearchAdapter;

    private List<String> recentSearchArrayList = new ArrayList<>();
    //    private List<String> modelArrayList = new ArrayList<>();
    ArrayList<GetVesselTypeData> modelArrayList = new ArrayList<GetVesselTypeData>();

    //    private ArrayList<String> mArrayListClass = new ArrayList<>();
    private ArrayList<GetVesselClassData> mArrayListClass = new ArrayList<>();
    private ArrayList<String> mArrayListType = new ArrayList<>();
    private VessalClassModel vessalClassModel = new VessalClassModel();
    private VesselTypesModel vesselTypesModel = new VesselTypesModel();

    //    private ArrayList<VesselesModel> mArryList = new ArrayList<VesselesModel>();
    private ArrayList<AllVessel> mArryList = new ArrayList<AllVessel>();

    String User_id = "", strPhotoURL = "", strNormalTEXT = "", strSearchType = "";

    DeleteVesselsInterface mDeleteVesselsInterface = new DeleteVesselsInterface() {
        @Override
        public void mDeleteVessel(String mVesselID) {
            deleteConfirmDialog(mVesselID);
        }
    };

    SendEmailInterface mSendEmailInterface = new SendEmailInterface() {
        @Override
        public void sendEmail(AllVessel mVesselesModel) {
            /*Send Email*/
            Intent mIntent = new Intent(mActivity, SendingMailActivity.class);
            mIntent.putExtra("vessalName", "ID " + mVesselesModel.getRecordId() + "  " + "-" + "  " + mVesselesModel.getVesselName() + ", IMO No. " + mVesselesModel.getIMONumber());
            mIntent.putExtra("recordID", mVesselesModel.getRecordId());
            startActivity(mIntent);
        }
    };

    SendMultiVesselDataInterface mSendMultiVesselDataInterface = new SendMultiVesselDataInterface() {
        @Override
        public void sendMultipleVesselsData(AllVessel mVesselesModel, boolean mDelete) {
        }
    };

    paginationforVesselsInterface mPagination = new paginationforVesselsInterface() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
        }
    };

    OpenPopUpVesselInterface mOpenPoUpInterface = new OpenPopUpVesselInterface() {
        @Override
        public void mOpenPopUpVesselInterface(String strTextnews, String strPhotoURLs) {
        }
    };

    SendingMailToVesselInterface mVesselToMail = new SendingMailToVesselInterface() {
        @Override
        public void mSendingMailVessel(AllVessel mModel) {
            /*Send Email Using Mail to List*/
            JaoharConstants.IsAllVesselsClick = true;
            Intent mIntent = new Intent(mActivity, SendingMailToLIst_Vessels.class);
            mIntent.putExtra("vessalName", "ID " + mModel.getRecordId() + "  " + "-" + "  " + mModel.getVesselName() + ", IMO No. " + mModel.getIMONumber());
            mIntent.putExtra("recordID", mModel.getRecordId());
            startActivity(mIntent);
        }
    };

    FavoriteVesselInterface mFavoriteVesselInterface = new FavoriteVesselInterface() {
        @Override
        public void mFavoriteVesselInterface(int position, String status, ImageView imageView) {
            String vessel_id = mArryList.get(position).getRecordId();
            if (status.equals("1")) {
                executeFavoriteAPI(vessel_id, imageView, position);
            } else if (status.equals("0")) {
                executeUnFavoriteAPI(vessel_id, imageView, position);
            }
        }
    };

    MailSelectedVesselsInterface mMailSelectedVesselsInterface = new MailSelectedVesselsInterface() {
        @Override
        public void MailSelectedVessels(final int position, String Vessel_id, ArrayList<String> Items,
                                        AllVessel mVesselesModel, boolean b) {

            if (Items != null && Items.size() > 0) {
                mailSelectedLL.setVisibility(View.VISIBLE);

                mailSelectedTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        JaoharConstants.IsAllVesselsClick = true;
                        Intent mIntent = new Intent(mActivity, SendingMailToLIst_Vessels.class);
                        mIntent.putExtra("vessalName", "ID " +
                                mArryList.get(position).getRecordId() + "  " + "-" + "  " +
                                mArryList.get(position).getVesselName() + ", IMO No. " +
                                mArryList.get(position).getIMONumber());
                        mIntent.putExtra("recordID", mArryList.get(position).getRecordId());
                        startActivity(mIntent);
                    }
                });

            } else {
                mailSelectedLL.setVisibility(View.GONE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(Color.WHITE);
        setContentView(R.layout.activity_search_vessels);

        setStatusBar();

        arrayStatus = SearchVesselsActivity.this.getResources().getStringArray(R.array.status_array_user);
        arrayGeared = mActivity.getResources().getStringArray(R.array.geared_array);

        /* initialize view ids */
        setWidgetsIds();

        /* get data throught intent from previous activity */
        getIntentData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (SearchType.equals("normal")) {
            if (JaoharSingleton.getInstance().getSearchedVessel() != null && !JaoharSingleton.getInstance().getSearchedVessel().equals(""))
                if (strSearchType.equals(JaoharConstants.FavoriteVessels)) {
                    executeNormalFavoriteSearch(JaoharSingleton.getInstance().getSearchedVessel(), JaoharConstants.SearchFavoriteVessel);
                } else {
                    executeNormalSearch(JaoharSingleton.getInstance().getSearchedVessel(), JaoharConstants.ROLE_SEARCH_API);
                }
        } else if (SearchType.equals("advance")) {
            if (strSearchType.equals(JaoharConstants.FavoriteVessels)) {
                executeAdvanceFavoriteSearchAPI(searchDialog, JaoharConstants.AdvancedFavoriteVessel);
            } else {
                executeAdvanceSearchAPI(searchDialog, JaoharConstants.ADVANCED_SEARCH);
            }
        }
    }

    private void getIntentData() {
//        if (JaoharConstants.is_VessalsForSale == true) {
////            User_id = "0";
//            User_id = JaoharPreference.readString(SearchVesselsActivity.this, JaoharPreference.USER_ID, "");
//        } else if (JaoharConstants.is_Staff_FragmentClick == true) {
//            User_id = JaoharPreference.readString(SearchVesselsActivity.this, JaoharPreference.STAFF_ID, "");
//        } else if (JaoharConstants.is_VesselsFavorite == true) {
//            User_id = JaoharPreference.readString(SearchVesselsActivity.this, JaoharPreference.STAFF_ID, "");
//        } else {
//            User_id = JaoharPreference.readString(SearchVesselsActivity.this, JaoharPreference.STAFF_ID, "");
//        }

        if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
            if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                User_id = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
            } else {
                User_id = JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "");
            }
        } else {
            User_id = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        }

        if (getIntent().getStringExtra(JaoharConstants.SearchType) != null) {
            strSearchType = getIntent().getStringExtra(JaoharConstants.SearchType);
        }

        if (strSearchType != null)
            if (strSearchType.equals(JaoharConstants.FavoriteVessels)) {
                recentTV.setVisibility(GONE);
                recentRV.setVisibility(GONE);
            } else {
                recentTV.setVisibility(View.VISIBLE);
                recentRV.setVisibility(View.VISIBLE);
            }

        executeAPI();
    }

    private void setWidgetsIds() {
        mailSelectedTV = findViewById(R.id.mailSelectedTV);
        mailSelectedLL = findViewById(R.id.mailSelectedLL);
        editSearchET = findViewById(R.id.editSearchET);
        cancelTV = findViewById(R.id.cancelTV);
        recentRV = findViewById(R.id.recentRV);
        ad_search_tv = findViewById(R.id.ad_search_tv);
        vesselsRV = (RecyclerView) findViewById(R.id.vesselsRV);
        recentTV = findViewById(R.id.recentTV);

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event == null || event.getAction() != KeyEvent.ACTION_DOWN) {
                    //do something
//                    onBackPressed();
                    if (strSearchType.equals(JaoharConstants.FavoriteVessels)) {
                        executeNormalFavoriteSearch(editSearchET.getText().toString(), JaoharConstants.SearchFavoriteVessel);
                    } else {
                        executeNormalSearch(editSearchET.getText().toString(), JaoharConstants.ROLE_SEARCH_API);
                    }
                }
                return false;
            }
        });

        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ad_search_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gettingVessal_Type();
                gettingVessal_Class();
                advancedSearchView();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*
     * Set Up Status Bar
     * */
    public void setStatusBar() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }

    /* *
     * Execute API for getting Educational blogs list
     * @param
     * @user_id
     * */
    public void executeAPI() {
        modelArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<RecentSearchModel> call1 = mApiInterface.getRecentSearchRequest(User_id);
        call1.enqueue(new Callback<RecentSearchModel>() {
            @Override
            public void onResponse(Call<RecentSearchModel> call, retrofit2.Response<RecentSearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                RecentSearchModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    recentSearchArrayList = mModel.getData();
                    setAdapter();
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RecentSearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    /* *
     * Execute API for getting Educational blogs list
     * @param
     * @user_id
     * */
//    public void executeAPI() {
//        modelArrayList.clear();
//        String strUrl = JaoharConstants.GetRecentSearch + "?user_id=" + User_id;
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
////                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***URLResponce***" + response);
//                parseResponce(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
////                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
//
//    void parseResponce(String responce) {
//        try {
//            JSONObject mJSonObject = new JSONObject(responce);
//            String strStatus = mJSonObject.getString("status");
//            String strMessage = mJSonObject.getString("message");
//            if (strStatus.equals("1")) {
//                JSONArray mJSONArray = mJSonObject.getJSONArray("data");
//
//                for (int i = 0; i < mJSONArray.length(); i++) {
//                    modelArrayList.add(String.valueOf(mJSONArray.get(i)));
//                }
//
//                setAdapter();
//
//            } else {
////                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    public void setAdapter() {
        recentRV.setNestedScrollingEnabled(false);
        recentRV.setLayoutManager(new LinearLayoutManager(mActivity, RecyclerView.VERTICAL, false));
        mVesselRecentSearchAdapter = new VesselRecentSearchAdapter(mActivity, recentSearchArrayList, new OnClickInterface() {
            @Override
            public void mOnClickInterface(int position) {
                String searchText = recentSearchArrayList.get(position);

                if (strSearchType.equals(JaoharConstants.FavoriteVessels)) {
                    executeNormalFavoriteSearch(searchText, JaoharConstants.SearchFavoriteVessel);
                } else {
                    executeNormalSearch(searchText, JaoharConstants.ROLE_SEARCH_API);
                }
            }
        });
        recentRV.setAdapter(mVesselRecentSearchAdapter);
    }

//    private void gettingVessal_Type() {
//        if (mArrayListType != null)
//            mArrayListType.clear();
//        String strUrl = JaoharConstants.GET_ALL_VESSEL_TYPES;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//
//                Log.e(TAG, "******" + response);
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//
//                    if (jsonObject.getString("status").equals("1")) {
//                        if (!jsonObject.isNull("data")) {
//                            JSONArray vehiclesArray = jsonObject.getJSONArray("data");
//
//                            for (int i = 0; i < vehiclesArray.length(); i++) {
//                                JSONObject mJsonObject11 = vehiclesArray.getJSONObject(i);
//
//                                vesselTypesModel.setId(mJsonObject11.getString("id"));
//                                vesselTypesModel.setName(mJsonObject11.getString("name"));
//                                mArrayListType.add(mJsonObject11.getString("name"));
//
//                            }
//                        }
//
//                    } else {
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void gettingVessal_Type() {
        if (modelArrayList != null)
            modelArrayList.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselTypeModel> call1 = mApiInterface.getVesselTypeRequest();
        call1.enqueue(new Callback<GetVesselTypeModel>() {
                          @Override
                          public void onResponse(Call<GetVesselTypeModel> call, retrofit2.Response<GetVesselTypeModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetVesselTypeModel mModel = response.body();
                              assert mModel != null;
                              if (mModel.getStatus().equals("1")) {
                                  modelArrayList = mModel.getData();
                              } else {
                                  AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetVesselTypeModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    private void gettingVessal_Class() {
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselClassModel> call1 = mApiInterface.getVesselClassRequest();
        call1.enqueue(new Callback<GetVesselClassModel>() {
                          @Override
                          public void onResponse(Call<GetVesselClassModel> call, retrofit2.Response<GetVesselClassModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetVesselClassModel mModel = response.body();
                              assert mModel != null;
                              if (mModel.getStatus().equals("1")) {
                                  mArrayListClass = mModel.getData();
                              } else {
                                  AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetVesselClassModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    public void advancedSearchView() {
        if (mArryList != null) {
            mArryList.clear();
            setAdapter(mArryList);
        }

        strCapacityFrom = "";
        strCapacityTo = "";
        strLoaFrom = "";
        strLoaTo = "";
        strType = "";
        strYearofBuildFrom = "";
        strYearofBuildTo = "";
        strGrainFrom = "";
        strGrainTo = "";
        strBaleFrom = "";
        strName = "";
        strRecordID = "";
        strBaleTo = "";
        strStatus = "";
        strGeared = "";
        strOffMarket = "";
        strTEUFrom = "";
        strTEUto = "";
        strPassangerFrom = "";
        strPassangerTo = "";
        strDraftsFrom = "";
        strDraftsTo = "";
        strLiquidFrom = "";
        strLiquidTo = "";
        strClass = "";
        strPlaceBuild = "";

        searchDialog = new Dialog(mActivity);
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_advanced_search);
        searchDialog.setCanceledOnTouchOutside(false);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        // set the custom dialog components - text, image and button
        Button btn_search = (Button) searchDialog.findViewById(R.id.btn_search);
        ImageView imgCloseIV = (ImageView) searchDialog.findViewById(R.id.imgCloseIV);
        final EditText editFromCapacityET = (EditText) searchDialog.findViewById(R.id.editFromCapacityET);
        final EditText editToCapacityET = (EditText) searchDialog.findViewById(R.id.editToCapacityET);
        final EditText editFromLoaET = (EditText) searchDialog.findViewById(R.id.editFromLoaET);
        final EditText editToLoaET = (EditText) searchDialog.findViewById(R.id.editToLoaET);
//        final Spinner typeSpinner = (Spinner) searchDialog.findViewById(R.id.typeSpinner);
        final EditText editFromGrainET = (EditText) searchDialog.findViewById(R.id.editFromGrainET);
        final EditText editToGrainET = (EditText) searchDialog.findViewById(R.id.editToGrainET);
        final EditText editFromYearBuiltET = (EditText) searchDialog.findViewById(R.id.editFromYearBuiltET);
        final EditText editToYearBuiltET = (EditText) searchDialog.findViewById(R.id.editToYearBuiltET);
        final EditText editFromBaleET = (EditText) searchDialog.findViewById(R.id.editFromBaleET);
        final EditText editToBaleET = (EditText) searchDialog.findViewById(R.id.editToBaleET);
        final EditText editNameET = (EditText) searchDialog.findViewById(R.id.editNameET);
        final EditText editPlaceBuiltET = (EditText) searchDialog.findViewById(R.id.editPlaceBuiltET);
        final EditText editRecordIDET = (EditText) searchDialog.findViewById(R.id.editRecordIDET);
        final EditText editStatusET = (EditText) searchDialog.findViewById(R.id.editStatusET);
        final EditText editGearedET = (EditText) searchDialog.findViewById(R.id.editGearedET);
        final EditText editOffMarketET = searchDialog.findViewById(R.id.editOffMarketET);
        final EditText editFromTEUET = (EditText) searchDialog.findViewById(R.id.editFromTEUET);
        final EditText editToTEUET = (EditText) searchDialog.findViewById(R.id.editToTEUET);
        final EditText editFromPassengerET = (EditText) searchDialog.findViewById(R.id.editFromPassengerET);
        final EditText editToPassendgerET = (EditText) searchDialog.findViewById(R.id.editToPassendgerET);
        final EditText editFromDraftrET = (EditText) searchDialog.findViewById(R.id.editFromDraftrET);
        final EditText editToDraftET = (EditText) searchDialog.findViewById(R.id.editToDraftET);
        final EditText editFromLiquidET = (EditText) searchDialog.findViewById(R.id.editFromLiquidET);
        final EditText editToLiquidET = (EditText) searchDialog.findViewById(R.id.editToLiquidET);
        final EditText editTypeET = (EditText) searchDialog.findViewById(R.id.editTypeET);
        final EditText editClassET = (EditText) searchDialog.findViewById(R.id.editClassET);
        final EditText editIMONumET = (EditText) searchDialog.findViewById(R.id.editIMONumET);
        final TextView recordTV = (TextView) searchDialog.findViewById(R.id.recordTV);
//        editStatusET.setText("Available");
//        recordTV.setText("IMO No.");
//        editRecordIDET.setHint("IMO No.");
        editStatusET.setKeyListener(null);
        editStatusET.setCursorVisible(false);
        editGearedET.setKeyListener(null);
        editGearedET.setCursorVisible(false);
        editOffMarketET.setKeyListener(null);
        editOffMarketET.setCursorVisible(false);
        final TextView txtHeaderStatus = (TextView) searchDialog.findViewById(R.id.txtHeaderStatus);
        final LinearLayout layoutNameLL = (LinearLayout) searchDialog.findViewById(R.id.layoutNameLL);
        final LinearLayout layoutPlaceBuiltLL = (LinearLayout) searchDialog.findViewById(R.id.layoutPlaceBuiltLL);
        final LinearLayout layoutRecordIDLL = (LinearLayout) searchDialog.findViewById(R.id.layoutRecordIDLL);
        final LinearLayout layoutStatusLL = (LinearLayout) searchDialog.findViewById(R.id.layoutStatusLL);
        final LinearLayout layoutGearedLL = (LinearLayout) searchDialog.findViewById(R.id.layoutGearedLL);
        final LinearLayout layoutIMONumLL = (LinearLayout) searchDialog.findViewById(R.id.layoutIMONumLL);
        TextView txtCenter = searchDialog.findViewById(R.id.txtCenter);
        layoutNameLL.setVisibility(View.VISIBLE);
        layoutPlaceBuiltLL.setVisibility(View.VISIBLE);
//        layoutIMONumLL.setVisibility(View.VISIBLE);
        layoutRecordIDLL.setVisibility(View.VISIBLE);

        final Spinner spinnerGeared = (Spinner) searchDialog.findViewById(R.id.spinnerGeared);

        final Typeface font = Typeface.createFromAsset(mActivity.getAssets(), "Calibri-Medium.ttf");
        final List<String> list = new ArrayList<String>();
        list.add("Select Geared");
        list.add("Yes");
        list.add("No");
        ArrayAdapter<String> mSpAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);

                    TextView itemTextView = v.findViewById(R.id.itemTextView);
                    String strName = list.get(position);
                    itemTextView.setText(strName);
                    itemTextView.setTypeface(font);
                }
                return v;
            }
        };
        spinnerGeared.setAdapter(mSpAdapter);
        spinnerGeared.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strGeared = adapterView.getItemAtPosition(i).toString();
                ((TextView) view).setTextSize(14);
                ((TextView) view).setTypeface(font);
                ((TextView) view).setPadding(1, 0, 0, 0);
                if (strGeared.equals("Select Geared")) {
                    strGeared = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        editTypeET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessel Type");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    ArrayAdapter<GetVesselTypeData> adapter = new ArrayAdapter<GetVesselTypeData>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, modelArrayList);
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            strType = modelArrayList.get(position).getName();
                            editTypeET.setText(strType);
                            categoryDialog.dismiss();
                        }
                    });
                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });

        editClassET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    final Dialog categoryDialog = new Dialog(mActivity);
                    categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    categoryDialog.setContentView(R.layout.item_list_categories);
                    categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    // set the custom dialog components - text, image and button
                    TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
                    txtTitle.setText("Vessel Class");
                    ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
                    // Define a new Adapter
                    // First parameter - Context
                    // Second parameter - Layout for the row
                    // Third parameter - ID of the TextView to which the data is written
                    // Forth - the Array of data
                    ArrayAdapter<GetVesselClassData> adapter = new ArrayAdapter<GetVesselClassData>(mActivity,
                            android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListClass);

                    // Assign adapter to ListView
                    lstListView.setAdapter(adapter);

                    lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            strClass = mArrayListClass.get(position).getClassName();
                            editClassET.setText(strClass);
                            categoryDialog.dismiss();

                        }
                    });


                    categoryDialog.show();
                    return true;
                }
                return false;
            }
        });


        layoutStatusLL.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        layoutGearedLL.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Geared", arrayGeared, editGearedET);
                    return true;
                }
                return false;
            }
        });
        editOffMarketET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "OffMarket", arrayGeared, editOffMarketET);
                    return true;
                }
                return false;
            }
        });
        txtHeaderStatus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        editGearedET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Geared", arrayGeared, editGearedET);
                    return true;
                }
                return false;
            }
        });

        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchDialog.dismiss();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strCapacityFrom = editFromCapacityET.getText().toString();
                strCapacityTo = editToCapacityET.getText().toString();

                strName = editNameET.getText().toString();
//                if (strName.equals("NAME")) {
//                    strName = "";
//                } else {
//                    strName = editNameET.getText().toString();
//                }

                strLoaFrom = editFromLoaET.getText().toString();
                strLoaTo = editToLoaET.getText().toString();

                strGrainFrom = editFromGrainET.getText().toString();
                strGrainTo = editToGrainET.getText().toString();

                strYearofBuildFrom = editFromYearBuiltET.getText().toString();
                strYearofBuildTo = editToYearBuiltET.getText().toString();

                strBaleFrom = editFromBaleET.getText().toString();
                strBaleTo = editToBaleET.getText().toString();

                strPlaceBuild = editPlaceBuiltET.getText().toString();
//                if (strPlaceBuild.equals("PLACE")) {
//                    strPlaceBuild = "";
//                } else {
//                    strPlaceBuild = editPlaceBuiltET.getText().toString();
//                }

                strRecordID = editRecordIDET.getText().toString();
//                if (strRecordID.equals("RECORD ID")) {
//                    strRecordID = "";
//                } else {
//                    strRecordID = editRecordIDET.getText().toString();
//                }

                strStatus = editStatusET.getText().toString();
//                if (strStatus.equals("AVAILABLE")) {
//                    strStatus = "Available";
//                } else {
//                    strStatus = editStatusET.getText().toString();
//                }

                strGeared = editGearedET.getText().toString();
//                if (strGeared.equals("YES/NO")) {
//                    strGeared = "";
//                } else {
//                    strGeared = editGearedET.getText().toString();
//                }

                strOffMarket = editOffMarketET.getText().toString();

                strTEUFrom = editFromTEUET.getText().toString();
                strTEUto = editToTEUET.getText().toString();
                strDraftsTo = editToDraftET.getText().toString();
                strDraftsFrom = editFromDraftrET.getText().toString();
                strPassangerFrom = editFromPassengerET.getText().toString();
                strPassangerTo = editToPassendgerET.getText().toString();
                strLiquidTo = editToLiquidET.getText().toString();
                strLiquidFrom = editFromLiquidET.getText().toString();
                strLiquidTo = editToLiquidET.getText().toString();

                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    /*Execute Vesseles API*/
                    if (strSearchType.equals(JaoharConstants.FavoriteVessels)) {
                        executeAdvanceFavoriteSearchAPI(searchDialog, JaoharConstants.AdvancedFavoriteVessel);
                    } else {
                        executeAdvanceSearchAPI(searchDialog, JaoharConstants.ADVANCED_SEARCH);
                    }
                }
            }
        });

        txtCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDialog.dismiss();
            }
        });

        searchDialog.show();
    }

    public void executeAdvanceSearchAPI(final Dialog mDialog, String url) {
        SearchType = "advance";
        mArryList.clear();

        String strNameText = "";
        if (strName != null && !strName.equals("")) {
//            strNameText = strName.replace(" ", "%20");
//        } else {
            strNameText = strName;
        }
        String strTypeText = "";
        if (strType != null && !strType.equals("")) {
//            strTypeText = strType.replace(" ", "%20");
//        } else {
            strTypeText = strType;
        }
        String strPlaceBuildText = "";
        if (strPlaceBuild != null && !strPlaceBuild.equals("")) {
//            strPlaceBuildText = strPlaceBuild.replace(" ", "%20");
//        } else {
            strPlaceBuildText = strPlaceBuild;
        }
        String strStatusText = "";
        if (strStatus != null && !strStatus.equals("")) {
//            strStatusText = strStatus.replace(" ", "%20");
//        } else {
            strStatusText = strStatus;
        }
        String str_Class = "";
        if (strType == null || strType.equals("TYPE")) {
            strType = "";
        }
        if (strClass == null || strClass.equals("SELECT CLASS")) {
            strClass = "";
        } else {
            if (!strClass.equals("")) {
//                str_Class = strClass.replace(" ", "%20");
//            } else {
                str_Class = strClass;
            }
        }
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchModel> call1 = mApiInterface.advanceSearchVesselRequest(strCapacityFrom, strCapacityTo, strLoaFrom, strLoaTo,
                strType, strYearofBuildFrom, strYearofBuildTo, strGrainFrom, strGrainTo, strBaleFrom, strName, strRecordID, strBaleTo, strStatus,
                strGeared, "staff", strTEUFrom, strTEUto, strPassangerFrom, strPassangerTo, strDraftsFrom, strDraftsTo,
                strLiquidFrom, strLiquidTo, strClass, strPlaceBuild, User_id, strOffMarket);
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                SearchModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    mDialog.dismiss();
                    mArryList = mModel.getAllSearchedVessels();
                    /* Set Adapter */
                    setAdapter(mArryList);

                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void executeAdvanceFavoriteSearchAPI(final Dialog mDialog, String url) {
        SearchType = "advance";
        mArryList.clear();

        String strNameText = "";
        if (strName != null && !strName.equals("")) {
//            strNameText = strName.replace(" ", "%20");
//        } else {
            strNameText = strName;
        }
        String strTypeText = "";
        if (strType != null && !strType.equals("")) {
//            strTypeText = strType.replace(" ", "%20");
//        } else {
            strTypeText = strType;
        }
        String strPlaceBuildText = "";
        if (strPlaceBuild != null && !strPlaceBuild.equals("")) {
//            strPlaceBuildText = strPlaceBuild.replace(" ", "%20");
//        } else {
            strPlaceBuildText = strPlaceBuild;
        }
        String strStatusText = "";
        if (strStatus != null && !strStatus.equals("")) {
//            strStatusText = strStatus.replace(" ", "%20");
//        } else {
            strStatusText = strStatus;
        }
        String str_Class = "";
        if (strType == null || strType.equals("TYPE")) {
            strType = "";
        }
        if (strClass == null || strClass.equals("SELECT CLASS")) {
            strClass = "";
        } else {
            if (!strClass.equals("") ) {
//                str_Class = strClass.replace(" ", "%20");
//            } else {
                str_Class = strClass;
            }
        }
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchModel> call1 = mApiInterface.executeAdvanceFavoriteSearchAPI(strCapacityFrom, strCapacityTo, strLoaFrom, strLoaTo,
                strType, strYearofBuildFrom, strYearofBuildTo, strGrainFrom, strGrainTo, strBaleFrom, strName, strRecordID, strBaleTo, strStatus,
                strGeared, "staff", strTEUFrom, strTEUto, strPassangerFrom, strPassangerTo, strDraftsFrom, strDraftsTo,
                strLiquidFrom, strLiquidTo, strClass, strPlaceBuild, User_id);
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                SearchModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    mDialog.dismiss();
                    mArryList = mModel.getAllSearchedVessels();

                    /* SetAdapter */
                    setAdapter(mArryList);

                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    String strSearchText = "";

    private void executeNormalSearch(String searchText, String url) {
        SearchType = "normal";
        mArryList.clear();

        if (searchText != null && !searchText.equals("")) {
//            strSearchText = searchText.replace(" ", "%20");
//        } else {
            strSearchText = searchText;
        }

        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchModel> call1 = mApiInterface.searchVesselRequest(strSearchText, "user", User_id);
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                JaoharSingleton.getInstance().setSearchedVessel(strSearchText);
                SearchModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    //mVesselesModel.setImage(imagesArray[i]);
                    mArryList = mModel.getAllSearchedVessels();
                    setAdapter(mArryList);
                    //to show recent search
                    executeAPI();

                } else if (mModel.getStatus().equals("100")) {
                    //to show recent search
                    executeAPI();

                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    //to show recent search
                    executeAPI();

                    setAdapter(mArryList);

                    AlertDialogManager.hideProgressDialog();
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    private void executeNormalFavoriteSearch(String searchText, String url) {
        SearchType = "normal";
        mArryList.clear();

        if (searchText != null && !searchText.equals("")) {
//            strSearchText = searchText.replace(" ", "%20");
//        } else {
            strSearchText = searchText;
        }

        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchModel> call1 = mApiInterface.searchFavoriteVesselRequest(strSearchText, "user", User_id);
        call1.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, retrofit2.Response<SearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                JaoharSingleton.getInstance().setSearchedVessel(strSearchText);
                SearchModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    JaoharConstants.IsAllVesselsDetailBack = true;
                    //mVesselesModel.setImage(imagesArray[i]);
                    mArryList = mModel.getAllSearchedVessels();
                    setAdapter(mArryList);
                    //to show recent search
                    executeAPI();

                } else if (mModel.getStatus().equals("100")) {
                    //to show recent search
                    executeAPI();

                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    //to show recent search
                    executeAPI();

                    setAdapter(mArryList);

                    AlertDialogManager.hideProgressDialog();
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    private void setAdapter(final ArrayList<AllVessel> mArrayList) {
        vesselsRV.setNestedScrollingEnabled(false);
        vesselsRV.setHasFixedSize(true);
        vesselsRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        VesselsAdapter mVesselsAdapter = new VesselsAdapter(mActivity, mArrayList, mDeleteVesselsInterface,
                mSendEmailInterface, mSendMultiVesselDataInterface, strTextNEWS,
                strNormalTEXT, strPhotoURL, mPagination, mOpenPoUpInterface,
                mVesselToMail, new OnClickInterface() {
            @Override
            public void mOnClickInterface(int position) {
                PerformOptionsClick(position, mArrayList);
            }
        }, mFavoriteVesselInterface, mMailSelectedVesselsInterface);
        vesselsRV.setAdapter(mVesselsAdapter);
    }

    private void PerformOptionsClick(final int position, final ArrayList<AllVessel> mArrayList) {

        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_vessels_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout shareRL = view.findViewById(R.id.shareRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout mailToListRL = view.findViewById(R.id.mailToListRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);

        shareRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                shareViaWhatsApp();
            }
        });

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(mActivity, EditVesselActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("Title", "Edit Vessels");
                mActivity.startActivity(mIntent);
            }
        });

        copyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(mActivity, CopyVesselActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("Title", "Edit Vessels");
                mActivity.startActivity(mIntent);
            }
        });

        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.Is_click_form_All_Vessals = true;
                Intent mIntent = new Intent(mActivity, DetailsActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("isEditShow", true);
                mActivity.startActivity(mIntent);
            }
        });

        mailToListRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IsAllVesselsClick = true;
                Intent mIntent = new Intent(mActivity, SendingMailToLIst_Vessels.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(mActivity, SendingMailActivity.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void shareViaWhatsApp() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.whatsapp");
            shareIntent.putExtra("Intent.EXTRA_SUBJECT", "Jaohar");
            String shareMessage = "\nSee this Video\n";
            shareMessage = "https://play.google.com/store/apps/details?id=jaohar.com.jaohar&hl=en_IN&gl=US";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    private void executeDeleteVesselseAPI(String strVesselID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteVesselsRequest(strVesselID, User_id);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Toast.makeText(mActivity, "" + mModel.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

    public void deleteConfirmDialog(final String mVesselID) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteVesselseAPI(mVesselID);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    /* *
     * Execute Favorite API for vessel
     * @param
     * @user_id
     * */
    public void executeFavoriteAPI(String vessel_id, final ImageView imageView, final int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.makeFavoriteVesselsRequest(vessel_id, User_id);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    imageView.setImageResource(R.drawable.ic_bookmark_yellow);
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.the_vessel_has_been_fav));
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    /* *
     * Execute UnFavorite API for vessel
     * @param
     * @user_id
     * */
    public void executeUnFavoriteAPI(String vessel_id, final ImageView imageView, final int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.unFavoriteVesselsRequest(vessel_id, User_id);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    imageView.setImageResource(R.drawable.ic_bookmark_border);
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.the_vessel_has_been_unfav));
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });


    }
}
