package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.utils.AlertDialogManager;

public class EditInvoiceItemActivity extends BaseActivity {
    Activity mActivity = EditInvoiceItemActivity.this;
    String TAG = EditInvoiceItemActivity.this.getClass().getSimpleName(), strItemID;
    //WIDGET
    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;

    EditText editItemsET;
    EditText editQuantityET;
    EditText editPriceET;
    EditText editDescriptionET;

    Button btnUpdate;
    ArrayList<DiscountModel> mdiscountmodel = new ArrayList<DiscountModel>();
    InvoiceAddItemModel mModel, pModel;
    int mPosition, originalPositioon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_invoice_item);

        if (getIntent() != null) {
            mModel = (InvoiceAddItemModel) getIntent().getSerializableExtra("Model");
            mdiscountmodel = mModel.getmDiscountModelArrayList();
            Log.e(TAG, "SizeArray" + mdiscountmodel.size());
            mPosition = getIntent().getIntExtra("Position", 0);
            originalPositioon = mPosition;
            strItemID = mModel.getItemID();
        }
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtCenter.setText("Edit Item");

        editItemsET = (EditText) findViewById(R.id.editItemsET);
        editItemsET.requestFocus();
        editQuantityET = (EditText) findViewById(R.id.editQuantityET);
        editPriceET = (EditText) findViewById(R.id.editPriceET);
        editDescriptionET = (EditText) findViewById(R.id.editDescriptionET);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);

        /*Set Old Data onWidgets*/
        setDataOnWidgets(mModel);
    }

    private void setDataOnWidgets(InvoiceAddItemModel mModel) {
        editItemsET.setText(mModel.getItem());
        editItemsET.setSelection(mModel.getItem().length());

        editQuantityET.setText("" + mModel.getQuantity());
        editPriceET.setText("" + mModel.getUnitprice());

        editDescriptionET.setText(mModel.getDescription());
        editDescriptionET.setSelection(mModel.getDescription().length());
    }


    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editItemsET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                } else if (editQuantityET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_quantity));
                } else if (editPriceET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_price11));
                } else if (editDescriptionET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_description11));
                } else {

                    EditInvoiceActivity.isPaymentUpdated = true;
                    /*Execute Add Item Api*/
                    //executeApi();

                    pModel = new InvoiceAddItemModel();

                    pModel.setItem(editItemsET.getText().toString());
                    pModel.setItemID(strItemID);

                    if (editPriceET.getText().toString().length() > 0)
                        pModel.setUnitprice(editPriceET.getText().toString());
                    if (editQuantityET.getText().toString().length() > 0)
                        pModel.setQuantity(Integer.parseInt(editQuantityET.getText().toString()));

                    pModel.setDescription(editDescriptionET.getText().toString());
                    pModel.setmDiscountModelArrayList(mdiscountmodel);
                    Log.e(TAG, "pModelArray" + pModel.getmDiscountModelArrayList().size());
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("pModel", pModel);
                    returnIntent.putExtra("Position", mPosition);
                    setResult(909, returnIntent);
                    finish();
//                    Intent returnIntent = new Intent(mActivity,ShowEditInvoiceItemAciivity.class);
//                    returnIntent.putExtra("Model", mModel);
//                    returnIntent.putExtra("Position", mPosition);
//                    startActivity(returnIntent);
//                    finish();
//                    overridePendingTransitionExit();
                }
            }
        });
    }

//    private void executeApi() {
//        String strUrl = JaoharConstants.EDIT_INVOICE_ITEM + "?serial_no=" + editItemsET.getText().toString() + "&quantity=" + editItemsET.getText().toString() + "&price=" + editPriceET.getText().toString() + "&description=" + editDescriptionET.getText().toString() + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&invoice_item_id=" + mModel.getItemID();
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAlerDialog(mActivity, getString(R.string.app_name), mJsonObject.getString("message"), mJsonObject.getString("id"));
//                    } else if (mJsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage, final String itemID) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                mModel = new InvoiceAddItemModel();

                mModel.setItem(editItemsET.getText().toString());
                if (editPriceET.getText().toString().length() > 0)
                    mModel.setUnitprice(editPriceET.getText().toString());
                if (editQuantityET.getText().toString().length() > 0)
                    mModel.setQuantity(Integer.parseInt(editQuantityET.getText().toString()));
                mModel.setDescription(editDescriptionET.getText().toString());

                mModel.setItemID(itemID);

                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", mModel);
                setResult(111, returnIntent);
                finish();
                overridePendingTransitionExit();
            }
        });
        alertDialog.show();
    }


}
