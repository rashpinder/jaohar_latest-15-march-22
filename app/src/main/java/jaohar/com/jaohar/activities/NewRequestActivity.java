package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.manager_module.SearchNewRequestsActivity;
import jaohar.com.jaohar.adapters.NewRequestAdapter;
import jaohar.com.jaohar.interfaces.ConfirmRejectRequestInterface;
import jaohar.com.jaohar.models.GetAllNewReqModel;
import jaohar.com.jaohar.models.NewReqData;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class NewRequestActivity extends BaseActivity {
    Activity mActivity = NewRequestActivity.this;
    String TAG = NewRequestActivity.this.getClass().getSimpleName();

    //WIDGETS
    ImageView imgRight;
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    TextView txtCenter;
    EditText editSearchET;
    RecyclerView mRecyclerviewRV;
    NewRequestAdapter mNewRequestAdapter;
    ArrayList<NewReqData> mArrayList;
    ArrayList<NewReqData> filteredList = new ArrayList<>();

    ConfirmRejectRequestInterface mConfirmRequestInterface = new ConfirmRejectRequestInterface() {
        @Override
        public void getConfirmRequest(NewReqData mNewRequestModel, String strType) {
            showAlertDialogConfirmCancel(mActivity, mNewRequestModel, strType);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_new_request);
        mArrayList = JaoharSingleton.getInstance().getNewRequestArrayList();
    }

    @Override
    protected void setViewsIDs() {
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRight = findViewById(R.id.imgRight);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        editSearchET = findViewById(R.id.editSearchET);
        mRecyclerviewRV = findViewById(R.id.mRecyclerviewRV);

        /* set tool bar */
        txtCenter.setText(getString(R.string.new_requests));
        imgRight.setImageResource(R.drawable.ic_magnifying_glass);

        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            /*Execute Vesseles API*/
            executeAPI();
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        editSearchET.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                query = query.toString().toLowerCase();
                filteredList.clear();
                for (int i = 0; i < mArrayList.size(); i++) {
                    final String text = mArrayList.get(i).getEmail().toLowerCase();
                    if (text.contains(query)) {
                        filteredList.add(mArrayList.get(i));
                    }
                }
//                homeImprovementRV.setLayoutManager(new LinearLayoutManager(getActivity()));
//                mAdapter = new HomeMultiItemRecyclerAdapter(getActivity(), filteredList, getSelectedSkill);
//                mAdapter = new Work_Messege_Adapter(getActivity(), filteredList, mSelectItems, is_in_action_mode, isChecked, mItemLongPress);
//                workRC.setAdapter(mAdapter);
                //calling a method of the adapter class and passing the filtered list
                mNewRequestAdapter.filterList(filteredList);
//                mAdapter.notifyDataSetChanged();  // data set changed
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mActivity, SearchNewRequestsActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (JaoharConstants.Is_click_form_Manager == true) {
            super.onBackPressed();
            JaoharConstants.Is_click_form_Manager = false;
            startActivity(new Intent(mActivity, ManagerActivity.class));
            finish();
            overridePendingTransitionExit();
        } else if (JaoharConstants.Is_click_form_Push == true) {
            super.onBackPressed();
            JaoharConstants.Is_click_form_Push = false;
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mIntent.putExtra(JaoharConstants.LOGIN, "Home");
            startActivity(mIntent);
            finish();
            getFragmentManager().popBackStack();
            overridePendingTransitionExit();
        }
    }

    private void setAdapter() {
        Log.e(TAG, "VesselsAdapter: " + mArrayList.size());
        mRecyclerviewRV.setNestedScrollingEnabled(false);
        mRecyclerviewRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mNewRequestAdapter = new NewRequestAdapter(mActivity, mArrayList, mConfirmRequestInterface);
        mRecyclerviewRV.setAdapter(mNewRequestAdapter);
    }


    private void executeAPI() { 
        mArrayList.clear();
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllNewReqModel> call1 = mApiInterface.getAllNewRequest();
        call1.enqueue(new Callback<GetAllNewReqModel>() {
            @Override
            public void onResponse(Call<GetAllNewReqModel> call, retrofit2.Response<GetAllNewReqModel> response) {
                Log.e(TAG, "***URLResponce***" + response);
                GetAllNewReqModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    hideProgressDialog();
                    mArrayList=mModel.getData();
                //*Set Adapter*//*
                setAdapter();

            }
                else {
                    hideProgressDialog();
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetAllNewReqModel> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}


    
//    public void executeAPI() {
//        mArrayList.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strUrl = JaoharConstants.GET_ALL_NEW_REQUEST;
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);
//                parseResponse(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void parseResponse(String response) {
//        try {
//            JSONObject mJsonObject = new JSONObject(response);
//            if (mJsonObject.getString("status").equals("1")) {
//                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
//                for (int i = 0; i < mJsonArray.length(); i++) {
//                    JSONObject mJson = mJsonArray.getJSONObject(i);
//                    NewRequestModel mModel = new NewRequestModel();
//                    if (!mJson.isNull("id")) {
//                        mModel.setId(mJson.getString("id"));
//                    }
//                    if (!mJson.isNull("email")) {
//                        mModel.setEmail(mJson.getString("email"));
//                    }
//                    if (!mJson.isNull("password")) {
//                        mModel.setPassword(mJson.getString("password"));
//                    }
//                    if (!mJson.isNull("role")) {
//                        mModel.setRole(mJson.getString("role"));
//                    }
//                    if (!mJson.isNull("status")) {
//                        mModel.setStatus(mJson.getString("status"));
//                    }
//                    if (!mJson.isNull("created")) {
//                        mModel.setCreated(mJson.getString("created"));
//                    }
//                    if (!mJson.isNull("enabled")) {
//                        mModel.setEnabled(mJson.getString("enabled"));
//                    }
//
//                    mArrayList.add(mModel);
//                }
//                //*Set Adapter*//*
//                setAdapter();
//
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mJsonObject.getString("message"));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public void showAlertDialogConfirmCancel(Activity mActivity, final NewReqData mNewRequestModel, final String strType) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_confirm_cancel);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtConfirmTV = (TextView) alertDialog.findViewById(R.id.txtConfirmTV);
        TextView txtCancelTV = (TextView) alertDialog.findViewById(R.id.txtCancelTV);

        txtTitle.setText(getString(R.string.app_name));
        if (strType.equals("confirm")) {
            txtMessage.setText(getString(R.string.are_you_sure_accept));
        } else if (strType.equals("reject")) {
            txtMessage.setText(getString(R.string.are_you_sure_reject));
        }

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txtConfirmTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                executeConfirmRequestAPI(mNewRequestModel, strType);
            }
        });
        alertDialog.show();
    }

    private void executeAcceptMemberRequestApi(NewReqData mNewRequestModel, String strType) {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.acceptMemberRequest(mNewRequestModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus()==0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}


    private void executeRejectMemberRequestApi(NewReqData mNewRequestModel, String strType) {
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface =  ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.rejectMemberRequest(mNewRequestModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus()==0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );
    }



    /*Cofirm The Request*/
    public void executeConfirmRequestAPI(NewReqData mNewRequestModel, String strType) {
//        String strUrl = "";
        AlertDialogManager.showProgressDialog(mActivity);
        if (strType.equals("confirm")) {
            executeAcceptMemberRequestApi(mNewRequestModel, strType);
//            strUrl = JaoharConstants.ACCEPT_MEMBER_REQUEST + "?user_id=" + mNewRequestModel.getId();
        } else if (strType.equals("reject")) {
            executeRejectMemberRequestApi(mNewRequestModel,strType);
//            strUrl = JaoharConstants.REJECT_MEMBER_REQUEST + "?user_id=" + mNewRequestModel.getId();
        } }


//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);//{"status":"1","message":"Account enabled successfully"}
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else if (mJsonObject.getString("status").equals("0")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }



    public void showAlertDialogOKDONE(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                executeAPI();
            }
        });
        alertDialog.show();
    }
}
