package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.invoices_module.MailSignAdapter;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class MailSignActivity extends BaseActivity {
    Activity mActivity = MailSignActivity.this;
    String TAG = MailSignActivity.this.getClass().getSimpleName();

    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.imgRightLL)
    RelativeLayout imgRightLL;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    @BindView(R.id.imgAdd)
    ImageView imgAdd;
    @BindView(R.id.mailSignRV)
    RecyclerView mailSignRV;

    ArrayList<SignatureModel> mArrayList = new ArrayList<SignatureModel>();
    ArrayList<SignatureModel> filteredList = new ArrayList<SignatureModel>();
    MailSignAdapter mSignaturesAdapter;

//    DeleteSignInterface mDeleteSignInterface = new DeleteSignInterface() {
//        @Override
//        public void deleteSignature(SignatureModel mSignatureModel, int position) {
//            deleteConfirmDialog(mSignatureModel, position);
//        }
//    };
//
//    EditSignatureInterface mEditSignatureInterface = new EditSignatureInterface() {
//        @Override
//        public void editSignature(SignatureModel mSignatureModel) {
//            Intent mIntent = new Intent(mActivity, EditSignatureActivity.class);
//            mIntent.putExtra("Model", mSignatureModel);
//            mActivity.startActivity(mIntent);
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_sign);
        ButterKnife.bind(this);
        setStatusBar();

//        setViewsIDs();
//
//        setClickListner();
        setAdapter();

//        if (Utilities.isNetworkAvailable(mActivity) == false) {
//            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
//        } else {
//            executeAPI();
//        }
    }


    @Override
    protected void onResume() {
        super.onResume();

//        if (JaoharConstants.IS_SIGNATURE_EDIT)
//            executeAPI();

    }

    @OnClick({R.id.llLeftLL, R.id.imgAdd, R.id.imgSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.imgAdd:
                performAddClick();
                break;
            case R.id.imgSearch:
                performSearchClick();
                break;
        }
    }

    private void performSearchClick() {
//        Intent intent = new Intent(mActivity, SearchInvoiceStampsActivity.class);
//        intent.putExtra("QuestionListExtra", mArrayList);
//        startActivityForResult(intent, 333);
//        overridePendingTransitionEnter();
    }

    private void performAddClick() {
        Intent intent = new Intent(mActivity, AddMailSignActivity.class);
        startActivity(intent);
        overridePendingTransitionEnter();
    }



    protected void setViewsIDs() {
        imgRightLL.setVisibility(View.VISIBLE);
    }


    OnClickInterface mOnClickInterface = new OnClickInterface() {
        @Override
        public void mOnClickInterface(int position) {
            PerformOptionsClick(position);
        }
    };

    private void PerformOptionsClick(final int position) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_invoice_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout downloadRL = view.findViewById(R.id.downloadRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);
        View copyView = view.findViewById(R.id.copyView);
        View mailView = view.findViewById(R.id.mailView);
        View downloadView = view.findViewById(R.id.downloadView);
        copyRL.setVisibility(View.GONE);
        mailRL.setVisibility(View.GONE);
        downloadRL.setVisibility(View.GONE);
        copyRL.setVisibility(View.GONE);
        copyView.setVisibility(View.GONE);
        mailView.setVisibility(View.GONE);
        downloadView.setVisibility(View.GONE);



        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, EditMailSignActivity.class);
                startActivity(intent);
                overridePendingTransitionEnter();
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    protected void setClickListner() {
//        llLeftLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
//
//        imgAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent mIntent = new Intent(mActivity, AddSignatureActivity.class);
//                startActivity(mIntent);
//                overridePendingTransitionEnter();
//            }
//        });
//
//        imgSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(mActivity, SearchInvoiceSignActivity.class);
//                intent.putExtra("QuestionListExtra", mArrayList);
//                startActivityForResult(intent, 444);
//                overridePendingTransitionEnter();
//            }
//        });
//
//        editSearchET.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                charSequence = charSequence.toString().toLowerCase();
//                filteredList.clear();
//                for (int k = 0; k < mArrayList.size(); k++) {
//                    final String text = mArrayList.get(k).getSignature_name().toLowerCase();
//                    if (text.contains(charSequence)) {
//                        filteredList.add(mArrayList.get(k));
//                    }
//                }
//                mailSignRV.setLayoutManager(new LinearLayoutManager(mActivity));
//                mSignaturesAdapter = new MailSignAdapter(mActivity, filteredList, mDeleteSignInterface, mEditSignatureInterface);
//                mailSignRV.setAdapter(mSignaturesAdapter);
//                mSignaturesAdapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private void setAdapter() {
        Log.e(TAG, "Stamps: " + mArrayList.size());
        mailSignRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mSignaturesAdapter = new MailSignAdapter(mActivity,mOnClickInterface);
        mailSignRV.setAdapter(mSignaturesAdapter);
    }

    public void executeAPI() {
        mArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllSignsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                JaoharConstants.IS_SIGNATURE_EDIT = false;
                AlertDialogManager.hideProgressDialog();
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeAPI() {
//        mArrayList.clear();
//        String strUrl = JaoharConstants.GET_ALL_SIGNS + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                JaoharConstants.IS_SIGNATURE_EDIT = false;
//                AlertDialogManager.hideProgressDialog();
//                parseResponse(response);
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void parseResponse(String response) {
        mArrayList.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    SignatureModel mModel = new SignatureModel();
                    if (!mJson.isNull("sign_id"))
                        mModel.setId(mJson.getString("sign_id"));
                    if (!mJson.isNull("sign_name"))
                        mModel.setSignature_name(mJson.getString("sign_name"));
                    if (!mJson.isNull("sign_image"))
                        mModel.setSignature_image(mJson.getString("sign_image"));
                    mArrayList.add(mModel);
                }
            } else if (mJsonObject.getString("status").equals("100")) {
                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            }
            /*SetAdapter*/
            setAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteConfirmDialog(final SignatureModel mSignatureModel, int position) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_signaturey));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mSignatureModel, position);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    public void executeDeleteAPI(SignatureModel mSignatureModel, int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteSignsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), mSignatureModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    mArrayList.remove(position);

                    if (mSignaturesAdapter != null)
                        mSignaturesAdapter.notifyDataSetChanged();

                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeDeleteAPI(SignatureModel mSignatureModel) {
//        String strUrl = JaoharConstants.DELETE_SIGN + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&sign_id=" + mSignatureModel.getId();
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        //Toast.makeText(mActivity,""+mJsonObject.getString("message"),Toast.LENGTH_SHORT).show();
//                        executeAPI();
//                    } else if (mJsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check that it is the Activity with an OK result
        if (requestCode == 444) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", data.getSerializableExtra("Model"));
                mActivity.setResult(444, returnIntent);
                mActivity.finish();
            }
        }
    }
}
