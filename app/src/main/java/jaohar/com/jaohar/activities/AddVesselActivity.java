package jaohar.com.jaohar.activities;

import static android.os.Build.VERSION.SDK_INT;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.fiberlink.maas360.android.richtexteditor.RichEditText;
import com.fiberlink.maas360.android.richtexteditor.RichTextActions;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.Deflater;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.AddDocumentsAdapter;
import jaohar.com.jaohar.adapters.AddImageListAdapter;
import jaohar.com.jaohar.beans.AddGalleryImagesModel;
import jaohar.com.jaohar.beans.DocumentModel;
import jaohar.com.jaohar.beans.VessalClassModel;
import jaohar.com.jaohar.beans.VesselTypesModel;
import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.interfaces.DeleteImagesVessels;
import jaohar.com.jaohar.models.AddVesselmodel;
import jaohar.com.jaohar.models.GetVesselClassData;
import jaohar.com.jaohar.models.GetVesselClassModel;
import jaohar.com.jaohar.models.GetVesselTypeData;
import jaohar.com.jaohar.models.GetVesselTypeModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class AddVesselActivity extends BaseActivity {
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private final String cameraStr = Manifest.permission.CAMERA;
    private final String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE;
    private final String writeStorageStr = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    Activity mActivity = AddVesselActivity.this;
    String TAG = AddVesselActivity.this.getClass().getSimpleName(), strHyperLink;
    ImageView imgBack;
    LinearLayout llLeftLL;
    RelativeLayout rl2;
    RelativeLayout machineryRL;
    TextView txtCenter;
    EditText editVesselNameET, editIMONoET, editVesselTypeET, editYearBuiltET, editPlaceBuiltET, editClassET, editFlagET,
            editPriceIdeaET, editShortDescriptionET, editDWTET, editLoaET, editStatusET, editVesselBreathET, editVesselDepthET,
            editVesselLocationET, editCurrencyET, editBaleET, editGrainET, editDateET, editVesselTEUET, editLiquidET,
            editVesselPassangerET, editGasET, editVesselGrosssTonnageET, editNetTonnageET, editBuilderET, mmsiET,
            editVesselLenghtOverallET, editDraughtET, editHyperLinkET, editlightshipET, editTcmET, editGearedET, editOffMarketET;
    RichEditText mRichEditText, owner_edit_text, remarks_edit_text, machinery_edit_text;
    RichTextActions richTextActions;
    RichTextActions ownerTextActions;
    RichTextActions remarksTextActions;
    RichTextActions machineryTextActions;
    Spinner spinnerGeared;
    LinearLayout layoutFullDescriptionLL;
    Button btnAdd;
    LinearLayout layoutAddImagesLL;
    RecyclerView galleryRecyclerView;
    AddImageListAdapter mAdapter;
    ArrayList<AddGalleryImagesModel> mGalleryArrayList = new ArrayList<AddGalleryImagesModel>();
    String[] arrayVesselsType;
    String[] arrayStatus;
    String[] arrayCurrency;
    String[] arrayGeared;
    String arrayClassl;
    RequestBody url;
    String mCurrentPhotoPath, mStoragePath = "";
    VesselesModel mVesselesModel;
    String strTitleToolbar = "";
    boolean isOnResumeFirst = false;
    String strGeared = "";
    LinearLayout llFocusLL;
    String strRenderHtmlContent = "", strMachineryContent = "", strOwnerDetails = "", strRemarks = "";
    boolean isStart = false;
    //    DeleteImagesVessels mDeleteImagesInterface;
    byte[] strByteArray;
    byte[] strByteArray1;
    String strDocumentName = "";
    String strDocumentPath = "";
    TextView txtAttachDocument;
    RecyclerView mDocumentRecyclerView;
    ArrayList<DocumentModel> documentArrayList = new ArrayList<DocumentModel>();
    //    ArrayList<DocumentModel> documentArrayList = new ArrayList<DocumentModel>();
    ArrayList<String> arrayClassVEssal = new ArrayList<String>();
    AddDocumentsAdapter mAddDocumentsAdapter;
    VessalClassModel vessalClassModel = new VessalClassModel();
    ArrayList<Bitmap> mArrayList;
    byte[] byteArray1;
    byte[] byteArrayDoc1;
    byte[] byteArrayDoc2;
    byte[] byteArrayDoc3;
    byte[] byteArrayDoc4;
    byte[] byteArrayDoc5;
    byte[] byteArrayDoc6;
    byte[] byteArrayDoc7;
    byte[] byteArrayDoc8;
    byte[] byteArrayDoc9;
    byte[] byteArrayDoc10;
    byte[] byteArrayDoc11;
    byte[] byteArrayDoc12;
    byte[] byteArrayDoc13;
    byte[] byteArrayDoc14;
    byte[] byteArrayDoc15;
    byte[] byteArray2;
    byte[] byteArray3;
    byte[] byteArray4;
    byte[] byteArray5;
    byte[] byteArray6;
    byte[] byteArray7;
    byte[] byteArray8;
    byte[] byteArray9;
    byte[] byteArray10;
    byte[] byteArray16;
    byte[] byteArray17;
    byte[] byteArray18;
    byte[] byteArray19;
    byte[] byteArray20;
    byte[] byteArray21;
    byte[] byteArray22;
    byte[] byteArray23;
    byte[] byteArray25;
    byte[] byteArray26;
    byte[] byteArray27;
    byte[] byteArray28;
    byte[] byteArray29;
    byte[] byteArray30;
    ArrayList<GetVesselClassData> modelArrayList = new ArrayList<GetVesselClassData>();
    ArrayList<GetVesselTypeData> mArrayListClass = new ArrayList<GetVesselTypeData>();
    VesselTypesModel vesselTypesModel = new VesselTypesModel();
    String doc1 = null, doc2 = null, doc3 = null, doc4 = null, doc5 = null, doc6 = null, doc7 = null, doc8 = null, doc9 = null, doc10 = null, doc11 = null, doc12 = null, doc13 = null, doc14 = null, doc15 = null, doc16 = null, doc17 = null, doc18 = null, doc19 = null, doc20 = null, doc21 = null, doc22 = null, doc23 = null, doc24 = null, doc25 = null, doc26 = null, doc27 = null, doc28 = null, doc29 = null, doc30 = null;
    String docEx1 = null, docEx2 = null, docEx3 = null, docEx4 = null, docEx5 = null, docEx6 = null, docEx7 = null, docEx8 = null, docEx9 = null, docEx10 = null;
    String strActivityRefrence = "", strRecordID;
    Dialog cameraGalleryDialog;
    DeleteImagesVessels mDeleteImagesInterface = new DeleteImagesVessels() {
        @Override
        public void deleteImagesVessels(AddGalleryImagesModel mGalleryModel, int position) {
            if (mGalleryArrayList != null) {
                mGalleryArrayList.remove(position);
                setUpGalleryAdapter();
            }
        }
    };
    private long mLastClickTab1 = 0;

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    /**
     * Turn bitmap into byte array
     *
     * @param bitmap data
     * @returen byte array
     */
    public static byte[] getFileDataFromBitmap(Context context, Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vessel);

        setStatusBar();

        setViewIDs();
        setClicListner();
        llFocusLL = findViewById(R.id.llFocusLL);
        llFocusLL.requestFocus();
        arrayVesselsType = mActivity.getResources().getStringArray(R.array.vessel_type_array);
        arrayStatus = mActivity.getResources().getStringArray(R.array.status_array_user);
        arrayCurrency = mActivity.getResources().getStringArray(R.array.currency_array);
        arrayGeared = mActivity.getResources().getStringArray(R.array.geared_array);
        if (getIntent() != null) {
            strActivityRefrence = getIntent().getStringExtra("From");
        }

        editVesselNameET.setFilters(new InputFilter[]{
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        // TODO Auto-generated method stub
                        if (cs.equals("")) { // for backspace
                            return cs;
                        }
                        if (cs.toString().matches("[a-zA-Z 0-9]+")) {
                            return cs;
                        }
                        return "";
                    }
                }
        });
    }


    protected void setViewIDs() {
        imgBack = (ImageView) findViewById(R.id.imgBack);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        if (mVesselesModel != null && strTitleToolbar.length() > 2) {
            txtCenter.setText(strTitleToolbar);
        } else {
            txtCenter.setText(getString(R.string.add_vessels));
        }
        rl2 = (RelativeLayout) findViewById(R.id.rl2);
        layoutAddImagesLL = (LinearLayout) findViewById(R.id.layoutAddImagesLL);
        machineryRL = findViewById(R.id.machineryRL);
        galleryRecyclerView = (RecyclerView) findViewById(R.id.galleryRecyclerView);
        editVesselNameET = (EditText) findViewById(R.id.editVesselNameET);
        editIMONoET = (EditText) findViewById(R.id.editIMONoET);
        editVesselTypeET = (EditText) findViewById(R.id.editVesselTypeET);
//        editVesselTypeET.setKeyListener(null);
        editYearBuiltET = (EditText) findViewById(R.id.editYearBuiltET);
        editYearBuiltET.setKeyListener(null);
        editPlaceBuiltET = (EditText) findViewById(R.id.editPlaceBuiltET);
        editClassET = (EditText) findViewById(R.id.editClassET);
        editFlagET = (EditText) findViewById(R.id.editFlagET);
        editPriceIdeaET = (EditText) findViewById(R.id.editPriceIdeaET);
        editShortDescriptionET = (EditText) findViewById(R.id.editShortDescriptionET);
        editDWTET = (EditText) findViewById(R.id.editDWTET);
//      editLoaET = (EditText) findViewById(R.id.editLoaET);
        editStatusET = (EditText) findViewById(R.id.editStatusET);
        editStatusET.setKeyListener(null);
        editStatusET.setText("Available");
        editGearedET = findViewById(R.id.editGearedET);
        editGearedET.setKeyListener(null);

        mmsiET = findViewById(R.id.mmsiET);
        editOffMarketET = findViewById(R.id.editOffMarketET);
        editOffMarketET.setKeyListener(null);

        editVesselBreathET = (EditText) findViewById(R.id.editVesselBreathET);
        editVesselDepthET = (EditText) findViewById(R.id.editVesselDepthET);
        editVesselLocationET = (EditText) findViewById(R.id.editVesselLocationET);
        editCurrencyET = (EditText) findViewById(R.id.editCurrencyET);
        editCurrencyET.setKeyListener(null);
        editBaleET = (EditText) findViewById(R.id.editBaleET);
        editGrainET = (EditText) findViewById(R.id.editGrainET);
        editDateET = (EditText) findViewById(R.id.editDateET);
        editVesselTEUET = (EditText) findViewById(R.id.editVesselTEUET);
        editLiquidET = (EditText) findViewById(R.id.editLiquidET);
        editVesselPassangerET = (EditText) findViewById(R.id.editVesselPassangerET);
        editGasET = (EditText) findViewById(R.id.editGasET);
        editVesselGrosssTonnageET = (EditText) findViewById(R.id.editVesselGrosssTonnageET);
        editNetTonnageET = (EditText) findViewById(R.id.editNetTonnageET);
        editBuilderET = (EditText) findViewById(R.id.editBuilderET);

        editVesselLenghtOverallET = (EditText) findViewById(R.id.editVesselLenghtOverallET);
        editDraughtET = (EditText) findViewById(R.id.editDraughtET);
        editHyperLinkET = (EditText) findViewById(R.id.editHyperLinkET);
        editDateET.setKeyListener(null);
        mRichEditText = findViewById(R.id.rich_edit_text);
        mRichEditText.setOnKeyListener(null);
        richTextActions = findViewById(R.id.richTextActions);
        ownerTextActions = findViewById(R.id.ownerTextActions);
        machineryTextActions = findViewById(R.id.machineryTextActions);
        remarksTextActions = findViewById(R.id.remarksTextActions);
        editlightshipET = findViewById(R.id.editlightshipET);
        editTcmET = findViewById(R.id.editTcmET);
        mRichEditText.setRichTextActionsView(richTextActions);
        mRichEditText.setPadding(6, 0, 6, 0);

//        editDraughtET.setText("0");
//        editGrainET.setText("0");
//        editBaleET.setText("0");
//        editVesselTEUET.setText("0");
//        editVesselPassangerET.setText("0");
//        editLiquidET.setText("0");
//        editGasET.setText("0");
//        editlightshipET.setText("0");
//        editTcmET.setText("0");

        if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplication().getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }
        layoutFullDescriptionLL = (LinearLayout) findViewById(R.id.layoutFullDescriptionLL);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        /*Attach Document*/
        txtAttachDocument = (TextView) findViewById(R.id.txtAttachDocument);
        mDocumentRecyclerView = (RecyclerView) findViewById(R.id.mDocumentRecyclerView);

        machinery_edit_text = findViewById(R.id.machinery_edit_text);
        machinery_edit_text.setRichTextActionsView(machineryTextActions);
        machinery_edit_text.setPadding(6, 0, 6, 0);

        owner_edit_text = findViewById(R.id.owner_edit_text);
        owner_edit_text.setRichTextActionsView(ownerTextActions);
        owner_edit_text.setPadding(6, 0, 6, 0);

        remarks_edit_text = findViewById(R.id.remarks_edit_text);
        remarks_edit_text.setRichTextActionsView(remarksTextActions);
        remarks_edit_text.setPadding(6, 0, 6, 0);

        spinnerGeared = (Spinner) findViewById(R.id.spinnerGeared);
        setUpGearedSpinnerAdapter();
    }

    private void setUpGearedSpinnerAdapter() {
        final Typeface font = Typeface.createFromAsset(mActivity.getAssets(), "Calibri-Medium.ttf");
        final List<String> list = new ArrayList<String>();
        list.add("");
        list.add("Yes");
        list.add("No");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);

                    TextView itemTextView = v.findViewById(R.id.itemTextView);
                    String strName = list.get(position);
                    itemTextView.setText(strName);
                    itemTextView.setTypeface(font);
                }
                return v;
            }
        };
        spinnerGeared.setAdapter(dataAdapter);
        spinnerGeared.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strGeared = adapterView.getItemAtPosition(i).toString();
                ((TextView) view).setTextSize(14);
                ((TextView) view).setTypeface(font);
                ((TextView) view).setPadding(1, 0, 0, 0);
                if (strGeared.equals("")) {
                    strGeared = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    protected void setClicListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        layoutAddImagesLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "mGalleryArrayList.size(): " + mGalleryArrayList.size());
                if (mGalleryArrayList.size() <= 10 - 1) {
                    setUpCameraGalleryDialog();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit));
                }
            }
        });

        txtAttachDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (documentArrayList.size() < 10) {
                        openInternalStorage();
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
                    }
            }
        });

        editVesselTypeET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
//                    AlertDialogManager.showSelectItemFromArray(mActivity, "Vessels Type", arrayVesselsType, editVesselTypeET);
                    showPopUpType();
                    return true;
                }
                return false;
            }
        });

        machineryRL.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                machinery_edit_text.setRichTextActionsView(machineryTextActions);
                return false;
            }
        });

        editClassET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    showPopUpAdvanceSearch();

                    return true;
                }
                return false;
            }
        });

        editCurrencyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Currency", arrayCurrency, editCurrencyET);
                    return true;
                }
                return false;
            }
        });


        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });
        editGearedET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Geared", arrayGeared, editGearedET);
                    return true;
                }
                return false;
            }
        });
        editOffMarketET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Off Market", arrayGeared, editOffMarketET);
                    return true;
                }
                return false;
            }
        });
        editYearBuiltET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showYearPicker(mActivity, editYearBuiltET);
                    return true;
                }
                return false;
            }
        });

        editDateET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showDatePicker(mActivity, editDateET);
                    return true;
                }
                return false;
            }
        });

        machinery_edit_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    rl2.setVisibility(View.VISIBLE);
                    remarksTextActions.setVisibility(View.GONE);
                    ownerTextActions.setVisibility(View.GONE);
                    machineryTextActions.setVisibility(View.VISIBLE);
                    richTextActions.setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });
        remarks_edit_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    rl2.setVisibility(View.VISIBLE);
                    remarksTextActions.setVisibility(View.VISIBLE);
                    ownerTextActions.setVisibility(View.GONE);
                    machineryTextActions.setVisibility(View.GONE);
                    richTextActions.setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });
        mRichEditText.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    rl2.setVisibility(View.VISIBLE);
                    remarksTextActions.setVisibility(View.GONE);
                    ownerTextActions.setVisibility(View.GONE);
                    machineryTextActions.setVisibility(View.GONE);
                    richTextActions.setVisibility(View.VISIBLE);
                    return true;
                }
                return false;
            }
        });
        owner_edit_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    rl2.setVisibility(View.VISIBLE);
                    remarksTextActions.setVisibility(View.GONE);
                    ownerTextActions.setVisibility(View.VISIBLE);
                    machineryTextActions.setVisibility(View.GONE);
                    richTextActions.setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Preventing multiple clicks, using threshold of 2 second
                if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
                    return;
                }
                mLastClickTab1 = SystemClock.elapsedRealtime();

                strHyperLink = editHyperLinkET.getText().toString();
                Log.e(TAG, "**TEST**" + strGeared);
                strRenderHtmlContent = mRichEditText.getHtml();
//                strMachineryContent = machinery_edit_text.getHtml().toString();
                strMachineryContent = machinery_edit_text.getHtml();
                strOwnerDetails = owner_edit_text.getHtml();
                strRemarks = remarks_edit_text.getHtml();
                if (mGalleryArrayList.size() == 0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_seelct_at_least));
                } else if (editDateET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_date));
                } else if (editVesselNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_name));
                } else if (editVesselTypeET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_select_vessel_type));
                } else if (mmsiET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_select_mmsi_number));
                } else if (editIMONoET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_imo_number));
                } else if (editDWTET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_dwt));
                } else if (editFlagET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_flag));
                } else if (editYearBuiltET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_select_year_built));
                } else if (editOffMarketET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_off_market));
                } else if (editShortDescriptionET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_short_description));
                } else if (editClassET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_classes));
                } else if (editPlaceBuiltET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_place_built));
                } else if (editBuilderET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_builder));
                } else if (editGearedET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_geared));
                } else if (editVesselLenghtOverallET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_over_all_lenght));
                } else if (editVesselBreathET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_breath));
                } else if (editDraughtET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_draught));
                } else if (editVesselDepthET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_depth));
                } else if (editVesselGrosssTonnageET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_gross_tonnage));
                } else if (editNetTonnageET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_net_tonnage));
                } else if (editGrainET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_grain));
                } else if (editBaleET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_bale));
                } else if (editVesselTEUET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_teu));
                } else if (editVesselPassangerET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_num_of_passangers));
                } else if (editLiquidET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_liquid));
                } else if (editGasET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_gas));
                } else if (editlightshipET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_lightship));
                } else if (editTcmET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_tcm));
                } else if (editVesselLocationET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_vessel_location));
                } else if (editPriceIdeaET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_the_price_idea));
                } else if (editCurrencyET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_currency));
                } else if (editStatusET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_status));
                }
                else if (machinery_edit_text.getHtml().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_machinery_details));
                } else if (mRichEditText.getHtml().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_description));
                } else if (owner_edit_text.getHtml().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_owner_manager_details));
                } else if (remarks_edit_text.getHtml().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_the_remarks));
                } else {
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        preventMultipleViewClick();
                        executeAddVesselsAPI();
                    }
                }
            }
        });
    }

    private void openInternalStorage() {
        Log.e(TAG, String.valueOf(documentArrayList.size()));
        if (documentArrayList.size() == 0 || documentArrayList.size() <= 10) {
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("*/*");
            String[] mimetypes = {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation",
                    "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                   "text/plain","application/pdf","audio/*","video/*","application/rtf","text/*"};
            chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
            chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            startActivityForResult(chooseFile, Constant.REQUEST_CODE_PICK_FILE);
        } else if (documentArrayList.size() == 10) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOnResumeFirst = true;
        if (Utilities.isNetworkAvailable(mActivity) != true) {
            AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.internetconnection));
        } else {
            gettingVessal_Type();
            gettingVessal_Class();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2296) {
            if (SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    // perform action when allow permission success
                } else {
                    Toast.makeText(this, "Allow permission for storage access!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (resultCode == 123) {
            strRenderHtmlContent = data.getStringExtra("result");
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);//images.get(i).path

                            showLoadingAlertDialog(mActivity, "JAOHAR", "Loading Images Please wait...");
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    alertDialog.dismiss();
                                }
                            }, 3000);

                            final Handler handller = new Handler();
                            handller.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    for (int i = 0, l = images.size(); i < l; i++) {
                                        AddGalleryImagesModel model = new AddGalleryImagesModel();
                                        byte[] btye = getFileDataFromBitmap(mActivity, getThumbnailBitmap(images.get(i).path, 500));
                                        model.setStrImagePath(images.get(i).path);
                                        model.setByteArray(btye);
                                        model.setmBitmap(getThumbnailBitmap(images.get(i).path, 500));

                                        if (mGalleryArrayList.size() < 10)
                                            mGalleryArrayList.add(model);
                                    }

                                    /* Set GalleryImages in recycler View */
                                    setUpGalleryAdapter();

                                }
                            }, 200);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "*****No Picture Selected****");
                        return;
                    }
                } finally {
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 0;
                    options.inJustDecodeBounds = true;
                    Bitmap thumb = BitmapFactory.decodeFile(mStoragePath, options);
                    ExifInterface exifInterface = null;
                    try {
                        exifInterface = new ExifInterface(mStoragePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Matrix matrix = new Matrix();
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            matrix.setRotate(90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            matrix.setRotate(180);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            matrix.setRotate(270);
                            break;
                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                    }

                    Bitmap rotate = getThumbnailBitmap(mStoragePath, 500);
                    byte[] btye = getFileDataFromBitmap(mActivity, rotate);
                    AddGalleryImagesModel model = new AddGalleryImagesModel();
                    model.setStrImagePath(mStoragePath);
                    model.setByteArray(btye);
                    model.setmBitmap(rotate);
                    if (mGalleryArrayList.size() < 10)
                        mGalleryArrayList.add(model);
                    /* Set GalleryImages in recycler View */
                    setUpGalleryAdapter();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (requestCode == Constant.REQUEST_CODE_PICK_FILE && resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    showLoadingAlertDialog(mActivity, "JAOHAR", "Loading File Please wait...");
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            alertDialog.dismiss();
                        }
                    }, 3000);

                    final Handler handller = new Handler();
                    handller.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                try {
                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    File myFile = new File(uriString);
                    strDocumentPath= myFile.getAbsolutePath();
                    strDocumentName = null;

                    if (uriString.startsWith("content://")) {
                        Cursor cursor = null;
                        try {
                            cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                strDocumentName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                            }
                        } finally {
                            cursor.close();
                        }
                    } else if (uriString.startsWith("file://")) {
                        strDocumentName = myFile.getName();
                    }

//                    Uri uri = data.getData();
//                    strDocumentPath = uri.getPath();
//                    strDocumentPath = strDocumentPath.replace(" ", "_");
//                    strDocumentName = uri.getLastPathSegment();
                    InputStream iStream = null;
                    if (isVirtualFile(uri)) {
                        try {
                            iStream = getInputStreamForVirtualFile(uri, "*/*");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    else {
                        try {
                            iStream = getContentResolver().openInputStream(uri);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } }
                    try {
                        strByteArray = getBytes(iStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    DocumentModel mDocumentModel = new DocumentModel();
                    mDocumentModel.setId("11");
                    mDocumentModel.setDocumentName(strDocumentName);
                    mDocumentModel.setByteArray(strByteArray);
                    mDocumentModel.setmUri(uri);
                    mDocumentModel.setDocumentBase64(Utilities.convertByteArrayToBase64(strByteArray));
                    mDocumentModel.setDocumentPath(strDocumentPath);
                    documentArrayList.add(mDocumentModel);
                    setAdapter(documentArrayList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                        }
                    }, 200);
            }}
            if (requestCode == 205 && resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap thumb = null;
                    Uri uri = data.getData();
                    strDocumentPath = uri.getPath();
                    strDocumentPath = strDocumentPath.replace(" ", "_");
                    strDocumentName = uri.getLastPathSegment();
                    Bitmap bitmap;
                    String strDocumentPath, strDocumentName;

                    strDocumentPath = uri.getPath();
                    strDocumentPath = strDocumentPath.replace(" ", "_");
                    strDocumentName = uri.getLastPathSegment();

                    File finalFile = new File(getRealPathFromURI(uri));

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 0;
                    options.inJustDecodeBounds = true;
                    bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
                    // For Convert And rotate image
                    ExifInterface exifInterface = null;
                    try {
                        exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Matrix matrix = new Matrix();
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            matrix.setRotate(90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            matrix.setRotate(180);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            matrix.setRotate(270);
                            break;
                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                    }

                    thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    byte[] btye = getFileDataFromBitmap(mActivity, thumb);
                    AddGalleryImagesModel model = new AddGalleryImagesModel();
                    model.setStrImagePath(strDocumentPath);
                    model.setByteArray(btye);
                    model.setmBitmap(thumb);
                    if (mGalleryArrayList.size() < 10)
                        mGalleryArrayList.add(model);
                    /* Set GalleryImages in recycler View */
                    setUpGalleryAdapter();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String copyFileToInternalStorage(Uri uri, String newDirName) {
        Uri returnUri = uri;

        Cursor returnCursor =getContentResolver().query(returnUri, new String[]{
                OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE
        }, null, null, null);


        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));

        File output;
        if (!newDirName.equals("")) {
            File dir = new File(getFilesDir() + "/" + newDirName);
            if (!dir.exists()) {
                dir.mkdir();
            }
            output = new File(getFilesDir() + "/" + newDirName + "/" + name);
        } else {
            output = new File(getFilesDir() + "/" + name);
        }
        try {
            InputStream inputStream = getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(output);
            int read = 0;
            int bufferSize = 1024;
            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }

            inputStream.close();
            outputStream.close();

        } catch (Exception e) {

            Log.e("Exception", e.getMessage());
        }

        return output.getPath();
    }


    private boolean isVirtualFile(Uri uri) {
        if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (!DocumentsContract.isDocumentUri(mActivity, uri)) {
                return false;
            }

            Cursor cursor = getContentResolver().query(
                    uri,
                    new String[]{DocumentsContract.Document.COLUMN_FLAGS},
                    null, null, null);
            int flags = 0;
            if (cursor.moveToFirst()) {
                flags = cursor.getInt(0);
            }
            cursor.close();
            return (flags & DocumentsContract.Document.FLAG_VIRTUAL_DOCUMENT) != 0;
        } else {
            return false;
        }
    }

    private InputStream getInputStreamForVirtualFile(Uri uri, String mimeTypeFilter)
            throws IOException {

        ContentResolver resolver = getContentResolver();

        String[] openableMimeTypes = resolver.getStreamTypes(uri, mimeTypeFilter);

        if (openableMimeTypes == null ||
                openableMimeTypes.length < 1) {
            throw new FileNotFoundException();
        }

        return resolver
                .openTypedAssetFileDescriptor(uri, openableMimeTypes[0], null)
                .createInputStream();
    }

    public void compressPdf(String src, String dest) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest), PdfWriter.VERSION_1_5);
        stamper.getWriter().setCompressionLevel(9);
        int total = reader.getNumberOfPages() + 1;
        for (int i = 1; i < total; i++) {
            reader.setPageContent(i, reader.getPageContent(i));
        }
        stamper.setFullCompression();
        stamper.close();
        reader.close();
    }

    public byte[] compressByte(byte[] input) {
        // Compressor with highest level of compression
        Deflater compressor = new Deflater();
        compressor.setLevel(Deflater.BEST_COMPRESSION);

        // Give the compressor the data to compress
        compressor.setInput(input);
        compressor.finish();

        // Create an expandable byte array to hold the compressed data.
        // It is not necessary that the compressed data will be smaller than
        // the uncompressed data.
        ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);

        // Compress the data
        byte[] buf = new byte[1024];
        while (!compressor.finished()) {
            int count = compressor.deflate(buf);
            bos.write(buf, 0, count);
        }
        try {
            bos.close();
        } catch (IOException e) {
        }
        // Get the compressed data
        byte[] compressedData = bos.toByteArray();
        return compressedData;
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 512;
        byte[] buffer = new byte[bufferSize];
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void setAdapter(ArrayList<DocumentModel> mArrayList) {
        mDocumentRecyclerView.setNestedScrollingEnabled(false);
        mDocumentRecyclerView.setLayoutManager(new LinearLayoutManager(AddVesselActivity.this));
        mAddDocumentsAdapter = new AddDocumentsAdapter(mActivity, mArrayList);
        mDocumentRecyclerView.setAdapter(mAddDocumentsAdapter);
//        mAdapter.notifyDataSetChanged();
    }

    private void setUpGalleryAdapter() {
        if (mGalleryArrayList.size() >= 1) {
            galleryRecyclerView.setVisibility(View.VISIBLE);
        } else {
            galleryRecyclerView.setVisibility(View.GONE);
        }
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        galleryRecyclerView.setLayoutManager(horizontalLayoutManager);
        mAdapter = new AddImageListAdapter(mActivity, mGalleryArrayList, mDeleteImagesInterface);
        galleryRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private Bitmap getBitmapFromPath(String strPath) {
        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = 3;

        bitmap = BitmapFactory.decodeFile(strPath, options);
        // For Convert And rotate image
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(strPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotate;
    }

    /**
     * returns the thumbnail image bitmap from the given url
     *
     * @param path
     * @param thumbnailSize
     * @return
     */
    private Bitmap getThumbnailBitmap(final String path, final int thumbnailSize) {
        Bitmap bitmap;
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
            bitmap = null;
        }
        int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
                : bounds.outWidth;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / thumbnailSize;
        bitmap = BitmapFactory.decodeFile(path, opts);
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotate;
    }

    private void openCameraGalleryDialog() {
        cameraGalleryDialog = new Dialog(mActivity);
        cameraGalleryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cameraGalleryDialog.setContentView(R.layout.image_display_dialog);

        TextView text_camra = (TextView) cameraGalleryDialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) cameraGalleryDialog.findViewById(R.id.txt_gallery);
        TextView txt_files = (TextView) cameraGalleryDialog.findViewById(R.id.txt_files);
        TextView txt_cancel = (TextView) cameraGalleryDialog.findViewById(R.id.txt_cancel);

        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
                openGallery();
            }
        });
        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
                if (mGalleryArrayList.size() <= 10 - 1) {
                    openFiles();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
                }

            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraGalleryDialog.dismiss();
            }
        });
        cameraGalleryDialog.show();
        Log.e("error is occured", cameraGalleryDialog.toString());
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 205);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void chooseFile(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        String[] extraMimeTypes = {"application/pdf", "application/doc"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMimeTypes);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 10);
        startActivityForResult(intent, 205);
    }


    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Log.e(TAG, String.valueOf(mGalleryArrayList.size()));
        if (mGalleryArrayList.size() == 0) {
            Intent intent = new Intent(AddVesselActivity.this, AlbumSelectActivity.class);
            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 10);
            startActivityForResult(intent, GALLERY_REQUEST);
        } else if (mGalleryArrayList.size() == 10) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit));
        } else if (mGalleryArrayList.size() != 0) {
            Intent intent = new Intent(AddVesselActivity.this, AlbumSelectActivity.class);
            int GallarySize = 10 - mGalleryArrayList.size();
            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, GallarySize);
            startActivityForResult(intent, GALLERY_REQUEST);
        }
    }
    private boolean checkPermission() {
//     if (SDK_INT >= Build.VERSION_CODES.R) {
//        return Environment.isExternalStorageManager();
//    } else {
        int result = ContextCompat.checkSelfPermission(mActivity, readStorageStr);
        int result1 = ContextCompat.checkSelfPermission(mActivity, writeStorageStr);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
//    }
}

    private boolean checkPermissionGAllery() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);
        int writeStorage = ContextCompat.checkSelfPermission(getApplicationContext(), writeStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED && writeStorage == PackageManager.PERMISSION_GRANTED;
    }

//    private void requestPermission() {
//        if (SDK_INT >= Build.VERSION_CODES.R) {
//            try {
//                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
//                intent.addCategory("android.intent.category.DEFAULT");
//                intent.setData(Uri.parse(String.format("package:%s",getApplicationContext().getPackageName())));
//                startActivityForResult(intent, 2296);
//            } catch (Exception e) {
//                Intent intent = new Intent();
//                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
//                startActivityForResult(intent, 2296);
//            }
//        } else {
////            below android 11
//            ActivityCompat.requestPermissions(mActivity, new String[]{cameraStr,writeStorageStr,readStorageStr}, PERMISSION_REQUEST_CODE);
//        }
//    }

    void requestPermissionGallery() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readStorageStr, writeStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            if (cameraGalleryDialog != null && cameraGalleryDialog.isShowing()) {
                                cameraGalleryDialog.dismiss();
                            }
                            openCameraGalleryDialog();

                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
//                            if (SDK_INT >= Build.VERSION_CODES.R) {
//                                requestPermission();
//                            }
//                            else {
//                                requestPermissionGallery();
//                            }
                        }
                    }
                }
                break;
        }
    }

    public void setUpCameraGalleryDialog() {
        if (checkPermissionGAllery()) {
            openCameraGalleryDialog();
        } else {
            requestPermissionGallery();

        }
    }

    /* Execute api */

    public byte[] convertBitmapToByteArrayUncompressed(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        Log.e(TAG, "value--" + byteArray);
        return byteArray;
    }

    // generate dynamically string
    public String getAlphaNumericString() {
        int n = 20;

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        Log.e(TAG, "**********Image Name******" + sb.toString());
        return sb.toString();
    }

    private void executeAddVesselsAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;
        MultipartBody.Part mMultipartBody3 = null;
        MultipartBody.Part mMultipartBody4 = null;
        MultipartBody.Part mMultipartBody5 = null;
        MultipartBody.Part mMultipartBody6 = null;
        MultipartBody.Part mMultipartBody7 = null;
        MultipartBody.Part mMultipartBody8 = null;
        MultipartBody.Part mMultipartBody9 = null;
        MultipartBody.Part mMultipartBody10 = null;


        if (mGalleryArrayList.size() > 0) {
            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(0).getmBitmap()));
            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericString() + ".jpeg", requestFile1);
            Log.e("img", "img" + mMultipartBody1);
        }
        if (mGalleryArrayList.size() > 1) {
            RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(1).getmBitmap()));
            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericString() + ".jpeg", requestFile2);
            Log.e("img", "img" + mMultipartBody2);
        }
        if (mGalleryArrayList.size() > 2) {
            RequestBody requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(2).getmBitmap()));
            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericString() + ".jpeg", requestFile3);
            Log.e("img", "img" + mMultipartBody3);
        }
        if (mGalleryArrayList.size() > 3) {
            RequestBody requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(3).getmBitmap()));
            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericString() + ".jpeg", requestFile4);
            Log.e("img", "img" + mMultipartBody4);
        }
        if (mGalleryArrayList.size() > 4) {
            RequestBody requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(4).getmBitmap()));
            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericString() + ".jpeg", requestFile5);
            Log.e("img", "img" + mMultipartBody5);
        }
        if (mGalleryArrayList.size() > 5) {
            RequestBody requestFile6 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(5).getmBitmap()));
            mMultipartBody6 = MultipartBody.Part.createFormData("photo6", getAlphaNumericString() + ".jpeg", requestFile6);
            Log.e("img", "img" + mMultipartBody6);
        }
        if (mGalleryArrayList.size() > 6) {
            RequestBody requestFile7 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(6).getmBitmap()));
            mMultipartBody7 = MultipartBody.Part.createFormData("photo7", getAlphaNumericString() + ".jpeg", requestFile7);
            Log.e("img", "img" + mMultipartBody7);
        }
        if (mGalleryArrayList.size() > 7) {
            RequestBody requestFile8 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(7).getmBitmap()));
            mMultipartBody8 = MultipartBody.Part.createFormData("photo8", getAlphaNumericString() + ".jpeg", requestFile8);
            Log.e("img", "img" + mMultipartBody8);
        }
        if (mGalleryArrayList.size() > 8) {
            RequestBody requestFile9 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(8).getmBitmap()));
            mMultipartBody9 = MultipartBody.Part.createFormData("photo9", getAlphaNumericString() + ".jpeg", requestFile9);
            Log.e("img", "img" + mMultipartBody9);
        }
        if (mGalleryArrayList.size() > 9) {
            RequestBody requestFile10 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mGalleryArrayList.get(9).getmBitmap()));
            mMultipartBody10 = MultipartBody.Part.createFormData("photo10", getAlphaNumericString() + ".jpeg", requestFile10);
            Log.e("img", "img" + mMultipartBody10);
        }

        RequestBody vessel_name = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselNameET.getText().toString());
        RequestBody vessel_IMO_no = RequestBody.create(MediaType.parse("multipart/form-data"), editIMONoET.getText().toString());
        RequestBody vessel_type = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselTypeET.getText().toString());
        RequestBody vessel_built_year = RequestBody.create(MediaType.parse("multipart/form-data"), editYearBuiltET.getText().toString());
        RequestBody vessel_place_built = RequestBody.create(MediaType.parse("multipart/form-data"), editPlaceBuiltET.getText().toString());
        RequestBody vessel_class = RequestBody.create(MediaType.parse("multipart/form-data"), editClassET.getText().toString());
        RequestBody vessel_flag = RequestBody.create(MediaType.parse("multipart/form-data"), editFlagET.getText().toString());
        RequestBody vessel_price_idea = RequestBody.create(MediaType.parse("multipart/form-data"), editPriceIdeaET.getText().toString());
        RequestBody vessel_short_description = RequestBody.create(MediaType.parse("multipart/form-data"), editShortDescriptionET.getText().toString());
        RequestBody vessel_DWT = RequestBody.create(MediaType.parse("multipart/form-data"), editDWTET.getText().toString());
        Log.e(TAG, "vessel_DWT: " + vessel_DWT);
        RequestBody vessel_loa = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselLenghtOverallET.getText().toString());
        RequestBody vessel_breadth = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselBreathET.getText().toString());
        RequestBody vessel_depth = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselDepthET.getText().toString());
        RequestBody vessel_location = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselLocationET.getText().toString());
        RequestBody vessel_currency = RequestBody.create(MediaType.parse("multipart/form-data"), editCurrencyET.getText().toString());
        RequestBody vessel_long_description = RequestBody.create(MediaType.parse("multipart/form-data"), strRenderHtmlContent);
        RequestBody vessel_status = RequestBody.create(MediaType.parse("multipart/form-data"), editStatusET.getText().toString());
        RequestBody bale = RequestBody.create(MediaType.parse("multipart/form-data"), editBaleET.getText().toString());
        RequestBody grain = RequestBody.create(MediaType.parse("multipart/form-data"), editGrainET.getText().toString());
        RequestBody date = RequestBody.create(MediaType.parse("multipart/form-data"), editDateET.getText().toString());
        RequestBody teu = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselTEUET.getText().toString());
        RequestBody no_passengers = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselPassangerET.getText().toString());
        RequestBody liquid = RequestBody.create(MediaType.parse("multipart/form-data"), editLiquidET.getText().toString());
        RequestBody builder = RequestBody.create(MediaType.parse("multipart/form-data"), editBuilderET.getText().toString());
        RequestBody gross_tonnage = RequestBody.create(MediaType.parse("multipart/form-data"), editVesselGrosssTonnageET.getText().toString());
        RequestBody net_tonnage = RequestBody.create(MediaType.parse("multipart/form-data"), editNetTonnageET.getText().toString());
        Log.e(TAG, "executeAddVesselsAPI: " + gross_tonnage + "" + net_tonnage);
        RequestBody gas = RequestBody.create(MediaType.parse("multipart/form-data"), editGasET.getText().toString());
        RequestBody draught = RequestBody.create(MediaType.parse("multipart/form-data"), editDraughtET.getText().toString());
        if (strHyperLink.equals("")) {
            url = RequestBody.create(MediaType.parse("multipart/form-data"), strHyperLink);
        } else {
            if (strHyperLink.contains("http://")) {
                url = RequestBody.create(MediaType.parse("multipart/form-data"), strHyperLink);
            } else {
                url = RequestBody.create(MediaType.parse("multipart/form-data"), "http://" + strHyperLink);
            }
        }
        RequestBody owner_detail = RequestBody.create(MediaType.parse("multipart/form-data"), strOwnerDetails);
        RequestBody machinery_detail = RequestBody.create(MediaType.parse("multipart/form-data"), strMachineryContent);
        RequestBody remarks = RequestBody.create(MediaType.parse("multipart/form-data"), strRemarks);
        RequestBody ldt = RequestBody.create(MediaType.parse("multipart/form-data"), editlightshipET.getText().toString());
        RequestBody tcm = RequestBody.create(MediaType.parse("multipart/form-data"), editTcmET.getText().toString());
        RequestBody geared = RequestBody.create(MediaType.parse("multipart/form-data"), editGearedET.getText().toString());
        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        RequestBody mmsi_no = RequestBody.create(MediaType.parse("multipart/form-data"), mmsiET.getText().toString());
        RequestBody off_market = RequestBody.create(MediaType.parse("multipart/form-data"), editOffMarketET.getText().toString());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.addVesselsRequest(vessel_name, vessel_IMO_no, vessel_type, vessel_built_year, vessel_place_built,
                vessel_class, vessel_flag,
                vessel_price_idea, vessel_short_description, vessel_DWT, vessel_loa, vessel_breadth, vessel_depth,
                vessel_location, vessel_currency, vessel_long_description, vessel_status, bale, grain, date,
                teu, no_passengers, liquid, builder, gross_tonnage, net_tonnage, gas, draught, url, owner_detail, machinery_detail,
                remarks, ldt, tcm, geared, mMultipartBody1, mMultipartBody2, mMultipartBody3, mMultipartBody4,
                mMultipartBody5, mMultipartBody6, mMultipartBody7, mMultipartBody8, mMultipartBody9, mMultipartBody10, user_id, mmsi_no, off_market).enqueue(new Callback<AddVesselmodel>() {
            @Override
            public void onResponse(Call<AddVesselmodel> call, retrofit2.Response<AddVesselmodel> response) {
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                AddVesselmodel mModel = response.body();

                if (mModel.getStatus().equals("1")) {
                    strRecordID = mModel.getId();
                    if (mModel.getVessel_details() != null) {
                        JaoharSingleton.getInstance().setmAllVessel(mModel.getVessel_details());
                    }
                    if (documentArrayList.size() != 0) {
                        executeUploadDocument();
                    } else {
                        AlertDialogManager.hideProgressDialog();
                        alertConfirmDialog();
                    }
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.hideProgressDialog();
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.hideProgressDialog();
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddVesselmodel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private ArrayList<Bitmap> getVesselImagesArray(ArrayList<AddGalleryImagesModel> mGalleryArrayList) {
        mArrayList = new ArrayList();
//        try {
//            for (int i = 0; i < mGalleryArrayList.size(); i++) {
        for (int i = 0; i < mGalleryArrayList.size(); i++) {
            JaoharConstants.IS_camera_Click = true;
//                mArrayList.add(ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap()));
            mArrayList.add(mGalleryArrayList.get(i).getmBitmap());
        }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return mArrayList;
    }

    public void alertConfirmDialog() {
        final Dialog alert = new Dialog(mActivity);
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert.setContentView(R.layout.dialog_add_vessel_confirmation);
        alert.setCanceledOnTouchOutside(false);
        alert.setCancelable(false);
        alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtOK = (TextView) alert.findViewById(R.id.txtOK);
        TextView txtMessage = (TextView) alert.findViewById(R.id.txtMessage);
        if (mVesselesModel != null && strTitleToolbar.equals("Edit Vessels")) {
            txtMessage.setText("Updated Successfully");
        } else {
            txtMessage.setText("Added Successfully");
        }

        txtOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editVesselNameET.setText("");
                editIMONoET.setText("");
                editVesselTypeET.setText("");
                editVesselTypeET.setKeyListener(null);
                editYearBuiltET.setText("");
                editYearBuiltET.setKeyListener(null);
                editPlaceBuiltET.setText("");
                editClassET.setText("");
                editFlagET.setText("");
                editFlagET.requestFocus();
                editPriceIdeaET.setText("");
                editShortDescriptionET.setText("");
                editDWTET.setText("");
//                editLoaET.setText("");
                editStatusET.setText("");
                editStatusET.setKeyListener(null);
                editVesselBreathET.setText("");
                editVesselDepthET.setText("");
                editVesselLocationET.setText("");
                editCurrencyET.setText("");
                editCurrencyET.setKeyListener(null);
                editBaleET.setText("");
                editGrainET.setText("");
                editDateET.setText("");
                editDateET.setKeyListener(null);
                finish();
            }
        });

        alert.show();
    }

    private void executeUploadDocument() {
        for (int i = 0; i < documentArrayList.size(); i++) {
            if (i == 0) {
                byteArrayDoc1 = documentArrayList.get(i).getByteArray();
                doc1 = documentArrayList.get(i).getDocumentName();
                docEx1 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 1) {
                byteArrayDoc2 = documentArrayList.get(i).getByteArray();
                doc2 = documentArrayList.get(i).getDocumentName();
                docEx2 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 2) {
                byteArrayDoc3 = documentArrayList.get(i).getByteArray();
                doc3 = documentArrayList.get(i).getDocumentName();
                docEx3 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 3) {
                byteArrayDoc4 = documentArrayList.get(i).getByteArray();
                doc4 = documentArrayList.get(i).getDocumentName();
                docEx4 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 4) {
                byteArrayDoc5 = documentArrayList.get(i).getByteArray();
                doc5 = documentArrayList.get(i).getDocumentName();
                docEx5 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 5) {
                byteArrayDoc6 = documentArrayList.get(i).getByteArray();
                doc6 = documentArrayList.get(i).getDocumentName();
                docEx6 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 6) {
                byteArrayDoc7 = documentArrayList.get(i).getByteArray();
                doc7 = documentArrayList.get(i).getDocumentName();
                docEx7 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 7) {
                byteArrayDoc8 = documentArrayList.get(i).getByteArray();
                doc8 = documentArrayList.get(i).getDocumentName();
                docEx8 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 8) {
                byteArrayDoc9 = documentArrayList.get(i).getByteArray();
                doc9 = documentArrayList.get(i).getDocumentName();
                docEx9 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 9) {
                byteArrayDoc10 = documentArrayList.get(i).getByteArray();
                doc10 = documentArrayList.get(i).getDocumentName();
                docEx10 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 10) {
                byteArrayDoc11 = documentArrayList.get(i).getByteArray();
                doc11 = documentArrayList.get(i).getDocumentName();
            } else if (i == 11) {
                byteArrayDoc12 = documentArrayList.get(i).getByteArray();
                doc12 = documentArrayList.get(i).getDocumentName();
            } else if (i == 12) {
                byteArrayDoc13 = documentArrayList.get(i).getByteArray();
                doc13 = documentArrayList.get(i).getDocumentName();
            } else if (i == 13) {
                byteArrayDoc14 = documentArrayList.get(i).getByteArray();
                doc14 = documentArrayList.get(i).getDocumentName();
            } else if (i == 14) {
                byteArrayDoc15 = documentArrayList.get(i).getByteArray();
                doc15 = documentArrayList.get(i).getDocumentName();
            } else if (i == 15) {
                byteArray16 = documentArrayList.get(i).getByteArray();
                doc16 = documentArrayList.get(i).getDocumentName();
            } else if (i == 16) {
                byteArray17 = documentArrayList.get(i).getByteArray();
                doc17 = documentArrayList.get(i).getDocumentName();
            } else if (i == 17) {
                byteArray18 = documentArrayList.get(i).getByteArray();
                doc18 = documentArrayList.get(i).getDocumentName();
            } else if (i == 18) {
                byteArray19 = documentArrayList.get(i).getByteArray();
                doc19 = documentArrayList.get(i).getDocumentName();
            } else if (i == 18) {
                byteArray19 = documentArrayList.get(i).getByteArray();
                doc19 = documentArrayList.get(i).getDocumentName();
            } else if (i == 20) {
                byteArray20 = documentArrayList.get(i).getByteArray();
                doc20 = documentArrayList.get(i).getDocumentName();
            } else if (i == 21) {
                byteArray21 = documentArrayList.get(i).getByteArray();
                doc21 = documentArrayList.get(i).getDocumentName();
            } else if (i == 22) {
                byteArray22 = documentArrayList.get(i).getByteArray();
                doc22 = documentArrayList.get(i).getDocumentName();
            } else if (i == 23) {
                byteArray23 = documentArrayList.get(i).getByteArray();
                doc23 = documentArrayList.get(i).getDocumentName();
            } else if (i == 24) {
                byteArray25 = documentArrayList.get(i).getByteArray();
                doc25 = documentArrayList.get(i).getDocumentName();
            } else if (i == 25) {
                byteArray26 = documentArrayList.get(i).getByteArray();
                doc26 = documentArrayList.get(i).getDocumentName();
            } else if (i == 26) {
                byteArray27 = documentArrayList.get(i).getByteArray();
                doc27 = documentArrayList.get(i).getDocumentName();
            } else if (i == 27) {
                byteArray28 = documentArrayList.get(i).getByteArray();
                doc28 = documentArrayList.get(i).getDocumentName();
            } else if (i == 28) {
                byteArray29 = documentArrayList.get(i).getByteArray();
                doc29 = documentArrayList.get(i).getDocumentName();
            } else if (i == 29) {
                byteArray30 = documentArrayList.get(i).getByteArray();
                doc30 = documentArrayList.get(i).getDocumentName();
            }
        }

        AlertDialogManager.showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBodyDoc1 = null;
        MultipartBody.Part mMultipartBodyDoc2 = null;
        MultipartBody.Part mMultipartBodyDoc3 = null;
        MultipartBody.Part mMultipartBodyDoc4 = null;
        MultipartBody.Part mMultipartBodyDoc5 = null;
        MultipartBody.Part mMultipartBodyDoc6 = null;
        MultipartBody.Part mMultipartBodyDoc7 = null;
        MultipartBody.Part mMultipartBodyDoc8 = null;
        MultipartBody.Part mMultipartBodyDoc9 = null;
        MultipartBody.Part mMultipartBodyDoc10 = null;

        if (doc1 != null) {
            if (doc1.contains("http")){
            RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc1);
            mMultipartBodyDoc1 = MultipartBody.Part.createFormData("Document1", doc1, requestFileDoc1);
            Log.e("img", "img" + mMultipartBodyDoc1);}
            else{
                RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc1);
                mMultipartBodyDoc1 = MultipartBody.Part.createFormData("Document1", doc1+docEx1, requestFileDoc1);
                Log.e("img", "img" + mMultipartBodyDoc1);
            }
        }
        if (doc2 != null) {
            if (doc2.contains("http")){
            RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc2);
            mMultipartBodyDoc2 = MultipartBody.Part.createFormData("Document2", doc2, requestFileDoc2);
            Log.e("img", "img" + mMultipartBodyDoc2);}
            else{
                RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc2);
                mMultipartBodyDoc2 = MultipartBody.Part.createFormData("Document2", doc2+docEx2, requestFileDoc2);
                Log.e("img", "img" + mMultipartBodyDoc2);
            }
        }
        if (doc3 != null) {
            if (doc3.contains("http")){
            RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc3);
            mMultipartBodyDoc3 = MultipartBody.Part.createFormData("Document3", doc3, requestFileDoc3);
            Log.e("img", "img" + mMultipartBodyDoc3);}
            else{
                RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc3);
                mMultipartBodyDoc3 = MultipartBody.Part.createFormData("Document3", doc3+docEx3, requestFileDoc3);
                Log.e("img", "img" + mMultipartBodyDoc3);
            }
        }
        if (doc4 != null) {
            if (doc4.contains("http")){
            RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc4);
            mMultipartBodyDoc4 = MultipartBody.Part.createFormData("Document4", doc4, requestFileDoc4);
            Log.e("img", "img" + mMultipartBodyDoc4);}
            else{
                RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc4);
                mMultipartBodyDoc4 = MultipartBody.Part.createFormData("Document4", doc4+docEx4, requestFileDoc4);
                Log.e("img", "img" + mMultipartBodyDoc4);
            }
        }
        if (doc5 != null) {
            if (doc5.contains("http")){
            RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc5);
            mMultipartBodyDoc5 = MultipartBody.Part.createFormData("Document5", doc5, requestFileDoc5);
            Log.e("img", "img" + mMultipartBodyDoc5);}
            else{
                RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc5);
                mMultipartBodyDoc5 = MultipartBody.Part.createFormData("Document5", doc5+docEx5, requestFileDoc5);
                Log.e("img", "img" + mMultipartBodyDoc5);
            }
        }
        if (doc6 != null) {
            if (doc6.contains("http")){
            RequestBody requestFileDoc6 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc6);
            mMultipartBodyDoc6 = MultipartBody.Part.createFormData("Document6", doc6, requestFileDoc6);
            Log.e("img", "img" + mMultipartBodyDoc6);}
            else{
                RequestBody requestFileDoc6 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc6);
                mMultipartBodyDoc6 = MultipartBody.Part.createFormData("Document6", doc6+docEx6, requestFileDoc6);
                Log.e("img", "img" + mMultipartBodyDoc6);
            }
        }
        if (doc7 != null) {
            if (doc7.contains("http")){
            RequestBody requestFileDoc7 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc7);
            mMultipartBodyDoc7 = MultipartBody.Part.createFormData("Document7", doc7, requestFileDoc7);
            Log.e("img", "img" + mMultipartBodyDoc7);}
            else{
                RequestBody requestFileDoc7 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc7);
                mMultipartBodyDoc7 = MultipartBody.Part.createFormData("Document7", doc7+docEx7, requestFileDoc7);
                Log.e("img", "img" + mMultipartBodyDoc7);
            }
        }
        if (doc8 != null) {
            if (doc8.contains("http")){
            RequestBody requestFileDoc8 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc8);
            mMultipartBodyDoc8 = MultipartBody.Part.createFormData("Document8", doc8, requestFileDoc8);
            Log.e("img", "img" + mMultipartBodyDoc8);}
            else{
                RequestBody requestFileDoc8 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc8);
                mMultipartBodyDoc8 = MultipartBody.Part.createFormData("Document8", doc8+docEx8, requestFileDoc8);
                Log.e("img", "img" + mMultipartBodyDoc8);
            }
        }
        if (doc9 != null) {
            if (doc9.contains("http")){
            RequestBody requestFileDoc9 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc9);
            mMultipartBodyDoc9 = MultipartBody.Part.createFormData("Document9", doc9, requestFileDoc9);
            Log.e("img", "img" + mMultipartBodyDoc9);
        }
            else{
                RequestBody requestFileDoc9 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc9);
                mMultipartBodyDoc9 = MultipartBody.Part.createFormData("Document9", doc9+docEx9, requestFileDoc9);
                Log.e("img", "img" + mMultipartBodyDoc9);
            }}
        if (doc10 != null) {
            if (doc10.contains("http")){
            RequestBody requestFileDoc10 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc10);
            mMultipartBodyDoc10 = MultipartBody.Part.createFormData("Document10", doc10, requestFileDoc10);
            Log.e("img", "img" + mMultipartBodyDoc10);}
            else{
                RequestBody requestFileDoc10 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc10);
                mMultipartBodyDoc10 = MultipartBody.Part.createFormData("Document10", doc10+docEx10, requestFileDoc10);
                Log.e("img", "img" + mMultipartBodyDoc10);
            }
        }
        RequestBody record_id = RequestBody.create(MediaType.parse("multipart/form-data"), strRecordID);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.uploadDocumentRequest(record_id, mMultipartBodyDoc1, mMultipartBodyDoc2, mMultipartBodyDoc3, mMultipartBodyDoc4, mMultipartBodyDoc5, mMultipartBodyDoc6, mMultipartBodyDoc7, mMultipartBodyDoc8, mMultipartBodyDoc9, mMultipartBodyDoc10).enqueue(new Callback<StatusMsgModel>() {

            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
//                    if (doc10 != null) {
//                        executeUploadeditDocument();
//                    }
//                else {
                    alertConfirmDialog();
                    Log.e(TAG, "documentArrayList: " + documentArrayList);
                    if (documentArrayList != null) {
                        if (documentArrayList.size() > 0) {
                            JaoharSingleton.getInstance().setmAllDocument(documentArrayList);
                        }
                    }
//                    }

                } else {
                    Log.e(TAG, "Unexpected*********:" + mModel.getMessage());
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void gettingVessal_Class() {
        modelArrayList.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselClassModel> call1 = mApiInterface.getVesselClassRequest();
        call1.enqueue(new Callback<GetVesselClassModel>() {
                          @Override
                          public void onResponse(Call<GetVesselClassModel> call, retrofit2.Response<GetVesselClassModel> response) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetVesselClassModel mModel = response.body();
                              assert mModel != null;
                              if (mModel.getStatus().equals("1")) {
                                  modelArrayList = mModel.getData();
                              } else {
                                  AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetVesselClassModel> call, Throwable t) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    private void gettingVessal_Type() {
        mArrayListClass.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselTypeModel> call1 = mApiInterface.getVesselTypeRequest();
        call1.enqueue(new Callback<GetVesselTypeModel>() {
                          @Override
                          public void onResponse(Call<GetVesselTypeModel> call, retrofit2.Response<GetVesselTypeModel> response) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetVesselTypeModel mModel = response.body();
                              assert mModel != null;
                              if (mModel.getStatus().equals("1")) {
                                  mArrayListClass = mModel.getData();
                              } else {
                                  AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetVesselTypeModel> call, Throwable t) {
//                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    private void showPopUpType() {
        final Dialog categoryyDialog = new Dialog(mActivity);
        categoryyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        categoryyDialog.setContentView(R.layout.item_list_categories);
        categoryyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) categoryyDialog.findViewById(R.id.txtTitle);
        txtTitle.setText("Vessel Type");
        ListView lstListView = (ListView) categoryyDialog.findViewById(R.id.lstListView);
        ArrayAdapter<GetVesselTypeData> adapter = new ArrayAdapter<GetVesselTypeData>(mActivity,
                android.R.layout.simple_list_item_1, android.R.id.text1, mArrayListClass);
        lstListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                editVesselTypeET.setText(mArrayListClass.get(position).getName());
                categoryyDialog.dismiss();
            }
        });
        categoryyDialog.show();
    }

    private void showPopUpAdvanceSearch() {
        final Dialog categoryDialog = new Dialog(mActivity);
        categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        categoryDialog.setContentView(R.layout.item_list_categories);
//        categoryDialog.setCanceledOnTouchOutside(true);
        categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) categoryDialog.findViewById(R.id.txtTitle);
        txtTitle.setText("Vessel Class");
        ListView lstListView = (ListView) categoryDialog.findViewById(R.id.lstListView);
        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data

        ArrayAdapter<GetVesselClassData> adapter = new ArrayAdapter<>(mActivity,
                android.R.layout.simple_list_item_1, android.R.id.text1, modelArrayList);
        // Assign adapter to ListView
        lstListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        lstListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                editClassET.setText(modelArrayList.get(position).getClassName());
                categoryDialog.dismiss();
            }
        });
        categoryDialog.show();
    }

    private void executeUploadeditDocument() {
        for (int i = 0; i < documentArrayList.size(); i++) {
            if (i == 0) {
                byteArray1 = documentArrayList.get(i).getByteArray();
                doc1 = documentArrayList.get(i).getDocumentName();
            } else if (i == 1) {
                byteArray2 = documentArrayList.get(i).getByteArray();
                doc2 = documentArrayList.get(i).getDocumentName();
            } else if (i == 2) {
                byteArray3 = documentArrayList.get(i).getByteArray();
                doc3 = documentArrayList.get(i).getDocumentName();
            } else if (i == 3) {
                byteArray4 = documentArrayList.get(i).getByteArray();
                doc4 = documentArrayList.get(i).getDocumentName();
            } else if (i == 4) {
                byteArray5 = documentArrayList.get(i).getByteArray();
                doc5 = documentArrayList.get(i).getDocumentName();
            } else if (i == 5) {
                byteArray6 = documentArrayList.get(i).getByteArray();
                doc6 = documentArrayList.get(i).getDocumentName();
            } else if (i == 6) {
                byteArray7 = documentArrayList.get(i).getByteArray();
                doc7 = documentArrayList.get(i).getDocumentName();
            } else if (i == 7) {
                byteArray8 = documentArrayList.get(i).getByteArray();
                doc8 = documentArrayList.get(i).getDocumentName();
            } else if (i == 8) {
                byteArray9 = documentArrayList.get(i).getByteArray();
                doc9 = documentArrayList.get(i).getDocumentName();
            } else if (i == 9) {
                byteArray10 = documentArrayList.get(i).getByteArray();
                doc10 = documentArrayList.get(i).getDocumentName();
            }
        }

        AlertDialogManager.showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;
        MultipartBody.Part mMultipartBody3 = null;
        MultipartBody.Part mMultipartBody4 = null;
        MultipartBody.Part mMultipartBody5 = null;
        MultipartBody.Part mMultipartBody6 = null;
        MultipartBody.Part mMultipartBody7 = null;
        MultipartBody.Part mMultipartBody8 = null;
        MultipartBody.Part mMultipartBody9 = null;
        MultipartBody.Part mMultipartBody10 = null;
        if (doc1 != null) {
            RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc1);
            mMultipartBody1 = MultipartBody.Part.createFormData("Document1", doc1, requestFileDoc1);
            Log.e("img", "img" + mMultipartBody1);
        }
        if (doc2 != null) {
            RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc2);
            mMultipartBody2 = MultipartBody.Part.createFormData("Document2", doc2, requestFileDoc2);
            Log.e("img", "img" + mMultipartBody2);
        }
        if (doc3 != null) {
            RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc3);
            mMultipartBody3 = MultipartBody.Part.createFormData("Document3", doc3, requestFileDoc3);
            Log.e("img", "img" + mMultipartBody3);
        }
        if (doc4 != null) {
            RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc4);
            mMultipartBody4 = MultipartBody.Part.createFormData("Document4", doc4, requestFileDoc4);
            Log.e("img", "img" + mMultipartBody4);
        }
        if (doc5 != null) {
            RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc5);
            mMultipartBody5 = MultipartBody.Part.createFormData("Document5", doc5, requestFileDoc5);
            Log.e("img", "img" + mMultipartBody5);
        }
        if (doc6 != null) {
            RequestBody requestFileDoc6 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc6);
            mMultipartBody6 = MultipartBody.Part.createFormData("Document6", doc6, requestFileDoc6);
            Log.e("img", "img" + mMultipartBody6);
        }
        if (doc7 != null) {
            RequestBody requestFileDoc7 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc7);
            mMultipartBody7 = MultipartBody.Part.createFormData("Document7", doc7, requestFileDoc7);
            Log.e("img", "img" + mMultipartBody7);
        }
        if (doc8 != null) {
            RequestBody requestFileDoc8 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc8);
            mMultipartBody8 = MultipartBody.Part.createFormData("Document8", doc8, requestFileDoc8);
            Log.e("img", "img" + mMultipartBody8);
        }
        if (doc9 != null) {
            RequestBody requestFileDoc9 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc9);
            mMultipartBody9 = MultipartBody.Part.createFormData("Document9", doc9, requestFileDoc9);
            Log.e("img", "img" + mMultipartBody9);
        }
        if (doc10 != null) {
            RequestBody requestFileDoc10 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc10);
            mMultipartBody10 = MultipartBody.Part.createFormData("Document10", doc10, requestFileDoc10);
            Log.e("img", "img" + mMultipartBody10);
        }
        RequestBody record_id = RequestBody.create(MediaType.parse("multipart/form-data"), strRecordID);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.uploadDocumentRequest(record_id, mMultipartBody1, mMultipartBody2, mMultipartBody3, mMultipartBody4, mMultipartBody5, mMultipartBody6, mMultipartBody7, mMultipartBody8, mMultipartBody9, mMultipartBody10).enqueue(new Callback<StatusMsgModel>() {

            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    Log.e(TAG, "documentArrayList: " + documentArrayList);
                    if (documentArrayList.size() > 0) {
                        JaoharSingleton.getInstance().setmAllDocument(documentArrayList);
                    }
                    Log.e(TAG, "Messsage******:" + mModel.getMessage());
                    alertConfirmDialog();
                } else {
                    Log.e(TAG, "Unexpected*********:" + mModel.getMessage());
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

}
