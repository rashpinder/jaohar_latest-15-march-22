package jaohar.com.jaohar.activities;

import static android.os.Build.VERSION.SDK_INT;

import static com.darsh.multipleimageselect.helpers.Constants.PERMISSION_REQUEST_CODE;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darsh.multipleimageselect.helpers.Constants;
import com.fiberlink.maas360.android.richtexteditor.RichEditText;
import com.fiberlink.maas360.android.richtexteditor.RichTextActions;
import com.google.gson.JsonObject;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.AddDocumentsAdapter;
import jaohar.com.jaohar.beans.AddGalleryImagesModel;
import jaohar.com.jaohar.beans.DocumentModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.RealPathUtil;
import jaohar.com.jaohar.utils.Utilities;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class SendMailingListActivity extends BaseActivity {
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    // Permissions
    private String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String CAMERA = Manifest.permission.CAMERA;
    Activity mActivity = SendMailingListActivity.this;
    String TAG = SendMailingListActivity.this.getClass().getSimpleName();
    TextView txtCenter;
    ImageView imgBack, imgDOCCrossIV;
    LinearLayout llLeftLL;
    RichEditText mRichEditText;
    RichTextActions richTextActions;
    String strRenderHtmlContent;
    ImageView imgImageIV, imgDOCIV;
    LinearLayout layoutTakeImageLL;
    Button btnSendMAILB;
    EditText editSubjectET, editHeading_OneET, editHeading_TwoET, editSignaturesET;
    String strIMAGE = "", strListID = "", strTypeOFUser = "";
    Bitmap thumb = null;
    String mCurrentPhotoPath, mStoragePath = "";
    ArrayList<String> mlistIDArray = new ArrayList<>();
    ArrayList<DocumentModel> documentArrayList = new ArrayList<DocumentModel>();
    ArrayList<String> list_ids = new ArrayList<>();
    static String isAllList = "";
    String strDocumentName = "";
//    String docEx1 = ".pdf";
    String strDocumentPath = "";
    String strDocBase64 = "";
    byte[] strByteArray;
    private long mLastClickTab1 = 0;
    TextView txtAttachDocument;
//    ArrayList<DocumentModel> documentArrayList = new ArrayList<DocumentModel>();
    RecyclerView mDocumentRecyclerView;
    AddDocumentsAdapter mAddDocumentsAdapter;
    byte[] byteArrayDoc1;
    byte[] byteArrayDoc2;
    byte[] byteArrayDoc3;
    byte[] byteArrayDoc4;
    byte[] byteArrayDoc5;
    String doc1 = null, doc2 = null, doc3 = null, doc4 = null, doc5 = null;
    RequestBody list_id;
//    RequestBody list_ids;
    RequestBody subject;
    RequestBody message;
    RequestBody head_one;
    RequestBody head_two;
    RequestBody doc_name;
    RequestBody image;
    RequestBody user_id;
    RequestBody sign;
    String docEx1 = null, docEx2 = null, docEx3 = null, docEx4 = null, docEx5 = null, docEx6 = null, docEx7 = null, docEx8 = null, docEx9 = null, docEx10 = null;
    private final String writeStorageStr = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE;
    //    ArrayList<String> descriptionList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_send_mailing_list);
        if (getIntent() != null) {
            if (getIntent().getStringExtra("listID") != null) {
                strListID = getIntent().getStringExtra("listID");
                strTypeOFUser = getIntent().getStringExtra("type");
                isAllList = "1";
            }
            if (getIntent().getStringArrayListExtra("listIDArray") != null) {
                mlistIDArray = getIntent().getStringArrayListExtra("listIDArray");
                strTypeOFUser = getIntent().getStringExtra("type");
                isAllList = "2";
            }
        }
    }

    @Override
    protected void setViewsIDs() {
        btnSendMAILB = findViewById(R.id.btnSendMAILB);
        mDocumentRecyclerView = findViewById(R.id.mDocumentRecyclerView);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        layoutTakeImageLL = (LinearLayout) findViewById(R.id.layoutTakeImageLL);
        imgImageIV = (ImageView) findViewById(R.id.imgImageIV);
        imgDOCCrossIV = (ImageView) findViewById(R.id.imgDOCCrossIV);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        editSignaturesET = findViewById(R.id.editSignaturesET);
        editSubjectET = findViewById(R.id.editSubjectET);
        editHeading_TwoET = findViewById(R.id.editHeading_TwoET);
        editHeading_OneET = findViewById(R.id.editHeading_OneET);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgDOCIV = (ImageView) findViewById(R.id.imgDOCIV);
        txtAttachDocument = (TextView) findViewById(R.id.txtAttachDocument);

        txtCenter.setText(getString(R.string.mail_details_));
        imgBack.setImageResource(R.drawable.back);

        mRichEditText = (RichEditText) findViewById(R.id.rich_edit_text);
        mRichEditText.setOnKeyListener(null);
        richTextActions = (RichTextActions) findViewById(R.id.richTextActions);
        mRichEditText.setRichTextActionsView(richTextActions);
        mRichEditText.setPadding(6, 0, 6, 0);
        strRenderHtmlContent = mRichEditText.getHtml().toString();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplication().getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }

        }
    }


    private boolean checkPermission() {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager();
        } else {
            int result = ContextCompat.checkSelfPermission(mActivity, readStorageStr);
            int result1 = ContextCompat.checkSelfPermission(mActivity, writeStorageStr);
            return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
        }}

    private void requestPermission() {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse(String.format("package:%s",getApplicationContext().getPackageName())));
                startActivityForResult(intent, 2296);
            } catch (Exception e) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(intent, 2296);
            }
        } else {
            //below android 11
            ActivityCompat.requestPermissions(mActivity, new String[]{writeStorageStr}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    protected void setClickListner() {
        layoutTakeImageLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mToGrantPermission();
            }
        });


        txtAttachDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (checkPermission()) {
                if (documentArrayList.size() <= 5 - 1) {
                    openInternalStorage();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limitt));
                }
//            }
//                else {
//                        requestPermission();
//                    }
            }
        });


        imgDOCIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (mCheckPermission()) {
                    openInternalStorage();
//                } else {
//                    mRequestPermission();
//                }

            }
        });

        imgDOCCrossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgDOCIV.setImageDrawable(getResources().getDrawable(R.drawable.palace_holder));
                imgDOCCrossIV.setVisibility(View.GONE);
                strDocBase64 = "";
                strDocumentName = "";
            }
        });




        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransitionExit();
            }
        });

        btnSendMAILB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editSubjectET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.enter_subject_name));
                }
//                else if (editHeading_OneET.getText().toString().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.enter_heading_one));
//                }
//                else if (editHeading_TwoET.getText().toString().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.enter_heading_two));
//                }
                else if (editSignaturesET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.enter_sign));
                } else if (mRichEditText.getHtml().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.enter_mail_body));
                } else {
                    if (Utilities.isNetworkAvailable(mActivity)) {
                        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
                            return;
                        }
                        mLastClickTab1 = SystemClock.elapsedRealtime();
                        showMailAlertDialog(mActivity);
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.internetconnection));
                    }
                }
//             Toast.makeText(mActivity,"Under Process" ,Toast.LENGTH_SHORT).show();
            }
        });
    }



    public void showMailAlertDialog(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_mail_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtSendMailInBg = (TextView) alertDialog.findViewById(R.id.txtSendMailInBg);
        TextView txtSendMailUsingWeb = (TextView) alertDialog.findViewById(R.id.txtSendMailUsingWeb);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txtSendMailInBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                AlertDialogManager.showProgressDialog(mActivity);
                sendEamilInBgAPI();
            }
        });
        txtSendMailUsingWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                AlertDialogManager.showProgressDialog(mActivity);
                sendEamilAPI();
            }
        });
        alertDialog.show();
    }

//    private void openInternalStorage() {
//        Intent intent = new Intent();
////        intent.setType("application/pdf");
//        intent.setType("*/*");
////        intent.setType("xlsx/xls/image/doc/audio/docx/ppt/pptx/txt/pdf");
//        intent.putExtra("maxSelection", 10);// Optional
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(intent, 505);
//    }


    private void openInternalStorage() {
        Log.e(TAG, String.valueOf(documentArrayList.size()));
        if (documentArrayList.size() == 0 || documentArrayList.size() <= 10) {
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("*/*");
            String[] mimetypes = {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation",
                    "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "text/plain","application/pdf","audio/*","video/*","application/rtf","text/*"};
            chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
            chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            startActivityForResult(chooseFile, Constant.REQUEST_CODE_PICK_FILE);
        } else if (documentArrayList.size() == 10) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limit));
        }
    }


//    private void openInternalStorage() {
//        Log.e(TAG, String.valueOf(documentArrayList.size()));
//        if (documentArrayList.size() == 0) {
//            Intent intent4 = new Intent(this, NormalFilePickActivity.class);
//            intent4.putExtra(Constant.MAX_NUMBER, 5);
//            intent4.putExtra(Constants.INTENT_EXTRA_LIMIT,5);
//            intent4.putExtra(NormalFilePickActivity.SUFFIX, new String[] {"pdf","txt","mp3","xlsx","rtf","xls","doc","docx","ppt","pptx","mp4"});
//            startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);
////            Intent intent = new Intent(this, FilePickerActivity.class);
////            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
////                    .setShowImages(true)
////                    .setShowAudios(false)
////                    .setMaxSelection(5)
////                    .setShowVideos(false)
////                    .setShowFiles(true)
////                    .setSuffixes("pdf","txt","mp3","xlsx","rtf","xls","doc","docx","ppt","pptx","mp4")
////                    .setSkipZeroSizeFiles(true)
////                    .build());
////
//////            Intent intent = new Intent(AddVesselActivity.this, AlbumSelectActivity.class);
////            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 5);
////            startActivityForResult(intent, 505);
//        } else if (documentArrayList.size() == 5) {
//            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_documents_limitt));
//        } else if (documentArrayList.size() != 0) {
//            Intent intent4 = new Intent(this, NormalFilePickActivity.class);
//            int GallarySize = 5 - documentArrayList.size();
//            intent4.putExtra(Constant.MAX_NUMBER, GallarySize);
//            intent4.putExtra(NormalFilePickActivity.SUFFIX, new String[] {"pdf","txt","mp3","xlsx","rtf","xls","doc","docx","ppt","pptx","mp4","jpeg","png","jpg"});
//            startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);
////            Intent intent = new Intent(this, FilePickerActivity.class);
////            int GallarySize = 5 - documentArrayList.size();
////            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, GallarySize);
////            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
////                    .setShowImages(true)
////                    .setShowAudios(false)
////                    .setShowVideos(false)
////                    .setMaxSelection(GallarySize)
////                    .setShowFiles(true)
////                    .setSuffixes("pdf","txt","mp3","xlsx","rtf","xls","doc","docx","ppt","pptx","mp4","jpeg","png","jpg")
////                    .setSkipZeroSizeFiles(true)
////                    .build());
////
////            startActivityForResult(intent, 505);
//        }}


    /*******
     * PERMISSIONS
     * **********/
    public void mToGrantPermission() {
        if (mCheckPermission()) {
            openCameraGalleryDialog();
        } else {
            mRequestPermission();
        }
    }

    private boolean mCheckPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int read_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int write_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return camera == PackageManager.PERMISSION_GRANTED && read_external_storage == PackageManager.PERMISSION_GRANTED && write_external_storage == PackageManager.PERMISSION_GRANTED;
    }

    private void mRequestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, 100);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            //All Permissions Granted
//                            openCameraGalleryDialog();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            Toast.makeText(mActivity, getString(R.string.goto_settings), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);
        TextView text_camra = (TextView) dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = (TextView) dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_files.setVisibility(View.GONE);
        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });

        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });

        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openFiles();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 505);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }


//    private void openInternalStorage() {
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.setType("application/pdf|text/plain");
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//
//        try {
//            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 606);
//        } catch (android.content.ActivityNotFoundException ex) {
//            // Potentially direct the user to the Market with a Dialog
//            Toast.makeText(this, "Please install a File Manager.",
//                    Toast.LENGTH_SHORT).show();
//        }
//
//    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                String authorities = getApplicationContext().getPackageName() + "com.dharmaniapps.fileprovider";
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            }
        }
        startActivityForResult(takePictureIntent, CAMERA_REQUEST);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2296) {
            if (SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    // perform action when allow permission success
                } else {
                    Toast.makeText(this, "Allow permission for storage access!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            Bitmap bitmap;
                            Uri uri = data.getData();
                            File finalFile = new File(getRealPathFromURI(uri));

                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 0;

                            bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                          For COnvert And rotate image
                            ExifInterface exifInterface = null;
                            try {
                                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                            Matrix matrix = new Matrix();
                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(270);
                                    break;
                                case ExifInterface.ORIENTATION_NORMAL:
                                default:
                            }
                            JaoharConstants.IS_camera_Click = false;
                            thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                            imgImageIV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            imgImageIV.setImageBitmap(thumb);
//                            strIMAGE = ImageUtils.getInstant().getBase64FromBitmap(thumb);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "*****No Picture Selected*****");
                        return;
                    }
                } finally {
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                try {
                    JaoharConstants.IS_camera_Click = true;
                    thumb = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    imgImageIV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    imgImageIV.setImageBitmap(thumb);
//                    strIMAGE = ImageUtils.getInstant().getBase64FromBitmap(thumb);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == Constant.REQUEST_CODE_PICK_FILE && resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    showLoadingAlertDialog(mActivity, "JAOHAR", "Loading File Please wait...");
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            alertDialog.dismiss();
                        }
                    }, 3000);

                    final Handler handller = new Handler();
                    handller.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Uri uri = data.getData();
                                strDocumentPath = uri.getPath();
                                String size=getRealSizeFromUri(mActivity,uri);
                                String uriString = uri.toString();
//                                File myFile = new File(RealPathUtil.getRealPath(mActivity, data.getData()));
                                File myFile = new File(uriString);
//                                strDocumentPath= myFile.getAbsolutePath();
                                double sizeInBytes = Double.parseDouble(size);
                                double fileSizeInKB = sizeInBytes / 1024;
                                double fileSizeInMB = fileSizeInKB / 1024;
                                if (fileSizeInMB <= 10 && fileSizeInMB!=0) {

                                strDocumentName = null;

                                if (uriString.startsWith("content://")) {
                                    Cursor cursor = null;
                                    try {
                                        cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
                                        if (cursor != null && cursor.moveToFirst()) {
                                            strDocumentName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                        }
                                    } finally {
                                        cursor.close();
                                    }
                                } else if (uriString.startsWith("file://")) {
                                    strDocumentName = myFile.getName();
                                }
                                InputStream iStream = null;
                                if (isVirtualFile(uri)) {
                                    try {
                                        iStream = getInputStreamForVirtualFile(uri, "*/*");
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                                else {
                                    try {
                                        iStream = getContentResolver().openInputStream(uri);
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    } }
                                try {
                                    strByteArray = getBytes(iStream);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                DocumentModel mDocumentModel = new DocumentModel();
                                mDocumentModel.setDocumentPath(strDocumentPath);
                                mDocumentModel.setByteArray(strByteArray);
                                String extension = myFile.getPath().substring(myFile.getPath().lastIndexOf("."));
                                mDocumentModel.setDocumunetExtension(extension);
                                mDocumentModel.setDocumentName(strDocumentName);
                                if (documentArrayList.size() <= 10)
                                    documentArrayList.add(mDocumentModel);
                                setAdapter(documentArrayList);
                            } }catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 200);
                }}


//            if (requestCode == Constant.REQUEST_CODE_PICK_FILE && resultCode == Activity.RESULT_OK) {
//                if (data != null) {
//                    ArrayList<NormalFile> files = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
//                    InputStream iStream = null;
//                    for (int i = 0;i<files.size(); i++) {
//                        // Get the number of bytes in the file
//                        double sizeInBytes = files.get(i).getSize();
//                        double fileSizeInKB = sizeInBytes / 1024;
//                        double fileSizeInMB = fileSizeInKB / 1024;
//                        if (fileSizeInMB <= 10 && fileSizeInMB!=0) {
//                            DocumentModel mDocumentModel = new DocumentModel();
//                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                            try {
//                                iStream = new FileInputStream(new File(files.get(i).getPath()));
//                                byte[] buf = new byte[1024];
//                                int n;
//                                while (-1 != (n = iStream.read(buf)))
//                                    baos.write(buf, 0, n);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                            strByteArray = baos.toByteArray();
//
//                            mDocumentModel.setDocumentPath(files.get(i).getPath());
//                            mDocumentModel.setByteArray(strByteArray);
//                            String extension = files.get(i).getPath().substring(files.get(i).getPath().lastIndexOf("."));
//                                mDocumentModel.setDocumunetExtension(extension);
//                            mDocumentModel.setDocumentName(files.get(i).getName());
//
//                            if (documentArrayList.size() <= 10)
//                                documentArrayList.add(mDocumentModel);
//
//                        /* Set document in recycler View */
//                        setAdapter(documentArrayList);
//                    }}}
//                else {
//                        Log.e(TAG, "*****No Picture Selected****");
//                        return;
//                    }
//                        }
//
//            catch (Exception e) {
//                    e.printStackTrace();
//            }

//            if (requestCode == 505) {
//                Uri uri = data.getData();
//                String uriString = uri.toString();
//                File myFile = new File(uriString);
//                strDocumentPath= myFile.getAbsolutePath();
////                String name = uri.getLastPathSegment();
//////Here uri is your uri from which you want to get extension
////                docEx1 = name.substring(name.lastIndexOf("."));
//                strDocumentName = null;
//
//                if (uriString.startsWith("content://")) {
//                    Cursor cursor = null;
//
//                    try {
//                        cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
//                        if (cursor != null && cursor.moveToFirst()) {
//                            strDocumentName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
//                        }
//                    } finally {
//                        cursor.close();
//                    }
//                } else if (uriString.startsWith("file://")) {
//                    strDocumentName = myFile.getName();
//                }
//                InputStream iStream = null;
//                if (isVirtualFile(uri)) {
//                    try {
//                        iStream = getInputStreamForVirtualFile(uri, "*/*");
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//
////                Uri uri = data.getData();
////                strDocumentPath = uri.getPath();
////                strDocumentPath = strDocumentPath.replace(" ", "_");
////                strDocumentName = uri.getLastPathSegment();
//                else {
//                try {
//                    iStream = getContentResolver().openInputStream(uri);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } }
//                try {
//                    strByteArray = getBytes(iStream);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                DocumentModel mDocumentModel = new DocumentModel();
//                mDocumentModel.setId("11");
//                mDocumentModel.setDocumentName(strDocumentName);
//                mDocumentModel.setDocumunetExtension(docEx1);
//                mDocumentModel.setByteArray(strByteArray);
//                mDocumentModel.setmUri(uri);
//                mDocumentModel.setDocumentBase64(Utilities.convertByteArrayToBase64(strByteArray));
//                mDocumentModel.setDocumentPath(strDocumentPath);
//                documentArrayList.add(mDocumentModel);
//                setAdapter(documentArrayList);
//            }
//                Bitmap bitmap;
//                String strDocumentPath, strDocumentName;
//                Uri uri = data.getData();
//                strDocumentPath = uri.getPath();
//                strDocumentPath = strDocumentPath.replace(" ", "_");
//                strDocumentName = uri.getLastPathSegment();
//
//                File finalFile = new File(getRealPathFromURI(uri));
//
//                BitmapFactory.Options options = new BitmapFactory.Options();
//                options.inSampleSize = 0;
//
//                bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
////                 For COnvert And rotate image
//                ExifInterface exifInterface = null;
//                try {
//                    exifInterface = new ExifInterface(finalFile.getAbsolutePath());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
//                Matrix matrix = new Matrix();
//                switch (orientation) {
//                    case ExifInterface.ORIENTATION_ROTATE_90:
//                        matrix.setRotate(90);
//                        break;
//                    case ExifInterface.ORIENTATION_ROTATE_180:
//                        matrix.setRotate(180);
//                        break;
//                    case ExifInterface.ORIENTATION_ROTATE_270:
//                        matrix.setRotate(270);
//                        break;
//                    case ExifInterface.ORIENTATION_NORMAL:
//                    default:
//
//                }
//                JaoharConstants.IS_camera_Click = false;
//                thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
//                imgImageIV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//                imgImageIV.setImageBitmap(thumb);
//                strIMAGE = ImageUtils.getInstant().getBase64FromBitmap(thumb);


            if (requestCode == 606 && resultCode == Activity.RESULT_OK) {
                try {
                    Uri uri = data.getData();
                    strDocumentPath = uri.getPath();
                    strDocumentPath = strDocumentPath.replace(" ", "_");
                    strDocumentName = strDocumentPath.substring(strDocumentPath.lastIndexOf("/") + 1);
//                    String name = uri.getLastPathSegment();
//Here uri is your uri from which you want to get extension
//                    docEx1 = name.substring(name.lastIndexOf("."));
                    //strByteArray = Utilities.convertDocumentToByteArray(strDocumentPath);
                    InputStream iStream = getContentResolver().openInputStream(uri);
                    strByteArray = getBytes(iStream);
                    imgDOCIV.setImageDrawable(getResources().getDrawable(R.drawable.icon_doc));
                    imgDOCCrossIV.setVisibility(View.VISIBLE);
                    strDocBase64 = Utilities.convertByteArrayToBase64(strByteArray);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String getRealSizeFromUri(Context context, Uri uri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Audio.Media.SIZE };
            cursor = context.getContentResolver().query(uri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.SIZE);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private boolean isVirtualFile(Uri uri) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (!DocumentsContract.isDocumentUri(mActivity, uri)) {
                return false;
            }

            Cursor cursor = getContentResolver().query(
                    uri,
                    new String[]{DocumentsContract.Document.COLUMN_FLAGS},
                    null, null, null);
            int flags = 0;
            if (cursor.moveToFirst()) {
                flags = cursor.getInt(0);
            }
            cursor.close();
            return (flags & DocumentsContract.Document.FLAG_VIRTUAL_DOCUMENT) != 0;
        } else {
            return false;
        }
    }

    private InputStream getInputStreamForVirtualFile(Uri uri, String mimeTypeFilter)
            throws IOException {

        ContentResolver resolver =getContentResolver();

        String[] openableMimeTypes = resolver.getStreamTypes(uri, mimeTypeFilter);

        if (openableMimeTypes == null ||
                openableMimeTypes.length < 1) {
            throw new FileNotFoundException();
        }

        return resolver
                .openTypedAssetFileDescriptor(uri, openableMimeTypes[0], null)
                .createInputStream();
    }


    public void setAdapter(ArrayList<DocumentModel> mArrayList) {
        mDocumentRecyclerView.setNestedScrollingEnabled(false);
        mDocumentRecyclerView.setLayoutManager(new LinearLayoutManager(SendMailingListActivity.this));
        mAddDocumentsAdapter = new AddDocumentsAdapter(mActivity, mArrayList);
        mDocumentRecyclerView.setAdapter(mAddDocumentsAdapter);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /*
     * Converting Doc to byte Array
     * */
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024*1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }



    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        if (isAllList.equals("1")) {
            mMap.put("list_id", strListID);
        } else {
            mMap.put("list_ids", String.valueOf(mlistIDArray));
        }
        mMap.put("subject", editSubjectET.getText().toString());
        mMap.put("message", mRichEditText.getHtml().toString());
        mMap.put("head_one", editHeading_OneET.getText().toString());
        mMap.put("head_two", editHeading_TwoET.getText().toString());
        mMap.put("doc_name", "JaoharDOC");
        mMap.put("doc", strDocBase64);
        if (thumb != null) {
            mMap.put("image", strIMAGE.trim());
        } else {
            mMap.put("image", "");
        }
        if (strTypeOFUser.equals("manager")) {
            mMap.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        } else if (strTypeOFUser.equals("staff")) {
            mMap.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        }
        if (editSignaturesET.getText().toString().trim().contains("\n")) {
            String strSignature = "";
            String lastname = "";
            String[] brokenName = editSignaturesET.getText().toString().trim().split("\n");
            String firstname = brokenName[0];
            lastname = brokenName[1];
            strSignature = firstname + "<br>" + lastname;
            mMap.put("sign", strSignature);
        } else {
            mMap.put("sign", editSignaturesET.getText().toString());
        }
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    /* Execute api */

    public byte[] convertBitmapToByteArrayUncompressed(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        Log.e(TAG, "value--" + byteArray);
        return byteArray;
    }

    // generate dynamically string
    public String getAlphaNumericString() {
        int n = 20;

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        Log.e(TAG, "**********Image Name******" + sb.toString());
        return sb.toString();
    }


    private void executeMailToListRequest() {
//        AlertDialogManager.showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        if (isAllList.equals("1")) {
            list_id = RequestBody.create(MediaType.parse("multipart/form-data"), strListID);
        } else {
            list_ids = mlistIDArray;
        }
        subject = RequestBody.create(MediaType.parse("multipart/form-data"),editSubjectET.getText().toString());
        message = RequestBody.create(MediaType.parse("multipart/form-data"), mRichEditText.getHtml());
        head_one = RequestBody.create(MediaType.parse("multipart/form-data"),editHeading_OneET.getText().toString());
        head_two = RequestBody.create(MediaType.parse("multipart/form-data"),editHeading_TwoET.getText().toString());
        doc_name = RequestBody.create(MediaType.parse("multipart/form-data"),"JaoharDOC");
        if (thumb != null) {
            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(thumb));
            mMultipartBody1 = MultipartBody.Part.createFormData("image", getAlphaNumericString() + ".jpeg", requestFile1);
            Log.e("img", "img" + mMultipartBody1);

//            image = RequestBody.create(MediaType.parse("multipart/form-data"),strIMAGE.trim());
        } else {
            mMultipartBody1 = null;
            Log.e("img", "img" + mMultipartBody1);
//            image = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        }
        if (strTypeOFUser.equals("manager")) {
            user_id = RequestBody.create(MediaType.parse("multipart/form-data"),JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        } else if (strTypeOFUser.equals("staff")) {
            user_id = RequestBody.create(MediaType.parse("multipart/form-data"),JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        }
        if (editSignaturesET.getText().toString().trim().contains("\n")) {
            String strSignature = "";
            String lastname = "";
            String[] brokenName = editSignaturesET.getText().toString().trim().split("\n");
            String firstname = brokenName[0];
            lastname = brokenName[1];
            strSignature = firstname + "<br>" + lastname;
            sign = RequestBody.create(MediaType.parse("multipart/form-data"),strSignature);
        } else {
            sign = RequestBody.create(MediaType.parse("multipart/form-data"),editSignaturesET.getText().toString());
        }

        for (int i = 0; i < documentArrayList.size(); i++) {
            if (i == 0) {
                byteArrayDoc1 = documentArrayList.get(i).getByteArray();
                doc1 = documentArrayList.get(i).getDocumentName();
                docEx1 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 1) {
                byteArrayDoc2 = documentArrayList.get(i).getByteArray();
                doc2 = documentArrayList.get(i).getDocumentName();
                docEx2 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 2) {
                byteArrayDoc3 = documentArrayList.get(i).getByteArray();
                doc3 = documentArrayList.get(i).getDocumentName();
                docEx3 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 3) {
                byteArrayDoc4 = documentArrayList.get(i).getByteArray();
                doc4 = documentArrayList.get(i).getDocumentName();
                docEx4 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 4) {
                byteArrayDoc5 = documentArrayList.get(i).getByteArray();
                doc5 = documentArrayList.get(i).getDocumentName();
                docEx5 = documentArrayList.get(i).getDocumunetExtension();
            }
        }
        MultipartBody.Part mMultipartBodyDoc1 = null;
        MultipartBody.Part mMultipartBodyDoc2 = null;
        MultipartBody.Part mMultipartBodyDoc3 = null;
        MultipartBody.Part mMultipartBodyDoc4 = null;
        MultipartBody.Part mMultipartBodyDoc5 = null;

        if (doc1 != null) {
            if (doc1.contains("http")){
            RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc1);
            mMultipartBodyDoc1 = MultipartBody.Part.createFormData("doc0", doc1 , requestFileDoc1);
            Log.e("img", "img" + mMultipartBodyDoc1);}
            else{
                RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc1);
                mMultipartBodyDoc1 = MultipartBody.Part.createFormData("doc0", doc1+docEx1 , requestFileDoc1);
                Log.e("img", "img" + mMultipartBodyDoc1);
            }
        }
        if (doc2 != null) {
            if (doc2.contains("http")){
            RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc2);
            mMultipartBodyDoc2 = MultipartBody.Part.createFormData("doc1", doc2 , requestFileDoc2);
            Log.e("img", "img" + mMultipartBodyDoc2);}
            else{
                RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc2);
                mMultipartBodyDoc2 = MultipartBody.Part.createFormData("doc1", doc2 +docEx2, requestFileDoc2);
                Log.e("img", "img" + mMultipartBodyDoc2);
            }
        }
        if (doc3 != null) {
            if (doc3.contains("http")){
            RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc3);
            mMultipartBodyDoc3 = MultipartBody.Part.createFormData("doc2", doc3 , requestFileDoc3);
            Log.e("img", "img" + mMultipartBodyDoc3);}
            else{
                RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc3);
                mMultipartBodyDoc3 = MultipartBody.Part.createFormData("doc2", doc3+docEx3 , requestFileDoc3);
                Log.e("img", "img" + mMultipartBodyDoc3);
            }
        }
        if (doc4 != null) {
            if (doc4.contains("http")){
            RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc4);
            mMultipartBodyDoc4 = MultipartBody.Part.createFormData("doc3", doc4 , requestFileDoc4);
            Log.e("img", "img" + mMultipartBodyDoc4);}
            else{
                RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc4);
                mMultipartBodyDoc4 = MultipartBody.Part.createFormData("doc3", doc4+docEx4 , requestFileDoc4);
                Log.e("img", "img" + mMultipartBodyDoc4);
            }
        }
        if (doc5 != null) {
            if (doc5.contains("http")){
            RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc5);
            mMultipartBodyDoc5 = MultipartBody.Part.createFormData("doc4", doc5 , requestFileDoc5);
            Log.e("img", "img" + mMultipartBodyDoc5);}
            else{
                RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc5);
                mMultipartBodyDoc5 = MultipartBody.Part.createFormData("doc4", doc5+docEx5 , requestFileDoc5);
                Log.e("img", "img" + mMultipartBodyDoc5);
            }
        }


        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.mailToListIOSRequest(list_id,list_ids,subject,message,head_one,head_two,doc_name,mMultipartBody1,user_id,sign, mMultipartBodyDoc1, mMultipartBodyDoc2, mMultipartBodyDoc3, mMultipartBodyDoc4, mMultipartBodyDoc5).enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
//                         tell everybody you have succeed upload image and post strings
                        String strBaseURL = "";
                        Log.e(TAG, "Messsage******:" + message);
                        String SessionID = jsonObject.getString("data");
                        if (JaoharConstants.SERVER_URL.contains("Staging")) {
                            if (strTypeOFUser.equals("manager")) {
                                strBaseURL = "http://staging.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
                            } else if (strTypeOFUser.equals("staff")) {
                                strBaseURL = "http://staging.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
                            }
                        } else if (JaoharConstants.SERVER_URL.contains("Development")) {
                            if (strTypeOFUser.equals("manager")) {
                                strBaseURL = "https://dev.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
                            } else if (strTypeOFUser.equals("staff")) {
                                strBaseURL = "https://dev.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
                            }
                        } else {
                            if (strTypeOFUser.equals("manager")) {
                                strBaseURL = "https://office.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
                            } else if (strTypeOFUser.equals("staff")) {
                                strBaseURL = "https://office.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
                            }
                        }
                        Intent intent = new Intent(mActivity, webViewActivity.class);
                        intent.putExtra("news", strBaseURL);
                        startActivity(intent);
                        finish();

                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }

                } catch (JSONException e) {
                    AlertDialogManager.hideProgressDialog();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }

    private void executeMailToMultipleListRequest() {
//        AlertDialogManager.showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        if (isAllList.equals("1")) {
            list_id = RequestBody.create(MediaType.parse("multipart/form-data"), strListID);
        } else {
            list_ids = mlistIDArray;
//            descriptionList.add(String.valueOf(MultipartBody.Part.createFormData("list_ids", String.valueOf(mlistIDArray))));
//            list_ids = RequestBody.create(MediaType.parse("multipart/form-data"),  String.valueOf(mlistIDArray));
        }
        subject = RequestBody.create(MediaType.parse("multipart/form-data"),editSubjectET.getText().toString());
        message = RequestBody.create(MediaType.parse("multipart/form-data"), mRichEditText.getHtml());
        head_one = RequestBody.create(MediaType.parse("multipart/form-data"),editHeading_OneET.getText().toString());
        head_two = RequestBody.create(MediaType.parse("multipart/form-data"),editHeading_TwoET.getText().toString());
        doc_name = RequestBody.create(MediaType.parse("multipart/form-data"),"JaoharDOC");
        if (thumb != null) {
            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(thumb));
            mMultipartBody1 = MultipartBody.Part.createFormData("image", getAlphaNumericString() + ".jpeg", requestFile1);
            Log.e("img", "img" + mMultipartBody1);

//            image = RequestBody.create(MediaType.parse("multipart/form-data"),strIMAGE.trim());
        } else {
            mMultipartBody1 = null;
//            image = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        }
        if (strTypeOFUser.equals("manager")) {
            user_id = RequestBody.create(MediaType.parse("multipart/form-data"),JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        } else if (strTypeOFUser.equals("staff")) {
            user_id = RequestBody.create(MediaType.parse("multipart/form-data"),JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        }
        if (editSignaturesET.getText().toString().trim().contains("\n")) {
            String strSignature = "";
            String lastname = "";
            String[] brokenName = editSignaturesET.getText().toString().trim().split("\n");
            String firstname = brokenName[0];
            lastname = brokenName[1];
            strSignature = firstname + "<br>" + lastname;
            sign = RequestBody.create(MediaType.parse("multipart/form-data"),strSignature);
        } else {
            sign = RequestBody.create(MediaType.parse("multipart/form-data"),editSignaturesET.getText().toString());
        }

        for (int i = 0; i < documentArrayList.size(); i++) {
            if (i == 0) {
                byteArrayDoc1 = documentArrayList.get(i).getByteArray();
                doc1 = documentArrayList.get(i).getDocumentName();
                docEx1 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 1) {
                byteArrayDoc2 = documentArrayList.get(i).getByteArray();
                doc2 = documentArrayList.get(i).getDocumentName();
                docEx2 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 2) {
                byteArrayDoc3 = documentArrayList.get(i).getByteArray();
                doc3 = documentArrayList.get(i).getDocumentName();
                docEx3 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 3) {
                byteArrayDoc4 = documentArrayList.get(i).getByteArray();
                doc4 = documentArrayList.get(i).getDocumentName();
                docEx4 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 4) {
                byteArrayDoc5 = documentArrayList.get(i).getByteArray();
                doc5 = documentArrayList.get(i).getDocumentName();
                docEx5 = documentArrayList.get(i).getDocumunetExtension();
            }
        }

        MultipartBody.Part mMultipartBodyDoc1 = null;
        MultipartBody.Part mMultipartBodyDoc2 = null;
        MultipartBody.Part mMultipartBodyDoc3 = null;
        MultipartBody.Part mMultipartBodyDoc4 = null;
        MultipartBody.Part mMultipartBodyDoc5 = null;

        if (doc1 != null) {
            if (doc1.contains("http")){
                RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc1);
                mMultipartBodyDoc1 = MultipartBody.Part.createFormData("doc0", doc1 , requestFileDoc1);
                Log.e("img", "img" + mMultipartBodyDoc1);
            }
            else{
                RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc1);
                mMultipartBodyDoc1 = MultipartBody.Part.createFormData("doc0", doc1+docEx1 , requestFileDoc1);
                Log.e("img", "img" + mMultipartBodyDoc1);
            }

        }
        if (doc2 != null) {
            if (doc2.contains("http")){
            RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc2);
            mMultipartBodyDoc2 = MultipartBody.Part.createFormData("doc1", doc2 , requestFileDoc2);
            Log.e("img", "img" + mMultipartBodyDoc2);}
            else{
                RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc2);
                mMultipartBodyDoc2 = MultipartBody.Part.createFormData("doc1", doc2+docEx2 , requestFileDoc2);
                Log.e("img", "img" + mMultipartBodyDoc2);
            }
        }
        if (doc3 != null) {
            if (doc3.contains("http")){
            RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc3);
            mMultipartBodyDoc3 = MultipartBody.Part.createFormData("doc2", doc3, requestFileDoc3);
            Log.e("img", "img" + mMultipartBodyDoc3);}
            else{
                RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc3);
                mMultipartBodyDoc3 = MultipartBody.Part.createFormData("doc2", doc3+docEx3, requestFileDoc3);
                Log.e("img", "img" + mMultipartBodyDoc3);
            }
        }
        if (doc4 != null) {
            if (doc4.contains("http")){
            RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc4);
            mMultipartBodyDoc4 = MultipartBody.Part.createFormData("doc3", doc4, requestFileDoc4);
            Log.e("img", "img" + mMultipartBodyDoc4);}
            else{
                RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc4);
                mMultipartBodyDoc4 = MultipartBody.Part.createFormData("doc3", doc4+docEx4, requestFileDoc4);
                Log.e("img", "img" + mMultipartBodyDoc4);
            }
        }
        if (doc5 != null) {
            if (doc5.contains("http")){
            RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc5);
            mMultipartBodyDoc5 = MultipartBody.Part.createFormData("doc4", doc5, requestFileDoc5);
            Log.e("img", "img" + mMultipartBodyDoc5);}
            else{
                RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc5);
                mMultipartBodyDoc5 = MultipartBody.Part.createFormData("doc4", doc5+docEx5, requestFileDoc5);
                Log.e("img", "img" + mMultipartBodyDoc5);
            }
        }


        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.mailToMultipleListIOSRequest(list_id,list_ids,subject,message,head_one,head_two,doc_name,mMultipartBody1,user_id,sign, mMultipartBodyDoc1, mMultipartBodyDoc2, mMultipartBodyDoc3, mMultipartBodyDoc4, mMultipartBodyDoc5).enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
//                         tell everybody you have succed upload image and post strings
                        String strBaseURL = "";
                        Log.e(TAG, "Messsage******:" + message);
                        String SessionID = jsonObject.getString("data");
                        if (JaoharConstants.SERVER_URL.contains("Staging")) {
                            if (strTypeOFUser.equals("manager")) {
                                strBaseURL = "http://staging.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
                            } else if (strTypeOFUser.equals("staff")) {
                                strBaseURL = "http://staging.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
                            }
                        } else if (JaoharConstants.SERVER_URL.contains("Development")) {
                            if (strTypeOFUser.equals("manager")) {
                                strBaseURL = "https://dev.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
                            } else if (strTypeOFUser.equals("staff")) {
                                strBaseURL = "https://dev.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
                            }
                        }else {
                            if (strTypeOFUser.equals("manager")) {
                                strBaseURL = "https://office.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
                            } else if (strTypeOFUser.equals("staff")) {
                                strBaseURL = "https://office.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
                            }
                        }
                        Intent intent = new Intent(mActivity, webViewActivity.class);
                        intent.putExtra("news", strBaseURL);
                        startActivity(intent);
                        finish();

                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }

                } catch (JSONException e) {
                    AlertDialogManager.hideProgressDialog();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }


    private void executeMailToListBgRequest() {
//        AlertDialogManager.showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBodyDoc1 = null;
        MultipartBody.Part mMultipartBodyDoc2 = null;
        MultipartBody.Part mMultipartBodyDoc3 = null;
        MultipartBody.Part mMultipartBodyDoc4 = null;
        MultipartBody.Part mMultipartBodyDoc5 = null;

        if (isAllList.equals("1")) {
            list_id = RequestBody.create(MediaType.parse("multipart/form-data"), strListID);
        } else {
            list_ids = mlistIDArray;
//            list_ids = RequestBody.create(MediaType.parse("multipart/form-data"),  String.valueOf(mlistIDArray));
        }
        subject = RequestBody.create(MediaType.parse("multipart/form-data"),editSubjectET.getText().toString());
        message = RequestBody.create(MediaType.parse("multipart/form-data"), mRichEditText.getHtml());
        head_one = RequestBody.create(MediaType.parse("multipart/form-data"),editHeading_OneET.getText().toString());
        head_two = RequestBody.create(MediaType.parse("multipart/form-data"),editHeading_TwoET.getText().toString());
        doc_name = RequestBody.create(MediaType.parse("multipart/form-data"),"JaoharDOC");
        if (thumb != null) {
            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(thumb));
            mMultipartBody1 = MultipartBody.Part.createFormData("image", getAlphaNumericString() + ".jpeg", requestFile1);
            Log.e("img", "img" + mMultipartBody1);

//            image = RequestBody.create(MediaType.parse("multipart/form-data"),strIMAGE.trim());
        } else {
            mMultipartBody1 = null;
            Log.e("img", "img" + mMultipartBody1);
//            image = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        }
        if (strTypeOFUser.equals("manager")) {
            user_id = RequestBody.create(MediaType.parse("multipart/form-data"),JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        } else if (strTypeOFUser.equals("staff")) {
            user_id = RequestBody.create(MediaType.parse("multipart/form-data"),JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        }
        if (editSignaturesET.getText().toString().trim().contains("\n")) {
            String strSignature = "";
            String lastname = "";
            String[] brokenName = editSignaturesET.getText().toString().trim().split("\n");
            String firstname = brokenName[0];
            lastname = brokenName[1];
            strSignature = firstname + "<br>" + lastname;
            sign = RequestBody.create(MediaType.parse("multipart/form-data"),strSignature);
        } else {
            sign = RequestBody.create(MediaType.parse("multipart/form-data"),editSignaturesET.getText().toString());
        }

        for (int i = 0; i < documentArrayList.size(); i++) {
            if (i == 0) {
                byteArrayDoc1 = documentArrayList.get(i).getByteArray();
                doc1 = documentArrayList.get(i).getDocumentName();
                docEx1 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 1) {
                byteArrayDoc2 = documentArrayList.get(i).getByteArray();
                doc2 = documentArrayList.get(i).getDocumentName();
                docEx2 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 2) {
                byteArrayDoc3 = documentArrayList.get(i).getByteArray();
                doc3 = documentArrayList.get(i).getDocumentName();
                docEx3 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 3) {
                byteArrayDoc4 = documentArrayList.get(i).getByteArray();
                doc4 = documentArrayList.get(i).getDocumentName();
                docEx4 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 4) {
                byteArrayDoc5 = documentArrayList.get(i).getByteArray();
                doc5 = documentArrayList.get(i).getDocumentName();
                docEx5 = documentArrayList.get(i).getDocumunetExtension();
            }
        }
        if (doc1 != null) {
            if (doc1.contains("http")){
                RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc1);
                mMultipartBodyDoc1 = MultipartBody.Part.createFormData("doc0", doc1, requestFileDoc1);
                Log.e("img", "img" + mMultipartBodyDoc1);
            }
            else{
            RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc1);
            mMultipartBodyDoc1 = MultipartBody.Part.createFormData("doc0", doc1+docEx1, requestFileDoc1);
            Log.e("img", "img" + mMultipartBodyDoc1);
        }}
        if (doc2 != null) {
            if (doc2.contains("http")){
            RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc2);
            mMultipartBodyDoc2 = MultipartBody.Part.createFormData("doc1", doc2, requestFileDoc2);
            Log.e("img", "img" + mMultipartBodyDoc2);}
            else{
                RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc2);
                mMultipartBodyDoc2 = MultipartBody.Part.createFormData("doc1", doc2+docEx2, requestFileDoc2);
                Log.e("img", "img" + mMultipartBodyDoc2);
            }
        }
        if (doc3 != null) {
            if (doc3.contains("http")){
            RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc3);
            mMultipartBodyDoc3 = MultipartBody.Part.createFormData("doc2", doc3, requestFileDoc3);
            Log.e("img", "img" + mMultipartBodyDoc3);
        }
        else{
            RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc3);
            mMultipartBodyDoc3 = MultipartBody.Part.createFormData("doc2", doc3+docEx3, requestFileDoc3);
            Log.e("img", "img" + mMultipartBodyDoc3);
        }}
        if (doc4 != null) {
            if (doc4.contains("http")){
            RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc4);
            mMultipartBodyDoc4 = MultipartBody.Part.createFormData("doc3", doc4, requestFileDoc4);
            Log.e("img", "img" + mMultipartBodyDoc4);
        }
            else{
                RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc4);
                mMultipartBodyDoc4 = MultipartBody.Part.createFormData("doc3", doc4+docEx4, requestFileDoc4);
                Log.e("img", "img" + mMultipartBodyDoc4);
            }}
        if (doc5 != null) {
            if (doc5.contains("http")){
            RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc5);
            mMultipartBodyDoc5 = MultipartBody.Part.createFormData("doc4", doc5, requestFileDoc5);
            Log.e("img", "img" + mMultipartBodyDoc5);}
            else{
                RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc5);
                mMultipartBodyDoc5 = MultipartBody.Part.createFormData("doc4", doc5+docEx5, requestFileDoc5);
                Log.e("img", "img" + mMultipartBodyDoc5);
            }
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.mailToListIOSShellRequest(list_id,list_ids,subject,message,head_one,head_two,doc_name,mMultipartBody1,user_id,sign, mMultipartBodyDoc1, mMultipartBodyDoc2, mMultipartBodyDoc3, mMultipartBodyDoc4, mMultipartBodyDoc5).enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
                        showBgMailAlertDialog(mActivity,"Jaohar",message);
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }

                } catch (JSONException e) {
                    AlertDialogManager.hideProgressDialog();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }

    private void executeMailToMultipleListBgRequest() {
//        AlertDialogManager.showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        if (isAllList.equals("1")) {
            list_id = RequestBody.create(MediaType.parse("multipart/form-data"), strListID);
        } else {
            list_ids = mlistIDArray;
//            list_ids = RequestBody.create(MediaType.parse("multipart/form-data"),  String.valueOf(mlistIDArray));
        }
        subject = RequestBody.create(MediaType.parse("multipart/form-data"),editSubjectET.getText().toString());
        message = RequestBody.create(MediaType.parse("multipart/form-data"), mRichEditText.getHtml());
        head_one = RequestBody.create(MediaType.parse("multipart/form-data"),editHeading_OneET.getText().toString());
        head_two = RequestBody.create(MediaType.parse("multipart/form-data"),editHeading_TwoET.getText().toString());
        doc_name = RequestBody.create(MediaType.parse("multipart/form-data"),"JaoharDOC");
        if (thumb != null) {
            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(thumb));
            mMultipartBody1 = MultipartBody.Part.createFormData("image", getAlphaNumericString() + ".jpeg", requestFile1);
            Log.e("img", "img" + mMultipartBody1);

//            image = RequestBody.create(MediaType.parse("multipart/form-data"),strIMAGE.trim());
        } else {
            mMultipartBody1 = null;
//            image = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        }
        if (strTypeOFUser.equals("manager")) {
            user_id = RequestBody.create(MediaType.parse("multipart/form-data"),JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        } else if (strTypeOFUser.equals("staff")) {
            user_id = RequestBody.create(MediaType.parse("multipart/form-data"),JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        }
        if (editSignaturesET.getText().toString().trim().contains("\n")) {
            String strSignature = "";
            String lastname = "";
            String[] brokenName = editSignaturesET.getText().toString().trim().split("\n");
            String firstname = brokenName[0];
            lastname = brokenName[1];
            strSignature = firstname + "<br>" + lastname;
            sign = RequestBody.create(MediaType.parse("multipart/form-data"),strSignature);
        } else {
            sign = RequestBody.create(MediaType.parse("multipart/form-data"),editSignaturesET.getText().toString());
        }

        for (int i = 0; i < documentArrayList.size(); i++) {
            if (i == 0) {
                byteArrayDoc1 = documentArrayList.get(i).getByteArray();
                doc1 = documentArrayList.get(i).getDocumentName();
                docEx1 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 1) {
                byteArrayDoc2 = documentArrayList.get(i).getByteArray();
                doc2 = documentArrayList.get(i).getDocumentName();
                docEx2 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 2) {
                byteArrayDoc3 = documentArrayList.get(i).getByteArray();
                doc3 = documentArrayList.get(i).getDocumentName();
                docEx3 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 3) {
                byteArrayDoc4 = documentArrayList.get(i).getByteArray();
                doc4 = documentArrayList.get(i).getDocumentName();
                docEx4 = documentArrayList.get(i).getDocumunetExtension();
            } else if (i == 4) {
                byteArrayDoc5 = documentArrayList.get(i).getByteArray();
                doc5 = documentArrayList.get(i).getDocumentName();
                docEx5 = documentArrayList.get(i).getDocumunetExtension();
            }
        }

        MultipartBody.Part mMultipartBodyDoc1 = null;
        MultipartBody.Part mMultipartBodyDoc2 = null;
        MultipartBody.Part mMultipartBodyDoc3 = null;
        MultipartBody.Part mMultipartBodyDoc4 = null;
        MultipartBody.Part mMultipartBodyDoc5 = null;

        if (doc1 != null) {
            if (doc1.contains("http")){
            RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc1);
            mMultipartBodyDoc1 = MultipartBody.Part.createFormData("doc0", doc1, requestFileDoc1);
            Log.e("img", "img" + mMultipartBodyDoc1);}
            else{
                RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc1);
                mMultipartBodyDoc1 = MultipartBody.Part.createFormData("doc0", doc1+docEx1, requestFileDoc1);
                Log.e("img", "img" + mMultipartBodyDoc1);
            }
        }
        if (doc2 != null) {
            if (doc2.contains("http")){
            RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc2);
            mMultipartBodyDoc2 = MultipartBody.Part.createFormData("doc1", doc2, requestFileDoc2);
            Log.e("img", "img" + mMultipartBodyDoc2);}
            else{
                RequestBody requestFileDoc2 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc2);
                mMultipartBodyDoc2 = MultipartBody.Part.createFormData("doc1", doc2+docEx2, requestFileDoc2);
                Log.e("img", "img" + mMultipartBodyDoc2);
            }

        }
        if (doc3 != null) {
            if (doc3.contains("http")){
            RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc3);
            mMultipartBodyDoc3 = MultipartBody.Part.createFormData("doc2", doc3, requestFileDoc3);
            Log.e("img", "img" + mMultipartBodyDoc3);}
            else{
                RequestBody requestFileDoc3 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc3);
                mMultipartBodyDoc3 = MultipartBody.Part.createFormData("doc2", doc3+docEx3, requestFileDoc3);
                Log.e("img", "img" + mMultipartBodyDoc3);
            }
        }
        if (doc4 != null) {
            if (doc4.contains("http")){
            RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc4);
            mMultipartBodyDoc4 = MultipartBody.Part.createFormData("doc3", doc4, requestFileDoc4);
            Log.e("img", "img" + mMultipartBodyDoc4);
        }
        else{
                RequestBody requestFileDoc4 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc4);
                mMultipartBodyDoc4 = MultipartBody.Part.createFormData("doc3", doc4+docEx4, requestFileDoc4);
                Log.e("img", "img" + mMultipartBodyDoc4);}
        }
        if (doc5 != null) {
            if (doc5.contains("http")){
            RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc5);
            mMultipartBodyDoc5 = MultipartBody.Part.createFormData("doc4", doc5, requestFileDoc5);
            Log.e("img", "img" + mMultipartBodyDoc5);}
            else{
                RequestBody requestFileDoc5 = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayDoc5);
                mMultipartBodyDoc5 = MultipartBody.Part.createFormData("doc4", doc5+docEx5, requestFileDoc5);
                Log.e("img", "img" + mMultipartBodyDoc5);
            }
        }


        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.mailToMultipleListIOSShellRequest(list_id,list_ids,subject,message,head_one,head_two,doc_name,mMultipartBody1,user_id,sign, mMultipartBodyDoc1, mMultipartBodyDoc2, mMultipartBodyDoc3, mMultipartBodyDoc4, mMultipartBodyDoc5).enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
                        showBgMailAlertDialog(mActivity,"Jaohar",message);
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }

                } catch (JSONException e) {
                    AlertDialogManager.hideProgressDialog();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }


//    private void executeMailToListRequest() {
////        AlertDialogManager.showProgressDialog(mActivity);
//        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
//        mApiInterface.mailToListRequest(mParam()).enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject jsonObject = new JSONObject(response.body().toString());
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
////                         tell everybody you have succeed upload image and post strings
//                        String strBaseURL = "";
//                        Log.e(TAG, "Messsage******:" + message);
//                        String SessionID = jsonObject.getString("data");
//                        if (JaoharConstants.SERVER_URL.contains("Staging")) {
//                            if (strTypeOFUser.equals("manager")) {
//                                strBaseURL = "http://staging.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
//                            } else if (strTypeOFUser.equals("staff")) {
//                                strBaseURL = "http://staging.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
//                            }
//                        } else if (JaoharConstants.SERVER_URL.contains("Development")) {
//                            if (strTypeOFUser.equals("manager")) {
//                                strBaseURL = "https://dev.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
//                            } else if (strTypeOFUser.equals("staff")) {
//                                strBaseURL = "https://dev.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
//                            }
//                        } else {
//                            if (strTypeOFUser.equals("manager")) {
//                                strBaseURL = "https://office.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
//                            } else if (strTypeOFUser.equals("staff")) {
//                                strBaseURL = "https://office.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
//                            }
//                        }
//                        Intent intent = new Intent(mActivity, webViewActivity.class);
//                        intent.putExtra("news", strBaseURL);
//                        startActivity(intent);
//
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//                Log.e(TAG, "**ERROR**" + t.getMessage());
//                AlertDialogManager.hideProgressDialog();
//            }
//        });
//    }
//


    public void sendEamilAPI() {
        JaoharConstants.mArrayLISTDATA.clear();
        preventMultipleViewClick();
//        String strAPIUrl = "";

//        JSONObject jsonObject = new JSONObject();
        if (isAllList.equals("1")) {
            final Handler handller = new Handler();
            handller.postDelayed(new Runnable() {
                @Override
                public void run() {
                    executeMailToListRequest();
                }
            }, 200);

//            strAPIUrl = JaoharConstants.Mail_To_List;
        } else {
            final Handler handller = new Handler();
            handller.postDelayed(new Runnable() {
                @Override
                public void run() {
                    executeMailToMultipleListRequest();
                }
            }, 200);

//            strAPIUrl = JaoharConstants.Mail_To_Multiple_List;
        }
    }

    public void sendEamilInBgAPI() {
        JaoharConstants.mArrayLISTDATA.clear();
//        if (isAllList.equals("1")) {
            if (isAllList.equals("1")) {
                final Handler handller = new Handler();
                handller.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        executeMailToListBgRequest();
                    }
                }, 200);


//            strAPIUrl = JaoharConstants.Mail_To_List;
        } else {
                final Handler handller = new Handler();
                handller.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        executeMailToMultipleListBgRequest();
                    }
                }, 200);

            }

//            strAPIUrl = JaoharConstants.Mail_To_Multiple_List;
//        }
    }

//    private void executeMailToMultipleListRequest() {
//        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
//        mApiInterface.mailToMultipleListRequest(mParam()).enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject jsonObject = new JSONObject(response.body().toString());
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
////                         tell everybody you have succed upload image and post strings
//                        String strBaseURL = "";
//                        Log.e(TAG, "Messsage******:" + message);
//                        String SessionID = jsonObject.getString("data");
//                        if (JaoharConstants.SERVER_URL.contains("Staging")) {
//                            if (strTypeOFUser.equals("manager")) {
//                                strBaseURL = "http://staging.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
//                            } else if (strTypeOFUser.equals("staff")) {
//                                strBaseURL = "http://staging.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
//                            }
//                        } else if (JaoharConstants.SERVER_URL.contains("Development")) {
//                            if (strTypeOFUser.equals("manager")) {
//                                strBaseURL = "https://dev.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
//                            } else if (strTypeOFUser.equals("staff")) {
//                                strBaseURL = "https://dev.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
//                            }
//                        }else {
//                            if (strTypeOFUser.equals("manager")) {
//                                strBaseURL = "https://office.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
//                            } else if (strTypeOFUser.equals("staff")) {
//                                strBaseURL = "https://office.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
//                            }
//                        }
//                        Intent intent = new Intent(mActivity, webViewActivity.class);
//                        intent.putExtra("news", strBaseURL);
//                        startActivity(intent);
//
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//                Log.e(TAG, "**ERROR**" + t.getMessage());
//                AlertDialogManager.hideProgressDialog();
//            }
//        });
//    }


//    private void sendEamilAPI() {
//        JaoharConstants.mArrayLISTDATA.clear();
//        String strAPIUrl = "";
//        AlertDialogManager.showProgressDialog(mActivity);
//        JSONObject jsonObject = new JSONObject();
//        if (isAllList.equals("1")) {
//            strAPIUrl = JaoharConstants.Mail_To_List;
//        } else {
//            strAPIUrl = JaoharConstants.Mail_To_Multiple_List;
//        }
//
//        try {
//            if (isAllList.equals("1")) {
//                jsonObject.put("list_id", strListID);
//            } else {
//                jsonObject.put("list_ids", mlistIDArray);
//            }
//
//            jsonObject.put("subject", editSubjectET.getText().toString());
//            jsonObject.put("message", mRichEditText.getHtml().toString());
//            jsonObject.put("head_one", editHeading_OneET.getText().toString());
//            jsonObject.put("head_two", editHeading_TwoET.getText().toString());
//            jsonObject.put("doc_name", "JaoharDOC.pdf");
//            jsonObject.put("doc", strDocBase64);
//            if (thumb != null) {
//                jsonObject.put("image", strIMAGE.trim());
//            } else {
//                jsonObject.put("image", "");
//            }
//            if (strTypeOFUser.equals("manager")) {
//                jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//            } else if (strTypeOFUser.equals("staff")) {
//                jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//            }
//            if (editSignaturesET.getText().toString().trim().contains("\n")) {
//                String strSignature = "";
//                String lastname = "";
//                String[] brokenName = editSignaturesET.getText().toString().trim().split("\n");
//                String firstname = brokenName[0];
//                lastname = brokenName[1];
//                strSignature = firstname + "<br>" + lastname;
//                jsonObject.put("sign", strSignature);
//            } else {
//                jsonObject.put("sign", editSignaturesET.getText().toString());
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
////                         tell everybody you have succed upload image and post strings
//                        String strBaseURL = "";
//                        Log.e(TAG, "Messsage******:" + message);
//                        String SessionID = jsonObject.getString("data");
//                        if (JaoharConstants.SERVER_URL.contains("Staging")) {
//                            if (strTypeOFUser.equals("manager")) {
//                                strBaseURL = "http://staging.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
//                            } else if (strTypeOFUser.equals("staff")) {
//                                strBaseURL = "http://staging.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
//                            }
//                        } else {
//                            if (strTypeOFUser.equals("manager")) {
//                                strBaseURL = "https://office.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "/" + SessionID;
//                            } else if (strTypeOFUser.equals("staff")) {
//                                strBaseURL = "https://office.jaohar.com/index.php/Mailer/group_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
//                            }
//                        }
//                        Intent intent = new Intent(mActivity, webViewActivity.class);
//                        intent.putExtra("news", strBaseURL);
//                        startActivity(intent);
//
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + error.toString());
//
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void showAlertDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void showBgMailAlertDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }
}
