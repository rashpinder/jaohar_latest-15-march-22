package jaohar.com.jaohar.activities.trash_module;

import android.app.Activity;

import android.os.Bundle;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.RelativeLayout;
import android.widget.TextView;


import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.fragments.ManageUtilitiesFragment;
import jaohar.com.jaohar.fragments.VesselInTrashFragment;
import jaohar.com.jaohar.utils.JaoharConstants;

public class TrashActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */

    private Activity mActivity = TrashActivity.this;
    /**
     * Widgets
     */

    LinearLayout llLeftLL, vesselTrashViewLL, manageInvoiceviewLL;
    RelativeLayout manageUtilitiesRL, vesselTrashRL;
    ImageView imgBack;
    TextView txtCenter, vesselTrashTV, manageInvoiceTV;
    String strIsClickFrom = "vesselOPEN";


    /**
     * Activity @Override method
     * #onCreate
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trash);
    }

    /**
     * Binding variables with views from layout
     */


    @Override
    protected void setViewsIDs() {
        imgBack = findViewById(R.id.imgBack);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        manageUtilitiesRL = findViewById(R.id.manageUtilitiesRL);
        vesselTrashRL = findViewById(R.id.vesselTrashRL);
        vesselTrashTV = findViewById(R.id.vesselTrashTV);
        vesselTrashViewLL = findViewById(R.id.vesselTrashViewLL);
        manageInvoiceTV = findViewById(R.id.manageInvoiceTV);
        manageInvoiceviewLL = findViewById(R.id.manageInvoiceviewLL);
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        txtCenter.setText(getResources().getString(R.string.trash));
        // Getting Clicked Value From Intent
        if (getIntent() != null) {
            if (getIntent().getStringExtra("isClick").equals("manageUtilities")) {
                strIsClickFrom = getIntent().getStringExtra("isClick");
            }
        }
        if (strIsClickFrom.equals("manageUtilities")) {
            vesselTrashTV.setTextColor(getResources().getColor(R.color.black));
            manageInvoiceTV.setTextColor(getResources().getColor(R.color.colorPrimary));
            manageInvoiceviewLL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            vesselTrashViewLL.setBackgroundColor(getResources().getColor(R.color.white));
            switchFragment((FragmentActivity) mActivity, new ManageUtilitiesFragment(), JaoharConstants.Manage_Utilities_Fragment, false, null);

        } else {
            vesselTrashTV.setTextColor(getResources().getColor(R.color.colorPrimary));
            manageInvoiceTV.setTextColor(getResources().getColor(R.color.black));
            vesselTrashViewLL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            manageInvoiceviewLL.setBackgroundColor(getResources().getColor(R.color.white));
            switchFragment((FragmentActivity) mActivity, new VesselInTrashFragment(), JaoharConstants.VESSEL_IN_TRASH, false, null);

        }

    }


    /**
     * Setting up screen events
     */

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        manageUtilitiesRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vesselTrashTV.setTextColor(getResources().getColor(R.color.black));
                manageInvoiceTV.setTextColor(getResources().getColor(R.color.colorPrimary));
                manageInvoiceviewLL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                vesselTrashViewLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new ManageUtilitiesFragment(), JaoharConstants.Manage_Utilities_Fragment, false, null);

            }
        });

        vesselTrashRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vesselTrashTV.setTextColor(getResources().getColor(R.color.colorPrimary));
                manageInvoiceTV.setTextColor(getResources().getColor(R.color.black));
                vesselTrashViewLL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                manageInvoiceviewLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new VesselInTrashFragment(), JaoharConstants.VESSEL_IN_TRASH, false, null);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /********
     *Replace Fragment In Activity
     **********/
    public static void switchFragment(FragmentActivity activity, Fragment fragment, String TAG, boolean addToStack, Bundle bundle) {
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.layoutContainerLL, fragment, TAG);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }
}
