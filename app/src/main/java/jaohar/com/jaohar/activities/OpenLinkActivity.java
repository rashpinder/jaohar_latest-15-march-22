
package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;

public class OpenLinkActivity extends BaseActivity {
    //Toolbar
    public LinearLayout llLeftLL;
    public RelativeLayout imgRightLL;
    public TextView txtCenter;
    public ImageView imgRight, imgBack;
    public WebView mWebView;
    Activity mActivity = OpenLinkActivity.this;
    String TAG = OpenLinkActivity.this.getClass().getSimpleName();
    ProgressBar circlePB;

    String strTitle = "";
    String strLink = "";
//    String mPDF = "", mPDFName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_link);
        setStatusBar();

        PRDownloader.initialize(getApplicationContext());

        if (getIntent() != null) {
            strTitle = getIntent().getStringExtra("TITLE");
            strLink = getIntent().getStringExtra("LINK");
        }
        mDownloadPDFMethod(strLink,strTitle);

    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    private void saveFileUsingMediaStore(Context context, String url, String fileName) {
//        String extStorageDirectory = Environment.getExternalStorageDirectory()
//                .toString();
//        File folder = new File(extStorageDirectory, "pdf");
        String path = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getPath() + "/jaohar");
//        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/jaoharr");
        File folder = new File(path);


        folder.mkdir();
        File file = new File(folder, fileName);
        try {
            file.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        Downloader.DownloadFile(url, file);
    }


    public static class Downloader {

        public static void DownloadFile(String fileURL, File directory) {
            try {
                FileOutputStream f = new FileOutputStream(directory);
                URL u = new URL(fileURL);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
                f.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }




//}

    /* get path of pdf */
    private String outputPath() {
        String path = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getPath()
                + "/jaohar");



        //String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
//        String path = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());

        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdirs();
        } else {
//            folder.delete();
//            folder.mkdirs();
        }
        return path;
    }

    private void downloadPDF(String url, String dirPath, String fileName) {
        int downloadId = PRDownloader.download(url, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        Log.d("PRDOWNLOADER", "Download completed at::::" + dirPath);
                        hideProgressDialog();
                        showPDFAlertDialog(mActivity, getResources().getString(R.string.app_name), "Downloaded Successfully!");
                    }

                    @Override
                    public void onError(Error error) {
                        Log.d("PRDOWNLOADER", "Download failed at::::" + error.getServerErrorMessage());
                        hideProgressDialog();
                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), "PDF format is not correct!");
                    }
                });
    }


    /*
     * Show Alert Dailog Box
     * */
    public void showPDFAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    @Override
    protected void setViewsIDs() {
        /*Toolbar*/
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.GONE);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtCenter.setText(strTitle);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setColorFilter(ContextCompat.getColor(mActivity, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
        imgBack.setImageResource(R.drawable.back);
        mWebView = (WebView) findViewById(R.id.mWebView);
        circlePB = (ProgressBar) findViewById(R.id.circlePB);

        String GoogleDocs = "http://docs.google.com/gview?embedded=true&url=";

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                circlePB.setVisibility(View.VISIBLE);
//                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                circlePB.setVisibility(View.GONE);
            }
        });

//        mWebView.loadUrl(GoogleDocs + strLink);

    }

    private void mDownloadPDFMethod(String mPDF, String mPDFName) {
        // write the document content
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            new AsyncCaller().execute();
//        } else {
//            showProgressDialog(mActivity);
//            if (strLink.contains(".pdf")|strLink.contains(".xlsx")|strLink.contains(".rtf")|strLink.contains(".txt")|strLink.contains(".xls")|strLink.contains(".doc")|
//                    (strLink.contains(".ppt")|strLink.contains(".pptx")|strLink.contains(".mp4")|strLink.contains(".mp3"))){
                downloadPDF(strLink,outputPath(),strTitle);
//    }
//            else{
//                mWebView.loadUrl(strLink);
//            }
        }


//    private class AsyncCaller extends AsyncTask<Void, Void, Void> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            //this method will be running on UI thread
//            showProgressDialog(mActivity);
//        }
//
//        @RequiresApi(api = Build.VERSION_CODES.Q)
//        @Override
//        protected Void doInBackground(Void... params) {
//
//            //this method will be running on background thread so don't update UI frome here
//            //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here
//            saveFileUsingMediaStore(mActivity, strLink, strTitle);
//
//            return null;
//        }}



    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}