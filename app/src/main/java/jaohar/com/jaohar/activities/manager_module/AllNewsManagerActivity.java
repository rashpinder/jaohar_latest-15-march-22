package jaohar.com.jaohar.activities.manager_module;

import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddNewsActivity;
import jaohar.com.jaohar.activities.EditNewsActivity;
import jaohar.com.jaohar.activities.GalleryActivity;
import jaohar.com.jaohar.adapters.manager_module.AllNewsManagerAdapter;
import jaohar.com.jaohar.beans.NewsModel;
import jaohar.com.jaohar.interfaces.DeleteNewsInterface;
import jaohar.com.jaohar.interfaces.EditNewsInterface;
import jaohar.com.jaohar.interfaces.OpenNewsPopUpInterFace;
import jaohar.com.jaohar.models.AllNewsModel;
import jaohar.com.jaohar.models.Datum;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;
import jaohar.com.jaohar.views.PinchRecyclerView;
import jaohar.com.jaohar.views.ZoomLayout;
import retrofit2.Call;
import retrofit2.Callback;

public class AllNewsManagerActivity extends BaseActivity {
    Activity mActivity = AllNewsManagerActivity.this;
    String TAG = AllNewsManagerActivity.this.getClass().getSimpleName();
    //WIDGETS
    ImageView imgBack;
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    ImageView imgRight;
    ZoomLayout zoomLayout;
    boolean isOpenDialogBOX = false;
    TextView txtCenter;
    PinchRecyclerView newsRV;
    ArrayList<String> mImageArryList = new ArrayList<>();

//    ArrayList<AllNewsModel> mNewsArrayList = new ArrayList<AllNewsModel>();
    ArrayList<Datum> mNewsArrayList = new ArrayList<Datum>();
    OpenNewsPopUpInterFace mOpenNewsPopUP = new OpenNewsPopUpInterFace() {
        @Override
        public void openNewsPopUpInterFace(String strWebTxt, String strPhotoURL) {
            if (isOpenDialogBOX == true) {
                isOpenDialogBOX = false;
            } else {
                setUpNewsDialog(strWebTxt, strPhotoURL);
            }

        }
    };

    EditNewsInterface mEditNewsInterface = new EditNewsInterface() {
        @Override
        public void mEditNews(Datum mNewsModel) {
            Intent mIntent = new Intent(mActivity, EditNewsActivity.class);
            mIntent.putExtra("Model", mNewsModel);
            startActivity(mIntent);
        }
    };

    DeleteNewsInterface mDeleteNewsInterface = new DeleteNewsInterface() {
        @Override
        public void mDeleteNews(Datum mNewsModel, int position) {
            deleteConfirmDialog(mNewsModel,position);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_all_news_manager);
    }

    public void setUpNewsDialog(String strNormalTEXT, final String strPhotoURL) {

        final Dialog searchDialog = new Dialog(mActivity);
        isOpenDialogBOX = true;
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.activity_full_news_description);
//        searchDialog.setContentView(R.layout.dialog_new_details);
        searchDialog.setCanceledOnTouchOutside(true);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        ImageView add_image1 = (ImageView) searchDialog.findViewById(R.id.add_image1);
        WebView showTXT = (WebView) searchDialog.findViewById(R.id.showTXT);
        final ProgressBar progressbar1 = searchDialog.findViewById(R.id.progressbar1);
        progressbar1.setVisibility(View.VISIBLE);
        if (!strPhotoURL.equals("")) {
            Glide.with(mActivity).load(strPhotoURL).into(add_image1);
        }
        add_image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!strPhotoURL.equals("")) {
                    mImageArryList.clear();
                    mImageArryList.add(strPhotoURL);
                    Intent intent = new Intent(mActivity, GalleryActivity.class);
                    intent.putStringArrayListExtra("LIST", mImageArryList);
                    startActivity(intent);
                }
            }
        });
        showTXT.getSettings().setJavaScriptEnabled(true);
        showTXT.getSettings().setLoadWithOverviewMode(true);
        showTXT.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progressbar1.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                progressbar1.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                progressbar1.setVisibility(View.GONE);
            }
        });

        showTXT.loadDataWithBaseURL(null, strNormalTEXT, "text/html", "UTF-8", null);
        WebSettings webSettings = showTXT.getSettings();
        Resources res = mActivity.getResources();
        webSettings.setDefaultFontSize((int) res.getDimension(R.dimen._3sdp));
        searchDialog.show();
    }

    @Override
    protected void setViewsIDs() {
        /*SET UP TOOLBAR*/
        imgBack = (ImageView) findViewById(R.id.imgBack);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgRightLL.setVisibility(View.VISIBLE);
        imgRight.setVisibility(View.VISIBLE);
        imgRight.setImageResource(R.drawable.plus_symbol);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        newsRV = (PinchRecyclerView) findViewById(R.id.newsRV);

        /* set tool bar */
        txtCenter.setText(getString(R.string.all_news));
        imgRight.setImageResource(R.drawable.add_icon);
        imgBack.setImageResource(R.drawable.back);

        if (JaoharConstants.IS_SEE_ALL_NEWS_CLICK == true) {
            imgRightLL.setVisibility(View.GONE);
        } else if (JaoharConstants.IS_CLICK_FROM_VESSELS_FOR_SALE == true) {
            imgRightLL.setVisibility(View.GONE);
        } else if (JaoharConstants.IS_ACTIVITY_CLICK == true) {
            imgRightLL.setVisibility(View.GONE);
        }

        if (!Utilities.isNetworkAvailable(mActivity)) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            executeGettingAllNews();
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AddNewsActivity.class));
                overridePendingTransitionEnter();
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (JaoharConstants.IS_CLICK_FROM_VESSELS_FOR_SALE == true) {
            super.onBackPressed();
            JaoharConstants.IS_CLICK_FROM_VESSELS_FOR_SALE = false;
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mIntent.putExtra(JaoharConstants.LOGIN, "VesselForSale");
            mActivity.startActivity(mIntent);
            finish();
            overridePendingTransitionExit();
        } else {
            super.onBackPressed();
            JaoharConstants.IS_SEE_ALL_NEWS_CLICK = false;
            JaoharConstants.IS_ACTIVITY_CLICK = false;
            finish();
            overridePendingTransitionExit();
        }

    }

    private void setAdapter() {
        newsRV.setNestedScrollingEnabled(false);
        newsRV.setLayoutManager(new LinearLayoutManager(mActivity));
        AllNewsManagerAdapter mNewsAdapter = new AllNewsManagerAdapter(mActivity, mNewsArrayList, mEditNewsInterface, mDeleteNewsInterface, mOpenNewsPopUP);
        newsRV.setAdapter(mNewsAdapter);
    }

    private void executeGettingAllNews() {
        mNewsArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<AllNewsModel> call1 = mApiInterface.getallNewsRequest();
        call1.enqueue(new Callback<AllNewsModel>() {
            @Override
            public void onResponse(Call<AllNewsModel> call, retrofit2.Response<AllNewsModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                AllNewsModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    mNewsArrayList=mModel.getData();
//                    mNewsArrayList=mModel.getData();
                    /*setAdapter*/
                    setAdapter();}
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AllNewsModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}


//    private void executeGettingAllNews() {
//        mNewsArrayList.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strAPIUrl = JaoharConstants.GET_ALL_NEWS;
//        StringRequest mStringRequest = new StringRequest(Request.Method.GET, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        JSONArray mArray = mJsonObject.getJSONArray("data");
//                        for (int i = 0; i < mArray.length(); i++) {
//                            JSONObject mJson = mArray.getJSONObject(i);
//                            NewsModel mModel = new NewsModel();
//                            mModel.setCreated_at(mJson.getString("created_at"));
//                            mModel.setId(mJson.getString("id"));
//                            mModel.setNews(mJson.getString("news"));
//                            mModel.setType(mJson.getString("type"));
//                            mModel.setPhoto(mJson.getString("photo"));
//                            mModel.setNews_text(mJson.getString("news_text"));
//                            mNewsArrayList.add(mModel);
//                        }
//                        /*setAdapter*/
//                        setAdapter();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(mStringRequest);
//    }

    public void deleteConfirmDialog(final Datum mNewsModel,int position) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_news));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mNewsModel,position);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    private void executeDeleteAPI(Datum newsModel, int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteNewsRequest(newsModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                }
                else if (mModel.getStatus()==0) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

//    public void executeDeleteAPI(NewsModel newsModel) {
//        String strUrl = JaoharConstants.DELETE_NEWS + "?news_id=" + newsModel.getId();
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAlertDialog(mActivity, getTitle().toString(), mJsonObject.getString("message"));
//                    } else if (mJsonObject.getString("status").equals("0")) {
//                        showAlertDialog(mActivity, getTitle().toString(), mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                executeGettingAllNews();
            }
        });
        alertDialog.show();
    }
}