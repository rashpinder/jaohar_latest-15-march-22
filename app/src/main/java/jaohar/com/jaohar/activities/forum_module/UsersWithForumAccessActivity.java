package jaohar.com.jaohar.activities.forum_module;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.forum_module.UsersWithForumAdapter;
import jaohar.com.jaohar.beans.ForumModule.ForumUserTypeModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class UsersWithForumAccessActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = UsersWithForumAccessActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = UsersWithForumAccessActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    LinearLayout llLeftLL;
    TextView txtCenter, txtRight;
    ImageView imgImageIV, imgBack;
    RecyclerView usersRV;

    /*
     * Array List And Models
     * */
    ArrayList<ForumUserTypeModel> mArrayListData = new ArrayList<>();

    UsersWithForumAdapter mUsersWithForumAdapter;
    String strType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_users_with_forum_access);
    }

    /**
     * Set Widget IDS
     **/
    @Override
    protected void setViewsIDs() {
        imgBack = findViewById(R.id.imgBack);
        imgImageIV = findViewById(R.id.imgImageIV);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        usersRV = findViewById(R.id.usersRV);
        txtRight = findViewById(R.id.txtRight);

        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        txtCenter.setText(getResources().getString(R.string.users_with_forum_access));
        txtRight.setText(getString(R.string.done));
        txtRight.setAllCaps(false);
        txtRight.setTextColor(getResources().getColor(R.color.blue));

        /*
         * Retrieve data from previous Activity
         * */
        if (getIntent() != null) {
            strType = getIntent().getStringExtra("type");
        }

        /*
         * Execute getting Api of User Email List
         * */
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            mArrayListData.clear();
            executeGettingUserAPI();
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txtRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    /**
     * Implemented API to Add forum data in Server @AddStaffForum
     *
     * @params
     * @user_id
     * @subject
     * @photo
     **/
    private void executeGettingUserAPI() {
        String strAPIUrl = "";
        AlertDialogManager.showProgressDialog(mActivity);
        if (strType.equals("UKadmin")) {
           executeUkAdminApi();
        } else if (strType.equals("UKstaff")) {
            executeUkStaffListApi();
        } else {
            executeStaffListApi();
        }
    }

    private void executeStaffListApi() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getUserListForumStaffRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "==Response==" + response);
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    AlertDialogManager.hideProgressDialog();
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
                        parseResponce(jsonObject);
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void executeUkStaffListApi() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getUserListForumUKStaffRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "==Response==" + response);
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    AlertDialogManager.hideProgressDialog();
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
                        parseResponce(jsonObject);
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void executeUkAdminApi() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getUserListForumAdminRequest(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "==Response==" + response);
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    AlertDialogManager.hideProgressDialog();
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
                        parseResponce(jsonObject);
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


//    private void executeGettingUserAPI() {
//        String strAPIUrl = "";
//        AlertDialogManager.showProgressDialog(mActivity);
//        if (strType.equals("UKadmin")) {
//            strAPIUrl = JaoharConstants.GetUserListForumAdmin;
//        } else if (strType.equals("UKstaff")) {
//            strAPIUrl = JaoharConstants.GetUserListForum;
//        } else {
//            strAPIUrl = JaoharConstants.GetUserListStaffForum;
//        }
//
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "==Response==" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        parseResponce(jsonObject);
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "***Error**" + error.toString());
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
//
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                HashMap<String, String> params = new HashMap<>();
//                if (strType.equals("staff")) {
//                    params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//
//                } else if (strType.equals("UKstaff")) {
//                    params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//
//                } else {
//                    params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//
//                }
//                return params;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    /*
     * Parse Data And Set It on Spinner Adapter
     * */
    private void parseResponce(JSONObject jsonObject) {
        JSONArray mjsonArrayData = null;
        try {
//            ForumUserTypeModel mModel1 = new ForumUserTypeModel();
//            mModel1.setEmail("Choose Users With Forum Access");
//            mArrayListData.add(mModel1);
            mjsonArrayData = jsonObject.getJSONArray("users");
            for (int i = 0; i < mjsonArrayData.length(); i++) {
                JSONObject mJsonData = mjsonArrayData.getJSONObject(i);
                ForumUserTypeModel mModel = new ForumUserTypeModel();
                if (!mJsonData.getString("id").equals("")) {
                    mModel.setId(mJsonData.getString("id"));
                }
                if (!mJsonData.getString("email").equals("")) {
                    mModel.setEmail(mJsonData.getString("email"));
                }
                if (!mJsonData.getString("image").equals("")) {
                    mModel.setImage(mJsonData.getString("image"));
                }
                if (!mJsonData.getString("image").equals("")) {
                    mModel.setImage(mJsonData.getString("image"));
                }
                if (!mJsonData.getString("first_name").equals("")) {
                    mModel.setFirstname(mJsonData.getString("first_name"));
                }
                if (!mJsonData.getString("last_name").equals("")) {
                    mModel.setLastname(mJsonData.getString("last_name"));
                }
                mArrayListData.add(mModel);
            }

            /*
             * SetUp Adapter
             * */
            setAdapter(mArrayListData);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setAdapter(ArrayList<ForumUserTypeModel> mArrayList) {
        usersRV.setNestedScrollingEnabled(false);
        usersRV.setLayoutManager(new LinearLayoutManager(UsersWithForumAccessActivity.this));
        mUsersWithForumAdapter = new UsersWithForumAdapter(mActivity, mArrayList);
        usersRV.setAdapter(mUsersWithForumAdapter);
    }
}
