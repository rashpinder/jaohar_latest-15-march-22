package jaohar.com.jaohar.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.InvoiceDetailsItemsAdapter;
import jaohar.com.jaohar.beans.AddTotalInvoiceDiscountModel;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.beans.PaymentModel;
import jaohar.com.jaohar.beans.PriviewModelItems;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class InvoiceDetailsActivity extends BaseActivity {
    public final int REQUEST_PERMISSIONS = 1;
    Activity mActivity = InvoiceDetailsActivity.this;
    String TAG = InvoiceDetailsActivity.this.getClass().getSimpleName();
    LinearLayout rootLinearLayoutLL, llLeftLL, mainLL1, swiftLL, gbpLL;
    NestedScrollView mNestedScrollView;
    RelativeLayout imgRightLL;
    Bitmap mGeneratedPDFBitmap = null;
    String strPDFPath = "", strInvoiceNumber = "", strInvoiceVesselName = "", strCompanyName = "";
    boolean boolean_permission;
    boolean boolean_save;
    /*PDF Widgets*/
    TextView txtCompanyName,txtCenter, txtCompanyAddress2, txtCompanyAddress3, txtCompanyAddress4, txtCompanyAddress5, txtCompanyAddress, txtInVoiceNumberValueTV, txtDateValueTV, txtDueValueTV, txtRefrenceValue,
            txtCurrencyValueTV, txtSubTotalValueTV, txtVATPercentTV, txtVatValueTV, txtTotalValueTV, txtPaidValueTV, txtBalanceDueValueTV,
            txtBenificiaryValueTV, txtBankNameTV, txtCurrenCyGBPTV, txtAddress1TV, txtCurrencyRONTV, txtCurrencyUSDTV, txtCurrencyEURTV, txtSwiftCodeTV, txtRefrence1, txtRefrence2, txtRefrence3, txtAddress2TV;
    LinearLayout usdLL, eurLL, ronLL;
    ImageView imgTopStatusIV, imgSigneIV;
    jaohar.com.jaohar.views.CircleImageView imgStampIV;
    RecyclerView rvItemsRowsRV;
    ProgressBar progress1;
    /*Invoice Model*/
    InVoicesModel mInVoicesModel;
    InVoicesModel mModel;
    /*Adapter*/
    InvoiceDetailsItemsAdapter mInvoiceDetailsItemsAdapter;
    //Invoice Items ArrayList
    ArrayList<InvoiceAddItemModel> mInvoiceItemArrayList = new ArrayList<InvoiceAddItemModel>();
    ArrayList<PriviewModelItems> mPriviewModelItemArray = new ArrayList<PriviewModelItems>();
    ArrayList<PriviewModelItems> mPriviewModelItemArrayNew = new ArrayList<PriviewModelItems>();

    ArrayList<AddTotalInvoiceDiscountModel> mArrayTotalDiscount = new ArrayList<AddTotalInvoiceDiscountModel>();
    Button btnEdit, btnMail;
    //PDF File Path
    String mStoragePath = "";
    private String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_details);

        setStatusBar();

        if (getIntent() != null) {
            mInVoicesModel = (InVoicesModel) getIntent().getSerializableExtra("Model");
        }

        /*Execute Getting All Data About Selected Invoice Post*/
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {

        }

        if (checkPermission()) {
            boolean_permission = true;
        } else {
            requestPermission();
        }
        executeGettingDataApi();
    }

    @Override
    protected void setViewsIDs() {
        mNestedScrollView = findViewById(R.id.mNestedScrollView);
        txtCenter = findViewById(R.id.txtCenter);
        rootLinearLayoutLL = findViewById(R.id.rootLinearLayoutLL);
        llLeftLL = findViewById(R.id.llLeftLL);
        mainLL1 = findViewById(R.id.mainLL1);
        swiftLL = findViewById(R.id.swiftLL);
        gbpLL = findViewById(R.id.gbpLL);
        imgRightLL = findViewById(R.id.imgRightLL);
        txtCompanyName = findViewById(R.id.txtCompanyName);
        txtCompanyAddress2 = findViewById(R.id.txtCompanyAddress2);
        txtCompanyAddress3 = findViewById(R.id.txtCompanyAddress3);
        txtCompanyAddress4 = findViewById(R.id.txtCompanyAddress4);
        txtCompanyAddress5 = findViewById(R.id.txtCompanyAddress5);
        txtCompanyAddress = findViewById(R.id.txtCompanyAddress);
        txtInVoiceNumberValueTV = findViewById(R.id.txtInVoiceNumberValueTV);
        txtDateValueTV = findViewById(R.id.txtDateValueTV);
        txtDueValueTV = findViewById(R.id.txtDueValueTV);
        txtRefrenceValue = findViewById(R.id.txtRefrenceValue);
        txtCurrencyValueTV = findViewById(R.id.txtCurrencyValueTV);
        txtBenificiaryValueTV = findViewById(R.id.txtBenificiaryValueTV);
        txtBankNameTV = findViewById(R.id.txtBankNameTV);
        txtAddress1TV = findViewById(R.id.txtAddress1TV);
        txtAddress2TV = findViewById(R.id.txtAddress2TV);
        txtCurrenCyGBPTV = findViewById(R.id.txtCurrenCyGBPTV);
        txtCurrencyRONTV = findViewById(R.id.txtCurrencyRONTV);
        txtCurrencyUSDTV = findViewById(R.id.txtCurrencyUSDTV);
        txtCurrencyEURTV = findViewById(R.id.txtCurrencyEURTV);
        txtSwiftCodeTV = findViewById(R.id.txtSwiftCodeTV);
        txtSubTotalValueTV = findViewById(R.id.txtSubTotalValueTV);
        txtVATPercentTV = findViewById(R.id.txtVATPercentTV);
        txtVatValueTV = findViewById(R.id.txtVatValueTV);
        txtTotalValueTV = findViewById(R.id.txtTotalValueTV);
        txtPaidValueTV = findViewById(R.id.txtPaidValueTV);
        txtBalanceDueValueTV = findViewById(R.id.txtBalanceDueValueTV);
        imgTopStatusIV = findViewById(R.id.imgTopStatusIV);
        imgStampIV = findViewById(R.id.imgStampIV);
        imgSigneIV = findViewById(R.id.imgSigneIV);
        usdLL = findViewById(R.id.usdLL);
        eurLL = findViewById(R.id.eurLL);
        ronLL = findViewById(R.id.ronLL);
        progress1 = findViewById(R.id.progress1);
        txtRefrence1 = findViewById(R.id.txtRefrence1);
        txtRefrence2 = findViewById(R.id.txtRefrence2);
        txtRefrence3 = findViewById(R.id.txtRefrence3);
        btnEdit = findViewById(R.id.btnEdit);
        btnMail = findViewById(R.id.btnMail);
        txtCenter.setText(mInVoicesModel.getInvoice_number());
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, EditInvoiceActivity.class);
                mIntent.putExtra("Model", mModel);
                mActivity.startActivity(mIntent);
                overridePendingTransitionEnter();
            }
        });

        btnMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Send Email*/
                Intent mIntent = new Intent(mActivity, SendingInvoiceEmailActivity.class);
                mIntent.putExtra("SubjectName", "Invoice    " + strInvoiceNumber + strInvoiceVesselName);
                mIntent.putExtra("vesselID", mInVoicesModel.getInvoice_id());
                startActivity(mIntent);
            }
        });
    }

    private void sendEmail(String strFilePath) {
        Uri imageURI = null;
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", new File(strFilePath));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            imageURI = Uri.fromFile(new File(strFilePath));
        }
        Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:")); // it's not ACTION_SEND
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Invoice Details: ");
//      intent.putExtra(Intent.EXTRA_TEXT, "Ops (Jaohar UK Limited)\n"+"Invoice Details PDF:\n"+"Hello Please find the attached pdf:");
        intent.putExtra(Intent.EXTRA_TEXT, "Please find attached requested invoice");
        intent.putExtra(Intent.EXTRA_STREAM, imageURI);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (grantResults.length > 0) {
                    boolean writeStorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean readStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (writeStorage && readStorage) {
                        boolean_permission = true;
                    } else {
                        Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_LONG).show();
                        requestPermission();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (JaoharConstants.IS_INVOICE_BACK) {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private void executeGettingDataApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.editInvoiceDataRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), JaoharConstants.Invoice_ID);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    if (mJsonObject.getString("status").equals("1")) {
                        setPrevousDatatoEditonWidgets(mJsonObject);
                    } else if (mJsonObject.getString("status").equals("100")) {
                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
                    } else if (mJsonObject.getString("status").equals("0")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void setPrevousDatatoEditonWidgets(JSONObject mJsonObject) {
        try {
            JSONObject mJson = mJsonObject.getJSONObject("data");
            JSONObject mAllDataObj = mJson.getJSONObject("all_data");
            mModel = new InVoicesModel();
            if (!mAllDataObj.isNull("invoice_id"))
                mModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
            JaoharConstants.Invoice_ID = mAllDataObj.getString("invoice_id");
            if (!mAllDataObj.isNull("invoice_no"))
                mModel.setInvoice_number(mAllDataObj.getString("invoice_no"));
            strInvoiceNumber = "JAORO" + mAllDataObj.getString("invoice_no");
            if (!mAllDataObj.isNull("invoice_date"))
                mModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
            if (!mAllDataObj.isNull("term_days"))
                mModel.setTerm_days(mAllDataObj.getString("term_days"));
            if (!mAllDataObj.isNull("currency"))
                mModel.setCurrency(mAllDataObj.getString("currency"));
            if (!mAllDataObj.isNull("status"))
                mModel.setStatus(mAllDataObj.getString("status"));
            if (!mAllDataObj.isNull("reference"))
                mModel.setRefrence1(mAllDataObj.getString("reference"));
            if (!mAllDataObj.isNull("reference1"))
                mModel.setRefrence2(mAllDataObj.getString("reference1"));
            if (!mAllDataObj.isNull("reference2"))
                mModel.setRefrence3(mAllDataObj.getString("reference2"));
            /*Company Model*/
            /*Search Vessel Model*/
            if (mJson.has("sign_data") && !mJson.getString("sign_data").equals("")) {
                JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                SignatureModel mSignModel = new SignatureModel();
                if (!mSignDataObj.isNull("sign_id"))
                    mSignModel.setId(mSignDataObj.getString("sign_id"));
                if (!mSignDataObj.isNull("sign_name"))
                    mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                if (!mSignDataObj.isNull("sign_image"))
                    mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                mModel.setmSignatureModel(mSignModel);
            }
            if (mJson.has("stamp_data") && !mJson.getString("stamp_data").equals("")) {
                JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                StampsModel mStampsModel = new StampsModel();
                if (!mStampDataObj.isNull("stamp_id"))
                    mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                if (!mStampDataObj.isNull("stamp_name"))
                    mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                if (!mStampDataObj.isNull("stamp_image"))
                    mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                mModel.setmStampsModel(mStampsModel);

            }
            if (mJson.has("bank_data") && !mJson.getString("bank_data").equals("")) {
                JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                BankModel mBankModel = new BankModel();
                if (!mBankDataObj.isNull("bank_id"))
                    mBankModel.setId(mBankDataObj.getString("bank_id"));
                if (!mBankDataObj.isNull("beneficiary"))
                    mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                if (!mBankDataObj.isNull("bank_name"))
                    mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                if (!mBankDataObj.isNull("address1"))
                    mBankModel.setAddress1(mBankDataObj.getString("address1"));
                if (!mBankDataObj.isNull("address2"))
                    mBankModel.setAddress2(mBankDataObj.getString("address2"));
                if (!mBankDataObj.isNull("iban_ron"))
                    mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                if (!mBankDataObj.isNull("iban_usd"))
                    mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                if (!mBankDataObj.isNull("iban_eur"))
                    mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                if (!mBankDataObj.isNull("iban_gbp"))
                    mBankModel.setIban_gbp(mBankDataObj.getString("iban_gbp"));
                if (!mBankDataObj.isNull("swift"))
                    mBankModel.setSwift(mBankDataObj.getString("swift"));
                mModel.setmBankModel(mBankModel);
            }

            if (mJson.has("search_vessel_data") && !mJson.getString("search_vessel_data").equals("")) {
                JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                if (!mSearchVesselObj.isNull("vessel_id"))
                    mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                if (!mSearchVesselObj.isNull("vessel_name"))
                    mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                strInvoiceVesselName = "   " + mSearchVesselObj.getString("vessel_name");
                if (!mSearchVesselObj.isNull("IMO_no"))
                    mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                if (!mSearchVesselObj.isNull("flag"))
                    mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
            }

            if (mJson.has("search_company_data") && !mJson.getString("search_company_data").equals("")) {
                JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                CompaniesModel mCompaniesModel = new CompaniesModel();
                if (!mSearchCompanyObj.isNull("id"))
                    mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                if (!mSearchCompanyObj.isNull("company_name"))
                    mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                strCompanyName = "  " + mSearchCompanyObj.getString("company_name");
                if (!mSearchCompanyObj.isNull("Address1"))
                    mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                if (!mSearchCompanyObj.isNull("Address2"))
                    mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                if (!mSearchCompanyObj.isNull("Address3"))
                    mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                if (!mSearchCompanyObj.isNull("Address4"))
                    mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                if (!mSearchCompanyObj.isNull("Address5"))
                    mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                mModel.setmCompaniesModel(mCompaniesModel);
            }

            if (mJson.has("payment_data") && !mJson.getString("payment_data").equals("")) {

                JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                PaymentModel mPaymentModel = new PaymentModel();
                if (!mPaymentObject.isNull("payment_id"))
                    mPaymentModel.setPayment_id(mPaymentObject.getString("payment_id"));
                if (!mPaymentObject.isNull("sub_total"))
                    mPaymentModel.setSubTotal(mPaymentObject.getString("sub_total"));
                if (!mPaymentObject.isNull("VAT"))
                    mPaymentModel.setVAT(mPaymentObject.getString("VAT"));
                if (!mPaymentObject.isNull("vat_price"))
                    mPaymentModel.setVATPrice(mPaymentObject.getString("vat_price"));
                if (!mPaymentObject.isNull("total"))
                    mPaymentModel.setTotal(mPaymentObject.getString("total"));
                if (!mPaymentObject.isNull("paid"))
                    mPaymentModel.setPaid(mPaymentObject.getString("paid"));
                if (!mPaymentObject.isNull("due"))
                    mPaymentModel.setBalanceDue(mPaymentObject.getString("due"));
                mModel.setmPaymentModel(mPaymentModel);
            }

            if (mJson.has("items_data") && !mJson.getString("items_data").equals("")) {
                mInvoiceItemArrayList.clear();

                JSONArray mItemArray = mJson.getJSONArray("items_data");
                for (int i = 0; i < mItemArray.length(); i++) {

                    InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                    JSONObject mItemObj = mItemArray.getJSONObject(i);

                    if (!mItemObj.getString("item_id").equals("")) {
                        mItemModel.setItemID(mItemObj.getString("item_id"));
                    }

                    if (!mItemObj.getString("item_serial_no").equals("")) {
                        mItemModel.setItem(mItemObj.getString("item_serial_no"));
                    }
                    if (!mItemObj.getString("quantity").equals("")) {
                        mItemModel.setQuantity(mItemObj.getInt("quantity"));
                    }
                    if (!mItemObj.getString("price").equals("")) {
                        mItemModel.setUnitprice(mItemObj.getString("price"));
                    }
                    if (!mItemObj.getString("description").equals("")) {
                        mItemModel.setDescription(mItemObj.getString("description"));
                    }
                    if (!mItemObj.getString("TotalunitPrice").equals("")) {
                        mItemModel.setStrTotalunitPrice(mItemObj.getString("TotalunitPrice"));
                    }
                    if (!mItemObj.getString("TotalAmount").equals("")) {
                        mItemModel.setTotalAmount(mItemObj.getString("TotalAmount"));
                    }
                    if (!mItemObj.getString("TotalAmountUnit").equals("")) {
                        mItemModel.setStrTotalAmountUnit(mItemObj.getString("TotalAmountUnit"));
                    }
                    if (!mItemObj.getString("arraylistDiscount").equals("")) {
                        ArrayList<DiscountModel> mDiscountModelArrayList = new ArrayList<DiscountModel>();
                        JSONArray mItemDiscountArray = mItemObj.getJSONArray("arraylistDiscount");

                        for (int k = 0; k < mItemDiscountArray.length(); k++) {
                            DiscountModel mDiscountModel = new DiscountModel();
                            JSONObject mItemObJDiscount = mItemDiscountArray.getJSONObject(k);

                            if (!mItemObJDiscount.getString("ADD_items").equals("")) {
                                mDiscountModel.setADD_items(mItemObJDiscount.getString("ADD_items"));
                            }
                            if (!mItemObJDiscount.getString("ADD_quantity").equals("")) {
                                mDiscountModel.setADD_quantity(mItemObJDiscount.getString("ADD_quantity"));
                            }
                            if (!mItemObJDiscount.getString("Add_description").equals("")) {
                                mDiscountModel.setAdd_description(mItemObJDiscount.getString("Add_description"));
                            }
                            if (!mItemObJDiscount.getString("ADD_unitPrice").equals("")) {
                                mDiscountModel.setADD_unitPrice(mItemObJDiscount.getString("ADD_unitPrice"));
                            }
                            if (!mItemObJDiscount.getString("Add_Value").equals("")) {
                                mDiscountModel.setAdd_Value(mItemObJDiscount.getString("Add_Value"));
                            }
                            if (!mItemObJDiscount.getString("Type").equals("")) {
                                mDiscountModel.setType(mItemObJDiscount.getString("Type"));
                            }
                            if (!mItemObJDiscount.getString("Subtract_items").equals("")) {
                                mDiscountModel.setSubtract_items(mItemObJDiscount.getString("Subtract_items"));
                            }
                            if (!mItemObJDiscount.getString("Subtract_quantity").equals("")) {
                                mDiscountModel.setSubtract_quantity(mItemObJDiscount.getString("Subtract_quantity"));
                            }
                            if (!mItemObJDiscount.getString("Subtract_description").equals("")) {
                                mDiscountModel.setSubtract_description(mItemObJDiscount.getString("Subtract_description"));
                            }
                            if (!mItemObJDiscount.getString("Subtract_Value").equals("")) {
                                mDiscountModel.setSubtract_Value(mItemObJDiscount.getString("Subtract_Value"));
                            }
                            if (!mItemObJDiscount.getString("Subtract_unitPrice").equals("")) {
                                mDiscountModel.setSubtract_unitPrice(mItemObJDiscount.getString("Subtract_unitPrice"));
                            }
                            if (!mItemObJDiscount.getString("addAndSubtractTotal").equals("")) {
                                mDiscountModel.setAddAndSubtractTotal(mItemObJDiscount.getString("addAndSubtractTotal"));
                            }
                            if (!mItemObJDiscount.getString("totalPercentValue").equals("")) {
                                mDiscountModel.setStrtotalPercentValue(mItemObJDiscount.getString("totalPercentValue"));
                            }
                            mDiscountModelArrayList.add(mDiscountModel);
                        }
                        mItemModel.setmDiscountModelArrayList(mDiscountModelArrayList);
                    }
                    mInvoiceItemArrayList.add(mItemModel);
                    mModel.setmItemModelArrayList(mInvoiceItemArrayList);
                }

            }
            if (mJson.has("itemdiscount") && !mJson.getString("itemdiscount").equals("")) {

                mArrayTotalDiscount.clear();
                JSONArray mItemDiscountArray = mJson.getJSONArray("itemdiscount");
                for (int k = 0; k < mItemDiscountArray.length(); k++) {
                    AddTotalInvoiceDiscountModel mTotalDiscountModel = new AddTotalInvoiceDiscountModel();
                    JSONObject mItemObJDiscount = mItemDiscountArray.getJSONObject(k);

                    if (!mItemObJDiscount.getString("discount_id").equals("")) {
                        mTotalDiscountModel.setDiscount_id(mItemObJDiscount.getString("discount_id"));
                    }
                    if (!mItemObJDiscount.getString("discount_subtract_description").equals("")) {
                        mTotalDiscountModel.setSubtract_Description(mItemObJDiscount.getString("discount_subtract_description"));
                    }
                    if (!mItemObJDiscount.getString("discount_subtract_value").equals("")) {
                        mTotalDiscountModel.setSubtract_DiscountValue(mItemObJDiscount.getString("discount_subtract_value"));
                    }
                    if (!mItemObJDiscount.getString("discount_subtract_unitprice").equals("")) {
                        mTotalDiscountModel.setSubtract_UnitPrice(Double.parseDouble(mItemObJDiscount.getString("discount_subtract_unitprice")));
                    }
                    if (!mItemObJDiscount.getString("discount_add_value").equals("")) {
                        mTotalDiscountModel.setADD_DiscountValue(mItemObJDiscount.getString("discount_add_value"));
                    }
                    if (!mItemObJDiscount.getString("discount_add_description").equals("")) {
                        mTotalDiscountModel.setADD_Description(mItemObJDiscount.getString("discount_add_description"));
                    }
                    if (!mItemObJDiscount.getString("discount_add_unitprice").equals("")) {

                        mTotalDiscountModel.setADD_UnitPrice(Double.parseDouble(mItemObJDiscount.getString("discount_add_unitprice")));
                    }
                    if (!mItemObJDiscount.getString("discount_total_value").equals("")) {
                        mTotalDiscountModel.setStrTotalvalue(mItemObJDiscount.getString("discount_total_value"));
                    }
                    if (!mItemObJDiscount.getString("discount_type").equals("")) {
                        mTotalDiscountModel.setDiscountType(mItemObJDiscount.getString("discount_type"));
                    }
                    if (!mItemObJDiscount.getString("discount_percent").equals("")) {
                        mTotalDiscountModel.setStrAddAndSubtracttotalPercent(mItemObJDiscount.getString("discount_percent"));
                    }
                    if (!mItemObJDiscount.getString("discount_add_and_subtract_total").equals("")) {
                        mTotalDiscountModel.setAddAndSubtractTotal(Double.parseDouble(mItemObJDiscount.getString("discount_add_and_subtract_total")));
                    }
                    mArrayTotalDiscount.add(mTotalDiscountModel);
                }
            }
            if (mModel.getmCompaniesModel() != null) {
                txtCompanyName.setText(mModel.getmCompaniesModel().getCompany_name());
                strCompanyName = "-" + mModel.getmCompaniesModel().getCompany_name();
                if (mModel.getmCompaniesModel().getAddress1().length() > 0) {
                    String strAddress2 = mModel.getmCompaniesModel().getAddress1();
                    txtCompanyAddress.setText(mModel.getmCompaniesModel().getAddress1());
                }

                if (mModel.getmCompaniesModel().getAddress1().length() > 0 && mModel.getmCompaniesModel().getAddress2().length() > 0) {
                    String strAddress12 = mModel.getmCompaniesModel().getAddress2();

                    txtCompanyAddress2.setText(mModel.getmCompaniesModel().getAddress2());
                }

                if (mModel.getmCompaniesModel().getAddress1().length() > 0 && mModel.getmCompaniesModel().getAddress2().length() > 0 && mModel.getmCompaniesModel().getAddress3().length() > 0) {
                    String strAddress12 = mModel.getmCompaniesModel().getAddress3();

                    txtCompanyAddress3.setText(mModel.getmCompaniesModel().getAddress3());
                }

                if (mModel.getmCompaniesModel().getAddress1().length() > 0 && mModel.getmCompaniesModel().getAddress2().length() > 0 && mModel.getmCompaniesModel().getAddress3().length() > 0 && mModel.getmCompaniesModel().getAddress4().length() > 0) {
                    String strAddress12 = mModel.getmCompaniesModel().getAddress4();

                    txtCompanyAddress4.setText(mModel.getmCompaniesModel().getAddress4());
                }

                if (mModel.getmCompaniesModel().getAddress1().length() > 0 && mModel.getmCompaniesModel().getAddress2().length() > 0 && mModel.getmCompaniesModel().getAddress3().length() > 0 && mModel.getmCompaniesModel().getAddress4().length() > 0 && mModel.getmCompaniesModel().getAddress5().length() > 0) {
                    String strAddress12 = mModel.getmCompaniesModel().getAddress5();

                    txtCompanyAddress5.setText(mModel.getmCompaniesModel().getAddress5());
                }


            }

            txtInVoiceNumberValueTV.setText("JAORO" + mModel.getInvoice_number());
            txtDateValueTV.setText(Utilities.gettingFormatTime(mModel.getInvoice_date()));

            String strDueDate = Utilities.getNewDateFormatWhenAddDays(mModel.getInvoice_date(), Integer.parseInt(mModel.getTerm_days()));
            txtDueValueTV.setText(Utilities.gettingFormatTime(strDueDate));
            if (mModel.getmVesselSearchInvoiceModel() != null) {
                txtRefrenceValue.setText("M/V " + mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_name() + ", IMO " + mInVoicesModel.getmVesselSearchInvoiceModel().getIMO_no() + ", Flag " + mInVoicesModel.getmVesselSearchInvoiceModel().getFlag());

            }
            txtRefrence1.setText(mModel.getRefrence1());
            txtRefrence2.setText(mModel.getRefrence2());
            txtRefrence3.setText(mModel.getRefrence3());

            txtCurrencyValueTV.setText(mModel.getCurrency());
            if (mModel.getmBankModel() != null) {

            }
            if (mModel.getmBankModel() != null) {
                txtBenificiaryValueTV.setText(mModel.getmBankModel().getBenificiary());
                txtBankNameTV.setText(mModel.getmBankModel().getBankName());
                txtAddress1TV.setText(mModel.getmBankModel().getAddress1());
                txtAddress2TV.setText(mModel.getmBankModel().getAddress2());
                if (mModel.getmBankModel().getIban_gbp() != null && mModel.getmBankModel().getIban_gbp().length() > 0) {
                    txtCurrenCyGBPTV.setText(mModel.getmBankModel().getIban_gbp());
                } else {
                    gbpLL.setVisibility(View.GONE);
                }

            }

            if (mModel.getmBankModel() != null) {
                if (mModel.getmBankModel().getIbanUSD().length() > 0) {
                    usdLL.setVisibility(View.VISIBLE);
                    txtCurrencyUSDTV.setText(mModel.getmBankModel().getIbanUSD());
                } else {
                    usdLL.setVisibility(View.GONE);
                }
            } else {
                usdLL.setVisibility(View.GONE);
            }
            if (mModel.getmBankModel() != null) {
                if (mModel.getmBankModel().getIbanEUR().length() > 0) {
                    eurLL.setVisibility(View.VISIBLE);
                    txtCurrencyEURTV.setText(mModel.getmBankModel().getIbanEUR());
                } else {
                    eurLL.setVisibility(View.GONE);
                }
            } else {
                eurLL.setVisibility(View.GONE);
            }
            if (mModel.getmBankModel() != null) {
                if (mModel.getmBankModel().getIbanRON().length() > 0) {
                    ronLL.setVisibility(View.VISIBLE);
                    txtCurrencyRONTV.setText(mModel.getmBankModel().getIbanRON());
                } else {
                    ronLL.setVisibility(View.GONE);
                }
            } else {
                ronLL.setVisibility(View.GONE);
            }

            if (mModel.getmBankModel() != null) {
                if (mModel.getmBankModel().getSwift() != null && mModel.getmBankModel().getSwift().length() > 0) {
                    txtSwiftCodeTV.setText(mModel.getmBankModel().getSwift());
                } else {
                    swiftLL.setVisibility(View.GONE);
                }

            }

            double mSubTotal = 0;
            double intItems = 0;
            double intUnitPrice = 0;
            double mAmount = 0;

            if (mInvoiceItemArrayList.size() != 0) {
                mPriviewModelItemArray.clear();
                for (int i = 0; i < mInvoiceItemArrayList.size(); i++) {
                    PriviewModelItems mModel = new PriviewModelItems();
                    double intItems1 = mInvoiceItemArrayList.get(i).getQuantity();
                    double intUnitPrice1 = Double.parseDouble(mInvoiceItemArrayList.get(i).getUnitprice());
                    double mAmount1 = intItems1 * intUnitPrice1;
                    mModel.setStrAmount(String.valueOf(mAmount1));
                    mModel.setStrDescription(mInvoiceItemArrayList.get(i).getDescription());
                    mModel.setStrItem(mInvoiceItemArrayList.get(i).getItem());
                    mModel.setStrQuantity(String.valueOf(mInvoiceItemArrayList.get(i).getQuantity()));
                    mModel.setStrUnitPrice(String.valueOf(mInvoiceItemArrayList.get(i).getUnitprice()));
                    mPriviewModelItemArray.add(mModel);
                    for (int k = 0; k < mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().size(); k++) {
                        String strType = mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getType();
                        if (strType.equals("ADD")) {
                            PriviewModelItems mModel1 = new PriviewModelItems();
                            mModel.setStrItem("w");
                            mModel1.setStrQuantity("1");
                            mModel1.setStrUnitPrice(mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                            mModel1.setStrAmount(mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                            mModel1.setStrDescription(mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());
                            mPriviewModelItemArray.add(mModel1);
                        } else if (strType.equals("ADD_VALUE")) {
                            PriviewModelItems mModel1 = new PriviewModelItems();
                            mModel.setStrItem("w");
                            mModel1.setStrQuantity("1");
                            mModel1.setStrUnitPrice(mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                            mModel1.setStrAmount(mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                            mModel1.setStrDescription(mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());
                            mPriviewModelItemArray.add(mModel1);
                        } else if (strType.equals("SUBTRACT")) {
                            PriviewModelItems mModel1 = new PriviewModelItems();
                            mModel.setStrItem("w");
                            mModel1.setStrQuantity("1");
                            mModel1.setStrUnitPrice("-" + mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                            mModel1.setStrAmount("-" + mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                            mModel1.setStrDescription(mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                            mPriviewModelItemArray.add(mModel1);
                        } else if (strType.equals("SUBTRACT_VALUE")) {
                            PriviewModelItems mModel1 = new PriviewModelItems();
                            mModel.setStrItem("w");
                            mModel1.setStrQuantity("1");
                            mModel1.setStrUnitPrice("-" + mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                            mModel1.setStrAmount("-" + mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                            mModel1.setStrDescription(mInvoiceItemArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                            mPriviewModelItemArray.add(mModel1);
                        }
                    }
                }
                if (mArrayTotalDiscount.size() != 0) {
                    for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                        String strType = mArrayTotalDiscount.get(i).getDiscountType();
                        if (strType.equals("Add Percentage")) {
                            PriviewModelItems mModel = new PriviewModelItems();
                            mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                            mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                            mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                            mModel.setStrItem("w");
                            mModel.setStrQuantity("1");
                            mPriviewModelItemArray.add(mModel);
                        } else if (strType.equals("Add Value")) {
                            PriviewModelItems mModel = new PriviewModelItems();
                            mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                            mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                            mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                            mModel.setStrItem("w");
                            mModel.setStrQuantity("1");
                            mPriviewModelItemArray.add(mModel);
                        } else if (strType.equals("Subtract Percentage")) {
                            PriviewModelItems mModel = new PriviewModelItems();
                            mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                            mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                            mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                            mModel.setStrItem("w");
                            mModel.setStrQuantity("1");
                            mPriviewModelItemArray.add(mModel);
                        } else if (strType.equals("Subtract Value")) {
                            PriviewModelItems mModel = new PriviewModelItems();
                            mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                            mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                            mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                            mModel.setStrItem("w");
                            mModel.setStrQuantity("1");
                            mPriviewModelItemArray.add(mModel);
                        }
                    }
                }
            }
            mPriviewModelItemArrayNew = Utilities.gettingFormatedItems(mPriviewModelItemArray);
            Log.e(TAG, "*****SIZE****" + mPriviewModelItemArrayNew);

            for (int i = 0; i < mModel.getmItemModelArrayList().size(); i++) {
                intItems = mModel.getmItemModelArrayList().get(i).getQuantity();
                intUnitPrice = Double.parseDouble(mModel.getmItemModelArrayList().get(i).getUnitprice());
                if (i == 0) {
                    mAmount = intItems * intUnitPrice;
                    mSubTotal = mAmount;
                } else {
                    mAmount = intItems * intUnitPrice;
                    mSubTotal = mSubTotal + mAmount;
                }
            }

            if (mModel.getmPaymentModel() != null) {
                if (mModel.getmPaymentModel().getSubTotal().contains(".")) {
                    String strDWT2 = "00";
                    DecimalFormat df = new DecimalFormat("#.##");
                    Double f1 = Double.parseDouble(mModel.getmPaymentModel().getSubTotal());
                    df.setRoundingMode(RoundingMode.DOWN);
                    String strNumDWT = df.format(f1);
                    String[] separated = strNumDWT.split("\\.");
                    String strDWT = separated[0]; // this will contain "Fruit"
                    if (strNumDWT.contains(".")) {
                        strDWT2 = separated[1];
                    }
                    long numberGrain = Long.parseLong(strDWT);
                    String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                    txtSubTotalValueTV.setText(strNuumGrain + "." + strDWT2 + "  " + mModel.getCurrency());
                } else {
                    long numberGrossTonage = Long.parseLong(mModel.getmPaymentModel().getSubTotal());
                    String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                    txtSubTotalValueTV.setText(strNumGrossTonage + "  " + mModel.getCurrency());
//                         GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                }
//                txtSubTotalValueTV.setText("" + mInVoicesModel.getmPaymentModel().getSubTotal() + "  " + mInVoicesModel.getCurrency());

                if (mModel.getmPaymentModel().getVAT().contains(".")) {
                    String strDWT2 = "00";
                    DecimalFormat df = new DecimalFormat("#.##");
                    Double f1 = Double.parseDouble(mModel.getmPaymentModel().getVAT());
                    df.setRoundingMode(RoundingMode.DOWN);
                    String strNumDWT = df.format(f1);
                    String[] separated = strNumDWT.split("\\.");
                    String strDWT = separated[0]; // this will contain "Fruit"
                    if (strNumDWT.contains(".")) {
                        strDWT2 = separated[1];
                    }
                    long numberGrain = Long.parseLong(strDWT);
                    String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                    txtVATPercentTV.setText("V.A.T(" + strNuumGrain + "." + strDWT2 + "%):");
                } else {
                    long numberGrossTonage = Long.parseLong(mModel.getmPaymentModel().getVAT());
                    String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                    txtVATPercentTV.setText("V.A.T(" + strNumGrossTonage + "%):");
//                         GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                }
                if (mModel.getmPaymentModel().getVATPrice().contains(".")) {
                    String strDWT2 = "00";
                    DecimalFormat df = new DecimalFormat("#.##");
                    Double f1 = Double.parseDouble(mModel.getmPaymentModel().getVATPrice());
                    df.setRoundingMode(RoundingMode.DOWN);
                    String strNumDWT = df.format(f1);
                    String[] separated = strNumDWT.split("\\.");
                    String strDWT = separated[0]; // this will contain "Fruit"
                    if (strNumDWT.contains(".")) {
                        strDWT2 = separated[1];
                    }
                    long numberGrain = Long.parseLong(strDWT);
                    String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                    txtVatValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mModel.getCurrency());
                } else {
                    long numberGrossTonage = Long.parseLong(mModel.getmPaymentModel().getVATPrice());
                    String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                    txtVatValueTV.setText(" " + strNumGrossTonage + "  " + mModel.getCurrency());
//                         GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                }
                if (mModel.getmPaymentModel().getTotal().contains(".")) {
                    String strDWT2 = "00";
                    DecimalFormat df = new DecimalFormat("#.##");
                    Double f1 = Double.parseDouble(mModel.getmPaymentModel().getTotal());
                    df.setRoundingMode(RoundingMode.DOWN);
                    String strNumDWT = df.format(f1);
                    String[] separated = strNumDWT.split("\\.");
                    String strDWT = separated[0]; // this will contain "Fruit"
                    if (strNumDWT.contains(".")) {
                        strDWT2 = separated[1];
                    }
                    long numberGrain = Long.parseLong(strDWT);
                    String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                    txtTotalValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mModel.getCurrency());
                } else {
                    long numberGrossTonage = Long.parseLong(mModel.getmPaymentModel().getTotal());
                    String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                    txtTotalValueTV.setText(" " + strNumGrossTonage + "  " + mModel.getCurrency());
//                         GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                }
                if (mModel.getmPaymentModel().getPaid().contains(".")) {
                    String strDWT2 = "00";
                    DecimalFormat df = new DecimalFormat("#.##");
                    Double f1 = Double.parseDouble(mModel.getmPaymentModel().getPaid());
                    df.setRoundingMode(RoundingMode.DOWN);
                    String strNumDWT = df.format(f1);
                    String[] separated = strNumDWT.split("\\.");
                    String strDWT = separated[0]; // this will contain "Fruit"
                    if (strNumDWT.contains(".")) {
                        strDWT2 = separated[1];
                    }
                    long numberGrain = Long.parseLong(strDWT);
                    String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                    txtPaidValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mModel.getCurrency());
                } else {
                    long numberGrossTonage = Long.parseLong(mModel.getmPaymentModel().getPaid());
                    String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                    txtPaidValueTV.setText(" " + strNumGrossTonage + "  " + mModel.getCurrency());
//                  GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                }
//                txtVATPercentTV.setText("V.A.T(" + mInVoicesModel.getmPaymentModel().getVAT() + "%)");
//                txtVatValueTV.setText("" + mInVoicesModel.getmPaymentModel().getVATPrice() + "  " + mInVoicesModel.getCurrency());
//                txtTotalValueTV.setText("" + mInVoicesModel.getmPaymentModel().getTotal() + "  " + mInVoicesModel.getCurrency());
//                txtPaidValueTV.setText("" + mInVoicesModel.getmPaymentModel().getPaid() + "  " + mInVoicesModel.getCurrency());
                if (!mModel.getmPaymentModel().getBalanceDue().equals(""))
                    if (mModel.getmPaymentModel().getBalanceDue().contains(".")) {
                        String strDWT2 = "00";
                        DecimalFormat df = new DecimalFormat("#.##");
                        Double f1 = Double.parseDouble(mModel.getmPaymentModel().getBalanceDue());
                        df.setRoundingMode(RoundingMode.DOWN);
                        String strNumDWT = df.format(f1);
                        String[] separated = strNumDWT.split("\\.");
                        String strDWT = separated[0]; // this will contain "Fruit"
                        if (strNumDWT.contains(".")) {
                            strDWT2 = separated[1];
                        }
                        long numberGrain = Long.parseLong(strDWT);
                        String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                        txtBalanceDueValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mModel.getCurrency());
                    } else {
                        long numberGrossTonage = Long.parseLong(mModel.getmPaymentModel().getBalanceDue());
                        String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                        txtBalanceDueValueTV.setText(" " + strNumGrossTonage + "  " + mModel.getCurrency());
//                         GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                    }
            }
            imgTopStatusIV.setImageResource(Utilities.getStatusImage(mModel.getStatus()));
            if (mModel.getmStampsModel() != null) {
                if (mModel.getmStampsModel().getStamp_image().length() > 0) {
                    progress1.setVisibility(View.GONE);
//                    Picasso.with(mActivity)
//                            .load(mModel.getmStampsModel().getStamp_image())
//                            .listener(new RequestListener<String, GlideDrawable>() {
//                                @Override
//                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                                    Toast.makeText(mActivity, "Error Image is Too large ", Toast.LENGTH_SHORT).show();
//                                    return false;
//                                }
//
//                                @Override
//                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                                    progress1.setVisibility(View.GONE);
//                                    return false;
//                                }
//                            }).into(imgStampIV);
                    Picasso.get().load(mModel.getmStampsModel().getStamp_image())
//                            .placeholder(R.drawable.profile)
//                            .error(R.drawable.profile)
//                            .transform(new RoundedTransformation(50, 4))
//                            .resizeDimen(R.dimen.list_detail_image_size, R.dimen.list_detail_image_size)
//                            .centerCrop()
                            .into(imgStampIV);
                }
            } else {
                progress1.setVisibility(View.GONE);
            }
            if (mModel.getmSignatureModel() != null) {
                if (mModel.getmSignatureModel().getSignature_image().length() > 0) {
//                    Glide.with(mActivity)
//                            .load(mModel.getmSignatureModel().getSignature_image())
//                            .into(imgSigneIV);
                    Picasso.get().load(mModel.getmSignatureModel().getSignature_image())
//                            .placeholder(R.drawable.profile)
//                            .error(R.drawable.profile)
//                            .transform(new RoundedTransformation(50, 4))
//                            .resizeDimen(R.dimen.list_detail_image_size, R.dimen.list_detail_image_size)
//                            .centerCrop()
                            .into(imgSigneIV);
                }
            }


            if (mModel.getmItemModelArrayList().size() > 0) {
                /*Set Items Adapter*/
                for (int i = 0; i < mPriviewModelItemArrayNew.size(); i++) {
                    LayoutInflater layoutInflater =
                            (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(R.layout.item_pdf_item_layout, null);
                    TextView item_idTV = (TextView) addView.findViewById(R.id.item_idTV);
                    LinearLayout itemParentLL = (LinearLayout) addView.findViewById(R.id.itemParentLL);
                    TextView item_DescriptionTV = (TextView) addView.findViewById(R.id.item_DescriptionTV);
                    TextView item_QuantityTV = (TextView) addView.findViewById(R.id.item_QuantityTV);
                    TextView item_UnitPriceTV = (TextView) addView.findViewById(R.id.item_UnitPriceTV);
                    TextView item_AmountTV = (TextView) addView.findViewById(R.id.item_AmountTV);
                    PriviewModelItems tempValue = mPriviewModelItemArrayNew.get(i);
                    int strCOUNT = i + 1;
                    if (!tempValue.getStrDescription().equals("")) {
                        item_idTV.setText("" + strCOUNT);
                    }

                    if (!tempValue.getStrDescription().equals("")) {
                        int strCharcter = tempValue.getStrDescription().length();
                        if (strCharcter <= 60) {

                            itemParentLL.setMinimumHeight(40);
                        }
                        item_DescriptionTV.setText(tempValue.getStrDescription());
                    } else {
                        itemParentLL.setMinimumHeight(40);

                    }
                    if (!tempValue.getStrUnitPrice().equals("")) {
                        if (tempValue.getStrUnitPrice().contains(".")) {
                            String strDWT2 = "00";
                            DecimalFormat df = new DecimalFormat("#.##");
                            Double f1 = Double.parseDouble(tempValue.getStrUnitPrice());
                            df.setRoundingMode(RoundingMode.DOWN);
                            String strNumDWT = df.format(f1);
                            String[] separated = strNumDWT.split("\\.");
                            String strDWT = separated[0]; // this will contain "Fruit"
                            if (strNumDWT.contains(".")) {
                                strDWT2 = separated[1];
                            }
                            long numberGrain = Long.parseLong(strDWT);
                            String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                            item_UnitPriceTV.setText(strNuumGrain + "." + strDWT2);
                        } else {
                            long numberGrossTonage = Long.parseLong(tempValue.getStrUnitPrice());
                            String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                            item_UnitPriceTV.setText(strNumGrossTonage);
//                         GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                        }
//                        item_UnitPriceTV.setText("" + tempValue.getStrUnitPrice() + " " );
                    }
                    if (!tempValue.getStrAmount().equals("")) {
                        if (tempValue.getStrAmount().contains(".")) {
                            if (tempValue.getStrAmount().contains("+")) {
                                String strDWT2 = "00";
                                DecimalFormat df = new DecimalFormat("##.##");
                                Double f1 = Double.parseDouble(tempValue.getStrAmount());
                                df.setRoundingMode(RoundingMode.DOWN);
                                String strNumDWT = df.format(f1);
                                String[] separated = strNumDWT.split("\\.");
                                String strDWT = separated[0]; // this will contain "Fruit"
                                if (strNumDWT.contains(".")) {
                                    strDWT2 = separated[1];
                                }
                                long numberGrain = Long.parseLong(strDWT);
                                String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                                item_AmountTV.setText(strNuumGrain + "." + strDWT2);
                            } else if (tempValue.getStrAmount().contains("-")) {
                                String strDWT2 = "00";
                                DecimalFormat df = new DecimalFormat("#.##");
                                Double f1 = Double.parseDouble(tempValue.getStrAmount());
                                df.setRoundingMode(RoundingMode.DOWN);
                                String strNumDWT = df.format(f1);
                                String[] separated = strNumDWT.split("\\.");
                                String strDWT = separated[0]; // this will contain "Fruit"
                                if (strNumDWT.contains(".")) {
                                    strDWT2 = separated[1];
                                }
                                long numberGrain = Long.parseLong(strDWT);
                                String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                                if (strNuumGrain.contains("-")) {
                                    item_AmountTV.setText(strNuumGrain + "." + strDWT2);
                                } else {
                                    item_AmountTV.setText("-" + strNuumGrain + "." + strDWT2);
                                }
                            } else {
                                String strDWT2 = "00";
                                DecimalFormat df = new DecimalFormat("#.##");
                                Double f1 = Double.parseDouble(tempValue.getStrAmount());
                                df.setRoundingMode(RoundingMode.DOWN);
                                String strNumDWT = df.format(f1);
                                String[] separated = strNumDWT.split("\\.");
                                String strDWT = separated[0]; // this will contain "Fruit"
                                if (strNumDWT.contains(".")) {
                                    strDWT2 = separated[1];
                                }
                                long numberGrain = Long.parseLong(strDWT);
                                String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                                item_AmountTV.setText(strNuumGrain + "." + strDWT2);
                            }

                        } else {
                            long numberGrossTonage = Long.parseLong(tempValue.getStrAmount());
                            String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                            item_AmountTV.setText(strNumGrossTonage);
//                          GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                        }
//                        item_AmountTV.setText("" + tempValue.getStrAmount() + " ");
                    }
                    if (!tempValue.getStrQuantity().equals("")) {
                        item_QuantityTV.setText("" + tempValue.getStrQuantity());
                    }
//                    + mInVoicesModel.getCurrency()
                    if (i % 2 == 0) {
                        itemParentLL.setBackgroundResource(R.drawable.pdf_row_odd);
                    } else {
                        itemParentLL.setBackgroundResource(R.drawable.pdf_row_even);
                    }
                    mainLL1.addView(addView);
                    ViewTreeObserver observer = mainLL1.getViewTreeObserver();
                    observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            // TODO Auto-generated method stub
                            init();
                            mainLL1.getViewTreeObserver().removeGlobalOnLayoutListener(
                                    this);
                        }
                    });
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void init() {
        int a = mainLL1.getHeight();
        int b = mainLL1.getWidth();
        if (a < 490) {

        } else {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mainLL1.getLayoutParams();
//        Changes the height and width to the specified *pixels*
            params.height = 490;
            mainLL1.setLayoutParams(params);
        }
//        Toast.makeText(mActivity,""+a+" "+b,Toast.LENGTH_LONG).show();
    }
}
