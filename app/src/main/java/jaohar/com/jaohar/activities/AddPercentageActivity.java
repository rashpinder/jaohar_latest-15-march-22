package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.Utilities;

public class AddPercentageActivity extends BaseActivity {
    Activity mActivity = AddPercentageActivity.this;
    String TAG = AddPercentageActivity.this.getClass().getSimpleName(), strItemID,strItem,strQuantity,strPrice;
    LinearLayout llLeftLL;
    TextView txtSubTotalTV, txtQuantityTV, txtVatPriceTV, txtTotalTV;
    EditText AddValueTV, txtDescriptionTV;
    Button btnSubmitB;
    InvoiceAddItemModel mModel, pModel;
    int mPosition;
    private long parcentValue;
    private long sumData;
    private double totalprice;
    ArrayList<DiscountModel> mDiscountModelArrayList = new ArrayList<DiscountModel>();
    DiscountModel mDiscountModel =new DiscountModel();
    double totalPercent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_percentage);
        if (getIntent() != null) {
            mModel = (InvoiceAddItemModel) getIntent().getSerializableExtra("Model");
            mDiscountModelArrayList =mModel.getmDiscountModelArrayList();
            Log.e(TAG,"ARRAY1=============="+mDiscountModelArrayList.size());
            mPosition = getIntent().getIntExtra("Position", 0);
            strItemID = mModel.getItemID();
        }
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        txtSubTotalTV = (TextView) findViewById(R.id.txtSubTotalTV);
        txtQuantityTV = (TextView) findViewById(R.id.txtQuantityTV);
        txtDescriptionTV = (EditText) findViewById(R.id.txtDescriptionTV);
        txtVatPriceTV = (TextView) findViewById(R.id.txtVatPriceTV);
        txtTotalTV = (TextView) findViewById(R.id.txtTotalTV);
        AddValueTV = (EditText) findViewById(R.id.AddValueTV);
        btnSubmitB = (Button) findViewById(R.id.btnSubmitB);
        totalprice =Double.parseDouble(Utilities.convertEvalueToNormal(Double.parseDouble(mModel.getStrTotalAmountUnit()))) ;
        String total = String.valueOf(totalprice);
        strItem = mModel.getItem();
        strQuantity = String.valueOf(mModel.getQuantity());
        strPrice = String.valueOf(mModel.getUnitprice());
        txtSubTotalTV.setText(strItem);
//      txtVatPriceTV.setText(strPrice);
        txtVatPriceTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(mModel.getStrTotalAmountUnit())));
//      txtDescriptionTV.setText(strDescription);
        txtQuantityTV.setText(strQuantity);
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        btnSubmitB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtDescriptionTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_description));
                } else if (AddValueTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_value));
                }else{
                    mDiscountModel.setAdd_description(txtDescriptionTV.getText().toString());
                    mDiscountModel.setADD_items(strItem);
                    mDiscountModel.setADD_quantity(strQuantity);
                    mDiscountModel.setADD_unitPrice(String.valueOf(totalprice));
                    mDiscountModel.setAdd_Value(AddValueTV.getText().toString());
                    mDiscountModel.setType("ADD");
                    mDiscountModel.setAddAndSubtractTotal(txtTotalTV.getText().toString());
                    mDiscountModel.setStrtotalPercentValue(String.valueOf(totalPercent));
                    mDiscountModelArrayList.add(mDiscountModel);
                    Log.e(TAG,"ARRAY2=============="+mDiscountModelArrayList.size());
//                  mModel.setmDiscountModelArrayList(mDiscountModelArrayList);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("pModel", mDiscountModelArrayList);
                    returnIntent.putExtra("Position", mPosition);
                    returnIntent.putExtra("IS_ADD_DISCOUNT", true);
                    setResult(910, returnIntent);
                    onBackPressed();
                    finish();
                }
            }
        });

        AddValueTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double basePrice = 0;
                double sum;
                String str = AddValueTV.getText().toString();
                try {

                    basePrice = Double.parseDouble(mModel.getStrTotalAmountUnit());//   Integer.parseInt()
                    totalPercent= ((basePrice * Double.parseDouble(str)) / 100);
                    sum = basePrice + totalPercent;
                    txtTotalTV.setText("" + Utilities.convertEvalueToNormal(sum));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
