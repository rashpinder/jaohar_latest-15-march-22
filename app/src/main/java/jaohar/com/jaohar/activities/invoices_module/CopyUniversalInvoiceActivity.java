package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddInvoiceItemActivity;
import jaohar.com.jaohar.activities.AddTotalDisount;
import jaohar.com.jaohar.activities.AllBankDetailsActivity;
import jaohar.com.jaohar.activities.AllCurrencyActivity;
import jaohar.com.jaohar.activities.EditTotalDiscountActivity;
import jaohar.com.jaohar.activities.InvoiceVesselsActivity;
import jaohar.com.jaohar.activities.PaymentActivity;
import jaohar.com.jaohar.activities.PreviewActivity;
import jaohar.com.jaohar.activities.SearchCompanyActivity;
import jaohar.com.jaohar.activities.ShowEditInvoiceItemAciivity;
import jaohar.com.jaohar.activities.SignatureActivity;
import jaohar.com.jaohar.activities.StampsActivity;
import jaohar.com.jaohar.activities.UploadInvoiceODF;
import jaohar.com.jaohar.adapters.InVoiceItemsAdapter;
import jaohar.com.jaohar.adapters.InvoiceTotalDiscountAdapter;
import jaohar.com.jaohar.beans.AddTotalInvoiceDiscountModel;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.beans.CurrenciesModel;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.beans.PaymentModel;
import jaohar.com.jaohar.beans.PriviewModelItems;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.DeleteItemInvoiceInterface;
import jaohar.com.jaohar.interfaces.EditInvoiceItemData;
import jaohar.com.jaohar.interfaces.EditTotalDiscountItemdata;
import jaohar.com.jaohar.models.InvoiceModel;
import jaohar.com.jaohar.models.invoicemodels.GetUniversalInvoiceNoModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class CopyUniversalInvoiceActivity extends BaseActivity {
    public static boolean isPaymentUpdated = false;
    static double mSubTotal1 = 0;
    static double mTotal1 = 0;
    static double mTotalASDisc1 = 0;
    Activity mActivity = CopyUniversalInvoiceActivity.this;
    String TAG = CopyUniversalInvoiceActivity.this.getClass().getSimpleName();
    ImageView imgBack;
    LinearLayout llLeftLL;
    TextView txtCenter;

    //Form
    EditText editInVoiceNumET, editInVoiceDateET, editTermsDaysET, editSearchCompanyET, /*editSearchVesselsET,*/
            editCurrencyET, invoiceNumET,
            editStampET, editSignInvoiceET, editStatusET, editRefrance1ET, editRefrance2ET, editRefrance3ET, editBankET/*, editQuantityET, editPriceET, editDescriptionET*/;
    TextView txtItemsET, txtdiscountET;
    EditText editSearchVesselsET;
    Button btnSave, btnPaymentB, btnPreview;
    String arrayVesselsType[], arrayStatus[], arrayCurrency[];
    InvoiceAddItemModel mInvoiceAddItemModel;
    CompaniesModel mCompaniesModel;
    StampsModel mStampsModel;
    SignatureModel mSignatureModel;
    BankModel mBankModel;
    VesselSearchInvoiceModel mVesselInvoiceModel;
    CurrenciesModel mCurrenciesModel;
    PaymentModel mPaymentModelP = new PaymentModel();
    PaymentModel mPaymentModel = new PaymentModel();
    RecyclerView mRecyclerView, mRecyclerViewDiscount;
    NestedScrollView mNestedScrollView;
    InVoiceItemsAdapter mInVoiceItemsAdapter;

    int mSelectedItemPosition;
    boolean isSelectedItemPosition = false;
    boolean isDiscountPosition = false;
    boolean isDiscountPositionNEW = false;
    InVoicesModel mInVoicesModel;
    AddTotalInvoiceDiscountModel mTotalDiscountModel;
    InvoiceTotalDiscountAdapter mInvoiceTotalDiscountAdapter;

    ArrayList<InvoiceAddItemModel> itemsArrayList = new ArrayList<InvoiceAddItemModel>();
    ArrayList<AddTotalInvoiceDiscountModel> mArrayTotalDiscount = new ArrayList<AddTotalInvoiceDiscountModel>();
    ArrayList<AddTotalInvoiceDiscountModel> mArrayTotalDiscountNEW = new ArrayList<AddTotalInvoiceDiscountModel>();
    ArrayList<PriviewModelItems> mPriviewModelItemArray = new ArrayList<PriviewModelItems>();

    private static ArrayList<String> itemIDARRAY = new ArrayList<String>();
    private static ArrayList<String> totalDiscountIDARRAY = new ArrayList<String>();
    AddTotalInvoiceDiscountModel mTotalDiscountModelNew;
    String strPAID = "", strVAt = "", strPaymentID = "", strInvoiceNum = "";
    int data = 0;
    String prefix = "";
    double mPaid = 0;
    double mBalanceDue = 0;
    double mTotal = 0, mVatCost = 0;
    DiscountModel mDiscountModel;
    double mSubTotal = 0;
    double intItems = 0;
    double intUnitPrice = 0;
    double mAmount = 0;
    String strVessalID = "";
    String strStamPId = "";
    String strSignatireId = "";
    String strBankId = "";
    String strCompanyId = "";
    JSONArray itemsJsonArray;
    JSONArray itemsDiscountJsonArray;
    String strInvoiceID = "", strSearchVessel = "", strSearchCompany = "", strStamp = "", strSign = "", strBankDetails = "";
    private static int owner_id = 0;
    EditInvoiceItemData mEditInvoiceItemData = new EditInvoiceItemData() {
        @Override
        public void editInvoiceItemData(InvoiceAddItemModel mModel, int position) {
            mSelectedItemPosition = position;
//            Intent mIntent = new Intent(mActivity, EditInvoiceItemActivity.class);
//            mIntent.putExtra("Model", mModel);
//            mIntent.putExtra("Position", mSelectedItemPosition);
//            startActivityForResult(mIntent, 111);
            Intent mIntent = new Intent(mActivity, ShowEditInvoiceItemAciivity.class);
            mIntent.putExtra("Model", mModel);
            mIntent.putExtra("Position", mSelectedItemPosition);
            startActivityForResult(mIntent, 111);

        }
    };

    EditTotalDiscountItemdata mEditInvoiceDiscountItemData = new EditTotalDiscountItemdata() {
        @Override
        public void editTotalDiscountItemdata(AddTotalInvoiceDiscountModel mModel, int position, String strType) {
            Intent mIntent = new Intent(mActivity, EditTotalDiscountActivity.class);
            mIntent.putExtra("Model", mModel);
            mIntent.putExtra("Position", position);
            startActivityForResult(mIntent, 1212);

        }
    };

    DeleteItemInvoiceInterface mDeleteItemInvoiceInterface = new DeleteItemInvoiceInterface() {
        @Override
        public void deleteItemInvoice(InvoiceAddItemModel model) {
            if (Utilities.isNetworkAvailable(mActivity) == false) {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
            } else {
                showAlertDeleteDialog(mActivity, model);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_universal_invoice);

        arrayVesselsType = mActivity.getResources().getStringArray(R.array.vessel_type_array);
        arrayStatus = mActivity.getResources().getStringArray(R.array.status_array);
        arrayCurrency = mActivity.getResources().getStringArray(R.array.currency_array);

        setStatusBar();

        if (JaoharConstants.IS_EDIT_ITEM_CLICK == false) {
            if (getIntent() != null) {
//                owner_id = getIntent().getIntExtra("owner_id", 0);
                mInVoicesModel = (InVoicesModel) getIntent().getSerializableExtra("Model");
                strInvoiceID = mInVoicesModel.getInvoice_id();
                if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
                    strSearchVessel = mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_id();
                }
                if (mInVoicesModel.getmCompaniesModel() != null) {
                    strSearchCompany = mInVoicesModel.getmCompaniesModel().getId();
                }
                if (mInVoicesModel.getmStampsModel() != null) {
                    strStamp = mInVoicesModel.getmStampsModel().getId();
                }
                if (mInVoicesModel.getmSignatureModel() != null) {
                    strSign = mInVoicesModel.getmSignatureModel().getId();
                }
                if (mInVoicesModel.getmBankModel() != null) {
                    strBankDetails = mInVoicesModel.getmBankModel().getId();
                }
                if (mInVoicesModel.getOwner_id() != null) {
                    owner_id = Integer.parseInt(mInVoicesModel.getOwner_id());
                }
            }
        }
            executeGetInvoiceNoAPI();
        mSetViewsIDs();
        mSetClickListner();
    }

    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("owner_id", String.valueOf(owner_id));
        mMap.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeGetInvoiceNoAPI() {
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getUniversalInvoiceNoRequest(String.valueOf(owner_id),JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""))
                .enqueue(new Callback<GetUniversalInvoiceNoModel>() {
                    @Override
                    public void onResponse(Call<GetUniversalInvoiceNoModel> call, retrofit2.Response<GetUniversalInvoiceNoModel> response) {
//                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response.body());
                        GetUniversalInvoiceNoModel mModel = response.body();
                        if (mModel.getStatus().equals("1")) {
                            data = mModel.getData();
                            prefix = mModel.getPrefix();
                        } else if (mModel.getStatus().equals("100")) {
                            AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<GetUniversalInvoiceNoModel> call, Throwable t) {
//                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**ERROR**" + t.getMessage());
                    }
                });
    }


    private void mSetViewsIDs() {
        invoiceNumET = findViewById(R.id.invoiceNumET);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtCenter.setText("Copy Universal Invoice");
        editInVoiceNumET = (EditText) findViewById(R.id.editInVoiceNumET);
        editInVoiceNumET.setSelection(0);
        editInVoiceDateET = (EditText) findViewById(R.id.editInVoiceDateET);
        editInVoiceDateET.setSelection(0);
        editInVoiceDateET.setKeyListener(null);
        editTermsDaysET = (EditText) findViewById(R.id.editTermsDaysET);
        editTermsDaysET.setSelection(0);
        editSearchCompanyET = (EditText) findViewById(R.id.editSearchCompanyET);
        editSearchCompanyET.setSelection(0);
        editSearchCompanyET.setKeyListener(null);
        editSearchVesselsET = (EditText) findViewById(R.id.editSearchVesselsET);
        editSearchVesselsET.setSelection(0);
        editCurrencyET = (EditText) findViewById(R.id.editCurrencyET);
        editCurrencyET.setSelection(0);
        editCurrencyET.setKeyListener(null);
        editStampET = (EditText) findViewById(R.id.editStampET);
        editStampET.setSelection(0, 0);
        editStampET.setKeyListener(null);
        editSignInvoiceET = (EditText) findViewById(R.id.editSignInvoiceET);
        editSignInvoiceET.setKeyListener(null);
        editSignInvoiceET.setSelection(0);
        editStatusET = (EditText) findViewById(R.id.editStatusET);
        editStatusET.setKeyListener(null);
        editStatusET.setSelection(0);
        editRefrance1ET = (EditText) findViewById(R.id.editRefrance1ET);
        editRefrance1ET.setSelection(0, 0);
        editRefrance2ET = (EditText) findViewById(R.id.editRefrance2ET);
        editRefrance2ET.setSelection(0, 0);
        editRefrance3ET = (EditText) findViewById(R.id.editRefrance3ET);
        editRefrance3ET.setSelection(0, 0);
        editBankET = (EditText) findViewById(R.id.editBankET);
        editBankET.setSelection(0);
        editBankET.setKeyListener(null);
        txtItemsET = (TextView) findViewById(R.id.txtItemsET);
        txtdiscountET = (TextView) findViewById(R.id.txtdiscountET);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mRecyclerViewDiscount = (RecyclerView) findViewById(R.id.mRecyclerViewDiscount);
        mNestedScrollView = (NestedScrollView) findViewById(R.id.mNestedScrollView);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnPreview = (Button) findViewById(R.id.btnPreview);
        btnPaymentB = (Button) findViewById(R.id.btnPaymentB);

        if (JaoharConstants.IS_EDIT_ITEM_CLICK == false) {
            /*Execute Getting All Data About Selected Invoice Post*/
            if (Utilities.isNetworkAvailable(mActivity) == false) {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
            } else {
                executeGettingDataApi(mInVoicesModel);
            }
        }
    }

    private void mSetClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });

        editInVoiceDateET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showInvoiceDatePicker(mActivity, editInVoiceDateET);
                    return true;
                }
                return false;
            }
        });

        editCurrencyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Currency", arrayCurrency, editCurrencyET);
                    return true;
                }
                return false;
            }
        });

        editStatusET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    AlertDialogManager.showSelectItemFromArray(mActivity, "Status", arrayStatus, editStatusET);
                    return true;
                }
                return false;
            }
        });

        editSearchCompanyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent mIntent = new Intent(mActivity, SearchCompanyActivity.class);
                    startActivityForResult(mIntent, 222);
                    return true;
                }
                return false;
            }
        });

        editStampET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (editStampET.getText().toString().equals("")) {
                        Intent mIntent = new Intent(mActivity, StampsActivity.class);
                        startActivityForResult(mIntent, 333);
                    } else {
                        openDeleteDialog(editStampET, "stamp");
                    }
                    return true;
                }
                return false;
            }
        });

        editSignInvoiceET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (editSignInvoiceET.getText().toString().equals("")) {
                        Intent mIntent = new Intent(mActivity, SignatureActivity.class);
                        startActivityForResult(mIntent, 444);
                    } else {
                        openDeleteDialog(editSignInvoiceET, "sign");
                    }

                    return true;
                }
                return false;
            }
        });

        editBankET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent mIntent = new Intent(mActivity, AllBankDetailsActivity.class);
                    startActivityForResult(mIntent, 555);
                    return true;
                }
                return false;
            }
        });

        editSearchVesselsET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent mIntent = new Intent(mActivity, InvoiceVesselsActivity.class);
                    startActivityForResult(mIntent, 666);
                    return true;
                }
                return false;
            }
        });

        editCurrencyET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    //AlertDialogManager.showSelectItemFromArray(mActivity, "Currency", arrayCurrency, editCurrencyET);
                    Intent mIntent = new Intent(mActivity, AllCurrencyActivity.class);
                    startActivityForResult(mIntent, 777);
                    overridePendingTransitionEnter();
                    return true;
                }
                return false;
            }
        });

        btnPaymentB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mArrayTotalDiscount.size() == 0) {
                    if (itemsArrayList.size() == 0) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                    } else {
                        Intent mIntent = new Intent(mActivity, PaymentActivity.class);
                        mIntent.putExtra("IsEdit", false);
                        mIntent.putExtra("mPaymnetModel", mPaymentModelP);
                        mIntent.putExtra("mArrayList", itemsArrayList);
                        startActivityForResult(mIntent, 888);
                        overridePendingTransitionEnter();
                    }
//                 AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_discounts));
                } else {
                    if (itemsArrayList.size() == 0) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                    } else {
                        if (isDiscountPositionNEW == true) {
                            isDiscountPositionNEW = false;
                            Intent mIntent = new Intent(mActivity, PaymentActivity.class);
                            mIntent.putExtra("IsEdit", false);
                            mIntent.putExtra("ArrayList", mArrayTotalDiscountNEW);
                            mIntent.putExtra("mPaymnetModel", mPaymentModelP);
                            startActivityForResult(mIntent, 888);
                            overridePendingTransitionEnter();
                        } else {
                            Intent mIntent = new Intent(mActivity, PaymentActivity.class);
                            mIntent.putExtra("IsEdit", false);
                            mIntent.putExtra("ArrayList", mArrayTotalDiscount);
                            mIntent.putExtra("mPaymnetModel", mPaymentModelP);
                            startActivityForResult(mIntent, 888);
                            overridePendingTransitionEnter();
                        }
                    }
                }
            }
        });

        txtItemsET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, AddInvoiceItemActivity.class);
                startActivityForResult(mIntent, 101);
            }
        });

        txtdiscountET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemsArrayList.size() == 0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                } else {
                    Intent mIntent = new Intent(mActivity, AddTotalDisount.class);
                    mIntent.putExtra("mArraYList", itemsArrayList);
                    startActivityForResult(mIntent, 999);
                }
            }
        });

        btnPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isActualVessel = false;
                if (editInVoiceDateET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_date));
                } else if (editTermsDaysET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_termdays));
                } else if (editSearchCompanyET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_search_company));
                }
//                else if (editSearchVesselsET.getText().toString().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_search_vessels));
//                }
                else if (editCurrencyET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_currency));
                }
//                else if (editStampET.getText().toString().trim().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_stamp));
//                }
//                else if (editSignInvoiceET.getText().toString().trim().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_sign_invoice));
//                }
                else if (itemsArrayList.size() == 0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                }
//                else if (isPaymentUpdated) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_go_to_payment_and_fill_details));
//                }
                else if (editBankET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_bank_details));
                } else {

                    mInVoicesModel.setInvoice_number(strInvoiceNum);
                    mInVoicesModel.setInvoice_date(editInVoiceDateET.getText().toString());
                    mInVoicesModel.setTerm_days(editTermsDaysET.getText().toString());
                    mInVoicesModel.setSearch_company(editSearchCompanyET.getText().toString());
                    mInVoicesModel.setSearch_vessel(editSearchVesselsET.getText().toString());
                    mInVoicesModel.setCurrency(editCurrencyET.getText().toString());
                    mInVoicesModel.setStamp(editStampET.getText().toString());
                    mInVoicesModel.setSign_invoice(editSignInvoiceET.getText().toString());
                    mInVoicesModel.setStatus(editStatusET.getText().toString());
                    mInVoicesModel.setRefrence1(editRefrance1ET.getText().toString());
                    mInVoicesModel.setRefrence2(editRefrance2ET.getText().toString());
                    mInVoicesModel.setRefrence3(editRefrance3ET.getText().toString());

//                    mInVoicesModel.setmItemModelArrayList(itemsArrayList);

//                    if (mCompaniesModel != null)
//                        mInVoicesModel.setmCompaniesModel(mCompaniesModel);

//                    if (mStampsModel != null)
//                        mInVoicesModel.setmStampsModel(mStampsModel);

//                    if (mSignatureModel != null)
//                        mInVoicesModel.setmSignatureModel(mSignatureModel);

//                    if (mBankModel != null)
//                        mInVoicesModel.setmBankModel(mBankModel);

//                    if (mVesselSearchModel != null)
//                        mInVoicesModel.setmVesselSearchInvoiceModel(mVesselSearchModel);

//                    if (mPaymentModel != null)
//                        mInVoicesModel.setmPaymentModel(mPaymentModel);

                    if (JaoharConstants.IS_Payment_Update == true) {
                        JaoharConstants.IS_Payment_Update = false;
                        mInVoicesModel.setmPaymentModel(mPaymentModelP);
                    } else {
                        if (mPaymentModelP != null) {
                            mSubTotal1 = Double.parseDouble(mPaymentModelP.getSubTotal());
                            strPaymentID = mPaymentModelP.getPayment_id();
                            if (mPaymentModelP.getPaid() != null) {
                                strPAID = mPaymentModelP.getPaid().toString();
                            }
                            if (mPaymentModelP.getVAT() != null) {
                                strVAt = mPaymentModelP.getVAT().toString();
                            }
                        }
                        if (itemsArrayList.size() != 0) {
                            double mAmount1 = 0;
                            for (int i = 0; i < itemsArrayList.size(); i++) {
                                if (i == 0) {
                                    mAmount1 = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                                    mSubTotal1 = mAmount1;
                                    mSubTotal1 = Utilities.getRoundOff2Decimal(mSubTotal1);
                                } else {
                                    mAmount1 = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                                    mSubTotal1 = mSubTotal1 + mAmount1;
                                    mSubTotal1 = Utilities.getRoundOff2Decimal(mSubTotal1);
                                }
                            }
                            if (!strVAt.equals("")) {
                                String strValue = strVAt;
                                if (strValue.length() > 0) {
                                    Float mVat = Float.parseFloat(strVAt);
                                    mVatCost = (mSubTotal1 * mVat) / 100;
                                    mTotal = mSubTotal1 + mVatCost;
                                    mTotal = Utilities.getRoundOff2Decimal(mTotal);
                                    mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                                    mBalanceDue = mTotal;
//                                txtVatPriceTV.setText("" + mVatCost);
//                                txtTotalTV.setText("" + mTotal);
//                                txtBalaceDueTV.setText("" + mTotal);
//                                txtSubTotalTV.setText("" + mSubTotal);
                                }
                            }
                            if (!strPAID.equals("")) {
                                String strValue1 = strPAID;
                                if (strValue1.length() > 0) {
                                    mPaid = Double.parseDouble(strPAID);
                                    mBalanceDue = mTotal - mPaid;
                                    mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
//                                  txtSubTotalTV.setText("" + mSubTotal);
                                }
                            }

                            Log.e(TAG, String.valueOf(mSubTotal1));
                        }
//                        } else {
                        if (mArrayTotalDiscount.size() != 0) {
                            for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                                AddTotalInvoiceDiscountModel mDiscountModel = mArrayTotalDiscount.get(i);
                                if (mDiscountModel.getDiscountType().equals("Add Percentage")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());

                                } else if (mDiscountModel.getDiscountType().equals("Add Value")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());

                                } else if (mDiscountModel.getDiscountType().equals("Subtract Percentage")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Subtract Value")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                }
                                Log.e(TAG, "***Final Total***" + mTotalASDisc1);
                            }
                            mSubTotal1 = Utilities.getRoundOff2Decimal(mTotal1 + mTotalASDisc1);
                            mTotalASDisc1 = 0;
                            String strValue = strVAt;
                            if (strValue.length() > 0) {
                                Float mVat = Float.parseFloat(strVAt);
                                mVatCost = (mSubTotal1 * mVat) / 100;
                                mTotal = mSubTotal1 + mVatCost;
                                mTotal = Utilities.getRoundOff2Decimal(mTotal);
                                mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                                mBalanceDue = mTotal;
//                                txtVatPriceTV.setText("" + mVatCost);
//                                txtTotalTV.setText("" + mTotal);
//                                txtBalaceDueTV.setText("" + mTotal);
//                                txtSubTotalTV.setText("" + mSubTotal);
                            }
                            String strValue1 = strPAID;
                            if (strValue1.length() > 0) {
                                mPaid = Double.parseDouble(strPAID);
                                mBalanceDue = mTotal - mPaid;
                                mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
//                                txtSubTotalTV.setText("" + mSubTotal);
                            }
                            Log.e(TAG, String.valueOf(mSubTotal1));
                        }
                        mPaymentModelP.setSubTotal("" + mSubTotal1);
                        mPaymentModelP.setVAT(strVAt);
                        mPaymentModelP.setPaid(strPAID);
                        mPaymentModelP.setTotal("" + mTotal);
                        mPaymentModelP.setBalanceDue("" + mBalanceDue);
                        mPaymentModelP.setVATPrice("" + mVatCost);
                        mPaymentModelP.setPayment_id(strPaymentID);
                    }
//                        mPaymentModelP = new PaymentModel();
//                        mPaymentModelP.setBalanceDue(String.valueOf(mBalanceDue));
//                        mPaymentModelP.setSubTotal(String.valueOf(mSubTotal1));
//                        mPaymentModelP.setTotal(String.valueOf(mSubTotal1));
//                        mPaymentModelP.setVAT("0");
//                        mPaymentModelP.setPaid("0");
//                        mPaymentModelP.setVATPrice(String.valueOf(mVatCost));
//                        mPaymentModelP.setPayment_id("0");
//                        mInVoicesModel.setmPaymentModel(mPaymentModelP);
                }
                mPriviewModelItemArray.clear();
                if (itemsArrayList.size() != 0) {

                    for (int i = 0; i < itemsArrayList.size(); i++) {
                        PriviewModelItems mModel = new PriviewModelItems();
                        double intItems = itemsArrayList.get(i).getQuantity();
                        double intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getUnitprice());
                        double mAmount = intItems * intUnitPrice;
                        mModel.setStrAmount(String.valueOf(mAmount));
                        mModel.setStrDescription(itemsArrayList.get(i).getDescription());
                        mModel.setStrItem(itemsArrayList.get(i).getItem());
                        mModel.setStrQuantity(String.valueOf(itemsArrayList.get(i).getQuantity()));
                        mModel.setStrUnitPrice(String.valueOf(itemsArrayList.get(i).getUnitprice()));
                        mPriviewModelItemArray.add(mModel);
                        for (int k = 0; k < itemsArrayList.get(i).getmDiscountModelArrayList().size(); k++) {
                            String strType = itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getType();
                            if (strType.equals("ADD")) {
                                PriviewModelItems mModel1 = new PriviewModelItems();
                                mModel1.setStrItem("w");
                                mModel1.setStrQuantity("1");
                                String finals = itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue();
                                mModel1.setStrAmount(finals);
                                mModel1.setStrUnitPrice(finals);
                                mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());

                                mPriviewModelItemArray.add(mModel1);
                            } else if (strType.equals("ADD_VALUE")) {
                                PriviewModelItems mModel1 = new PriviewModelItems();
                                mModel1.setStrItem("w");
                                mModel1.setStrQuantity("1");
                                String finals = "+" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue();
                                mModel1.setStrAmount(finals);
                                mModel1.setStrUnitPrice(finals);
                                mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());

                                mPriviewModelItemArray.add(mModel1);
                            } else if (strType.equals("SUBTRACT")) {
                                PriviewModelItems mModel1 = new PriviewModelItems();
                                mModel1.setStrItem("w");
                                mModel1.setStrQuantity("1");
                                if (itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue().contains("-")) {
                                    mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                    mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                } else {
                                    mModel1.setStrUnitPrice("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                    mModel1.setStrAmount("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                }
                                mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                                mPriviewModelItemArray.add(mModel1);
                            } else if (strType.equals("SUBTRACT_VALUE")) {
                                PriviewModelItems mModel1 = new PriviewModelItems();
                                mModel1.setStrItem("w");
                                mModel1.setStrQuantity("1");
                                if (itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue().contains("-")) {
                                    mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                    mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                } else {
                                    mModel1.setStrUnitPrice("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                    mModel1.setStrAmount("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());

                                }
                                mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                                mPriviewModelItemArray.add(mModel1);
                            }
                        }
                    }
                    if (mArrayTotalDiscount.size() != 0) {
                        for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                            String strType = mArrayTotalDiscount.get(i).getDiscountType();
                            if (strType.equals("Add Percentage")) {
                                PriviewModelItems mModel = new PriviewModelItems();
                                mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                                mModel.setStrItem("w");
                                mModel.setStrQuantity("1");

                                mPriviewModelItemArray.add(mModel);
                            } else if (strType.equals("Add Value")) {
                                PriviewModelItems mModel = new PriviewModelItems();
                                mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                                mModel.setStrItem("w");
                                mModel.setStrQuantity("1");

                                mPriviewModelItemArray.add(mModel);
                            } else if (strType.equals("Subtract Percentage")) {
                                PriviewModelItems mModel = new PriviewModelItems();
                                if (mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent().contains("-")) {
                                    mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                } else {
                                    mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                }

                                mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                                mModel.setStrItem("w");
                                mModel.setStrQuantity("1");
                                mPriviewModelItemArray.add(mModel);
                            } else if (strType.equals("Subtract Value")) {
                                PriviewModelItems mModel = new PriviewModelItems();
                                if (mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent().contains("-")) {
                                    mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                } else {
                                    mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                }

                                mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                                mModel.setStrItem("w");
                                mModel.setStrQuantity("1");
                                mPriviewModelItemArray.add(mModel);
                            }

                        }
                    }
                    Log.e("TAGSIZE", String.valueOf(mPriviewModelItemArray.size()));
                    Intent mIntent = new Intent(mActivity, PreviewActivity.class);
                    mIntent.putExtra("Model", mInVoicesModel);
                    mIntent.putExtra("mPriviewModelArray", mPriviewModelItemArray);
                    mActivity.startActivity(mIntent);
                    overridePendingTransitionEnter();
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isActualVessel = false;
                if (editInVoiceDateET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_date));
                } else if (editTermsDaysET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_termdays));
                } else if (editSearchCompanyET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_search_company));
                }
//                else if (editSearchVesselsET.getText().toString().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_search_vessels));
//                }
                else if (editCurrencyET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_currency));
                }
//                else if (editStampET.getText().toString().trim().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_stamp));
//                }
//                else if (editSignInvoiceET.getText().toString().trim().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_sign_invoice));
//                }
                else if (itemsArrayList.size() == 0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                }
//                else if (isPaymentUpdated) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_go_to_payment_and_fill_details));
//                }
                else if (editBankET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_bank_details));
                } else {
                    mInVoicesModel.setInvoice_number(strInvoiceNum);
                    mInVoicesModel.setInvoice_date(editInVoiceDateET.getText().toString());
                    mInVoicesModel.setTerm_days(editTermsDaysET.getText().toString());
                    mInVoicesModel.setSearch_company(editSearchCompanyET.getText().toString());
                    mInVoicesModel.setSearch_vessel(editSearchVesselsET.getText().toString());
                    mInVoicesModel.setCurrency(editCurrencyET.getText().toString());
                    mInVoicesModel.setStamp(editStampET.getText().toString());
                    mInVoicesModel.setSign_invoice(editSignInvoiceET.getText().toString());
                    mInVoicesModel.setStatus(editStatusET.getText().toString());
                    mInVoicesModel.setRefrence1(editRefrance1ET.getText().toString());
                    mInVoicesModel.setRefrence2(editRefrance2ET.getText().toString());
                    mInVoicesModel.setRefrence3(editRefrance3ET.getText().toString());

                    mInVoicesModel.setmItemModelArrayList(itemsArrayList);

                    if (mCompaniesModel != null)
                        mInVoicesModel.setmCompaniesModel(mCompaniesModel);
                    if (mStampsModel != null)
                        mInVoicesModel.setmStampsModel(mStampsModel);
                    if (mSignatureModel != null)
                        mInVoicesModel.setmSignatureModel(mSignatureModel);
                    if (mBankModel != null)
                        mInVoicesModel.setmBankModel(mBankModel);
                    if (mVesselInvoiceModel != null)
                        mInVoicesModel.setmVesselSearchInvoiceModel(mVesselInvoiceModel);
//                    mInVoicesModel.setInvoice_number(editInVoiceNumET.getText().toString());
//                    mInVoicesModel.setInvoice_date(editInVoiceDateET.getText().toString());
//                    mInVoicesModel.setTerm_days(editTermsDaysET.getText().toString());
//                    mInVoicesModel.setSearch_company(editSearchCompanyET.getText().toString());
//                    mInVoicesModel.setSearch_vessel(editSearchVesselsET.getText().toString());
//                    mInVoicesModel.setCurrency(editCurrencyET.getText().toString());
//                    mInVoicesModel.setStamp(editStampET.getText().toString());
//                    mInVoicesModel.setSign_invoice(editSignInvoiceET.getText().toString());
//                    mInVoicesModel.setStatus(editStatusET.getText().toString());
//                    mInVoicesModel.setRefrence1(editRefrance1ET.getText().toString());
//                    mInVoicesModel.setRefrence2(editRefrance2ET.getText().toString());
//                    mInVoicesModel.setRefrence3(editRefrance3ET.getText().toString());

//                    mInVoicesModel.setmItemModelArrayList(itemsArrayList);

//                    if (mCompaniesModel != null)
//                        mInVoicesModel.setmCompaniesModel(mCompaniesModel);

//                    if (mStampsModel != null)
//                        mInVoicesModel.setmStampsModel(mStampsModel);

//                    if (mSignatureModel != null)
//                        mInVoicesModel.setmSignatureModel(mSignatureModel);

//                    if (mBankModel != null)
//                        mInVoicesModel.setmBankModel(mBankModel);

//                    if (mVesselSearchModel != null)
//                        mInVoicesModel.setmVesselSearchInvoiceModel(mVesselSearchModel);

//                    if (mPaymentModel != null)
//                        mInVoicesModel.setmPaymentModel(mPaymentModel);
                    if (JaoharConstants.IS_Payment_Update == true) {
                        JaoharConstants.IS_Payment_Update = false;
                        mInVoicesModel.setmPaymentModel(mPaymentModelP);
                    } else {
                        if (mPaymentModelP != null) {
                            if (!mPaymentModelP.getSubTotal().equals("")) {
                                mSubTotal1 = Double.parseDouble(mPaymentModelP.getSubTotal());
                                strPaymentID = mPaymentModelP.getPayment_id();
                                if (mPaymentModelP.getPaid() != null) {
                                    strPAID = mPaymentModelP.getPaid().toString();
                                }
                                if (mPaymentModelP.getVAT() != null) {
                                    strVAt = mPaymentModelP.getVAT().toString();
                                }
                            }
                        }
                        if (itemsArrayList.size() != 0) {
                            double mAmount1 = 0;
                            for (int i = 0; i < itemsArrayList.size(); i++) {
                                if (i == 0) {
                                    mAmount1 = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                                    mSubTotal1 = mAmount1;
                                    mSubTotal1 = Utilities.getRoundOff2Decimal(mSubTotal1);
                                } else {
                                    mAmount1 = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                                    mSubTotal1 = mSubTotal1 + mAmount1;
                                    mSubTotal1 = Utilities.getRoundOff2Decimal(mSubTotal1);
                                }
                            }
                            String strValue = strVAt;
                            if (strValue.length() > 0) {
                                Float mVat = Float.parseFloat(strVAt);
                                mVatCost = (mSubTotal1 * mVat) / 100;
                                mTotal = mSubTotal1 + mVatCost;
                                mTotal = Utilities.getRoundOff2Decimal(mTotal);
                                mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                                mBalanceDue = mTotal;
//                                txtVatPriceTV.setText("" + mVatCost);
//                                txtTotalTV.setText("" + mTotal);
//                                txtBalaceDueTV.setText("" + mTotal);
//                                txtSubTotalTV.setText("" + mSubTotal);
                            }
                            String strValue1 = strPAID;
                            if (strValue1.length() > 0) {
                                mPaid = Double.parseDouble(strPAID);
                                mBalanceDue = mTotal - mPaid;
                                mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
//                                txtSubTotalTV.setText("" + mSubTotal);
                            }
                            Log.e(TAG, String.valueOf(mSubTotal1));
                        }
//                        } else {
                        if (mArrayTotalDiscount.size() != 0) {
                            for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                                AddTotalInvoiceDiscountModel mDiscountModel = mArrayTotalDiscount.get(i);
                                if (mDiscountModel.getDiscountType().equals("Add Percentage")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());

                                } else if (mDiscountModel.getDiscountType().equals("Subtract Percentage")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Subtract Value")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 - Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                } else if (mDiscountModel.getDiscountType().equals("Add Value")) {
                                    mTotal1 = Double.parseDouble(String.valueOf(mDiscountModel.getADD_UnitPrice()));
                                    mTotalASDisc1 = mTotalASDisc1 + Double.parseDouble(mDiscountModel.getStrAddAndSubtracttotalPercent());
                                }
                                Log.e(TAG, "***Final Total***" + mTotalASDisc1);
                            }
                            mSubTotal1 = Utilities.getRoundOff2Decimal(mTotal1 + mTotalASDisc1);
                            mTotalASDisc1 = 0;
                            String strValue = strVAt;
                            if (strValue.length() > 0) {
                                Float mVat = Float.parseFloat(strVAt);
                                mVatCost = (mSubTotal1 * mVat) / 100;
                                mTotal = mSubTotal1 + mVatCost;
                                mTotal = Utilities.getRoundOff2Decimal(mTotal);
                                mVatCost = Utilities.getRoundOff2Decimal(mVatCost);
                                mBalanceDue = mTotal;
//                                txtVatPriceTV.setText("" + mVatCost);
//                                txtTotalTV.setText("" + mTotal);
//                                txtBalaceDueTV.setText("" + mTotal);
//                                txtSubTotalTV.setText("" + mSubTotal);
                            }
                            String strValue1 = strPAID;
                            if (strValue1.length() > 0) {
                                mPaid = Double.parseDouble(strPAID);
                                mBalanceDue = mTotal - mPaid;
                                mBalanceDue = Utilities.getRoundOff2Decimal(mBalanceDue);
//                                txtSubTotalTV.setText("" + mSubTotal);
                            }
                            Log.e(TAG, String.valueOf(mSubTotal1));
                        }
                        mPaymentModelP.setSubTotal("" + mSubTotal1);
                        mPaymentModelP.setVAT(strVAt);
                        mPaymentModelP.setPaid(strPAID);
                        mPaymentModelP.setTotal("" + mTotal);
                        mPaymentModelP.setBalanceDue("" + mBalanceDue);
                        mPaymentModelP.setVATPrice("" + mVatCost);
                        mPaymentModelP.setPayment_id(strPaymentID);
                        mInVoicesModel.setmPaymentModel(mPaymentModelP);
                    }
                }

                mPriviewModelItemArray.clear();
                if (itemsArrayList.size() != 0) {
                    for (int i = 0; i < itemsArrayList.size(); i++) {
                        PriviewModelItems mModel = new PriviewModelItems();
                        double intItems = itemsArrayList.get(i).getQuantity();
                        double intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getUnitprice());
                        double mAmount = intItems * intUnitPrice;
                        mModel.setStrAmount(String.valueOf(mAmount));
                        mModel.setStrDescription(itemsArrayList.get(i).getDescription());
                        mModel.setStrItem(itemsArrayList.get(i).getItem());
                        mModel.setStrQuantity(String.valueOf(itemsArrayList.get(i).getQuantity()));
                        mModel.setStrUnitPrice(String.valueOf(itemsArrayList.get(i).getUnitprice()));
                        mPriviewModelItemArray.add(mModel);
                        for (int k = 0; k < itemsArrayList.get(i).getmDiscountModelArrayList().size(); k++) {
                            String strType = itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getType();
                            if (strType.equals("ADD")) {
                                PriviewModelItems mModel1 = new PriviewModelItems();
                                mModel1.setStrItem("w");
                                mModel1.setStrQuantity("1");
                                mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());
                                mPriviewModelItemArray.add(mModel1);
                            } else if (strType.equals("ADD_VALUE")) {
                                PriviewModelItems mModel1 = new PriviewModelItems();
                                mModel1.setStrItem("w");
                                mModel1.setStrQuantity("1");
                                mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getAdd_description());
                                mPriviewModelItemArray.add(mModel1);
                            } else if (strType.equals("SUBTRACT")) {
                                PriviewModelItems mModel1 = new PriviewModelItems();
                                mModel1.setStrItem("w");
                                mModel1.setStrQuantity("1");
                                if (itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue().contains("-")) {
                                    mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                    mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                } else {
                                    mModel1.setStrAmount("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                    mModel1.setStrUnitPrice("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                }
                                mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                                mPriviewModelItemArray.add(mModel1);
                            } else if (strType.equals("SUBTRACT_VALUE")) {
                                PriviewModelItems mModel1 = new PriviewModelItems();
                                mModel1.setStrItem("w");
                                mModel1.setStrQuantity("1");
                                if (itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue().contains("-")) {
                                    mModel1.setStrUnitPrice(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                    mModel1.setStrAmount(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                } else {
                                    mModel1.setStrUnitPrice("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                    mModel1.setStrAmount("-" + itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getStrtotalPercentValue());
                                }
                                mModel1.setStrDescription(itemsArrayList.get(i).getmDiscountModelArrayList().get(k).getSubtract_description());
                                mPriviewModelItemArray.add(mModel1);
                            }
                        }
                    }
                    if (mArrayTotalDiscount.size() != 0) {
                        for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                            String strType = mArrayTotalDiscount.get(i).getDiscountType();
                            if (strType.equals("Add Percentage")) {
                                PriviewModelItems mModel = new PriviewModelItems();
                                mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                                mModel.setStrItem("w");
                                mModel.setStrQuantity("1");
                                mPriviewModelItemArray.add(mModel);
                            } else if (strType.equals("Add Value")) {
                                PriviewModelItems mModel = new PriviewModelItems();
                                mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                mModel.setStrDescription(mArrayTotalDiscount.get(i).getADD_Description());
                                mModel.setStrItem("w");
                                mModel.setStrQuantity("1");
                                mPriviewModelItemArray.add(mModel);
                            } else if (strType.equals("Subtract Percentage")) {
                                PriviewModelItems mModel = new PriviewModelItems();
                                if (mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent().contains("-")) {
                                    mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                } else {
                                    mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                }
                                mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                                mModel.setStrItem("w");
                                mModel.setStrQuantity("1");
                                mPriviewModelItemArray.add(mModel);
                            } else if (strType.equals("Subtract Value")) {
                                PriviewModelItems mModel = new PriviewModelItems();
                                if (mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent().contains("-")) {
                                    mModel.setStrUnitPrice(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrAmount(mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                } else {
                                    mModel.setStrUnitPrice("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                    mModel.setStrAmount("-" + mArrayTotalDiscount.get(i).getStrAddAndSubtracttotalPercent());
                                }
                                mModel.setStrDescription(mArrayTotalDiscount.get(i).getSubtract_Description());
                                mModel.setStrItem("w");
                                mModel.setStrQuantity("1");
                                mPriviewModelItemArray.add(mModel);
                            }
                        }
                    }

                    JaoharConstants.IS_Click_From_Edit = false;
                    JaoharConstants.IS_Click_From_Copy = true;
                    JaoharConstants.Universal_Invoice_ID = strInvoiceID;
                    executeCopyInVoicesAPI();
//                    Log.e("TAGSIZE", String.valueOf(mPriviewModelItemArray.size()));
//                    Intent mIntent = new Intent(mActivity, UploadInvoiceODF.class);
//                    mIntent.putExtra("Model", mInVoicesModel);
//                    mIntent.putExtra("ItemArray", itemIDARRAY);
//                    mIntent.putExtra("TotalDiscountArray", totalDiscountIDARRAY);
//                    mIntent.putExtra("mPriviewModelArray", mPriviewModelItemArray);
//                    mIntent.putExtra("mItemModelArray", itemsArrayList);
//                    mIntent.putExtra("mTotalDiscountModelArray", mArrayTotalDiscount);
//                    mActivity.startActivity(mIntent);
//                    finish();
                }
            }
        });
    }


    private JsonObject mCopyParam() {
        JSONObject jsonObject = new JSONObject();
        itemsJsonArray = gettingItemIDsArray(itemsArrayList);
        itemsDiscountJsonArray = gettingItemDiscounArray(mArrayTotalDiscount);
        try {
            jsonObject.put("owner_id", mInVoicesModel.getOwner_id());
            jsonObject.put("invoice_no", prefix+data);
            jsonObject.put("term_days", mInVoicesModel.getTerm_days());
            jsonObject.put("invoice_date", mInVoicesModel.getInvoice_date());
            jsonObject.put("search_company", strCompanyId);
            jsonObject.put("search_vessel", strVessalID);
            jsonObject.put("currency", mInVoicesModel.getCurrency());
            jsonObject.put("stamp", strStamPId);
            jsonObject.put("sign", strSignatireId);
            jsonObject.put("status", mInVoicesModel.getStatus());
            jsonObject.put("reference", mInVoicesModel.getRefrence1());
            jsonObject.put("reference1", mInVoicesModel.getRefrence2());
            jsonObject.put("reference2", mInVoicesModel.getRefrence3());
            jsonObject.put("bank_details", strBankId);
            jsonObject.put("payment_id", mInVoicesModel.getmPaymentModel().getPayment_id());
            jsonObject.put("items", itemsJsonArray);
            jsonObject.put("itemdiscount", itemsDiscountJsonArray);
            jsonObject.put("sub_total", mInVoicesModel.getmPaymentModel().getSubTotal());
            jsonObject.put("VAT", mInVoicesModel.getmPaymentModel().getVAT());
            jsonObject.put("vat_price", mInVoicesModel.getmPaymentModel().getVATPrice());
            jsonObject.put("total", mInVoicesModel.getmPaymentModel().getTotal());
            jsonObject.put("paid", mInVoicesModel.getmPaymentModel().getPaid());
            jsonObject.put("due", mInVoicesModel.getmPaymentModel().getBalanceDue());
            jsonObject.put("previous_items", String.valueOf(itemIDARRAY));
            jsonObject.put("previous_discounts", String.valueOf(totalDiscountIDARRAY));
            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
            Log.e(TAG, "**PARAM**" + jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
        return gsonObject;
    }

    public void executeCopyInVoicesAPI() {
        if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
            strVessalID = mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_id();
        } else {
            strVessalID = "";
        }
        if (mInVoicesModel.getmStampsModel() != null) {
            strStamPId = mInVoicesModel.getmStampsModel().getId();
        } else {
            strStamPId = "";
        }
        if (mInVoicesModel.getmSignatureModel() != null) {
            strSignatireId = mInVoicesModel.getmSignatureModel().getId();
        } else {
            strSignatireId = "";
        }
        if (mInVoicesModel.getmBankModel() != null) {
            strBankId = mInVoicesModel.getmBankModel().getId();
        } else {
            strBankId = "";
        }
        if (mInVoicesModel.getmCompaniesModel() != null) {
            strCompanyId = mInVoicesModel.getmCompaniesModel().getId();
        } else {
            strCompanyId = "";
        }

        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<InvoiceModel> call1 = mApiInterface.copyUniversalInvoiceRequestNew(mCopyParam());
        call1.enqueue(new Callback<InvoiceModel>() {
            @Override
            public void onResponse(Call<InvoiceModel> call, retrofit2.Response<InvoiceModel> response) {
                AlertDialogManager.hideProgressDialog();
                InvoiceModel mModel = response.body();
                if (mModel.getStatus().equals("1") || mModel.getStatus().equals("200")) {
                    Intent mIntent = new Intent(mActivity, DetailUniversalInvoiceActivity.class);
                    mIntent.putExtra("id", mInVoicesModel.getInvoice_id());
                    mIntent.putExtra("universal", "true");
                    mActivity.startActivity(mIntent);
                    finish();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<InvoiceModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void executeGettingDataApi(final InVoicesModel mInVoicesModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllInvoiceDataCopyUniversalRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), JaoharConstants.Universal_Invoice_ID);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    if (mJsonObject.getString("status").equals("1")) {
                        setPrevousDatatoEditonWidgets(mJsonObject);
                    } else if (mJsonObject.getString("status").equals("100")) {
                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
                    } else if (mJsonObject.getString("status").equals("0")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (JaoharConstants.IS_EDIT_ITEM_CLICK == true) {
            JaoharConstants.IS_EDIT_ITEM_CLICK = false;
//            if (getIntent() != null) {
//                if ( getIntent().getExtras().getBoolean("IS_EDIT") == true) {
//                    mInvoiceAddItemModel = (InvoiceAddItemModel) getIntent().getSerializableExtra("Model");
//                    itemsArrayList.set(mSelectedItemPosition, mInvoiceAddItemModel);
//                    mInVoicesModel.setmItemModelArrayList(itemsArrayList);
//                    mInVoiceItemsAdapter.notifyDataSetChanged();
//                }
//            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check that it is the Activity with an OK result
        if (requestCode == 111) {

            if (data != null && data.getSerializableExtra("Model") != null) {
                mInvoiceAddItemModel = (InvoiceAddItemModel) data.getSerializableExtra("Model");
//                if (data.getIntExtra("Position", 0) == mSelectedItemPosition) {
                isSelectedItemPosition = true;
                itemsArrayList.set(mSelectedItemPosition, mInvoiceAddItemModel);
                if (itemsArrayList.size() > 0) {
                    setAdapter();
                }
                if (mArrayTotalDiscount.size() != 0) {
                    mArrayTotalDiscountNEW = new ArrayList<AddTotalInvoiceDiscountModel>();

                    for (int i = 0; i < itemsArrayList.size(); i++) {
                        intItems = itemsArrayList.get(i).getQuantity();
                        intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                        if (i == 0) {
//                        mAmount = intItems * intUnitPrice;
                            mAmount = intUnitPrice;
                            mSubTotal = mAmount;
                            mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                        } else {
                            mAmount = intUnitPrice;
                            mSubTotal = mSubTotal + mAmount;
                            mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                        }
                    }

                    Log.e("TAg", String.valueOf(mSubTotal));
                    Log.e("TAg", String.valueOf(mInvoiceAddItemModel.getmDiscountModelArrayList().size()));

                    for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                        isDiscountPosition = true;
                        mTotalDiscountModelNew = new AddTotalInvoiceDiscountModel();
                        String strType = mArrayTotalDiscount.get(i).getDiscountType();
                        if (strType.equals("Add Percentage")) {
                            double basePrice = 0;
                            double sum;
                            double mSubTotal1;
//                          mSubTotal1 =  Double.parseDouble(mInvoiceAddItemModel.getmDiscountModelArrayList().get(i).getADD_unitPrice());
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                            double totalPercent;
                            totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                            sum = basePrice + totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);

                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                            mTotalDiscountModelNew.setADD_DiscountValue(str);
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
//                          txtTotalTV.setText("" + sum);
//                          strTOTALValue = String.valueOf(sum);
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                            setAdapterTotalDiscount();
                        } else if (strType.equals("Add Value")) {
                            double basePrice = 0;
                            double sum;
                            double mSubTotal1;
//                          mSubTotal1 =  Double.parseDouble(mInvoiceAddItemModel.getmDiscountModelArrayList().get(i).getADD_unitPrice());
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                            double totalPercent;
                            totalPercent = Double.parseDouble(str);
                            sum = basePrice + totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);

                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                            mTotalDiscountModelNew.setADD_DiscountValue(str);
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
//                          txtTotalTV.setText("" + sum);
//                          strTOTALValue = String.valueOf(sum);
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                            setAdapterTotalDiscount();
                        } else if (strType.equals("Subtract Percentage")) {
                            double basePrice = 0;
                            double sum;
                            double mSubTotal1;
//                          mSubTotal1 =  Double.parseDouble(mInvoiceAddItemModel.getmDiscountModelArrayList().get(i).getSubtract_unitPrice());
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                            double totalPercent;
                            totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                            sum = basePrice - totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                            mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                            setAdapterTotalDiscount();
                        } else if (strType.equals("Subtract Value")) {
                            double basePrice = 0;
                            double sum;
                            double mSubTotal1;
//                          mSubTotal1 =  Double.parseDouble(mInvoiceAddItemModel.getmDiscountModelArrayList().get(i).getSubtract_unitPrice());
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                            float percentValue = Float.parseFloat(str);
                            double totalPercent;
                            totalPercent = basePrice - percentValue;
                            totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                            sum = totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                            mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);

                            setAdapterTotalDiscount();
                        }
                    }
//                  mArrayTotalDiscount.clear();
//                  setAdapterTotalDiscount();
                }
            }
//            }
        }

        if (requestCode == 101) {

            if (data != null && data.getSerializableExtra("Model") != null) {
                mInvoiceAddItemModel = (InvoiceAddItemModel) data.getSerializableExtra("Model");
                mInvoiceAddItemModel.setTotalAmount(String.valueOf(Double.parseDouble(mInvoiceAddItemModel.getUnitprice()) * mInvoiceAddItemModel.getQuantity()));
                itemsArrayList.add(mInvoiceAddItemModel);
                if (itemsArrayList.size() > 0) {
                    setAdapter();
                }

                if (mArrayTotalDiscount.size() != 0) {
                    mArrayTotalDiscountNEW = new ArrayList<AddTotalInvoiceDiscountModel>();

                    for (int i = 0; i < itemsArrayList.size(); i++) {
                        intItems = itemsArrayList.get(i).getQuantity();
                        intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                        if (i == 0) {
//                        mAmount = intItems * intUnitPrice;
                            mAmount = intUnitPrice;
                            mSubTotal = mAmount;
                            mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                        } else {
                            mAmount = intUnitPrice;
                            mSubTotal = mSubTotal + mAmount;
                            mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                        }
                    }
                    Log.e("TAg", String.valueOf(mSubTotal));
                    Log.e("TAg", String.valueOf(mInvoiceAddItemModel.getmDiscountModelArrayList().size()));

                    for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                        isDiscountPosition = true;
                        mTotalDiscountModelNew = new AddTotalInvoiceDiscountModel();
                        String strType = mArrayTotalDiscount.get(i).getDiscountType();
                        if (strType.equals("Add Percentage")) {
                            double basePrice = 0;
                            double sum;
                            double mSubTotal1;
//                          mSubTotal1 =  Double.parseDouble(mInvoiceAddItemModel.getmDiscountModelArrayList().get(i).getADD_unitPrice());
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                            double totalPercent;
                            totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                            sum = basePrice + totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                            mTotalDiscountModelNew.setADD_DiscountValue(str);
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
//                          txtTotalTV.setText("" + sum);
//                          strTOTALValue = String.valueOf(sum);
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);

                            setAdapterTotalDiscount();
                        } else if (strType.equals("Add Value")) {
                            double basePrice = 0;
                            double sum;
                            double mSubTotal1;
//                          mSubTotal1 =  Double.parseDouble(mInvoiceAddItemModel.getmDiscountModelArrayList().get(i).getADD_unitPrice());
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                            double totalPercent;
                            totalPercent = Double.parseDouble(str);
                            sum = basePrice + totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                            mTotalDiscountModelNew.setADD_DiscountValue(str);
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
//                          txtTotalTV.setText("" + sum);
//                          strTOTALValue = String.valueOf(sum);
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);

                            setAdapterTotalDiscount();
                        } else if (strType.equals("Subtract Percentage")) {
                            double basePrice = 0;
                            double sum;
                            double mSubTotal1;
//                          mSubTotal1 =  Double.parseDouble(mInvoiceAddItemModel.getmDiscountModelArrayList().get(i).getSubtract_unitPrice());
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                            double totalPercent;
                            totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                            sum = basePrice - totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                            mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                            mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                            setAdapterTotalDiscount();
                        } else if (strType.equals("Subtract Value")) {
                            double basePrice = 0;
                            double sum;
                            double mSubTotal1;
//                          mSubTotal1 =  Double.parseDouble(mInvoiceAddItemModel.getmDiscountModelArrayList().get(i).getSubtract_unitPrice());
                            basePrice = mSubTotal;//   Integer.parseInt()
                            String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                            float percentValue = Float.parseFloat(str);
                            double totalPercent;
                            totalPercent = basePrice - percentValue;
                            totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                            sum = totalPercent;

                            mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                            mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                            mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                            mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                            mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                            mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                            mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                            setAdapterTotalDiscount();
                        }

                    }

//                  mArrayTotalDiscount.clear();
//                  setAdapterTotalDiscount();
                }

            }

        }
        if (requestCode == 222) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mCompaniesModel = (CompaniesModel) data.getSerializableExtra("Model");
                if (mCompaniesModel != null) {
                    editSearchCompanyET.setText(mCompaniesModel.getCompany_name());
                    strSearchCompany = mCompaniesModel.getId();
                    mInVoicesModel.setmCompaniesModel(mCompaniesModel);
                }
            }
        }
        if (requestCode == 333) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mStampsModel = (StampsModel) data.getSerializableExtra("Model");
                if (mStampsModel != null) {
                    editStampET.setText(mStampsModel.getStamp_name());
                    strStamp = mStampsModel.getId();
                    mInVoicesModel.setmStampsModel(mStampsModel);
                }
            }
        }
        if (requestCode == 444) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mSignatureModel = (SignatureModel) data.getSerializableExtra("Model");
                if (mSignatureModel != null) {
                    editSignInvoiceET.setText(mSignatureModel.getSignature_name());
                    strSign = mSignatureModel.getId();
                    mInVoicesModel.setmSignatureModel(mSignatureModel);
                }
            }

        }
        if (requestCode == 555) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mBankModel = (BankModel) data.getSerializableExtra("Model");
                if (mBankModel != null) {
                    editBankET.setText(mBankModel.getBenificiary());
                    strBankDetails = mBankModel.getId();
                    mInVoicesModel.setmBankModel(mBankModel);

                }
            }

        }
        if (requestCode == 666) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mVesselInvoiceModel = (VesselSearchInvoiceModel) data.getSerializableExtra("Model");
                if (mVesselInvoiceModel != null) {
                    editSearchVesselsET.setText(mVesselInvoiceModel.getVessel_name());
                    strSearchVessel = mVesselInvoiceModel.getVessel_id();
                    mInVoicesModel.setmVesselSearchInvoiceModel(mVesselInvoiceModel);
                }
            }
        }
        if (requestCode == 777) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mCurrenciesModel = (CurrenciesModel) data.getSerializableExtra("Model");
                if (mCurrenciesModel != null) {
                    editCurrencyET.setText(mCurrenciesModel.getCurrency_name());
                    mInVoicesModel.setmCurrenciesModel(mCurrenciesModel);
                }

            }
        }

        if (requestCode == 888) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                isPaymentUpdated = false;
                mPaymentModelP = (PaymentModel) data.getSerializableExtra("Model");
                mInVoicesModel.setmPaymentModel(mPaymentModelP);
            }
        }
        if (requestCode == 999) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                mTotalDiscountModel = (AddTotalInvoiceDiscountModel) data.getSerializableExtra("Model");
                isDiscountPositionNEW = false;
                mArrayTotalDiscount.add(mTotalDiscountModel);
                if (mArrayTotalDiscount.size() > 0)
                    setAdapterTotalDiscount();

            }

        }
        if (requestCode == 1212) {

            if (data != null && data.getSerializableExtra("Model") != null) {
                mTotalDiscountModel = (AddTotalInvoiceDiscountModel) data.getSerializableExtra("Model");
                int postion = data.getIntExtra("Position", 0);
                mArrayTotalDiscount.set(postion, mTotalDiscountModel);
                if (mArrayTotalDiscount.size() > 0)
                    setAdapterTotalDiscount();
            }

        }


    }

    private void setAdapterTotalDiscount() {
        Log.e(TAG, "VesselsAdapter: " + mArrayTotalDiscount.size());
        mRecyclerViewDiscount.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewDiscount.setNestedScrollingEnabled(false);
        mRecyclerViewDiscount.setHasFixedSize(false);
        mRecyclerViewDiscount.setLayoutManager(layoutManager);
        if (isDiscountPosition == true) {
            isDiscountPosition = false;
            isDiscountPositionNEW = true;
            mInvoiceTotalDiscountAdapter = new InvoiceTotalDiscountAdapter(mActivity, mArrayTotalDiscountNEW, mEditInvoiceDiscountItemData);
        } else {
            isDiscountPositionNEW = false;
            mInvoiceTotalDiscountAdapter = new InvoiceTotalDiscountAdapter(mActivity, mArrayTotalDiscount, mEditInvoiceDiscountItemData);
        }

        mRecyclerViewDiscount.setAdapter(mInvoiceTotalDiscountAdapter);
    }

    private void executeDeleteItemInvoice(final InvoiceAddItemModel model) {

        itemsArrayList.remove(model);
        if (itemsArrayList.size() > 0) {
            setAdapter();
        }

        // ***************Array List Refresh after deletion*************
        if (itemsArrayList.size() == 0) {
            mArrayTotalDiscountNEW = new ArrayList<AddTotalInvoiceDiscountModel>();
            itemsArrayList.clear();
            mInVoiceItemsAdapter.notifyDataSetChanged();
            mArrayTotalDiscount.clear();
            if (mArrayTotalDiscountNEW.size() != 0) {
                isDiscountPosition = true;
                mArrayTotalDiscountNEW.clear();
                setAdapterTotalDiscount();
            } else {
                isDiscountPosition = false;
                setAdapterTotalDiscount();
            }


            mInvoiceTotalDiscountAdapter.notifyDataSetChanged();
        }

        // Discount Array list referesh when item is deleted According to new data

        if (mArrayTotalDiscount.size() != 0) {
            mArrayTotalDiscountNEW = new ArrayList<AddTotalInvoiceDiscountModel>();

            for (int i = 0; i < itemsArrayList.size(); i++) {
                intItems = itemsArrayList.get(i).getQuantity();
                intUnitPrice = Double.parseDouble(itemsArrayList.get(i).getTotalAmount());
                if (i == 0) {
//                        mAmount = intItems * intUnitPrice;
                    mAmount = intUnitPrice;
                    mSubTotal = mAmount;
                    mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                } else {
                    mAmount = intUnitPrice;
                    mSubTotal = mSubTotal + mAmount;
                    mSubTotal = Utilities.getRoundOff2Decimal(mSubTotal);
                }
            }

            if (mArrayTotalDiscount != null && mArrayTotalDiscount.size() != 0) {
                for (int i = 0; i < mArrayTotalDiscount.size(); i++) {
                    isDiscountPosition = true;
                    mTotalDiscountModelNew = new AddTotalInvoiceDiscountModel();
                    String strType = mArrayTotalDiscount.get(i).getDiscountType();
                    if (strType.equals("Add Percentage")) {
                        isDiscountPosition = true;
                        double basePrice = 0;
                        double sum;
                        double mSubTotal1;
                        basePrice = mSubTotal;//   Integer.parseInt()
                        String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                        double totalPercent;
                        totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                        sum = basePrice + totalPercent;
                        mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                        mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);
                        mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                        mTotalDiscountModelNew.setADD_DiscountValue(str);
                        mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());

                        mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                        mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                        setAdapterTotalDiscount();
                    } else if (strType.equals("Add Value")) {
                        isDiscountPosition = true;
                        double basePrice = 0;
                        double sum;
                        double mSubTotal1;
                        basePrice = mSubTotal;//   Integer.parseInt()
                        String str = mArrayTotalDiscount.get(i).getADD_DiscountValue();
                        double totalPercent;
                        totalPercent = Double.parseDouble(str);
                        sum = basePrice + totalPercent;
                        mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                        mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        mTotalDiscountModelNew.setADD_UnitPrice(mSubTotal);
                        mTotalDiscountModelNew.setADD_Description(mArrayTotalDiscount.get(i).getADD_Description());
                        mTotalDiscountModelNew.setADD_DiscountValue(str);
                        mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());

                        mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                        mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                        setAdapterTotalDiscount();
                    } else if (strType.equals("Subtract Percentage")) {
                        isDiscountPosition = true;
                        double basePrice = 0;
                        double sum;
                        double mSubTotal1;
                        basePrice = mSubTotal;//   Integer.parseInt()
                        String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                        double totalPercent;
                        totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
                        sum = basePrice - totalPercent;
                        mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                        mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(String.valueOf(totalPercent));
                        mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                        mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                        mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                        mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                        mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                        mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                        setAdapterTotalDiscount();
                    } else if (strType.equals("Subtract Value")) {
                        isDiscountPosition = true;
                        double basePrice = 0;
                        double sum;
                        double mSubTotal1;
                        basePrice = mSubTotal;//   Integer.parseInt()
                        String str = mArrayTotalDiscount.get(i).getSubtract_DiscountValue();
                        float percentValue = Float.parseFloat(str);
                        double totalPercent;
                        totalPercent = basePrice - percentValue;
                        totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                        sum = totalPercent;
                        mTotalDiscountModelNew.setAddAndSubtractTotal(sum);
                        mTotalDiscountModelNew.setStrAddAndSubtracttotalPercent(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                        mTotalDiscountModelNew.setSubtract_UnitPrice(mSubTotal);
                        mTotalDiscountModelNew.setSubtract_Description(mArrayTotalDiscount.get(i).getSubtract_Description());
                        mTotalDiscountModelNew.setSubtract_DiscountValue(mArrayTotalDiscount.get(i).getSubtract_DiscountValue());
                        mTotalDiscountModelNew.setDiscountType(mArrayTotalDiscount.get(i).getDiscountType());
                        mArrayTotalDiscountNEW.add(mTotalDiscountModelNew);
                        mArrayTotalDiscount.set(i, mTotalDiscountModelNew);
                        setAdapterTotalDiscount();
                    }
                }
            }

            setAdapterTotalDiscount();
        }

    }

    private void setAdapter() {
        Log.e(TAG, "VesselsAdapter: " + itemsArrayList.size());
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mInVoiceItemsAdapter = new InVoiceItemsAdapter(mActivity, itemsArrayList, mDeleteItemInvoiceInterface, mEditInvoiceItemData);
        mRecyclerView.setAdapter(mInVoiceItemsAdapter);

    }

    private JSONArray gettingItemIDsArray(ArrayList<InvoiceAddItemModel> itemArrayList) {
        JSONArray mJsonArray = new JSONArray();
        try {
            for (int i = 0; i < itemArrayList.size(); i++) {

                JSONObject mJsonObject = new JSONObject();
                InvoiceAddItemModel mModel = itemArrayList.get(i);
                mJsonObject.put("serial_no", mModel.getItem());
                mJsonObject.put("quantity", mModel.getQuantity());
                mJsonObject.put("price", mModel.getUnitprice());
                mJsonObject.put("description", mModel.getDescription());
                mJsonObject.put("TotalAmount", mModel.getTotalAmount());
                mJsonObject.put("TotalunitPrice", mModel.getStrTotalunitPrice());
                mJsonObject.put("TotalAmountUnit", mModel.getStrTotalAmountUnit());
                mJsonObject.put("arraylistDiscount", getJSONArray(mModel.getmDiscountModelArrayList()));
                mJsonArray.put(mJsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mJsonArray;
    }

    private JSONArray getJSONArray(ArrayList<DiscountModel> mDiscountModelArrayList) {
        JSONArray mJsonArray = new JSONArray();
        try {
            for (int i = 0; i < mDiscountModelArrayList.size(); i++) {
                DiscountModel mDiscount = mDiscountModelArrayList.get(i);
                JSONObject mJsonObject = new JSONObject();
                mJsonObject.put("ADD_items", mDiscount.getADD_items());
                mJsonObject.put("ADD_quantity", mDiscount.getADD_quantity());
                mJsonObject.put("Add_description", mDiscount.getAdd_description());
                mJsonObject.put("ADD_unitPrice", mDiscount.getADD_unitPrice());
                mJsonObject.put("Add_Value", mDiscount.getAdd_Value());
                mJsonObject.put("Type", mDiscount.getType());
                mJsonObject.put("Subtract_items", mDiscount.getSubtract_items());
                mJsonObject.put("Subtract_quantity", mDiscount.getSubtract_quantity());
                mJsonObject.put("Subtract_description", mDiscount.getSubtract_description());
                mJsonObject.put("Subtract_unitPrice", mDiscount.getSubtract_unitPrice());
                mJsonObject.put("Subtract_Value", mDiscount.getSubtract_Value());
                mJsonObject.put("totalPercentValue", mDiscount.getStrtotalPercentValue());
                mJsonObject.put("addAndSubtractTotal", mDiscount.getAddAndSubtractTotal());
                mJsonObject.put("TotalEditAmount", mDiscount.getStrTotalEditAmount());
                mJsonArray.put(mJsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mJsonArray;
    }

    private JSONArray gettingItemDiscounArray(ArrayList<AddTotalInvoiceDiscountModel> itemArrayList) {
        JSONArray mJsonArray = new JSONArray();
        try {
            for (int i = 0; i < itemArrayList.size(); i++) {
                //mJsonArray.put(itemArrayList.get(i).getItemID());
                JSONObject mJsonObject = new JSONObject();
                AddTotalInvoiceDiscountModel mModel = itemArrayList.get(i);
                mJsonObject.put("discount_add_value", mModel.getADD_DiscountValue());
                mJsonObject.put("discount_type", mModel.getDiscountType());
                mJsonObject.put("discount_add_description", mModel.getADD_Description());
                mJsonObject.put("discount_add_unitprice", mModel.getADD_UnitPrice());
                mJsonObject.put("discount_percent", mModel.getStrAddAndSubtracttotalPercent());
                mJsonObject.put("discount_total_value", mModel.getStrTotalvalue());
                mJsonObject.put("discount_add_and_subtract_total", mModel.getAddAndSubtractTotal());
                mJsonObject.put("discount_subtract_unitprice", mModel.getSubtract_UnitPrice());
                mJsonObject.put("discount_subtract_description", mModel.getSubtract_Description());
                mJsonObject.put("discount_subtract_value", mModel.getSubtract_DiscountValue());
                mJsonArray.put(mJsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mJsonArray;
    }

    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }

    private void setPrevousDatatoEditonWidgets(JSONObject mJsonObject) {
        AlertDialogManager.hideProgressDialog();
        try {
            JSONObject mJson = mJsonObject.getJSONObject("data");
            JSONObject mAllDataObj = mJson.getJSONObject("all_data");
            mInVoicesModel = new InVoicesModel();
            if (!mAllDataObj.getString("invoice_id").equals("")) {
                mInVoicesModel.setInvoice_id(mAllDataObj.getString("invoice_id"));
                strInvoiceID = mAllDataObj.getString("invoice_id");
            }

            if (!mAllDataObj.getString("invoice_no").equals("")) {
                strInvoiceNum = mAllDataObj.getString("invoice_no");
                mInVoicesModel.setInvoice_number(mAllDataObj.getString("invoice_no"));
                invoiceNumET.setText(mAllDataObj.getString("invoice_no"));
            }

            if (!mAllDataObj.getString("invoice_date").equals("")) {
                mInVoicesModel.setInvoice_date(mAllDataObj.getString("invoice_date"));
            }
            if (!mAllDataObj.getString("owner_id").equals("")) {
                mInVoicesModel.setOwner_id(mAllDataObj.getString("owner_id"));
            }

            if (!mAllDataObj.getString("term_days").equals("")) {
                mInVoicesModel.setTerm_days(mAllDataObj.getString("term_days"));
            }

            if (!mAllDataObj.getString("currency").equals("")) {
                mInVoicesModel.setCurrency(mAllDataObj.getString("currency"));
            }

            if (!mAllDataObj.getString("status").equals("")) {
                mInVoicesModel.setStatus(mAllDataObj.getString("status"));
            }

            if (!mAllDataObj.getString("reference").equals("")) {
                mInVoicesModel.setRefrence1(mAllDataObj.getString("reference"));
                editRefrance1ET.setText(mInVoicesModel.getRefrence1());
                editRefrance1ET.setSelection(0, 0);
            }

            if (!mAllDataObj.getString("reference1").equals("")) {
                mInVoicesModel.setRefrence2(mAllDataObj.getString("reference1"));
                editRefrance2ET.setText(mInVoicesModel.getRefrence2());
                editRefrance2ET.setSelection(0, 0);
            }

            if (!mAllDataObj.getString("reference2").equals("")) {
                mInVoicesModel.setRefrence3(mAllDataObj.getString("reference2"));
                editRefrance3ET.setText(mInVoicesModel.getRefrence3());
                editRefrance3ET.setSelection(0, 0);
            }

            if (!mAllDataObj.getString("payment_id").equals("")) {
                mInVoicesModel.setPayment_id(mAllDataObj.getString("payment_id"));
            }

            if (mJson.has("sign_data") && !mJson.getString("sign_data").equals("")) {
                JSONObject mSignDataObj = mJson.getJSONObject("sign_data");
                SignatureModel mSignModel = new SignatureModel();
                if (!mSignDataObj.isNull("sign_id"))
                    mSignModel.setId(mSignDataObj.getString("sign_id"));
                strSign = mSignDataObj.getString("sign_id");
                if (!mSignDataObj.isNull("sign_name"))
                    mSignModel.setSignature_name(mSignDataObj.getString("sign_name"));
                if (!mSignDataObj.isNull("sign_image"))
                    mSignModel.setSignature_image(mSignDataObj.getString("sign_image"));
                mInVoicesModel.setmSignatureModel(mSignModel);
            }
            if (mJson.has("stamp_data") && !mJson.get("stamp_data").equals("")) {
                JSONObject mStampDataObj = mJson.getJSONObject("stamp_data");
                StampsModel mStampsModel = new StampsModel();
                if (!mStampDataObj.isNull("stamp_id"))
                    mStampsModel.setId(mStampDataObj.getString("stamp_id"));
                strStamp = mStampDataObj.getString("stamp_id");
                if (!mStampDataObj.isNull("stamp_name"))
                    mStampsModel.setStamp_name(mStampDataObj.getString("stamp_name"));
                if (!mStampDataObj.isNull("stamp_image"))
                    mStampsModel.setStamp_image(mStampDataObj.getString("stamp_image"));
                mInVoicesModel.setmStampsModel(mStampsModel);
            }
            if (mJson.has("bank_data") && !mJson.getString("bank_data").equals("")) {
                JSONObject mBankDataObj = mJson.getJSONObject("bank_data");
                BankModel mBankModel = new BankModel();
                if (!mBankDataObj.isNull("bank_id"))
                    mBankModel.setId(mBankDataObj.getString("bank_id"));
                strBankDetails = mBankDataObj.getString("bank_id");
                if (!mBankDataObj.isNull("beneficiary"))
                    mBankModel.setBenificiary(mBankDataObj.getString("beneficiary"));
                if (!mBankDataObj.isNull("bank_name"))
                    mBankModel.setBankName(mBankDataObj.getString("bank_name"));
                if (!mBankDataObj.isNull("address1"))
                    mBankModel.setAddress1(mBankDataObj.getString("address1"));
                if (!mBankDataObj.isNull("address2"))
                    mBankModel.setAddress2(mBankDataObj.getString("address2"));
                if (!mBankDataObj.isNull("iban_ron"))
                    mBankModel.setIbanRON(mBankDataObj.getString("iban_ron"));
                if (!mBankDataObj.isNull("iban_usd"))
                    mBankModel.setIbanUSD(mBankDataObj.getString("iban_usd"));
                if (!mBankDataObj.isNull("iban_eur"))
                    mBankModel.setIbanEUR(mBankDataObj.getString("iban_eur"));
                if (!mBankDataObj.isNull("iban_gbp"))
                    mBankModel.setIban_gbp(mBankDataObj.getString("iban_gbp"));
                if (!mBankDataObj.isNull("swift"))
                    mBankModel.setSwift(mBankDataObj.getString("swift"));
                mInVoicesModel.setmBankModel(mBankModel);
            }
            if (mJson.has("search_vessel_data") && !mJson.getString("search_vessel_data").equals("")) {
                JSONObject mSearchVesselObj = mJson.getJSONObject("search_vessel_data");
                VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                if (!mSearchVesselObj.isNull("vessel_id"))
                    mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                strSearchVessel = mSearchVesselObj.getString("vessel_id");
                if (!mSearchVesselObj.isNull("vessel_name"))
                    mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                if (!mSearchVesselObj.isNull("IMO_no"))
                    mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                if (!mSearchVesselObj.isNull("flag"))
                    mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                mInVoicesModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
            }
            if (mJson.has("search_company_data") && !mJson.getString("search_company_data").equals("")) {
                JSONObject mSearchCompanyObj = mJson.getJSONObject("search_company_data");
                CompaniesModel mCompaniesModel = new CompaniesModel();
                if (!mSearchCompanyObj.isNull("id"))
                    mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                strSearchCompany = mSearchCompanyObj.getString("id");
                if (!mSearchCompanyObj.isNull("company_name"))
                    mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                if (!mSearchCompanyObj.isNull("Address1"))
                    mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                if (!mSearchCompanyObj.isNull("Address2"))
                    mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                if (!mSearchCompanyObj.isNull("Address3"))
                    mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                if (!mSearchCompanyObj.isNull("Address4"))
                    mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                if (!mSearchCompanyObj.isNull("Address5"))
                    mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                mInVoicesModel.setmCompaniesModel(mCompaniesModel);
            }
            if (mJson.has("payment_data") && !mJson.getString("payment_data").equals("")) {
                JSONObject mPaymentObject = mJson.getJSONObject("payment_data");
                if (!mPaymentObject.isNull("payment_id"))
                    mPaymentModelP.setPayment_id(mPaymentObject.getString("payment_id"));
                if (!mPaymentObject.isNull("sub_total"))
                    mPaymentModelP.setSubTotal(mPaymentObject.getString("sub_total"));
                if (!mPaymentObject.isNull("VAT"))
                    mPaymentModelP.setVAT(mPaymentObject.getString("VAT"));
                if (!mPaymentObject.isNull("vat_price"))
                    mPaymentModelP.setVATPrice(mPaymentObject.getString("vat_price"));
                if (!mPaymentObject.isNull("total"))
                    mPaymentModelP.setTotal(mPaymentObject.getString("total"));
                if (!mPaymentObject.isNull("paid"))
                    mPaymentModelP.setPaid(mPaymentObject.getString("paid"));
                if (!mPaymentObject.isNull("due"))
                    mPaymentModelP.setBalanceDue(mPaymentObject.getString("due"));
                mInVoicesModel.setmPaymentModel(mPaymentModelP);
            }

            if (mJson.has("items_data") && !mJson.getString("items_data").equals("")) {
                itemsArrayList.clear();

                JSONArray mItemArray = mJson.getJSONArray("items_data");
                for (int i = 0; i < mItemArray.length(); i++) {

                    InvoiceAddItemModel mItemModel = new InvoiceAddItemModel();
                    JSONObject mItemObj = mItemArray.getJSONObject(i);

                    if (!mItemObj.getString("item_id").equals("")) {
                        mItemModel.setItemID(mItemObj.getString("item_id"));
                    }

                    if (!mItemObj.getString("item_serial_no").equals("")) {
                        mItemModel.setItem(mItemObj.getString("item_serial_no"));
                    }
                    if (!mItemObj.getString("quantity").equals("")) {
                        mItemModel.setQuantity(mItemObj.getInt("quantity"));
                    }
                    if (!mItemObj.getString("price").equals("")) {
                        mItemModel.setUnitprice(mItemObj.getString("price"));
                    }
                    if (!mItemObj.getString("description").equals("")) {
                        mItemModel.setDescription(mItemObj.getString("description"));
                    }
                    if (!mItemObj.getString("TotalunitPrice").equals("")) {
                        mItemModel.setStrTotalunitPrice(mItemObj.getString("TotalunitPrice"));
                    }
                    if (!mItemObj.getString("TotalAmount").equals("")) {
                        mItemModel.setTotalAmount(mItemObj.getString("TotalAmount"));
                    }
                    if (!mItemObj.getString("TotalAmountUnit").equals("")) {
                        mItemModel.setStrTotalAmountUnit(mItemObj.getString("TotalAmountUnit"));
                    }
                    if (mItemObj.has("arraylistDiscount") && !mItemObj.getString("arraylistDiscount").equals("")) {
                        ArrayList<DiscountModel> mDiscountModelArrayList = new ArrayList<DiscountModel>();
                        JSONArray mItemDiscountArray = mItemObj.getJSONArray("arraylistDiscount");

                        for (int k = 0; k < mItemDiscountArray.length(); k++) {
                            JSONObject mItemObJDiscount = mItemDiscountArray.getJSONObject(k);
                            DiscountModel mDiscountModel = new DiscountModel();
                            if (!mItemObJDiscount.getString("ADD_items").equals("")) {
                                mDiscountModel.setADD_items(mItemObJDiscount.getString("ADD_items"));
                            }
                            if (!mItemObJDiscount.getString("ADD_quantity").equals("")) {
                                mDiscountModel.setADD_quantity(mItemObJDiscount.getString("ADD_quantity"));
                            }
                            if (!mItemObJDiscount.getString("Add_description").equals("")) {
                                mDiscountModel.setAdd_description(mItemObJDiscount.getString("Add_description"));
                            }
                            if (!mItemObJDiscount.getString("ADD_unitPrice").equals("")) {
                                mDiscountModel.setADD_unitPrice(mItemObJDiscount.getString("ADD_unitPrice"));
                            }
                            if (!mItemObJDiscount.getString("Add_Value").equals("")) {
                                mDiscountModel.setAdd_Value(mItemObJDiscount.getString("Add_Value"));
                            }
                            if (!mItemObJDiscount.getString("Type").equals("")) {
                                mDiscountModel.setType(mItemObJDiscount.getString("Type"));
                            }
                            if (!mItemObJDiscount.getString("Subtract_items").equals("")) {
                                mDiscountModel.setSubtract_items(mItemObJDiscount.getString("Subtract_items"));
                            }
                            if (!mItemObJDiscount.getString("Subtract_quantity").equals("")) {
                                mDiscountModel.setSubtract_quantity(mItemObJDiscount.getString("Subtract_quantity"));
                            }
                            if (!mItemObJDiscount.getString("Subtract_description").equals("")) {
                                mDiscountModel.setSubtract_description(mItemObJDiscount.getString("Subtract_description"));
                            }
                            if (!mItemObJDiscount.getString("Subtract_Value").equals("")) {
                                mDiscountModel.setSubtract_Value(mItemObJDiscount.getString("Subtract_Value"));
                            }
                            if (!mItemObJDiscount.getString("Subtract_unitPrice").equals("")) {
                                mDiscountModel.setSubtract_unitPrice(mItemObJDiscount.getString("Subtract_unitPrice"));
                            }
                            if (!mItemObJDiscount.getString("addAndSubtractTotal").equals("")) {
                                mDiscountModel.setAddAndSubtractTotal(mItemObJDiscount.getString("addAndSubtractTotal"));
                            }
                            if (!mItemObJDiscount.getString("totalPercentValue").equals("")) {
                                mDiscountModel.setStrtotalPercentValue(mItemObJDiscount.getString("totalPercentValue"));
                            }
                            mDiscountModelArrayList.add(mDiscountModel);
                        }
                        mItemModel.setmDiscountModelArrayList(mDiscountModelArrayList);
                    }
                    itemsArrayList.add(mItemModel);
                    mInVoicesModel.setmItemModelArrayList(itemsArrayList);
                }
            }
            if (mJson.has("itemdiscount") && !mJson.getString("itemdiscount").equals("")) {
                mArrayTotalDiscount.clear();
                totalDiscountIDARRAY.clear();

                JSONArray mItemDiscountArray = mJson.getJSONArray("itemdiscount");

                for (int k = 0; k < mItemDiscountArray.length(); k++) {
                    AddTotalInvoiceDiscountModel mTotalDiscountModel = new AddTotalInvoiceDiscountModel();
                    JSONObject mItemObJDiscount = mItemDiscountArray.getJSONObject(k);

                    if (!mItemObJDiscount.getString("discount_id").equals("")) {
                        mTotalDiscountModel.setDiscount_id(mItemObJDiscount.getString("discount_id"));
                        totalDiscountIDARRAY.add(mItemObJDiscount.getString("discount_id"));
                    }
                    if (!mItemObJDiscount.getString("discount_subtract_description").equals("")) {
                        mTotalDiscountModel.setSubtract_Description(mItemObJDiscount.getString("discount_subtract_description"));
                    }
                    if (!mItemObJDiscount.getString("discount_subtract_value").equals("")) {
                        mTotalDiscountModel.setSubtract_DiscountValue(mItemObJDiscount.getString("discount_subtract_value"));
                    }
                    if (!mItemObJDiscount.getString("discount_subtract_unitprice").equals("")) {
                        mTotalDiscountModel.setSubtract_UnitPrice(Double.parseDouble(mItemObJDiscount.getString("discount_subtract_unitprice")));
                    }
                    if (!mItemObJDiscount.getString("discount_add_value").equals("")) {
                        mTotalDiscountModel.setADD_DiscountValue(mItemObJDiscount.getString("discount_add_value"));
                    }
                    if (!mItemObJDiscount.getString("discount_add_description").equals("")) {
                        mTotalDiscountModel.setADD_Description(mItemObJDiscount.getString("discount_add_description"));
                    }
                    if (!mItemObJDiscount.getString("discount_add_unitprice").equals("")) {

                        mTotalDiscountModel.setADD_UnitPrice(Double.parseDouble(mItemObJDiscount.getString("discount_add_unitprice")));
                    }
                    if (!mItemObJDiscount.getString("discount_total_value").equals("")) {
                        mTotalDiscountModel.setStrTotalvalue(mItemObJDiscount.getString("discount_total_value"));
                    }
                    if (!mItemObJDiscount.getString("discount_type").equals("")) {
                        mTotalDiscountModel.setDiscountType(mItemObJDiscount.getString("discount_type"));
                    }
                    if (!mItemObJDiscount.getString("discount_percent").equals("")) {
                        mTotalDiscountModel.setStrAddAndSubtracttotalPercent(mItemObJDiscount.getString("discount_percent"));
                    }
                    if (!mItemObJDiscount.getString("discount_add_and_subtract_total").equals("")) {
                        mTotalDiscountModel.setAddAndSubtractTotal(Double.parseDouble(mItemObJDiscount.getString("discount_add_and_subtract_total")));
                    }
                    mArrayTotalDiscount.add(mTotalDiscountModel);
                    isDiscountPositionNEW = false;
                    setAdapterTotalDiscount();
                }
            }

            mInVoicesModel.setmItemModelArrayList(itemsArrayList);
            editInVoiceNumET.setText(mInVoicesModel.getInvoice_number());
            editInVoiceNumET.setSelection(0, 0);
            editInVoiceDateET.setText(mInVoicesModel.getInvoice_date());
            editInVoiceDateET.setSelection(0, 0);
            editTermsDaysET.setText(mInVoicesModel.getTerm_days());
            editTermsDaysET.setSelection(0, 0);
            if (mInVoicesModel.getmCompaniesModel() != null) {
                editSearchCompanyET.setText(mInVoicesModel.getmCompaniesModel().getCompany_name());
                editSearchCompanyET.setSelection(0, 0);
            }
            if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
                editSearchVesselsET.setText(mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_name());
                editSearchVesselsET.setSelection(0, 0);
            }
            editCurrencyET.setText(mInVoicesModel.getCurrency());
            editCurrencyET.setSelection(0, 0);
            if (mInVoicesModel.getmStampsModel() != null) {
                editStampET.setText(mInVoicesModel.getmStampsModel().getStamp_name());
                editStampET.setSelection(0, 0);
            }
            if (mInVoicesModel.getmSignatureModel() != null) {
                editSignInvoiceET.setText(mInVoicesModel.getmSignatureModel().getSignature_name());
                editSignInvoiceET.setSelection(0, 0);
            }
            if (mInVoicesModel.getStatus() != null) {
                editStatusET.setText(mInVoicesModel.getStatus());
                editStatusET.setSelection(0, 0);
            }
            if (mInVoicesModel.getmBankModel() != null) {
                editBankET.setText(mInVoicesModel.getmBankModel().getBenificiary());
                editBankET.setSelection(0, 0);
            }
            if (mInVoicesModel.getmItemModelArrayList().size() > 0)
                setAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openDeleteDialog(final EditText editText, final String strName) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_stamp);
        TextView txt_Choose = (TextView) dialog.findViewById(R.id.txt_Choose);
        final TextView txt_Delete = (TextView) dialog.findViewById(R.id.txt_Delete);
        final TextView txt_Cancel = (TextView) dialog.findViewById(R.id.txt_Cancel);

        if (strName.equals("sign")) {
            txt_Choose.setText("Choose Signature");
            txt_Delete.setText("Delete Signature");
        } else if (strName.equals("stamp")) {
            txt_Choose.setText("Choose Stamp");
            txt_Delete.setText("Delete Stamp");
        }
        txt_Choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strName.equals("sign")) {
                    dialog.dismiss();
                    Intent mIntent = new Intent(mActivity, SignatureActivity.class);
                    startActivityForResult(mIntent, 444);

                } else if (strName.equals("stamp")) {
                    dialog.dismiss();
                    Intent mIntent = new Intent(mActivity, StampsActivity.class);
                    startActivityForResult(mIntent, 333);

                }
            }
        });
        txt_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strName.equals("sign")) {
                    mSignatureModel = null;
                    mInVoicesModel.setmSignatureModel(mSignatureModel);
                } else if (strName.equals("stamp")) {
                    mStampsModel = null;
                    mInVoicesModel.setmStampsModel(mStampsModel);
                }
                editText.setText("");
                dialog.dismiss();
            }
        });
        txt_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    public void showAlertDeleteDialog(Activity mActivity, final InvoiceAddItemModel model) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_delete_confirmation);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtConfirm = (TextView) alertDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) alertDialog.findViewById(R.id.txtCacel);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);

        txtMessage.setText("Are you sure want to delete item?");

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                executeDeleteItemInvoice(model);
            }
        });
        alertDialog.show();
    }
}