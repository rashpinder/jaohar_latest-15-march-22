package jaohar.com.jaohar.activities.invoices_module;

import static jaohar.com.jaohar.utils.JaoharConstants.IS_INVOICE_UPLOADED;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.krishna.fileloader.FileLoader;
import com.krishna.fileloader.listener.FileRequestListener;
import com.krishna.fileloader.pojo.FileResponse;
import com.krishna.fileloader.request.FileLoadRequest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class DetailUniversalInvoiceActivity extends BaseActivity {
    Activity mActivity = DetailUniversalInvoiceActivity.this;
    String TAG = DetailUniversalInvoiceActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
     @BindView(R.id.txtRight)
    TextView txtRight;
     @BindView(R.id.imgRightLL)
     RelativeLayout imgRightLL;
         @BindView(R.id.mWebView)
         WebView mWebView;
         @BindView(R.id.progressbar1)
         ProgressBar progressbar1;
         @BindView(R.id.rootLinearLayoutLL)
         RelativeLayout rootLinearLayoutLL;
    Uri mFileImageUrl = null;
    private static final int MEGABYTE = 1024 * 1024;

    private String id;
    private DownloadManager downloadManager;
    private long downloadReference;
    File mFinalFile = null;
    String mStoragePath = "";
    boolean boolean_permission;
    Bitmap mGeneratedPDFBitmap = null;
    String strInvoiceVesselName = "";
    String strCompanyName = "";
    String strInvoiceNumber = "";
    boolean boolean_save;
    String strPDFPath = "";
    byte[] strByteArray;
    private final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    public final int REQUEST_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_universal);
        ButterKnife.bind(this);

        if (checkPermission()) {
            boolean_permission = true;
        } else {
            requestPermission();
        }
        if (getIntent() != null) {
            id =  getIntent().getStringExtra("id");
        }
        if (!id.equals("")) {
//            String extStorageDirectory = Environment.getExternalStorageDirectory()
//                    .toString();
//            File folder = new File(extStorageDirectory, "pdf");
//            folder.mkdir();
//            File file = new File(folder, "Read.pdf");
//            try {
//                file.createNewFile();
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            }
//           DownloadFile("http://122.248.233.68/pvfiles/Guide-2.pdf", file);
//            if (JaoharConstants.SERVER_URL.contains("Staging")) {
//                downLoadFileFromUrl("https://staging.jaohar.com/index.php/UniversalInvoices/view_invoice/"+id);
//            }
//            if (JaoharConstants.SERVER_URL.contains("Development")) {
//                downLoadFileFromUrl("https://dev.jaohar.com/index.php/UniversalInvoices/view_invoice/"+id);
//            } else {
//                downLoadFileFromUrl("https://office.jaohar.com/index.php/UniversalInvoices/view_invoice/"+id);
//            }

        } else {
            Toast.makeText(mActivity, "No PDF", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void setViewsIDs() {
        imgRightLL.setVisibility(View.VISIBLE);
        txtCenter.setText(getResources().getString(R.string.detail_universal_invoice));
        txtRight.setText(getResources().getString(R.string.upload));
        imgBack.setImageResource(R.drawable.back);


        if (!id.equals("")) {
            progressbar1.setVisibility(View.VISIBLE);
            // To Show PDF IN WebView
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setBuiltInZoomControls(true);
            mWebView.getSettings().setLoadWithOverviewMode(true);
            mWebView.getSettings().setUseWideViewPort(true);
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    progressbar1.setVisibility(View.VISIBLE);
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, final String url) {
                    progressbar1.setVisibility(View.GONE);
                }
            });
            if (JaoharConstants.SERVER_URL.contains("Staging")) {
                mWebView.loadUrl("https://staging.jaohar.com/index.php/UniversalInvoices/view_invoice/" + id);
            }
            if (JaoharConstants.SERVER_URL.contains("Development")) {
                mWebView.loadUrl("https://dev.jaohar.com/index.php/UniversalInvoices/view_invoice/" + id);
             } else {
                mWebView.loadUrl("https://office.jaohar.com/index.php/UniversalInvoices/view_invoice/" + id);
            }
        } else {
            Toast.makeText(mActivity, "No PDF", Toast.LENGTH_SHORT).show();
        }

    }



    @OnClick({R.id.llLeftLL, R.id.imgRightLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.imgRightLL:
                if (boolean_permission) {
                    mGeneratedPDFBitmap = loadBitmapFromView(rootLinearLayoutLL, rootLinearLayoutLL.getWidth(), rootLinearLayoutLL.getHeight());
                    createPdf(mGeneratedPDFBitmap);
                }
                break;
        }
    }

    public Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);
        return b;
    }

    private void createPdf(Bitmap bitmap) {
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(bitmap.getWidth(), bitmap.getHeight(), 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);
        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#ffffff"));
        canvas.drawPaint(paint);
        String imageFileName = "";
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            imageFileName = "INV" + strInvoiceNumber + strInvoiceVesselName + strCompanyName + ".pdf";
            File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/jaoharr");
            if (directory.exists()) {

//                imageFileName = directory.toString() + File.separator;
            }
            if (!directory.exists()) {
                directory.mkdirs();
            }

            WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            DisplayMetrics displaymetrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            float hight = displaymetrics.heightPixels;
            float width = displaymetrics.widthPixels;

            int convertHighet = (int) hight, convertWidth = (int) width;

//        Resources mResources = getResources();
//        Bitmap bitmap = BitmapFactory.decodeResource(mResources, R.drawable.screenshot);

            bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
            paint.setColor(Color.BLUE);
            canvas.drawBitmap(bitmap, 0, 0, null);
            document.finishPage(page);

            // write the document content
//        String targetPdf = "/test.pdf";

            File file = new File(directory, imageFileName);
            //   File filePath = new File(String.valueOf(directory,));
            try {
                document.writeTo(new FileOutputStream(file));
//            btn_convert.setText("Check PDF");
                boolean_save = true;

//            Toast.makeText(this, "SAVED"+file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
//            Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
            }
            strPDFPath = file.getAbsolutePath();
//                // close the document
//                document.close();
                sendEmail(strPDFPath);
//        }

//        else {
//            File mGeneratedFile = null;
//            try {
//                mGeneratedFile = createImageFile();
//                Log.e(TAG, "******Generated File Path*******" + mGeneratedFile.getAbsolutePath());
//                document.writeTo(new FileOutputStream(mGeneratedFile));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            strPDFPath = mGeneratedFile.getAbsolutePath();}
//                // close the document
        document.close();
        UploadPdf();
//        }
    }


    private void sendEmail(String strFilePath) {
        Uri imageURI = null;
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", new File(strFilePath));
//                imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", new File(strFilePath));
                mFileImageUrl = imageURI;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            imageURI = Uri.fromFile(new File(strFilePath));
        }

        try {
            InputStream iStream = getContentResolver().openInputStream(imageURI);
            strByteArray = getBytes(iStream);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public byte[] getBytes(InputStream inputStream) {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
//        int bufferSize = 2024;
        byte[] buffer = new byte[MEGABYTE];
        int len = 0;
        while (true) {
            try {
                if (!((len = inputStream.read(buffer)) > 0)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "INV" + strInvoiceNumber + strInvoiceVesselName + strCompanyName;
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File mFile = File.createTempFile(imageFileName, /* prefix */
                ".pdf", /* suffix */
                storageDir /* directory */);
        File from = new File(storageDir, mFile.getName());
        File to = new File(storageDir, imageFileName + ".pdf");
        from.renameTo(to);
        Log.i("Directory is", storageDir.toString());
        Log.i("Default path is", storageDir.toString());
        Log.i("From path is", from.toString());
        Log.i("To path is", to.toString());
        // Save a file: path for use with ACTION_VIEW intents
        String path = new File(imageFileName).getParent();
        mStoragePath = to.getAbsolutePath();
        return to;
    }


    // generate dynamically string
    public String getAlphaNumericString() {
        int n = 20;

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        Log.e(TAG, "**********Image Name******" + sb.toString());
        return sb.toString();
    }


    private void UploadPdf() {
        AlertDialogManager.showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBodyDoc1 = null;

        RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), strByteArray);
        mMultipartBodyDoc1 = MultipartBody.Part.createFormData("pdf", getAlphaNumericString() + ".pdf", requestFileDoc1);
        Log.e("img", "img" + mMultipartBodyDoc1);

        RequestBody record_id = RequestBody.create(MediaType.parse("multipart/form-data"), id);
        RequestBody name = RequestBody.create(MediaType.parse("multipart/form-data"), strInvoiceNumber + ".pdf");
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.uploadPdfUniversalInvoiceRequest(record_id, mMultipartBodyDoc1).enqueue(new Callback<StatusMsgModel>() {

            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlerDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    Log.e(TAG, "Unexpected*********:" + mModel.getMessage());
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                IS_INVOICE_UPLOADED = true;
                if (JaoharConstants.IS_Click_From_DRAFT == true) {
                    Intent mIntent = new Intent(mActivity, AllDraftsInvoicesActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {
                    onBackPressed();
//                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
//                    startActivity(mIntent);
//                    finish();
                }
            }
        });
        alertDialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (grantResults.length > 0) {
                    boolean writeStorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean readStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean manageStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (writeStorage && readStorage && manageStorage) {
                        boolean_permission = true;
                    } else {
                        Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_LONG).show();
                        requestPermission();
                    }
                }
                break;
        }
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{
                READ_EXTERNAL_STORAGE,
                WRITE_EXTERNAL_STORAGE
        }, REQUEST_PERMISSIONS);
    }

    public boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
//        int manage = ContextCompat.checkSelfPermission(getApplicationContext(), MANAGE_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED ;
    }


    private void downLoadFileFromUrl(String pdfUrl) {
//        FileLoader.with(this)
//                .load(pdfUrl, true) //2nd parameter is optioal, pass true to force load from network
//                .fromDirectory("BrillWill", FileLoader.DIR_INTERNAL)
//                .asFile(new FileRequestListener<File>() {
//                    @Override
//                    public void onLoad(FileLoadRequest request, FileResponse<File> response) {
//                        mFinalFile = response.getBody();
//                        // do something with the file
//                        Log.e(TAG, "==FILE==" + mFinalFile);
//
//                    }
//
//                    @Override
//                    public void onError(FileLoadRequest request, Throwable t) {
//                        Log.e(TAG, "==ERROR==");
//                    }
//                });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
