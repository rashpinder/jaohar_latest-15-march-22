package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;

public class AddDiscontActivity extends BaseActivity {
    Activity mActivity = AddDiscontActivity.this;
    String TAG = AddDiscontActivity.this.getClass().getSimpleName(), strItemID;
    TextView addDiscountTXT, addPercentageTXT, addValueTXT, subValueTXT;
    LinearLayout mainLayoutLL;
    InvoiceAddItemModel mModel, pModel;
    DiscountModel discountModel;
    int mPosition, originalPositioon;
    ArrayList<InvoiceAddItemModel> mArrayList = new ArrayList();
    ArrayList<InvoiceAddItemModel> mArrayListPmodel = new ArrayList();
    ArrayList<DiscountModel> mDiscountModelArrayList = new ArrayList<DiscountModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_discont);

        if (getIntent() != null) {
            mModel = (InvoiceAddItemModel) getIntent().getSerializableExtra("Model");
            mPosition = getIntent().getIntExtra("Position", 0);
            originalPositioon = mPosition;

            strItemID = mModel.getItemID();
        }
    }

    @Override
    protected void setViewsIDs() {
        mainLayoutLL = (LinearLayout) findViewById(R.id.mainLayoutLL);
        addDiscountTXT = (TextView) findViewById(R.id.addDiscountTXT);
        addPercentageTXT = (TextView) findViewById(R.id.addPercentageTXT);
        addValueTXT = (TextView) findViewById(R.id.addValueTXT);
        subValueTXT = (TextView) findViewById(R.id.subValueTXT);
    }

    @Override
    protected void setClickListner() {
        mainLayoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent returnIntent = new Intent();
//                returnIntent.putExtra("pModel", mDiscountModelArrayList);
//                returnIntent.putExtra("Position", originalPositioon);
//                setResult(909, returnIntent);
                onBackPressed();
                finish();
            }
        });

        addDiscountTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                Intent mIntent = new Intent(mActivity, AddPercentageActivity.class);
                mIntent.putExtra("Model", mModel);
                System.out.print("Item1223" + mPosition);
                mIntent.putExtra("Position", mPosition);
                startActivityForResult(mIntent, 910);
            }
        });

        addPercentageTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, SubtractPercentageActivity.class);
                mIntent.putExtra("Model", mModel);
                System.out.print("Item1223" + mPosition);
                mIntent.putExtra("Position", mPosition);
                startActivityForResult(mIntent, 810);
            }
        });
        addValueTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, Add_ValueDiscountInvoiceActivity.class);
                mIntent.putExtra("Model", mModel);
                System.out.print("Item1223" + mPosition);
                mIntent.putExtra("Position", mPosition);
                startActivityForResult(mIntent, 910);
            }
        });
        subValueTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, Subtract_ValueDiscountInvoiceActivity.class);
                mIntent.putExtra("Model", mModel);
                System.out.print("Item1223" + mPosition);
                mIntent.putExtra("Position", mPosition);
                startActivityForResult(mIntent, 810);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 910) {
            if (data != null && data.getSerializableExtra("pModel") != null) {
                mDiscountModelArrayList = (ArrayList<DiscountModel>) data.getSerializableExtra("pModel");
                mPosition = data.getIntExtra("Position", 0);
                Log.e(TAG, "SiZEARRAYLIST1" + mDiscountModelArrayList.size());
                mModel.setmDiscountModelArrayList(mDiscountModelArrayList);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("pModel", mDiscountModelArrayList);
                returnIntent.putExtra("Position", originalPositioon);
                setResult(909, returnIntent);
                finish();
            }
        }
        if (requestCode == 810) {
            if (data != null && data.getSerializableExtra("pModel") != null) {
                mDiscountModelArrayList = (ArrayList<DiscountModel>) data.getSerializableExtra("pModel");
                mPosition = data.getIntExtra("Position", 0);
                Log.e(TAG, "SiZEARRAYLIST2" + mDiscountModelArrayList.size());
                mModel.setmDiscountModelArrayList(mDiscountModelArrayList);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("pModel", mDiscountModelArrayList);
                returnIntent.putExtra("Position", originalPositioon);
                setResult(909, returnIntent);
                finish();
            }
        }
    }

    public void setUpSubtractDiscount() {
        final Dialog searchDialog = new Dialog(mActivity);
        searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        searchDialog.setContentView(R.layout.dialog_new_details);
        searchDialog.setCanceledOnTouchOutside(true);
        searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = searchDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView itemPRice = (TextView) searchDialog.findViewById(R.id.itemPRice);

        String unitPRice = String.valueOf(mModel.getUnitprice());
        itemPRice.setText(unitPRice);

        searchDialog.show();
    }
}
