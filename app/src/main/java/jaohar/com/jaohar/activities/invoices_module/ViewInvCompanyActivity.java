package jaohar.com.jaohar.activities.invoices_module;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.ButterKnife;
import jaohar.com.jaohar.R;

public class ViewInvCompanyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_inv_company);
        ButterKnife.bind(this);
    }
}