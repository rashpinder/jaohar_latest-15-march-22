package jaohar.com.jaohar.activities.universal_invoice_module;

import static jaohar.com.jaohar.utils.JaoharConstants.IS_INVOICE_UPLOADED;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.invoices_module.AllDraftsInvoicesActivity;
import jaohar.com.jaohar.adapters.PreviewItemsAdapter;
import jaohar.com.jaohar.beans.AddTotalInvoiceDiscountModel;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.beans.PriviewItemModel;
import jaohar.com.jaohar.beans.PriviewModelItems;
import jaohar.com.jaohar.models.InvoiceModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import jaohar.com.jaohar.views.ZoomLayout;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class UploadUniversalPdfActivity extends BaseActivity {
    private static final int MEGABYTE = 1024 * 1024;
    private static final String FILE = "Jaohar/Invoice.pdf"; // add permission in your manifest...
    public final int REQUEST_PERMISSIONS = 1;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgRight)
    ImageView imgRight;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    JSONArray itemsJsonArray;
    JSONArray itemsDiscountJsonArray;
    Activity mActivity = UploadUniversalPdfActivity.this;
    String TAG = UploadUniversalPdfActivity.this.getClass().getSimpleName();
    LinearLayout rootLinearLayoutLL, swiftLL, gbpLL;
    NestedScrollView mNestedScrollView;
    RelativeLayout imgRightLL;
    Bitmap mGeneratedPDFBitmap = null;
    String strPDFPath = "";
    String strInvoiceVesselName = "";
    String strCompanyName = "";
    String strInvoiceNumber = "";
    boolean boolean_permission;
    boolean boolean_save;
    ZoomLayout mZoomLayout;
    ProgressBar progress1;
    PriviewItemModel priviewModel = new PriviewItemModel();
    ArrayList<PriviewModelItems> mArraypriviewModelPrievw = new ArrayList<PriviewModelItems>();
    ArrayList<PriviewModelItems> mArraypriviewModeNEWPRIVIEW = new ArrayList<PriviewModelItems>();
    //    ZoomableRelativeLayout mZoomLayoutRelativeLayout;
    /*PDF Widgets*/
    TextView txtCompanyName, txtCompanyAddress2, txtCompanyAddress3, txtCompanyAddress4, txtCompanyAddress5, txtCompanyAddress, txtInVoiceNumberValueTV, txtDateValueTV, txtDueValueTV, txtRefrenceValue,
            txtCurrencyValueTV, txtSubTotalValueTV, txtVatValueTV, txtVATPercentTV, txtTotalValueTV, txtPaidValueTV, txtBalanceDueValueTV,
            txtBenificiaryValueTV, txtBankNameTV, txtCurrenCyGBPTV, txtAddress1TV, txtCurrencyRONTV, txtCurrencyUSDTV, txtCurrencyEURTV, txtSwiftCodeTV, txtRefrence1, txtRefrence2, txtRefrence3, txtAddress2TV;
    LinearLayout usdLL, eurLL, ronLL, mainLL, mailLl, mainLL1;
    ImageView imgTopStatusIV, imgSigneIV;
    ImageView imgStampIV;
    RecyclerView rvItemsRowsRV;
    /*Invoice Model*/
    InVoicesModel mInVoicesModel;
    /*Adapter*/
    PreviewItemsAdapter mPreviewItemsAdapter;
    //Invoice Items ArrayList
    ArrayList<InvoiceAddItemModel> mInvoiceItemArrayList = new ArrayList<InvoiceAddItemModel>();
    ArrayList<InvoiceAddItemModel> itemsArrayList = new ArrayList<InvoiceAddItemModel>();
    //PDF File Path
    String mStoragePath = "";
    boolean Is_Edit = false;
    ArrayList<AddTotalInvoiceDiscountModel> mArrayTotalDiscount = new ArrayList<AddTotalInvoiceDiscountModel>();
    byte[] strByteArray;
    String strPDFBase64;
    Button btnUpload;
    String ownerId;
    String strVessalID = "";
    String strStamPId = "";
    String strSignatireId = "";
    String strBankId = "";
    String strCompanyId = "";
    String strInvoiceID = "";
    Uri mFileImageUrl = null;
    private final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    //    private final String MANAGE_EXTERNAL_STORAGE = Manifest.permission.MANAGE_EXTERNAL_STORAGE;
    private ArrayList<String> itemIDARRAY = new ArrayList<String>();
    private ArrayList<String> totalDiscountIDARRAY = new ArrayList<String>();

    private static void SaveImage(Bitmap finalBitmap) {
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();

        String fname = "Image-" + "Testing" + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            Log.e("IMAGE", "SaveImage: " + root);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_universal_pdf);

        setStatusBar();

        ButterKnife.bind(this);

        setTopBarMenu();

        if (checkPermission()) {
            boolean_permission = true;
        } else {
            requestPermission();
        }
        setViewss();
    }

    private void setTopBarMenu() {
        imgBack.setImageResource(R.drawable.back);
        imgRight.setImageResource(R.drawable.ic_mail_invoice);
        imgRight.setVisibility(View.GONE);
        txtCenter.setText("Upload Invoice");

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    protected void setViewss() {
        mNestedScrollView = (NestedScrollView) findViewById(R.id.mNestedScrollView);
        rootLinearLayoutLL = (LinearLayout) findViewById(R.id.rootLinearLayoutLL);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        mainLL = (LinearLayout) findViewById(R.id.mainLL);
        swiftLL = (LinearLayout) findViewById(R.id.swiftLL);
        gbpLL = (LinearLayout) findViewById(R.id.gbpLL);
//      mailLl = (LinearLayout) findViewById(R.id.mailLl);
        mainLL1 = (LinearLayout) findViewById(R.id.mainLL1);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        txtCompanyName = (TextView) findViewById(R.id.txtCompanyName);
        txtCompanyAddress2 = (TextView) findViewById(R.id.txtCompanyAddress2);
        txtCompanyAddress3 = (TextView) findViewById(R.id.txtCompanyAddress3);
        txtCompanyAddress4 = (TextView) findViewById(R.id.txtCompanyAddress4);
        txtCompanyAddress5 = (TextView) findViewById(R.id.txtCompanyAddress5);
        txtCompanyAddress = (TextView) findViewById(R.id.txtCompanyAddress);
        txtCurrenCyGBPTV = (TextView) findViewById(R.id.txtCurrenCyGBPTV);
        txtInVoiceNumberValueTV = (TextView) findViewById(R.id.txtInVoiceNumberValueTV);
        txtDateValueTV = (TextView) findViewById(R.id.txtDateValueTV);
        txtDueValueTV = (TextView) findViewById(R.id.txtDueValueTV);
        txtRefrenceValue = (TextView) findViewById(R.id.txtRefrenceValue);
        txtCurrencyValueTV = (TextView) findViewById(R.id.txtCurrencyValueTV);
        txtBenificiaryValueTV = (TextView) findViewById(R.id.txtBenificiaryValueTV);
        txtBankNameTV = (TextView) findViewById(R.id.txtBankNameTV);
        txtAddress1TV = (TextView) findViewById(R.id.txtAddress1TV);
        txtAddress2TV = (TextView) findViewById(R.id.txtAddress2TV);
        txtCurrencyRONTV = (TextView) findViewById(R.id.txtCurrencyRONTV);
        txtCurrencyUSDTV = (TextView) findViewById(R.id.txtCurrencyUSDTV);
        txtCurrencyEURTV = (TextView) findViewById(R.id.txtCurrencyEURTV);
        txtSwiftCodeTV = (TextView) findViewById(R.id.txtSwiftCodeTV);
        txtSubTotalValueTV = (TextView) findViewById(R.id.txtSubTotalValueTV);
        txtVatValueTV = (TextView) findViewById(R.id.txtVatValueTV);
        txtVATPercentTV = (TextView) findViewById(R.id.txtVATPercentTV);
        txtTotalValueTV = (TextView) findViewById(R.id.txtTotalValueTV);
        txtPaidValueTV = (TextView) findViewById(R.id.txtPaidValueTV);
        txtBalanceDueValueTV = (TextView) findViewById(R.id.txtBalanceDueValueTV);
        imgTopStatusIV = (ImageView) findViewById(R.id.imgTopStatusIV);
        imgStampIV = (ImageView) findViewById(R.id.imgStampIV);
        imgSigneIV = (ImageView) findViewById(R.id.imgSigneIV);
        mZoomLayout = new ZoomLayout(mActivity);
        usdLL = (LinearLayout) findViewById(R.id.usdLL);
        eurLL = (LinearLayout) findViewById(R.id.eurLL);
        ronLL = (LinearLayout) findViewById(R.id.ronLL);
        txtRefrence1 = (TextView) findViewById(R.id.txtRefrence1);
        txtRefrence2 = (TextView) findViewById(R.id.txtRefrence2);
        txtRefrence3 = (TextView) findViewById(R.id.txtRefrence3);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        progress1 = (ProgressBar) findViewById(R.id.progress1);

        if (getIntent() != null) {
            if (JaoharConstants.IS_Click_From_Edit == true) {
                mInVoicesModel = (InVoicesModel) getIntent().getSerializableExtra("Model");
                itemIDARRAY = (ArrayList<String>) getIntent().getSerializableExtra("ItemArray");
                mArrayTotalDiscount = (ArrayList<AddTotalInvoiceDiscountModel>) getIntent().getSerializableExtra("mTotalDiscountModelArray");
                itemsArrayList = (ArrayList<InvoiceAddItemModel>) getIntent().getSerializableExtra("mItemModelArray");
                totalDiscountIDARRAY = (ArrayList<String>) getIntent().getSerializableExtra("TotalDiscountArray");
                if ((ArrayList<PriviewModelItems>) getIntent().getSerializableExtra("mPriviewModelArray") != null) {
                    mArraypriviewModelPrievw = (ArrayList<PriviewModelItems>) getIntent().getSerializableExtra("mPriviewModelArray");
                }
                if (getIntent().getExtras().getBoolean("IsEdit") == true) {
                    Is_Edit = getIntent().getExtras().getBoolean("IsEdit");
                }
                setDataOnWidgets(mInVoicesModel);
                System.out.println("ITEAM===============SIZE" + mInVoicesModel.getmItemModelArrayList().size());
            } else {
                mInVoicesModel = (InVoicesModel) getIntent().getSerializableExtra("Model");
                mArrayTotalDiscount = (ArrayList<AddTotalInvoiceDiscountModel>) getIntent().getSerializableExtra("mTotalDiscountModelArray");
                itemsArrayList = (ArrayList<InvoiceAddItemModel>) getIntent().getSerializableExtra("mItemModelArray");
                if ((ArrayList<PriviewModelItems>) getIntent().getSerializableExtra("mPriviewModelArray") != null) {
                    mArraypriviewModelPrievw = (ArrayList<PriviewModelItems>) getIntent().getSerializableExtra("mPriviewModelArray");
                }
                if (getIntent().getExtras().getBoolean("IsEdit") == true) {
                    Is_Edit = getIntent().getExtras().getBoolean("IsEdit");
                }
                setDataOnWidgets(mInVoicesModel);
                System.out.println("ITEAM===============SIZE" + mInVoicesModel.getmItemModelArrayList().size());
            }
        }

    }

    private void setDataOnWidgets(InVoicesModel mInVoicesModel) {
        try {
//            if (mInVoicesModel.getmCompaniesModel() != null) {
//                txtCompanyName.setText(mInVoicesModel.getmCompaniesModel().getCompany_name());
//
//                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0)
//                    txtCompanyAddress.setText(mInVoicesModel.getmCompaniesModel().getAddress1());
//                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0)
//                    txtCompanyAddress2.setText(mInVoicesModel.getmCompaniesModel().getAddress2());
//                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress3().length() > 0)
//                    txtCompanyAddress3.setText(mInVoicesModel.getmCompaniesModel().getAddress3());
//                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress3().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress4().length() > 0)
//                    txtCompanyAddress4.setText(mInVoicesModel.getmCompaniesModel().getAddress4());
//                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress3().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress4().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress5().length() > 0)
//                    txtCompanyAddress5.setText(mInVoicesModel.getmCompaniesModel().getAddress5());
//            }
            if (mInVoicesModel.getmCompaniesModel() != null) {
                txtCompanyName.setText(mInVoicesModel.getmCompaniesModel().getCompany_name());
                strCompanyName = "-" + mInVoicesModel.getmCompaniesModel().getCompany_name();
                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0)
                    txtCompanyAddress.setText(mInVoicesModel.getmCompaniesModel().getAddress1());
                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0)
                    txtCompanyAddress2.setText(mInVoicesModel.getmCompaniesModel().getAddress2());
                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress3().length() > 0)
                    txtCompanyAddress3.setText(mInVoicesModel.getmCompaniesModel().getAddress3());
                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress3().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress4().length() > 0)
                    txtCompanyAddress4.setText(mInVoicesModel.getmCompaniesModel().getAddress4());
                if (mInVoicesModel.getmCompaniesModel().getAddress1().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress2().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress3().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress4().length() > 0 && mInVoicesModel.getmCompaniesModel().getAddress5().length() > 0)
                    txtCompanyAddress5.setText(mInVoicesModel.getmCompaniesModel().getAddress5());


            }
            if (mInVoicesModel.getInvoice_number().length() > 0)
                txtInVoiceNumberValueTV.setText("JAORO" + mInVoicesModel.getInvoice_number());
            strInvoiceNumber = mInVoicesModel.getInvoice_number();
            if (mInVoicesModel.getInvoice_date().length() > 0)
                txtDateValueTV.setText(Utilities.gettingFormatTime(mInVoicesModel.getInvoice_date()));

            String strDueDate = Utilities.getNewDateFormatWhenAddDays(mInVoicesModel.getInvoice_date(), Integer.parseInt(mInVoicesModel.getTerm_days()));
            txtDueValueTV.setText(Utilities.gettingFormatTime(strDueDate));
            if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
                txtRefrenceValue.setText("M/V " + mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_name() + ", IMO " + mInVoicesModel.getmVesselSearchInvoiceModel().getIMO_no() + ", Flag " + mInVoicesModel.getmVesselSearchInvoiceModel().getFlag());
                strInvoiceVesselName = "-" + mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_name();
            }

            txtRefrence1.setText(mInVoicesModel.getRefrence1());
            txtRefrence2.setText(mInVoicesModel.getRefrence2());
            txtRefrence3.setText(mInVoicesModel.getRefrence3());
            if (mInVoicesModel.getCurrency().equals("USD")) {
                txtCurrencyValueTV.setText(mInVoicesModel.getCurrency());
                txtCurrencyValueTV.setTextColor(getResources().getColor(R.color.colorAccentRed));
            } else {
                txtCurrencyValueTV.setText(mInVoicesModel.getCurrency());
                txtCurrencyValueTV.setTextColor(getResources().getColor(R.color.colorAccentRed));
            }


            if (mInVoicesModel.getmBankModel() != null) {
                txtBenificiaryValueTV.setText(mInVoicesModel.getmBankModel().getBenificiary());
                txtBankNameTV.setText(mInVoicesModel.getmBankModel().getBankName());
                txtAddress1TV.setText(mInVoicesModel.getmBankModel().getAddress1());
                txtAddress2TV.setText(mInVoicesModel.getmBankModel().getAddress2());

                if (mInVoicesModel.getmBankModel().getIban_gbp() != null && mInVoicesModel.getmBankModel().getIban_gbp().length() > 0) {
                    txtCurrenCyGBPTV.setText(mInVoicesModel.getmBankModel().getIban_gbp());
                } else {
                    gbpLL.setVisibility(View.GONE);
                }
            }

            if (mInVoicesModel.getmBankModel() != null) {
                if (mInVoicesModel.getmBankModel().getIbanUSD().length() > 0) {
                    usdLL.setVisibility(View.VISIBLE);
                    txtCurrencyUSDTV.setText(mInVoicesModel.getmBankModel().getIbanUSD());
                } else {
                    usdLL.setVisibility(View.GONE);
                }
            } else {
                usdLL.setVisibility(View.GONE);
            }
            if (mInVoicesModel.getmBankModel() != null) {
                if (mInVoicesModel.getmBankModel().getIbanEUR().length() > 0) {
                    eurLL.setVisibility(View.VISIBLE);
                    txtCurrencyEURTV.setText(mInVoicesModel.getmBankModel().getIbanEUR());
                } else {
                    eurLL.setVisibility(View.GONE);
                }
            } else {
                eurLL.setVisibility(View.GONE);
            }
            if (mInVoicesModel.getmBankModel() != null) {
                if (mInVoicesModel.getmBankModel().getIbanRON().length() > 0) {
                    ronLL.setVisibility(View.VISIBLE);
                    txtCurrencyRONTV.setText(mInVoicesModel.getmBankModel().getIbanRON());
                } else {
                    ronLL.setVisibility(View.GONE);
                }
                if (mInVoicesModel.getmBankModel().getSwift() != null && mInVoicesModel.getmBankModel().getSwift().length() > 0) {
                    txtSwiftCodeTV.setText(mInVoicesModel.getmBankModel().getSwift());
                } else {
                    swiftLL.setVisibility(View.GONE);
                }
            } else {
                ronLL.setVisibility(View.GONE);
            }


            if (mInVoicesModel.getmItemModelArrayList().size() > 0)
                Log.e(TAG, "*****SIZE****" + mInVoicesModel.getmItemModelArrayList().size());

            mArraypriviewModeNEWPRIVIEW = Utilities.gettingFormatedItems(mArraypriviewModelPrievw);
            Log.e(TAG, "*****SIZEsfasf****" + mArraypriviewModeNEWPRIVIEW.size());
            if (mInVoicesModel.getmPaymentModel() != null) {
                if (mInVoicesModel.getmPaymentModel().getSubTotal().contains(".")) {
                    String strDWT2 = "00";
                    DecimalFormat df = new DecimalFormat("#.##");
                    Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getSubTotal());
                    df.setRoundingMode(RoundingMode.DOWN);
                    String strNumDWT = df.format(f1);
                    String[] separated = strNumDWT.split("\\.");
                    String strDWT = separated[0]; // this will contain "Fruit"
                    if (strNumDWT.contains(".")) {
                        strDWT2 = separated[1];
                    }
                    long numberGrain = Long.parseLong(strDWT);
                    String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                    txtSubTotalValueTV.setText(strNuumGrain + "." + strDWT2 + "  " + mInVoicesModel.getCurrency());
                } else {
                    long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getSubTotal());
                    String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                    txtSubTotalValueTV.setText(strNumGrossTonage + "  " + mInVoicesModel.getCurrency());
//                         GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                }
//                txtSubTotalValueTV.setText("" + mInVoicesModel.getmPaymentModel().getSubTotal() + "  " + mInVoicesModel.getCurrency());
                String str = mInVoicesModel.getmPaymentModel().getVAT();
                if (!str.equals("")) {
                    if (mInVoicesModel.getmPaymentModel().getVAT().contains(".")) {
                        String strDWT2 = "00";
                        DecimalFormat df = new DecimalFormat("#.##");
                        Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getVAT());
                        df.setRoundingMode(RoundingMode.DOWN);
                        String strNumDWT = df.format(f1);
                        String[] separated = strNumDWT.split("\\.");
                        String strDWT = separated[0]; // this will contain "Fruit"
                        if (strNumDWT.contains(".")) {
                            strDWT2 = separated[1];
                        }
                        long numberGrain = Long.parseLong(strDWT);
                        String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                        txtVATPercentTV.setText("V.A.T(" + strNuumGrain + "." + strDWT2 + "%):");
                    } else {
                        long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getVAT());
                        String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                        txtVATPercentTV.setText("V.A.T(" + strNumGrossTonage + "%):");
//                         GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                    }
                } else {
                    txtVATPercentTV.setText("V.A.T(" + 0 + "%):");
                }
                if (!mInVoicesModel.getmPaymentModel().getVATPrice().equals("")) {
                    if (mInVoicesModel.getmPaymentModel().getVATPrice().contains(".")) {
                        String strDWT2 = "00";
                        DecimalFormat df = new DecimalFormat("#.##");
                        Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getVATPrice());
                        df.setRoundingMode(RoundingMode.DOWN);
                        String strNumDWT = df.format(f1);
                        String[] separated = strNumDWT.split("\\.");
                        String strDWT = separated[0]; // this will contain "Fruit"
                        if (strNumDWT.contains(".")) {
                            strDWT2 = separated[1];
                        }
                        long numberGrain = Long.parseLong(strDWT);
                        String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                        txtVatValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mInVoicesModel.getCurrency());
                    } else {
                        long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getVATPrice());
                        String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                        txtVatValueTV.setText(" " + strNumGrossTonage + "  " + mInVoicesModel.getCurrency());
//                         GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                    }
                } else {
                    txtVatValueTV.setText(" " + 0 + "  " + mInVoicesModel.getCurrency());
                }

                if (mInVoicesModel.getmPaymentModel().getTotal().contains(".")) {
                    String strDWT2 = "00";
                    DecimalFormat df = new DecimalFormat("#.##");
                    Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getTotal());
                    df.setRoundingMode(RoundingMode.DOWN);
                    String strNumDWT = df.format(f1);
                    String[] separated = strNumDWT.split("\\.");
                    String strDWT = separated[0]; // this will contain "Fruit"
                    if (strNumDWT.contains(".")) {
                        strDWT2 = separated[1];
                    }
                    long numberGrain = Long.parseLong(strDWT);
                    String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                    txtTotalValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mInVoicesModel.getCurrency());
                } else {
                    long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getTotal());
                    String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                    txtTotalValueTV.setText(" " + strNumGrossTonage + "  " + mInVoicesModel.getCurrency());
//                         GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                }
                if (!mInVoicesModel.getmPaymentModel().getPaid().equals("")) {
                    if (mInVoicesModel.getmPaymentModel().getPaid().contains(".")) {
                        String strDWT2 = "00";
                        DecimalFormat df = new DecimalFormat("#.##");
                        Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getPaid());
                        df.setRoundingMode(RoundingMode.DOWN);
                        String strNumDWT = df.format(f1);
                        String[] separated = strNumDWT.split("\\.");
                        String strDWT = separated[0]; // this will contain "Fruit"
                        if (strNumDWT.contains(".")) {
                            strDWT2 = separated[1];
                        }
                        long numberGrain = Long.parseLong(strDWT);
                        String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);

                        txtPaidValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mInVoicesModel.getCurrency());
                    } else {
                        long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getPaid());
                        String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                        txtPaidValueTV.setText(" " + strNumGrossTonage + "  " + mInVoicesModel.getCurrency());
//                  GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                    }
                } else {
                    txtPaidValueTV.setText(" " + 0 + "  " + mInVoicesModel.getCurrency());

                }
//                txtVATPercentTV.setText("V.A.T(" + mInVoicesModel.getmPaymentModel().getVAT() + "%)");
//                txtVatValueTV.setText("" + mInVoicesModel.getmPaymentModel().getVATPrice() + "  " + mInVoicesModel.getCurrency());
//                txtTotalValueTV.setText("" + mInVoicesModel.getmPaymentModel().getTotal() + "  " + mInVoicesModel.getCurrency());
//                txtPaidValueTV.setText("" + mInVoicesModel.getmPaymentModel().getPaid() + "  " + mInVoicesModel.getCurrency());

                if (!mInVoicesModel.getmPaymentModel().getBalanceDue().equals(""))
                    if (mInVoicesModel.getmPaymentModel().getBalanceDue().contains(".")) {
                        String strDWT2 = "00";
                        DecimalFormat df = new DecimalFormat("#.##");
                        Double f1 = Double.parseDouble(mInVoicesModel.getmPaymentModel().getBalanceDue());
                        df.setRoundingMode(RoundingMode.DOWN);
                        String strNumDWT = df.format(f1);
                        String[] separated = strNumDWT.split("\\.");
                        String strDWT = separated[0]; // this will contain "Fruit"
                        if (strNumDWT.contains(".")) {
                            strDWT2 = separated[1];
                        }
                        long numberGrain = Long.parseLong(strDWT);
                        String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                        txtBalanceDueValueTV.setText(" " + strNuumGrain + "." + strDWT2 + "  " + mInVoicesModel.getCurrency());
                    } else {
                        long numberGrossTonage = Long.parseLong(mInVoicesModel.getmPaymentModel().getBalanceDue());
                        String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                        txtBalanceDueValueTV.setText(" " + strNumGrossTonage + "  " + mInVoicesModel.getCurrency());
//                      GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                    }
            }

            imgTopStatusIV.setImageResource(Utilities.getStatusImage(mInVoicesModel.getStatus()));
            if (mInVoicesModel.getmStampsModel() != null) {

                if (mInVoicesModel.getmStampsModel().getStamp_image().length() > 0) {
                    progress1.setVisibility(View.GONE);
//                    Glide.with(mActivity)
//                            .load(mInVoicesModel.getmStampsModel().getStamp_image())
//                            .listener(new RequestListener<String, GlideDrawable>() {
//                                @Override
//                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                                    Toast.makeText(mActivity, "Error Image is Too large ", Toast.LENGTH_SHORT).show();
//                                    return false;
//                                }
//                                @Override
//                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                                    progress1.setVisibility(View.GONE);
//                                    return false;
//                                }
//                            }).into(imgStampIV);
                    Picasso.get().load(mInVoicesModel.getmStampsModel().getStamp_image())
//                            .placeholder(R.drawable.profile)
//                            .error(R.drawable.profile)
//                            .transform(new RoundedTransformation(50, 4))
//                            .resizeDimen(R.dimen.list_detail_image_size, R.dimen.list_detail_image_size)
//                            .centerCrop()
                            .into(imgStampIV);
                }
            } else {
                progress1.setVisibility(View.GONE);
            }
            if (mInVoicesModel.getmSignatureModel() != null) {
                if (mInVoicesModel.getmSignatureModel().getSignature_image().length() > 0) {
//                 Glide.with(mActivity)
//                            .load(mInVoicesModel.getmSignatureModel().getSignature_image())
//                            .into(imgSigneIV);
                    Picasso.get().load(mInVoicesModel.getmSignatureModel().getSignature_image())
//                            .placeholder(R.drawable.profile)
//                            .error(R.drawable.profile)
//                            .transform(new RoundedTransformation(50, 4))
//                            .resizeDimen(R.dimen.list_detail_image_size, R.dimen.list_detail_image_size)
//                            .centerCrop()
                            .into(imgSigneIV);
                }
            }


            if (mArraypriviewModeNEWPRIVIEW.size() != 0) {

                for (int i = 0; i < mArraypriviewModeNEWPRIVIEW.size(); i++) {
                    LayoutInflater layoutInflater =
                            (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(R.layout.item_pdf_item_layout, null);
                    TextView item_idTV = (TextView) addView.findViewById(R.id.item_idTV);
                    LinearLayout itemParentLL = (LinearLayout) addView.findViewById(R.id.itemParentLL);
                    TextView item_DescriptionTV = (TextView) addView.findViewById(R.id.item_DescriptionTV);
                    TextView item_QuantityTV = (TextView) addView.findViewById(R.id.item_QuantityTV);
                    TextView item_UnitPriceTV = (TextView) addView.findViewById(R.id.item_UnitPriceTV);
                    TextView item_AmountTV = (TextView) addView.findViewById(R.id.item_AmountTV);
                    PriviewModelItems tempValue = mArraypriviewModeNEWPRIVIEW.get(i);
                    int strCOUNT = i + 1;
                    if (!tempValue.getStrDescription().equals("")) {
                        item_idTV.setText("" + strCOUNT);
                    }

                    if (!tempValue.getStrDescription().equals("")) {
                        int strCharcter = tempValue.getStrDescription().length();
                        if (strCharcter <= 60) {
                            itemParentLL.setMinimumHeight(40);
                        }
                        item_DescriptionTV.setText(tempValue.getStrDescription());
                    } else {
                        itemParentLL.setMinimumHeight(40);
                    }
                    if (!tempValue.getStrUnitPrice().equals("")) {
                        if (tempValue.getStrUnitPrice().contains(".")) {
                            String strDWT2 = "00";
                            DecimalFormat df = new DecimalFormat("#.##");
                            Double f1 = Double.parseDouble(tempValue.getStrUnitPrice());
                            df.setRoundingMode(RoundingMode.DOWN);
                            String strNumDWT = df.format(f1);
                            String[] separated = strNumDWT.split("\\.");
                            String strDWT = separated[0]; // this will contain "Fruit"
                            if (strNumDWT.contains(".")) {
                                strDWT2 = separated[1];
                            }
                            long numberGrain = Long.parseLong(strDWT);
                            String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                            item_UnitPriceTV.setText(strNuumGrain + "." + strDWT2);
                        } else {
                            long numberGrossTonage = Long.parseLong(tempValue.getStrUnitPrice());
                            String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                            item_UnitPriceTV.setText(strNumGrossTonage);
//                          GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                        }
//                          item_UnitPriceTV.setText("" + tempValue.getStrUnitPrice() + " " );
                    }
                    if (!tempValue.getStrAmount().equals("")) {
                        if (tempValue.getStrAmount().contains(".")) {
                            if (tempValue.getStrAmount().contains("+")) {
                                String strDWT2 = "00";
                                DecimalFormat df = new DecimalFormat("##.##");
                                Double f1 = Double.parseDouble(tempValue.getStrAmount());
                                df.setRoundingMode(RoundingMode.DOWN);
                                String strNumDWT = df.format(f1);
                                String[] separated = strNumDWT.split("\\.");
                                String strDWT = separated[0]; // this will contain "Fruit"
                                if (strNumDWT.contains(".")) {
                                    strDWT2 = separated[1];
                                }
                                long numberGrain = Long.parseLong(strDWT);
                                String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                                item_AmountTV.setText(strNuumGrain + "." + strDWT2);
                            } else if (tempValue.getStrAmount().contains("-")) {
                                String strDWT2 = "00";
                                DecimalFormat df = new DecimalFormat("#.##");
                                Double f1 = Double.parseDouble(tempValue.getStrAmount());
                                df.setRoundingMode(RoundingMode.DOWN);
                                String strNumDWT = df.format(f1);
                                String[] separated = strNumDWT.split("\\.");
                                String strDWT = separated[0]; // this will contain "Fruit"
                                if (strNumDWT.contains(".")) {
                                    strDWT2 = separated[1];
                                }
                                long numberGrain = Long.parseLong(strDWT);
                                String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                                if (strNuumGrain.contains("-")) {
                                    item_AmountTV.setText(strNuumGrain + "." + strDWT2);
                                } else {
                                    item_AmountTV.setText("-" + strNuumGrain + "." + strDWT2);
                                }
                            } else {
                                String strDWT2 = "00";
                                DecimalFormat df = new DecimalFormat("#.##");
                                Double f1 = Double.parseDouble(tempValue.getStrAmount());
                                df.setRoundingMode(RoundingMode.DOWN);
                                String strNumDWT = df.format(f1);
                                String[] separated = strNumDWT.split("\\.");
                                String strDWT = separated[0]; // this will contain "Fruit"
                                if (strNumDWT.contains(".")) {
                                    strDWT2 = separated[1];
                                }
                                long numberGrain = Long.parseLong(strDWT);
                                String strNuumGrain = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrain);
                                item_AmountTV.setText(strNuumGrain + "." + strDWT2);
                            }

                        } else {
                            long numberGrossTonage = Long.parseLong(tempValue.getStrAmount());
                            String strNumGrossTonage = NumberFormat.getNumberInstance(Locale.getDefault()).format(numberGrossTonage);
                            item_AmountTV.setText(strNumGrossTonage);
//                          GrossTonnageTV.setText(mVesselesModel.getGross_tonnage());
                        }
//                          item_AmountTV.setText("" + tempValue.getStrAmount() + " ");
                    }
                    if (!tempValue.getStrQuantity().equals("")) {
                        item_QuantityTV.setText("" + tempValue.getStrQuantity());
                    }

                    if (i % 2 == 0) {
                        itemParentLL.setBackgroundResource(R.drawable.pdf_row_odd);
                    } else {
                        itemParentLL.setBackgroundResource(R.drawable.pdf_row_even);
                    }
                    mainLL1.addView(addView);
                    ViewTreeObserver observer = mainLL1.getViewTreeObserver();
                    observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            // TODO Auto-generated method stub
                            init();
                            mainLL1.getViewTreeObserver().removeGlobalOnLayoutListener(
                                    this);
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void init() {
        int a = mainLL1.getHeight();
        int b = mainLL1.getWidth();
        if (a < 490) {

        } else {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mainLL1.getLayoutParams();
//          Changes the height and width to the specified *pixels*
            params.height = 490;
            mainLL1.setLayoutParams(params);
        }
//          Toast.makeText(mActivity,""+a+" "+b,Toast.LENGTH_LONG).show();
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (boolean_permission) {
//                    mGeneratedPDFBitmap = loadBitmapFromView(rootLinearLayoutLL, rootLinearLayoutLL.getWidth(), rootLinearLayoutLL.getHeight());
//                    createPdf(mGeneratedPDFBitmap);
//                }
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (boolean_permission) {
                    mGeneratedPDFBitmap = loadBitmapFromView(rootLinearLayoutLL, rootLinearLayoutLL.getWidth(), rootLinearLayoutLL.getHeight());
                    createPdf(mGeneratedPDFBitmap);
                }
            }
        });
    }

    private JsonObject mParam() {
        JSONObject jsonObject = new JSONObject();
        itemsJsonArray = gettingItemIDsArray(itemsArrayList);
        itemsDiscountJsonArray = gettingItemDiscounArray(mArrayTotalDiscount);
        try {
            jsonObject.put("owner_id", ownerId);
            jsonObject.put("invoice_no", mInVoicesModel.getInvoice_number());
            jsonObject.put("term_days", mInVoicesModel.getTerm_days());
            jsonObject.put("invoice_date", mInVoicesModel.getInvoice_date());
            jsonObject.put("search_company", strCompanyId);
            jsonObject.put("search_vessel", strVessalID);
            jsonObject.put("currency", mInVoicesModel.getCurrency());
            jsonObject.put("stamp", strStamPId);
            jsonObject.put("sign", strSignatireId);
            jsonObject.put("status", mInVoicesModel.getStatus());
            jsonObject.put("reference", mInVoicesModel.getRefrence1());
            jsonObject.put("reference1", mInVoicesModel.getRefrence2());
            jsonObject.put("reference2", mInVoicesModel.getRefrence3());
            jsonObject.put("bank_details", strBankId);
            jsonObject.put("payment_id", mInVoicesModel.getmPaymentModel().getPayment_id());
            jsonObject.put("items", itemsJsonArray);
            jsonObject.put("itemdiscount", itemsDiscountJsonArray);
            jsonObject.put("sub_total", mInVoicesModel.getmPaymentModel().getSubTotal());
            jsonObject.put("VAT", mInVoicesModel.getmPaymentModel().getVAT());
            jsonObject.put("vat_price", mInVoicesModel.getmPaymentModel().getVATPrice());
            jsonObject.put("total", mInVoicesModel.getmPaymentModel().getTotal());
            jsonObject.put("paid", mInVoicesModel.getmPaymentModel().getPaid());
            jsonObject.put("due", mInVoicesModel.getmPaymentModel().getBalanceDue());
            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
            Log.e(TAG, "**PARAM**" + jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
        return gsonObject;
    }

    public void executeAddInVoicesAPI() {
        if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
            strVessalID = mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_id();
        } else {
            strVessalID = "";
        }
        if (mInVoicesModel.getmStampsModel() != null) {
            strStamPId = mInVoicesModel.getmStampsModel().getId();
        } else {
            strStamPId = "";
        }
        if (mInVoicesModel.getmSignatureModel() != null) {
            strSignatireId = mInVoicesModel.getmSignatureModel().getId();
        } else {
            strSignatireId = "";
        }
        if (mInVoicesModel.getmBankModel() != null) {
            strBankId = mInVoicesModel.getmBankModel().getId();
        } else {
            strBankId = "";
        }
        if (mInVoicesModel.getmCompaniesModel() != null) {
            strCompanyId = mInVoicesModel.getmCompaniesModel().getId();
        } else {
            strCompanyId = "";
        }

        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<InvoiceModel> call1 = mApiInterface.addUniversalInvoiceRequestNew(mParam());
        call1.enqueue(new Callback<InvoiceModel>() {
            @Override
            public void onResponse(Call<InvoiceModel> call, retrofit2.Response<InvoiceModel> response) {
                AlertDialogManager.hideProgressDialog();
                InvoiceModel mModel = response.body();

                if (mModel.getStatus().equals("1")) {
                    UploadPdf();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<InvoiceModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private JsonObject mCopyParam() {
        JSONObject jsonObject = new JSONObject();
        itemsJsonArray = gettingItemIDsArray(itemsArrayList);
        itemsDiscountJsonArray = gettingItemDiscounArray(mArrayTotalDiscount);
        try {
//            jsonObject.put("invoice_id", mInVoicesModel.getInvoice_id());
            jsonObject.put("invoice_no", "JAORO" + mInVoicesModel.getInvoice_number());
            jsonObject.put("term_days", mInVoicesModel.getTerm_days());
            jsonObject.put("invoice_date", mInVoicesModel.getInvoice_date());
            jsonObject.put("search_company", strCompanyId);
            jsonObject.put("search_vessel", strVessalID);
            jsonObject.put("currency", mInVoicesModel.getCurrency());
            jsonObject.put("stamp", strStamPId);
            jsonObject.put("sign", strSignatireId);
            jsonObject.put("status", mInVoicesModel.getStatus());
            jsonObject.put("reference", mInVoicesModel.getRefrence1());
            jsonObject.put("reference1", mInVoicesModel.getRefrence2());
            jsonObject.put("reference2", mInVoicesModel.getRefrence3());
            jsonObject.put("bank_details", strBankId);
            jsonObject.put("payment_id", mInVoicesModel.getmPaymentModel().getPayment_id());
            jsonObject.put("items", itemsJsonArray);
            jsonObject.put("itemdiscount", itemsDiscountJsonArray);
            jsonObject.put("sub_total", mInVoicesModel.getmPaymentModel().getSubTotal());
            jsonObject.put("VAT", mInVoicesModel.getmPaymentModel().getVAT());
            jsonObject.put("vat_price", mInVoicesModel.getmPaymentModel().getVATPrice());
            jsonObject.put("total", mInVoicesModel.getmPaymentModel().getTotal());
            jsonObject.put("paid", mInVoicesModel.getmPaymentModel().getPaid());
            jsonObject.put("due", mInVoicesModel.getmPaymentModel().getBalanceDue());
            jsonObject.put("previous_items", String.valueOf(itemIDARRAY));
            jsonObject.put("previous_discounts", String.valueOf(totalDiscountIDARRAY));
            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
            Log.e(TAG, "**PARAM**" + jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
        return gsonObject;
    }

    public void executeCopyInVoicesAPI() {
        if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
            strVessalID = mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_id();
        } else {
            strVessalID = "";
        }
        if (mInVoicesModel.getmStampsModel() != null) {
            strStamPId = mInVoicesModel.getmStampsModel().getId();
        } else {
            strStamPId = "";
        }
        if (mInVoicesModel.getmSignatureModel() != null) {
            strSignatireId = mInVoicesModel.getmSignatureModel().getId();
        } else {
            strSignatireId = "";
        }
        if (mInVoicesModel.getmBankModel() != null) {
            strBankId = mInVoicesModel.getmBankModel().getId();
        } else {
            strBankId = "";
        }
        if (mInVoicesModel.getmCompaniesModel() != null) {
            strCompanyId = mInVoicesModel.getmCompaniesModel().getId();
        } else {
            strCompanyId = "";
        }

        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<InvoiceModel> call1 = mApiInterface.copyInvoiceRequestNew(mCopyParam());
        call1.enqueue(new Callback<InvoiceModel>() {
            @Override
            public void onResponse(Call<InvoiceModel> call, retrofit2.Response<InvoiceModel> response) {
                AlertDialogManager.hideProgressDialog();
                InvoiceModel mModel = response.body();
                if (mModel.getStatus().equals("1") || mModel.getStatus().equals("200")) {
                    UploadPdf();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<InvoiceModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private JSONArray gettingItemIDsArray(ArrayList<InvoiceAddItemModel> itemArrayList) {
        JSONArray mJsonArray = new JSONArray();
        try {
            for (int i = 0; i < itemArrayList.size(); i++) {
                //mJsonArray.put(itemArrayList.get(i).getItemID());
                JSONObject mJsonObject = new JSONObject();
                InvoiceAddItemModel mModel = itemArrayList.get(i);
                mJsonObject.put("serial_no", mModel.getItem());
                mJsonObject.put("quantity", mModel.getQuantity());
                mJsonObject.put("price", mModel.getUnitprice());
                mJsonObject.put("description", mModel.getDescription());
                mJsonObject.put("TotalAmount", mModel.getTotalAmount());
                mJsonObject.put("TotalunitPrice", mModel.getStrTotalunitPrice());
                mJsonObject.put("TotalAmountUnit", mModel.getStrTotalAmountUnit());
                mJsonObject.put("arraylistDiscount", getJSONArray(mModel.getmDiscountModelArrayList()));
                mJsonArray.put(mJsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mJsonArray;
    }

    private JSONArray gettingItemDiscounArray(ArrayList<AddTotalInvoiceDiscountModel> itemArrayList) {
        JSONArray mJsonArray = new JSONArray();
        try {
            for (int i = 0; i < itemArrayList.size(); i++) {
                //mJsonArray.put(itemArrayList.get(i).getItemID());
                JSONObject mJsonObject = new JSONObject();
                AddTotalInvoiceDiscountModel mModel = itemArrayList.get(i);
                mJsonObject.put("discount_add_value", mModel.getADD_DiscountValue());
                mJsonObject.put("discount_type", mModel.getDiscountType());
                mJsonObject.put("discount_add_description", mModel.getADD_Description());
                mJsonObject.put("discount_add_unitprice", mModel.getADD_UnitPrice());
                mJsonObject.put("discount_percent", mModel.getStrAddAndSubtracttotalPercent());
                mJsonObject.put("discount_total_value", mModel.getStrTotalvalue());
                mJsonObject.put("discount_add_and_subtract_total", mModel.getAddAndSubtractTotal());
                mJsonObject.put("discount_subtract_unitprice", mModel.getSubtract_UnitPrice());
                mJsonObject.put("discount_subtract_description", mModel.getSubtract_Description());
                mJsonObject.put("discount_subtract_value", mModel.getSubtract_DiscountValue());
                mJsonArray.put(mJsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mJsonArray;
    }

    private JSONArray getJSONArray(ArrayList<DiscountModel> mDiscountModelArrayList) {
        JSONArray mJsonArray = new JSONArray();
        try {
            for (int i = 0; i < mDiscountModelArrayList.size(); i++) {
                DiscountModel mDiscount = mDiscountModelArrayList.get(i);
                JSONObject mJsonObject = new JSONObject();
                mJsonObject.put("ADD_items", mDiscount.getADD_items());
                mJsonObject.put("ADD_quantity", mDiscount.getADD_quantity());
                mJsonObject.put("Add_description", mDiscount.getAdd_description());
                mJsonObject.put("ADD_unitPrice", mDiscount.getADD_unitPrice());
                mJsonObject.put("Add_Value", mDiscount.getAdd_Value());
                mJsonObject.put("Type", mDiscount.getType());
                mJsonObject.put("Subtract_items", mDiscount.getSubtract_items());
                mJsonObject.put("Subtract_quantity", mDiscount.getSubtract_quantity());
                mJsonObject.put("Subtract_description", mDiscount.getSubtract_description());
                mJsonObject.put("Subtract_unitPrice", mDiscount.getSubtract_unitPrice());
                mJsonObject.put("Subtract_Value", mDiscount.getSubtract_Value());
                mJsonObject.put("totalPercentValue", mDiscount.getStrtotalPercentValue());
                mJsonObject.put("addAndSubtractTotal", mDiscount.getAddAndSubtractTotal());
                mJsonObject.put("TotalEditAmount", mDiscount.getStrTotalEditAmount());
                mJsonArray.put(mJsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mJsonArray;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

//    private void createPdf(Bitmap mBitmap) {
//        PdfDocument document = new PdfDocument();
//        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(mBitmap.getWidth(), mBitmap.getHeight(), 1).create();
//        PdfDocument.Page page = document.startPage(pageInfo);
//        Canvas canvas = page.getCanvas();
//        Paint paint = new Paint();
//        canvas.drawPaint(paint);
//        paint.setColor(Color.WHITE);
//        canvas.drawColor(Color.WHITE);
//        canvas.drawBitmap(mBitmap, 0, 0, null);
//        document.finishPage(page);
//
//        // write the document content
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
//            try {
////                SaveImage(mBitmap);
//                String imageFileName = "INV" + strInvoiceNumber + strInvoiceVesselName + strCompanyName;
//
//                ByteArrayOutputStream bos = new ByteArrayOutputStream();
//                mBitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
//                byte[] bitmapdata = bos.toByteArray();
//                ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
//
//                Uri savedFileUri = savePDFFile(mActivity, bs, "files/pdf", imageFileName, "jaohar");
////                Uri savedFileUri = savePDFFile(mActivity, bs, "pdf", imageFileName, "jaohar");
//                Log.d("URI: ", savedFileUri.toString());
//                strPDFPath = getRealPathFromURI(mActivity, savedFileUri);
//                Log.d(TAG, "File Path: " + strPDFPath);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        } else {
//
//            File mGeneratedFile = null;
//            try {
//                    mGeneratedFile = createImageFile();
//                Log.e(TAG, "******Generated File Path*******" + mGeneratedFile.getAbsolutePath());
//                document.writeTo(new FileOutputStream(mGeneratedFile));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            strPDFPath = mGeneratedFile.getAbsolutePath();
//        }
//
//        // close the document
//        document.close();
//        sendEmail(strPDFPath);
//    }

    public Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);
        return b;
    }

    private void createPdf(Bitmap bitmap) {
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(bitmap.getWidth(), bitmap.getHeight(), 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();

        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#ffffff"));
        canvas.drawPaint(paint);
        String imageFileName = "";
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            imageFileName = "INV" + strInvoiceNumber + strInvoiceVesselName + strCompanyName + ".pdf";
            File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/jaoharr");
            if (directory.exists()) {

//                imageFileName = directory.toString() + File.separator;
            }
            if (!directory.exists()) {
                directory.mkdirs();
            }

            WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            DisplayMetrics displaymetrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            float hight = displaymetrics.heightPixels;
            float width = displaymetrics.widthPixels;

            int convertHighet = (int) hight, convertWidth = (int) width;

//        Resources mResources = getResources();
//        Bitmap bitmap = BitmapFactory.decodeResource(mResources, R.drawable.screenshot);

            bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
            paint.setColor(Color.BLUE);
            canvas.drawBitmap(bitmap, 0, 0, null);
            document.finishPage(page);

            // write the document content
//        String targetPdf = "/test.pdf";

            File file = new File(directory, imageFileName);
            //   File filePath = new File(String.valueOf(directory,));
            try {
                document.writeTo(new FileOutputStream(file));
//            btn_convert.setText("Check PDF");
                boolean_save = true;

//            Toast.makeText(this, "SAVED"+file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
//            Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
            }
            strPDFPath = file.getAbsolutePath();
//                // close the document
//                document.close();
//                sendEmail(strPDFPath);
        } else {
            File mGeneratedFile = null;
            try {
                mGeneratedFile = createImageFile();
                Log.e(TAG, "******Generated File Path*******" + mGeneratedFile.getAbsolutePath());
                document.writeTo(new FileOutputStream(mGeneratedFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
            strPDFPath = mGeneratedFile.getAbsolutePath();}
//                // close the document
        document.close();
        sendEmail(strPDFPath);
//        }


    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "INV" + strInvoiceNumber + strInvoiceVesselName + strCompanyName;
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File mFile = File.createTempFile(imageFileName, /* prefix */
                ".pdf", /* suffix */
                storageDir /* directory */);
        File from = new File(storageDir, mFile.getName());
        File to = new File(storageDir, imageFileName + ".pdf");
        from.renameTo(to);
        Log.i("Directory is", storageDir.toString());
        Log.i("Default path is", storageDir.toString());
        Log.i("From path is", from.toString());
        Log.i("To path is", to.toString());
        // Save a file: path for use with ACTION_VIEW intents
        String path = new File(imageFileName).getParent();
        mStoragePath = to.getAbsolutePath();
        return to;
    }

    private void sendEmail(String strFilePath) {
        Uri imageURI = null;
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", new File(strFilePath));
//                imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", new File(strFilePath));
                mFileImageUrl = imageURI;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            imageURI = Uri.fromFile(new File(strFilePath));
        }

        try {
            InputStream iStream = getContentResolver().openInputStream(imageURI);
            strByteArray = getBytes(iStream);
            strPDFBase64 = Utilities.convertByteArrayToBase64(strByteArray);
            Log.e(TAG, strPDFBase64);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (JaoharConstants.IS_Click_From_Edit == true) {
            executeEditInVoicesAPI();
        } else if (JaoharConstants.IS_Click_From_DRAFT == true) {
            executeDraftVoicesAPI();
        } else if (JaoharConstants.IS_Click_From_Edit_DRAFT == true) {
            executeEditDraftInVoicesAPI();
        } else if (JaoharConstants.IS_Click_From_Copy == true) {
            executeCopyInVoicesAPI();
        } else if (JaoharConstants.IS_Click_From_DRAFT_SAVE == true) {
            executeSaveDraftInVoicesAPI();
        } else {
            executeAddInVoicesAPI();
        }

//        Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:")); // it's not ACTION_SEND
//        intent.setType("text/plain");
//        intent.putExtra(Intent.EXTRA_SUBJECT, "Invoice Details: ");
////        intent.putExtra(Intent.EXTRA_TEXT, "Ops (Jaohar UK Limited)\n"+"Invoice Details PDF:\n"+"Hello Please find the attached pdf:");
//        intent.putExtra(Intent.EXTRA_TEXT, "Please find attached requested invoice");
//        intent.putExtra(Intent.EXTRA_STREAM, imageURI);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
//        startActivity(intent);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{
                READ_EXTERNAL_STORAGE,
                WRITE_EXTERNAL_STORAGE
        }, REQUEST_PERMISSIONS);
    }

//    private void requestPermission() {
//        ActivityCompat.requestPermissions(mActivity, new String[]{
//                READ_EXTERNAL_STORAGE,
//                WRITE_EXTERNAL_STORAGE,
//                MANAGE_EXTERNAL_STORAGE,
//        }, REQUEST_PERMISSIONS);
//    } Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION

    public byte[] getBytes(InputStream inputStream) {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
//        int bufferSize = 2024;
        byte[] buffer = new byte[MEGABYTE];
        int len = 0;
        while (true) {
            try {
                if (!((len = inputStream.read(buffer)) > 0)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (grantResults.length > 0) {
                    boolean writeStorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean readStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean manageStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (writeStorage && readStorage && manageStorage) {
                        boolean_permission = true;
                    } else {
                        Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_LONG).show();
                        requestPermission();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
//        int manage = ContextCompat.checkSelfPermission(getApplicationContext(), MANAGE_EXTERNAL_STORAGE);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED ;
    }


//    private Map<String, String> mParams() {
//        Map<String, String> mMap = new HashMap<>();
//        mMap.put("record_id", mInVoicesModel.getInvoice_number());
//        mMap.put("name", strInvoiceNumber + ".pdf");
//        mMap.put("pdf", strPDFBase64);
//        Log.e(TAG, "**PARAM**" + mMap.toString());
//        return mMap;
//    }
//
//    private void UploadPdf() {
//        AlertDialogManager.showProgressDialog(mActivity);
//        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
//        mApiInterface.uploadPdfInvoiceRequest(mParams()).enqueue(new Callback<StatusMsgModel>() {
//            @Override
//            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "**RESPONSE**" + response.body());
//                StatusMsgModel mModel = response.body();
//                if (mModel.getStatus() == 1) {
//                    showAlerDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
//                } else {
//                    Log.e(TAG, "Unexpected*********:" + mModel.getMessage());
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
//                Log.e(TAG, "**ERROR**" + t.getMessage());
//            }
//        });
//    }

    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                IS_INVOICE_UPLOADED = true;
                if (JaoharConstants.IS_Click_From_DRAFT == true) {
                    Intent mIntent = new Intent(mActivity, AllDraftsInvoicesActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {
                    onBackPressed();
//                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
//                    startActivity(mIntent);
//                    finish();
                }
            }
        });
        alertDialog.show();
    }

    // generate dynamically string
    public String getAlphaNumericString() {
        int n = 20;

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        Log.e(TAG, "**********Image Name******" + sb.toString());
        return sb.toString();
    }


//    private void UploadPdf() {
////        "INV"+strInvoiceNumber+strInvoiceVesselName+strCompanyName+".pdf"
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.Update_Invoice_Pdf;
//        try {
//            jsonObject.put("record_id", mInVoicesModel.getInvoice_number());
//            jsonObject.put("name", strInvoiceNumber + ".pdf");
//            jsonObject.put("pdf", strPDFBase64);
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        AlertDialogManager.showProgressDialog(mActivity);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        // tell everybody you have succed upload image and post strings
//                        Log.e(TAG, "Messsage******:" + message);
//                        showAlerDialog(mActivity, getString(R.string.app_name), "" + message);
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******error*****" + error);
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void executeEditInVoicesAPI() {
//        String strVessalID = "";
//        String strStamPId = "";
//        String strSignatireId = "";
//        String strBankId = "";
//        String strCompanyId = "";
//        if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
//            strVessalID = mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_id();
//        } else {
//            strVessalID = "";
//        }
//        if (mInVoicesModel.getmStampsModel() != null) {
//            strStamPId = mInVoicesModel.getmStampsModel().getId();
//        } else {
//            strStamPId = "";
//        }
//        if (mInVoicesModel.getmSignatureModel() != null) {
//            strSignatireId = mInVoicesModel.getmSignatureModel().getId();
//        } else {
//            strSignatireId = "";
//        }
//        if (mInVoicesModel.getmBankModel() != null) {
//            strBankId = mInVoicesModel.getmBankModel().getId();
//        } else {
//            strBankId = "";
//        }
//        if (mInVoicesModel.getmCompaniesModel() != null) {
//            strCompanyId = mInVoicesModel.getmCompaniesModel().getId();
//        } else {
//            strCompanyId = "";
//        }
//        String strInvoiceID = JaoharConstants.Invoice_ID;
////        JaoharConstants.Invoice_ID =strInvoiceID;
////        String strUrl = JaoharConstants.EDIT_INVOICE + "?invoice_no=" + mInVoicesModel.getInvoice_number() +
////                "&invoice_date=" + mInVoicesModel.getInvoice_date() +
////                "&term_days=" + mInVoicesModel.getTerm_days() +
////                "&search_company=" + strCompanyId +
////                "&search_vessel=" + strVessalID +
////                "&currency=" + mInVoicesModel.getCurrency() +
////                "&stamp=" + strStamPId +
////                "&sign=" + strSignatireId +
////                "&status=" +  mInVoicesModel.getStatus() +
////                "&reference=" + mInVoicesModel.getRefrence1() +
////                "&reference1=" + mInVoicesModel.getRefrence2() +
////                "&reference2=" + mInVoicesModel.getRefrence3() +
////                "&bank_details=" + strBankId +
////                "&payment_id=" +mInVoicesModel.getmPaymentModel().getPayment_id() +
////                "&items=" + gettingItemIDsArray(itemsArrayList) +
////                "&itemdiscount=" + gettingItemDiscounArray(mArrayTotalDiscount) +
////                "&sub_total=" + mInVoicesModel.getmPaymentModel().getSubTotal() +
////                "&VAT=" + mInVoicesModel.getmPaymentModel().getVAT() +
////                "&vat_price=" + mInVoicesModel.getmPaymentModel().getVATPrice() +
////                "&total=" + mInVoicesModel.getmPaymentModel().getTotal() +
////                "&paid=" + mInVoicesModel.getmPaymentModel().getPaid() +
////                "&due=" + mInVoicesModel.getmPaymentModel().getBalanceDue() +
////                "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "")+
////               "&invoice_id=" +strInvoiceID +
////                "&previous_items=" + itemIDARRAY+
////                "&previous_discounts=" + totalDiscountIDARRAY;
////
////
////
////        strUrl = strUrl.replace(" ", "_");
////        Log.e(TAG, "***URL***" + strUrl);
////
////        AlertDialogManager.showProgressDialog(mActivity);
////        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
////
////            @Override
////            public void onResponse(String response) {
////                AlertDialogManager.hideProgressDialog();
////                Log.e(TAG, "*****Response****" + response);
////
////                try {
////                    JSONObject mJsonObject = new JSONObject(response);
//////                    if (mJsonObject.getString("status").equals("1")) {
//////                        showAlerDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//////                    }
//////                    else
////                    UploadPdf();
////                    JaoharConstants.IS_Click_From_Edit=false;
////                    if (mJsonObject.getString("status").equals("100")) {
////                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
////                    }
////                    else if (mJsonObject.getString("status").equals("0")) {
////                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
////                    }
////                    else if (mJsonObject.getString("status").equals("200")) {
////                        showAlerDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
////                    }
////
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
////
////            }
////        }, new Response.ErrorListener() {
////
////            @Override
////            public void onErrorResponse(VolleyError error) {
////                AlertDialogManager.hideProgressDialog();
////                Log.e(TAG, "***Error**" + error.toString());
////            }
////        }) {
////            @Override
////            public Map<String, String> getHeaders() throws AuthFailureError {
////                HashMap<String, String> headers = new HashMap<String, String>();
////                headers.put("Content-Type", "application/json");
////                return headers;
////            }
////        };
////        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.EDIT_INVOICE;
//        try {
//            jsonObject.put("invoice_no", mInVoicesModel.getInvoice_number());
//            jsonObject.put("term_days", mInVoicesModel.getTerm_days());
//            jsonObject.put("invoice_date", mInVoicesModel.getInvoice_date());
//            jsonObject.put("search_company", strCompanyId);
//            jsonObject.put("search_vessel", strVessalID);
//            jsonObject.put("currency", mInVoicesModel.getCurrency());
//            jsonObject.put("stamp", strStamPId);
//            jsonObject.put("sign", strSignatireId);
//            jsonObject.put("status", mInVoicesModel.getStatus());
//            jsonObject.put("reference", mInVoicesModel.getRefrence1());
//            jsonObject.put("reference1", mInVoicesModel.getRefrence2());
//            jsonObject.put("reference2", mInVoicesModel.getRefrence3());
//            jsonObject.put("bank_details", strBankId);
//            jsonObject.put("payment_id", mInVoicesModel.getmPaymentModel().getPayment_id());
//            jsonObject.put("items", gettingItemIDsArray(itemsArrayList));
//            jsonObject.put("itemdiscount", gettingItemDiscounArray(mArrayTotalDiscount));
//            jsonObject.put("sub_total", mInVoicesModel.getmPaymentModel().getSubTotal());
//            jsonObject.put("VAT", mInVoicesModel.getmPaymentModel().getVAT());
//            jsonObject.put("vat_price", mInVoicesModel.getmPaymentModel().getVATPrice());
//            jsonObject.put("total", mInVoicesModel.getmPaymentModel().getTotal());
//            jsonObject.put("paid", mInVoicesModel.getmPaymentModel().getPaid());
//            jsonObject.put("due", mInVoicesModel.getmPaymentModel().getBalanceDue());
//            jsonObject.put("invoice_id", strInvoiceID);
//            jsonObject.put("previous_items", itemIDARRAY);
//            jsonObject.put("previous_discounts", totalDiscountIDARRAY);
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        AlertDialogManager.showProgressDialog(mActivity);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("200")) {
//                        // tell everybody you have succed upload image and post strings
//                        Log.e(TAG, "Messsage******:" + message);
//                        UploadPdf();
//                        JaoharConstants.IS_Click_From_Edit=false;
////                        showAlerDialog(mActivity, getString(R.string.app_name), "" + message);
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******error*****" + error);
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void UploadPdf() {
        AlertDialogManager.showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBodyDoc1 = null;

        RequestBody requestFileDoc1 = RequestBody.create(MediaType.parse("multipart/form-data"), strByteArray);
        mMultipartBodyDoc1 = MultipartBody.Part.createFormData("pdf", getAlphaNumericString() + ".pdf", requestFileDoc1);
        Log.e("img", "img" + mMultipartBodyDoc1);

        RequestBody record_id = RequestBody.create(MediaType.parse("multipart/form-data"), mInVoicesModel.getInvoice_number());
        RequestBody name = RequestBody.create(MediaType.parse("multipart/form-data"), strInvoiceNumber + ".pdf");
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.uploadPdfInvoiceRequest(record_id, name, mMultipartBodyDoc1).enqueue(new Callback<StatusMsgModel>() {

            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlerDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    Log.e(TAG, "Unexpected*********:" + mModel.getMessage());
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

//    private Map<String, String> mEditParam() {
//        Map<String, String> mMap = new HashMap<>();
//        mMap.put("invoice_id", mInVoicesModel.getInvoice_id());
//        mMap.put("invoice_no", mInVoicesModel.getInvoice_number());
//        mMap.put("term_days", mInVoicesModel.getTerm_days());
//        mMap.put("invoice_date", mInVoicesModel.getInvoice_date());
//        mMap.put("search_company", strCompanyId);
//        mMap.put("search_vessel", strVessalID);
//        mMap.put("currency", mInVoicesModel.getCurrency());
//        mMap.put("stamp", strStamPId);
//        mMap.put("sign", strSignatireId);
//        mMap.put("status", mInVoicesModel.getStatus());
//        mMap.put("reference", mInVoicesModel.getRefrence1());
//        mMap.put("reference1", mInVoicesModel.getRefrence2());
//        mMap.put("reference2", mInVoicesModel.getRefrence3());
//        mMap.put("bank_details", strBankId);
//        mMap.put("payment_id", mInVoicesModel.getmPaymentModel().getPayment_id());
//        mMap.put("items", String.valueOf(gettingItemIDsArray(itemsArrayList)));
//        mMap.put("itemdiscount", String.valueOf(gettingItemDiscounArray(mArrayTotalDiscount)));
//        mMap.put("sub_total", mInVoicesModel.getmPaymentModel().getSubTotal());
//        mMap.put("VAT", mInVoicesModel.getmPaymentModel().getVAT());
//        mMap.put("vat_price", mInVoicesModel.getmPaymentModel().getVATPrice());
//        mMap.put("total", mInVoicesModel.getmPaymentModel().getTotal());
//        mMap.put("paid", mInVoicesModel.getmPaymentModel().getPaid());
//        mMap.put("due", mInVoicesModel.getmPaymentModel().getBalanceDue());
//        mMap.put("previous_items", String.valueOf(itemIDARRAY));
//        mMap.put("previous_discounts", String.valueOf(totalDiscountIDARRAY));
//        mMap.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//        Log.e(TAG, "**PARAM**" + mMap.toString());
//        return mMap;
//    }

    private JsonObject mEditParam() {
        JSONObject jsonObject = new JSONObject();
        itemsJsonArray = gettingItemIDsArray(itemsArrayList);
        itemsDiscountJsonArray = gettingItemDiscounArray(mArrayTotalDiscount);
        try {
            jsonObject.put("invoice_id", mInVoicesModel.getInvoice_id());
            jsonObject.put("invoice_no", "JAORO" + mInVoicesModel.getInvoice_number());
            jsonObject.put("term_days", mInVoicesModel.getTerm_days());
            jsonObject.put("invoice_date", mInVoicesModel.getInvoice_date());
            jsonObject.put("search_company", strCompanyId);
            jsonObject.put("search_vessel", strVessalID);
            jsonObject.put("currency", mInVoicesModel.getCurrency());
            jsonObject.put("stamp", strStamPId);
            jsonObject.put("sign", strSignatireId);
            jsonObject.put("status", mInVoicesModel.getStatus());
            jsonObject.put("reference", mInVoicesModel.getRefrence1());
            jsonObject.put("reference1", mInVoicesModel.getRefrence2());
            jsonObject.put("reference2", mInVoicesModel.getRefrence3());
            jsonObject.put("bank_details", strBankId);
            jsonObject.put("payment_id", mInVoicesModel.getmPaymentModel().getPayment_id());
            jsonObject.put("items", itemsJsonArray);
            jsonObject.put("itemdiscount", itemsDiscountJsonArray);
            jsonObject.put("sub_total", mInVoicesModel.getmPaymentModel().getSubTotal());
            jsonObject.put("VAT", mInVoicesModel.getmPaymentModel().getVAT());
            jsonObject.put("vat_price", mInVoicesModel.getmPaymentModel().getVATPrice());
            jsonObject.put("total", mInVoicesModel.getmPaymentModel().getTotal());
            jsonObject.put("paid", mInVoicesModel.getmPaymentModel().getPaid());
            jsonObject.put("due", mInVoicesModel.getmPaymentModel().getBalanceDue());
            jsonObject.put("previous_items", String.valueOf(itemIDARRAY));
            jsonObject.put("previous_discounts", String.valueOf(totalDiscountIDARRAY));
            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
            Log.e(TAG, "**PARAM**" + jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
        return gsonObject;
    }

    private void executeEditInVoicesAPI() {
        if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
            strVessalID = mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_id();
        } else {
            strVessalID = "";
        }
        if (mInVoicesModel.getmStampsModel() != null) {
            strStamPId = mInVoicesModel.getmStampsModel().getId();
        } else {
            strStamPId = "";
        }
        if (mInVoicesModel.getmSignatureModel() != null) {
            strSignatireId = mInVoicesModel.getmSignatureModel().getId();
        } else {
            strSignatireId = "";
        }
        if (mInVoicesModel.getmBankModel() != null) {
            strBankId = mInVoicesModel.getmBankModel().getId();
        } else {
            strBankId = "";
        }
        if (mInVoicesModel.getmCompaniesModel() != null) {
            strCompanyId = mInVoicesModel.getmCompaniesModel().getId();
        } else {
            strCompanyId = "";
        }
        strInvoiceID = JaoharConstants.Invoice_ID;

        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editInvoiceRequest(mEditParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 200 || mModel.getStatus().equals("1")) {

                    /* for back press */
                    JaoharConstants.IS_INVOICE_BACK = true;

                    // tell everybody you have succed upload image and post strings
                    Log.e(TAG, "Messsage******:" + mModel.getMessage());
                    UploadPdf();
                    JaoharConstants.IS_Click_From_Edit = false;
//                        showAlerDialog(mActivity, getString(R.string.app_name), "" + message);
                } else {
                    Log.e(TAG, "Unexpected*********:" + mModel.getMessage());
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private JsonObject mEditDraftParam() {
        JSONObject jsonObject = new JSONObject();
        itemsJsonArray = gettingItemIDsArray(itemsArrayList);
        itemsDiscountJsonArray = gettingItemDiscounArray(mArrayTotalDiscount);
        try {
            jsonObject.put("draft", "1");
            jsonObject.put("invoice_id", mInVoicesModel.getInvoice_id());
            jsonObject.put("invoice_no", "JAORO" + mInVoicesModel.getInvoice_number());
            jsonObject.put("term_days", mInVoicesModel.getTerm_days());
            jsonObject.put("invoice_date", mInVoicesModel.getInvoice_date());
            jsonObject.put("search_company", strCompanyId);
            jsonObject.put("search_vessel", strVessalID);
            jsonObject.put("currency", mInVoicesModel.getCurrency());
            jsonObject.put("stamp", strStamPId);
            jsonObject.put("sign", strSignatireId);
            jsonObject.put("status", mInVoicesModel.getStatus());
            jsonObject.put("reference", mInVoicesModel.getRefrence1());
            jsonObject.put("reference1", mInVoicesModel.getRefrence2());
            jsonObject.put("reference2", mInVoicesModel.getRefrence3());
            jsonObject.put("bank_details", strBankId);
            jsonObject.put("payment_id", mInVoicesModel.getmPaymentModel().getPayment_id());
            jsonObject.put("items", itemsJsonArray);
            jsonObject.put("itemdiscount", itemsDiscountJsonArray);
            jsonObject.put("sub_total", mInVoicesModel.getmPaymentModel().getSubTotal());
            jsonObject.put("VAT", mInVoicesModel.getmPaymentModel().getVAT());
            jsonObject.put("vat_price", mInVoicesModel.getmPaymentModel().getVATPrice());
            jsonObject.put("total", mInVoicesModel.getmPaymentModel().getTotal());
            jsonObject.put("paid", mInVoicesModel.getmPaymentModel().getPaid());
            jsonObject.put("due", mInVoicesModel.getmPaymentModel().getBalanceDue());
            jsonObject.put("previous_items", String.valueOf(itemIDARRAY));
            jsonObject.put("previous_discounts", String.valueOf(totalDiscountIDARRAY));
            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
            Log.e(TAG, "**PARAM**" + jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
        return gsonObject;
    }

    private void executeEditDraftInVoicesAPI() {
        if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
            strVessalID = mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_id();
        } else {
            strVessalID = "";
        }
        if (mInVoicesModel.getmStampsModel() != null) {
            strStamPId = mInVoicesModel.getmStampsModel().getId();
        } else {
            strStamPId = "";
        }
        if (mInVoicesModel.getmSignatureModel() != null) {
            strSignatireId = mInVoicesModel.getmSignatureModel().getId();
        } else {
            strSignatireId = "";
        }
        if (mInVoicesModel.getmBankModel() != null) {
            strBankId = mInVoicesModel.getmBankModel().getId();
        } else {
            strBankId = "";
        }
        if (mInVoicesModel.getmCompaniesModel() != null) {
            strCompanyId = mInVoicesModel.getmCompaniesModel().getId();
        } else {
            strCompanyId = "";
        }
        strInvoiceID = JaoharConstants.Invoice_ID;

        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editInvoiceRequest(mEditDraftParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 200 || mModel.getStatus().equals("1")) {

                    /* for back press */
                    JaoharConstants.IS_INVOICE_BACK = true;

                    // tell everybody you have succed upload image and post strings
                    Log.e(TAG, "Messsage******:" + mModel.getMessage());
                    UploadPdf();
                    JaoharConstants.IS_Click_From_Edit = false;
//                        showAlerDialog(mActivity, getString(R.string.app_name), "" + message);
                } else {
                    Log.e(TAG, "Unexpected*********:" + mModel.getMessage());
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private JsonObject mDraftParam() {
        JSONObject jsonObject = new JSONObject();
        itemsJsonArray = gettingItemIDsArray(itemsArrayList);
        itemsDiscountJsonArray = gettingItemDiscounArray(mArrayTotalDiscount);
        try {
            jsonObject.put("draft", "1");
            jsonObject.put("invoice_no", mInVoicesModel.getInvoice_number());
            jsonObject.put("term_days", mInVoicesModel.getTerm_days());
            jsonObject.put("invoice_date", mInVoicesModel.getInvoice_date());
            jsonObject.put("search_company", strCompanyId);
            jsonObject.put("search_vessel", strVessalID);
            jsonObject.put("currency", mInVoicesModel.getCurrency());
            jsonObject.put("stamp", strStamPId);
            jsonObject.put("sign", strSignatireId);
            jsonObject.put("status", mInVoicesModel.getStatus());
            jsonObject.put("reference", mInVoicesModel.getRefrence1());
            jsonObject.put("reference1", mInVoicesModel.getRefrence2());
            jsonObject.put("reference2", mInVoicesModel.getRefrence3());
            jsonObject.put("bank_details", strBankId);
            jsonObject.put("payment_id", mInVoicesModel.getmPaymentModel().getPayment_id());
            jsonObject.put("items", itemsJsonArray);
            jsonObject.put("itemdiscount", itemsDiscountJsonArray);
            jsonObject.put("sub_total", mInVoicesModel.getmPaymentModel().getSubTotal());
            jsonObject.put("VAT", mInVoicesModel.getmPaymentModel().getVAT());
            jsonObject.put("vat_price", mInVoicesModel.getmPaymentModel().getVATPrice());
            jsonObject.put("total", mInVoicesModel.getmPaymentModel().getTotal());
            jsonObject.put("paid", mInVoicesModel.getmPaymentModel().getPaid());
            jsonObject.put("due", mInVoicesModel.getmPaymentModel().getBalanceDue());
            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
            Log.e(TAG, "**PARAM**" + jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
        return gsonObject;
    }

    public void executeDraftVoicesAPI() {
        if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
            strVessalID = mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_id();
        } else {
            strVessalID = "";
        }
        if (mInVoicesModel.getmStampsModel() != null) {
            strStamPId = mInVoicesModel.getmStampsModel().getId();
        } else {
            strStamPId = "";
        }
        if (mInVoicesModel.getmSignatureModel() != null) {
            strSignatireId = mInVoicesModel.getmSignatureModel().getId();
        } else {
            strSignatireId = "";
        }
        if (mInVoicesModel.getmBankModel() != null) {
            strBankId = mInVoicesModel.getmBankModel().getId();
        } else {
            strBankId = "";
        }
        if (mInVoicesModel.getmCompaniesModel() != null) {
            strCompanyId = mInVoicesModel.getmCompaniesModel().getId();
        } else {
            strCompanyId = "";
        }

        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<InvoiceModel> call1 = mApiInterface.addInvoiceRequestNew(mDraftParam());
        call1.enqueue(new Callback<InvoiceModel>() {
            @Override
            public void onResponse(Call<InvoiceModel> call, retrofit2.Response<InvoiceModel> response) {
                AlertDialogManager.hideProgressDialog();
                InvoiceModel mModel = response.body();

                if (mModel.getStatus().equals("1")) {
                    UploadPdf();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<InvoiceModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private JsonObject mSaveDraftParam() {
        JSONObject jsonObject = new JSONObject();
        itemsJsonArray = gettingItemIDsArray(itemsArrayList);
        itemsDiscountJsonArray = gettingItemDiscounArray(mArrayTotalDiscount);
        try {
            jsonObject.put("draft", "0");
            jsonObject.put("invoice_id", mInVoicesModel.getInvoice_id());
            jsonObject.put("invoice_no", "JAORO" + mInVoicesModel.getInvoice_number());
            jsonObject.put("term_days", mInVoicesModel.getTerm_days());
            jsonObject.put("invoice_date", mInVoicesModel.getInvoice_date());
            jsonObject.put("search_company", strCompanyId);
            jsonObject.put("search_vessel", strVessalID);
            jsonObject.put("currency", mInVoicesModel.getCurrency());
            jsonObject.put("stamp", strStamPId);
            jsonObject.put("sign", strSignatireId);
            jsonObject.put("status", mInVoicesModel.getStatus());
            jsonObject.put("reference", mInVoicesModel.getRefrence1());
            jsonObject.put("reference1", mInVoicesModel.getRefrence2());
            jsonObject.put("reference2", mInVoicesModel.getRefrence3());
            jsonObject.put("bank_details", strBankId);
            jsonObject.put("payment_id", mInVoicesModel.getmPaymentModel().getPayment_id());
            jsonObject.put("items", itemsJsonArray);
            jsonObject.put("itemdiscount", itemsDiscountJsonArray);
            jsonObject.put("sub_total", mInVoicesModel.getmPaymentModel().getSubTotal());
            jsonObject.put("VAT", mInVoicesModel.getmPaymentModel().getVAT());
            jsonObject.put("vat_price", mInVoicesModel.getmPaymentModel().getVATPrice());
            jsonObject.put("total", mInVoicesModel.getmPaymentModel().getTotal());
            jsonObject.put("paid", mInVoicesModel.getmPaymentModel().getPaid());
            jsonObject.put("due", mInVoicesModel.getmPaymentModel().getBalanceDue());
            jsonObject.put("previous_items", String.valueOf(itemIDARRAY));
            jsonObject.put("previous_discounts", String.valueOf(totalDiscountIDARRAY));
            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
            Log.e(TAG, "**PARAM**" + jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonObject.toString());
        return gsonObject;
    }

    private void executeSaveDraftInVoicesAPI() {
        if (mInVoicesModel.getmVesselSearchInvoiceModel() != null) {
            strVessalID = mInVoicesModel.getmVesselSearchInvoiceModel().getVessel_id();
        } else {
            strVessalID = "";
        }
        if (mInVoicesModel.getmStampsModel() != null) {
            strStamPId = mInVoicesModel.getmStampsModel().getId();
        } else {
            strStamPId = "";
        }
        if (mInVoicesModel.getmSignatureModel() != null) {
            strSignatireId = mInVoicesModel.getmSignatureModel().getId();
        } else {
            strSignatireId = "";
        }
        if (mInVoicesModel.getmBankModel() != null) {
            strBankId = mInVoicesModel.getmBankModel().getId();
        } else {
            strBankId = "";
        }
        if (mInVoicesModel.getmCompaniesModel() != null) {
            strCompanyId = mInVoicesModel.getmCompaniesModel().getId();
        } else {
            strCompanyId = "";
        }
        strInvoiceID = JaoharConstants.Invoice_ID;

        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editInvoiceRequest(mSaveDraftParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 200 || mModel.getStatus().equals("1")) {

                    /* for back press */
                    JaoharConstants.IS_INVOICE_BACK_FROM_DRAFT = true;

                    // tell everybody you have succed upload image and post strings
                    Log.e(TAG, "Messsage******:" + mModel.getMessage());
                    UploadPdf();
                    JaoharConstants.IS_Click_From_Edit = false;
//                        showAlerDialog(mActivity, getString(R.string.app_name), "" + message);
                } else {
                    Log.e(TAG, "Unexpected*********:" + mModel.getMessage());
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

//    void convertToPdf() throws IOException, BadElementException {
//        Document document = new Document();
//
//        String directoryPath = android.os.Environment.getExternalStorageDirectory().toString();
//
//        PdfWriter.getInstance(document, new FileOutputStream(directoryPath + "/example.pdf")); //  Change pdf's name.
//
//        document.open();
//
//        Image image = Image.getInstance(directoryPath + "/" + "example.jpg");  // Change image's name and extension.
//
//        float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
//                - document.rightMargin() - 0) / image.getWidth()) * 100; // 0 means you have no indentation. If you have any, change it.
//        image.scalePercent(scaler);
//        image.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
//
//        document.add(image);
//        document.close();
//    }
}