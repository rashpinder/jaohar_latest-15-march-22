package jaohar.com.jaohar.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import jaohar.com.jaohar.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.Utilities;

class EditADDDiscountActivity extends BaseActivity {
    Activity mActivity = EditADDDiscountActivity.this;
    String TAG = EditADDDiscountActivity.this.getClass().getSimpleName(), strDescription, strSubtractValue, strTotal, strItem, strQuantity, strPrice;
    LinearLayout llLeftLL;
    TextView txtSubTotalTV, txtQuantityTV, txtVatPriceTV, txtTotalTV;
    EditText AddValueTV, txtDescriptionTV;
    Button btnSubmitB;
    InvoiceAddItemModel mModel, pModel;
    int mPosition;
    private long parcentValue;
    private long sumData;
    double totalPercent;
    ArrayList<DiscountModel> mDiscountModelArrayList = new ArrayList<DiscountModel>();
    DiscountModel mDiscountModel;
    private double totalPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_adddiscount);
    }


    @Override
    protected void setViewsIDs() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        txtSubTotalTV = (TextView) findViewById(R.id.txtSubTotalTV);
        txtQuantityTV = (TextView) findViewById(R.id.txtQuantityTV);
        txtDescriptionTV = (EditText) findViewById(R.id.txtDescriptionTV);
        txtVatPriceTV = (TextView) findViewById(R.id.txtVatPriceTV);
        txtTotalTV = (TextView) findViewById(R.id.txtTotalTV);
        AddValueTV = (EditText) findViewById(R.id.AddValueTV);
        btnSubmitB = (Button) findViewById(R.id.btnSubmitB);

        if (getIntent() != null) {
            mDiscountModel = new DiscountModel();
            mDiscountModel = (DiscountModel) getIntent().getSerializableExtra("Model");
// mDiscountModelArrayList =mModel.getmDiscountModelArrayList();
            mPosition = getIntent().getIntExtra("Position", 0);
// mDiscountModel=mDiscountModelArrayList.get(mPosition);
// strItemID = mModel.getItemID();
        }
// if(!mDiscountModel.getStrTotalEditAmount().equals("")){
// double totalunitPrice = Double.parseDouble(mDiscountModel.getStrTotalEditAmount());
// totalPrice = Double.parseDouble(String.valueOf(mDiscountModel.getADD_quantity())) *totalunitPrice ;
// }else {
// }
        totalPrice = Double.parseDouble(mDiscountModel.getADD_unitPrice());
        String strTotal1 = String.valueOf(totalPrice);
        strItem = mDiscountModel.getADD_items();
        strQuantity = String.valueOf(mDiscountModel.getADD_quantity());
        strPrice = String.valueOf(mDiscountModel.getADD_unitPrice());
        strTotal = String.valueOf(mDiscountModel.getAddAndSubtractTotal());
        strSubtractValue = String.valueOf(mDiscountModel.getAdd_Value());
        strDescription = String.valueOf(mDiscountModel.getAdd_description());
        totalPercent = Double.parseDouble(mDiscountModel.getStrtotalPercentValue());
        txtSubTotalTV.setText(strItem);
// txtVatPriceTV.setText(strPrice);
        txtVatPriceTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strTotal1)));
        txtDescriptionTV.setText(strDescription);
        txtQuantityTV.setText(strQuantity);
        AddValueTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strSubtractValue)));
        txtTotalTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strTotal)));


    }


    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        btnSubmitB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtDescriptionTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_description));
                } else if (AddValueTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_value));
                } else {
                    mDiscountModel = new DiscountModel();
                    mDiscountModel.setAdd_description(txtDescriptionTV.getText().toString());
                    mDiscountModel.setADD_items(strItem);
                    mDiscountModel.setADD_quantity(strQuantity);
                    mDiscountModel.setADD_unitPrice(strPrice);
                    mDiscountModel.setAdd_Value(AddValueTV.getText().toString());
                    mDiscountModel.setType("ADD");

                    mDiscountModel.setAddAndSubtractTotal(txtTotalTV.getText().toString());
                    mDiscountModel.setStrtotalPercentValue(String.valueOf(totalPercent));
                    mDiscountModelArrayList.add(mDiscountModel);
                    Log.e(TAG, "ARRAY2==============" + mDiscountModelArrayList.size());
// mModel.setmDiscountModelArrayList(mDiscountModelArrayList);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("pModel", mDiscountModel);
                    returnIntent.putExtra("pModel1", mDiscountModelArrayList);
                    returnIntent.putExtra("Position", mPosition);
                    returnIntent.putExtra("IS_ADD_DISCOUNT", true);
                    setResult(123, returnIntent);
                    onBackPressed();
                    finish();
                }
            }
        });

        AddValueTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double basePrice = 0;
                double sum;
                String str = AddValueTV.getText().toString();
                try {
                    basePrice = totalPrice;// Integer.parseInt()
                    float percentValue = (Float.parseFloat(str)) / 100;
// double totalPercent;
                    totalPercent = ((basePrice * Double.parseDouble(str)) / 100);
// sum = basePrice + totalPercent;
                    totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                    sum = totalPrice + totalPercent;
                    sum = Utilities.getRoundOff2Decimal(sum);
                    txtTotalTV.setText("" + sum);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
