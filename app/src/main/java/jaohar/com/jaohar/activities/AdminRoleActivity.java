package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;

import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.manager_module.SearchAdminAndRoleActivity;
import jaohar.com.jaohar.adapters.AdminRoleAdapter;
import jaohar.com.jaohar.interfaces.ChangeRoleInterface;
import jaohar.com.jaohar.interfaces.DeleteUserInterface;
import jaohar.com.jaohar.interfaces.EnableDisableAdimRoleInterface;
import jaohar.com.jaohar.models.AllAdminBySearchModel;
import jaohar.com.jaohar.models.AllUser;
import jaohar.com.jaohar.models.GetAllAdminRoleModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AdminRoleActivity extends BaseActivity implements ChangeRoleInterface {
    Activity mActivity = AdminRoleActivity.this;
    String TAG = AdminRoleActivity.this.getClass().getSimpleName();
    String strLastPage = "FALSE";

    //WIDGETS
    ImageView imgBack, imgRight;
    LinearLayout llLeftLL;
    TextView txtCenter;
    EditText editSearchET;
    RelativeLayout imgRightLL;

    RecyclerView mRecyclerView;
    AdminRoleAdapter mAdminRoleAdapter;
//    ArrayList<AdminRoleModel> mAdminRoleArrayList = new ArrayList<AdminRoleModel>();
    ArrayList<AllUser> mAdminRoleArrayList = new ArrayList<AllUser>();

    SwipeRefreshLayout swipeToRefresh;
    NestedScrollView nestedScrollNS;
    ProgressBar progressBottomPB;
//    ArrayList<AdminRoleModel> mLoadMore = new ArrayList<AdminRoleModel>();
    ArrayList<AllUser> mLoadMore = new ArrayList<AllUser>();
    int page_no = 0;
    boolean isSwipeRefresh = false, isNormalSearch = false, isScroolling = false;
    ChangeRoleInterface mChangeRoleInterface;

    EnableDisableAdimRoleInterface mEnableDisableAdimRoleInterface = new EnableDisableAdimRoleInterface() {
        @Override
        public void getEnableDisableAdminRole(AllUser mAdminRoleModel) {
            showAlertDialogConfirmCancel(mActivity, mAdminRoleModel);
        }
    };

    DeleteUserInterface mDeleteUserInterface = new DeleteUserInterface() {
        @Override
        public void deleteUser(AllUser mAdminRoleModel) {
            deleteUserConfirmDialog(mActivity, mAdminRoleModel);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_admin_role);
        mChangeRoleInterface = this;
    }

    @Override
    protected void setViewsIDs() {
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRight = findViewById(R.id.imgRight);
        progressBottomPB = findViewById(R.id.progressBottomPB);
        llLeftLL = findViewById(R.id.llLeftLL);
        editSearchET = findViewById(R.id.editSearchET);
        txtCenter = findViewById(R.id.txtCenter);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        nestedScrollNS = findViewById(R.id.nestedScrollNS);
        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                isNormalSearch = false;
                mAdminRoleArrayList.clear();
                mLoadMore.clear();
                page_no = 1;
                executeAPI();
            }
        });

        /* set tool bar */
        txtCenter.setText(getString(R.string.admin_and_role));
        imgRight.setImageResource(R.drawable.ic_magnifying_glass);

        nestedScrollNS.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {

                    if (isNormalSearch == false) {
                        isScroolling = true;
                        ++page_no;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressBottomPB.setVisibility(View.VISIBLE);
                                if (strLastPage.equals("FALSE")) {
                                    executeAPI();
                                } else {
                                    progressBottomPB.setVisibility(View.GONE);
                                }
                            }
                        }, 500);
                    }
                }
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mActivity, SearchAdminAndRoleActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute Adimins And Roles API*//
            mAdminRoleArrayList.clear();
            mLoadMore.clear();
            page_no = 1;
            if (strLastPage.equals("FALSE")) {
                executeAPI();
            } else {
                progressBottomPB.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    executeNormalSearch();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void executeNormalSearch() {
        mAdminRoleArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<AllAdminBySearchModel> call1 = mApiInterface.getAllAdminRolesSearchRequest(editSearchET.getText().toString().trim());
        call1.enqueue(new Callback<AllAdminBySearchModel>() {
                          @Override
                          public void onResponse(Call<AllAdminBySearchModel> call, retrofit2.Response<AllAdminBySearchModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              editSearchET.setText("");
                              isNormalSearch = true;

                              AllAdminBySearchModel mModel = response.body();
                              if (mModel.getStatus().equals("1")) {
                                  mAdminRoleArrayList=mModel.getData();
                                  /*Set Adapter*/
                                  setAdapter();
                              } else {
                                  AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<AllAdminBySearchModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

//    public void executeNormalSearch() {
//        mAdminRoleArrayList.clear();
//        String strUrl = JaoharConstants.Get_All_Admin_And_Roles_By_Search + "?search=" + editSearchET.getText().toString().trim();
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                editSearchET.setText("");
//                isNormalSearch = true;
//                Log.e(TAG, "*****Response****" + response);
//                JSONObject mJsonObject = null;
//                try {
//                    mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
//                        for (int i = 0; i < mJsonArray.length(); i++) {
//                            JSONObject mJson = mJsonArray.getJSONObject(i);
//                            AdminRoleModel mModel = new AdminRoleModel();
//                            if (!mJson.isNull("id")) {
//                                mModel.setId(mJson.getString("id"));
//                            }
//                            if (!mJson.isNull("email")) {
//                                mModel.setEmail(mJson.getString("email"));
//                            }
//                            if (!mJson.isNull("online_status")) {
//                                mModel.setOnline_status(mJson.getString("online_status"));
//                            }
//                            if (!mJson.isNull("login_time")) {
//                                mModel.setLogin_time(mJson.getString("login_time"));
//                            }
//                            if (!mJson.isNull("login_device")) {
//                                mModel.setLogin_device(mJson.getString("login_device"));
//                            }
//                            if (!mJson.isNull("password")) {
//                                mModel.setPassword(mJson.getString("password"));
//                            }
//                            if (!mJson.isNull("role")) {
//                                mModel.setRole(mJson.getString("role"));
//                            }
//                            if (!mJson.isNull("status")) {
//                                mModel.setStatus(mJson.getString("status"));
//                            }
//                            if (!mJson.isNull("created")) {
//                                mModel.setCreated(mJson.getString("created"));
//                            }
//                            if (!mJson.isNull("enabled")) {
//                                mModel.setEnabled(mJson.getString("enabled"));
//                            }
//                            if (!mJson.isNull("company_name")) {
//                                mModel.setCompany_name(mJson.getString("company_name"));
//                            }
//                            if (!mJson.isNull("first_name")) {
//                                mModel.setFirst_name(mJson.getString("first_name"));
//                            }
//                            if (!mJson.isNull("last_name")) {
//                                mModel.setLast_name(mJson.getString("last_name"));
//                            }
//                            mAdminRoleArrayList.add(mModel);
//                        }
//                        setAdapter();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void setAdapter() {
        Log.e(TAG, "VesselsAdapter: " + mAdminRoleArrayList.size());
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdminRoleAdapter = new AdminRoleAdapter(mActivity, mAdminRoleArrayList, mEnableDisableAdimRoleInterface, mChangeRoleInterface, mDeleteUserInterface);
        mRecyclerView.setAdapter(mAdminRoleAdapter);
    }

    public void executeAPI() {
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
            AlertDialogManager.hideProgressDialog();
        }
        if (page_no == 1) {
            progressBottomPB.setVisibility(View.GONE);
            AlertDialogManager.showProgressDialog(mActivity);
        }
        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllAdminRoleModel> call1 = mApiInterface.getAllAdminRolesRequest(String.valueOf(page_no));
        call1.enqueue(new Callback<GetAllAdminRoleModel>() {
                          @Override
                          public void onResponse(Call<GetAllAdminRoleModel> call, retrofit2.Response<GetAllAdminRoleModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              progressBottomPB.setVisibility(View.GONE);
                              Log.e(TAG, "*****Response****" + response);
                              if (isSwipeRefresh) {
                                  isSwipeRefresh = false;
                                  swipeToRefresh.setRefreshing(false);
                              }
                              GetAllAdminRoleModel mModel = response.body();
                              assert mModel != null;
                              if (mModel.getStatus().equals("1")) {
                                  strLastPage = mModel.getData().getLastPage();

                                  if (page_no == 1) {
                                      mAdminRoleArrayList=mModel.getData().getAllUsers();
                                  } else if (page_no > 1) {
                                      mLoadMore=mModel.getData().getAllUsers();
                                  }

                              if (mLoadMore.size() > 0) {
                                  mAdminRoleArrayList.addAll(mLoadMore);
                              }
                              /*Set Adapter*/
                              setAdapter();
                          } else {
                                  AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetAllAdminRoleModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

//    public void executeAPI() {
//        String strUrl = JaoharConstants.GET_ALL_ADMIN_ROLE + "?page_no=" + page_no;
//        Log.e(TAG, "***URL***" + strUrl);
//        if (page_no > 1) {
//            progressBottomPB.setVisibility(View.VISIBLE);
//            AlertDialogManager.hideProgressDialog();
//        }
//        if (page_no == 1) {
//            progressBottomPB.setVisibility(View.GONE);
//            AlertDialogManager.showProgressDialog(mActivity);
//        }
//        if (!isSwipeRefresh) {
//            progressBottomPB.setVisibility(View.GONE);
//        }
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                progressBottomPB.setVisibility(View.GONE);
//                Log.e(TAG, "*****Response****" + response);
//                if (isSwipeRefresh) {
//                    isSwipeRefresh = false;
//                    swipeToRefresh.setRefreshing(false);
//                }
//                parseResponse(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void parseResponse(String response) {
//        try {
//            JSONObject mJsonObject = new JSONObject(response);
//            if (mJsonObject.getString("status").equals("1")) {
//                JSONObject mJsonObjectData = mJsonObject.getJSONObject("data");
//                strLastPage = mJsonObjectData.getString("last_page");
//                JSONArray mJsonArray = mJsonObjectData.getJSONArray("all_users");
//
//                for (int i = 0; i < mJsonArray.length(); i++) {
//                    JSONObject mJson = mJsonArray.getJSONObject(i);
//                    AdminRoleModel mModel = new AdminRoleModel();
//                    if (!mJson.isNull("id")) {
//                        mModel.setId(mJson.getString("id"));
//                    }
//                    if (!mJson.isNull("email")) {
//                        mModel.setEmail(mJson.getString("email"));
//                    }
//                    if (!mJson.isNull("online_status")) {
//                        mModel.setOnline_status(mJson.getString("online_status"));
//                    }
//                    if (!mJson.isNull("login_time")) {
//                        mModel.setLogin_time(mJson.getString("login_time"));
//                    }
//                    if (!mJson.isNull("login_device")) {
//                        mModel.setLogin_device(mJson.getString("login_device"));
//                    }
//                    if (!mJson.isNull("password")) {
//                        mModel.setPassword(mJson.getString("password"));
//                    }
//                    if (!mJson.isNull("role")) {
//                        mModel.setRole(mJson.getString("role"));
//                    }
//                    if (!mJson.isNull("status")) {
//                        mModel.setStatus(mJson.getString("status"));
//                    }
//                    if (!mJson.isNull("created")) {
//                        mModel.setCreated(mJson.getString("created"));
//                    }
//                    if (!mJson.isNull("enabled")) {
//                        mModel.setEnabled(mJson.getString("enabled"));
//                    }
//                    if (!mJson.isNull("company_name")) {
//                        mModel.setCompany_name(mJson.getString("company_name"));
//                    }
//                    if (!mJson.isNull("first_name")) {
//                        mModel.setFirst_name(mJson.getString("first_name"));
//                    }
//                    if (!mJson.isNull("last_name")) {
//                        mModel.setLast_name(mJson.getString("last_name"));
//                    }
//
//                    if (page_no == 1) {
//                        mAdminRoleArrayList.add(mModel)
//                    } else if (page_no > 1) {
//                        mLoadMore.add(mModel);
//                    }
//                }
//                if (mLoadMore.size() > 0) {
//                    mAdminRoleArrayList.addAll(mLoadMore);
//                }
//                /*Set Adapter*/
//                setAdapter();
//
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mJsonObject.getString("message"));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    /*Execute Enable/Disable API*/
    public void executeEnableDisableAPI(AllUser mAdminRoleModel) {
        if (mAdminRoleModel.getEnabled().equals("1")) {
            executeDisableAPI(mAdminRoleModel);
        } else if (mAdminRoleModel.getEnabled().equals("0")) {
            executeEnableAPI(mAdminRoleModel);
        }}


    private void executeEnableAPI(AllUser mAdminRoleModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.enableAccountRequest(mAdminRoleModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus()==0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void executeDisableAPI(AllUser mAdminRoleModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.disableAccountRequest(mAdminRoleModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus()==0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    /*Execute Enable/Disable API*/
//    public void executeEnableDisableAPI(AllUser mAdminRoleModel) {
//        String strUrl = "";
//        if (mAdminRoleModel.getEnabled().equals("1")) {
//            strUrl = JaoharConstants.DISABLE_ACCOUNT + "?user_id=" + mAdminRoleModel.getId();
//        } else if (mAdminRoleModel.getEnabled().equals("0")) {
//            strUrl = JaoharConstants.ENABLE_ACCOUNT + "?user_id=" + mAdminRoleModel.getId();
//        }
//        Log.e(TAG, "***URL***" + strUrl);
//
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);//{"status":"1","message":"Account enabled successfully"}
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else if (mJsonObject.getString("status").equals("0")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    public void showAlertDialogConfirmCancel(Activity mActivity, final AllUser mAdminRoleModel) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_confirm_cancel);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtConfirmTV = alertDialog.findViewById(R.id.txtConfirmTV);
        TextView txtCancelTV = alertDialog.findViewById(R.id.txtCancelTV);


        txtTitle.setText(getString(R.string.app_name));
        if (mAdminRoleModel.getEnabled().equals("1")) {
            txtMessage.setText(getString(R.string.are_you_sure_want_to_disable));
        } else if (mAdminRoleModel.getEnabled().equals("0")) {
            txtMessage.setText(getString(R.string.are_you_sure_want_to_enable));
        }

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txtConfirmTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                executeEnableDisableAPI(mAdminRoleModel);
            }
        });
        alertDialog.show();
    }

    public void showAlertDialogOKDONE(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mAdminRoleArrayList.clear();
                mLoadMore.clear();
                executeAPI();
            }
        });
        alertDialog.show();
    }

    public void showEditRoleDialog(Activity mActivity, final AllUser mAdminRoleModel) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_edit_role);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        ListView lstRoleLV = (ListView) alertDialog.findViewById(R.id.lstRoleLV);
        final String roleArray[] = mActivity.getResources().getStringArray(R.array.role_array_array);
        /*Set Adapter*/
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(mActivity, R.layout.item_role, R.id.itemTxtTV, roleArray);
        lstRoleLV.setAdapter(mAdapter);

        lstRoleLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                alertDialog.dismiss();
                executeUpdateRoleAPI(mAdminRoleModel, Utilities.getRoleWithFormat(roleArray[position]));
            }
        });
        alertDialog.show();
    }


    private void executeUpdateRoleAPI(AllUser mAdminRoleModel, String strNewRole) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.editRoleRequest(mAdminRoleModel.getId(),strNewRole);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus()==0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" +mModel.getMessage());
                }
                else{
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    /*Execute Enable/Disable API*/
//    public void executeUpdateRoleAPI(AllUser mAdminRoleModel, String strNewRole) {
//        String strUrl = "";
//        strUrl = JaoharConstants.EDIT_ROLE + "?user_id=" + mAdminRoleModel.getId() + "&new_role=" + strNewRole;
//        Log.e(TAG, "***URL***" + strUrl);
//
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);//{"status":"1","message":"Account enabled successfully"}
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else if (mJsonObject.getString("status").equals("0")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    /*Confirm Delete Dialog With API*/
    public void deleteUserConfirmDialog(Activity mActivity, final AllUser mAdminRoleModel) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_confirm_cancel);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtConfirmTV = (TextView) alertDialog.findViewById(R.id.txtConfirmTV);
        TextView txtCancelTV = (TextView) alertDialog.findViewById(R.id.txtCancelTV);

        txtTitle.setText(getString(R.string.app_name));
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_user));
        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txtConfirmTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                executeDeleteUserAPI(mAdminRoleModel);
            }
        });
        alertDialog.show();
    }


//    public void executeDeleteUserAPI(AllUser mAdminRoleModel) {
//        String strUrl = "";
//        strUrl = JaoharConstants.DELETE_USER + "?id=" + mAdminRoleModel.getId() + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&device_type=Android";
//        Log.e(TAG, "***URL***" + strUrl);
//
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);//{"status":"1","message":"Account enabled successfully"}
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else if (mJsonObject.getString("status").equals("0")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /*Execute DeleteUser API*/
    private void executeDeleteUserAPI(AllUser mAdminRoleModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteUserRequest(mAdminRoleModel.getId(),JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""),"Android");
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus()==0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" +mModel.getMessage());
                }
                else{
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());

                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


    @Override
    public void getChangedRole(AllUser mAdminRoleModel) {
        showEditRoleDialog(mActivity, mAdminRoleModel);
    }

    @Override
    public void getResstPassword(AllUser mAdminRoleModel) {
        resetPasswordConfirmDialog(mActivity, mAdminRoleModel);
    }

    /*Execute ResetPassword API*/
    private void executeResetPasswordAPI(AllUser mAdminRoleModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.resetPasswordRequest(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""),mAdminRoleModel.getEmail(),mAdminRoleModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus()==0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" +mModel.getMessage());
                }
                else{
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    /*Execute ResetPassword API*/
//    public void executeResetPasswordAPI(AllUser mAdminRoleModel) {
//        String strUrl = "";
//        strUrl = JaoharConstants.RESET_PASSWORD + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&email=" + mAdminRoleModel.getEmail() + "&reset_user_id=" + mAdminRoleModel.getId();
//        Log.e(TAG, "***URL***" + strUrl);
//
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);//{"status":"1","message":"Account enabled successfully"}
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else if (mJsonObject.getString("status").equals("0")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void resetPasswordConfirmDialog(Activity mActivity, final AllUser mAdminRoleModel) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_confirm_cancel);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtConfirmTV = (TextView) alertDialog.findViewById(R.id.txtConfirmTV);
        TextView txtCancelTV = (TextView) alertDialog.findViewById(R.id.txtCancelTV);

        txtTitle.setText(getString(R.string.app_name));
        txtMessage.setText(getString(R.string.are_you_sure_want_to_reset_password));
        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txtConfirmTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                executeResetPasswordAPI(mAdminRoleModel);
            }
        });
        alertDialog.show();
    }

}
