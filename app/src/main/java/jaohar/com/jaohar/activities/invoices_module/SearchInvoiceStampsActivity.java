package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.EditStampActivity;
import jaohar.com.jaohar.adapters.StampsAdapter;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.interfaces.DeleteStampInterface;
import jaohar.com.jaohar.interfaces.EditStampInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class SearchInvoiceStampsActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = SearchInvoiceStampsActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = SearchInvoiceStampsActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.cancelTV)
    TextView cancelTV;
    @BindView(R.id.invoicesRV)
    RecyclerView rvStampsRV;

    ArrayList<StampsModel> mArrayList = new ArrayList<StampsModel>();
    ArrayList<StampsModel> filteredList = new ArrayList<StampsModel>();
    StampsAdapter mStampsAdapter;

    DeleteStampInterface mDeleteStampInterface = new DeleteStampInterface() {
        @Override
        public void deleteInterface(StampsModel mStampsModel, int position) {
            deleteConfirmDialog(mStampsModel, position);
        }
    };

    EditStampInterface mEditStampInterface = new EditStampInterface() {
        @Override
        public void editInterface(StampsModel mStampsModel) {
            Intent mIntent = new Intent(mActivity, EditStampActivity.class);
            mIntent.putExtra("Model", mStampsModel);
            mActivity.startActivity(mIntent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_invoice_stamps);

        setStatusBar();

        getIntentData();

        ButterKnife.bind(this);
    }

    private void getIntentData() {
        mArrayList = (ArrayList<StampsModel>) getIntent().getSerializableExtra("QuestionListExtra");
    }

    protected void setClickListner() {
        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                charSequence = charSequence.toString().toLowerCase();
                filteredList.clear();
                for (int k = 0; k < mArrayList.size(); k++) {
                    final String text = mArrayList.get(k).getStamp_name().toLowerCase();
                    if (text.contains(charSequence)) {
                        filteredList.add(mArrayList.get(k));
                    }
                }
                rvStampsRV.setLayoutManager(new LinearLayoutManager(mActivity));
                mStampsAdapter = new StampsAdapter(mActivity, filteredList, mDeleteStampInterface, mEditStampInterface);
                rvStampsRV.setAdapter(mStampsAdapter);
                mStampsAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void executeDeleteAPI(StampsModel mStampsModel, int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteStampsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), mStampsModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    mArrayList.remove(position);
                    filteredList.remove(position);

                    if (mStampsAdapter != null)
                        mStampsAdapter.notifyDataSetChanged();

                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void deleteConfirmDialog(final StampsModel mStampsModel, int position) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_stamp));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mStampsModel, position);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }
}