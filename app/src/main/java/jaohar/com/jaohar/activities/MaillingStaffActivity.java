package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.MailingListStaffAdapter;
import jaohar.com.jaohar.interfaces.SendingMulipleLISTID;
import jaohar.com.jaohar.models.AllMailList;
import jaohar.com.jaohar.models.GetAllMailListsModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import jaohar.com.jaohar.views.CustomNestedScrollView;
import retrofit2.Call;
import retrofit2.Callback;

public class MaillingStaffActivity extends BaseActivity {
    Activity mActivity = MaillingStaffActivity.this;
    String TAG = MaillingStaffActivity.this.getClass().getSimpleName();
    TextView txtCenter, txtRight, txtMailTV;
    ImageView imgBack;
    LinearLayout llLeftLL;
    RecyclerView mailingListRV;
    SwipeRefreshLayout swipeToRefresh;
    CustomNestedScrollView parentSV;
    boolean isSwipeRefresh = false;
    int page_no = 1;
    public String strLastPage = "FALSE";
    boolean isLoadMore = false;
    MailingListStaffAdapter mAdapter;
    ArrayList<AllMailList> mArrayList = new ArrayList();
    ArrayList<AllMailList> loadMoreArrayList = new ArrayList();
    //    ArrayList<MailingListModel> mArrayList = new ArrayList();
//    ArrayList<MailingListModel> loadMoreArrayList = new ArrayList();
    ArrayList<String> mListID = new ArrayList();
    ProgressBar progressBottomPB;
    static int IsActive = 0;

    //    private ArrayList<MailingListModel> multiSelectArrayList = new ArrayList<MailingListModel>();
    private ArrayList<AllMailList> multiSelectArrayList = new ArrayList<AllMailList>();

    MailingListStaffAdapter.ClickManagerModulesInterface clickManagerModulesInterface = new MailingListStaffAdapter.ClickManagerModulesInterface() {
        @Override
        public void mClickManagerModulesInterface(int position, String strListId, String strListName) {
            PerformOptionsClick(position, strListId, strListName);
        }
    };

    private void PerformOptionsClick(final int position, final String strListID, final String strListNAME) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_mailing_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);

        editRL.setVisibility(View.GONE);
        deleteRL.setVisibility(View.GONE);

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                strLastPage = "FALSE";
                Intent mIntent = new Intent(mActivity, AddMailingListActivity.class);
                mIntent.putExtra("isAddClick", "false");
                mIntent.putExtra("listID", strListID);
                mIntent.putExtra("listNAME", strListNAME);
                startActivity(mIntent);
            }
        });

        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(mActivity, ShowMailingContactLISTActivity.class);
                mIntent.putExtra("listID", strListID);
                mActivity.startActivity(mIntent);
            }
        });

        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                /*Send Email*/
                Intent mIntent = new Intent(mActivity, SendMailingListActivity.class);
//                mIntent.putExtra("listIDArray", getMultiDetailsData());
                mIntent.putExtra("listID", strListID);
                mIntent.putExtra("type", "staff");
                mActivity.startActivity(mIntent);
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    SendingMulipleLISTID mMultimaIlInterface = new SendingMulipleLISTID() {
        @Override
        public void mSendingMultipleLIST(AllMailList mMailingLIST, boolean mDelete) {
            if (mMailingLIST != null) {

                if (!mDelete) {
                    IsActive = IsActive + 1;
                    multiSelectArrayList.add(mMailingLIST);
                    String strID = mMailingLIST.getListId();
                    mListID.add(mMailingLIST.getListId());
                } else {
                    IsActive = IsActive - 1;
                    multiSelectArrayList.remove(mMailingLIST);
                    String strID = mMailingLIST.getListId();
                    mListID.remove(mMailingLIST.getListId());
                }


                if (IsActive > 0) {
                    txtMailTV.setVisibility(View.VISIBLE);
                } else {
                    txtMailTV.setVisibility(View.GONE);
                }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_mailling_staff);
    }

    @Override
    protected void setViewsIDs() {
        progressBottomPB = findViewById(R.id.progressBottomPB);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        mailingListRV = (RecyclerView) findViewById(R.id.mailingListRV);
        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtRight = (TextView) findViewById(R.id.txtRight);
        txtMailTV = (TextView) findViewById(R.id.txtMailTV);
        txtRight.setVisibility(View.GONE);
        txtCenter.setText(getString(R.string.mailing_list));
        imgBack = (ImageView) findViewById(R.id.imgBack);

        imgBack.setImageResource(R.drawable.back);
        parentSV = (CustomNestedScrollView) findViewById(R.id.parentSV);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                page_no = 1;
                mArrayList.clear();
                loadMoreArrayList.clear();
                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    executeAPI(page_no);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            page_no = 1;
            mArrayList.clear();
            loadMoreArrayList.clear();
            executeAPI(page_no);
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransitionExit();
            }
        });

        parentSV.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    progressBottomPB.setVisibility(View.VISIBLE);
                    isLoadMore = true;
                    ++page_no;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (strLastPage.equals("FALSE")) {
                                executeAPI(page_no);
                            } else {
                                progressBottomPB.setVisibility(View.GONE);
                            }
                        }
                    }, 1000);
                }
            }
        });

        txtMailTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Send Email*/
                Intent mIntent = new Intent(mActivity, SendMailingListActivity.class);
                mIntent.putExtra("listIDArray", getMultiDetailsData());
                mIntent.putExtra("type", "staff");
                mActivity.startActivity(mIntent);

            }
        });
    }

    private ArrayList<String> getMultiDetailsData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();

        for (int i = 0; i < mListID.size(); i++) {
            strData.add(mListID.get(i));
        }
        mListID.clear();
        return strData;
    }

    public void executeAPI(int page_no) {
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
            AlertDialogManager.hideProgressDialog();
        }
        if (page_no == 1) {
            progressBottomPB.setVisibility(View.GONE);
            AlertDialogManager.showProgressDialog(mActivity);
//          Utilities.showProgressDialog(getActivity());
        }

        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllMailListsModel> call1 = mApiInterface.getAllMailsListRequest(String.valueOf(page_no), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<GetAllMailListsModel>() {
            @Override
            public void onResponse(Call<GetAllMailListsModel> call, retrofit2.Response<GetAllMailListsModel> response) {
                AlertDialogManager.hideProgressDialog();
                GetAllMailListsModel mModel = response.body();
                progressBottomPB.setVisibility(View.GONE);

                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
                if (mModel.getStatus().equals("1")) {
                    strLastPage = mModel.getData().getLastPage();
                    if (page_no == 1) {
                        mArrayList = mModel.getData().getAllMailLists();
                    } else if (page_no > 1) {
                        loadMoreArrayList = mModel.getData().getAllMailLists();
                    }

                    if (loadMoreArrayList.size() > 0) {
                        mArrayList.addAll(loadMoreArrayList);
                    }
                    setAdapter();
                } else {

                }
            }

            @Override
            public void onFailure(Call<GetAllMailListsModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        mailingListRV.setNestedScrollingEnabled(false);
        mailingListRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new MailingListStaffAdapter(mActivity, mArrayList, mMultimaIlInterface, clickManagerModulesInterface);
        mailingListRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent mIntent = new Intent(mActivity, HomeActivity.class);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
//        mActivity.startActivity(mIntent);
//        mActivity.finish();
    }
}
