package jaohar.com.jaohar.activities.mail_list_all_activities;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.SendingMailToList_VesselAdapter1;
import jaohar.com.jaohar.activities.webViewActivity;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.SendingMailToList_VesselAdapter;
import jaohar.com.jaohar.beans.MailingListModel;
import jaohar.com.jaohar.interfaces.SelectingListNameInterface;
import jaohar.com.jaohar.models.AllMailList;
import jaohar.com.jaohar.models.GetAllMailListsModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

import static jaohar.com.jaohar.adapters.VesselsAdapter.Items;

public class SendingMailToLIst_Vessels extends BaseActivity {
    Activity mActivity = SendingMailToLIst_Vessels.this;
    String TAG = SendingMailToLIst_Vessels.this.getClass().getSimpleName();
    //    ArrayList<MailingListModel> mArrayList = new ArrayList();
    ArrayList<AllMailList> mArrayList = new ArrayList();
    ArrayList<String> SelectedArraYLIST = new ArrayList();
    ImageView clossRL;
    TextView mItemNameTV;
    RelativeLayout chooseListRL;
    String strRecordID, strSubject, strListID;
    LinearLayout listShowLL;
    EditText editSubjectET;
    RecyclerView mRecyclerViewShowLIST;
    Button btnCancel, btnSend;
    SendingMailToList_VesselAdapter mShowListAdapter;
    SelectingListNameInterface mSeletingListName = new SelectingListNameInterface() {
        @Override
        public void mSelectingListnameInterface(AllMailList mModel) {
            listShowLL.setVisibility(View.GONE);

            strListID = mModel.getListId();
            mItemNameTV.setText(mModel.getListName());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sending_mail_to_list__vessels);

        if (getIntent() != null) {
            if (getIntent().getStringExtra("recordID") != null) {
                strRecordID = getIntent().getStringExtra("recordID");
                strSubject = getIntent().getStringExtra("vessalName");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        executeAPI();
    }

    @Override
    protected void setViewsIDs() {
        clossRL = findViewById(R.id.clossRL);
        chooseListRL = findViewById(R.id.chooseListRL);
        btnSend = findViewById(R.id.btnSend);
        btnCancel = findViewById(R.id.btnCancel);
        mItemNameTV = findViewById(R.id.mItemNameTV);
        editSubjectET = findViewById(R.id.editSubjectET);
        listShowLL = findViewById(R.id.listShowLL);
        mRecyclerViewShowLIST = findViewById(R.id.mRecyclerViewShowLIST);
        editSubjectET.setText(strSubject);

        initializeUI1();
    }

    @Override
    protected void setClickListner() {
        clossRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        chooseListRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listShowLL.setVisibility(View.VISIBLE);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemNameTV.getText().toString().equals("Choose list")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_fill_all_the_details));
                } else if (editSubjectET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_fill_all_the_details));
                } else {
//                    executeAPISendingEmail();
                    openMailDialog();
                    // Sending EMAIL API
                }
            }
        });
        mItemNameTV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                listShowLL.setVisibility(View.VISIBLE);
                return false;
            }
        });
        mItemNameTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listShowLL.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    //    https://root.jaohar.com/Staging/JaoharWebServicesNew/GetMailLists.php

    private Map<String, String> mParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        params.put("vessel_id", strRecordID);
        params.put("list_id", strListID);
        params.put("subject", editSubjectET.getText().toString());
        Log.e("**PARAMS**", params.toString());
        return params;
    }

    public void executeAPISendingEmail() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.mailVesseltoList(mParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    String strBaseURL = "";
                    String SessionID = mModel.getData();
                    if (JaoharConstants.SERVER_URL.contains("Staging")) {
                        strBaseURL = "https://staging.jaohar.com/index.php/MailVesselDetails/vessel_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
                    }
                    if (JaoharConstants.SERVER_URL.contains("Development")) {
                        strBaseURL = "https://dev.jaohar.com/index.php/MailVesselDetails/vessel_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
                    } else {
                        strBaseURL = "https://office.jaohar.com/index.php/MailVesselDetails/vessel_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
                    }
                    // tell everybody you have succed upload image and post strings

                    Log.e(TAG, "MesssageURL******:" + "https://office.jaohar.com/index.php/MailVesselDetails/vessel_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID);
                    Intent intent = new Intent(mActivity, webViewActivity.class);
                    intent.putExtra("news", strBaseURL);
//                        intent.putExtra("news","https://office.jaohar.com/index.php/MailVesselDetails/vessel_mail_api/"+JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "")+"/"+SessionID);
                    startActivity(intent);
                    finish();

                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

//    private void executeAPISendingEmail() {
////      https://root.jaohar.com/Staging/JaoharWebServicesNew/MailVesselToList.php
//        JaoharConstants.mArrayLISTDATA.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
//        JSONObject jsonObject = new JSONObject();
//        String strAPIUrl = JaoharConstants.MailVesselToList;
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//            jsonObject.put("vessel_id", strRecordID);
//            jsonObject.put("list_id", strListID);
//            jsonObject.put("subject", editSubjectET.getText().toString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******responseMAILDATA*****" + jsonObject.toString());
//                //to clear selected vessels
//                Items.clear();
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        String strBaseURL = "";
//                        Log.e(TAG, "Messsage******:" + message);
//                        String SessionID = jsonObject.getString("data");
//                        if (JaoharConstants.SERVER_URL.contains("Staging")) {
//                            strBaseURL = "https://staging.jaohar.com/index.php/MailVesselDetails/vessel_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
//                        } else {
//                            strBaseURL = "https://office.jaohar.com/index.php/MailVesselDetails/vessel_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID;
//                        }
//                        // tell everybody you have succed upload image and post strings
//
//                        Log.e(TAG, "MesssageURL******:" + "https://office.jaohar.com/index.php/MailVesselDetails/vessel_mail_api/" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "/" + SessionID);
//                        Intent intent = new Intent(mActivity, webViewActivity.class);
//                        intent.putExtra("news", strBaseURL);
////                        intent.putExtra("news","https://office.jaohar.com/index.php/MailVesselDetails/vessel_mail_api/"+JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "")+"/"+SessionID);
//                        startActivity(intent);
//                        finish();
//
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + error.toString());
//
//            }
//        }) {
//            /**
//             * Passing some request headers
//             **/
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private Map<String, String> mShellParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        params.put("vessel_id", strRecordID);
        params.put("list_id", strListID);
        params.put("subject", editSubjectET.getText().toString());
        Log.e("**PARAMS**", params.toString());
        return params;
    }

    public void executeAPISendingEmailShell() {
        JaoharConstants.mArrayLISTDATA.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.mailVesselToListShell(mShellParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Items.clear();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    String strBaseURL = "";
                    AlertDialogManager.showFinishAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

//    private void executeAPISendingEmailShell() {
////      https://root.jaohar.com/Staging/JaoharWebServicesNew/MailVesselToList.php
//        JaoharConstants.mArrayLISTDATA.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
//        JSONObject jsonObject = new JSONObject();
//        String strAPIUrl = JaoharConstants.MailVesselToListShell;
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//            jsonObject.put("vessel_id", strRecordID);
//            jsonObject.put("list_id", strListID);
//            jsonObject.put("subject", editSubjectET.getText().toString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******responseMAILDATA*****" + jsonObject.toString());
//                //to clear selected vessels
//                Items.clear();
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        String strBaseURL = "";
//                        finish();
//                        Log.e(TAG, "Messsage******:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + error.toString());
//
//            }
//        }) {
//            /**
//             * Passing some request headers
//             **/
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void executeAPI() {
        mArrayList.clear();
        SelectedArraYLIST.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllMailListsModel> call1 = mApiInterface.getMailLists(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<GetAllMailListsModel>() {
            @Override
            public void onResponse(Call<GetAllMailListsModel> call, retrofit2.Response<GetAllMailListsModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLADDResponce***" + response.body());
                GetAllMailListsModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    for (int i = 0; i < mModel.getData().getAllMailLists().size(); i++) {
                        SelectedArraYLIST.add(mModel.getData().getAllMailLists().get(i).getListName());
                    }
                    mArrayList = mModel.getData().getAllMailLists();
                    initializeUI();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetAllMailListsModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

//    public void executeAPI() {
//        mArrayList.clear();
//        SelectedArraYLIST.clear();
//        String strUrl = JaoharConstants.GetMailLists + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
//
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonData = new JSONObject(response);
//                    if (mJsonData.getString("status").equals("1")) {
//                        parseResponce(response);
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void parseResponce(String response) {
//        try {
//            JSONObject mDataObj = new JSONObject(response);
//            JSONObject mDataObject = mDataObj.getJSONObject("data");
//            JSONArray mJsonArray = mDataObject.getJSONArray("all_mail_lists");
//            for (int i = 0; i < mJsonArray.length(); i++) {
//                JSONObject mJsonObject1 = mJsonArray.getJSONObject(i);
//                MailingListModel mMailingModel = new MailingListModel();
//                if (!mJsonObject1.getString("list_id").equals("")) {
//                    mMailingModel.setList_id(mJsonObject1.getString("list_id"));
//                }
//                if (!mJsonObject1.getString("list_name").equals("")) {
//                    mMailingModel.setList_name(mJsonObject1.getString("list_name"));
//                    SelectedArraYLIST.add(mJsonObject1.getString("list_name"));
//                }
//                if (!mJsonObject1.getString("added_by").equals("")) {
//                    mMailingModel.setAdded_by(mJsonObject1.getString("added_by"));
//                }
//                if (!mJsonObject1.getString("modification_date").equals("")) {
//                    mMailingModel.setModification_date(mJsonObject1.getString("modification_date"));
//                }
//
//                mArrayList.add(mMailingModel);
//
//                initializeUI();
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    private void initializeUI() {
        mRecyclerViewShowLIST.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewShowLIST.setNestedScrollingEnabled(false);
        mRecyclerViewShowLIST.setHasFixedSize(false);
        mRecyclerViewShowLIST.setLayoutManager(layoutManager);
        mShowListAdapter = new SendingMailToList_VesselAdapter(mActivity, mArrayList, mSeletingListName);
        mRecyclerViewShowLIST.setAdapter(mShowListAdapter);


    }

    private void initializeUI1() {
        mRecyclerViewShowLIST.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewShowLIST.setNestedScrollingEnabled(false);
        mRecyclerViewShowLIST.setHasFixedSize(false);
        mRecyclerViewShowLIST.setLayoutManager(layoutManager);
        SendingMailToList_VesselAdapter1 mShowListAdapter = new SendingMailToList_VesselAdapter1(mActivity, mArrayList, mSeletingListName);
        mRecyclerViewShowLIST.setAdapter(mShowListAdapter);


    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }

    private void openMailDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.mail_to_list_dialog);

        TextView txt_mail_background = (TextView) dialog.findViewById(R.id.txt_mail_background);
        TextView txt_mail_web = (TextView) dialog.findViewById(R.id.txt_mail_web);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);

        txt_mail_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                executeAPISendingEmailShell();
            }
        });
        txt_mail_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                executeAPISendingEmail();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Log.e("error is occured", dialog.toString());
    }
}

