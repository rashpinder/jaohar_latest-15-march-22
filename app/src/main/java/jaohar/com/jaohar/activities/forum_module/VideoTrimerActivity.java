package jaohar.com.jaohar.activities.forum_module;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import jaohar.com.jaohar.R;
import life.knowledge4.videotrimmer.K4LVideoTrimmer;
import life.knowledge4.videotrimmer.interfaces.OnTrimVideoListener;

public class VideoTrimerActivity extends AppCompatActivity {
    K4LVideoTrimmer timeLine;
    Button butonSave;
    /**
     * set Activity
     **/
    Activity mActivity = VideoTrimerActivity.this;
    /**
     * set Activity TAG
     **/
    String TAG = VideoTrimerActivity.this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_video_trimer);
        // setUp Views
        setUpViews();
    }

    private void setUpViews() {
        timeLine = ((K4LVideoTrimmer) findViewById(R.id.timeLine));
        timeLine.setMaxDuration(30);

        /*Retrive Data from Privious Activity*/
        if (getIntent() != null) {
            if (getIntent().getStringExtra("filepath") != null) {
                String strFilePath = getIntent().getStringExtra("filepath");
                if (timeLine != null) {
                    timeLine.setVideoURI(Uri.parse(strFilePath));
                }
            }
        }

        timeLine.setOnTrimVideoListener(new OnTrimVideoListener() {
            @Override
            public void getResult(Uri uri) {
                String strURI = String.valueOf(uri);
                Log.e("test", "URITEST====" + strURI);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("filepath1", uri.toString());
                setResult(201, returnIntent);
                finish();
            }


            @Override
            public void cancelAction() {
                finish();
            }
        });

    }


}
