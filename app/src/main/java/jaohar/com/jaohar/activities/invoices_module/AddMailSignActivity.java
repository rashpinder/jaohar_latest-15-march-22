package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.fonts.ButtonPoppinsMedium;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;

public class AddMailSignActivity extends BaseActivity {
    private final Activity mActivity = AddMailSignActivity.this;
    /**
     * Widgets
     */
    @BindView(R.id.imgMailImageIV)
    ImageView imgMailImageIV;
    @BindView(R.id.editCompanyNameET)
    EditText editCompanyNameET;
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPhoneET)
    EditText editPhoneET;
    @BindView(R.id.editFaxET)
    EditText editFaxET;
    @BindView(R.id.editWebsiteET)
    EditText editWebsiteET;
    @BindView(R.id.editMailTextET)
    EditText editMailTextET;
    @BindView(R.id.btnAddMailSign)
    ButtonPoppinsMedium btnAddMailSign;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;

    private String strContactID = "", strUSERID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mail_sign);
        ButterKnife.bind(this);

        if (JaoharConstants.is_Staff_FragmentClick == true) {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
        }
    }



    @OnClick({R.id.llLeftLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
