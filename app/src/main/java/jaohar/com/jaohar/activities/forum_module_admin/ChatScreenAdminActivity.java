package jaohar.com.jaohar.activities.forum_module_admin;

import static android.provider.MediaStore.ACTION_VIDEO_CAPTURE;
import static android.provider.MediaStore.EXTRA_VIDEO_QUALITY;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.forum_module.ForumListActivity;
import jaohar.com.jaohar.activities.forum_module.VideoTrimerActivity;
import jaohar.com.jaohar.adapters.AddImageListAdapter;
import jaohar.com.jaohar.adapters.forum_module.ForumChatAdapter1;
import jaohar.com.jaohar.adapters.forum_module_admin_adapter.ForumChatAdminAdapter1;
import jaohar.com.jaohar.beans.AddGalleryImagesModel;
import jaohar.com.jaohar.beans.ForumModule.ForumChatModel;
import jaohar.com.jaohar.beans.ForumModule.ReplyForModelForum;
import jaohar.com.jaohar.beans.ForumModule.UserDetailForumChatModel;
import jaohar.com.jaohar.interfaces.DeleteImagesVessels;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.forumModule.Add_Del_Edit_forumchatInterface;
import jaohar.com.jaohar.interfaces.forum_module_admin.Add_Del_Edit_forumchatInterfaceAdmin;
import jaohar.com.jaohar.models.forummodels.AddStaffForumMessageModel;
import jaohar.com.jaohar.models.forummodels.AllMessagesItem;
import jaohar.com.jaohar.models.forummodels.AppendData;
import jaohar.com.jaohar.models.forummodels.DeleteStaffForumMessageModel;
import jaohar.com.jaohar.models.forummodels.ForumDetail;
import jaohar.com.jaohar.models.forummodels.GetAllForumMessagesModel;
import jaohar.com.jaohar.models.forummodels.ReplyFor;
import jaohar.com.jaohar.models.forummodels.UpdateForumHeadingModel;
import jaohar.com.jaohar.models.forummodels.UserDetail;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.FileUtil;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.RealPathUtil;
import jaohar.com.jaohar.utils.Utilities;
import jaohar.com.jaohar.volley.VolleyMultipartRequest;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatScreenAdminActivity extends BaseActivity {
    /**
     * set Activity
     **/
    Activity mActivity = ChatScreenAdminActivity.this;
    /**
     * set Activity TAG
     **/
    String TAG = ChatScreenAdminActivity.this.getClass().getSimpleName();
    /**
     * Widgets AND Adapters,Strings, Array List,Boolean and Integers
     **/
    @BindView(R.id.imageRplyRL)
    RelativeLayout imageRplyRL;
    @BindView(R.id.messagePicReplyIV)
    ImageView messagePicReplyIV;
    @BindView(R.id.replyBlurIV)
    ImageView replyBlurIV;
    @BindView(R.id.blurPicRL)
    RelativeLayout blurPicRL;
    @BindView(R.id.textCountPicTv)
    TextView textCountPicTv;

    String strforumID = "", strLastPage = "FALSE", strPushType = "";
    ArrayList<AddGalleryImagesModel> mGalleryArrayList = new ArrayList<AddGalleryImagesModel>();
    ForumChatAdapter1 mAdapter;
    int page_num = 0, currentMSGpositon = 0;
    RecyclerView dataRV;
    EditText textED;
    ProgressBar progressBottomPB, sendProgressBar;
    TextView textvesselName, textStatus, recordIDheadingTV, recordIDTV, headingTV;
    LinearLayout llLeftLL, llRightLL, recordIDLL, headingIDLL, galaryLL, imageLL;
    CircleImageView imagePic;
    ImageView sendIV, closeImageIV, addImgIV, addNote;
    RelativeLayout messageReplyRL, crossRL;
    AddImageListAdapter mImageAdapter;
    SwipeRefreshLayout swipeTorefefresh;
    String strPhoto1 = "", strPhoto2 = "", strPhoto3 = "", strPhoto4 = "", strPhoto5 = "", strPhoto6 = "",
            strPhoto7 = "", strPhoto8 = "", strPhoto9 = "", strPhoto10 = "", strDocumentNameforSend = "", strDoc = "",
            strDocname1 = "", strDocname2 = "", strDocname3 = "", strDocname4 = "", strDocname5 = "";
    String strHeadingType = "", strReplyID = "";
    Bitmap bitmap1;
    Bitmap bitmap2;
    Bitmap bitmap3;
    Bitmap bitmap4;
    Bitmap bitmap5;
    TextView editMessageText;
    Uri fileUri;
    byte[] byteArray1;
    byte[] byteArray2;
    byte[] byteArray3;
    byte[] byteArray4;
    byte[] byteArray5;
    boolean isSwipeRefresh = false;

    ArrayList<NormalFile> list = new ArrayList<>();
    public static String[] thumbColumns = {MediaStore.Video.Thumbnails.DATA};
    public static String[] mediaColumns = {MediaStore.Video.Media._ID};
    ArrayList<String> strData;

    /*
     * Permissions fo camera and gallary
     * */
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;

    // Permissions
    private final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private final String CAMERA = Manifest.permission.CAMERA;
    private final String recorfAudioStr = Manifest.permission.RECORD_AUDIO;
    String mCurrentPhotoPath = "", mStoragePath = "", strMessageID = "", strRoomID = "";
    RecyclerView galleryRecyclerView;
    static boolean isEditMessage = false, isReplyMessage = false, isAddVideo = false;

    private List<AllMessagesItem> mAllMessagesItem = new ArrayList<AllMessagesItem>();
    private List<AllMessagesItem> mGetAllLoadMore = new ArrayList<>();

    /*Timer*/
    boolean isAddClick = false;
    private Socket mSocket;
    private Boolean isConnected = true;
    private boolean mTyping = false;
    private final Handler mTypingHandler = new Handler();

    /* *
     * Interface to delete images From  Array list
     * */
    DeleteImagesVessels mDeleteImagesInterface = new DeleteImagesVessels() {
        @Override
        public void deleteImagesVessels(AddGalleryImagesModel mGalleryModel, int position) {
            if (mGalleryArrayList != null) {
                mGalleryArrayList.remove(position);
                setUpGalleryAdapter();
            }
        }
    };

    OnClickInterface onClickInterface = new OnClickInterface() {
        @Override
        public void mOnClickInterface(int position) {
            currentMSGpositon = position;
        }
    };

    /* *
     * Interface to Edit , Reply, Delete Forum Message
     * */
    Add_Del_Edit_forumchatInterface mAdd_Edit_DelInterface = new Add_Del_Edit_forumchatInterface() {
        @Override
        public void mAdd_Del_ChatInterface(final AllMessagesItem mModel, View view, String strType, int position) {
            strMessageID = mModel.getMessageId();
            currentMSGpositon = position;
            if (strType.equals("sender")) {
                PopupMenu popup = new PopupMenu(mActivity, view);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.reply_btn:
                                strReplyID = mModel.getMessageId();
                                isReplyMessage = true;
                                isEditMessage = false;
                                mGalleryArrayList.clear();
                                setUpGalleryAdapter();
                                galaryLL.setVisibility(View.VISIBLE);
                                messageReplyRL.setVisibility(View.VISIBLE);
                                imageLL.setVisibility(View.GONE);

                                if (mModel.getMessage() != null && !mModel.getMessage().equals("")) {
                                    editMessageText.setText(html2text(mModel.getMessage()));
                                } else {
                                    editMessageText.setText("");
                                }

                                /* to show and hide images for reply case */
                                if (mModel.getImageCount() != null && !mModel.getImageCount().equals("")
                                        && !mModel.getImageCount().equals("0")) {
                                    imageRplyRL.setVisibility(VISIBLE);

                                    if (mModel.getImageCount().equals("1")) {
                                        blurPicRL.setVisibility(GONE);
                                        messagePicReplyIV.setVisibility(VISIBLE);

                                        if (!Utilities.isDocAdded(mModel.getImage())) {
                                            if (Utilities.isVideoAdded(mModel.getImage())) {
                                                messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.video_icon));
                                            } else {
                                                Glide.with(mActivity).load(mModel.getImage()).into(messagePicReplyIV);
                                            }
                                        } else {
                                            messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.icon_doc));
                                        }

                                    } else {
                                        blurPicRL.setVisibility(VISIBLE);
                                        messagePicReplyIV.setVisibility(VISIBLE);

                                        if (!Utilities.isDocAdded(mModel.getImage())) {
                                            if (Utilities.isVideoAdded(mModel.getImage())) {
                                                messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.video_icon));
                                            } else {
                                                Glide.with(mActivity).load(mModel.getImage()).into(messagePicReplyIV);
                                            }
                                        } else {
                                            messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.icon_doc));
                                        }

                                        int imageCount = Integer.parseInt(mModel.getImageCount()) - 1;
                                        String strImageCount = "+" + imageCount;
                                        textCountPicTv.setText(strImageCount);
                                    }
                                } else {
                                    imageRplyRL.setVisibility(GONE);
                                }

                                break;
                            case R.id.edit_btn:
                                isEditMessage = true;
                                isReplyMessage = false;
                                mGalleryArrayList.clear();
                                messageReplyRL.setVisibility(GONE);
                                galaryLL.setVisibility(GONE);
                                imageLL.setVisibility(View.VISIBLE);

                                if (mModel.getMessage() != null && !mModel.getMessage().equals("")) {
                                    editMessageText.setText(html2text(mModel.getMessage()));
                                    textED.setText(html2text(mModel.getMessage()));
                                    textED.setSelection(textED.getText().length());
                                } else {
                                    editMessageText.setText("");
                                }
                                /* *
                                 * Set up Gallary Adapter if image  is Exists
                                 * */
                                if (!mModel.getImageCount().equals("0")) {
                                    performGallaryAdapterSetup(mModel);
                                } else {
                                    mGalleryArrayList.clear();
//                                    setUpGalleryAdapter();
                                }

                                break;

                            case R.id.del_btn:
                                deleteConfirmDialog(getResources().getString(R.string.are_you_sure_want_to_delete_message_));
                                break;
                        }
                        return false;
                    }
                });

                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.option_chat_left_menu, popup.getMenu());
                popup.show();

            } else {
                PopupMenu popup = new PopupMenu(mActivity, view);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.reply_btn:
                                strReplyID = mModel.getMessageId();
                                isReplyMessage = true;
                                isEditMessage = false;
                                mGalleryArrayList.clear();
                                setUpGalleryAdapter();
                                galaryLL.setVisibility(View.VISIBLE);
                                messageReplyRL.setVisibility(View.VISIBLE);
                                imageLL.setVisibility(View.GONE);

                                if (mModel.getMessage() != null && !mModel.getMessage().equals("")) {
                                    editMessageText.setText(html2text(mModel.getMessage()));
                                } else {
                                    editMessageText.setText("");
                                }

                                /* to show and hide images for reply case */
                                if (mModel.getImageCount() != null && !mModel.getImageCount().equals("")
                                        && !mModel.getImageCount().equals("0")) {
                                    imageRplyRL.setVisibility(VISIBLE);

                                    if (mModel.getImageCount().equals("1")) {
                                        blurPicRL.setVisibility(GONE);
                                        messagePicReplyIV.setVisibility(VISIBLE);

                                        if (!Utilities.isDocAdded(mModel.getImage())) {
                                            if (Utilities.isVideoAdded(mModel.getImage())) {
                                                messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.video_icon));
                                            } else {
                                                Glide.with(mActivity).load(mModel.getImage()).into(messagePicReplyIV);
                                            }
                                        } else {
                                            messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.icon_doc));
                                        }

                                    } else {
                                        blurPicRL.setVisibility(VISIBLE);
                                        messagePicReplyIV.setVisibility(VISIBLE);

                                        if (!Utilities.isDocAdded(mModel.getImage())) {
                                            if (Utilities.isVideoAdded(mModel.getImage())) {
                                                messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.video_icon));
                                            } else {
                                                Glide.with(mActivity).load(mModel.getImage()).into(messagePicReplyIV);
                                            }
                                        } else {
                                            messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.icon_doc));
                                        }

                                        int imageCount = Integer.parseInt(mModel.getImageCount()) - 1;
                                        String strImageCount = "+" + imageCount;
                                        textCountPicTv.setText(strImageCount);
                                    }
                                } else {
                                    imageRplyRL.setVisibility(GONE);
                                }

                                break;
                        }
                        return false;
                    }
                });

                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.option_menu_right_chat, popup.getMenu());
                popup.show();
            }
        }
    };

    /*
     * SetUp Gallary Adapter for editing the message
     * */
    private void performGallaryAdapterSetup(AllMessagesItem mModel) {
        mGalleryArrayList.clear();
        setUpGalleryAdapter();
        if (!mModel.getImage().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();
            if (!Utilities.isDocAdded(mModel.getImage())) {
                if (Utilities.isVideoAdded(mModel.getImage())) {
                    model.setStrImagePath(mModel.getImage());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getImage());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getImage());
                model.setStrType("doc");
            }

            mGalleryArrayList.add(model);
        }
        if (!mModel.getImage2().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getImage2())) {
                if (Utilities.isVideoAdded(mModel.getImage2())) {
                    model.setStrImagePath(mModel.getImage2());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getImage2());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getImage2());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getImage3().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();
            if (!Utilities.isDocAdded(mModel.getImage3())) {
                if (Utilities.isVideoAdded(mModel.getImage3())) {
                    model.setStrImagePath(mModel.getImage3());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getImage3());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getImage3());
                model.setStrType("doc");
            }

            mGalleryArrayList.add(model);
        }
        if (!mModel.getImage4().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getImage4())) {
                if (Utilities.isVideoAdded(mModel.getImage4())) {
                    model.setStrImagePath(mModel.getImage4());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getImage4());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getImage4());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getImage5().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getImage5())) {
                if (Utilities.isVideoAdded(mModel.getImage5())) {
                    model.setStrImagePath(mModel.getImage5());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getImage5());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getImage5());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getImage6().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getImage6())) {
                if (Utilities.isVideoAdded(mModel.getImage6())) {
                    model.setStrImagePath(mModel.getImage6());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getImage6());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getImage6());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getImage7().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getImage7())) {
                if (Utilities.isVideoAdded(mModel.getImage7())) {
                    model.setStrImagePath(mModel.getImage7());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getImage7());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getImage7());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getImage8().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getImage8())) {
                if (Utilities.isVideoAdded(mModel.getImage8())) {
                    model.setStrImagePath(mModel.getImage8());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getImage8());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getImage8());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getImage9().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getImage9())) {
                if (Utilities.isVideoAdded(mModel.getImage9())) {
                    model.setStrImagePath(mModel.getImage9());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getImage9());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getImage9());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getImage10().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getImage10())) {
                if (Utilities.isVideoAdded(mModel.getImage10())) {
                    model.setStrImagePath(mModel.getImage10());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getImage10());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getImage10());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }

        /* Set Gallery Images in recycler View */
        setUpGalleryAdapter();
    }

    /**
     * Activity Default Method
     **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_screen_forum);
        ButterKnife.bind(this);

        /* to handle crash */
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        /* Retrieve Data from Previous Activity */
        if (getIntent() != null) {
            if (getIntent().getStringExtra(JaoharConstants.NOTIFICATION_TYPE) != null) {
                strPushType = getIntent().getStringExtra(JaoharConstants.NOTIFICATION_TYPE);
            }
            if (getIntent().getStringExtra("forum_id") != null) {
                strforumID = "";
                strforumID = getIntent().getStringExtra("forum_id");
            }
            if (getIntent().getStringExtra("room_id") != null) {
                strRoomID = getIntent().getStringExtra("room_id");
            }
        }

        /*
         * Implement Socket
         **/
        if (!Utilities.isNetworkAvailable(Objects.requireNonNull(mActivity))) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            JaoharApplication app = (JaoharApplication) getApplication();
            mSocket = app.getSocket();
            mSocket.emit("ConncetedChat", strRoomID);
            mSocket.on(Socket.EVENT_CONNECT, onConnect);
            mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
            mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            mSocket.on("newMessage", onNewMessage);
            mSocket.on("ChatStatus", onUserJoined);
            mSocket.on("type", onTyping);
            mSocket.connect();
            if (isAddClick == true) {
                isAddClick = false;
            } else {
                //*Execute API For Getting List *//*
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                mAllMessagesItem.clear();
                mGalleryArrayList.clear();
                page_num = 1;
                executeAPIRetrofitNew();
            }
        }
    }

    @Override
    protected void setViewsIDs() {
        dataRV = findViewById(R.id.dataRV);
        textED = findViewById(R.id.textED);
        progressBottomPB = findViewById(R.id.progressBottomPB);
        sendProgressBar = findViewById(R.id.sendProgressBar);
        textvesselName = findViewById(R.id.textvesselName);
        textStatus = findViewById(R.id.textStatus);
        llLeftLL = findViewById(R.id.llLeftLL);
        llRightLL = findViewById(R.id.llRightLL);
        recordIDLL = findViewById(R.id.recordIDLL);
        recordIDheadingTV = findViewById(R.id.recordIDheadingTV);
        headingIDLL = findViewById(R.id.headingIDLL);
        recordIDTV = findViewById(R.id.recordIDTV);
        headingTV = findViewById(R.id.headingTV);
        addNote = findViewById(R.id.addNote);
        imagePic = findViewById(R.id.imagePic);
        addImgIV = findViewById(R.id.addImgIV);
        closeImageIV = findViewById(R.id.closeImageIV);
        sendIV = findViewById(R.id.sendIV);
        imageLL = findViewById(R.id.imageLL);
        crossRL = findViewById(R.id.crossRL);
        galaryLL = findViewById(R.id.galaryLL);
        editMessageText = findViewById(R.id.editMessageText);
        messageReplyRL = findViewById(R.id.messageReplyRL);
        galleryRecyclerView = findViewById(R.id.galleryRecyclerView);
        swipeTorefefresh = findViewById(R.id.swipeTorefefresh);
        swipeTorefefresh.setColorSchemeResources(R.color.colorAccent);

        swipeTorefefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                /* only refresh when last page is true i.e. there are more messages */
                if (strLastPage.equals("FALSE")) {
                    ++page_num;
                    executeAPIRetrofitNew();
                } else {
                    swipeTorefefresh.setRefreshing(false);
                }
            }
        });
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        llRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textStatus.getText().toString().equals("")) {
                    addOrUpdateNoteDialog("", "Add Note", "Save");
                } else {
                    addOrUpdateNoteDialog(textStatus.getText().toString(), "Update Note", "Update");
                }
            }
        });

        addImgIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "mGalleryArrayList.size(): " + mGalleryArrayList.size());
                if (mGalleryArrayList.size() <= 5 - 1) {
                    mToGrantPermission();
                } else {
                    showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit_chat));
                }
            }
        });

        closeImageIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mGalleryArrayList.clear();
            }
        });

        crossRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mGalleryArrayList.clear();
                messageReplyRL.setVisibility(GONE);
                isEditMessage = false;
                isReplyMessage = false;
                imageLL.setVisibility(GONE);
                galaryLL.setVisibility(View.GONE);
                editMessageText.setText("");
                textED.setText("");
            }
        });

        sendIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textED.getText().toString().trim().equals("")) {
                    showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_text));
                } else {
                    /* *
                     * Send Comments API
                     * */
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        if (isEditMessage == true) {
                            isEditMessage = false;
                            sendEditMessageAPIRetrofitNew();
                        } else if (isReplyMessage == true) {
                            isReplyMessage = false;
                            sendReplyMessageAPINew(strReplyID);
                        } else {
                            sendMessageAPIRetrofitNew();
                        }
                    }
                }
            }
        });

        textED.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String strChar = s.toString();
                if (!strChar.equals("")) {
                    if (!mSocket.connected()) return;

                    if (!mTyping) {
                        mTyping = true;
                        mSocket.emit("type", strRoomID, true);
                    }
                    mTypingHandler.removeCallbacks(onTypingTimeout);
                    mTypingHandler.postDelayed(onTypingTimeout, 1000);
                    scrollToBottom();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    /* ******
     * PERMISSIONS
     * ******/
    public void mToGrantPermission() {
        if (mCheckPermission()) {
            if (mGalleryArrayList.size() < 5) {
                openCameraGalleryDialog();
            } else {
                showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit_chat));
            }
        } else {
            mRequestPermission();
        }
    }

    private boolean mCheckPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int recordAudio = ContextCompat.checkSelfPermission(getApplicationContext(), recorfAudioStr);
        int read_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int write_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        return camera == PackageManager.PERMISSION_GRANTED && read_external_storage == PackageManager.PERMISSION_GRANTED && write_external_storage == PackageManager.PERMISSION_GRANTED && recordAudio == PackageManager.PERMISSION_GRANTED;
    }

    private void mRequestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{CAMERA, recorfAudioStr, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, 100);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    openCameraGalleryDialog();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
            default:
                break;
        }
    }

    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);
        TextView text_camra = dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = dialog.findViewById(R.id.txt_cancel);
        TextView txt_addDoc = dialog.findViewById(R.id.txt_addDoc);
        TextView tittleTV = dialog.findViewById(R.id.tittleTV);
        tittleTV.setText("Attach Files :-");
        tittleTV.setVisibility(GONE);
        txt_addDoc.setVisibility(VISIBLE);
        txt_files.setVisibility(GONE);
        txt_cancel.setVisibility(GONE);
        txt_files.setText("Camera Video");
        txt_cancel.setText("Gallery Video");
        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });

        txt_addDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (mGalleryArrayList.size() <= 5 - 1) {
                    openInternalStorage();
                } else {
                    showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_doc_limit_chat));
                }
            }
        });

        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });

        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openVideoCamera();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openVideoGallery();
                dialog.dismiss();
            }
        });

        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    private void openVideoGallery() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_PICK);
        intent.putExtra(EXTRA_VIDEO_QUALITY, 0);
        startActivityForResult(intent, 777);
    }

    private void openVideoCamera() {
        Intent takeVideoIntent = new Intent(ACTION_VIDEO_CAPTURE);
        takeVideoIntent.putExtra(EXTRA_VIDEO_QUALITY, 0);
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 29);

        if (takeVideoIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                ContentValues values = new ContentValues(1);
                values.put(MediaStore.Video.Media.MIME_TYPE, "video/*");
                fileUri = getApplicationContext().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);

                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(takeVideoIntent, 666);
            } else {
                takeVideoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivityForResult(takeVideoIntent, 666);

            }
        }
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 205);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */);
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Log.e(TAG, String.valueOf(mGalleryArrayList.size()));
        if (mGalleryArrayList.size() == 0) {
            Intent intent = new Intent(mActivity, AlbumSelectActivity.class);
            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 5);
            startActivityForResult(intent, GALLERY_REQUEST);
        } else if (mGalleryArrayList.size() == 5) {
            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit_chat));
        } else if (mGalleryArrayList.size() != 0) {
            Intent intent = new Intent(mActivity, AlbumSelectActivity.class);
            int GallarySize = 5 - mGalleryArrayList.size();
            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, GallarySize);
            startActivityForResult(intent, GALLERY_REQUEST);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllStaffForumMessages(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strforumID, String.valueOf(page_num)).
                enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                        Log.e(TAG, "***URLResponce***" + response);

//                        /* to manage onBackPressed for push and normal intent */
//                        if (!strPushType.equals("") && strPushType.equals(JaoharConstants.PUSH)) {
//                            Intent mIntent = new Intent(mActivity, ForumListActivity.class);
//                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            mIntent.putExtra(JaoharConstants.NOTIFICATION_TYPE, JaoharConstants.PUSH);
//                            startActivity(mIntent);
//                            finish();
//                        } else {
//                            finish();
//                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "***Error**" + t.getMessage());

//                        /* to manage onBackPressed for push and normal intent */
//                        if (!strPushType.equals("") && strPushType.equals(JaoharConstants.PUSH)) {
//                            Intent mIntent = new Intent(mActivity, ForumListActivity.class);
//                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            mIntent.putExtra(JaoharConstants.NOTIFICATION_TYPE, JaoharConstants.PUSH);
//                            startActivity(mIntent);
//                            finish();
//                        } else {
//                            finish();
//                        }
                    }
                });

        mSocket.emit("leaveChat", strRoomID);
    }

    private void openInternalStorage() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 505);

    }

    /* *
     * Execute API for Sending Forum Chat  Messages // API  AddStaffForumMessageNew
     * @param
     * @user_id
     * @forum_id
     * @message
     * @reply_id// Not Mandatory
     * @photo1// Not Mandatory
     * @photo2// Not Mandatory
     * @photo3// Not Mandatory
     * @photo4// Not Mandatory
     * @photo5// Not Mandatory
     * @photo6// Not Mandatory
     * @photo7// Not Mandatory
     * @photo8// Not Mandatory
     * @photo9// Not Mandatory
     * @photo10// Not Mandatory
     * @doc_name// Not Mandatory
     * @doc// Not Mandatory
     * */
    private void sendMessageAPIRetrofitNew() {
        RequestBody requestFile1;
        RequestBody requestFile2;
        RequestBody requestFile3;
        RequestBody requestFile4;
        RequestBody requestFile5;

        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;
        MultipartBody.Part mMultipartBody3 = null;
        MultipartBody.Part mMultipartBody4 = null;
        MultipartBody.Part mMultipartBody5 = null;

        if (mGalleryArrayList.size() > 0) {
            for (int i = 0; i < mGalleryArrayList.size(); i++) {
                if (i == 0) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrDocName();
                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody1 = MultipartBody.Part.createFormData("photo1", file.getName(), requestFile1);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                        }
                    }

                } else if (i == 1) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody2 = MultipartBody.Part.createFormData("photo2", file.getName(), requestFile2);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                        }
                    }

                } else if (i == 2) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody3 = MultipartBody.Part.createFormData("photo3", file.getName(), requestFile3);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                        }
                    }

                } else if (i == 3) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody4 = MultipartBody.Part.createFormData("photo4", file.getName(), requestFile4);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                        }
                    }

                } else if (i == 4) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody5 = MultipartBody.Part.createFormData("photo5", file.getName(), requestFile5);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                        }
                    }
                }
            }
        }

        sendProgressBar.setVisibility(VISIBLE);
        sendIV.setVisibility(GONE);

        RequestBody message = RequestBody.create(MediaType.parse("multipart/form-data"), textED.getText().toString());
        RequestBody forum_id = RequestBody.create(MediaType.parse("multipart/form-data"), strforumID);
        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addStaffForumMessageRequestNew(message, forum_id, user_id, mMultipartBody1, mMultipartBody2, mMultipartBody3, mMultipartBody4,
                mMultipartBody5).enqueue(new Callback<AddStaffForumMessageModel>() {
            @Override
            public void onResponse(Call<AddStaffForumMessageModel> call, retrofit2.Response<AddStaffForumMessageModel> response) {
                sendProgressBar.setVisibility(GONE);
                sendIV.setVisibility(VISIBLE);
                Log.e(TAG, "RESPONSE" + response.toString());

                if (response.body() != null) {
                    if (response.body().getStatus() == 1) {

                        textED.setText("");
                        mTyping = false;
                        mSocket.emit("type", strRoomID, false);

                        if (progressBottomPB != null) {
                            progressBottomPB.setVisibility(GONE);
                        }
                        textED.setText("");

                        mGalleryArrayList.clear();
                        setUpGalleryAdapter();
                        list.clear();
                        strPhoto1 = "";
                        strPhoto2 = "";
                        strPhoto3 = "";
                        strPhoto4 = "";
                        strPhoto5 = "";
                        strPhoto6 = "";
                        strPhoto7 = "";
                        strPhoto8 = "";
                        strPhoto9 = "";
                        strPhoto10 = "";
                        strDocname1 = "";
                        strDocname2 = "";
                        strDocname3 = "";
                        strDocname4 = "";
                        strDocname5 = "";
                        byteArray1 = null;
                        byteArray2 = null;
                        byteArray3 = null;
                        byteArray4 = null;
                        byteArray5 = null;
                        strDoc = "";
                        strDocumentNameforSend = "";
                        imageLL.setVisibility(View.VISIBLE);
                        galaryLL.setVisibility(View.GONE);
                        messageReplyRL.setVisibility(GONE);
                        isEditMessage = false;
                        editMessageText.setText("");
                        textED.setText("");
                        isAddClick = true;

                        AllMessagesItem mAllMessages = response.body().getAppendData();

                        if (!response.body().getUserId().equals("")) {
                            mAllMessages.setUserId(response.body().getUserId());
                            if (response.body().getUserId().equals(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""))) {
                                mAllMessages.setIntType(ForumChatModel.Sender);
                            } else {
                                mAllMessages.setIntType(ForumChatModel.Receiver);
                            }
                        }

                        // perform the sending message attempt.
                        Gson gson = new Gson();
                        String mOjectString = gson.toJson(response.body());
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(mOjectString);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("TAG", "*****Msg****" + mOjectString);
                        mSocket.emit("newMessage", strRoomID, obj);

                        mAllMessagesItem.add(mAllMessages);
                        setAdapter();
                        scrollToBottom();

                    } else if (response.body().getStatus() == 101) {
                        Log.e(TAG, "Unexpected*********:" + message);
                        onBackPressed();
                    } else {
                        showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                }
            }

            @Override
            public void onFailure(Call<AddStaffForumMessageModel> call, Throwable t) {
                sendProgressBar.setVisibility(GONE);
                sendIV.setVisibility(VISIBLE);
                Log.e(TAG, "ERROR" + t.toString());
            }
        });
    }

    /* *
     * Execute API for Sending Forum Chat  Messages // API  AddStaffForumMessageAndroid
     * @param
     * @user_id
     * @forum_id
     * @message
     * @reply_id// Mandatory
     * @photo1// Not Mandatory
     * @photo2// Not Mandatory
     * @photo3// Not Mandatory
     * @photo4// Not Mandatory
     * @photo5// Not Mandatory
     * @photo6// Not Mandatory
     * @photo7// Not Mandatory
     * @photo8// Not Mandatory
     * @photo9// Not Mandatory
     * @photo10// Not Mandatory
     * @doc_name// Not Mandatory
     * @doc// Not Mandatory
     * */
    private void sendReplyMessageAPINew(final String strReplyI) {
        RequestBody requestFile1;
        RequestBody requestFile2;
        RequestBody requestFile3;
        RequestBody requestFile4;
        RequestBody requestFile5;

        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;
        MultipartBody.Part mMultipartBody3 = null;
        MultipartBody.Part mMultipartBody4 = null;
        MultipartBody.Part mMultipartBody5 = null;

        if (mGalleryArrayList.size() > 0) {
            for (int i = 0; i < mGalleryArrayList.size(); i++) {
                if (i == 0) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrDocName();
                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody1 = MultipartBody.Part.createFormData("photo1", file.getName(), requestFile1);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                        }
                    }

                } else if (i == 1) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody2 = MultipartBody.Part.createFormData("photo2", file.getName(), requestFile2);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                        }
                    }

                } else if (i == 2) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody3 = MultipartBody.Part.createFormData("photo3", file.getName(), requestFile3);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                        }
                    }

                } else if (i == 3) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody4 = MultipartBody.Part.createFormData("photo4", file.getName(), requestFile4);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                        }
                    }

                } else if (i == 4) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody5 = MultipartBody.Part.createFormData("photo5", file.getName(), requestFile5);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                        }
                    }
                }
            }
        }

        RequestBody message = RequestBody.create(MediaType.parse("multipart/form-data"), textED.getText().toString());
        RequestBody forum_id = RequestBody.create(MediaType.parse("multipart/form-data"), strforumID);
        RequestBody reply_id = RequestBody.create(MediaType.parse("multipart/form-data"), strReplyI);
        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));

        sendProgressBar.setVisibility(VISIBLE);
        sendIV.setVisibility(GONE);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.replyStaffForumMessageRequestNew(message, forum_id, reply_id, user_id, mMultipartBody1, mMultipartBody2, mMultipartBody3, mMultipartBody4,
                mMultipartBody5).enqueue(new Callback<AddStaffForumMessageModel>() {
            @Override
            public void onResponse(Call<AddStaffForumMessageModel> call, retrofit2.Response<AddStaffForumMessageModel> response) {
                sendProgressBar.setVisibility(GONE);
                sendIV.setVisibility(VISIBLE);

                if (response.body() != null) {
                    if (response.body().getStatus() == 1) {

                        textED.setText("");
                        mTyping = false;
                        mSocket.emit("type", strRoomID, false);

                        if (progressBottomPB != null) {
                            progressBottomPB.setVisibility(GONE);
                        }
                        textED.setText("");
                        mGalleryArrayList.clear();
                        setUpGalleryAdapter();
                        list.clear();
                        strPhoto1 = "";
                        strPhoto2 = "";
                        strPhoto3 = "";
                        strPhoto4 = "";
                        strPhoto5 = "";
                        strPhoto6 = "";
                        strReplyID = "";
                        strPhoto7 = "";
                        strPhoto8 = "";
                        strPhoto9 = "";
                        strPhoto10 = "";
                        strDocname1 = "";
                        strDocname2 = "";
                        strDocname3 = "";
                        strDocname4 = "";
                        strDocname5 = "";
                        byteArray1 = null;
                        byteArray2 = null;
                        byteArray3 = null;
                        byteArray4 = null;
                        byteArray5 = null;
                        strDoc = "";
                        strDocumentNameforSend = "";
                        imageLL.setVisibility(View.VISIBLE);
                        galaryLL.setVisibility(GONE);
                        messageReplyRL.setVisibility(GONE);
                        isEditMessage = false;
                        isReplyMessage = false;
                        editMessageText.setText("");
                        textED.setText("");
                        isAddClick = true;

                        AllMessagesItem mAllMessages = response.body().getAppendData();

                        if (!response.body().getUserId().equals("")) {
                            mAllMessages.setUserId(response.body().getUserId());
                            if (response.body().getUserId().equals(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""))) {
                                mAllMessages.setIntType(ForumChatModel.Sender);
                            } else {
                                mAllMessages.setIntType(ForumChatModel.Receiver);
                            }
                        }

                        // perform the sending message attempt.
                        Gson gson = new Gson();
                        String mOjectString = gson.toJson(response.body());
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(mOjectString);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("TAG", "*****Msg****" + mOjectString);
                        mSocket.emit("newMessage", strRoomID, obj);

                        mAllMessagesItem.add(mAllMessages);
                        setAdapter();
                        scrollToBottom();

                    } else if (response.body().getStatus() == 101) {
                        Log.e(TAG, "Unexpected*********:" + message);
                        onBackPressed();
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                }
            }

            @Override
            public void onFailure(Call<AddStaffForumMessageModel> call, Throwable t) {
                sendProgressBar.setVisibility(GONE);
                sendIV.setVisibility(VISIBLE);
                Log.e(TAG, "ERROR" + t.toString());
            }
        });
    }

    /* *
     * Execute API for Sending Forum Chat Edit Messages // API  EditStaffForumMessageNew
     * @param
     * @user_id
     * @message_id
     * @message
     * @reply_id// Not Mandatory
     * @photo1// Not Mandatory
     * @photo2// Not Mandatory
     * @photo3// Not Mandatory
     * @photo4// Not Mandatory
     * @photo5// Not Mandatory
     * @photo6// Not Mandatory
     * @photo7// Not Mandatory
     * @photo8// Not Mandatory
     * @photo9// Not Mandatory
     * @photo10// Not Mandatory
     * @doc_name// Not Mandatory
     * @doc// Not Mandatory
     * */
    private void sendEditMessageAPIRetrofitNew() {
        RequestBody requestFile1;
        RequestBody requestFile2;
        RequestBody requestFile3;
        RequestBody requestFile4;
        RequestBody requestFile5;
        RequestBody requestFile6;
        RequestBody requestFile7;
        RequestBody requestFile8;
        RequestBody requestFile9;
        RequestBody requestFile10;
        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;
        MultipartBody.Part mMultipartBody3 = null;
        MultipartBody.Part mMultipartBody4 = null;
        MultipartBody.Part mMultipartBody5 = null;
        MultipartBody.Part mMultipartBody6 = null;
        MultipartBody.Part mMultipartBody7 = null;
        MultipartBody.Part mMultipartBody8 = null;
        MultipartBody.Part mMultipartBody9 = null;
        MultipartBody.Part mMultipartBody10 = null;

        if (mGalleryArrayList.size() > 0) {
            for (int i = 0; i < mGalleryArrayList.size(); i++) {
                if (i == 0) {
                    if (mGalleryArrayList.get(i).getByteArray() != null) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strPhoto1 = "";
                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            strDocname1 = mGalleryArrayList.get(i).getStrDocName();

                            File file = null;
                            try {
                                file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", file.getName(), requestFile1);

                        } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                            strDocname1 = mGalleryArrayList.get(i).getStrDocName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                            }

                        } else {
                            strDocname1 = mGalleryArrayList.get(i).getStrImageName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                            }
                        }

                    } else {
                        strPhoto1 = mGalleryArrayList.get(i).getStrImagePath();
                        strDocname1 = "";

                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {

                            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                            File folder = new File(extStorageDirectory, "pdf");
                            folder.mkdir();
                            File file = new File(folder, "Read.pdf");
                            try {
                                file.createNewFile();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            DownloadFile(mGalleryArrayList.get(i).getStrImagePath(), file);

                            requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", file.getName(), requestFile1);

                        } else {
                            bitmap1 = getBitmapFromURL(mGalleryArrayList.get(i).getStrImagePath());

                            if (bitmap1 != null) {
                                requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), getbyteArray(bitmap1));
                                mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                            }
                        }
                    }
                } else if (i == 1) {
                    if (mGalleryArrayList.get(i).getByteArray() != null) {
                        strPhoto2 = "";
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                            File file = null;
                            try {
                                file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", file.getName(), requestFile2);

                        } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                            strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                            }
                        } else {
                            strDocname2 = mGalleryArrayList.get(i).getStrImageName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                            }
                        }

                    } else {

                        strPhoto2 = mGalleryArrayList.get(i).getStrImagePath();
                        strDocname2 = "";

                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {

                            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                            File folder = new File(extStorageDirectory, "pdf");
                            folder.mkdir();
                            File file = new File(folder, "Read.pdf");
                            try {
                                file.createNewFile();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            DownloadFile(mGalleryArrayList.get(i).getStrImagePath(), file);

                            requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", file.getName(), requestFile2);

                        } else {
                            bitmap2 = getBitmapFromURL(mGalleryArrayList.get(i).getStrImagePath());

                            if (bitmap2 != null) {
                                requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), getbyteArray(bitmap2));
                                mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                            }
                        }
                    }
                } else if (i == 2) {
                    if (mGalleryArrayList.get(i).getByteArray() != null) {
                        strPhoto3 = "";
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                            File file = null;
                            try {
                                file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", file.getName(), requestFile3);

                        } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                            strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                            }
                        } else {
                            strDocname3 = mGalleryArrayList.get(i).getStrImageName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                            }
                        }

                    } else {
                        strPhoto3 = mGalleryArrayList.get(i).getStrImagePath();
                        strDocname3 = "";

                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {

                            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                            File folder = new File(extStorageDirectory, "pdf");
                            folder.mkdir();
                            File file = new File(folder, "Read.pdf");
                            try {
                                file.createNewFile();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            DownloadFile(mGalleryArrayList.get(i).getStrImagePath(), file);

                            requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", file.getName(), requestFile3);

                        } else {

                            bitmap3 = getBitmapFromURL(mGalleryArrayList.get(i).getStrImagePath());

                            if (bitmap3 != null) {
                                requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), getbyteArray(bitmap3));
                                mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                            }
                        }
                    }
                } else if (i == 3) {
                    if (mGalleryArrayList.get(i).getByteArray() != null) {
                        strPhoto4 = "";
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                            File file = null;
                            try {
                                file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", file.getName(), requestFile4);

                        } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                            strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                            }
                        } else {
                            strDocname4 = mGalleryArrayList.get(i).getStrImageName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                            }
                        }

                    } else {
                        strPhoto4 = mGalleryArrayList.get(i).getStrImagePath();
                        strDocname4 = "";

                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {

                            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                            File folder = new File(extStorageDirectory, "pdf");
                            folder.mkdir();
                            File file = new File(folder, "Read.pdf");
                            try {
                                file.createNewFile();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            DownloadFile(mGalleryArrayList.get(i).getStrImagePath(), file);

                            requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", file.getName(), requestFile4);

                        } else {

                            bitmap4 = getBitmapFromURL(mGalleryArrayList.get(i).getStrImagePath());

                            if (bitmap4 != null) {
                                requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), getbyteArray(bitmap4));
                                mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                            }
                        }
                    }
                } else if (i == 4) {
                    if (mGalleryArrayList.get(i).getByteArray() != null) {
                        strPhoto5 = "";
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                            File file = null;
                            try {
                                file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", file.getName(), requestFile5);

                        } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                            strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                            }
                        } else {
                            strDocname5 = mGalleryArrayList.get(i).getStrImageName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                            }
                        }


                    } else {
                        strPhoto5 = mGalleryArrayList.get(i).getStrImagePath();
                        strDocname5 = "";

                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {

                            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                            File folder = new File(extStorageDirectory, "pdf");
                            folder.mkdir();
                            File file = new File(folder, "Read.pdf");
                            try {
                                file.createNewFile();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            DownloadFile(mGalleryArrayList.get(i).getStrImagePath(), file);

                            requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", file.getName(), requestFile5);

                        } else {

                            bitmap5 = getBitmapFromURL(mGalleryArrayList.get(i).getStrImagePath());

                            if (bitmap5 != null) {
                                requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), getbyteArray(bitmap5));
                                mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                            }
                        }
                    }
                } else if (i == 5) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains(".pdf")) {
                            strPhoto6 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strDoc = Utilities.convertByteArrayToBase64(mGalleryArrayList.get(i).getByteArray());
                            strDocumentNameforSend = mGalleryArrayList.get(i).getStrDocName();
                        }
                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains("http")) {
                            strPhoto6 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strPhoto6 = ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap());
                        }
                    }
                } else if (i == 6) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains(".pdf")) {
                            strPhoto7 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strDoc = Utilities.convertByteArrayToBase64(mGalleryArrayList.get(i).getByteArray());
                            strDocumentNameforSend = mGalleryArrayList.get(i).getStrDocName();
                        }
                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains("http")) {
                            strPhoto7 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strPhoto7 = ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap());
                        }
                    }
                } else if (i == 7) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains(".pdf")) {
                            strPhoto8 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strDoc = Utilities.convertByteArrayToBase64(mGalleryArrayList.get(i).getByteArray());
                            strDocumentNameforSend = mGalleryArrayList.get(i).getStrDocName();
                        }
                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains("http")) {
                            strPhoto8 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strPhoto8 = ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap());
                        }
                    }
                } else if (i == 8) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains(".pdf")) {
                            strPhoto9 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strDoc = Utilities.convertByteArrayToBase64(mGalleryArrayList.get(i).getByteArray());
                            strDocumentNameforSend = mGalleryArrayList.get(i).getStrDocName();
                        }
                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains("http")) {
                            strPhoto9 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strPhoto9 = ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap());
                        }
                    }
                } else if (i == 9) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains(".pdf")) {
                            strPhoto10 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strDoc = Utilities.convertByteArrayToBase64(mGalleryArrayList.get(i).getByteArray());
                            strDocname1 = mGalleryArrayList.get(i).getStrDocName();
                        }
                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains("http")) {
                            strPhoto10 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strPhoto10 = ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap());
                        }
                    }
                }
            }
        }

        RequestBody message = RequestBody.create(MediaType.parse("multipart/form-data"), textED.getText().toString());
        RequestBody message_id = RequestBody.create(MediaType.parse("multipart/form-data"), strMessageID);
        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));

        sendProgressBar.setVisibility(VISIBLE);
        sendIV.setVisibility(GONE);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editStaffForumMessageRequestNew(message, message_id, user_id, mMultipartBody1, mMultipartBody2, mMultipartBody3, mMultipartBody4,
                mMultipartBody5).enqueue(new Callback<AddStaffForumMessageModel>() {
            @Override
            public void onResponse(Call<AddStaffForumMessageModel> call, retrofit2.Response<AddStaffForumMessageModel> response) {
                sendProgressBar.setVisibility(GONE);
                sendIV.setVisibility(VISIBLE);

                if (response.body() != null) {
                    if (response.body().getStatus() == 1) {

                        textED.setText("");
                        mTyping = false;
                        mSocket.emit("type", strRoomID, false);

                        if (progressBottomPB != null) {
                            progressBottomPB.setVisibility(GONE);
                        }
                        textED.setText("");
                        mGalleryArrayList.clear();
                        setUpGalleryAdapter();
                        list.clear();
                        strPhoto1 = "";
                        strPhoto2 = "";
                        strPhoto3 = "";
                        strPhoto4 = "";
                        strPhoto5 = "";
                        strPhoto6 = "";
                        strReplyID = "";
                        strPhoto7 = "";
                        strPhoto8 = "";
                        strPhoto9 = "";
                        strPhoto10 = "";
                        strDocname1 = "";
                        strDocname2 = "";
                        strDocname3 = "";
                        strDocname4 = "";
                        strDocname5 = "";
                        byteArray1 = null;
                        byteArray2 = null;
                        byteArray3 = null;
                        byteArray4 = null;
                        byteArray5 = null;
                        strDoc = "";
                        strDocumentNameforSend = "";
                        imageLL.setVisibility(View.VISIBLE);
                        galaryLL.setVisibility(GONE);
                        messageReplyRL.setVisibility(GONE);
                        isEditMessage = false;
                        isReplyMessage = false;
                        editMessageText.setText("");
                        textED.setText("");
                        isAddClick = true;

                        AllMessagesItem mAllMessages = response.body().getAppendData();

                        if (!response.body().getUserId().equals("")) {
                            mAllMessages.setUserId(response.body().getUserId());
                            if (response.body().getUserId().equals(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""))) {
                                mAllMessages.setIntType(ForumChatModel.Sender);
                            } else {
                                mAllMessages.setIntType(ForumChatModel.Receiver);
                            }
                        }

                        // perform the sending message attempt.
                        Gson gson = new Gson();
                        String mOjectString = gson.toJson(response.body());
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(mOjectString);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("TAG", "*****Msg****" + mOjectString);
                        mSocket.emit("newMessage", strRoomID, obj);

                        mAllMessagesItem.set(currentMSGpositon, mAllMessages);
                        mAdapter.notifyItemChanged(currentMSGpositon);
//                        scrollToBottom();

                    } else if (response.body().getStatus() == 101) {
                        Log.e(TAG, "Unexpected*********:" + message);
                        onBackPressed();
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                }
            }

            @Override
            public void onFailure(Call<AddStaffForumMessageModel> call, Throwable t) {
                sendProgressBar.setVisibility(GONE);
                sendIV.setVisibility(VISIBLE);
                Log.e(TAG, "ERROR" + t.toString());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /* *
     * Execute API for getting Forum Chat All Messages // API  GetAllStaffForumMessages
     * @param
     * @user_id
     * @forum_id
     * */
    public void executeAPIRetrofitNew() {
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(GONE);
            }
        } else {
            if (page_num == 1) {
                if (isSwipeRefresh) {
                }
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllStaffForumMessagesNew(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strforumID, String.valueOf(page_num)).
                enqueue(new Callback<GetAllForumMessagesModel>() {
                    @Override
                    public void onResponse(Call<GetAllForumMessagesModel> call, retrofit2.Response<GetAllForumMessagesModel> response) {
                        Log.e(TAG, "***URLResponce***" + response);

                        if (isSwipeRefresh) {
                            swipeTorefefresh.setRefreshing(false);
                        }

                        if (response.body() != null) {
                            if (response.body().getStatus().equals("1")) {

                                ForumDetail mData = response.body().getData().getForumDetail();

                                if (!mData.getVesselRecordId().equals("")) {
                                    recordIDheadingTV.setText(getResources().getString(R.string.record_id));
                                    recordIDTV.setText(mData.getVesselRecordId());
                                } else {
                                    recordIDheadingTV.setText(getResources().getString(R.string.forum_id));
                                    recordIDTV.setText(strforumID);
                                }
                                if (!mData.getHeading().equals("")) {
                                    headingIDLL.setVisibility(View.GONE);
                                    addNote.setImageDrawable(getResources().getDrawable(R.drawable.edit_white_icon));
                                    textStatus.setVisibility(VISIBLE);
                                    textStatus.setText(mData.getHeading());
                                    strHeadingType = "edit";
                                } else {
                                    strHeadingType = "add";
                                    addNote.setImageDrawable(getResources().getDrawable(R.drawable.add_icon));
                                    headingIDLL.setVisibility(GONE);
                                    textStatus.setVisibility(View.GONE);
                                }
                                if (!mData.getLastPage().equals("")) {
                                    strLastPage = mData.getLastPage();
                                }
                                if (!mData.getVesselName().equals("")) {
                                    textvesselName.setText(mData.getVesselName());
                                }
                                if (!mData.getIMONumber().equals("")) {

                                }
                                if (!mData.getPic().equals("")) {
//                                    if (!this.isFinishing()) {
                                    // Load the image using Glide
                                    Glide.with(mActivity).load(mData.getPic()).into(imagePic);
//                                    }
                                } else {
                                    imagePic.setImageDrawable(getDrawable(R.drawable.palace_holder));
                                }

                                for (int i = 0; i < response.body().getData().getForumDetail().getAllMessages().size(); i++) {

                                    if (!response.body().getData().getForumDetail().getAllMessages().get(i).getUserId().equals("")) {
                                        if (response.body().getData().getForumDetail().getAllMessages().get(i).getUserId().equals(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""))) {
                                            response.body().getData().getForumDetail().getAllMessages().get(i).setIntType(JaoharConstants.Sender);
                                        } else if (response.body().getData().getForumDetail().getAllMessages().get(i).getUserId().equals(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""))) {
                                            response.body().getData().getForumDetail().getAllMessages().get(i).setIntType(JaoharConstants.Sender);
                                        } else {
                                            response.body().getData().getForumDetail().getAllMessages().get(i).setIntType(JaoharConstants.Receiver);
                                        }
                                    }

                                    if (response.body().getData().getForumDetail().getAllMessages().get(i).isEditable()) {
                                        response.body().getData().getForumDetail().getAllMessages().get(i).setIntType(ForumChatModel.Sender);
                                    } else if (!response.body().getData().getForumDetail().getAllMessages().get(i).isEditable()) {
                                        response.body().getData().getForumDetail().getAllMessages().get(i).setIntType(ForumChatModel.Receiver);
                                    }
                                }

                                if (page_num == 1) {
                                    mAllMessagesItem = response.body().getData().getForumDetail().getAllMessages();
                                } else if (page_num > 1) {
                                    mGetAllLoadMore = response.body().getData().getForumDetail().getAllMessages();
                                }

                                if (mGetAllLoadMore.size() > 0) {
                                    mAllMessagesItem.addAll(0, mGetAllLoadMore);
                                }
                                Log.e(TAG, "**mAllMessagesItem**" + mAllMessagesItem.toString());

                                if (page_num == 1) {
                                    setAdapter();
                                    scrollToBottom();
                                } else {
                                    mAdapter.updateLastPageData(strLastPage);
                                    if (mGetAllLoadMore != null && mGetAllLoadMore.size() > 0) {
                                        dataRV.scrollToPosition(mGetAllLoadMore.size());
                                    }
                                    mAdapter.notifyDataSetChanged();
                                }

                            } else if (response.body().getStatus().equals("101")) {
                                Log.e(TAG, "Unexpected*********:" + response.message());
                                onBackPressed();
                            } else {
                                showAlertDialog(mActivity, getString(R.string.app_name), "" + response.message());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetAllForumMessagesModel> call, Throwable t) {
                        Log.e(TAG, "***Error**" + t.toString());
                        hideProgressDialog();
                        showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
                    }
                });
    }

    Map<String, String> mParams() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        params.put("forum_id", strforumID);
        params.put("msg_ids", String.valueOf(strData));
        Log.e(TAG, "**PARAMS**" + params);
        return params;
    }

    public void executeCheckUpdateAPI() {
        strData = new ArrayList<>();
        Log.e("TAG", "sizeArrayList" + mAllMessagesItem.size());
        if (mAllMessagesItem.size() != 0) {
            for (int i = 0; i < mAllMessagesItem.size(); i++) {
                strData.add(mAllMessagesItem.get(i).getMessageId());
            }
            Log.e("TAG", "sizeArrayListDAta" + strData.toString());
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.checkStaffForumUpdation(mParams()).
                enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                        Log.e(TAG, "***URLResponce***" + response);

                        try {
                            JSONObject mJsonDATA = new JSONObject(response.body().toString());
                            if (mJsonDATA.getString("status").equals("1")) {
                                if (isAddClick == true) {
                                    isAddClick = false;
                                } else {
                                    mGalleryArrayList.clear();
                                    strData.clear();

                                    page_num = 1;
                                    executeAPIRetrofitNew();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Log.e(TAG, "***Error**" + t.toString());
                        hideProgressDialog();
                    }
                });
    }

    /* *
     * Setting up Adapter to show chat in recycler view
     * */
    void setAdapter() {
        mAdapter = new ForumChatAdapter1(mActivity, mAllMessagesItem, mAdd_Edit_DelInterface, strLastPage, onClickInterface);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        if (textED.hasFocus()) {
            mLayoutManager.scrollToPosition(mAllMessagesItem.size() - 1);
        }
        textED.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayoutManager.scrollToPosition(mAllMessagesItem.size() - 1);
            }
        });
        if (page_num == 1) {
            mLayoutManager.scrollToPosition(mAllMessagesItem.size() - 1);
        }
        dataRV.setLayoutManager(mLayoutManager);
        dataRV.setItemAnimator(new DefaultItemAnimator());
        dataRV.setAdapter(mAdapter);
        mGalleryArrayList.clear();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);//images.get(i).path
                            for (int i = 0, l = images.size(); i < l; i++) {
                                byte[] btye = getFileDataFromBitmap(mActivity, getThumbnailBitmap(images.get(i).path, 1000));
                                AddGalleryImagesModel model = new AddGalleryImagesModel();
                                model.setStrImagePath(images.get(i).path);
                                model.setByteArray(btye);
                                model.setStrType("img");
                                model.setStrImageName(images.get(i).path.substring(images.get(i).path.lastIndexOf("/") + 1));
                                model.setmBitmap(getThumbnailBitmap(images.get(i).path, 1000));
                                if (mGalleryArrayList.size() < 5) {
                                    galaryLL.setVisibility(View.VISIBLE);
                                    imageLL.setVisibility(View.VISIBLE);
                                    mGalleryArrayList.add(model);
                                }
                            }
                            /*Set GalleryImages
                             * in recycler View*/
                            setUpGalleryAdapter();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "*****No Picture Selected****");
                        return;
                    }
                } finally {
                }
            } else if (requestCode == CAMERA_REQUEST) {
                try {
                    //Bitmap thumb = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);//getCompressedBitmap(mStoragePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 0;
                    Bitmap thumb = BitmapFactory.decodeFile(mStoragePath, options);
                    ExifInterface exifInterface = null;
                    try {
                        exifInterface = new ExifInterface(mStoragePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Matrix matrix = new Matrix();
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            matrix.setRotate(90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            matrix.setRotate(180);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            matrix.setRotate(270);
                            break;
                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                    }

                    Bitmap rotate = getThumbnailBitmap(mStoragePath, 1000);
                    byte[] btye = getFileDataFromBitmap(mActivity, rotate);
                    AddGalleryImagesModel model = new AddGalleryImagesModel();
                    model.setStrImagePath(mStoragePath);
                    model.setByteArray(btye);
                    model.setStrImageName(mStoragePath.substring(mStoragePath.lastIndexOf("/") + 1));
                    model.setStrType("img");
                    // model.setStrImageBitmap(strImageBase64);
                    model.setmBitmap(rotate);
                    if (mGalleryArrayList.size() < 5) {
                        galaryLL.setVisibility(View.VISIBLE);
                        imageLL.setVisibility(View.VISIBLE);
                        mGalleryArrayList.add(model);
                    }

                    /*Set GalleryImages
                     * in recycler View*/
                    setUpGalleryAdapter();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 505 && resultCode == Activity.RESULT_OK) {
                try {
                    Uri fileuri = data.getData();
                    String docFilePath = getFileNameByUri(mActivity, fileuri);

                    AddGalleryImagesModel model = new AddGalleryImagesModel();
                    model.setStrImagePath(docFilePath);
                    model.setmUri(fileuri);
                    model.setByteArray(Utilities.convertDocumentToByteArray(docFilePath));
                    model.setStrDocName(docFilePath.substring(docFilePath.lastIndexOf("/") + 1));
                    model.setStrType("doc");

                    if (mGalleryArrayList.size() < 5) {
                        galaryLL.setVisibility(VISIBLE);
                        imageLL.setVisibility(VISIBLE);
                        mGalleryArrayList.add(model);
                    }
                    /*Set GalleryImages in recycler View*/
                    setUpGalleryAdapter();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 666 && resultCode == Activity.RESULT_OK) {
                Uri vd = fileUri;
                Uri videoUri;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    videoUri = fileUri;
                } else {
                    videoUri = data.getData();
                }
                Log.i("", "" + videoUri);

                //To Show The Video Thumbnail......
                String filePath = getRealPathFromURIView(mActivity, videoUri, "VIDEO");
                Intent mIntent = new Intent(mActivity, VideoTrimerActivity.class);
                mIntent.putExtra("filepath", filePath);
                startActivityForResult(mIntent, 201);
            } else if (resultCode == RESULT_OK && requestCode == 777) {
                Uri videoUri = data.getData();
                Log.i("", "" + videoUri);

                try {
                    if (videoUri != null) {
                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//                      use one of overloaded setDataSource() functions to set your data source
                        retriever.setDataSource(getApplicationContext(), videoUri);
                        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                        int timeInMillisec = Integer.parseInt(time);
                        int temp = timeInMillisec / 1000;
                        retriever.release();
                        if (temp <= 30) {
                            String mUri = RealPathUtil.getRealPath(mActivity, videoUri);
                            Intent mIntent = new Intent(mActivity, VideoTrimerActivity.class);
                            mIntent.putExtra("filepath", mUri);
                            startActivityForResult(mIntent, 201);

                        } else {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(mActivity, "Maximum 30 Seconds", Toast.LENGTH_LONG).show();
                                    Log.e("Maximum 30 Seconds", "*******************Maximum 30 Seconds********************");
                                }
                            }, 3000);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 201) {
                if (data != null) {
                    InputStream iStream = null;
                    Uri mImageUri = Uri.parse(data.getStringExtra("filepath1"));
                    String fileName = mImageUri.getPath();
                    galaryLL.setVisibility(View.VISIBLE);
                    imageLL.setVisibility(View.VISIBLE);
//                Log.e("test","URITESTName===="+fileName.substring(fileName.lastIndexOf("/") + 1));
                    /**Convert URI to Byte Array**/
//                    iStream = getApplicationContext().getContentResolver().openInputStream(mImageUri);
//                    byte[] inputData =getBytes(iStream);
//                    String filepath = getRealPathFromURI(mImageUri);

                    AddGalleryImagesModel model1 = new AddGalleryImagesModel();
                    model1.setStrImagePath(fileName);
                    model1.setByteArray(Utilities.convertDocumentToByteArray(fileName));
                    model1.setStrDocName(fileName.substring(fileName.lastIndexOf("/") + 1));
                    model1.setStrType("video");

                    if (mGalleryArrayList.size() < 5) {
                        isAddVideo = true;
                        mGalleryArrayList.add(model1);
                    }

                    /*Set GalleryImages
                     * in recycler View*/
                    setUpGalleryAdapter();
                }
            }
        }
    }

    // Getting File name from uri
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    /**
     * Getting Byte Array From Input File Stream
     **/
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 512;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    /**
     * Turn bitmap into byte array
     *
     * @param bitmap data
     * @returen byte array
     */
    public static byte[] getFileDataFromBitmap(Context context, Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    private void setUpGalleryAdapter() {
        if (mGalleryArrayList.size() >= 1) {
            Log.e("TAG", "DATASIZE" + "Video1ImageVisisble" + mGalleryArrayList.size());
            galaryLL.setVisibility(View.VISIBLE);

        } else {
            if (isEditMessage == true) {
                imageLL.setVisibility(VISIBLE);
                galaryLL.setVisibility(VISIBLE);
            } else if (isReplyMessage == true) {
                imageLL.setVisibility(VISIBLE);
                galaryLL.setVisibility(VISIBLE);
            } else {
                imageLL.setVisibility(VISIBLE);
                galaryLL.setVisibility(View.GONE);
            }
        }
        Log.e("TAG", "DATASIZE" + "Video1" + mGalleryArrayList.size());
        mImageAdapter = new AddImageListAdapter(mActivity, mGalleryArrayList, mDeleteImagesInterface);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        galleryRecyclerView.setLayoutManager(horizontalLayoutManagaer);
        galleryRecyclerView.setAdapter(mImageAdapter);
    }

    public String getRealPathFromURI(Uri contentUri) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }

    private String queryName(ContentResolver resolver, Uri uri) {
        Cursor returnCursor =
                resolver.query(uri, null, null, null, null);
        assert returnCursor != null;
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        returnCursor.close();
        return name;
    }

    /**
     * returns the thumbnail image bitmap from the given url
     *
     * @param path
     * @param thumbnailSize
     * @return
     **/
    private Bitmap getThumbnailBitmap(final String path, final int thumbnailSize) {
        Bitmap bitmap;
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
            bitmap = null;
        }
        int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
                : bounds.outWidth;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / thumbnailSize;
        bitmap = BitmapFactory.decodeFile(path, opts);
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotate;
    }

    /* *
     * PopUp to Delete or Cancel the Message
     * */
    public void deleteConfirmDialog(String strMessage) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /* *
                 * Execute API to Delete Forum  Single message
                 * */
                executeDeleteAPINew();
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    /* *
     * PopUp to Add  or Update Note
     * */
    public void addOrUpdateNoteDialog(String strMessage, String strTittle, String strSend) {
        final Dialog addOrUpdateNoteDialog = new Dialog(mActivity);
        addOrUpdateNoteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addOrUpdateNoteDialog.setContentView(R.layout.item_adding_heading);
        addOrUpdateNoteDialog.setCanceledOnTouchOutside(true);
        addOrUpdateNoteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final EditText editToET = addOrUpdateNoteDialog.findViewById(R.id.editToET);
        final TextView textHeadingTV = addOrUpdateNoteDialog.findViewById(R.id.textHeadingTV);
        textHeadingTV.setText(strTittle);
        if (!strMessage.equals("")) {
            editToET.setText(strMessage);
            editToET.setSelection(editToET.getText().length());
        }

        TextView btnSend = addOrUpdateNoteDialog.findViewById(R.id.btnSend);
        TextView btnCancel = addOrUpdateNoteDialog.findViewById(R.id.btnCancel);
        RelativeLayout clossRL = addOrUpdateNoteDialog.findViewById(R.id.clossRL);

        btnSend.setText(strSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editToET.getText().toString().equals("")) {
//                    addOrUpdateNoteDialog.dismiss();
                    showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_enter_the_heading));
                } else {
                    addOrUpdateNoteDialog.dismiss();
                    /*
                     * Execute API to Add or Update note Forum
                     * */
                    executeAddNoteAPINew(editToET.getText().toString());
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addOrUpdateNoteDialog.dismiss();
            }
        });
        clossRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addOrUpdateNoteDialog.dismiss();
            }
        });
        addOrUpdateNoteDialog.show();
    }

    /* *
     * Execute API for Delete Forum Chat Message // API  DeleteStaffForumMessage
     * @param
     * @message_id
     * @user_id
     * */
    private void executeDeleteAPINew() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteStaffForumMessageNew(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strMessageID).
                enqueue(new Callback<DeleteStaffForumMessageModel>() {
                    @Override
                    public void onResponse(Call<DeleteStaffForumMessageModel> call, retrofit2.Response<DeleteStaffForumMessageModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "***URLResponce***" + response);

                        if (response.body() != null) {
                            if (response.body().getStatus() == 1) {
                                if (Utilities.isNetworkAvailable(mActivity) == false) {
                                    showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                                } else {
                                    //*Execute API For Getting List *//
                                    if (progressBottomPB != null) {
                                        progressBottomPB.setVisibility(GONE);
                                    }

                                    strDocumentNameforSend = "";
                                    messageReplyRL.setVisibility(GONE);
                                    isEditMessage = false;
                                    editMessageText.setText("");
                                    textED.setText("");
                                    isAddClick = true;

                                    AppendData addNewMessageModel = response.body().getAppendData();

                                    // perform the sending message attempt.
                                    Gson gson = new Gson();
                                    String mOjectString = gson.toJson(response.body());
                                    JSONObject obj = null;
                                    try {
                                        obj = new JSONObject(mOjectString);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Log.e("TAG", "*****Msg****" + mOjectString);
                                    mSocket.emit("newMessage", strRoomID, obj);

                                    Log.e(TAG, "Size - " + mAllMessagesItem.size());
                                    for (int i = 0; i < mAllMessagesItem.size(); i++) {
                                        if (mAllMessagesItem.get(i).getMessageId().equals(strMessageID)) {
                                            mAllMessagesItem.remove(i);
                                        }
                                    }
                                    mAdapter.notifyDataSetChanged();
                                }
                            } else {
                                showAlertDialog(mActivity, getResources().getString(R.string.app_name), response.body().getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteStaffForumMessageModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "***Error**" + t.getMessage());
                    }
                });
    }

    /* *
     * Execute API for getting Forum Chat All Messages // API  GetAllStaffForumMessages
     * @param
     * @user_id
     * @forum_id
     * @heading
     * */
    public void executeAddNoteAPINew(final String strMessage) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<UpdateForumHeadingModel> call1 = mApiInterface.updateForumHeadingRequestNew(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strforumID, strMessage.trim());
        call1.enqueue(new Callback<UpdateForumHeadingModel>() {
            @Override
            public void onResponse(Call<UpdateForumHeadingModel> call, retrofit2.Response<UpdateForumHeadingModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);

                if (response.body() != null) {

                    if (response.body().getStatus() == 1) {
                        if (progressBottomPB != null) {
                            progressBottomPB.setVisibility(GONE);
                        }
                        mAdapter.notifyDataSetChanged();
                        strDocumentNameforSend = "";
                        messageReplyRL.setVisibility(GONE);
                        isEditMessage = false;
                        editMessageText.setText("");
                        textED.setText("");

                        page_num = 1;
                        mAllMessagesItem.clear();
                        executeAPIRetrofitNew();

                    } else {
                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateForumHeadingModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    public void executeAddNoteAPI(final String strMessage) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.updateForumHeadingRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strforumID, strMessage.trim());
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                try {
                    JSONObject mJsonDATA = new JSONObject(response.body().toString());
                    if (mJsonDATA.getString("status").equals("1")) {
                        //*Execute API For Getting List *//*
                        if (progressBottomPB != null) {
                            progressBottomPB.setVisibility(GONE);
                        }
                        mAdapter.notifyDataSetChanged();
                        strDocumentNameforSend = "";
                        messageReplyRL.setVisibility(GONE);
                        isEditMessage = false;
                        editMessageText.setText("");
                        textED.setText("");

                        page_num = 1;
                        mAllMessagesItem.clear();
                        executeAPIRetrofitNew();

                    } else {
                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonDATA.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    public static String getRealPathFromURIView(Context context, Uri contentURI, String type) {
        String result = null;
        try {
            Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
            if (cursor == null) { // Source is Dropbox or other similar local file path
                result = contentURI.getPath();
                Log.d("TAG", "result******************" + result);
            } else {
                cursor.moveToFirst();
                int idx = 0;
                if (type.equalsIgnoreCase("IMAGE")) {
                    idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                } else if (type.equalsIgnoreCase("VIDEO")) {
                    idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
                } else if (type.equalsIgnoreCase("AUDIO")) {
                    idx = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);
                }
                result = cursor.getString(idx);
                Log.d("TAG", "result*************else*****" + result);
                cursor.close();
            }
        } catch (Exception e) {
            Log.e("TAG", "Exception ", e);
        }
        return result;
    }

    public static long getFileId(Activity context, Uri fileUri) {
        Cursor cursor = context.managedQuery(fileUri, mediaColumns, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
            int id = cursor.getInt(columnIndex);
            return id;
        }
        return 0;
    }

    // Socket Methods
    private final Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isConnected) {
                        isConnected = true;
                    }
                }
            });
        }
    };

    private final Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "diconnected");
                    isConnected = false;

                }
            });
        }
    };

    private final Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");

                }
            });
        }
    };

    private final Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        JSONObject mJsonData = new JSONObject(String.valueOf(args[1]));
                        JSONObject mJSONAppendData = mJsonData.getJSONObject("append_data");
                        if (mJSONAppendData.getString("message_type").equals("delete")) {
                            Log.e(TAG, "Size - " + mAllMessagesItem.size());
                            for (int i = 0; i < mAllMessagesItem.size(); i++) {
                                if (mAllMessagesItem.get(i).getMessageId().equals(mJSONAppendData.getString("message_id"))) {
                                    mAllMessagesItem.remove(i);
                                }
                            }
                            mAdapter.notifyDataSetChanged();
                        }
                        if (mJSONAppendData.getString("message_type").equals("update")) {
                            parseEditResponce(mJsonData);
                        } else {
                            parseAddResponce(mJsonData);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private final Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                }
            });
        }
    };

    private final Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };

    private final Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };

    private final Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    try {
                        username = data.getString("username");
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        return;
                    }
                }
            });
        }
    };

    private final Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
            mSocket.emit("type", strRoomID, false);
        }
    };

    private void scrollToBottom() {
        if (mAdapter != null)
            dataRV.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    private void parseAddResponce(JSONObject mJsonDATA) {
        try {
            JSONObject mJsonData = mJsonDATA.getJSONObject("append_data");

            AllMessagesItem mForumDATAMODEL = new AllMessagesItem();
            if (!mJsonData.getString("message_id").equals("")) {
                mForumDATAMODEL.setMessageId(mJsonData.getString("message_id"));
            }
            if (!mJsonData.getString("reply_id").equals("")) {
                mForumDATAMODEL.setReplyId(mJsonData.getString("reply_id"));
            }
            if (!mJsonData.getString("forum_id").equals("")) {
                mForumDATAMODEL.setForumId(mJsonData.getString("forum_id"));
            }
            if (!mJsonData.getString("user_id").equals("")) {
                mForumDATAMODEL.setUserId(mJsonData.getString("user_id"));
                if (mJsonData.getString("user_id").equals(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""))) {
                    mForumDATAMODEL.setIntType(ForumChatModel.Sender);
                } else {
                    mForumDATAMODEL.setIntType(ForumChatModel.Receiver);
                }
            }
            if (!mJsonData.getString("message").equals("")) {
                mForumDATAMODEL.setMessage(mJsonData.getString("message"));
            }
            if (!mJsonData.getString("creation_date").equals("")) {
                mForumDATAMODEL.setCreationDate(mJsonData.getString("creation_date"));
            }
            if (!mJsonData.getString("image").equals("")) {
                mForumDATAMODEL.setImage(mJsonData.getString("image"));
            }
            if (!mJsonData.getString("image2").equals("")) {
                mForumDATAMODEL.setImage2(mJsonData.getString("image2"));
            }
            if (!mJsonData.getString("image3").equals("")) {
                mForumDATAMODEL.setImage3(mJsonData.getString("image3"));
            }
            if (!mJsonData.getString("image4").equals("")) {
                mForumDATAMODEL.setImage4(mJsonData.getString("image4"));
            }
            if (!mJsonData.getString("image5").equals("")) {
                mForumDATAMODEL.setImage5(mJsonData.getString("image5"));
            }
            if (!mJsonData.getString("image6").equals("")) {
                mForumDATAMODEL.setImage6(mJsonData.getString("image6"));
            }
            if (!mJsonData.getString("image7").equals("")) {
                mForumDATAMODEL.setImage7(mJsonData.getString("image7"));
            }
            if (!mJsonData.getString("image8").equals("")) {
                mForumDATAMODEL.setImage8(mJsonData.getString("image8"));
            }
            if (!mJsonData.getString("image9").equals("")) {
                mForumDATAMODEL.setImage9(mJsonData.getString("image9"));
            }
            if (!mJsonData.getString("image10").equals("")) {
                mForumDATAMODEL.setImage10(mJsonData.getString("image10"));
            }
            if (!mJsonData.getString("image_count").equals("")) {
                mForumDATAMODEL.setImageCount(mJsonData.getString("image_count"));
            }
            if (!mJsonData.getString("editable").equals("")) {
                mForumDATAMODEL.setEditable(mJsonData.getBoolean("editable"));

            }
            if (!mJsonData.getString("user_detail").equals("")) {
                JSONObject mjsonUserDetail = mJsonData.getJSONObject("user_detail");
                jaohar.com.jaohar.models.forummodels.UserDetail mUserDetail = new UserDetail();
                if (!mjsonUserDetail.getString("id").equals("")) {
                    mUserDetail.setId(mjsonUserDetail.getString("id"));
                }
                if (!mjsonUserDetail.getString("email").equals("")) {
                    mUserDetail.setEmail(mjsonUserDetail.getString("email"));
                }
                if (!mjsonUserDetail.getString("password").equals("")) {
                    mUserDetail.setPassword(mjsonUserDetail.getString("password"));
                }
                if (!mjsonUserDetail.getString("role").equals("")) {
                    mUserDetail.setRole(mjsonUserDetail.getString("role"));
                }
                if (!mjsonUserDetail.getString("status").equals("")) {
                    mUserDetail.setStatus(mjsonUserDetail.getString("status"));
                }
                if (!mjsonUserDetail.getString("created").equals("")) {
                    mUserDetail.setCreated(mjsonUserDetail.getString("created"));
                }
                if (!mjsonUserDetail.getString("enabled").equals("")) {
                    mUserDetail.setEnabled(mjsonUserDetail.getString("enabled"));
                }
                if (!mjsonUserDetail.getString("company_name").equals("")) {
                    mUserDetail.setCompanyName(mjsonUserDetail.getString("company_name"));
                }
                if (!mjsonUserDetail.getString("first_name").equals("")) {
                    mUserDetail.setFirstName(mjsonUserDetail.getString("first_name"));
                }
                if (!mjsonUserDetail.getString("last_name").equals("")) {
                    mUserDetail.setLastName(mjsonUserDetail.getString("last_name"));
                }
                if (!mjsonUserDetail.getString("image").equals("")) {
                    mUserDetail.setImage(mjsonUserDetail.getString("image"));
                }
                mForumDATAMODEL.setUserDetail(mUserDetail);
            }

            if (mJsonData.has("reply_for") && !mJsonData.getString("reply_for").equals("")) {
                JSONObject mjsonReplyForDetail = mJsonData.getJSONObject("reply_for");
                ReplyFor mReplyDetail = new ReplyFor();

                if (!mjsonReplyForDetail.getString("message_id").equals("")) {
                    mReplyDetail.setMessageId(mjsonReplyForDetail.getString("message_id"));
                }
                if (!mjsonReplyForDetail.getString("reply_id").equals("")) {
                    mReplyDetail.setReplyId(mjsonReplyForDetail.getString("reply_id"));
                }
                if (!mjsonReplyForDetail.getString("forum_id").equals("")) {
                    mReplyDetail.setForumId(mjsonReplyForDetail.getString("forum_id"));
                }
                if (!mjsonReplyForDetail.getString("user_id").equals("")) {
                    mReplyDetail.setUserId(mjsonReplyForDetail.getString("user_id"));
                }
                if (!mjsonReplyForDetail.getString("message").equals("")) {
                    mReplyDetail.setMessage(mjsonReplyForDetail.getString("message"));
                }
                if (!mjsonReplyForDetail.getString("image").equals("")) {
                    mReplyDetail.setImage(mjsonReplyForDetail.getString("image"));
                }
                if (!mjsonReplyForDetail.getString("creation_date").equals("")) {
                    mReplyDetail.setCreationDate(mjsonReplyForDetail.getString("creation_date"));
                }
                if (!mjsonReplyForDetail.getString("image2").equals("")) {
                    mReplyDetail.setImage2(mjsonReplyForDetail.getString("image2"));
                }
                if (!mjsonReplyForDetail.getString("image_count").equals("")) {
                    mReplyDetail.setImageCount(mjsonReplyForDetail.getString("image_count"));
                }
                if (!mjsonReplyForDetail.getString("image3").equals("")) {
                    mReplyDetail.setImage3(mjsonReplyForDetail.getString("image3"));
                }
                if (!mjsonReplyForDetail.getString("image4").equals("")) {
                    mReplyDetail.setImage4(mjsonReplyForDetail.getString("image4"));
                }
                if (!mjsonReplyForDetail.getString("image5").equals("")) {
                    mReplyDetail.setImage5(mjsonReplyForDetail.getString("image5"));
                }
                if (!mjsonReplyForDetail.getString("image6").equals("")) {
                    mReplyDetail.setImage6(mjsonReplyForDetail.getString("image6"));
                }
                if (!mjsonReplyForDetail.getString("image7").equals("")) {
                    mReplyDetail.setImage7(mjsonReplyForDetail.getString("image7"));
                }
                if (!mjsonReplyForDetail.getString("image8").equals("")) {
                    mReplyDetail.setImage8(mjsonReplyForDetail.getString("image8"));
                }
                if (!mjsonReplyForDetail.getString("image9").equals("")) {
                    mReplyDetail.setImage9(mjsonReplyForDetail.getString("image9"));
                }
                if (!mjsonReplyForDetail.getString("image10").equals("")) {
                    mReplyDetail.setImage10(mjsonReplyForDetail.getString("image10"));
                }
                if (!mjsonReplyForDetail.getString("edited").equals("")) {
                    mReplyDetail.setEdited(mjsonReplyForDetail.getString("edited"));
                }
                if (!mjsonReplyForDetail.getString("enable").equals("")) {
                    mReplyDetail.setEnable(mjsonReplyForDetail.getString("enable"));
                }
                if (!mjsonReplyForDetail.getString("user_detail").equals("")) {
                    JSONObject mjsonUserDetail = mjsonReplyForDetail.getJSONObject("user_detail");
                    UserDetail mUserDetail = new UserDetail();
                    if (!mjsonUserDetail.getString("id").equals("")) {
                        mUserDetail.setId(mjsonUserDetail.getString("id"));
                    }
                    if (!mjsonUserDetail.getString("email").equals("")) {
                        mUserDetail.setEmail(mjsonUserDetail.getString("email"));
                    }
                    if (!mjsonUserDetail.getString("password").equals("")) {
                        mUserDetail.setPassword(mjsonUserDetail.getString("password"));
                    }
                    if (!mjsonUserDetail.getString("role").equals("")) {
                        mUserDetail.setRole(mjsonUserDetail.getString("role"));
                    }
                    if (!mjsonUserDetail.getString("status").equals("")) {
                        mUserDetail.setStatus(mjsonUserDetail.getString("status"));
                    }
                    if (!mjsonUserDetail.getString("created").equals("")) {
                        mUserDetail.setCreated(mjsonUserDetail.getString("created"));
                    }
                    if (!mjsonUserDetail.getString("enabled").equals("")) {
                        mUserDetail.setEnabled(mjsonUserDetail.getString("enabled"));
                    }
                    if (!mjsonUserDetail.getString("company_name").equals("")) {
                        mUserDetail.setCompanyName(mjsonUserDetail.getString("company_name"));
                    }
                    if (!mjsonUserDetail.getString("first_name").equals("")) {
                        mUserDetail.setFirstName(mjsonUserDetail.getString("first_name"));
                    }
                    if (!mjsonUserDetail.getString("last_name").equals("")) {
                        mUserDetail.setLastName(mjsonUserDetail.getString("last_name"));
                    }
                    if (!mjsonUserDetail.getString("image").equals("")) {
                        mUserDetail.setImage(mjsonUserDetail.getString("image"));
                    }
                    mReplyDetail.setUserDetail(mUserDetail);
                }
                mForumDATAMODEL.setReplyFor(mReplyDetail);
            }
            mAllMessagesItem.add(mForumDATAMODEL);
            setAdapter();
            scrollToBottom();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseEditResponce(JSONObject mJsonData1) {
        try {
            JSONObject mJsonData = mJsonData1.getJSONObject("append_data");

            AllMessagesItem mForumDATAMODEL = new AllMessagesItem();
            if (!mJsonData.getString("message_id").equals("")) {
                mForumDATAMODEL.setMessageId(mJsonData.getString("message_id"));
            }
            if (!mJsonData.getString("reply_id").equals("")) {
                mForumDATAMODEL.setReplyId(mJsonData.getString("reply_id"));
            }
            if (!mJsonData.getString("forum_id").equals("")) {
                mForumDATAMODEL.setForumId(mJsonData.getString("forum_id"));
            }
            if (!mJsonData.getString("user_id").equals("")) {
                mForumDATAMODEL.setUserId(mJsonData.getString("user_id"));
                if (mJsonData.getString("user_id").equals(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""))) {
                    mForumDATAMODEL.setIntType(ForumChatModel.Sender);
                } else {
                    mForumDATAMODEL.setIntType(ForumChatModel.Receiver);
                }
            }
            if (!mJsonData.getString("message").equals("")) {
                mForumDATAMODEL.setMessage(mJsonData.getString("message"));
            }
            if (!mJsonData.getString("creation_date").equals("")) {
                mForumDATAMODEL.setCreationDate(mJsonData.getString("creation_date"));
            }
            if (!mJsonData.getString("image").equals("")) {
                mForumDATAMODEL.setImage(mJsonData.getString("image"));
            }
            if (!mJsonData.getString("image2").equals("")) {
                mForumDATAMODEL.setImage2(mJsonData.getString("image2"));
            }
            if (!mJsonData.getString("image3").equals("")) {
                mForumDATAMODEL.setImage3(mJsonData.getString("image3"));
            }
            if (!mJsonData.getString("image4").equals("")) {
                mForumDATAMODEL.setImage4(mJsonData.getString("image4"));
            }
            if (!mJsonData.getString("image5").equals("")) {
                mForumDATAMODEL.setImage5(mJsonData.getString("image5"));
            }
            if (!mJsonData.getString("image6").equals("")) {
                mForumDATAMODEL.setImage6(mJsonData.getString("image6"));
            }
            if (!mJsonData.getString("image7").equals("")) {
                mForumDATAMODEL.setImage7(mJsonData.getString("image7"));
            }
            if (!mJsonData.getString("image8").equals("")) {
                mForumDATAMODEL.setImage8(mJsonData.getString("image8"));
            }
            if (!mJsonData.getString("image9").equals("")) {
                mForumDATAMODEL.setImage9(mJsonData.getString("image9"));
            }
            if (!mJsonData.getString("image10").equals("")) {
                mForumDATAMODEL.setImage10(mJsonData.getString("image10"));
            }
            if (!mJsonData.getString("image_count").equals("")) {
                mForumDATAMODEL.setImageCount(mJsonData.getString("image_count"));
            }
            if (!mJsonData.getString("editable").equals("")) {
                mForumDATAMODEL.setEditable(mJsonData.getBoolean("editable"));

            }
            if (!mJsonData.getString("user_detail").equals("")) {
                JSONObject mjsonUserDetail = mJsonData.getJSONObject("user_detail");
                jaohar.com.jaohar.models.forummodels.UserDetail mUserDetail = new UserDetail();
                if (!mjsonUserDetail.getString("id").equals("")) {
                    mUserDetail.setId(mjsonUserDetail.getString("id"));
                }
                if (!mjsonUserDetail.getString("email").equals("")) {
                    mUserDetail.setEmail(mjsonUserDetail.getString("email"));
                }
                if (!mjsonUserDetail.getString("password").equals("")) {
                    mUserDetail.setPassword(mjsonUserDetail.getString("password"));
                }
                if (!mjsonUserDetail.getString("role").equals("")) {
                    mUserDetail.setRole(mjsonUserDetail.getString("role"));
                }
                if (!mjsonUserDetail.getString("status").equals("")) {
                    mUserDetail.setStatus(mjsonUserDetail.getString("status"));
                }
                if (!mjsonUserDetail.getString("created").equals("")) {
                    mUserDetail.setCreated(mjsonUserDetail.getString("created"));
                }
                if (!mjsonUserDetail.getString("enabled").equals("")) {
                    mUserDetail.setEnabled(mjsonUserDetail.getString("enabled"));
                }
                if (!mjsonUserDetail.getString("company_name").equals("")) {
                    mUserDetail.setCompanyName(mjsonUserDetail.getString("company_name"));
                }
                if (!mjsonUserDetail.getString("first_name").equals("")) {
                    mUserDetail.setFirstName(mjsonUserDetail.getString("first_name"));
                }
                if (!mjsonUserDetail.getString("last_name").equals("")) {
                    mUserDetail.setLastName(mjsonUserDetail.getString("last_name"));
                }
                if (!mjsonUserDetail.getString("image").equals("")) {
                    mUserDetail.setImage(mjsonUserDetail.getString("image"));
                }
                mForumDATAMODEL.setUserDetail(mUserDetail);
            }

            if (mJsonData.has("reply_for") && !mJsonData.getString("reply_for").equals("")) {
                JSONObject mjsonReplyForDetail = mJsonData.getJSONObject("reply_for");
                ReplyFor mReplyDetail = new ReplyFor();

                if (!mjsonReplyForDetail.getString("message_id").equals("")) {
                    mReplyDetail.setMessageId(mjsonReplyForDetail.getString("message_id"));
                }
                if (!mjsonReplyForDetail.getString("reply_id").equals("")) {
                    mReplyDetail.setReplyId(mjsonReplyForDetail.getString("reply_id"));
                }
                if (!mjsonReplyForDetail.getString("forum_id").equals("")) {
                    mReplyDetail.setForumId(mjsonReplyForDetail.getString("forum_id"));
                }
                if (!mjsonReplyForDetail.getString("user_id").equals("")) {
                    mReplyDetail.setUserId(mjsonReplyForDetail.getString("user_id"));
                }
                if (!mjsonReplyForDetail.getString("message").equals("")) {
                    mReplyDetail.setMessage(mjsonReplyForDetail.getString("message"));
                }
                if (!mjsonReplyForDetail.getString("image").equals("")) {
                    mReplyDetail.setImage(mjsonReplyForDetail.getString("image"));
                }
                if (!mjsonReplyForDetail.getString("creation_date").equals("")) {
                    mReplyDetail.setCreationDate(mjsonReplyForDetail.getString("creation_date"));
                }
                if (!mjsonReplyForDetail.getString("image2").equals("")) {
                    mReplyDetail.setImage2(mjsonReplyForDetail.getString("image2"));
                }
                if (!mjsonReplyForDetail.getString("image_count").equals("")) {
                    mReplyDetail.setImageCount(mjsonReplyForDetail.getString("image_count"));
                }
                if (!mjsonReplyForDetail.getString("image3").equals("")) {
                    mReplyDetail.setImage3(mjsonReplyForDetail.getString("image3"));
                }
                if (!mjsonReplyForDetail.getString("image4").equals("")) {
                    mReplyDetail.setImage4(mjsonReplyForDetail.getString("image4"));
                }
                if (!mjsonReplyForDetail.getString("image5").equals("")) {
                    mReplyDetail.setImage5(mjsonReplyForDetail.getString("image5"));
                }
                if (!mjsonReplyForDetail.getString("image6").equals("")) {
                    mReplyDetail.setImage6(mjsonReplyForDetail.getString("image6"));
                }
                if (!mjsonReplyForDetail.getString("image7").equals("")) {
                    mReplyDetail.setImage7(mjsonReplyForDetail.getString("image7"));
                }
                if (!mjsonReplyForDetail.getString("image8").equals("")) {
                    mReplyDetail.setImage8(mjsonReplyForDetail.getString("image8"));
                }
                if (!mjsonReplyForDetail.getString("image9").equals("")) {
                    mReplyDetail.setImage9(mjsonReplyForDetail.getString("image9"));
                }
                if (!mjsonReplyForDetail.getString("image10").equals("")) {
                    mReplyDetail.setImage10(mjsonReplyForDetail.getString("image10"));
                }
                if (!mjsonReplyForDetail.getString("edited").equals("")) {
                    mReplyDetail.setEdited(mjsonReplyForDetail.getString("edited"));
                }
                if (!mjsonReplyForDetail.getString("enable").equals("")) {
                    mReplyDetail.setEnable(mjsonReplyForDetail.getString("enable"));
                }
                if (!mjsonReplyForDetail.getString("user_detail").equals("")) {
                    JSONObject mjsonUserDetail = mjsonReplyForDetail.getJSONObject("user_detail");
                    UserDetail mUserDetail = new UserDetail();
                    if (!mjsonUserDetail.getString("id").equals("")) {
                        mUserDetail.setId(mjsonUserDetail.getString("id"));
                    }
                    if (!mjsonUserDetail.getString("email").equals("")) {
                        mUserDetail.setEmail(mjsonUserDetail.getString("email"));
                    }
                    if (!mjsonUserDetail.getString("password").equals("")) {
                        mUserDetail.setPassword(mjsonUserDetail.getString("password"));
                    }
                    if (!mjsonUserDetail.getString("role").equals("")) {
                        mUserDetail.setRole(mjsonUserDetail.getString("role"));
                    }
                    if (!mjsonUserDetail.getString("status").equals("")) {
                        mUserDetail.setStatus(mjsonUserDetail.getString("status"));
                    }
                    if (!mjsonUserDetail.getString("created").equals("")) {
                        mUserDetail.setCreated(mjsonUserDetail.getString("created"));
                    }
                    if (!mjsonUserDetail.getString("enabled").equals("")) {
                        mUserDetail.setEnabled(mjsonUserDetail.getString("enabled"));
                    }
                    if (!mjsonUserDetail.getString("company_name").equals("")) {
                        mUserDetail.setCompanyName(mjsonUserDetail.getString("company_name"));
                    }
                    if (!mjsonUserDetail.getString("first_name").equals("")) {
                        mUserDetail.setFirstName(mjsonUserDetail.getString("first_name"));
                    }
                    if (!mjsonUserDetail.getString("last_name").equals("")) {
                        mUserDetail.setLastName(mjsonUserDetail.getString("last_name"));
                    }
                    if (!mjsonUserDetail.getString("image").equals("")) {
                        mUserDetail.setImage(mjsonUserDetail.getString("image"));
                    }
                    mReplyDetail.setUserDetail(mUserDetail);
                }
                mForumDATAMODEL.setReplyFor(mReplyDetail);
            }

            for (int i = 0; i < mAllMessagesItem.size(); i++) {
                if (mAllMessagesItem.get(i).getMessageId().equals(mJsonData.getString("message_id"))) {
                    mForumDATAMODEL.setEdited("1");
                    mAllMessagesItem.set(i, mForumDATAMODEL);
                    mAdapter.notifyItemChanged(i);
                }
            }

//            mAllMessagesItem.set(currentMSGpositon, mForumDATAMODEL);
//            mAdapter.notifyItemChanged(currentMSGpositon);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
