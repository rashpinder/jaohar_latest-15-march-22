package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.utils.JaoharConstants;

public class FullNewsDescriptionActivity extends BaseActivity {
    Activity mActivity = FullNewsDescriptionActivity.this;
    String TAG = FullNewsDescriptionActivity.this.getClass().getSimpleName(), strNews, strNewsPhotoURL;
    LinearLayout mainLayoutLL;
    RoundedImageView add_image1;
    WebView showTXT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_news_description);
        showTXT = (WebView) findViewById(R.id.showTXT);
    }

    @Override
    protected void setViewsIDs() {
        mainLayoutLL = (LinearLayout) findViewById(R.id.mainLayoutLL);
        add_image1 = (RoundedImageView) findViewById(R.id.add_image1);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            strNews = extras.getString("news");
            strNewsPhotoURL = extras.getString("newsImage");
        }
        if (!strNews.equals("")) {
            showTXT.loadDataWithBaseURL(null, strNews, "text/html", "UTF-8", null);
            WebSettings webSettings = showTXT.getSettings();
            Resources res = mActivity.getResources();
            webSettings.setDefaultFontSize((int) res.getDimension(R.dimen._3sdp));
        }

        if (!strNewsPhotoURL.equals("")) {
            Picasso.get().load(strNewsPhotoURL).into(add_image1);
        }
    }

    @Override
    protected void setClickListner() {
        mainLayoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//        if (JaoharConstants.is_Staff_FragmentClick == true) {
//            JaoharConstants.is_Staff_FragmentClick = false;
//            Intent mIntent = new Intent(mActivity, HomeActivity.class);
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            mIntent.putExtra(JaoharConstants.LOGIN, "Staff");
//            mActivity.startActivity(mIntent);
//            finish();
//        } else if (JaoharConstants.is_VessalsForSale == true) {
//            Intent mIntent = new Intent(mActivity, HomeActivity.class);
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            mIntent.putExtra(JaoharConstants.LOGIN, "VesselForSale");
//            mActivity.startActivity(mIntent);
//            finish();
//        } else if (JaoharConstants.Is_click_form_NEWS == true) {
//            JaoharConstants.Is_click_form_NEWS = false;
//            Intent mIntent = new Intent(mActivity, AllNewsActivity.class);
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            mActivity.startActivity(mIntent);
//            finish();
//        } else if (JaoharConstants.Is_click_form_Internal_NEWS == true) {
//            JaoharConstants.Is_click_form_Internal_NEWS = false;
//            Intent mIntent = new Intent(mActivity, AllInternalNewsActivity.class);
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            mActivity.startActivity(mIntent);
//            finish();
//        } else if (JaoharConstants.IS_VESSAL_ACTIVITY == true) {
//            JaoharConstants.IS_VESSAL_ACTIVITY = false;
//            Intent mIntent = new Intent(mActivity, AllVesselsActivity.class);
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            mActivity.startActivity(mIntent);
//            finish();
//        }
    }
}
