package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.LoginModell;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends BaseActivity {
    Activity mActivity = LoginActivity.this;
    String TAG = LoginActivity.this.getClass().getSimpleName();
    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;

    EditText editUserNameET, editPasswordET;
    Button btnLogin;
    TextView txtForgotPwd, termsTV;
    LinearLayout llDontHaveAccountSignUpLL;

    String strLoginType = "";
    String refreshedToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_login);

        getPushToken();
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    refreshedToken = task.getResult();
                    Log.e(TAG, "**Push Token**" + refreshedToken);

                });
    }

    @Override
    protected void setViewsIDs() {
        termsTV = findViewById(R.id.termsTV);
        llLeftLL = findViewById(R.id.llLeftLL);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = findViewById(R.id.txtCenter);
        editUserNameET = findViewById(R.id.editUserNameET);
        editPasswordET = findViewById(R.id.editPasswordET);
        btnLogin = findViewById(R.id.btnLogin);
        txtForgotPwd = findViewById(R.id.txtForgotPwd);
        llDontHaveAccountSignUpLL = findViewById(R.id.llDontHaveAccountSignUpLL);

        if (getIntent() != null) {
            strLoginType = getIntent().getStringExtra(JaoharConstants.LOGIN_TYPE);
            if (strLoginType.equals(JaoharConstants.USER)) {
                txtCenter.setText("User Login");
                txtForgotPwd.setVisibility(View.VISIBLE);
                llDontHaveAccountSignUpLL.setVisibility(View.VISIBLE);
            } else if (strLoginType.equals("VesselForSaleActivity")) {
                txtCenter.setText("User Login");
                txtForgotPwd.setVisibility(View.VISIBLE);
                llDontHaveAccountSignUpLL.setVisibility(View.VISIBLE);
            } else if (strLoginType.equals(JaoharConstants.ADMIN)) {
                txtCenter.setText("Manager Login");
                txtForgotPwd.setVisibility(View.VISIBLE);
                llDontHaveAccountSignUpLL.setVisibility(View.GONE);
            } else if (strLoginType.equals(JaoharConstants.BLOG)) {
                txtCenter.setText("User Login");
                txtForgotPwd.setVisibility(View.VISIBLE);
                llDontHaveAccountSignUpLL.setVisibility(View.VISIBLE);
            }
        }

        addClicksToTermsAndPrivacyString();
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtForgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strLoginType.equals(JaoharConstants.USER)) {
                    Intent mIntent = new Intent(mActivity, ForgotPasswordActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, strLoginType);
                    startActivity(mIntent);
                    overridePendingTransitionEnter();
                } else if (strLoginType.equals("VesselForSaleActivity")) {
                    Intent mIntent = new Intent(mActivity, ForgotPasswordActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, strLoginType);
                    startActivity(mIntent);
                    overridePendingTransitionEnter();
                } else if (strLoginType.equals(JaoharConstants.ADMIN)) {
                    Intent mIntent = new Intent(mActivity, ForgotPasswordActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, strLoginType);
                    startActivity(mIntent);
                    overridePendingTransitionEnter();
                } else if (strLoginType.equals(JaoharConstants.BLOG)) {
                    Intent mIntent = new Intent(mActivity, ForgotPasswordActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, JaoharConstants.USER);
                    startActivity(mIntent);
                    overridePendingTransitionEnter();
                }
            }
        });

        llDontHaveAccountSignUpLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strLoginType.equals(JaoharConstants.USER)) {
                    Intent mIntent = new Intent(mActivity, SignUpActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, strLoginType);
                    startActivity(mIntent);
                }
                if (strLoginType.equals(JaoharConstants.BLOG)) {
                    Intent mIntent = new Intent(mActivity, SignUpActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, JaoharConstants.USER);
                    startActivity(mIntent);
                } else if (strLoginType.equals("VesselForSaleActivity")) {
                    Intent mIntent = new Intent(mActivity, SignUpActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, "VesselForSaleActivity");
                    startActivity(mIntent);
                }
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editUserNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (Utilities.isValidEmaillId(editUserNameET.getText().toString()) != true) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (editPasswordET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_password));
                } else {
                    /*Execute Login API*/
                    executeAPI();
                }
            }
        });
    }

    private void addClicksToTermsAndPrivacyString() {
        SpannableString SpanString = new SpannableString(getString(R.string.by_logging_in));

        ClickableSpan termsAndCondition = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
                mIntent.putExtra("TITLE", "Terms & Conditions");
                mIntent.putExtra("LINK", JaoharConstants.TERMS_AND_CONDITIONS);
                startActivity(mIntent);

            }
        };

        // Character starting from 31 - 44 is privacy policy.
        // Character starting from 52 - 69 is Terms and condition.

        ClickableSpan privacy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
                mIntent.putExtra("TITLE", "Privacy Policy");
                mIntent.putExtra("LINK", JaoharConstants.PRIVACY_POLICY);
                startActivity(mIntent);
            }
        };

        SpanString.setSpan(termsAndCondition, 49, 67, 0);
        SpanString.setSpan(privacy, 30, 44, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 30, 44, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 49, 67, 0);
        SpanString.setSpan(new UnderlineSpan(), 30, 44, 0);
        SpanString.setSpan(new UnderlineSpan(), 49, 67, 0);

        termsTV.setMovementMethod(LinkMovementMethod.getInstance());
        termsTV.setText(SpanString, TextView.BufferType.SPANNABLE);
        termsTV.setSelected(true);
    }

    @Override
    public void onBackPressed() {
        if (strLoginType.equals(JaoharConstants.ADMIN)) {
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
            mActivity.startActivity(mIntent);
            mActivity.finish();
        } else if (strLoginType.equals(JaoharConstants.USER)) {
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mIntent.putExtra(JaoharConstants.LOGIN, "Home");
            mActivity.startActivity(mIntent);
            mActivity.finish();
        } else if (strLoginType.equals("VesselForSaleActivity")) {
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mIntent.putExtra(JaoharConstants.LOGIN, "Home");
            mActivity.startActivity(mIntent);
            mActivity.finish();
        } else if (strLoginType.equals(JaoharConstants.BLOG)) {
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mIntent.putExtra(JaoharConstants.LOGIN, "BlogHOME");
            mActivity.startActivity(mIntent);
            mActivity.finish();
        }
    }

    public void executeAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<LoginModell> call1 = mApiInterface.loginRequest(editUserNameET.getText().toString(), editPasswordET.getText().toString(), refreshedToken, "Android");
        call1.enqueue(new Callback<LoginModell>() {
            @Override
            public void onResponse(Call<LoginModell> call, retrofit2.Response<LoginModell> response) {
                AlertDialogManager.hideProgressDialog();
                LoginModell mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    JaoharPreference.writeString(mActivity, JaoharPreference.Role, mModel.getData().getRole());
                    JaoharPreference.writeString(mActivity, JaoharPreference.full_name, mModel.getData().getFirstName() + " " + mModel.getData().getLastName());
                    JaoharPreference.writeString(mActivity, JaoharPreference.first_name, mModel.getData().getFirstName());

                    if (strLoginType.equals(JaoharConstants.USER) && mModel.getData().getRole().equals(JaoharConstants.USER)) {
                        JaoharPreference.writeString(mActivity, JaoharPreference.USER_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(mActivity, JaoharPreference.USER_ID, mModel.getData().getId());
                        JaoharPreference.writeString(mActivity, JaoharPreference.BLOGUSER_ID, mModel.getData().getId());
                        JaoharPreference.writeString(mActivity, JaoharPreference.USER_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, true);
                        Intent mIntent = new Intent(mActivity, HomeActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mIntent.putExtra(JaoharConstants.LOGIN, "VesselForSale");
                        mActivity.startActivity(mIntent);
                        finish();
                    } else if (strLoginType.equals(JaoharConstants.BLOG)) {
                        JaoharPreference.writeString(mActivity, JaoharPreference.USER_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(mActivity, JaoharPreference.BLOGUSER_ID, mModel.getData().getId());
                        JaoharPreference.writeString(mActivity, JaoharPreference.USER_ID, mModel.getData().getId());
                        JaoharPreference.writeString(mActivity, JaoharPreference.USER_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, true);
                        Intent mIntent = new Intent(mActivity, HomeActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mIntent.putExtra(JaoharConstants.LOGIN, "BlogHOME");
                        mActivity.startActivity(mIntent);
                        finish();
                    } else if (strLoginType.equals("VesselForSaleActivity") && mModel.getData().getRole().equals(JaoharConstants.USER)) {
                        JaoharPreference.writeString(mActivity, JaoharPreference.USER_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(mActivity, JaoharPreference.USER_ID, mModel.getData().getId());
                        JaoharPreference.writeString(mActivity, JaoharPreference.USER_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, true);
                        Intent mIntent = new Intent(mActivity, VesselForSaleActivity.class);
                        mActivity.startActivity(mIntent);
                        finish();
                    } else if (strLoginType.equals(JaoharConstants.USER) && mModel.getData().getRole().equals(JaoharConstants.STAFF)) {
                        JaoharPreference.writeString(mActivity, JaoharPreference.STAFF_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(mActivity, JaoharPreference.STAFF_ID, mModel.getData().getId());
                        JaoharPreference.writeString(mActivity, JaoharPreference.STAFF_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, true);
                        Intent mIntent = new Intent(mActivity, HomeActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mIntent.putExtra(JaoharConstants.LOGIN, "VesselForSale");
                        mActivity.startActivity(mIntent);
                        finish();
                        mActivity.getFragmentManager().popBackStack();
                    } else if (strLoginType.equals("VesselForSaleActivity") && mModel.getData().getRole().equals(JaoharConstants.STAFF)) {
                        JaoharPreference.writeString(mActivity, JaoharPreference.STAFF_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(mActivity, JaoharPreference.STAFF_ID, mModel.getData().getId());
                        JaoharPreference.writeString(mActivity, JaoharPreference.STAFF_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, true);
                        Intent mIntent = new Intent(mActivity, VesselForSaleActivity.class);
                        mActivity.startActivity(mIntent);
                        finish();
                    } else if (strLoginType.equals(JaoharConstants.ADMIN) && mModel.getData().getRole().equals(JaoharConstants.ADMIN)) {
                        if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                            JaoharPreference.writeString(mActivity, JaoharPreference.ADMIN_EMAIL, mModel.getData().getEmail());
                            JaoharPreference.writeString(mActivity, JaoharPreference.ADMIN_ID, mModel.getData().getId());
                            JaoharPreference.writeString(mActivity, JaoharPreference.ADMIN_ROLE, mModel.getData().getRole());
                            JaoharPreference.writeBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_ADMIN, true);
                            Intent mIntent = new Intent(mActivity, ManagerActivity.class);
                            startActivity(mIntent);
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "Please enter staff credentials.");
                        }
                    } else if (mModel.getData().getRole().equals(JaoharConstants.OFFICE)) {
                        JaoharPreference.writeString(mActivity, JaoharPreference.STAFF_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(mActivity, JaoharPreference.STAFF_ID, mModel.getData().getId());
                        JaoharPreference.writeString(mActivity, JaoharPreference.STAFF_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, true);
                        Intent mIntent = new Intent(mActivity, HomeActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mIntent.putExtra(JaoharConstants.LOGIN, "VesselForSale");
                        mActivity.startActivity(mIntent);
                        finish();
                        mActivity.getFragmentManager().popBackStack();
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "Please enter user credentials.");
                    }
                } else {
                    AlertDialogManager.hideProgressDialog();
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
//                        editUserNameET.setText("");
//                        editPasswordET.setText("");
                }
            }

            @Override
            public void onFailure(Call<LoginModell> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

}
