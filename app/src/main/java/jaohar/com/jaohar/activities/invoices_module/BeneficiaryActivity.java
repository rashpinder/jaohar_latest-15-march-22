package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.invoices_module.BeneficiaryAdapter;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class BeneficiaryActivity extends BaseActivity {
    Activity mActivity = BeneficiaryActivity.this;
    String TAG = BeneficiaryActivity.this.getClass().getSimpleName();


    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.imgRightLL)
    RelativeLayout imgRightLL;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    @BindView(R.id.imgAdd)
    ImageView imgAdd;
    @BindView(R.id.benficiaryRV)
    RecyclerView benficiaryRV;
    ArrayList<BankModel> mArrayList = new ArrayList<BankModel>();
    //    ArrayList<BankModel> mArrayList = new ArrayList<BankModel>();
    ArrayList<BankModel> filteredList = new ArrayList<BankModel>();
    //    ArrayList<BankModel> filteredList = new ArrayList<BankModel>();
    BeneficiaryAdapter mBankDetailsAdapter;

//    DeleteBankDetails mDeleteBankDetails = new DeleteBankDetails() {
//        @Override
//        public void deleteBankDetail(BankModel mBankModel) {
//            deleteConfirmDialog(mBankModel);
//        }
//    };
//    EditBankDetails mEditBankDetails = new EditBankDetails() {
//        @Override
//        public void editBankDetails(BankModel mBankModel) {
//            Intent mIntent = new Intent(mActivity, EditBankDetailsActivity.class);
//            mIntent.putExtra("Model", String.valueOf(mBankModel));
//            mActivity.startActivity(mIntent);
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beneficiary);
        ButterKnife.bind(this);
        setAdapter();
//        if (Utilities.isNetworkAvailable(mActivity) == false) {
//            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
//        } else {
//            executeAPI();
//        }
    }



    @OnClick({R.id.llLeftLL, R.id.imgAdd, R.id.imgSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.imgAdd:
                performAddClick();
                break;
            case R.id.imgSearch:
                performSearchClick();
                break;
        }
    }


    private void performSearchClick() {
//        Intent intent = new Intent(mActivity, SearchInvoiceStampsActivity.class);
//        intent.putExtra("QuestionListExtra", mArrayList);
//        startActivityForResult(intent, 333);
//        overridePendingTransitionEnter();
    }

    private void performAddClick() {
        Intent intent = new Intent(mActivity, AddBeneficiaryActivity.class);
        startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (JaoharConstants.IS_BANK_DETAILS_EDIT)
//            executeAPI();
    }


    OnClickInterface mOnClickInterface = new OnClickInterface() {
        @Override
        public void mOnClickInterface(int position) {
            PerformOptionsClick(position);
        }
    };

    private void PerformOptionsClick(final int position) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_invoice_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout downloadRL = view.findViewById(R.id.downloadRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);
        View copyView = view.findViewById(R.id.copyView);
        View mailView = view.findViewById(R.id.mailView);
        View downloadView = view.findViewById(R.id.downloadView);
        copyRL.setVisibility(View.GONE);
        viewRL.setVisibility(View.VISIBLE);
        mailRL.setVisibility(View.GONE);
        downloadRL.setVisibility(View.GONE);
        copyRL.setVisibility(View.GONE);
        copyView.setVisibility(View.GONE);
        mailView.setVisibility(View.GONE);
        downloadView.setVisibility(View.GONE);



        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, EditBeneficiaryActivity.class);
                startActivity(intent);
                overridePendingTransitionEnter();
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    
//    protected void setViewsIDs() {
//        editSearchET = findViewById(R.id.editSearchET);
//        txtCenter = findViewById(R.id.txtCenter);
//        llLeftLL = findViewById(R.id.llLeftLL);
//        imgRightLL = findViewById(R.id.imgRightLL);
//        imgRightLL.setVisibility(View.VISIBLE);
//        imgBack = findViewById(R.id.imgBack);
//        imgBack.setImageResource(R.drawable.back);
//        benficiaryRV = findViewById(R.id.benficiaryRV);
//    }

//    protected void setClickListner() {
//        llLeftLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
//        imgRightLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent mIntent = new Intent(mActivity, AddBankDetailsActivity.class);
//                startActivity(mIntent);
//                overridePendingTransitionEnter();
//            }
//        });
//
//        editSearchET.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                charSequence = charSequence.toString().toLowerCase();
//                filteredList.clear();
//                for (int k = 0; k < mArrayList.size(); k++) {
//                    final String text = mArrayList.get(k).getBenificiary().toLowerCase();
//                    if (text.contains(charSequence)) {
//                        filteredList.add(mArrayList.get(k));
//                    }
//                }
//                benficiaryRV.setLayoutManager(new LinearLayoutManager(mActivity));
//                mBankDetailsAdapter = new BeneficiaryAdapter(mActivity, filteredList, mDeleteBankDetails, mEditBankDetails);
//                benficiaryRV.setAdapter(mBankDetailsAdapter);
//                mBankDetailsAdapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//    }
//        });
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private void setAdapter() {
        Log.e(TAG, "Stamps: " + mArrayList.size());
        benficiaryRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mBankDetailsAdapter = new BeneficiaryAdapter(mActivity,mOnClickInterface);
        benficiaryRV.setAdapter(mBankDetailsAdapter);
    }

    private void executeAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllBankDetailsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "******Response*****" + response);
                JaoharConstants.IS_BANK_DETAILS_EDIT = false;
                AlertDialogManager.hideProgressDialog();
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeAPI() {
//        String strUrl = JaoharConstants.GET_ALL_BANK_DETAILS + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                JaoharConstants.IS_BANK_DETAILS_EDIT = false;
//                AlertDialogManager.hideProgressDialog();
//                parseResponse(response);
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void parseResponse(String response) {
        mArrayList.clear();
        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    BankModel mModel = new BankModel();
                    mModel.setId(mJson.getString("bank_id"));
                    mModel.setBenificiary(mJson.getString("beneficiary"));
                    mModel.setBankName(mJson.getString("bank_name"));
                    mModel.setAddress1(mJson.getString("address1"));
                    mModel.setAddress2(mJson.getString("address2"));
                    mModel.setIbanRON(mJson.getString("iban_ron"));
                    mModel.setIbanUSD(mJson.getString("iban_usd"));
                    mModel.setIbanEUR(mJson.getString("iban_eur"));
                    mModel.setIban_gbp(mJson.getString("iban_gbp"));
                    mModel.setSwift(mJson.getString("swift"));

                    mArrayList.add(mModel);
                }

                /*SetAdapter*/
                setAdapter();

            } else if (mJsonObject.getString("status").equals("100")) {
                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteConfirmDialog(final BankModel mBankModel) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_bank_details));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mBankModel);
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }

    /*Execute DeleteUser API*/
    private void executeDeleteAPI(BankModel mBankModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteBankDetailsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),mBankModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    executeAPI();
                } else if (mModel.getStatus()==100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
                else{
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());

                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }



//    public void executeDeleteAPI(BankModel mBankModel) {
//        String strUrl = JaoharConstants.DELETE_BANK_DETAILS + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&bank_id=" + mBankModel.getBankId();
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        //  Toast.makeText(mActivity, "" + mJsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//                        executeAPI();
//                    } else if (mJsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

}
