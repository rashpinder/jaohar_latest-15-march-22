package jaohar.com.jaohar.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import jaohar.com.jaohar.R;

public class OpenDynamicLinksActivity extends AppCompatActivity {
    //Toolbar
    public LinearLayout llLeftLL;
    public RelativeLayout imgRightLL;
    public TextView txtCenter;
    public ImageView imgRight, imgBack;
    public WebView mWebView;
    Activity mActivity = OpenDynamicLinksActivity.this;
    String TAG = OpenDynamicLinksActivity.this.getClass().getSimpleName();
    ProgressBar circlePB;

    String strTitle = "";
    String strLink = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_dynamic_links);

        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.GONE);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        DrawableCompat.setTint(imgBack.getDrawable(), ContextCompat.getColor(mActivity, R.color.white));
//        imgBack.setColorFilter(mActivity.getResources().getColor(R.color.white));
        mWebView = (WebView) findViewById(R.id.mWebView);
        circlePB = (ProgressBar) findViewById(R.id.circlePB);

        if (getIntent() != null) {
            strTitle = getIntent().getStringExtra("TITLE");
            strLink = getIntent().getStringExtra("LINK");

            txtCenter.setText(strTitle);
            setLinks();
        }

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setLinks() {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                circlePB.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                circlePB.setVisibility(View.GONE);
            }
        });
        mWebView.loadUrl(strLink);
    }
}
