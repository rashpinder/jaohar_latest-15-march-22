package jaohar.com.jaohar.activities.manager_module;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.NewRequestAdapter;
import jaohar.com.jaohar.interfaces.ConfirmRejectRequestInterface;
import jaohar.com.jaohar.models.NewReqData;
import jaohar.com.jaohar.models.SearchNewReqModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class SearchNewRequestsActivity extends AppCompatActivity {
    Activity mActivity = SearchNewRequestsActivity.this;
    String TAG = SearchNewRequestsActivity.this.getClass().getSimpleName();

    //WIDGETS
    RecyclerView mRecyclerView;
    EditText editSearchET;
    TextView cancelTV;

    boolean isNormalSearch = false;
//    ArrayList<NewRequestModel> mArrayList = new ArrayList<>();
    ArrayList<NewReqData> mArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_search_new_requests);

        /* initialize view ids */
        setWidgetsIds();
    }

    private void setWidgetsIds() {
        editSearchET = findViewById(R.id.editSearchET);
        cancelTV = findViewById(R.id.cancelTV);
        mRecyclerView = findViewById(R.id.mRecyclerView);

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event == null || event.getAction() != KeyEvent.ACTION_DOWN) {
                    //do something
                    executeNormalSearch();
                }
                return false;
            }
        });

        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setAdapter() {
        Log.e(TAG, "VesselsAdapter: " + mArrayList.size());
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        NewRequestAdapter mNewRequestAdapter = new NewRequestAdapter(mActivity, mArrayList, mConfirmRequestInterface);
        mRecyclerView.setAdapter(mNewRequestAdapter);
    }

    ConfirmRejectRequestInterface mConfirmRequestInterface = new ConfirmRejectRequestInterface() {
        @Override
        public void getConfirmRequest(NewReqData mNewRequestModel, String strType) {
            showAlertDialogConfirmCancel(mActivity, mNewRequestModel, strType);
        }
    };

    public void showAlertDialogConfirmCancel(Activity mActivity, final NewReqData mNewRequestModel, final String strType) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_confirm_cancel);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtConfirmTV = (TextView) alertDialog.findViewById(R.id.txtConfirmTV);
        TextView txtCancelTV = (TextView) alertDialog.findViewById(R.id.txtCancelTV);

        txtTitle.setText(getString(R.string.app_name));
        if (strType.equals("confirm")) {
            txtMessage.setText(getString(R.string.are_you_sure_accept));
        } else if (strType.equals("reject")) {
            txtMessage.setText(getString(R.string.are_you_sure_reject));
        }

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txtConfirmTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                executeConfirmRequestAPI(mNewRequestModel, strType);
            }
        });
        alertDialog.show();
    }

    /*Cofirm The Request*/
//    public void executeConfirmRequestAPI(NewReqData mNewRequestModel, String strType) {
//        String strUrl = "";
//        if (strType.equals("confirm")) {
//            strUrl = JaoharConstants.ACCEPT_MEMBER_REQUEST + "?user_id=" + mNewRequestModel.getId();
//        } else if (strType.equals("reject")) {
//            strUrl = JaoharConstants.REJECT_MEMBER_REQUEST + "?user_id=" + mNewRequestModel.getId();
//        }
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);//{"status":"1","message":"Account enabled successfully"}
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else if (mJsonObject.getString("status").equals("0")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void executeAcceptMemberRequestApi(NewReqData mNewRequestModel, String strType) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.acceptMemberRequest(mNewRequestModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus()==0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}


    private void executeRejectMemberRequestApi(NewReqData mNewRequestModel, String strType) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.rejectMemberRequest(mNewRequestModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showAlertDialogOKDONE(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus()==0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );
    }



    /*Cofirm The Request*/
    public void executeConfirmRequestAPI(NewReqData mNewRequestModel, String strType) {
//        String strUrl = "";
        if (strType.equals("confirm")) {
            executeAcceptMemberRequestApi(mNewRequestModel, strType);
//            strUrl = JaoharConstants.ACCEPT_MEMBER_REQUEST + "?user_id=" + mNewRequestModel.getId();
        } else if (strType.equals("reject")) {
            executeRejectMemberRequestApi(mNewRequestModel,strType);
//            strUrl = JaoharConstants.REJECT_MEMBER_REQUEST + "?user_id=" + mNewRequestModel.getId();
        } }


    public void showAlertDialogOKDONE(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                executeNormalSearch();
            }
        });
        alertDialog.show();
    }

    private void executeNormalSearch() {
        mArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<SearchNewReqModel> call1 = mApiInterface.searchNewRequest(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""),editSearchET.getText().toString().trim());
        call1.enqueue(new Callback<SearchNewReqModel>() {
            @Override
            public void onResponse(Call<SearchNewReqModel> call, retrofit2.Response<SearchNewReqModel> response) {
                AlertDialogManager.hideProgressDialog();
                editSearchET.setText("");
                isNormalSearch = true;
                Log.e(TAG, "*****Response****" + response);
                SearchNewReqModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    mArrayList=mModel.getData();
                //*Set Adapter*//*
                setAdapter();
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SearchNewReqModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}


//    public void executeNormalSearch() {
//        mArrayList.clear();
//        String strUrl = JaoharConstants.SearchNewRequest + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&input_search_value=" + editSearchET.getText().toString().trim();
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                editSearchET.setText("");
//                isNormalSearch = true;
//                Log.e(TAG, "*****Response****" + response);
//
//                parseResponse(response);
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void parseResponse(String response) {
//        try {
//            JSONObject mJsonObject = new JSONObject(response);
//            if (mJsonObject.getString("status").equals("1")) {
//                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
//                for (int i = 0; i < mJsonArray.length(); i++) {
//                    JSONObject mJson = mJsonArray.getJSONObject(i);
//                    NewRequestModel mModel = new NewRequestModel();
//                    if (!mJson.isNull("id")) {
//                        mModel.setId(mJson.getString("id"));
//                    }
//                    if (!mJson.isNull("email")) {
//                        mModel.setEmail(mJson.getString("email"));
//                    }
//                    if (!mJson.isNull("password")) {
//                        mModel.setPassword(mJson.getString("password"));
//                    }
//                    if (!mJson.isNull("role")) {
//                        mModel.setRole(mJson.getString("role"));
//                    }
//                    if (!mJson.isNull("status")) {
//                        mModel.setStatus(mJson.getString("status"));
//                    }
//                    if (!mJson.isNull("created")) {
//                        mModel.setCreated(mJson.getString("created"));
//                    }
//                    if (!mJson.isNull("enabled")) {
//                        mModel.setEnabled(mJson.getString("enabled"));
//                    }
//
//                    mArrayList.add(mModel);
//                }
//
//                //*Set Adapter*//*
//                setAdapter();
//
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mJsonObject.getString("message"));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
