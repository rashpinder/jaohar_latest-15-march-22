package jaohar.com.jaohar.activities.universal_invoice_module;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.j4velin.lib.colorpicker.ColorPickerDialog;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.TemplatesAdapter;
import jaohar.com.jaohar.interfaces.TemplatesInterFace;
import jaohar.com.jaohar.models.GetAllInvoiceTemplate;
import jaohar.com.jaohar.models.InvoiceDataModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AddInvoicingCompanyActivity extends BaseActivity {
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;
    Activity mActivity = AddInvoicingCompanyActivity.this;
    String TAG = AddInvoicingCompanyActivity.this.getClass().getSimpleName();
    LinearLayout llLeftLL, headingsColorLL, contentColorLL, table_headColorLL, logoTEXTColorLL, table_alterColorLL, table_border_and_gradient_ColorLL;
    ImageView imgBack, mImage, itemDeleteIV, image2, itemImagedelete;
    private TextView txtCenter, headerColorTV, logoTEXTColorTV, contentColorTV, table_headColorTV, table_alterColorTV, table_border_and_gradient_ColorTV;
    private RecyclerView templatesRV;
    private Button add_BTN;

    private TemplatesAdapter mAdapter;
    //    private ArrayList<TemplatesInvoicModel> mArrayLIST = new ArrayList<>();
    private ArrayList<InvoiceDataModel> mArrayLIST = new ArrayList<>();
    private final String cameraStr = Manifest.permission.CAMERA;
    private final String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE;
    private String mCurrentPhotoPath;
    private String mStoragePath;
    private String strBase64;

    Bitmap rotate = null;
    Boolean IsIMAGE1 = false;
    Boolean IsIMAGE2 = false;
    private String strBase64IMAGE1 = "", strBase64IMAGE2 = "";
    private EditText editCompanyNameET, editInvoicePrefixET, editOwnerEmailET, editAddresLineOneET, editAddresLineTwoET, editAddresLineThreeET, editRegistryNumberET, editFiscalCodeET, editIMONoET, editPhoneNumberET, editlogoTextET, editSignatureET;
    /**
     * Persist URI image to crop URI if specific permissions are required
     */
    private Uri mCropImageUri;
    Uri uri;

    TemplatesInterFace mTemPlateInterFace = new TemplatesInterFace() {
        @Override
        public void mTemplatesInterface(InvoiceDataModel mModel) {
            headerColorTV.setText(mModel.getHeadingColor());
            headingsColorLL.setBackgroundColor(Color.parseColor(mModel.getHeadingColor()));
            contentColorTV.setText(mModel.getContentColor());
            contentColorLL.setBackgroundColor(Color.parseColor(mModel.getContentColor()));
            table_headColorTV.setText(mModel.getTableHeadColor());
            table_headColorLL.setBackgroundColor(Color.parseColor(mModel.getTableHeadColor()));
            table_alterColorTV.setText(mModel.getTableRowColor());
            table_alterColorLL.setBackgroundColor(Color.parseColor(mModel.getTableRowColor()));
            table_border_and_gradient_ColorTV.setText(mModel.getBorderGradientColor());
            table_border_and_gradient_ColorLL.setBackgroundColor(Color.parseColor(mModel.getBorderGradientColor()));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_invoicing_company);

    }


    @Override
    protected void setViewsIDs() {
        add_BTN = findViewById(R.id.add_BTN);
        templatesRV = findViewById(R.id.templatesRV);
        llLeftLL = findViewById(R.id.llLeftLL);
        headingsColorLL = findViewById(R.id.headingsColorLL);
        logoTEXTColorLL = findViewById(R.id.logoTEXTColorLL);
        table_border_and_gradient_ColorLL = findViewById(R.id.table_border_and_gradient_ColorLL);
        contentColorLL = findViewById(R.id.contentColorLL);
        table_headColorLL = findViewById(R.id.table_headColorLL);
        itemDeleteIV = findViewById(R.id.itemDeleteIV);
        itemImagedelete = findViewById(R.id.itemImagedelete);
        table_alterColorLL = findViewById(R.id.table_alterColorLL);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = findViewById(R.id.txtCenter);
        logoTEXTColorTV = findViewById(R.id.logoTEXTColorTV);
        headerColorTV = findViewById(R.id.headerColorTV);
        contentColorTV = findViewById(R.id.contentColorTV);
        table_headColorTV = findViewById(R.id.table_headColorTV);
        table_alterColorTV = findViewById(R.id.table_alterColorTV);
        table_border_and_gradient_ColorTV = findViewById(R.id.table_border_and_gradient_ColorTV);
        txtCenter.setText(getResources().getString(R.string.add_company_));
        mImage = findViewById(R.id.mImage);
        image2 = findViewById(R.id.image2);
        editCompanyNameET = findViewById(R.id.editCompanyNameET);
        editInvoicePrefixET = findViewById(R.id.editInvoicePrefixET);
        editOwnerEmailET = findViewById(R.id.editOwnerEmailET);
        editAddresLineOneET = findViewById(R.id.editAddresLineOneET);
        editAddresLineTwoET = findViewById(R.id.editAddresLineTwoET);
        editAddresLineThreeET = findViewById(R.id.editAddresLineThreeET);
        editRegistryNumberET = findViewById(R.id.editRegistryNumberET);
        editFiscalCodeET = findViewById(R.id.editFiscalCodeET);
        editIMONoET = findViewById(R.id.editIMONoET);
        editPhoneNumberET = findViewById(R.id.editPhoneNumberET);
        editlogoTextET = findViewById(R.id.editlogoTextET);
        editSignatureET = findViewById(R.id.editSignatureET);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute  API*//*
            gettingImage();
        }
    }

    public void gettingImage() {
        mArrayLIST.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllInvoiceTemplate> call1 = mApiInterface.getAllInvoiceTemplatesRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<GetAllInvoiceTemplate>() {
            @Override
            public void onResponse(Call<GetAllInvoiceTemplate> call, retrofit2.Response<GetAllInvoiceTemplate> response) {
                Log.e(TAG, "******Response*****" + response);
                AlertDialogManager.hideProgressDialog();
                GetAllInvoiceTemplate mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    mArrayLIST = mModel.getData();
                    setAdapter();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetAllInvoiceTemplate> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


//    private void gettingImage() {
//        mArrayLIST.clear();
//        //    https://root.jaohar.com/Staging/JaoharWebServicesNew/GetAllInvoiceTemplates.php//user_id
//        String strUrl = JaoharConstants.GetAllInvoiceTemplates + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//
//
//                Log.e(TAG, "*****ResponseALL****" + response);
//                parseResponse(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void parseResponse(String response) {
//
//        try {
//            JSONObject mJsonOBJ = new JSONObject(response);
//            String strStatus = mJsonOBJ.getString("status");
//            if (strStatus.equals("1")) {
//                JSONArray mjsonArrayData = mJsonOBJ.getJSONArray("data");
//                if (mjsonArrayData != null) {
//                    for (int i = 0; i < mjsonArrayData.length(); i++) {
//                        JSONObject mJSonDATA = mjsonArrayData.getJSONObject(i);
//                        TemplatesInvoicModel mModel = new TemplatesInvoicModel();
//                        if (!mJSonDATA.getString("template_id").equals("")) {
//                            mModel.setTemplate_id(mJSonDATA.getString("template_id"));
//                        }
//                        if (!mJSonDATA.getString("template_image").equals("")) {
//                            mModel.setTemplate_image(mJSonDATA.getString("template_image"));
//                        }
//                        if (!mJSonDATA.getString("heading_color").equals("")) {
//                            mModel.setHeading_color(mJSonDATA.getString("heading_color"));
//                        }
//                        if (!mJSonDATA.getString("content_color").equals("")) {
//                            mModel.setContent_color(mJSonDATA.getString("content_color"));
//                        }
//                        if (!mJSonDATA.getString("table_head_color").equals("")) {
//                            mModel.setTable_head_color(mJSonDATA.getString("table_head_color"));
//                        }
//                        if (!mJSonDATA.getString("table_row_color").equals("")) {
//                            mModel.setTable_row_color(mJSonDATA.getString("table_row_color"));
//                        }
//                        if (!mJSonDATA.getString("border_gradient_color").equals("")) {
//                            mModel.setBorder_gradient_color(mJSonDATA.getString("border_gradient_color"));
//                        }
//                        mArrayLIST.add(mModel);
//
//                    }
//
//                    setAdapter();
//                }
//
//
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonOBJ.getString("message"));
//
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    private void setAdapter() {
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        templatesRV.setLayoutManager(horizontalLayoutManager);
        mAdapter = new TemplatesAdapter(mActivity, mArrayLIST, mTemPlateInterFace);
        templatesRV.setAdapter(mAdapter);
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        add_BTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mActivity, "Comming Soon ...", Toast.LENGTH_LONG).show();

            }
        });
        mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    IsIMAGE1 = true;
                    IsIMAGE2 = false;
                    onSelectImageClick();
                } else {
                    IsIMAGE1 = true;
                    IsIMAGE2 = false;
                    requestPermission();
                }


            }
        });
        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    IsIMAGE2 = true;
                    IsIMAGE1 = false;
                    onSelectImageClick();
                } else {
                    IsIMAGE2 = true;
                    IsIMAGE1 = false;
                    requestPermission();
                }


            }
        });

        itemDeleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImage.setImageResource(R.drawable.palace_holder);
                itemDeleteIV.setVisibility(View.GONE);
                IsIMAGE1 = false;
                strBase64 = "";
                rotate = null;
            }
        });
        itemImagedelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image2.setImageResource(R.drawable.palace_holder);
                itemImagedelete.setVisibility(View.GONE);
                IsIMAGE2 = false;
                strBase64 = "";
                rotate = null;
            }
        });

        headingsColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int headerColor = Color.parseColor(headerColorTV.getText().toString());
                getColorCode(headingsColorLL, headerColorTV, headerColor);
            }
        });
        contentColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int contentColor = Color.parseColor(contentColorTV.getText().toString());
                getColorCode(contentColorLL, contentColorTV, contentColor);
            }
        });
        table_headColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tableHeaderColor = Color.parseColor(table_headColorTV.getText().toString());
                getColorCode(table_headColorLL, table_headColorTV, tableHeaderColor);
            }
        });

        table_alterColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tableAlterColor = Color.parseColor(table_alterColorTV.getText().toString());
                getColorCode(table_alterColorLL, table_alterColorTV, tableAlterColor);
            }
        });

        table_border_and_gradient_ColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tableBorderColor = Color.parseColor(table_border_and_gradient_ColorTV.getText().toString());

                getColorCode(table_border_and_gradient_ColorLL, table_border_and_gradient_ColorTV, tableBorderColor);
            }
        });
        logoTEXTColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getColorCode(logoTEXTColorLL, logoTEXTColorTV, Color.BLACK);
            }
        });


        add_BTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (headerColorTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_select_templates));

                } else if (editCompanyNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_company_name));
                } else if (editInvoicePrefixET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_enter_invoice_prefix));
                } else if (editlogoTextET.getText().toString().equals("") && strBase64IMAGE1.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_add_logo_text_or_logo_image));
                } else {
                    if (!editOwnerEmailET.getText().toString().equals("")) {
                        if (Utilities.isValidEmaillId(editOwnerEmailET.getText().toString()) == false) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                        } else {
                            /*Execute API to Add Company*/
                            if (!Utilities.isNetworkAvailable(mActivity)) {
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                            } else {
                                /*ExecuteApi*/
                                executeAddAPI();
                            }
                        }
                    } else {
                        /*Execute API to Add Company*/
                        if (!Utilities.isNetworkAvailable(mActivity)) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            /*ExecuteApi*/
                            executeAddAPI();
                        }
                    }

                }
            }
        });
    }

    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick() {
        CropImage.startPickImageActivity(this);
    }


    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
//                .setGuidelines(CropImageView.Guidelines.OFF).setAspectRatio(1,1)
//                .setFixAspectRatio(true)

                .setMultiTouchEnabled(false)
                .start(this);
    }


    /*********
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/

    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }


    void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                            onSelectImageClick();
//                            startCropImageActivity(mCropImageUri);
//                            startActivity(new Intent(SplashSlidesActivity.this, SelectionActivity.class));
//                            finish();
//                            Toast.makeText(context,"on",Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            requestPermission();
//                            permissionAccepted = false;
                        }
                    }
                }
                break;
        }
    }

    private void getColorCode(final LinearLayout colorLL, final TextView colorTV, int color) {
        ColorPickerDialog dialog = new ColorPickerDialog(mActivity, color);
        dialog.setOnColorChangedListener(new ColorPickerDialog.OnColorChangedListener() {
            @Override
            public void onColorChanged(int color) {
                // apply new color
                String hexColor = String.format("#%06X", (0xFFFFFF & color));
                colorTV.setText(hexColor);
                colorLL.setBackgroundColor(color);

            }

        });
        dialog.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                if (IsIMAGE1 == true) {
                    Bitmap bitmap = null;
                    try {

                        final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//                        String encodedImage = encodeImage(selectedImage);

                        //                    mImage.setImageURI(result.getUri());
                        mImage.setImageBitmap(selectedImage);
                        strBase64IMAGE1 = getBase64String(selectedImage);
                        itemDeleteIV.setVisibility(View.GONE);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } else if (IsIMAGE2 == true) {
                    try {
                        final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        image2.setImageBitmap(selectedImage);
                        itemImagedelete.setVisibility(View.GONE);
                        strBase64IMAGE2 = getBase64String(selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    //    https://root.jaohar.com/Staging/JaoharWebServicesNew/AddOwner.php

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("owner_name", editCompanyNameET.getText().toString().trim());
        mMap.put("email", editOwnerEmailET.getText().toString().trim());
        mMap.put("address1", editAddresLineOneET.getText().toString().trim());
        mMap.put("address2", editAddresLineTwoET.getText().toString().trim());
        mMap.put("address3", editAddresLineThreeET.getText().toString().trim());
        mMap.put("registry_no", editRegistryNumberET.getText().toString().trim());
        mMap.put("fiscal_code", editFiscalCodeET.getText().toString().trim());
        mMap.put("logo", strBase64IMAGE1);
        mMap.put("footer", strBase64IMAGE2);
        mMap.put("color", contentColorTV.getText().toString().trim());
        mMap.put("head_color", headerColorTV.getText().toString().trim());
        mMap.put("table_head_color", table_headColorTV.getText().toString().trim());
        mMap.put("table_row_color", table_alterColorTV.getText().toString().trim());
        mMap.put("gradient_color", table_border_and_gradient_ColorTV.getText().toString().trim());
        mMap.put("prefix", editInvoicePrefixET.getText().toString().trim());
        mMap.put("imo_no", editIMONoET.getText().toString().trim());
        mMap.put("phone", editPhoneNumberET.getText().toString().trim());
        mMap.put("signature", editSignatureET.getText().toString().trim());
        mMap.put("logo_text", editlogoTextET.getText().toString().trim());
        mMap.put("logo_text_color", logoTEXTColorTV.getText().toString().trim());
        mMap.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAddAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addOwnerRequest(mParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mAlerDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


//    public void executeAddAPI() {
//        AlertDialogManager.showProgressDialog(mActivity);
//        String url = JaoharConstants.Add_Owner;
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("owner_name", editCompanyNameET.getText().toString().trim());
//            jsonObject.put("email", editOwnerEmailET.getText().toString().trim());
//            jsonObject.put("address1", editAddresLineOneET.getText().toString().trim());
//            jsonObject.put("address2", editAddresLineTwoET.getText().toString().trim());
//            jsonObject.put("address3", editAddresLineThreeET.getText().toString().trim());
//            jsonObject.put("registry_no", editRegistryNumberET.getText().toString().trim());
//            jsonObject.put("fiscal_code", editFiscalCodeET.getText().toString().trim());
//            jsonObject.put("logo", strBase64IMAGE1);
//            jsonObject.put("footer", strBase64IMAGE2);
//            jsonObject.put("color", contentColorTV.getText().toString().trim());
//            jsonObject.put("head_color", headerColorTV.getText().toString().trim());
//            jsonObject.put("table_head_color", table_headColorTV.getText().toString().trim());
//            jsonObject.put("table_row_color", table_alterColorTV.getText().toString().trim());
//            jsonObject.put("gradient_color", table_border_and_gradient_ColorTV.getText().toString().trim());
//            jsonObject.put("prefix", editInvoicePrefixET.getText().toString().trim());
//            jsonObject.put("imo_no", editIMONoET.getText().toString().trim());
//            jsonObject.put("phone", editPhoneNumberET.getText().toString().trim());
//            jsonObject.put("signature", editSignatureET.getText().toString().trim());
//            jsonObject.put("logo_text", editlogoTextET.getText().toString().trim());
//            jsonObject.put("logo_text_color", logoTEXTColorTV.getText().toString().trim());
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                url, jsonObject,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        AlertDialogManager.hideProgressDialog();
//                        Log.d(TAG, response.toString());
//                        try {
//                            if (response.getString("status").equals("1")) {
//                                mAlerDialog(mActivity, getString(R.string.app_name), response.getString("message"));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                VolleyLog.d(TAG, "Error: " + error.getMessage());
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
////      Adding request to request queue
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjReq);
//    }

    public void mAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();

        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return base64String;
    }
}
