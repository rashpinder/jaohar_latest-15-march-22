package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.ContactByIdModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class ViewCompanyActivity extends BaseActivity {
    private final Activity mActivity = ViewCompanyActivity.this;
    /**
     * Widgets
     */
    @BindView(R.id.txtCompanyNameTV)
    TextView txtCompanyNameTV;
    @BindView(R.id.txtAddress1TV)
    TextView txtAddress1TV;
    @BindView(R.id.txtAddress2TV)
    TextView txtAddress2TV;
    @BindView(R.id.txtAddress3TV)
    TextView txtAddress3TV;
    @BindView(R.id.txtAddress4TV)
    TextView txtAddress4TV;
    @BindView(R.id.txtAddress5TV)
    TextView txtAddress5TV;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    private String strContactID = "", strUSERID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_company);
        ButterKnife.bind(this);
        if (JaoharConstants.is_Staff_FragmentClick == true) {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
        }
    }


    @OnClick({R.id.llLeftLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (Utilities.isNetworkAvailable(mActivity) == false) {
//            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
//        } else {
//            executeAPIforGetting();
//        }
    }


    private void executeAPIforGetting() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ContactByIdModel> call1 = mApiInterface.getAddressBookByRequest(strUSERID, strContactID);
        call1.enqueue(new Callback<ContactByIdModel>() {
            @Override
            public void onResponse(Call<ContactByIdModel> call, retrofit2.Response<ContactByIdModel> response) {
                AlertDialogManager.hideProgressDialog();
                ContactByIdModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    txtCompanyNameTV.setText(mModel.getData().getName());
                    txtAddress1TV.setText(mModel.getData().getEmail1());
                    txtAddress2TV.setText(mModel.getData().getEmail2());
                    txtAddress3TV.setText(mModel.getData().getEmail3());
                    txtAddress4TV.setText(mModel.getData().getPhone1());
                    txtAddress5TV.setText(mModel.getData().getPhone2());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ContactByIdModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

}
