package jaohar.com.jaohar.activities.forum_module_admin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.forum_module_admin_adapter.ClosedForumListAdapter;
import jaohar.com.jaohar.beans.ForumModule.ForumModel;
import jaohar.com.jaohar.interfaces.forumModule.ForumItemClickInterace;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.interfaces.forum_module_admin.CloseAndDeleteForumInterface;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.LinearLayoutManagerWrapper;
import jaohar.com.jaohar.utils.Utilities;

import static android.view.View.GONE;

public class ClosedForumListActivity extends BaseActivity {
    /*
     * set Activity
     * */
    Activity mActivity = ClosedForumListActivity.this;
    /*
     * set Activity TAG
     * */
    String TAG = ClosedForumListActivity.this.getClass().getSimpleName();
    /**
     * Widgets
     */
    ProgressBar progressBottomPB;
    RecyclerView dataRV;
    SwipeRefreshLayout swipeToRefresh;
    ImageView imgBack, imgRight;
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    TextView txtCenter;
    int page_no = 1;
    String strLastPage = "TRUE";
    boolean isSwipeRefresh = false;
    String strForumId = "";

    /*
     * Setting Up Array List
     * */
    ArrayList<ForumModel> mLoadMore = new ArrayList<>();
    ArrayList<ForumModel> modelArrayList = new ArrayList<>();

    /*
     * Setting Up Adapter
     * */
    ClosedForumListAdapter mAdapter;


    /* *
     * Setting Up Interface For selecting Items
     * */
    ForumItemClickInterace mInterfaceForum = new ForumItemClickInterace() {
        @Override
        public void ForumItemClick(ForumModel mModel) {
            Intent mIntent = new Intent(mActivity, ChatScreenAdminActivity.class);
            mIntent.putExtra("forum_id", mModel.getId());
            mIntent.putExtra("type", "closedForum");
            mActivity.startActivity(mIntent);
        }
    };


    /* *
     * Setting Up Interface For Deleting and Close the Forum Items
     * */
    CloseAndDeleteForumInterface mCloseAndDelForum = new CloseAndDeleteForumInterface() {
        @Override
        public void mCloseAndDeleteInterFace(ForumModel mModel, String strType) {
            strForumId =mModel.getId();
            if (strType.equals("delete")) {

                deleteConfirmDialog("Are you sure want to delete this forum?", "close");
            }
        }
    };



    /**
     * Recycler View Pagination Adapter Interface
     **/
    PaginationListForumAdapter mPaginationInterFace = new PaginationListForumAdapter() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            progressBottomPB.setVisibility(View.VISIBLE);
                            ++page_no;
                            mLoadMore.clear();
                            executeAPI();
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                        }
                    }
                }, 1500);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_closed_forum_list);

    }


    @Override
    protected void setViewsIDs() {
        progressBottomPB = findViewById(R.id.progressBottomPB);
        dataRV = findViewById(R.id.dataRV);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRight = findViewById(R.id.imgRight);
        imgRight.setVisibility(GONE);
        imgRightLL.setVisibility(GONE);
        imgRight.setImageDrawable(getResources().getDrawable(R.drawable.add_icon));
        txtCenter.setText(getResources().getText(R.string.closed_forum));
        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                modelArrayList.clear();
                mLoadMore.clear();
                page_no = 1;
                executeAPI();
            }
        });
        setAdapter();
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute API For Getting List *//*
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(GONE);
            }
            modelArrayList.clear();
            page_no = 1;
            isSwipeRefresh = false;
            swipeToRefresh.setRefreshing(false);
            mLoadMore.clear();
            executeAPI();
        }
    }


    /* *
     * Execute API for getting ClosedForum list
     * @param
     * @user_id
     * @page_no
     * */
    public void executeAPI() {
        mLoadMore.clear();
        if (strLastPage.equals("FALSE")) {
            if (isSwipeRefresh == true) {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }else {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if (page_no == 1) {
                if (isSwipeRefresh == true) {
                    if (progressBottomPB != null) {
                        progressBottomPB.setVisibility(GONE);
                    }
                }else {
                    AlertDialogManager.showProgressDialog(mActivity);
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                    if (progressBottomPB != null) {
                        progressBottomPB.setVisibility(GONE);
                    }
                }


            }
        }
        String strUrl = JaoharConstants.GetAllClosedStaffForums + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&page_no=" + page_no;
        Log.e(TAG, "***URLTESTING***" + strUrl);
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                if (isSwipeRefresh == true) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }
                parseResponce(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    void parseResponce(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            String strStatus = mJSonObject.getString("status");
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_forums");
                if (mjsonArrayData != null) {
                    ArrayList<ForumModel> mTempraryList = new ArrayList<>();
                    mTempraryList.clear();
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                       JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        ForumModel mModel = new ForumModel();
                        if (!mJsonDATA.getString("id").equals("")) {
                            mModel.setId(mJsonDATA.getString("id"));
                        }
                        if (!mJsonDATA.getString("vessel_id").equals("")) {
                            mModel.setVessel_id(mJsonDATA.getString("vessel_id"));
                        }
                        if (!mJsonDATA.getString("user_id").equals("")) {
                            mModel.setUser_id(mJsonDATA.getString("user_id"));
                        }
                        if (!mJsonDATA.getString("creation_date").equals("")) {
                            mModel.setCreation_date(mJsonDATA.getString("creation_date"));
                        }
                        if (!mJsonDATA.getString("disable").equals("")) {
                            mModel.setDisable(mJsonDATA.getString("disable"));
                        }
                        if (!mJsonDATA.getString("vessel_record_id").equals("")) {
                            mModel.setVessel_record_id(mJsonDATA.getString("vessel_record_id"));
                        }
                        if (!mJsonDATA.getString("vessel_name").equals("")) {
                            mModel.setVessel_name(mJsonDATA.getString("vessel_name"));
                        }
                        if (!mJsonDATA.getString("IMO_number").equals("")) {
                            mModel.setIMO_number(mJsonDATA.getString("IMO_number"));
                        }
                        if (!mJsonDATA.getString("status").equals("")) {
                            mModel.setStatus(mJsonDATA.getString("status"));
                        }
                        if (!mJsonDATA.getString("pic").equals("")) {
                            mModel.setPic(mJsonDATA.getString("pic"));
                        }
                        if (!mJsonDATA.getString("total_messages").equals("")) {
                            mModel.setTotal_messages(mJsonDATA.getString("total_messages"));
                        }
                        if (!mJsonDATA.getString("unread_messages").equals("")) {
                            mModel.setUnread_messages(mJsonDATA.getString("unread_messages"));
                        }


                            mTempraryList.add( mModel);

                    }

                        modelArrayList.addAll(mTempraryList);

//                    if (page_no == 1) {
//
//                    } else {
                       mAdapter.notifyDataSetChanged();
//                    }
//                    setAdapter();
                }

            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Setting Up Adapter
     **/
    private void setAdapter() {
//        if(page_no!=1){
//            Collections.reverse(modelArrayList);
//        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManagerWrapper(mActivity, LinearLayoutManager.VERTICAL, false);

//      dataRV.setLayoutManager(new LinearLayoutManager(mActivity));
        dataRV.setLayoutManager(mLayoutManager);

        mAdapter = new ClosedForumListAdapter(mActivity, modelArrayList, mPaginationInterFace, mInterfaceForum,mCloseAndDelForum);
        mAdapter.setHasStableIds(true);
        dataRV.setAdapter(mAdapter);

    }

    public void deleteConfirmDialog(String strMessage, final String strType) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();

                /*Execute Delete API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeDeleteForumAPI();
                    }

            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }


    /* *
     * Execute API for  DeleteStaffForum
     * @param
     * @forum_id
     * */
    public void executeDeleteForumAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        String strUrl = JaoharConstants.DeleteStaffForum + "?forum_id=" + strForumId;
        Log.e(TAG, "***URL***" + strUrl);
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                try {
                    JSONObject mJsonData = new JSONObject(response);
                    if(mJsonData.getString("status").equals("1")){
                        modelArrayList.clear();
                        mLoadMore.clear();
                        page_no=1;
                        executeAPI();

                    }else {
                        AlertDialogManager.showAlertDialog(mActivity,getString(R.string.app_name),mJsonData.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}

