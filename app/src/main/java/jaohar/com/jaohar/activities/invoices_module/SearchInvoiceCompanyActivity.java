package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.EditCompanyActivity;
import jaohar.com.jaohar.adapters.CompaniesAdapter;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.interfaces.DeleteCompanyInterface;
import jaohar.com.jaohar.interfaces.EditCompanyInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class SearchInvoiceCompanyActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = SearchInvoiceCompanyActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = SearchInvoiceCompanyActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.cancelTV)
    TextView cancelTV;
    @BindView(R.id.invoicesRV)
    RecyclerView rvCompaniesRV;

    ArrayList<CompaniesModel> mArrayList = new ArrayList<CompaniesModel>();
    ArrayList<CompaniesModel> filteredList = new ArrayList<CompaniesModel>();
    CompaniesAdapter mCompaniesAdapter;

    EditCompanyInterface mEditCompanyInterface = new EditCompanyInterface() {
        @Override
        public void editCompany(CompaniesModel mCompaniesModel) {
            Intent mIntent = new Intent(mActivity, EditCompanyActivity.class);
            mIntent.putExtra("Model", mCompaniesModel);
            mActivity.startActivity(mIntent);
        }
    };

    DeleteCompanyInterface mDeleteCompanyInterface = new DeleteCompanyInterface() {
        @Override
        public void deleteCompany(CompaniesModel mCompaniesModel, int position) {
            deleteConfirmDialog(mCompaniesModel, position);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_invoice_company);

        setStatusBar();

        getIntentData();

        ButterKnife.bind(this);
    }

    private void getIntentData() {
        mArrayList = (ArrayList<CompaniesModel>) getIntent().getSerializableExtra("QuestionListExtra");
    }

    protected void setClickListner() {
        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                charSequence = charSequence.toString().toLowerCase();
                filteredList.clear();
                for (int k = 0; k < mArrayList.size(); k++) {
                    final String text = mArrayList.get(k).getCompany_name().toLowerCase();
                    if (text.contains(charSequence)) {
                        filteredList.add(mArrayList.get(k));
                    }
                }
                rvCompaniesRV.setLayoutManager(new LinearLayoutManager(mActivity));
                mCompaniesAdapter = new CompaniesAdapter(mActivity, filteredList, mDeleteCompanyInterface, mEditCompanyInterface);
                rvCompaniesRV.setAdapter(mCompaniesAdapter);
                mCompaniesAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void deleteConfirmDialog(final CompaniesModel mCompaniesModel, int position) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_company));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mCompaniesModel, position);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    private void executeDeleteAPI(CompaniesModel mCompaniesModel, int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteCompaniesRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), mCompaniesModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
                          @Override
                          public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              StatusMsgModel mModel = response.body();
                              if (mModel.getStatus() == 1) {

                                  mArrayList.remove(position);
                                  filteredList.remove(position);

                                  if (mCompaniesAdapter != null)
                                      mCompaniesAdapter.notifyDataSetChanged();

                              } else if (mModel.getStatus() == 100) {
                                  AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                              } else {
                                  AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }
}
