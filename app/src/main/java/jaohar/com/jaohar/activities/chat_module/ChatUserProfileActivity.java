package jaohar.com.jaohar.activities.chat_module;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.utils.JaoharConstants;

public class ChatUserProfileActivity extends AppCompatActivity {
    /**
     * set Activity
     **/
    Activity mActivity = ChatUserProfileActivity.this;

    /**
     * set Activity TAG
     **/
    String TAG = ChatUserProfileActivity.this.getClass().getSimpleName();

    /**
     * Widgets AND Adapters,Strings, Array List,Boolean and Integers
     **/
    @BindView(R.id.firstNameTV)
    TextView firstNameTV;
    @BindView(R.id.lastNameTV)
    TextView lastNameTV;
    @BindView(R.id.roleTV)
    TextView roleTV;
    @BindView(R.id.userImage)
    ImageView userImage;
    @BindView(R.id.imgBack)
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_chat);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        ButterKnife.bind(this);

        getIntentData();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getIntentData() {
        if (getIntent() != null) {
            if (getIntent().getStringExtra(JaoharConstants.USER_NAME) != null) {
                String fullName = getIntent().getStringExtra(JaoharConstants.USER_NAME);
                int idx = fullName.lastIndexOf(' ');
                String firstName = fullName.substring(0, idx);
                String lastName = fullName.substring(idx + 1);

                firstNameTV.setText(firstName);
                lastNameTV.setText(lastName);
            }

            if (getIntent().getStringExtra(JaoharConstants.ROLE) != null) {
                roleTV.setText(getIntent().getStringExtra(JaoharConstants.ROLE));
            }

            if (getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC) != null) {
                Glide.with(mActivity).load(getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC))
                        .placeholder(R.drawable.ic_user)
                        .into(userImage);
            }
        }
    }
}