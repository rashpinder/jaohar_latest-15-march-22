package jaohar.com.jaohar.activities.universal_invoice_module;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddInvoiceActivity;
import jaohar.com.jaohar.activities.SendingInvoiceEmailActivity;
import jaohar.com.jaohar.adapters.AllUniversalInvoiceAdapter;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.DeleteInvoiceInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.PDFdownloadInterface;
import jaohar.com.jaohar.interfaces.SendMultipleInvoiceMAIlInterface;
import jaohar.com.jaohar.interfaces.SinglemailInvoiceInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AllUniversalInvoiceActivity extends BaseActivity {
    Activity mActivity = AllUniversalInvoiceActivity.this;
    String TAG = AllUniversalInvoiceActivity.this.getClass().getSimpleName();
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL, downArrowRL, resetRL1, invoiceCompanyRL;
    TextView txtCenter, txtRight, txtMailTV, mailAllInvoices, selectedCompanyTV;
    ImageView imgBack, imgRight;
    RecyclerView inVoicesRV;
    AllUniversalInvoiceAdapter mUniversalAdapter;
    SwipeRefreshLayout swipeToRefresh;
    EditText editSearchET;
    ProgressBar progressBottomPB;
    ArrayList<InVoicesModel> modelArrayList = new ArrayList<InVoicesModel>();
    ArrayList<InVoicesModel> mLoadMore = new ArrayList<InVoicesModel>();
    private DownloadManager downloadManager;
    private static String strLastPage = "FALSE";
    static int IsInvoiceActive = 0;
    private long downloadReference;
    private static int page_no = 1;
    private static int OwnerID = 0;
    boolean isSwipeRefresh = false;
    NestedScrollView invoiceNestedScroll;

    DeleteInvoiceInterface mDeleteInvoiceInterface = new DeleteInvoiceInterface() {
        @Override
        public void deleteInvoice(InVoicesModel mInVoicesModel) {
            deleteConfirmDialog(mInVoicesModel);
        }
    };

    private ArrayList<InVoicesModel> multiSelectArrayList = new ArrayList<InVoicesModel>();
    SendMultipleInvoiceMAIlInterface mMultimaIlInterface = new SendMultipleInvoiceMAIlInterface() {
        @Override
        public void mSendMutliInvoice(InVoicesModel mInVoivce, boolean mDelete) {
//            if (mInVoivce != null) {
//
//                if (!mDelete) {
//
//                    IsInvoiceActive = IsInvoiceActive + 1;
//                    multiSelectArrayList.add(mInVoivce);
//                    String strID = mInVoivce.getInvoice_number();
//
//
//                } else {
//                    IsInvoiceActive = IsInvoiceActive - 1;
//                    multiSelectArrayList.remove(mInVoivce);
//                    String strID = mInVoivce.getInvoice_number();
//
//
//                }
//
//
//                if (IsInvoiceActive > 0) {
//                    txtMailTV.setVisibility(View.VISIBLE);
//                } else {
//                    txtMailTV.setVisibility(View.GONE);
//                }
//
//            }
        }
    };

    SinglemailInvoiceInterface msinglemailInvoiceInterface = new SinglemailInvoiceInterface() {
        @Override
        public void mSinglemailInvoice(InVoicesModel mInvoiceModel) {
            String strInvoiceNumber = mInvoiceModel.getInvoice_number();
            if (mInvoiceModel.getmVesselSearchInvoiceModel() != null) {

                String strInvoiceVesselName = "   " + mInvoiceModel.getmVesselSearchInvoiceModel().getVessel_name();

                /*Send Email*/
                Intent mIntent = new Intent(mActivity, SendingInvoiceEmailActivity.class);
                mIntent.putExtra("SubjectName", "Invoice " + strInvoiceNumber + strInvoiceVesselName);
                mIntent.putExtra("vesselIDUniversal", mInvoiceModel.getInvoice_id());
                mIntent.putExtra("OwnerID", mInvoiceModel.getOwner_id());
                startActivity(mIntent);
            } else {

                /*Send Email*/
                Intent mIntent = new Intent(mActivity, SendingInvoiceEmailActivity.class);
                mIntent.putExtra("SubjectName", "Invoice  " + strInvoiceNumber);
                mIntent.putExtra("vesselIDUniversal", mInvoiceModel.getInvoice_id());
                mIntent.putExtra("OwnerID", mInvoiceModel.getOwner_id());
                startActivity(mIntent);
            }


        }
    };

    PDFdownloadInterface mPdfDownloader = new PDFdownloadInterface() {
        @Override
        public void pdfDownloadInterface(final String strPDF, final String strPDFNAme) {
//          new DownloadFile().execute("http://maven.apache.org/maven-1.x/maven.pdf", "maven.pdf");
            if (!strPDF.equals("")) {

                // your code
                downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse(strPDF);
                DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                //Restrict the types of networks over which this download may proceed.
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                //Set whether this download may proceed over a roaming connection.
                request.setAllowedOverRoaming(false);
                //Set the title of this download, to be displayed in notifications (if enabled).
                request.setTitle(strPDFNAme);
                //Set a description of this download, to be displayed in notifications (if enabled)
                request.setDescription("" + "Universal Invoice PDF is Download Please Wait...");
                //Set the local destination for the downloaded file to a path within the application's external files directory
                request.setDestinationInExternalFilesDir(mActivity, Environment.DIRECTORY_DOWNLOADS, strPDFNAme);
                //Enqueue a new download and same the referenceId
                downloadReference = downloadManager.enqueue(request);

            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), "Their is no PDF");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_universal_invoice);
    }

    @Override
    protected void setViewsIDs() {
        progressBottomPB = findViewById(R.id.progressBottomPB);
        editSearchET = (EditText) findViewById(R.id.editSearchET);

        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtMailTV = (TextView) findViewById(R.id.txtMailTV);
        selectedCompanyTV = (TextView) findViewById(R.id.selectedCompanyTV);
        mailAllInvoices = (TextView) findViewById(R.id.mailAllInvoices);
        txtMailTV.setVisibility(View.GONE);
//        txtRight = (TextView) findViewById(R.id.txtRight);
//        txtRight.setVisibility(View.GONE);
        txtCenter.setText(getString(R.string.all_universal_invoices));
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        downArrowRL = (RelativeLayout) findViewById(R.id.downArrowRL);
        invoiceCompanyRL = (RelativeLayout) findViewById(R.id.invoiceCompanyRL);
        resetRL1 = (RelativeLayout) findViewById(R.id.resetRL1);
        imgRightLL.setVisibility(View.VISIBLE);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgRight.setImageResource(R.drawable.plus_symbol);
        inVoicesRV = (RecyclerView) findViewById(R.id.inVoicesRV);
        invoiceNestedScroll = (NestedScrollView) findViewById(R.id.invoiceNestedScroll);
        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
//              isNormalSearch = false;
//              isAdvanceSearch = false;
                modelArrayList.clear();
                editSearchET.setText("");
                mLoadMore.clear();
                page_no = 1;
                executeAPI();
            }
        });

    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransitionExit();
            }
        });

        invoiceCompanyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, InvoiceCompanyActivity.class);
                startActivityForResult(mIntent, 111);
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strLastPage = "FALSE";
                if (selectedCompanyTV.getText().toString().equals("")||selectedCompanyTV.getText().toString().equals(null)){
                    showAlertDialog(mActivity,getString(R.string.app_name),"Please select a company inorder to add Invoice");
                }
                else{
//                    Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
                    Intent mIntent = new Intent(mActivity, AddUniversalInvoiceActivity.class);
                mIntent.putExtra("owner_id", OwnerID);
                    mActivity.startActivity(mIntent);
                    overridePendingTransitionEnter();
                }
            }
        });

        downArrowRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    executeNormalSearch();
                    return true;
                }
                return false;
            }
        });

        txtMailTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    resetRL1.setVisibility(View.VISIBLE);
                } else {
                    resetRL1.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        resetRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelArrayList.clear();
                editSearchET.setText("");
                txtMailTV.setVisibility(View.GONE);
                //*Execute Vesseles API*//*
                page_no = 1;
                executeAPI();

            }
        });
        mailAllInvoices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        invoiceNestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
//                    modelArrayList.clear();
//                    mLoadMore.clear();
//                    if (isAdvanceSearch == false) {
//                        if (isNormalSearch == false) {
//                            isScroolling = true;

                    progressBottomPB.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (strLastPage.equals("FALSE")) {
                                ++page_no;
                                executeAPI();
                            } else {
                                progressBottomPB.setVisibility(View.GONE);
                            }
                        }
                    }, 500);
//                        }
//                    }
                    /* else {
                    progressPB.setVisibility(View.GONE);
                }*/
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        IsInvoiceActive = 0;
        modelArrayList.clear();
        txtMailTV.setVisibility(View.GONE);
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute Vesseles API*//*
            page_no = 1;
            executeAPI();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {
            if (data != null) {
                String strOwnerName = "", strOwnerID = "";
                if (data.getStringExtra("owner_ID") != null && data.getStringExtra("owner_NAME") != null) {
                    OwnerID = Integer.parseInt(data.getStringExtra("owner_ID"));
                    strOwnerName = data.getStringExtra("owner_NAME");
//                    Toast.makeText(mActivity, strOwnerID + strOwnerName, Toast.LENGTH_SHORT).show();

                    selectedCompanyTV.setText(strOwnerName);

                    IsInvoiceActive = 0;
                    modelArrayList.clear();
                    txtMailTV.setVisibility(View.GONE);
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        //*Execute Vesseles API*//*
                        page_no = 1;
                        executeAPI();
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        selectedCompanyTV.setText("");
//        OwnerID = 0;
//        Intent mIntent = new Intent(mActivity, HomeActivity.class);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
//        startActivity(mIntent);
//        finish();
    }

    public void executeAPI() {
        modelArrayList.clear();
//      https://root.jaohar.com/JaoharWebServicesNew/GetAllUniversalInvoice.php
        if (strLastPage.equals("FALSE")) {
            progressBottomPB.setVisibility(View.GONE);
        } else {
            if (page_no > 1) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
            if (page_no == 1) {
                isSwipeRefresh = false;
                swipeToRefresh.setRefreshing(false);
                progressBottomPB.setVisibility(View.GONE);
//                AlertDialogManager.showProgressDialog(mActivity);
            }
            if (!isSwipeRefresh) {
                progressBottomPB.setVisibility(View.GONE);
            }
        }
//      https://root.jaohar.com/Staging/JaoharWebServicesNew/GetAllUniversalInvoiceOwner.php
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllUniversalInvoiceOwnerRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), String.valueOf(page_no), String.valueOf(OwnerID));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.GONE);
                progressBottomPB.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }
                Log.e(TAG, "*****ResponseALL****" + response);
                parseResponse(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponse(String response) {
        try {
            JSONObject mJSonObject = new JSONObject(response);
            String strStatus = mJSonObject.getString("status");
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_invoices");
                if (mjsonArrayData != null) {
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        InVoicesModel mModel = new InVoicesModel();
                        JSONObject mJsonGetDATA = mJsonDATA.getJSONObject("all_data");
                        if (!mJsonGetDATA.getString("invoice_id").equals("")) {
                            mModel.setInvoice_id(mJsonGetDATA.getString("invoice_id"));
                        }
                        if (!mJsonGetDATA.getString("invoice_date").equals("")) {
                            mModel.setInvoice_date(mJsonGetDATA.getString("invoice_date"));
                        }
                        if (!mJsonGetDATA.getString("invoice_no").equals("")) {
                            mModel.setInvoice_number(mJsonGetDATA.getString("invoice_no"));
                        }
                        if (!mJsonGetDATA.getString("pdf").equals("")) {
                            mModel.setPdf(mJsonGetDATA.getString("pdf"));
                        }
                        if (!mJsonGetDATA.getString("pdf_name").equals("")) {
                            mModel.setPdf_name(mJsonGetDATA.getString("pdf_name"));
                        }
                        if (!mJsonGetDATA.getString("owner_id").equals("")) {
                            mModel.setOwner_id(mJsonGetDATA.getString("owner_id"));
                        }
                        if (!mJsonDATA.getString("search_company_data").equals("")) {
                            JSONObject mSearchCompanyObj = mJsonDATA.getJSONObject("search_company_data");
                            CompaniesModel mCompaniesModel = new CompaniesModel();
                            if (!mSearchCompanyObj.isNull("id"))
                                mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                            if (!mSearchCompanyObj.isNull("company_name"))
                                mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                            if (!mSearchCompanyObj.isNull("Address1"))
                                mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                            if (!mSearchCompanyObj.isNull("Address2"))
                                mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                            if (!mSearchCompanyObj.isNull("Address3"))
                                mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                            if (!mSearchCompanyObj.isNull("Address4"))
                                mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                            if (!mSearchCompanyObj.isNull("Address5"))
                                mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                            mModel.setmCompaniesModel(mCompaniesModel);
                        }
                        if (!mJsonDATA.getString("search_vessel_data").equals("")) {
                            JSONObject mSearchVesselObj = mJsonDATA.getJSONObject("search_vessel_data");
                            VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                            if (!mSearchVesselObj.isNull("vessel_id"))
                                mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                            if (!mSearchVesselObj.isNull("vessel_name"))
                                mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                            if (!mSearchVesselObj.isNull("IMO_no"))
                                mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                            if (!mSearchVesselObj.isNull("flag"))
                                mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                            mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                        }
                        if (page_no == 1) {
                            modelArrayList.add(mModel);
                        } else if (page_no > 1) {
                            mLoadMore.add(mModel);
                        }
                    }
                    if (mLoadMore.size() > 0) {
                        modelArrayList.addAll(mLoadMore);
                    }
                    setAdapter();
                }

            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    OnClickInterface mOnClickInterface = new OnClickInterface() {
        @Override
        public void mOnClickInterface(int position) {
//            PerformOptionsClick(position, modelArrayList.get(position).getPdf(), modelArrayList.get(position).getPdf_name());
//            mPDF = modelArrayList.get(position).getPdf();
//            mPDFName = modelArrayList.get(position).getPdf_name();
        }
    };

    private void setAdapter() {
        inVoicesRV.setNestedScrollingEnabled(false);
        inVoicesRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mUniversalAdapter = new AllUniversalInvoiceAdapter(mActivity, modelArrayList, mDeleteInvoiceInterface, mPdfDownloader, msinglemailInvoiceInterface, mMultimaIlInterface, mOnClickInterface);
        inVoicesRV.setAdapter(mUniversalAdapter);
        mUniversalAdapter.notifyDataSetChanged();
    }

    public void deleteConfirmDialog(final InVoicesModel mInVoicesModel) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_invoice));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteApi(mInVoicesModel.getInvoice_id());
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }

    private void executeDeleteApi(String strInvoiceID) {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteUniversalInvoice(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strInvoiceID).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        resetRL1.setVisibility(View.GONE);
                        progressBottomPB.setVisibility(View.GONE);
                        Log.e(TAG, "*****Response****" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            mLoadMore.clear();
                            modelArrayList.clear();
                            page_no = 1;
                            executeAPI();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }

    /*
     * Serach API Implementation
     * */
    private void executeNormalSearch() {
        modelArrayList.clear();
        resetRL1.setVisibility(View.VISIBLE);
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.simpleSearchInvoiceRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), editSearchET.getText().toString(), "staff");
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                resetRL1.setVisibility(View.GONE);
                progressBottomPB.setVisibility(View.GONE);
                Log.e(TAG, "*****Response****" + response);
                try {
                    AlertDialogManager.hideProgressDialog();
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    String strMessage = mJsonObject.getString("message");
                    if (strStatus.equals("1")) {
                        parseResponseSearch(response.body().toString());
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponseSearch(String response) {
        try {
            JSONObject mJSonObject = new JSONObject(response);
            String strStatus = mJSonObject.getString("status");
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
//                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
//                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mJSonObject.getJSONArray("all_searched_invoices");
                if (mjsonArrayData != null) {
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        InVoicesModel mModel = new InVoicesModel();
                        JSONObject mJsonGetDATA = mJsonDATA.getJSONObject("all_data");
                        if (!mJsonGetDATA.getString("invoice_id").equals("")) {
                            mModel.setInvoice_id(mJsonGetDATA.getString("invoice_id"));
                        }
                        if (!mJsonGetDATA.getString("invoice_date").equals("")) {
                            mModel.setInvoice_date(mJsonGetDATA.getString("invoice_date"));
                        }
                        if (!mJsonGetDATA.getString("invoice_no").equals("")) {
                            mModel.setInvoice_number(mJsonGetDATA.getString("invoice_no"));
                        }
                        if (!mJsonGetDATA.getString("pdf").equals("")) {
                            mModel.setPdf(mJsonGetDATA.getString("pdf"));
                        }
                        if (!mJsonGetDATA.getString("pdf_name").equals("")) {
                            mModel.setPdf_name(mJsonGetDATA.getString("pdf_name"));
                        }
                        if (!mJsonGetDATA.getString("owner_id").equals("")) {
                            mModel.setOwner_id(mJsonGetDATA.getString("owner_id"));
                        }
                        if (!mJsonDATA.getString("search_company_data").equals("")) {
                            JSONObject mSearchCompanyObj = mJsonDATA.getJSONObject("search_company_data");
                            CompaniesModel mCompaniesModel = new CompaniesModel();
                            if (!mSearchCompanyObj.isNull("id"))
                                mCompaniesModel.setId(mSearchCompanyObj.getString("id"));
                            if (!mSearchCompanyObj.isNull("company_name"))
                                mCompaniesModel.setCompany_name(mSearchCompanyObj.getString("company_name"));
                            if (!mSearchCompanyObj.isNull("Address1"))
                                mCompaniesModel.setAddress1(mSearchCompanyObj.getString("Address1"));
                            if (!mSearchCompanyObj.isNull("Address2"))
                                mCompaniesModel.setAddress2(mSearchCompanyObj.getString("Address2"));
                            if (!mSearchCompanyObj.isNull("Address3"))
                                mCompaniesModel.setAddress3(mSearchCompanyObj.getString("Address3"));
                            if (!mSearchCompanyObj.isNull("Address4"))
                                mCompaniesModel.setAddress4(mSearchCompanyObj.getString("Address4"));
                            if (!mSearchCompanyObj.isNull("Address5"))
                                mCompaniesModel.setAddress5(mSearchCompanyObj.getString("Address5"));
                            mModel.setmCompaniesModel(mCompaniesModel);
                        }
                        if (!mJsonDATA.getString("search_vessel_data").equals("")) {
                            JSONObject mSearchVesselObj = mJsonDATA.getJSONObject("search_vessel_data");
                            VesselSearchInvoiceModel mVesselSearchModel = new VesselSearchInvoiceModel();
                            if (!mSearchVesselObj.isNull("vessel_id"))
                                mVesselSearchModel.setVessel_id(mSearchVesselObj.getString("vessel_id"));
                            if (!mSearchVesselObj.isNull("vessel_name"))
                                mVesselSearchModel.setVessel_name(mSearchVesselObj.getString("vessel_name"));
                            if (!mSearchVesselObj.isNull("IMO_no"))
                                mVesselSearchModel.setIMO_no(mSearchVesselObj.getString("IMO_no"));
                            if (!mSearchVesselObj.isNull("flag"))
                                mVesselSearchModel.setFlag(mSearchVesselObj.getString("flag"));
                            mModel.setmVesselSearchInvoiceModel(mVesselSearchModel);
                        }
                        if (page_no == 1) {
                            modelArrayList.add(mModel);
                        } else if (page_no > 1) {
                            mLoadMore.add(mModel);
                        }
                    }
                    if (mLoadMore.size() > 0) {
                        modelArrayList.addAll(mLoadMore);
                    }
                    setAdapter();
                }

            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
