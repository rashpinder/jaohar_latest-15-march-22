package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.ContactByIdModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class DetailForAddressBookActivity extends BaseActivity {
    private final Activity mActivity = DetailForAddressBookActivity.this;
    private ImageView imgBack;
    private LinearLayout llLeftLL;
    private TextView txtCenter, listNameTV, email1TV, email2TV, email3TV, phone1TV, phone2TV, phone3TV, companyTV, modifyTV, modifyDateTV;
    private String strContactID = "", strUSERID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_for_address_book);
        if (JaoharConstants.is_Staff_FragmentClick == true) {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
        }
    }

    @Override
    protected void setViewsIDs() {
        listNameTV = findViewById(R.id.listNameTV);
        email1TV = findViewById(R.id.email1TV);
        email2TV = findViewById(R.id.email2TV);
        email3TV = findViewById(R.id.email3TV);
        phone1TV = findViewById(R.id.phone1TV);
        phone2TV = findViewById(R.id.phone2TV);
        phone3TV = findViewById(R.id.phone3TV);
        companyTV = findViewById(R.id.companyTV);
        modifyTV = findViewById(R.id.modifyTV);
        modifyDateTV = findViewById(R.id.modifyDateTV);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        txtCenter.setText(getResources().getString(R.string.detail_address_book));
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        if (getIntent() != null) {
            if (getIntent().getStringExtra("contactID") != null) {
                strContactID = getIntent().getStringExtra("contactID");

            }
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            executeAPIforGetting();
        }
    }


    private void executeAPIforGetting() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<ContactByIdModel> call1 = mApiInterface.getAddressBookByRequest(strUSERID, strContactID);
        call1.enqueue(new Callback<ContactByIdModel>() {
            @Override
            public void onResponse(Call<ContactByIdModel> call, retrofit2.Response<ContactByIdModel> response) {
                AlertDialogManager.hideProgressDialog();
                ContactByIdModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    listNameTV.setText(mModel.getData().getName());
                    companyTV.setText(mModel.getData().getCompanyName());
                    email1TV.setText(mModel.getData().getEmail1());
                    email2TV.setText(mModel.getData().getEmail2());
                    email3TV.setText(mModel.getData().getEmail3());
                    phone1TV.setText(mModel.getData().getPhone1());
                    phone2TV.setText(mModel.getData().getPhone2());
                    phone3TV.setText(mModel.getData().getPhone3());
                    modifyDateTV.setText(mModel.getData().getModifiedOn());
                    modifyTV.setText(mModel.getData().getModifiedBy());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());

                }
            }

            @Override
            public void onFailure(Call<ContactByIdModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    private  void executeAPIforGetting(){
//        String strUrl = null;
//
//        strUrl = JaoharConstants.GetAddressBookById + "?user_id=" + strUSERID + "&contact_id=" + strContactID ;// + "?page_no=" + page_no ;
//
//        strUrl=strUrl.replace(" ","%20");
//
//        Log.e("TAG", "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e("TAG", "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonData = new JSONObject(response);
//                        if (mJsonData.getString("`status`").equals("1")) {
//
//                        JSONObject mjsonDATA = mJsonData.getJSONObject("data");
//                        if(!mjsonDATA.getString("name").equals("")){
//                            listNameTV.setText(mjsonDATA.getString("name"));
//                        }
//                        if(!mjsonDATA.getString("company_name").equals("")){
//                            companyTV.setText(mjsonDATA.getString("company_name"));
//                        }
//                        if(!mjsonDATA.getString("email1").equals("")){
//                            email1TV.setText(mjsonDATA.getString("email1"));
//                        }
//                        if(!mjsonDATA.getString("email2").equals("")){
//                            email2TV.setText(mjsonDATA.getString("email2"));
//                        }
//                        if(!mjsonDATA.getString("email3").equals("")){
//                            email3TV.setText(mjsonDATA.getString("email3"));
//                        }
//                        if(!mjsonDATA.getString("phone1").equals("")){
//                            phone1TV.setText(mjsonDATA.getString("phone1"));
//                        } if(!mjsonDATA.getString("phone2").equals("")){
//                            phone2TV.setText(mjsonDATA.getString("phone2"));
//                        }
//                        if(!mjsonDATA.getString("phone3").equals("")){
//                            phone3TV.setText(mjsonDATA.getString("phone3"));
//                        }
//                        if(!mjsonDATA.getString("modified_on").equals("")){
//                            modifyDateTV.setText(mjsonDATA.getString("modified_on"));
//                        }
//                        if(!mjsonDATA.getString("modified_by").equals("")){
//
//                            modifyTV.setText(mjsonDATA.getString("modified_by"));
//                        }
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("TAG", "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
}
