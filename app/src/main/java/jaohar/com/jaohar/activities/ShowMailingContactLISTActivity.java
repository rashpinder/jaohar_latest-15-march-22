package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.MailListContactAdapter;
import jaohar.com.jaohar.interfaces.DeleteContacstMailInterface;
import jaohar.com.jaohar.interfaces.EditContactsMailListInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.AllMailListContact;
import jaohar.com.jaohar.models.AllMailListContactsModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import jaohar.com.jaohar.views.CustomNestedScrollView;
import retrofit2.Call;
import retrofit2.Callback;

public class ShowMailingContactLISTActivity extends BaseActivity {
    Activity mActivity = ShowMailingContactLISTActivity.this;
    String TAG = ShowMailingContactLISTActivity.this.getClass().getSimpleName();
    TextView txtCenter, txtRight;
    ImageView imgBack, imgRight;
    RelativeLayout imgRightLL;
    LinearLayout llLeftLL;
    RecyclerView mailingListRV;
    SwipeRefreshLayout swipeToRefresh;
    CustomNestedScrollView parentSV;
    boolean isSwipeRefresh = false;
    String strLISTID;
    int page_no = 1;
    public String strLastPage = "FALSE";
    boolean isLoadMore = false;
    MailListContactAdapter mAdapter;
//    ArrayList<MailContactListModel> mArrayList = new ArrayList();
    ArrayList<AllMailListContact> mArrayList = new ArrayList();
//    ArrayList<MailContactListModel> loadMoreArrayList = new ArrayList();
    ArrayList<AllMailListContact> loadMoreArrayList = new ArrayList();
    ProgressBar progressBottomPB;

    DeleteContacstMailInterface mDeleteContact = new DeleteContacstMailInterface() {
        @Override
        public void deleteContacstMailInterface(String strContactID) {
            if (Utilities.isNetworkAvailable(mActivity) == false) {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
            } else {
                deleteConfirmDialog(strContactID);
            }
        }
    };

    paginationforVesselsInterface mPagination = new paginationforVesselsInterface() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                progressBottomPB.setVisibility(View.VISIBLE);
                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            executeAPI(page_no);
                        } else {
                            progressBottomPB.setVisibility(View.GONE);
                        }
                    }
                }, 1000);
            }
        }
    };

    EditContactsMailListInterface mEditContact = new EditContactsMailListInterface() {
        @Override
        public void editContactsMailListInterface(AllMailListContact mModel) {
            strLastPage = "FALSE";
            Intent mIntent = new Intent(mActivity, AddContactMailListActivity.class);
            mIntent.putExtra("isAddClick", "false");
            mIntent.putExtra("contact_id", mModel.getContactId());
            mIntent.putExtra("firstname", mModel.getFirstname());
            mIntent.putExtra("lastname", mModel.getLastname());
            mIntent.putExtra("company", mModel.getCompany());
            mIntent.putExtra("email1", mModel.getEmail1());
            mIntent.putExtra("email2", mModel.getEmail2());
            startActivity(mIntent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_show_mailing_contact_list);

        if (getIntent() != null) {
            if (getIntent().getStringExtra("listID") != null) {
                strLISTID = getIntent().getStringExtra("listID");
            }
        }
    }

    @Override
    protected void setViewsIDs() {
        progressBottomPB = findViewById(R.id.progressBottomPB);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        mailingListRV = (RecyclerView) findViewById(R.id.mailingListRV);
        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
//        txtRight = (TextView) findViewById(R.id.txtRight);
//        txtRight.setVisibility(View.GONE);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);
//        parentSV = (CustomNestedScrollView) findViewById(R.id.parentSV);

        /* set top bar */
        imgBack.setImageResource(R.drawable.back);
        imgRight.setImageResource(R.drawable.add_icon);
        txtCenter.setText(getString(R.string.recipient_list));

        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                page_no = 1;
                mArrayList.clear();
                loadMoreArrayList.clear();
                executeAPI(page_no);
            }
        });
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransitionExit();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strLastPage = "FALSE";
                Intent mIntent = new Intent(mActivity, AddContactMailListActivity.class);
                mIntent.putExtra("isAddClick", "true");
                mIntent.putExtra("listID", strLISTID);
                startActivity(mIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent mIntent = new Intent(mActivity, MailIstManagerActivity.class);
//        startActivity(mIntent);
//        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            if (strLastPage.equals("FALSE")) {
                page_no = 1;
                mArrayList.clear();
                loadMoreArrayList.clear();
                executeAPI(page_no);
            }
        }
    }

    private void executeDeleteAPI(String strContactID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteMailListContactsRequest(strContactID)
                .enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus()==1) {
                            mArrayList.clear();
                            loadMoreArrayList.clear();
                            executeAPI(1);}
                        else {
                            AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        Log.e(TAG, "**ERROR**" + t.getMessage());
                    }
                });
    }

//    private void executeDeleteAPI(String strContactID) {
//        String strUrl = JaoharConstants.DeleteMailListContact + "?contact_id=" + strContactID;// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonData = new JSONObject(response);
//                    if (mJsonData.getString("status").equals("1")) {
//                        mArrayList.clear();
//                        loadMoreArrayList.clear();
//                        executeAPI(1);
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void executeAPI(int page_no) {
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
            AlertDialogManager.hideProgressDialog();
        }
        if (page_no == 1) {
            progressBottomPB.setVisibility(View.GONE);
            AlertDialogManager.showProgressDialog(mActivity);
//          Utilities.showProgressDialog(getActivity());
        }

        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllMailListContactsRequest(String.valueOf(page_no),JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),strLISTID)
                .enqueue(new Callback<AllMailListContactsModel>() {
            @Override
            public void onResponse(Call<AllMailListContactsModel> call, retrofit2.Response<AllMailListContactsModel> response) {
                AlertDialogManager.hideProgressDialog();
                progressBottomPB.setVisibility(View.GONE);

                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
                AllMailListContactsModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    strLastPage=mModel.getData().getLastPage();
                      if (page_no == 1) {
                        mArrayList=mModel.getData().getAllMailListContacts();
                    } else if (page_no > 1) {
                        loadMoreArrayList=mModel.getData().getAllMailListContacts();
                    }

            if (loadMoreArrayList.size() > 0) {
                mArrayList.addAll(loadMoreArrayList);
            }
            if (page_no == 1) {
                setAdapter();
            } else {
                mAdapter.notifyDataSetChanged();
            }}
                else {
            AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
        }
            }

            @Override
            public void onFailure(Call<AllMailListContactsModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


//    public void executeAPI(int page_no) {
//        String strUrl = JaoharConstants.GetAllMailListContacts + "?page_no=" + page_no + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&list_id=" + strLISTID;// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
//        if (page_no > 1) {
//            progressBottomPB.setVisibility(View.VISIBLE);
//            AlertDialogManager.hideProgressDialog();
//        }
//        if (page_no == 1) {
//            progressBottomPB.setVisibility(View.GONE);
//            AlertDialogManager.showProgressDialog(mActivity);
////          Utilities.showProgressDialog(getActivity());
//        }
//
//        if (!isSwipeRefresh) {
//            progressBottomPB.setVisibility(View.GONE);
//        }
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Responseshow******" + response);
//                AlertDialogManager.hideProgressDialog();
//                progressBottomPB.setVisibility(View.GONE);
//
//                if (isSwipeRefresh) {
//                    swipeToRefresh.setRefreshing(false);
//                }
//
//                try {
//                    JSONObject mJsonData = new JSONObject(response);
//                    if (mJsonData.getString("status").equals("1")) {
//                        parseResponce(response);
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error***" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void parseResponce(String response) {
//        try {
//            JSONObject mDataObj = new JSONObject(response);
//            JSONObject mDataObject = mDataObj.getJSONObject("data");
//            strLastPage = mDataObject.getString("last_page");
//            JSONArray mJsonArray = mDataObject.getJSONArray("all_mail_list_contacts");
//            if (mJsonArray != null) {
//                for (int i = 0; i < mJsonArray.length(); i++) {
//                    JSONObject mJsonObject1 = mJsonArray.getJSONObject(i);
//                    MailContactListModel mMailingModel = new MailContactListModel();
//                    if (!mJsonObject1.getString("contact_id").equals("")) {
//                        mMailingModel.setContact_id(mJsonObject1.getString("contact_id"));
//                    }
//                    if (!mJsonObject1.getString("list_id").equals("")) {
//                        mMailingModel.setList_id(mJsonObject1.getString("list_id"));
//                    }
//                    if (!mJsonObject1.getString("firstname").equals("")) {
//                        mMailingModel.setFirstname(mJsonObject1.getString("firstname"));
//                    }
//                    if (!mJsonObject1.getString("lastname").equals("")) {
//                        mMailingModel.setLastname(mJsonObject1.getString("lastname"));
//                    }
//                    if (!mJsonObject1.getString("company").equals("")) {
//                        mMailingModel.setCompany(mJsonObject1.getString("company"));
//                    }
//                    if (!mJsonObject1.getString("email1").equals("")) {
//                        mMailingModel.setEmail1(mJsonObject1.getString("email1"));
//                    }
//                    if (!mJsonObject1.getString("email2").equals("")) {
//                        mMailingModel.setEmail2(mJsonObject1.getString("email2"));
//                    }
//                    if (!mJsonObject1.getString("added_by").equals("")) {
//                        mMailingModel.setAdded_by(mJsonObject1.getString("added_by"));
//                    }
//                    if (!mJsonObject1.getString("modification_date").equals("")) {
//                        mMailingModel.setModification_date(mJsonObject1.getString("modification_date"));
//                    }
//                    if (page_no == 1) {
//                        mArrayList.add(mMailingModel);
//                    } else if (page_no > 1) {
//                        loadMoreArrayList.add(mMailingModel);
//                    }
//                }
//            }
//
//            if (loadMoreArrayList.size() > 0) {
//                mArrayList.addAll(loadMoreArrayList);
//            }
//            if (page_no == 1) {
//                setAdapter();
//            } else {
//                mAdapter.notifyDataSetChanged();
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    private void setAdapter() {
//        mailingListRV.setNestedScrollingEnabled(false);
        mailingListRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new MailListContactAdapter(mActivity, mArrayList, mDeleteContact, mEditContact, mPagination);
        mailingListRV.setAdapter(mAdapter);
//        mAdapter.notifyDataSetChanged();
    }

    public void deleteConfirmDialog(final String strListID) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_data));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(strListID);
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }
}
