package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.StatusAdapter;
import jaohar.com.jaohar.interfaces.DeleteStatusInterface;
import jaohar.com.jaohar.interfaces.EditStatusInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.models.StatusData;
import jaohar.com.jaohar.models.StatusModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AllStatusActivity extends BaseActivity {
    Activity mActivity = AllStatusActivity.this;
    String TAG = AllStatusActivity.this.getClass().getSimpleName();
    //WIDGETS
    ImageView imgBack;
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    ImageView imgRight;
    TextView txtCenter;
    RecyclerView statusRV;
    StatusAdapter mStatusAdapter;
    ArrayList<StatusData> mArrayList = new ArrayList<StatusData>();

    EditStatusInterface mEditStatusInterface = new EditStatusInterface() {
        @Override
        public void mEditStatusInterface(StatusData mStatusModel, int position) {
            editDialog(mStatusModel, position);
        }
    };

    DeleteStatusInterface mDeleteStatusInterface = new DeleteStatusInterface() {
        @Override
        public void mDeleteStatusInterface(StatusData mStatusModel, int position) {
            deleteConfirmDialog(mStatusModel, position);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_all_status);

        /* initialize view Ids */
        setIntViewsIDs();
    }

    public void setIntViewsIDs() {
        /*SET UP TOOLBAR*/
        imgBack = findViewById(R.id.imgBack);
        llLeftLL = findViewById(R.id.llLeftLL);
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRight = findViewById(R.id.imgRight);
        txtCenter = findViewById(R.id.txtCenter);
        statusRV = findViewById(R.id.statusRV);

        imgRightLL.setVisibility(View.VISIBLE);
        imgRight.setVisibility(View.VISIBLE);

        /* set tool bar */
        txtCenter.setText(getString(R.string.vessel_statuses));
        imgRight.setImageResource(R.drawable.add_icon);
        imgBack.setImageResource(R.drawable.back);

        if (!Utilities.isNetworkAvailable(mActivity)) {
            AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.internetconnection));
        } else {
            executeGetAll();
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private void setAdapter() {
        statusRV.setNestedScrollingEnabled(false);
        statusRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mStatusAdapter = new StatusAdapter(mActivity, mArrayList, mEditStatusInterface, mDeleteStatusInterface);
        statusRV.setAdapter(mStatusAdapter);
    }


    /*Add Sttaus Dialog*/
    public void addDialog() {
        final Dialog mDialog = new Dialog(mActivity, R.style.myFullscreenAlertDialogStyle);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_add_status);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

        // set the custom dialog components - text, image and button
        final EditText editVesselTypeET = mDialog.findViewById(R.id.editVesselTypeET);
        Button btnAddB = mDialog.findViewById(R.id.btnAddB);
        ImageView cancelImg = mDialog.findViewById(R.id.cancelImg);

        btnAddB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editVesselTypeET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_the_status_name));
                } else {
                    if (!Utilities.isNetworkAvailable(mActivity)) {
                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeAddAPI(editVesselTypeET.getText().toString());
                        mDialog.dismiss();
                    }

                }
            }
        });

        cancelImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    /*Add Sttaus Dialog*/
    public void editDialog(final StatusData mStatusModel,int position) {
        final Dialog mDialog = new Dialog(mActivity, R.style.myFullscreenAlertDialogStyle);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_edit_status);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

        // set the custom dialog components - text, image and button
        final EditText editVesselTypeET = mDialog.findViewById(R.id.editVesselTypeET);
        editVesselTypeET.setText(mStatusModel.getStatusName());
        editVesselTypeET.setSelection(mStatusModel.getStatusName().length());
        Button btnUpdateB = mDialog.findViewById(R.id.btnUpdateB);
        ImageView cancelImg = mDialog.findViewById(R.id.cancelImg);

        btnUpdateB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editVesselTypeET.getText().toString().equals("")) {
                    Toast.makeText(mActivity, getResources().getString(R.string.please_enter_the_status_name), Toast.LENGTH_LONG).show();
                } else {
                    if (!Utilities.isNetworkAvailable(mActivity)) {
                        Toast.makeText(mActivity, getResources().getString(R.string.internetconnection), Toast.LENGTH_LONG).show();
                    } else {
                        executeEditAPI(mStatusModel.getId(), editVesselTypeET.getText().toString());
                        mDialog.dismiss();
                    }
                }
            }
        });

        cancelImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    /*Execute Get All Status*/
    private void executeGetAll() {
        mArrayList.clear();
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getStatusRequest().enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, retrofit2.Response<StatusModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
//                   mArrayList.add(mModel);
                   mArrayList=mModel.getData();
                    /*setAdapter*/
                    setAdapter();
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }}

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }



//    public void executeGetAll() {
//        mArrayList.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strAPIUrl = JaoharConstants.GET_ALL_STATUS;
//        StringRequest mStringRequest = new StringRequest(Request.Method.GET, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        JSONArray mArray = mJsonObject.getJSONArray("data");
//                        for (int i = 0; i < mArray.length(); i++) {
//                            StatusData mModel = new StatusData();
//                            JSONObject obj = mArray.getJSONObject(i);
//                            mModel.setId(obj.getString("id"));
//                            mModel.setStatus_name(obj.getString("status_name"));
//
//                            mArrayList.add(mModel);
//                        }
//
//                        /*setAdapter*/
//                        setAdapter();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(mStringRequest);
//    }


    /*Execute Add Status API*/
    private void executeAddAPI(String strStatus) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.addStatusRequest(strStatus);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                }else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

//    public void executeAddAPI(String strStatus) {
//        String strUrl = JaoharConstants.ADD_STATUS + "?status_name=" + strStatus;
//        String actualURL = strUrl.replaceAll(" ", "%20");
//        Log.e(TAG, "***URL***" + actualURL);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, actualURL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAlertDialog(mActivity, getTitle().toString(), mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /*Execute Edit Status API*/
    private void executeEditAPI(String strStatusID, String strStatus) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.editStatusRequest(strStatus,strStatusID);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                }else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

//    public void executeEditAPI(String strStatusID, String strStatus) {
//        String strUrl = JaoharConstants.EDIT_STATUS + "?status_name=" + strStatus + "&status_id=" + strStatusID;
//        String actualURL = strUrl.replaceAll(" ", "%20");
//        Log.e(TAG, "***URL***" + actualURL);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, actualURL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//
//                        showAlertDialog(mActivity, getTitle().toString(), mJsonObject.getString("message"));
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    public void deleteConfirmDialog(final StatusData mStatusModel,int position) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_vessel_status));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mStatusModel, position);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    private void executeDeleteAPI(StatusData mStatusModel, int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteStatusRequest(mStatusModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                }
                else if (mModel.getStatus()==0) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }

//    public void executeDeleteAPI(StatusData mStatusModel) {
//        String strUrl = JaoharConstants.DELETE_STATUS + "?status_id=" + mStatusModel.getId();
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        showAlertDialog(mActivity, getTitle().toString(), mJsonObject.getString("message"));
//                    } else if (mJsonObject.getString("status").equals("0")) {
//                        showAlertDialog(mActivity, getTitle().toString(), mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                executeGetAll();
            }
        });
        alertDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();

        AlertDialogManager.hideProgressDialog();
    }
}
