package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;

public class AddInvoiceItemActivity extends BaseActivity {

    Activity mActivity = AddInvoiceItemActivity.this;
    String TAG = AddInvoiceItemActivity.this.getClass().getSimpleName();

    /* WIDGET */
    RelativeLayout imgRightLL;
    LinearLayout llLeftLL;
    ImageView imgBack, imgRight;
    TextView txtCenter;
    EditText editItemsET;
    EditText editQuantityET;
    EditText editPriceET;
    EditText editDescriptionET;
    Button btnSave;

    InvoiceAddItemModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_invoice_item);

        setStatusBar();
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = findViewById(R.id.llLeftLL);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setVisibility(View.GONE);
        imgRight = findViewById(R.id.imgRight);
        imgRight.setImageResource(R.drawable.close_cross);
        txtCenter = findViewById(R.id.txtCenter);
        txtCenter.setText("Add Item");
        editItemsET = findViewById(R.id.editItemsET);
        editItemsET.requestFocus();
        editQuantityET = findViewById(R.id.editQuantityET);
        editPriceET = findViewById(R.id.editPriceET);
        editDescriptionET = findViewById(R.id.editDescriptionET);
        btnSave = findViewById(R.id.btnSave);
        imgRightLL = findViewById(R.id.imgRightLL);
    }

    @Override
    protected void setClickListner() {
        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editItemsET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_items));
                } else if (editQuantityET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_quantity));
                } else if (editPriceET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_price11));
                } else if (editDescriptionET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_description11));
                } else {
                    EditInvoiceActivity.isPaymentUpdated = true;

                    InvoiceAddItemModel mModel = new InvoiceAddItemModel();
                    mModel.setItem(editItemsET.getText().toString());
                    mModel.setQuantity(Integer.parseInt(editQuantityET.getText().toString()));
                    mModel.setUnitprice(editPriceET.getText().toString());
                    mModel.setDescription(editDescriptionET.getText().toString());
                    mModel.setStrTotalAmountUnit(String.valueOf(Double.parseDouble(editPriceET.getText().toString()) * Double.parseDouble(editQuantityET.getText().toString())));
                    mModel.setTotalAmount(String.valueOf(Double.parseDouble(editPriceET.getText().toString()) * Double.parseDouble(editQuantityET.getText().toString())));
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("Model", mModel);
                    setResult(111, returnIntent);
                    finish();
                    overridePendingTransitionExit();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage, final String itemID) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                mModel = new InvoiceAddItemModel();

                mModel.setItem(editItemsET.getText().toString());
                if (editPriceET.getText().toString().length() > 0)
                    mModel.setUnitprice(editPriceET.getText().toString());
                if (editQuantityET.getText().toString().length() > 0)
                    mModel.setQuantity(Integer.parseInt(editQuantityET.getText().toString()));
                mModel.setDescription(editDescriptionET.getText().toString());

                mModel.setItemID(itemID);

                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", mModel);
                setResult(101, returnIntent);
                finish();
                overridePendingTransitionExit();
            }
        });
        alertDialog.show();
    }
}
