package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.blog_module.BlogListAdminActivity;
import jaohar.com.jaohar.activities.forum_module_admin.AdminForumActivity;
import jaohar.com.jaohar.activities.manager_module.AllInternalNewsManagerActivity;
import jaohar.com.jaohar.activities.manager_module.AllNewsManagerActivity;
import jaohar.com.jaohar.activities.modules_module.AdminModuleListActivity;
import jaohar.com.jaohar.activities.trash_module.TrashActivity;
import jaohar.com.jaohar.adapters.manager_module.DynamicAdditionsAdapter;
import jaohar.com.jaohar.adapters.manager_module.ManagerModulesAdapter;
import jaohar.com.jaohar.beans.ManagerModule.ManagerModulesModel;
import jaohar.com.jaohar.interfaces.managermodules.ClickManagerModulesInterface;
import jaohar.com.jaohar.utils.JaoharConstants;

public class DynamicAditionsActivity extends BaseActivity implements ClickManagerModulesInterface {
    Activity mActivity = DynamicAditionsActivity.this;
    String TAG = DynamicAditionsActivity.this.getClass().getSimpleName();
    //WIDGETS
    ImageView imgBack, imgRight;
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    TextView txtCenter, txtRight;
    RecyclerView tabListRV;

    Button btnVesselStatus, btnVesselTypes, btnAllNews,
            btnAllInternalNews, btnAddLinksToHome,
            btnAddLinksToHomeForStaff, btnBlogs, btnModules;

    List<ManagerModulesModel> ManagerModulesModelList = new ArrayList<>();
    ClickManagerModulesInterface clickManagerModulesInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_dynamic_aditions);

        /* initialize ids */
        setViewsIDs();

        /* set click interface */
        clickManagerModulesInterface = this;

        /* set all dynamic additions list */
        setModulesList();
    }

    public void setViewsIDs() {
        /*SET UP TOOLBAR*/
        imgRight = findViewById(R.id.imgRight);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.GONE);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        btnVesselStatus = (Button) findViewById(R.id.btnVesselStatus);
        btnVesselTypes = (Button) findViewById(R.id.btnVesselTypes);
        btnAllNews = (Button) findViewById(R.id.btnAllNews);
        btnAllInternalNews = (Button) findViewById(R.id.btnAllInternalNews);
        btnAddLinksToHome = (Button) findViewById(R.id.btnAddLinksToHome);
        btnAddLinksToHomeForStaff = (Button) findViewById(R.id.btnAddLinksToHomeForStaff);
        btnBlogs = (Button) findViewById(R.id.btnBlogs);
        btnModules = (Button) findViewById(R.id.btnModules);
        tabListRV = findViewById(R.id.tabListRV);

        /* set tool bar */
        txtCenter.setText(getString(R.string.dynamic_addition));
//        imgRight.setImageResource(R.drawable.ic_magnifying_glass);
    }

    private void setModulesList() {
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.vessel_statuses), R.drawable.ic_vessel_statuses_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.vessel_types), R.drawable.ic_vessel_types_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.all_news), R.drawable.ic_all_news_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.all_internal_news), R.drawable.ic_all_internal_news_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.add_links_to_home), R.drawable.ic_for_admin_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.add_links_to_home_for_staff), R.drawable.ic_for_staff_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.blogs), R.drawable.ic_blog_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.modules), R.drawable.ic_modules_png));

        /* set modules adapter */
        setAdapter();
    }

    private void setAdapter() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 2);
        tabListRV.setLayoutManager(gridLayoutManager);
        DynamicAdditionsAdapter mAdapter = new DynamicAdditionsAdapter(mActivity, ManagerModulesModelList, clickManagerModulesInterface);
        tabListRV.setAdapter(mAdapter);
    }

    @Override
    public void mClickManagerModulesInterface(int position, String name) {
        if (name.equalsIgnoreCase(getString(R.string.vessel_statuses))) {
            startActivity(new Intent(mActivity, AllStatusActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.vessel_types))) {
            startActivity(new Intent(mActivity, AllVesselTypesActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.all_news))) {
            startActivity(new Intent(mActivity, AllNewsManagerActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.all_internal_news))) {
            startActivity(new Intent(mActivity, AllInternalNewsManagerActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.add_links_to_home))) {
            startActivity(new Intent(mActivity, AllLinkAdminActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.add_links_to_home_for_staff))) {
            startActivity(new Intent(mActivity, AllLinkStaffActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.blogs))) {
            startActivity(new Intent(mActivity, BlogListAdminActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.modules))) {
            startActivity(new Intent(mActivity, AdminModuleListActivity.class));
            overridePendingTransitionEnter();
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnVesselStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AllStatusActivity.class));
                overridePendingTransitionEnter();
            }
        });
        btnVesselTypes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AllVesselTypesActivity.class));
                overridePendingTransitionEnter();
            }
        });
        btnAllNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AllNewsActivity.class));
                overridePendingTransitionEnter();
            }
        });
        btnAllInternalNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AllInternalNewsActivity.class));
                overridePendingTransitionEnter();
            }
        });
        btnAddLinksToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AllLinkAdminActivity.class));
                overridePendingTransitionEnter();
            }
        });
        btnAddLinksToHomeForStaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AllLinkStaffActivity.class));
                overridePendingTransitionEnter();
            }
        });

        btnBlogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, BlogListAdminActivity.class));
                overridePendingTransitionEnter();
            }
        });

        btnModules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mActivity, AdminModuleListActivity.class));
                overridePendingTransitionEnter();

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }
}
