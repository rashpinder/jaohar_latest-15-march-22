package jaohar.com.jaohar.activities.trash_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RD_Inv_Del_Recover_VesselTrashInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RD_Inv_VesselTrashSelectedInterface;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.trash_module_adapter.InvoicingVesselTrashAdapter;
import jaohar.com.jaohar.models.GetAllInvoiceVesselTrash;
import jaohar.com.jaohar.models.SearchVesselData;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class VesselTrashActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = VesselTrashActivity.this;
    /*
     * Getting the Class Name
     * */

    String TAG = VesselTrashActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     **/

    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;
    RecyclerView dataRV;
    ImageView recoverIV, deleteIV, emtyTrashIV;

    /* *
     * Initialize the String and ArrayList
     * */
    private String strLastPage = "FALSE", strVesselID = "";
    String strIsClickFrom = "";
//    ArrayList<VesselSearchInvoiceModel> vesselArrayList = new ArrayList<VesselSearchInvoiceModel>();
    ArrayList<SearchVesselData> vesselArrayList = new ArrayList<SearchVesselData>();
//    ArrayList<VesselSearchInvoiceModel> multiSelectArrayList = new ArrayList<VesselSearchInvoiceModel>();
    ArrayList<SearchVesselData> multiSelectArrayList = new ArrayList<SearchVesselData>();
    ArrayList<String> mRecordID = new ArrayList<String>();

    InvoicingVesselTrashAdapter mAdapter;





    /**
     * To get Selected and unSelected Vessel Interface
     */
    RD_Inv_VesselTrashSelectedInterface mRecoverDeleteSelected= new RD_Inv_VesselTrashSelectedInterface() {
        @Override
        public void mRD_Inv_VesselTrashInterFace(SearchVesselData mModel, boolean isSelected) {
            if (mModel != null) {
                if (!isSelected) {
                    multiSelectArrayList.add(mModel);
                    String strID = mModel.getVesselId();
                    mRecordID.add(mModel.getVesselId());
                } else {
                    multiSelectArrayList.remove(mModel);
                    String strID = mModel.getVesselId();
                    mRecordID.remove(mModel.getVesselId());
                }
            }
        }
    };
    RD_Inv_Del_Recover_VesselTrashInterface mrecoverDelete = new RD_Inv_Del_Recover_VesselTrashInterface() {
        @Override
        public void RD_Inv_RecoverVesselTrashInterface(SearchVesselData mModel, String strType) {
            strVesselID = mModel.getVesselId();
            if (strType.equals("recover")) {
                deleteConfirmDialog(getString(R.string.recover_trash), "recover");
            } else {
                deleteConfirmDialog(getString(R.string.are_you_sure_want_to_delete), "delete");
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vessel_trash);
    }


    /**
     * Set Widget IDS
     **/

    @Override
    protected void setViewsIDs() {
        imgBack = findViewById(R.id.imgBack);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        dataRV = findViewById(R.id.dataRV);
        recoverIV = findViewById(R.id.recoverIV);
        deleteIV = findViewById(R.id.deleteIV);
        emtyTrashIV = findViewById(R.id.emtyTrashIV);
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        txtCenter.setText(getResources().getString(R.string.vessel_trash_screen));

        //Get Clicked Value
        if (getIntent() != null) {
            if (getIntent().getStringExtra("isClick").equals("manageUtilities")) {
                strIsClickFrom = getIntent().getStringExtra("isClick");
            }
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        emtyTrashIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vesselArrayList.size() > 0) {
                    deleteConfirmDialog(getString(R.string.clear_trash), "empty");
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_vessel_found));
                }
            }
        });

        recoverIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vesselArrayList.size() > 0) {

                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {

                        if(mRecordID.size()>0){
                            executeRecoverSelectedTrash();
                        }else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.there_is_no_selected_item));

                        }
                    }
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_vessel_found));

                }
            }
        });

        deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vesselArrayList.size() > 0) {
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        if(mRecordID.size()>0){
                            executeDeleteSelectedTrash();
                        }else {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.there_is_no_selected_item));

                        }

                    }
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.no_vessel_found));

                }
            }
        });
    }



    
    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {

            executeAPI();

        }
    }


    /**
     * Call GetAllCompaniesTrash API for getting
     * list of all deleted companies
     *
     * @param
     * @user_id
     */
//    public void executeAPI() {
//        String strUrl = JaoharConstants.GetAllInvoiceVesselsTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
//
//        AlertDialogManager.showProgressDialog(mActivity);
//
//        vesselArrayList.clear();
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******ResponseVesselTrash*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        parseResponse(response);
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void executeAPI() {
//        String strUrl = JaoharConstants.GetAllInvoiceVesselsTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);

        AlertDialogManager.showProgressDialog(mActivity);
        vesselArrayList.clear();
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllInvoiceVesselTrash> call1 = mApiInterface.getAllInvoiceVesselsTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<GetAllInvoiceVesselTrash>() {
            @Override
            public void onResponse(Call<GetAllInvoiceVesselTrash> call, retrofit2.Response<GetAllInvoiceVesselTrash> response) {
                Log.e(TAG, "******ResponseVesselTrash*****" + response);
                AlertDialogManager.hideProgressDialog();
                GetAllInvoiceVesselTrash mModel = response.body();
                if (mModel.getStatus() == 1) {
                    vesselArrayList=mModel.getData();
                /*Set Adapter*/
                setAdapter();
//                        parseResponse(response);
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                    }
            }

         @Override
        public void onFailure(Call<GetAllInvoiceVesselTrash> call, Throwable t) {
            AlertDialogManager.hideProgressDialog();
            Log.e(TAG, "******error*****" + t.getMessage());
        }
    });}

//    private void parseResponse(String response) {
//
//        JSONObject mJsonObject = null;
//        try {
//            mJsonObject = new JSONObject(response);
//            JSONArray mJsonArray = mJsonObject.getJSONArray("data");
//            for (int i = 0; i < mJsonArray.length(); i++) {
//                JSONObject mJson = mJsonArray.getJSONObject(i);
//                VesselSearchInvoiceModel mModel = new VesselSearchInvoiceModel();
//                mModel.setVessel_id(mJson.getString("vessel_id"));
//                mModel.setVessel_name(mJson.getString("vessel_name"));
//                mModel.setIMO_no(mJson.getString("IMO_no"));
//                mModel.setFlag(mJson.getString("flag"));
//                mModel.setDeleted_by(mJson.getString("deleted_by"));
//                vesselArrayList.add(mModel);
//            }
//
//            /*Set Adapter*/
//            setAdapter();
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }

    private void setAdapter() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        dataRV.setLayoutManager(layoutManager);
        mAdapter = new InvoicingVesselTrashAdapter(mActivity, vesselArrayList,mRecoverDeleteSelected,mrecoverDelete);
        dataRV.setAdapter(mAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent = new Intent(mActivity, TrashActivity.class);
        mIntent.putExtra("isClick", strIsClickFrom);
        startActivity(mIntent);
        finish();
    }

    public void deleteConfirmDialog(String strMessage, final String strType) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                if (strType.equals("empty")) {
                    /*Execute EMPTY API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeEmptyTrash();
                    }
                } else if (strType.equals("delete")) {
                    /*Execute Delete API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeDeleteTrash();
                    }
                } else {
                    /*Execute Recover API*/
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeRecoverTrash();
                    }
                }
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    /**
     * Implement API for Clear All Trash
     */
    private void executeEmptyTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.emptyInvoiceVesselTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "")).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            vesselArrayList.clear();
                            executeAPI();
                            mAdapter.notifyDataSetChanged();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }



//    public void executeEmptyTrash() {
//
//        String strUrl = JaoharConstants.EmptyInvoiceVesselTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        vesselArrayList.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Implement API for Delete Invoice Vessel
     */
    private void executeDeleteTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteSingleInvoiceVesselTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""), strVesselID).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            vesselArrayList.clear();
                            executeAPI();
                            mAdapter.notifyDataSetChanged();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                        }
                    }
                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }

//    public void executeDeleteTrash() {
//        String strUrl = JaoharConstants.DeleteSingleInvoiceVesselTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&vessel_id=" + strVesselID;// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******ResponseDeleteSingle*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        vesselArrayList.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Implement API for Recover Invoice Vessel
     */
    private void executeRecoverTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.recoverSingleInvoiceVesselTrash(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""), strVesselID).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            vesselArrayList.clear();
                            mRecordID.clear();
                            executeAPI();
                            mAdapter.notifyDataSetChanged();
                        } else {
                            AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), mModel.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }

//    public void executeRecoverTrash() {
//        String strUrl = JaoharConstants.RecoverSingleInvoiceVesselTrash + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&vessel_id=" + strVesselID;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******ResponseDeleteSingle*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        vesselArrayList.clear();
//                        mRecordID.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Implement API for Recover Selected Vessel
     */

    private Map<String, String> mRecoverParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        params.put("vessel_ids", String.valueOf(getMultiVesselDetailsData()));
        Log.e("**PARAMS**",params.toString());
        return params;
    }

    public void executeRecoverSelectedTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.recoverSelectedInvoiceVesselTrash(mRecoverParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    vesselArrayList.clear();
                    mRecordID.clear();
                    executeAPI();
                    mAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" +t.getMessage());

            }
        });
    }


//    public void executeRecoverSelectedTrash() {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.RecoverSelectedInvoiceVesselTrash;
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("vessel_ids", getMultiVesselDetailsData());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.e("test", "******responseRecover*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String strStatus = jsonObject.getString("status");
//                    String strMessage = jsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        vesselArrayList.clear();
//                        mRecordID.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("test", "******response*****" + error.toString());
//            }
//        }) {
//            /* *
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /*
     * Arrange Selected Array list and Get ID from that
     * */
    private ArrayList<String> getMultiVesselDetailsData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();
        Log.e(TAG, "ArrayLIST_DATA1============= " + mRecordID.size());
        for (int i = 0; i < mRecordID.size(); i++) {
            strData.add(mRecordID.get(i));
        }

        return strData;
    }
    /**
     * Implement API for Delete Selected Company
     */
    private Map<String, String> mDeleteParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        params.put("vessel_ids", String.valueOf(getMultiVesselDetailsData()));
        Log.e("**PARAMS**", params.toString());
        return params;
    }

    public void executeDeleteSelectedTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteSelectedInvoiceVesselTrash(mDeleteParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    vesselArrayList.clear();
                    mRecordID.clear();
                    executeAPI();
                    mAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeDeleteSelectedTrash() {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.DeleteSelectedInvoiceVesselTrash;
//
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("vessel_ids", getMultiVesselDetailsData());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.e("test", "******responseDelete*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//
//                    String strStatus = jsonObject.getString("status");
//                    String strMessage = jsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//
//                        vesselArrayList.clear();
//                        mRecordID.clear();
//                        executeAPI();
//                        mAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("test", "******response*****" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

}
