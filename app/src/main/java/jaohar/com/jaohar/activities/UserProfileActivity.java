package jaohar.com.jaohar.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;

import androidx.exifinterface.media.ExifInterface;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
//import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class UserProfileActivity extends BaseActivity {
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private String cameraStr = Manifest.permission.CAMERA;
    private String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE;
    Activity mActivity = UserProfileActivity.this;
    String TAG = UserProfileActivity.this.getClass().getSimpleName();
    String refreshedToken = "";
    String strrLoginID = "";
    LinearLayout llLeftLL;
    TextView txtCenter;
    Bitmap rotate = null;
    Bitmap thumb = null;
    private ImageView itemDeleteIV;
    private RelativeLayout imgRightLL;
    private CircleImageView mImage;
    private EditText editFirstNameET, editLastNAMEET;
    private Button btnAddNews;
    private Spinner selectSpinner;

    private String mStoragePath;
    private String strBase64 = "", strType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_user_profile);
getPushToken();
//        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "********TOKEN*******" + refreshedToken);
        if (!Utilities.isNetworkAvailable(mActivity)) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            if (JaoharConstants.BOOLEAN_USER_LOGOUT) {
                gettingProfileDATA(JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, ""));
            } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT) {
                gettingProfileDATA(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
            } else if (JaoharConstants.Is_click_form_Manager) {
                gettingProfileDATA(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
            } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL) {
                gettingProfileDATA(JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, ""));
            }
        }
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
//handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }

// Get new Instance ID token
                    refreshedToken = task.getResult();
                    Log.e(TAG, "**Push Token**" + refreshedToken);

                });
    }

    @Override
    protected void setViewsIDs() {
        editLastNAMEET = findViewById(R.id.editLastNAMEET);
        editFirstNameET = findViewById(R.id.editFirstNameET);
        mImage = findViewById(R.id.mImage);
        llLeftLL = findViewById(R.id.llLeftLL);
        selectSpinner = findViewById(R.id.selectSpinner);
        btnAddNews = findViewById(R.id.btnAddNews);
        itemDeleteIV = findViewById(R.id.itemDeleteIV);
        txtCenter = findViewById(R.id.txtCenter);
//        txtCenter.setText(getResources().getString(R.string.profile));
        ImageView imgBack = findViewById(R.id.imgBack);
        ImageView imgRight = findViewById(R.id.imgRight);
        imgRight.setImageResource(R.drawable.ic_logout);
//        imgBack.setImageResource(R.drawable.back);
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);

        setUpCityAdapter();
    }

    @Override
    protected void setClickListner() {
        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialog();

            }
        });
        mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialog();

            }
        });
        mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpCameraGalleryDialog();
            }
        });

        itemDeleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpCameraGalleryDialog();
            }
        });
        btnAddNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utilities.isNetworkAvailable(mActivity)) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    if (JaoharConstants.BOOLEAN_USER_LOGOUT) {
                        executeAddAPI(JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, ""));
                    } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT) {
                        executeAddAPI(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
                    } else if (JaoharConstants.Is_click_form_Manager) {
                        executeAddAPI(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
                    } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL) {
                        executeAddAPI(JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, ""));
                    }

                }
            }
        });
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);

        TextView text_camra = dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = dialog.findViewById(R.id.txt_cancel);

        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        txt_files.setVisibility(View.GONE);
        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openFiles();
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 505);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                String authorities = getApplicationContext().getPackageName() + "com.dharmaniapps.fileprovider";
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            }
        }
        startActivityForResult(takePictureIntent, CAMERA_REQUEST);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        String mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    /*********
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }

    void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                            openCameraGalleryDialog();

                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            requestPermission();

                        }
                    }
                }
                break;
        }
    }

    public void setUpCameraGalleryDialog() {
        if (checkPermission()) {
            openCameraGalleryDialog();
        } else {
            requestPermission();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            Bitmap bitmap;
                            Uri uri = data.getData();
                            File finalFile = new File(getRealPathFromURI(uri));

                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inSampleSize = 0;

                            bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                          For COnvert And rotate image
                            ExifInterface exifInterface = null;
                            try {
                                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                            Matrix matrix = new Matrix();
                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(270);
                                    break;
                                case ExifInterface.ORIENTATION_NORMAL:
                                default:

                            }
                            JaoharConstants.IS_camera_Click = false;
                            thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                            mImage.setImageBitmap(thumb);
                            strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "*****No Picture Selected****");
                        return;
                    }
                } finally {
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                try {

                    JaoharConstants.IS_camera_Click = true;

                    thumb = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;

                    Bitmap bitmap = BitmapFactory.decodeFile(mStoragePath, options);
                    Bitmap thumb1 = rotateImage(bitmap);

                    mImage.setImageBitmap(thumb);

                    strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);

//                    imgStampImageIV.setImageURI(uri);
                    //strImageBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == 505) {
                Bitmap bitmap;
                String strDocumentPath, strDocumentName;
                Uri uri = data.getData();
                strDocumentPath = uri.getPath();
                strDocumentPath = strDocumentPath.replace(" ", "_");
                strDocumentName = uri.getLastPathSegment();
                File finalFile = new File(getRealPathFromURI(uri));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 0;
                bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                          For COnvert And rotate image
                ExifInterface exifInterface = null;
                try {
                    exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                Matrix matrix = new Matrix();
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.setRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.setRotate(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        matrix.setRotate(270);
                        break;
                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                }
                JaoharConstants.IS_camera_Click = false;
                thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                mImage.setImageBitmap(thumb);
                strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
            }
        }
    }
    //       Method Correct Rotate Image when Capture From Camera....

    private Bitmap rotateImage(Bitmap bitmap) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(mStoragePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            default:
        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotate;
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    public void onBackPressed() {
        if (JaoharConstants.BOOLEAN_USER_LOGOUT == true) {
            JaoharConstants.BOOLEAN_USER_LOGOUT = false;
            JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
            JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
            JaoharConstants.Is_click_form_Manager = false;
            super.onBackPressed();
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mIntent.putExtra(JaoharConstants.LOGIN, "VesselForSale");
            mActivity.startActivity(mIntent);
            finish();
            overridePendingTransitionExit();
        } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT == true) {
            JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
            JaoharConstants.BOOLEAN_USER_LOGOUT = false;
            JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
            JaoharConstants.Is_click_form_Manager = false;
            super.onBackPressed();
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mIntent.putExtra(JaoharConstants.LOGIN, "Staff");
            mActivity.startActivity(mIntent);
            finish();
            overridePendingTransitionExit();
        } else if (JaoharConstants.Is_click_form_Manager == true) {
            JaoharConstants.Is_click_form_Manager = false;
            JaoharConstants.BOOLEAN_USER_LOGOUT = false;
            JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
            JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
            super.onBackPressed();
            finish();
            overridePendingTransitionExit();
        } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL == true) {
            JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
            JaoharConstants.Is_click_form_Manager = false;
            JaoharConstants.BOOLEAN_USER_LOGOUT = false;
            JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
            JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
            super.onBackPressed();
            finish();
            overridePendingTransitionExit();
        } else {
            super.onBackPressed();
        }
    }

    //ADMIN_LOGOUT_API
    private void executeAdminLogoutApi(String strLoginID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.adminLogoutRequest(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""), refreshedToken, "Android");
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                        showLogoutDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                    } else {
                        mAlerDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                    }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }



   private void logOutAPI(String strLoginID) {
//      http://root.jaohar.net/Staging/JaoharWebServicesNew/logout.php
        String strUrl = "";
        if (JaoharConstants.Is_click_form_Manager == true) {
            executeAdminLogoutApi(strLoginID);
//            strUrl = JaoharConstants.ADMIN_LOGOUT_API + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&device_token=" + refreshedToken + "&device_type=" + "Android";
        } else {
            executeLogoutApi(strLoginID);
//            strUrl = JaoharConstants.LOGOUT_API + "?user_id=" + strLoginID + "&device_token=" + refreshedToken + "&device_type=" + "Android";
        }}

    private void executeLogoutApi(String strLoginID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.logoutRequest(strLoginID, refreshedToken, "Android");
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showLogoutDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                } else {
                    mAlerDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }



// private void logOutAPI(String strLoginID) {
////      http://root.jaohar.net/Staging/JaoharWebServicesNew/logout.php
//        String strUrl = "";
//        if (JaoharConstants.Is_click_form_Manager == true) {
//            strUrl = JaoharConstants.ADMIN_LOGOUT_API + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&device_token=" + refreshedToken + "&device_type=" + "Android";
//        } else {
//            strUrl = JaoharConstants.LOGOUT_API + "?user_id=" + strLoginID + "&device_token=" + refreshedToken + "&device_type=" + "Android";
//        }
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    if (strStatus.equals("1")) {
//                        showLogoutDialog(mActivity, getString(R.string.app_name), mJsonObject.getString("message"));
//                    } else {
//                        mAlerDialog(mActivity, getString(R.string.app_name), mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("first_name", editFirstNameET.getText().toString());
        mMap.put("last_name", editLastNAMEET.getText().toString());
        mMap.put("image", strBase64);
        mMap.put("user_id", strrLoginID);
        mMap.put("online_status", strType);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAddAPI(String strLoginID) {
        strrLoginID=strLoginID;
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.editUserProfileRequest(mParam());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mAlerDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                } else {
                    mAlerDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


//    public void executeAddAPI(String strLoginID) {
//        AlertDialogManager.showProgressDialog(mActivity);
//        String url = JaoharConstants.EditUserProfile_API;
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("first_name", editFirstNameET.getText().toString());
//            jsonObject.put("last_name", editLastNAMEET.getText().toString());
//            jsonObject.put("image", strBase64);
//            jsonObject.put("user_id", strLoginID);
//            jsonObject.put("online_status", strType);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                url, jsonObject,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        AlertDialogManager.hideProgressDialog();
//                        Log.d(TAG, response.toString());
//                        try {
//                            if (response.getString("status").equals("1")) {
//                                mAlerDialog(mActivity, getString(R.string.app_name), response.getString("message"));
//                            } else {
//                                mAlerDialog(mActivity, getString(R.string.app_name), response.getString("message"));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                VolleyLog.d(TAG, "Error: " + error.getMessage());
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), error.getMessage());
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
////      Adding request to request queue
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjReq);
//    }

    public void mAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }

    public void showLogoutDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (JaoharConstants.BOOLEAN_USER_LOGOUT == true) {
                    JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
                    JaoharConstants.BOOLEAN_USER_LOGOUT = false;

                    JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
                    JaoharConstants.Is_click_form_Manager = false;

                    SharedPreferences preferences = JaoharPreference.getPreferences(mActivity);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove(JaoharPreference.IS_LOGGED_IN_VESSELS);
                    editor.remove(JaoharPreference.USER_ID);
                    editor.remove(JaoharPreference.USER_EMAIL);
                    editor.remove(JaoharPreference.USER_ROLE);
                    editor.remove(JaoharPreference.first_name);
                    editor.remove(JaoharPreference.image);
                    editor.remove(JaoharPreference.full_name);
                    editor.remove(JaoharPreference.Role);

                    editor.apply();
                    editor.commit();
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "Home");
                    mActivity.startActivity(mIntent);
                    mActivity.finish();
                    overridePendingTransitionEnter();
                } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT == true) {
                    JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
                    JaoharConstants.BOOLEAN_USER_LOGOUT = false;

                    JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
                    JaoharConstants.Is_click_form_Manager = false;
                    SharedPreferences preferences = JaoharPreference.getPreferences(mActivity);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove(JaoharPreference.IS_LOGGED_IN_STAFF);
                    editor.remove(JaoharPreference.STAFF_ID);
                    editor.remove(JaoharPreference.STAFF_EMAIL);
                    editor.remove(JaoharPreference.STAFF_ROLE);
                    editor.remove(JaoharPreference.first_name);
                    editor.remove(JaoharPreference.image);
                    editor.remove(JaoharPreference.full_name);
                    editor.remove(JaoharPreference.Role);
                    editor.apply();
                    editor.commit();
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "Home");
                    mActivity.startActivity(mIntent);
                    mActivity.finish();
                    overridePendingTransitionEnter();
                } else if (JaoharConstants.Is_click_form_Manager == true) {
                    JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
                    JaoharConstants.BOOLEAN_USER_LOGOUT = false;

                    JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
                    JaoharConstants.Is_click_form_Manager = false;
                    SharedPreferences preferences = JaoharPreference.getPreferences(mActivity);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove(JaoharPreference.IS_LOGGED_IN_ADMIN);
                    editor.remove(JaoharPreference.ADMIN_ID);
                    editor.remove(JaoharPreference.ADMIN_EMAIL);
                    editor.remove(JaoharPreference.ADMIN_ROLE);
                    editor.remove(JaoharPreference.first_name);
                    editor.remove(JaoharPreference.image);
                    editor.remove(JaoharPreference.full_name);
                    editor.remove(JaoharPreference.Role);

                    editor.commit();
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
                    mActivity.startActivity(mIntent);
                    mActivity.finish();
                    overridePendingTransitionEnter();
                } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL == true) {
                    JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
                    JaoharConstants.BOOLEAN_USER_LOGOUT = false;
                    JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
                    JaoharConstants.Is_click_form_Manager = false;
                    SharedPreferences preferences = JaoharPreference.getPreferences(mActivity);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove(JaoharPreference.IS_LOGGED_IN_VESSELS);
                    editor.remove(JaoharPreference.USER_ID);
                    editor.remove(JaoharPreference.USER_EMAIL);
                    editor.remove(JaoharPreference.USER_ROLE);
                    editor.remove(JaoharPreference.first_name);
                    editor.remove(JaoharPreference.image);
                    editor.remove(JaoharPreference.full_name);
                    editor.remove(JaoharPreference.Role);

                    editor.apply();
                    editor.commit();
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "Home");
                    mActivity.startActivity(mIntent);
                    mActivity.finish();
                    overridePendingTransitionEnter();
                }
            }
        });
        alertDialog.show();
    }

    public void gettingProfileDATA(String strLoginID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getuserProfileRequest(strLoginID);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    if (strStatus.equals("1")) {
                        if (!mJsonObject.isNull("data")) {
                            JSONObject mJsonDATA = mJsonObject.getJSONObject("data");
                            if (!mJsonDATA.getString("image").equals("")) {
                                strBase64 = mJsonDATA.getString("image");
                                Glide.with(mActivity).load(strBase64).into(mImage);
                            } else {
                                mImage.setImageResource(R.drawable.profile);
                            }
                            if (!mJsonDATA.getString("first_name").equals("")) {
                                editFirstNameET.setText(mJsonDATA.getString("first_name"));
                                editFirstNameET.setSelection(mJsonDATA.getString("first_name").length());
                            }
                            if (!mJsonDATA.getString("last_name").equals("")) {
                                editLastNAMEET.setText(mJsonDATA.getString("last_name"));
                                editLastNAMEET.setSelection(mJsonDATA.getString("last_name").length());
                            }
                            if (!mJsonDATA.getString("chat_status").equals("")) {
                                if (mJsonDATA.getString("chat_status").equals("Invisible")) {
                                    selectSpinner.setSelection(2);
                                } else if (mJsonDATA.getString("chat_status").equals("Busy")) {
                                    selectSpinner.setSelection(1);
                                } else if (mJsonDATA.getString("chat_status").equals("Available")) {
                                    selectSpinner.setSelection(0);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });}

//    private void gettingProfileDATA(String strLoginID) {
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strUrl = JaoharConstants.GetUserProfile_API + "?user_id=" + strLoginID;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "*****Response****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    if (strStatus.equals("1")) {
//                        if (!mJsonObject.isNull("data")) {
//                            JSONObject mJsonDATA = mJsonObject.getJSONObject("data");
//                            if (!mJsonDATA.getString("image").equals("")) {
//                                strBase64 = mJsonDATA.getString("image");
////                                itemDeleteIV.setVisibility(View.VISIBLE);
////                                Glide.with(mActivity)
////                                        .load(strBase64)
////                                        .asBitmap()
////                                        .placeholder(R.drawable.profile)
////                                        .into(new SimpleTarget<Bitmap>() {
////                                            @Override
////                                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
////                                                // you can do something with loaded bitmap here
////                                                mImage.setImageBitmap(resource);
////                                            }
////                                        });
//                                Glide.with(mActivity).load(strBase64).into(mImage);
//
//                            } else {
//                                mImage.setImageResource(R.drawable.profile);
//                            }
//                            if (!mJsonDATA.getString("first_name").equals("")) {
//                                editFirstNameET.setText(mJsonDATA.getString("first_name"));
//                                editFirstNameET.setSelection(mJsonDATA.getString("first_name").length());
//                            }
//                            if (!mJsonDATA.getString("last_name").equals("")) {
//                                editLastNAMEET.setText(mJsonDATA.getString("last_name"));
//                                editLastNAMEET.setSelection(mJsonDATA.getString("last_name").length());
//                            }
//                            if (!mJsonDATA.getString("chat_status").equals("")) {
//                                if (mJsonDATA.getString("chat_status").equals("Invisible")) {
//                                    selectSpinner.setSelection(2);
//                                } else if (mJsonDATA.getString("chat_status").equals("Busy")) {
//                                    selectSpinner.setSelection(1);
//                                } else if (mJsonDATA.getString("chat_status").equals("Available")) {
//                                    selectSpinner.setSelection(0);
//                                }
//                            }
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                AlertDialogManager.hideProgressDialog();
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void ConfirmDialog() {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_logout));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                if (JaoharConstants.BOOLEAN_USER_LOGOUT == true) {
                    logOutAPI(JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, ""));
                } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT == true) {
                    logOutAPI(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
                } else if (JaoharConstants.Is_click_form_Manager == true) {
                    logOutAPI(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
                } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL == true) {
                    logOutAPI(JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, ""));
                }
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }

    private void setUpCityAdapter() {
        final String[] mStatusArray = getResources().getStringArray(R.array.status_online_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mStatusArray) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                TextView itemTextView = v.findViewById(R.id.itemTextView);
                itemTextView.setText(mStatusArray[position]);
                return v;
            }

        };
        selectSpinner.setAdapter(adapter);

        /*Status Spinner Click Listener*/
        selectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                strType = String.valueOf(parent.getItemAtPosition(position));

                ((TextView) view).setText(strType);

                ((TextView) view).setTextColor(getResources().getColor(R.color.grey)); //Change selected text color

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
