package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class EditBankDetailsActivity extends BaseActivity {
    Activity mActivity = EditBankDetailsActivity.this;
    String TAG = EditBankDetailsActivity.this.getClass().getSimpleName();
    //Toolbar
    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;

    EditText editBenificiaryNameET, editBankNameET, editAddress2ET, editRON, editUSD, editEUR, editSwiftET,editAddress1ET,editGBP;
    Button btnEditBankDetails;

    BankModel mBankModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_bank_details);
        if (getIntent() != null) {
            mBankModel = (BankModel) getIntent().getSerializableExtra("Model");
        }
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtCenter.setText(getString(R.string.editbankdetails));

        editBenificiaryNameET = (EditText) findViewById(R.id.editBenificiaryNameET);
        editBankNameET = (EditText) findViewById(R.id.editBankNameET);
        editAddress1ET = (EditText) findViewById(R.id.editAddress1ET);
        editAddress2ET = (EditText) findViewById(R.id.editAddress2ET);
        editRON = (EditText) findViewById(R.id.editRON);
        editUSD = (EditText) findViewById(R.id.editUSD);
        editEUR = (EditText) findViewById(R.id.editEUR);
        editGBP = (EditText) findViewById(R.id.editGBP);
        editSwiftET = (EditText) findViewById(R.id.editSwiftET);
        btnEditBankDetails = (Button) findViewById(R.id.btnEditBankDetails);

        
        /*SetData on Widgets*/
        setDataOnWidgets(mBankModel);
    }

    private void setDataOnWidgets(BankModel mBankModel) {
        editBenificiaryNameET.setText(mBankModel.getBenificiary());
        editBenificiaryNameET.setSelection(mBankModel.getBenificiary().length());
        editBenificiaryNameET.requestFocus();
        editBankNameET.setText(mBankModel.getBankName());
        editAddress1ET.setText(mBankModel.getAddress1());
        editAddress2ET.setText(mBankModel.getAddress2());
        editRON.setText(mBankModel.getIbanRON());
        editUSD.setText(mBankModel.getIbanUSD());
        editEUR.setText(mBankModel.getIbanEUR());
        editGBP.setText(mBankModel.getIban_gbp());
        editSwiftET.setText(mBankModel.getSwift());
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnEditBankDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editBenificiaryNameET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.msg1));
                } else if (editBankNameET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.msg2));
                } else if (editAddress1ET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.msg3));
                }
//                else if (editRON.getText().toString().trim().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.msg4));
//                } else if (editUSD.getText().toString().trim().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.msg5));
//                } else if (editEUR.getText().toString().trim().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.msg6));
//                }
//                else if (editSwiftET.getText().toString().trim().equals("")) {
//                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.msg7));
//                }
                else {

                    /*EXECUTE API*/
                    executeEditBankDetails(mBankModel);

                }
            }
        });
    }


    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("bank_id", mBankModel.getId());
        mMap.put("beneficiary", editBenificiaryNameET.getText().toString());
        mMap.put("bank_name", editBankNameET.getText().toString());
        mMap.put("address1", editAddress1ET.getText().toString());
        mMap.put("address2", editAddress2ET.getText().toString());
        mMap.put("iban_ron", editRON.getText().toString());
        mMap.put("iban_usd", editUSD.getText().toString());
        mMap.put("iban_eur", editEUR.getText().toString());
        mMap.put("swift", editSwiftET.getText().toString());
        mMap.put("iban_gbp", editGBP.getText().toString());
        mMap.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeEditBankDetails(BankModel mBankModel){
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editBankDetailsRequest(mParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    JaoharConstants.IS_BANK_DETAILS_EDIT = true;
                    showAlerDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }

//    private void executeEditBankDetails(BankModel mBankModel) {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.EDIT_BANK_DETAILS;
//        try {
//            jsonObject.put("bank_id", mBankModel.getId());
//            jsonObject.put("beneficiary", editBenificiaryNameET.getText().toString());
//            jsonObject.put("bank_name", editBankNameET.getText().toString());
//            jsonObject.put("address1", editAddress1ET.getText().toString());
//            jsonObject.put("address2", editAddress2ET.getText().toString());
//            jsonObject.put("iban_ron", editRON.getText().toString());
//            jsonObject.put("iban_usd", editUSD.getText().toString());
//            jsonObject.put("iban_eur", editEUR.getText().toString());
//            jsonObject.put("swift", editSwiftET.getText().toString());
//            jsonObject.put("iban_gbp", editGBP.getText().toString());
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        AlertDialogManager.showProgressDialog(mActivity);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + jsonObject.toString());
//                try {
//                    if (jsonObject.getString("status").equals("1")) {
//                        JaoharConstants.IS_BANK_DETAILS_EDIT = true;
//                        showAlerDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else if (jsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******error*****" + error);
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }
}
