package jaohar.com.jaohar.activities.educational_blogs_module;

import static android.view.View.GONE;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.educational_blogs_module.EducationalBlogsAdapter;
import jaohar.com.jaohar.beans.educational_blogs_module.EducationalBlogsModel;
import jaohar.com.jaohar.interfaces.PaginationInterface;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class EducationalBlogsActivity extends BaseActivity implements PaginationInterface {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = EducationalBlogsActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = EducationalBlogsActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.progressBottomPB)
    ProgressBar progressBottomPB;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgRight)
    ImageView imgRight;
    @BindView(R.id.usersRV)
    RecyclerView usersRV;

    int page_no = 1;
    String strLastPage = "TRUE";

    String strSearchText = "";
    /*
     * Setting Up Array List
     * */
    ArrayList<EducationalBlogsModel> modelArrayList = new ArrayList<>();

    /*
     * Setting Up Adapter
     * */
    EducationalBlogsAdapter mEducationalBlogsAdapter;

    PaginationInterface mPagination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(Color.WHITE);
        setContentView(R.layout.activity_educational_blogs);
        setStatusBar();
        ButterKnife.bind(this);

        JaoharSingleton.getInstance().setSearchedEducationalBlog("");
    }

    /**
     * Set Widget IDS
     **/
    @Override
    protected void setViewsIDs() {
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_left_arrow));
        txtCenter.setText(getResources().getString(R.string.educational_blogs));
        txtCenter.setGravity(Gravity.START);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(90, 0, 0, 0);
        txtCenter.setLayoutParams(params);
        imgRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_magnifying_glass));
    }

    @OnClick({R.id.llLeftLL, R.id.imgRight})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.imgRight:
                startActivity(new Intent(mActivity, SearchBlogsActivity.class));
                break;

        }
    }

    public void setAdapter(String type) {
        usersRV.setLayoutManager(new LinearLayoutManager(mActivity, RecyclerView.VERTICAL, false));
        mEducationalBlogsAdapter = new EducationalBlogsAdapter(mActivity, modelArrayList, mPagination, type);
        usersRV.setAdapter(mEducationalBlogsAdapter);
    }

    /* *
     * Execute API for getting Educational blogs list
     * @param
     * @user_id
     * */
    public void executeAPI() {
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_no == 1) {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getEducationalPosts(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), String.valueOf(page_no));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                hideProgressDialog();
                modelArrayList.clear();
                Log.e(TAG, "***URLResponce***" + response.body());
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                parseResponce(response.body().toString());

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    void parseResponce(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            String strStatus = mJSonObject.getString("status");
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_blogs");
                if (mjsonArrayData != null) {
                    ArrayList<EducationalBlogsModel> mTempraryList = new ArrayList<>();
                    mTempraryList.clear();
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        EducationalBlogsModel mModel = new EducationalBlogsModel();
                        if (!mJsonDATA.getString("id").equals("")) {
                            mModel.setId(mJsonDATA.getString("id"));
                        }
                        if (!mJsonDATA.getString("file_name").equals("")) {
                            mModel.setFile_name(mJsonDATA.getString("file_name"));
                        }
                        if (!mJsonDATA.getString("link").equals("")) {
                            mModel.setLink(mJsonDATA.getString("link"));
                        }
                        if (!mJsonDATA.getString("link_title").equals("")) {
                            mModel.setLink_title(mJsonDATA.getString("link_title"));
                        }
                        if (!mJsonDATA.getString("link_desc").equals("")) {
                            mModel.setLink_desc(mJsonDATA.getString("link_desc"));
                        }
                        if (!mJsonDATA.getString("link_image").equals("")) {
                            mModel.setLink_image(mJsonDATA.getString("link_image"));
                        }
                        if (!mJsonDATA.getString("message").equals("")) {
                            mModel.setMessage(mJsonDATA.getString("message"));
                        }
                        if (!mJsonDATA.getString("creation_date").equals("")) {
                            mModel.setCreation_date(mJsonDATA.getString("creation_date"));
                        }
                        if (!mJsonDATA.getString("disable").equals("")) {
                            mModel.setDisable(mJsonDATA.getString("disable"));
                        }
//                        if (!mJsonDATA.getString("is_featured").equals("")) {
//                            mModel.setIs_featured(mJsonDATA.getString("is_featured"));
//                        }
                        if (!mJsonDATA.getString("sortOrder").equals("")) {
                            mModel.setSortOrder(mJsonDATA.getString("sortOrder"));
                        }
                        mTempraryList.add(mModel);
                    }
                    modelArrayList.addAll(mTempraryList);
                    if (page_no == 1) {
                        /* for serach 1 else 0 */
                        setAdapter("0");
                    } else {
                        mEducationalBlogsAdapter.notifyDataSetChanged();
                    }
                }
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void mPaginationInterface(boolean isLastScroll) {
        if (isLastScroll == true) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        progressBottomPB.setVisibility(View.VISIBLE);
                        ++page_no;
                        executeAPI();
                    } else {
                        if (progressBottomPB != null) {
                            progressBottomPB.setVisibility(GONE);
                        }
                    }
                }
            }, 1500);
        }
    }

    /* *
     * Execute Search API for getting Educational blogs list
     * @param
     * @user_id
     * */
    public void executeSearchAPI() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getEducationPostBySearch(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strSearchText);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                modelArrayList.clear();
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                parseSearchResponce(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    void parseSearchResponce(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            String strStatus = String.valueOf(mJSonObject.getInt("status"));
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_blogs");

                ArrayList<EducationalBlogsModel> mTempraryList = new ArrayList<>();

                for (int i = 0; i < mjsonArrayData.length(); i++) {
                    JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                    EducationalBlogsModel mModel = new EducationalBlogsModel();
                    if (!mJsonDATA.getString("id").equals("")) {
                        mModel.setId(mJsonDATA.getString("id"));
                    }
                    if (!mJsonDATA.getString("file_name").equals("")) {
                        mModel.setFile_name(mJsonDATA.getString("file_name"));
                    }
                    if (!mJsonDATA.getString("link").equals("")) {
                        mModel.setLink(mJsonDATA.getString("link"));
                    }
                    if (!mJsonDATA.getString("link_title").equals("")) {
                        mModel.setLink_title(mJsonDATA.getString("link_title"));
                    }
                    if (!mJsonDATA.getString("link_desc").equals("")) {
                        mModel.setLink_desc(mJsonDATA.getString("link_desc"));
                    }
                    if (!mJsonDATA.getString("link_image").equals("")) {
                        mModel.setLink_image(mJsonDATA.getString("link_image"));
                    }
                    if (!mJsonDATA.getString("message").equals("")) {
                        mModel.setMessage(mJsonDATA.getString("message"));
                    }
                    if (!mJsonDATA.getString("creation_date").equals("")) {
                        mModel.setCreation_date(mJsonDATA.getString("creation_date"));
                    }
                    if (!mJsonDATA.getString("disable").equals("")) {
                        mModel.setDisable(mJsonDATA.getString("disable"));
                    }
                    if (mJsonDATA.has("is_featured") && !mJsonDATA.getString("is_featured").equals("")) {
                        mModel.setIs_featured(mJsonDATA.getString("is_featured"));
                    }
                    if (!mJsonDATA.getString("sortOrder").equals("")) {
                        mModel.setSortOrder(mJsonDATA.getString("sortOrder"));
                    }
                    mTempraryList.add(mModel);
                }
                modelArrayList.addAll(mTempraryList);
                /* for serach 1 else 0 */
                setAdapter("1");
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mPagination = this;

        if (modelArrayList != null) {
            modelArrayList.clear();

            /* for search 1 else 0 */
            if (!JaoharSingleton.getInstance().getSearchedEducationalBlog().equals("")) {
                setAdapter("1");
            } else {
                setAdapter("0");
            }
        }

        if (!JaoharSingleton.getInstance().getSearchedEducationalBlog().equals("")) {
            strSearchText = JaoharSingleton.getInstance().getSearchedEducationalBlog();

            executeSearchAPI();
        } else {
            //execute API
            page_no = 1;
            executeAPI();
        }
    }

    /*
     * Set Up Status Bar
     * */
    public void setStatusBar() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }
}
