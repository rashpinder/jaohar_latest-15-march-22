package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class AddCompanyActivity extends BaseActivity {
    Activity mActivity = AddCompanyActivity.this;
    String TAG = AddCompanyActivity.this.getClass().getSimpleName();
    //Toolbar
    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;

    EditText editCompanyNameET, editAddress1ET, editAddress2ET, editAddress3ET, editAddress4ET, editAddress5ET;

    TextView btnAddComapnyB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_company2);
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtCenter.setText(getString(R.string.add_company));
        editCompanyNameET = (EditText) findViewById(R.id.editCompanyNameET);
        editCompanyNameET.requestFocus();
        editAddress1ET = (EditText) findViewById(R.id.editAddress1ET);
        editAddress2ET = (EditText) findViewById(R.id.editAddress2ET);
        editAddress3ET = (EditText) findViewById(R.id.editAddress3ET);
        editAddress4ET = (EditText) findViewById(R.id.editAddress4ET);
        editAddress5ET = (EditText) findViewById(R.id.editAddress5ET);
        btnAddComapnyB = findViewById(R.id.btnAddComapnyB);
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnAddComapnyB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editCompanyNameET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_company_name));
                } else if (editAddress1ET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_company_address1));
                }
                else {
                    /*Execute Add Comapny API*/
                    executeAddCompanyAPI();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }


    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("company_name", editCompanyNameET.getText().toString());
        mMap.put("Address1", editAddress1ET.getText().toString());
        mMap.put("Address2", editAddress2ET.getText().toString());
        mMap.put("Address3", editAddress3ET.getText().toString());
        mMap.put("Address4", editAddress4ET.getText().toString());
        mMap.put("Address5", editAddress5ET.getText().toString());
        mMap.put("user_id", JaoharPreference.readString(mActivity,JaoharPreference.STAFF_ID,""));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAddCompanyAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addCompanyRequest(mParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    JaoharConstants.IS_COMPANY_EDIT = true;
                    showAlerDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" +mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    public static void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }


}
