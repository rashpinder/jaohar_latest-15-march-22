package jaohar.com.jaohar.activities.universal_invoice_module;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.j4velin.lib.colorpicker.ColorPickerDialog;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.TemplatesAdapter;
import jaohar.com.jaohar.beans.TemplatesInvoicModel;
import jaohar.com.jaohar.interfaces.TemplatesInterFace;
import jaohar.com.jaohar.models.GetAllInvoiceTemplate;
import jaohar.com.jaohar.models.InvoiceDataModel;
import jaohar.com.jaohar.models.OwnerByIdModel;
import jaohar.com.jaohar.models.OwnersModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class Edit_InvocingCompanyActivity extends BaseActivity {
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;
    Activity mActivity = Edit_InvocingCompanyActivity.this;
    String TAG = Edit_InvocingCompanyActivity.this.getClass().getSimpleName();
    LinearLayout llLeftLL, headingsColorLL, contentColorLL, table_headColorLL, logoTEXTColorLL, table_alterColorLL, table_border_and_gradient_ColorLL;
    ImageView imgBack, mImage, itemDeleteIV, image2, itemImagedelete;
    private TextView txtCenter, headerColorTV, logoTEXTColorTV, contentColorTV, table_headColorTV, table_alterColorTV, table_border_and_gradient_ColorTV;
    private RecyclerView templatesRV;
    private Button add_BTN;
    private CropImageView cropImageView;
    private TemplatesAdapter mAdapter;
//    private ArrayList<TemplatesInvoicModel> mArrayLIST = new ArrayList<>();
    private ArrayList<InvoiceDataModel> mArrayLIST = new ArrayList<>();
    private String cameraStr = Manifest.permission.CAMERA;
    private String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE, mCurrentPhotoPath, mStoragePath, strBase64;
    Bitmap rotate = null;
    Boolean IsIMAGE1 = false;
    Boolean IsIMAGE2 = false;
    private String strBase64IMAGE1 = "", strBase64IMAGE2 = "", strOwnerID = "";
    private EditText editCompanyNameET, editInvoicePrefixET, editOwnerEmailET, editAddresLineOneET, editAddresLineTwoET, editAddresLineThreeET, editRegistryNumberET, editFiscalCodeET, editIMONoET, editPhoneNumberET, editlogoTextET, editSignatureET;
    /**
     * Persist URI image to crop URI if specific permissions are required
     */
    private Uri mCropImageUri;
    Uri uri;
    TemplatesInterFace mTemPlateInterFace = new TemplatesInterFace() {
        @Override
        public void mTemplatesInterface(InvoiceDataModel mModel) {
            headerColorTV.setText(mModel.getHeadingColor());
            headingsColorLL.setBackgroundColor(Color.parseColor(mModel.getHeadingColor()));
            contentColorTV.setText(mModel.getContentColor());
            contentColorLL.setBackgroundColor(Color.parseColor(mModel.getContentColor()));
            table_headColorTV.setText(mModel.getTableHeadColor());
            table_headColorLL.setBackgroundColor(Color.parseColor(mModel.getTableHeadColor()));
            table_alterColorTV.setText(mModel.getTableRowColor());
            table_alterColorLL.setBackgroundColor(Color.parseColor(mModel.getTableRowColor()));
            table_border_and_gradient_ColorTV.setText(mModel.getBorderGradientColor());
            table_border_and_gradient_ColorLL.setBackgroundColor(Color.parseColor(mModel.getBorderGradientColor()));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__invocing_company);
        if (getIntent() != null) {
            if (getIntent().getStringExtra("owner_ID") != null) {
                strOwnerID = getIntent().getStringExtra("owner_ID");
            }
        }

        setViewsId();
        setClickListners();
    }


    protected void setViewsId() {
        add_BTN = (Button) findViewById(R.id.add_BTN);
        templatesRV = (RecyclerView) findViewById(R.id.templatesRV);
        cropImageView = (CropImageView) findViewById(R.id.cropImageView);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        headingsColorLL = (LinearLayout) findViewById(R.id.headingsColorLL);
        logoTEXTColorLL = (LinearLayout) findViewById(R.id.logoTEXTColorLL);
        table_border_and_gradient_ColorLL = (LinearLayout) findViewById(R.id.table_border_and_gradient_ColorLL);
        contentColorLL = (LinearLayout) findViewById(R.id.contentColorLL);
        table_headColorLL = (LinearLayout) findViewById(R.id.table_headColorLL);
        itemDeleteIV = (ImageView) findViewById(R.id.itemDeleteIV);
        itemImagedelete = (ImageView) findViewById(R.id.itemImagedelete);
        table_alterColorLL = (LinearLayout) findViewById(R.id.table_alterColorLL);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        logoTEXTColorTV = (TextView) findViewById(R.id.logoTEXTColorTV);
        headerColorTV = (TextView) findViewById(R.id.headerColorTV);
        contentColorTV = (TextView) findViewById(R.id.contentColorTV);
        table_headColorTV = (TextView) findViewById(R.id.table_headColorTV);
        table_alterColorTV = (TextView) findViewById(R.id.table_alterColorTV);
        table_border_and_gradient_ColorTV = (TextView) findViewById(R.id.table_border_and_gradient_ColorTV);
        txtCenter.setText(getResources().getString(R.string.edit_company_));
        mImage = (ImageView) findViewById(R.id.mImage);
        image2 = (ImageView) findViewById(R.id.image2);
        editCompanyNameET = (EditText) findViewById(R.id.editCompanyNameET);
        editInvoicePrefixET = (EditText) findViewById(R.id.editInvoicePrefixET);
        editOwnerEmailET = (EditText) findViewById(R.id.editOwnerEmailET);
        editAddresLineOneET = (EditText) findViewById(R.id.editAddresLineOneET);
        editAddresLineTwoET = (EditText) findViewById(R.id.editAddresLineTwoET);
        editAddresLineThreeET = (EditText) findViewById(R.id.editAddresLineThreeET);
        editRegistryNumberET = (EditText) findViewById(R.id.editRegistryNumberET);
        editFiscalCodeET = (EditText) findViewById(R.id.editFiscalCodeET);
        editIMONoET = (EditText) findViewById(R.id.editIMONoET);
        editPhoneNumberET = (EditText) findViewById(R.id.editPhoneNumberET);
        editlogoTextET = (EditText) findViewById(R.id.editlogoTextET);
        editSignatureET = (EditText) findViewById(R.id.editSignatureET);
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute  API*//*
            gettingImage();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    public void gettingImage() {
        mArrayLIST.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllInvoiceTemplate> call1 = mApiInterface.getAllInvoiceTemplatesRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<GetAllInvoiceTemplate>() {
            @Override
            public void onResponse(Call<GetAllInvoiceTemplate> call, retrofit2.Response<GetAllInvoiceTemplate> response) {
                Log.e(TAG, "******Response*****" + response);
                AlertDialogManager.hideProgressDialog();
                GetAllInvoiceTemplate mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    mArrayLIST=mModel.getData();
                setAdapter();
                gettingItemOFInvoiceCompany();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetAllInvoiceTemplate> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }



//    private void gettingImage() {
//        mArrayLIST.clear();
//        //    https://root.jaohar.com/Staging/JaoharWebServicesNew/GetAllInvoiceTemplates.php//user_id
//        String strUrl = JaoharConstants.GetAllInvoiceTemplates + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****ResponseALL****" + response);
//                parseResponse(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void parseResponse(String response) {
//        try {
//            JSONObject mJsonOBJ = new JSONObject(response);
//            String strStatus = mJsonOBJ.getString("status");
//            if (strStatus.equals("1")) {
//                JSONArray mjsonArrayData = mJsonOBJ.getJSONArray("data");
//                if (mjsonArrayData != null) {
//                    for (int i = 0; i < mjsonArrayData.length(); i++) {
//                        JSONObject mJSonDATA = mjsonArrayData.getJSONObject(i);
//                        TemplatesInvoicModel mModel = new TemplatesInvoicModel();
//                        if (!mJSonDATA.getString("template_id").equals("")) {
//                            mModel.setTemplate_id(mJSonDATA.getString("template_id"));
//                        }
//                        if (!mJSonDATA.getString("template_image").equals("")) {
//                            mModel.setTemplate_image(mJSonDATA.getString("template_image"));
//                        }
//                        if (!mJSonDATA.getString("heading_color").equals("")) {
//                            mModel.setHeading_color(mJSonDATA.getString("heading_color"));
//                        }
//                        if (!mJSonDATA.getString("content_color").equals("")) {
//                            mModel.setContent_color(mJSonDATA.getString("content_color"));
//                        }
//                        if (!mJSonDATA.getString("table_head_color").equals("")) {
//                            mModel.setTable_head_color(mJSonDATA.getString("table_head_color"));
//                        }
//                        if (!mJSonDATA.getString("table_row_color").equals("")) {
//                            mModel.setTable_row_color(mJSonDATA.getString("table_row_color"));
//                        }
//                        if (!mJSonDATA.getString("border_gradient_color").equals("")) {
//                            mModel.setBorder_gradient_color(mJSonDATA.getString("border_gradient_color"));
//                        }
//                        mArrayLIST.add(mModel);
//                    }
//
//                    setAdapter();
//                    gettingItemOFInvoiceCompany();
//                }
//
//
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonOBJ.getString("message"));
//
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    private void setAdapter() {
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        templatesRV.setLayoutManager(horizontalLayoutManager);
        mAdapter = new TemplatesAdapter(mActivity, mArrayLIST, mTemPlateInterFace);
        templatesRV.setAdapter(mAdapter);
    }


    protected void setClickListners() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        add_BTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mActivity, "Comming Soon ...", Toast.LENGTH_LONG).show();

            }
        });
        mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    IsIMAGE1 = true;
                    IsIMAGE2 = false;
                    onSelectImageClick();
                } else {
                    IsIMAGE1 = true;
                    IsIMAGE2 = false;
                    requestPermission();
                }


            }
        });
        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    IsIMAGE2 = true;
                    IsIMAGE1 = false;
                    onSelectImageClick();
                } else {
                    IsIMAGE2 = true;
                    IsIMAGE1 = false;
                    requestPermission();
                }


            }
        });

        itemDeleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImage.setImageResource(R.drawable.palace_holder);
                itemDeleteIV.setVisibility(View.GONE);
                IsIMAGE1 = false;
                strBase64 = "";
                rotate = null;
            }
        });
        itemImagedelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image2.setImageResource(R.drawable.palace_holder);
                itemImagedelete.setVisibility(View.GONE);
                IsIMAGE2 = false;
                strBase64 = "";
                rotate = null;
            }
        });

        headingsColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int headerColor;
                if(contentColorTV.getText().toString().equals("")){
                    headerColor=Color.BLACK;

                }else {
                    headerColor = Color.parseColor(headerColorTV.getText().toString());
                }

                getColorCode(headingsColorLL, headerColorTV, headerColor);
            }
        });
        contentColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int contentColor;
                if(contentColorTV.getText().toString().equals("")){
                    contentColor=Color.BLACK;

                }else {
                    contentColor = Color.parseColor(contentColorTV.getText().toString());
                }

                getColorCode(contentColorLL, contentColorTV, contentColor);
            }
        });
        table_headColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tableHeaderColor;
                if(table_headColorTV.getText().toString().equals("")){
                    tableHeaderColor=Color.BLACK;

                }else {
                    tableHeaderColor = Color.parseColor(table_headColorTV.getText().toString());
                }

                getColorCode(table_headColorLL, table_headColorTV, tableHeaderColor);
            }
        });

        table_alterColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tableAlterColor;
                if(table_alterColorTV.getText().toString().equals("")){
                    tableAlterColor=Color.BLACK;

                }else {
                    tableAlterColor = Color.parseColor(table_alterColorTV.getText().toString());
                }

                getColorCode(table_alterColorLL, table_alterColorTV, tableAlterColor);
            }
        });

        table_border_and_gradient_ColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tableBorderColor;
                if(table_border_and_gradient_ColorTV.getText().toString().equals("")){
                    tableBorderColor=Color.BLACK;

                }else {
                    tableBorderColor = Color.parseColor(table_border_and_gradient_ColorTV.getText().toString());
                }


                getColorCode(table_border_and_gradient_ColorLL, table_border_and_gradient_ColorTV, tableBorderColor);
            }
        });
        logoTEXTColorLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getColorCode(logoTEXTColorLL, logoTEXTColorTV, Color.BLACK);
            }
        });


        add_BTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (headerColorTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_select_templates));

                } else if (editCompanyNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_company_name));
                } else if (editInvoicePrefixET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_enter_invoice_prefix));
                } else if (editlogoTextET.getText().toString().equals("") && strBase64IMAGE1.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_add_logo_text_or_logo_image));
                } else {
                    if (!editOwnerEmailET.getText().toString().equals("")) {
                        if (Utilities.isValidEmaillId(editOwnerEmailET.getText().toString()) == false) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                        } else {
                            /*Execute API to Add Company*/
                            if (!Utilities.isNetworkAvailable(mActivity)) {
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                            } else {
                                /*ExecuteApi*/
                                executeEDITAPI();
                            }
                        }
                    } else {
                        /*Execute API to Add Company*/
                        if (!Utilities.isNetworkAvailable(mActivity)) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            /*ExecuteApi*/
                            executeEDITAPI();
                        }
                    }

                }
            }
        });
    }

    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick() {
        CropImage.startPickImageActivity(this);
    }


    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
//                .setGuidelines(CropImageView.Guidelines.OFF).setAspectRatio(1,1)
//                .setFixAspectRatio(true)

                .setMultiTouchEnabled(false)
                .start(this);
    }


    /*********
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/

    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }


    void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                            onSelectImageClick();
//                            startCropImageActivity(mCropImageUri);
//                            startActivity(new Intent(SplashSlidesActivity.this, SelectionActivity.class));
//                            finish();
//                            Toast.makeText(context,"on",Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            requestPermission();
//                            permissionAccepted = false;
                        }
                    }
                }
                break;
        }
    }

    private void getColorCode(final LinearLayout colorLL, final TextView colorTV, int color) {
        ColorPickerDialog dialog = new ColorPickerDialog(mActivity, color);
        dialog.setOnColorChangedListener(new ColorPickerDialog.OnColorChangedListener() {
            @Override
            public void onColorChanged(int color) {
                // apply new color
                String hexColor = String.format("#%06X", (0xFFFFFF & color));
                colorTV.setText(hexColor);
                colorLL.setBackgroundColor(color);

            }

        });
        dialog.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                if (IsIMAGE1 == true) {
                    Bitmap bitmap = null;
                    try {

                        final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//                        String encodedImage = encodeImage(selectedImage);

                        //                    mImage.setImageURI(result.getUri());
                        mImage.setImageBitmap(selectedImage);
                        strBase64IMAGE1 = getBase64String(selectedImage);
                        itemDeleteIV.setVisibility(View.GONE);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } else if (IsIMAGE2 == true) {
                    try {
                        final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        image2.setImageBitmap(selectedImage);
                        itemImagedelete.setVisibility(View.GONE);
                        strBase64IMAGE2 = getBase64String(selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    //    https://root.jaohar.com/Staging/JaoharWebServicesNew/EditOwner.php

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("owner_id", strOwnerID);
        mMap.put("owner_name", editCompanyNameET.getText().toString().trim());
        mMap.put("email", editOwnerEmailET.getText().toString().trim());
        mMap.put("address1", editAddresLineOneET.getText().toString().trim());
        mMap.put("address2", editAddresLineTwoET.getText().toString().trim());
        mMap.put("address3", editAddresLineThreeET.getText().toString().trim());
        mMap.put("registry_no", editRegistryNumberET.getText().toString().trim());
        mMap.put("fiscal_code", editFiscalCodeET.getText().toString().trim());
        mMap.put("logo", strBase64IMAGE1);
        mMap.put("footer", strBase64IMAGE2);
        mMap.put("color", contentColorTV.getText().toString().trim());
        mMap.put("head_color", headerColorTV.getText().toString().trim());
        mMap.put("table_head_color", table_headColorTV.getText().toString().trim());
        mMap.put("table_row_color", table_alterColorTV.getText().toString().trim());
        mMap.put("gradient_color", table_border_and_gradient_ColorTV.getText().toString().trim());
        mMap.put("prefix", editInvoicePrefixET.getText().toString().trim());
        mMap.put("imo_no", editIMONoET.getText().toString().trim());
        mMap.put("phone", editPhoneNumberET.getText().toString().trim());
        mMap.put("signature", editSignatureET.getText().toString().trim());
        mMap.put("logo_text", editlogoTextET.getText().toString().trim());
        mMap.put("logo_text_color", logoTEXTColorTV.getText().toString().trim());
        mMap.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeEDITAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editOwnerRequest(mParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mAlerDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

//    public void executeEDITAPI() {
//        AlertDialogManager.showProgressDialog(mActivity);
//        String url = JaoharConstants.Edit_Owner;
//        JSONObject jsonObject = new JSONObject();
//        try {
//
//            jsonObject.put("owner_id", strOwnerID);
//            jsonObject.put("owner_name", editCompanyNameET.getText().toString().trim());
//            jsonObject.put("email", editOwnerEmailET.getText().toString().trim());
//            jsonObject.put("address1", editAddresLineOneET.getText().toString().trim());
//            jsonObject.put("address2", editAddresLineTwoET.getText().toString().trim());
//            jsonObject.put("address3", editAddresLineThreeET.getText().toString().trim());
//            jsonObject.put("registry_no", editRegistryNumberET.getText().toString().trim());
//            jsonObject.put("fiscal_code", editFiscalCodeET.getText().toString().trim());
//            jsonObject.put("logo", strBase64IMAGE1);
//            jsonObject.put("footer", strBase64IMAGE2);
//            jsonObject.put("color", contentColorTV.getText().toString().trim());
//            jsonObject.put("head_color", headerColorTV.getText().toString().trim());
//            jsonObject.put("table_head_color", table_headColorTV.getText().toString().trim());
//            jsonObject.put("table_row_color", table_alterColorTV.getText().toString().trim());
//            jsonObject.put("gradient_color", table_border_and_gradient_ColorTV.getText().toString().trim());
//            jsonObject.put("prefix", editInvoicePrefixET.getText().toString().trim());
//            jsonObject.put("imo_no", editIMONoET.getText().toString().trim());
//            jsonObject.put("phone", editPhoneNumberET.getText().toString().trim());
//            jsonObject.put("signature", editSignatureET.getText().toString().trim());
//            jsonObject.put("logo_text", editlogoTextET.getText().toString().trim());
//            jsonObject.put("logo_text_color", logoTEXTColorTV.getText().toString().trim());
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                url, jsonObject,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        AlertDialogManager.hideProgressDialog();
//                        Log.d(TAG, response.toString());
//                        try {
//                            if (response.getString("status").equals("1")) {
//                                mAlerDialog(mActivity, getString(R.string.app_name), response.getString("message"));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                VolleyLog.d(TAG, "Error: " + error.getMessage());
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
////      Adding request to request queue
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjReq);
//    }

    public void mAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();

        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return base64String;
    }

    public void gettingItemOFInvoiceCompany() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<OwnerByIdModel> call1 = mApiInterface.getOwnerByIdRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<OwnerByIdModel>() {
            @Override
            public void onResponse(Call<OwnerByIdModel> call, retrofit2.Response<OwnerByIdModel> response) {
                Log.e(TAG, "******Response*****" + response);
                AlertDialogManager.hideProgressDialog();
                OwnerByIdModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                        editCompanyNameET.setText(mModel.getData().getOwnerName());
                        editAddresLineOneET.setText(mModel.getData().getAddress1());
                    editAddresLineTwoET.setText(mModel.getData().getAddress2());
                    editAddresLineThreeET.setText(mModel.getData().getAddress3());
                    editOwnerEmailET.setText(mModel.getData().getEmail());
                    editRegistryNumberET.setText(mModel.getData().getRegistryNo());
                    editFiscalCodeET.setText(mModel.getData().getFiscalCode());
                    editIMONoET.setText(mModel.getData().getImoNo());
                    strBase64IMAGE1 = mModel.getData().getLogo();
                    Picasso.get().load(strBase64IMAGE1).into(mImage);
                    editPhoneNumberET.setText(mModel.getData().getPhone());
                    strBase64IMAGE2 = mModel.getData().getFooter();
                    Picasso.get().load(strBase64IMAGE2).into(image2);
                    contentColorTV.setText(mModel.getData().getColor());
                    contentColorLL.setBackgroundColor(Color.parseColor(mModel.getData().getColor()));
                    headerColorTV.setText(mModel.getData().getHeadColor());
                    headingsColorLL.setBackgroundColor(Color.parseColor(mModel.getData().getHeadColor()));
                    table_headColorTV.setText(mModel.getData().getTableHeadColor());
                    table_headColorLL.setBackgroundColor(Color.parseColor(mModel.getData().getTableHeadColor()));
                    table_alterColorTV.setText(mModel.getData().getTableRowColor());
                    table_alterColorLL.setBackgroundColor(Color.parseColor(mModel.getData().getTableRowColor()));
                    table_border_and_gradient_ColorTV.setText(mModel.getData().getGradientColor());
                    table_border_and_gradient_ColorLL.setBackgroundColor(Color.parseColor(mModel.getData().getGradientColor()));
                    editInvoicePrefixET.setText(mModel.getData().getPrefix());
                    editSignatureET.setText(mModel.getData().getSignature());
                    editlogoTextET.setText(mModel.getData().getLogoText());
                    logoTEXTColorTV.setText(mModel.getData().getLogoTextColor());
                    logoTEXTColorLL.setBackgroundColor(Color.parseColor(mModel.getData().getLogoTextColor()));
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<OwnerByIdModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    private void gettingItemOFInvoiceCompany() {
////      https://root.jaohar.com/Staging/JaoharWebServicesNew/GetOwnerById.php
//
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strUrl = JaoharConstants.GetOwnerById + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&owner_id=" + strOwnerID;
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//
//                Log.e(TAG, "*****ResponseALL****" + response);
//                parseResponce(response);
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
//
//    private void parseResponce(String response) {
//        JSONObject mJSonObject = null;
//        try {
//            mJSonObject = new JSONObject(response);
//
//            String strStatus = mJSonObject.getString("status");
//            String strMessage = mJSonObject.getString("message");
//            if (strStatus.equals("1")) {
//                JSONObject mJsonData = mJSonObject.getJSONObject("data");
//
//                if (!mJsonData.getString("owner_name").equals("")) {
//                    editCompanyNameET.setText(mJsonData.getString("owner_name"));
//                }
//                if (!mJsonData.getString("address1").equals("")) {
//                    editAddresLineOneET.setText(mJsonData.getString("address1"));
//                }
//                if (!mJsonData.getString("address1").equals("")) {
//                    editAddresLineOneET.setText(mJsonData.getString("address1"));
//                }
//                if (!mJsonData.getString("address2").equals("")) {
//                    editAddresLineTwoET.setText(mJsonData.getString("address2"));
//                }
//                if (!mJsonData.getString("address3").equals("")) {
//                    editAddresLineThreeET.setText(mJsonData.getString("address3"));
//                }
//                if (!mJsonData.getString("email").equals("")) {
//                    editOwnerEmailET.setText(mJsonData.getString("email"));
//                }
//                if (!mJsonData.getString("registry_no").equals("")) {
//                    editRegistryNumberET.setText(mJsonData.getString("registry_no"));
//                }
//                if (!mJsonData.getString("fiscal_code").equals("")) {
//                    editFiscalCodeET.setText(mJsonData.getString("fiscal_code"));
//                }
//                if (!mJsonData.getString("imo_no").equals("")) {
//                    editIMONoET.setText(mJsonData.getString("imo_no"));
//                }
//                if (!mJsonData.getString("logo").equals("")) {
//                    strBase64IMAGE1 = mJsonData.getString("logo");
//                    Picasso.get().load(strBase64IMAGE1).into(mImage);
//                }
//                if (!mJsonData.getString("phone").equals("")) {
//                    editPhoneNumberET.setText(mJsonData.getString("phone"));
//                }
//                if (!mJsonData.getString("header").equals("")) {
//
//                }
//                if (!mJsonData.getString("footer").equals("")) {
//                    strBase64IMAGE2 = mJsonData.getString("footer");
//                    Picasso.get().load(strBase64IMAGE2).into(image2);
//                }
//                if (!mJsonData.getString("color").equals("")) {
//                    contentColorTV.setText(mJsonData.getString("color"));
//                    contentColorLL.setBackgroundColor(Color.parseColor(mJsonData.getString("color")));
//                }
//                if (!mJsonData.getString("head_color").equals("")) {
//                    headerColorTV.setText(mJsonData.getString("head_color"));
//                    headingsColorLL.setBackgroundColor(Color.parseColor(mJsonData.getString("head_color")));
//                }
//                if (!mJsonData.getString("table_head_color").equals("")) {
//                    table_headColorTV.setText(mJsonData.getString("table_head_color"));
//                    table_headColorLL.setBackgroundColor(Color.parseColor(mJsonData.getString("table_head_color")));
//                }
//                if (!mJsonData.getString("table_row_color").equals("")) {
//                    table_alterColorTV.setText(mJsonData.getString("table_row_color"));
//                    table_alterColorLL.setBackgroundColor(Color.parseColor(mJsonData.getString("table_row_color")));
//                }
//                if (!mJsonData.getString("gradient_color").equals("")) {
//                    table_border_and_gradient_ColorTV.setText(mJsonData.getString("gradient_color"));
//                    table_border_and_gradient_ColorLL.setBackgroundColor(Color.parseColor(mJsonData.getString("gradient_color")));
//                }
//                if (!mJsonData.getString("prefix").equals("")) {
//                    editInvoicePrefixET.setText(mJsonData.getString("prefix"));
//                }
//                if (!mJsonData.getString("signature").equals("")) {
//                    editSignatureET.setText(mJsonData.getString("signature"));
//                }
//                if (!mJsonData.getString("logo_text").equals("")) {
//                    editlogoTextET.setText(mJsonData.getString("logo_text"));
//                }
//                if (!mJsonData.getString("logo_text_color").equals("")) {
//                    logoTEXTColorTV.setText(mJsonData.getString("logo_text_color"));
//                    logoTEXTColorLL.setBackgroundColor(Color.parseColor(mJsonData.getString("logo_text_color")));
//                }
//            } else {
//                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//            }
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
}
