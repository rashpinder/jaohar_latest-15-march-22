package jaohar.com.jaohar.activities.blog_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.blog_adapter.Blog_List_Admin_Adapter;
import jaohar.com.jaohar.beans.blog_module.Blog_List_Model;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.interfaces.blog_module.Blog_Admin_DeleteInterface;
import jaohar.com.jaohar.interfaces.blog_module.Blog_Item_Click_Interface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

import static android.view.View.GONE;

public class BlogListAdminActivity extends BaseActivity implements PaginationListForumAdapter, Blog_Item_Click_Interface, Blog_Admin_DeleteInterface {
    /**
     * set Activity
     **/
    Activity mActivity = BlogListAdminActivity.this;

    /**
     * set Activity TAG
     **/
    String TAG = BlogListAdminActivity.this.getClass().getSimpleName();

    /**
     * Widgets AND Adapters,Strings, Array List,Boolean and Integers
     **/

    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgRight)
    ImageView imgRight;
    @BindView(R.id.blogRV)
    RecyclerView blogRV;
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;
    @BindView(R.id.progressBottomPB)
    ProgressBar progressBottomPB;
    @BindView(R.id.btnBlogsCategory)
    Button btnBlogsCategory;
    @BindView(R.id.imgRightLL)
    RelativeLayout imgRightLL;
    @BindView(R.id.BlogCategoryLL)
    RelativeLayout BlogCategoryLL;

    ArrayList<Blog_List_Model> modelArrayList = new ArrayList<>();
    Blog_List_Admin_Adapter mAdapter;
    int page_num = 1;
    PaginationListForumAdapter mPaination;
    String strLastPage = "FALSE", strBlogID = "";
    boolean isSwipeRefresh = false;

    Blog_Item_Click_Interface mBlogClickInterface;

    Blog_Admin_DeleteInterface mDeleteInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        setContentView(R.layout.activity_blog_list_admin);
        ButterKnife.bind(this);
//        setData();
        mPaination = this;
        mBlogClickInterface = this;
        mDeleteInterface = this;
    }


    /*
     *SetUp IDs to Views
     **/
    @Override
    protected void setViewsIDs() {

        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                modelArrayList.clear();
                page_num = 1;
                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    gettingBlogDATA();
                }
            }
        });

        /* set tool bar */
        txtCenter.setText(getString(R.string.blogs));
        imgRight.setImageResource(R.drawable.add_icon);
        imgBack.setImageResource(R.drawable.back);


    }

    /*
     *SetUp Clicks to Views
     **/
    @OnClick({R.id.llLeftLL,R.id.btnBlogsCategory,R.id.imgRightLL,R.id.BlogCategoryLL})
    public  void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.btnBlogsCategory:
               startActivity(new Intent(mActivity, BlogCategoryListActivity.class));
                break;
            case R.id.imgRightLL:
                startActivity(new Intent(mActivity, AddBlogsActivity.class));
                break;
            case R.id.BlogCategoryLL:
               startActivity(new Intent(mActivity, BlogCategoryListActivity.class));
                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void setData() {
        modelArrayList.clear();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            page_num = 1;
            gettingBlogDATA();
        }
    }


    /*
     * API to Get All Blog Delete Using API
     * @param
     * @user_id
     * @blog_id
     * */
    private void deleteBlogDATA() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteBlog(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""),strBlogID);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {

                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    modelArrayList.clear();
                        page_num = 1;
                        gettingBlogDATA();
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                    }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
            }
        });

    }

    /*
     * API to Get All Blog list Using API
     * */
    private void gettingBlogDATA() {
        if (!strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_num == 1) {
                AlertDialogManager.showProgressDialog(mActivity);
                isSwipeRefresh = false;
                swipeToRefresh.setRefreshing(false);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllBlog(String.valueOf(page_num));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                if (isSwipeRefresh == true) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }
                Log.e(TAG, "*****Response****" + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = String.valueOf(mJsonObject.getInt("status"));
                    if (strStatus.equals("1")) {
                        parseResponce(response.body().toString());
                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mJsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
            }
        });

   }


    /*
     * Data Parsing
     * */
    private void parseResponce(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
            strLastPage = mjsonDATA.getString("last_page");
            JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_blogs");
            if (mjsonArrayData != null) {
                ArrayList<Blog_List_Model> mTempraryList = new ArrayList<>();
                mTempraryList.clear();
                for (int i = 0; i < mjsonArrayData.length(); i++) {
                    JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                    Blog_List_Model mModel = new Blog_List_Model();
                    if (!mJsonDATA.getString("blog_id").equals("")) {
                        mModel.setBlog_id(mJsonDATA.getString("blog_id"));
                    }
                    if (!mJsonDATA.getString("category").equals("")) {
                        mModel.setCategory(mJsonDATA.getString("category"));
                    }
                    if (!mJsonDATA.getString("tag").equals("")) {
                        mModel.setTag(mJsonDATA.getString("tag"));
                    }

                    if (!mJsonDATA.getString("title").equals("")) {
                        mModel.setTitle(mJsonDATA.getString("title"));
                    }
                    if (!mJsonDATA.getString("image").equals("")) {
                        mModel.setImage(mJsonDATA.getString("image"));
                    }
                    if (!mJsonDATA.getString("content").equals("")) {
                        mModel.setContent(mJsonDATA.getString("content"));
                    }
                    if (!mJsonDATA.getString("created_by").equals("")) {
                        mModel.setCreated_by(mJsonDATA.getString("created_by"));
                    }
                    if (!mJsonDATA.getString("creation_date").equals("")) {
                        mModel.setCreation_date(mJsonDATA.getString("creation_date"));
                    }
                    if (!mJsonDATA.getString("share_url").equals("")) {
                        mModel.setShare_url(mJsonDATA.getString("share_url"));
                    }

                    mTempraryList.add(mModel);
                }

                modelArrayList.addAll(mTempraryList);

                if (page_num == 1) {
                    setAdapter();
                } else {
                    mAdapter.notifyDataSetChanged();
                }
//                    setAdapter();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     * Adapter
     **/
    private void setAdapter() {
        blogRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new Blog_List_Admin_Adapter(mActivity, modelArrayList, mPaination, mBlogClickInterface, mDeleteInterface);
        mAdapter.setHasStableIds(true);
        blogRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void mPaginationforVessels(boolean isLastScroll) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (strLastPage.equals("FALSE")) {
                    if (progressBottomPB != null) {
                        progressBottomPB.setVisibility(View.VISIBLE);
                    }
                    swipeToRefresh.setEnabled(true);
                    ++page_num;
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        gettingBlogDATA();
                    }
                } else {
                    if (progressBottomPB != null) {
                        progressBottomPB.setVisibility(GONE);
                    }
                }
            }
        }, 150);
    }

    @Override
    public void BlogInterface(Blog_List_Model mModel) {
        Intent mIntent = new Intent(mActivity, BlogDescriptionAdminActivity.class);
        mIntent.putExtra("model", mModel);
        startActivity(mIntent);
    }

    @Override
    public void mBlog_Admin(Blog_List_Model mModel, String strType) {

        strBlogID = mModel.getBlog_id();

        if (strType.equals("del")) {
            deleteConfirmDialog(getResources().getString(R.string.are_you_sure_want_to_delete_));
        } else {
            Intent mIntent = new Intent(mActivity, EditBlogActivity.class);
            mIntent.putExtra("model", mModel);
            startActivity(mIntent);
        }
    }

    /* *
     * PopUp to Delete or Cancel the Message
     * */
    public void deleteConfirmDialog(String strMessage) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /* *
                 * Execute API to Delete Blog Category
                 * */
                deleteBlogDATA();
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }
}
