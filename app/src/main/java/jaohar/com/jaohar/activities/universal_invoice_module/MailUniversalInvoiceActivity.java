package jaohar.com.jaohar.activities.universal_invoice_module;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.SelectedEmailShowListAdapter;
import jaohar.com.jaohar.adapters.ShowListEmailAdapter;
import jaohar.com.jaohar.adapters.ShowfromDATAEmailsAdapter;
import jaohar.com.jaohar.interfaces.SelectEmailListItem;
import jaohar.com.jaohar.interfaces.SelectFromEmailInterface;
import jaohar.com.jaohar.interfaces.SelectedAndUnselectedEmailListInterFace;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class MailUniversalInvoiceActivity extends BaseActivity {
    private RelativeLayout clossRL;
    private RecyclerView mRecyclerViewShowLIST, mRecyclerView, mRecyclerViewFROM;
    private LinearLayout listShowLL, listDisplayItem, listDisplayFromItem;
    private EditText editToET, editSubjectET, editFROMET;
    private Button btnSend, btnCancel;
    private String strRecordID = "", strSubject = "", strOwnerID = "";
    private Activity mActivity = MailUniversalInvoiceActivity.this;
    String TAG = MailUniversalInvoiceActivity.this.getClass().getSimpleName();
    ArrayList<String> mRecordIDStr = new ArrayList<>();
    static ArrayList<String> mStringFROM = new ArrayList<>();
    static ArrayList<String> idd = new ArrayList<>();
    ArrayList<String> filteredList = new ArrayList<>();
    ArrayList<String> mfilteredFromList = new ArrayList<>();
    ArrayList<String> mSelectedListArray = new ArrayList<>();
    static String isAllVessal = "";
    String strTO;
    String strFROM;
    String id;
    ShowfromDATAEmailsAdapter mShowfromDATAEmailsAdapter;
    ShowListEmailAdapter mShowListAdapter;
    SelectedEmailShowListAdapter mShowSelectedListAdapter;

    SelectFromEmailInterface mSeletFromEmail = new SelectFromEmailInterface() {
        @Override
        public void mSelectEmail(String strEmail, String idd) {
            id=idd;
            editFROMET.setText(strEmail);
            listDisplayFromItem.setVisibility(View.GONE);
            gettingFromEmail();
        }
    };

    SelectEmailListItem mSelectedItem = new SelectEmailListItem() {
        @Override
        public void mSelectEmailListItem(String strEmail, String cc) {
            editToET.setText("");
            listShowLL.setVisibility(View.GONE);
            mSelectedListArray.add(strEmail);
            setUpSelectedItems();
        }

    };

    SelectedAndUnselectedEmailListInterFace selectedAndUnselectedEmailListInterFace = new SelectedAndUnselectedEmailListInterFace() {
        @Override
        public void mSelectedEmail(String strEmail,String cc) {
            if (mSelectedListArray.size() > 0) {
                mSelectedListArray.remove(strEmail);
                setUpSelectedItems();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_universal_invoice);

        if (getIntent() != null) {
            if (getIntent().getStringExtra("vesselID") != null) {
                strRecordID = getIntent().getStringExtra("vesselID");
                strSubject = getIntent().getStringExtra("SubjectName");
                isAllVessal = "1";
            }
            if (getIntent().getStringExtra("vesselIDUniversal") != null) {
                strRecordID = getIntent().getStringExtra("vesselIDUniversal");
                strSubject = getIntent().getStringExtra("SubjectName");
                strOwnerID = getIntent().getStringExtra("OwnerID");
                isAllVessal = "4";
            }
            if (getIntent().getStringArrayListExtra("recordIDArray") != null) {
                mRecordIDStr.clear();
                strSubject = getIntent().getStringExtra("vessalName1");
                mRecordIDStr = getIntent().getStringArrayListExtra("recordIDArray");
                Log.e(TAG, "ArrayLIST_DATA============= " + mRecordIDStr);
                isAllVessal = "2";
            }
            if (getIntent().getStringExtra("vessalName2") != null) {
                strSubject = getIntent().getStringExtra("vessalName2");
                isAllVessal = "3";
            }
        }
    }

    @Override
    protected void setViewsIDs() {
        listDisplayItem = findViewById(R.id.listDisplayItem);
        listDisplayFromItem = findViewById(R.id.listDisplayFromItem);
        listShowLL = findViewById(R.id.listShowLL);
        editFROMET = findViewById(R.id.editFROMET);
        mRecyclerView = findViewById(R.id.mRecyclerView);
        mRecyclerViewShowLIST = findViewById(R.id.mRecyclerViewShowLIST);
        mRecyclerViewFROM = findViewById(R.id.mRecyclerViewFROM);
        clossRL = findViewById(R.id.clossRL);
        editToET = findViewById(R.id.editToET);
        editSubjectET = findViewById(R.id.editSubjectET);
        btnCancel = findViewById(R.id.btnCancel);
        btnSend = findViewById(R.id.btnSend);
        editSubjectET.setText(strSubject);

        if (Utilities.isNetworkAvailable(mActivity)) {
            gettingFromEmail();
        } else {
            AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.internetconnection));
        }
    }

    @Override
    protected void setClickListner() {
        clossRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        editToET.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                listDisplayFromItem.setVisibility(View.GONE);
                if (!query.toString().equals("")) {
                    listShowLL.setVisibility(View.VISIBLE);
                    query = query.toString().toLowerCase();
                    filteredList.clear();
                    for (int i = 0; i < JaoharConstants.mEmailsArrayLIST.size(); i++) {
                        Log.e("test", "onTextChanged: " + JaoharConstants.mEmailsArrayLIST.get(i).toString());
                        final String text = JaoharConstants.mEmailsArrayLIST.get(i).toString();
                        if (text.contains(query)) {
                            filteredList.add(JaoharConstants.mEmailsArrayLIST.get(i).toString());
                        }
                    }
                    setShowListAdapter(filteredList);
                } else {
                    listShowLL.setVisibility(View.GONE);
                }
            }
        });
        editFROMET.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                if (!query.toString().equals("")) {
                    listDisplayFromItem.setVisibility(View.VISIBLE);
                    query = query.toString().toLowerCase();
                    mfilteredFromList.clear();
                    for (int i = 0; i < mStringFROM.size(); i++) {
                        Log.e("test", "onTextChanged: " + mStringFROM.get(i).toString());
                        final String text = mStringFROM.get(i);
                        if (text.contains(query)) {
                            mfilteredFromList.add(mStringFROM.get(i));
                        }
                    }
                    mShowfromDATAEmailsAdapter = new ShowfromDATAEmailsAdapter(mActivity, mfilteredFromList, mSeletFromEmail, idd);
                    mRecyclerViewFROM.setAdapter(mShowfromDATAEmailsAdapter);
                } else {
                    listDisplayFromItem.setVisibility(View.GONE);
                    mShowfromDATAEmailsAdapter = new ShowfromDATAEmailsAdapter(mActivity, mStringFROM, mSeletFromEmail, idd);
                    mRecyclerViewFROM.setAdapter(mShowfromDATAEmailsAdapter);
                }
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editToET.getText().toString().equals("")) {
                    if (mSelectedListArray.size() > 0) {
                        if (editSubjectET.getText().toString().equals("")) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_subject));
                        } else if (editFROMET.getText().toString().equals("")) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_from_subject));

                        } else {
                            if (!Utilities.isNetworkAvailable(mActivity)) {
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                            } else {
                                if (mStringFROM.contains(editFROMET.getText().toString())) {
                                    sendEamilAPI(editToET.getText().toString(), editSubjectET.getText().toString(), strRecordID, editFROMET.getText().toString());

                                } else {
                                    editFROMET.setText("");
                                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_from_subject));

//                                    https://root.jaohar.com/Staging/JaoharWebServicesNew/MailSingleInvoice.php
                                    //                            sendEamilAPI(editToET.getText().toString(), editSubjectET.getText().toString(), strRecordID);

                                }
                            }
                        }
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                    }
                } else {
                    if (Utilities.isValidEmaillId(editToET.getText().toString()) != true) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                    } else if (editSubjectET.getText().toString().equals("")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_subject));
                    } else if (editFROMET.getText().toString().equals("")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_from_subject));

                    } else {
                        if (!Utilities.isNetworkAvailable(mActivity)) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            if (mStringFROM.contains(editFROMET.getText().toString())) {
                                sendEamilAPI(editToET.getText().toString(), editSubjectET.getText().toString(), strRecordID, editFROMET.getText().toString());

                            } else {
                                editFROMET.setText("");
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_from_subject));
                            }
                        }
                    }
                }
            }
        });
        editSubjectET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                listDisplayFromItem.setVisibility(View.GONE);
                return false;
            }
        });

        editFROMET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(mStringFROM.size()>0){

                    listDisplayFromItem.setVisibility(View.VISIBLE);
                    mShowfromDATAEmailsAdapter = new ShowfromDATAEmailsAdapter(mActivity,mStringFROM , mSeletFromEmail, idd);
                    mRecyclerViewFROM.setAdapter(mShowfromDATAEmailsAdapter);
                }else {

                    gettingFromEmail();
                    listDisplayFromItem.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });

        editFROMET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mStringFROM.size()>0){
                    editFROMET.setText("");
                    listDisplayFromItem.setVisibility(View.VISIBLE);
                    mShowfromDATAEmailsAdapter = new ShowfromDATAEmailsAdapter(mActivity,mStringFROM , mSeletFromEmail, idd);
                    mRecyclerViewFROM.setAdapter(mShowfromDATAEmailsAdapter);
                }else {
                    editFROMET.setText("");
                    gettingFromEmail();
                    listDisplayFromItem.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void gettingFromEmail() {
        mStringFROM.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getInvoiceMailsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    if (mJsonObject.getString("status").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            if (!mJsonArray.getString(i).toString().equals("")) {
                                String email = mJsonArray.getJSONObject(i).get("email").toString();
                                String id = mJsonArray.getJSONObject(i).get("id").toString();
                                mStringFROM.add(email);
                                idd.add(id);
                            }
                        }
                        setShowListFromAdapter();
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonObject.getString("message"));
                    }

                    Log.e(TAG, "*****Response****" + "Array List All EMAILS SIZE ======  " + JaoharConstants.mEmailsArrayLIST.size());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    private  void gettingFromEmail(){
//        mStringFROM.clear();
////        https://root.jaohar.com/Staging/JaoharWebServicesNew/GetInvoiceMails.php
//        String strUrl = JaoharConstants.Get_Invoice_Mails+"?user_id="+ JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//
//                Log.e(TAG, "******" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if(mJsonObject.getString("status").equals("1")){
//                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
//                        for (int i = 0; i < mJsonArray.length(); i++) {
//                            if(!mJsonArray.getString(i).toString().equals("")){
//                                mStringFROM.add(mJsonArray.getString(i));
//                            }
//
//                        }
//                        setShowListFromAdapter();
//                    }else {
//                        AlertDialogManager.showAlertDialog(mActivity,getResources().getString(R.string.app_name), mJsonObject.getString("message"));
//                    }
//
//                    Log.e(TAG, "*****Response****" + "Array List All EMAILS SIZE ======  "+ JaoharConstants.mEmailsArrayLIST.size());
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void setShowListAdapter(ArrayList<String> mFilterArray) {
        Log.e(TAG, "FilterArray: " + mFilterArray.size());
        mRecyclerViewShowLIST.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewShowLIST.setNestedScrollingEnabled(false);
        mRecyclerViewShowLIST.setHasFixedSize(false);
        mRecyclerViewShowLIST.setLayoutManager(layoutManager);
        mShowListAdapter = new ShowListEmailAdapter(mActivity, mFilterArray, mSelectedItem, "to");
        mRecyclerViewShowLIST.setAdapter(mShowListAdapter);
    }

    private void setUpSelectedItems() {
        Log.e(TAG, "SelectedArray: " + mSelectedListArray.size());
        mRecyclerView.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(layoutManager);
        mShowSelectedListAdapter = new SelectedEmailShowListAdapter(mActivity, mSelectedListArray, selectedAndUnselectedEmailListInterFace,"to");
        mRecyclerView.setAdapter(mShowSelectedListAdapter);
    }

    public void setShowListFromAdapter() {
        mRecyclerViewFROM.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewFROM.setNestedScrollingEnabled(false);
        mRecyclerViewFROM.setHasFixedSize(false);
        mRecyclerViewFROM.setLayoutManager(layoutManager);
        mShowfromDATAEmailsAdapter = new ShowfromDATAEmailsAdapter(mActivity, mStringFROM, mSeletFromEmail,idd);
        mRecyclerViewFROM.setAdapter(mShowfromDATAEmailsAdapter);
    }

    private void sendEamilAPI(final String strTO, final String strSubject, final String strRecordID, final String strFROM) {
        this.strFROM = strFROM;
        this.strTO = strTO;

//        if (isAllVessal.equals("2")) {
//            executeMailMultipleInvoiceApi();
//        } else if (isAllVessal.equals("1")) {
            executeMailSingleInvoiceApi();
//        } else if (isAllVessal.equals("3")) {
//            executeMailAllInvoiceApi();
//        } else if (isAllVessal.equals("4")) {
//            executeMailSingleUniversalInvoiceApi();
//        }
    }
////        String strUrl = JaoharConstants.Mail_With_Doc + strTO + "&subject=" + strSubject + "&vessel_id=" + strRecordID;

//    private Map<String, String> mParam() {
//        Map<String, String> mMap = new HashMap<>();
//
//        mMap.put("to_email", strTO);
//        if(mSelectedListArray.size()>0){
//            mMap.put("to_emails", String.valueOf(mSelectedListArray));
//        }else {
//            mMap.put("to_emails","");
//        }
//        mMap.put("subject", strSubject);
//        mMap.put("from_mail", strFROM);
//        if (isAllVessal.equals("2")) {
//            Log.e(TAG, "ArrayLIST_DATA============= " + String.valueOf(mRecordIDStr));
//            mMap.put("invoice_ids", String.valueOf(mRecordIDStr));
//        } else if(isAllVessal.equals("1")){
//            mMap.put("invoice_id", strRecordID);
//        }else if(isAllVessal.equals("4")){
//            mMap.put("invoice_id", strRecordID);
//        }
//        if(isAllVessal.equals("4")){
//            mMap.put("owner_id",strOwnerID);
//        }
//        Log.e(TAG, "**PARAM**" + mMap.toString());
//        return mMap;
//    }


    private void executeMailSingleUniversalInvoiceApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.mailSingleUniversalInvoiceRequest(strTO, String.valueOf(mSelectedListArray), strSubject, id, String.valueOf(mRecordIDStr), strRecordID, strOwnerID).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void executeMailAllInvoiceApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.mailAllInvoiceRequest(strTO, String.valueOf(mSelectedListArray), strSubject, id, String.valueOf(mRecordIDStr), strRecordID, strOwnerID).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void executeMailSingleInvoiceApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.mailSingleUniversalInvoiceRequest(strTO, String.valueOf(mSelectedListArray), strSubject, id, String.valueOf(mRecordIDStr), strRecordID, strOwnerID).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void executeMailMultipleInvoiceApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.mailMultipleInvoiceRequest(strTO, String.valueOf(mSelectedListArray), strSubject, id, String.valueOf(mRecordIDStr), strRecordID, strOwnerID).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    //    private void sendEamilAPI(final String strTO, final String strSubject, final String strRecordID, final String strFROM) {
////      http://jaohar.com/index.php/MailVesselDetails/mail_with_doc
////      http://jaohar.com/index.php/MailVesselDetails/multiple_vessels_api
////      https://root.jaohar.com/Staging/JaoharWebServicesNew/MailAllInvoices.php
////      https://root.jaohar.com/Staging/JaoharWebServicesNew/MailSingleUniversalInvoice.php
//        String strUrl = "";
//        if (isAllVessal.equals("2")) {
//            strUrl = JaoharConstants.Mail_Multiple_Invoices;
//        } else if(isAllVessal.equals("1")){
//            strUrl = JaoharConstants.Mail_Single_Invoice;
//        } else if(isAllVessal.equals("3")){
//            strUrl = JaoharConstants.Mail_All_Invoices;
//        } else if(isAllVessal.equals("4")){
//            strUrl = JaoharConstants.Mail_Single_Universal_Invoice;
//        }
//////        String strUrl = JaoharConstants.Mail_With_Doc + strTO + "&subject=" + strSubject + "&vessel_id=" + strRecordID;
////
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*************" + response.toString());
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.getString("status").equals("1")) {
//                        showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else if (jsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("to_email", strTO);
//                if(mSelectedListArray.size()>0){
//                    params.put("to_emails", String.valueOf(mSelectedListArray));
//                }else {
//                    params.put("to_emails","");
//                }
//                params.put("subject", strSubject);
//                params.put("from_mail", strFROM);
//                if (isAllVessal.equals("2")) {
//                    Log.e(TAG, "ArrayLIST_DATA============= " + String.valueOf(mRecordIDStr));
//                    params.put("invoice_ids", String.valueOf(mRecordIDStr));
//                } else if(isAllVessal.equals("1")){
//                    params.put("invoice_id", strRecordID);
//                }else if(isAllVessal.equals("4")){
//                    params.put("invoice_id", strRecordID);
//                }
//                if(isAllVessal.equals("4")){
//                    params.put("owner_id",strOwnerID);
//                }
//                return params;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//
//    }
    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

//    @Override
//    public void onBackPressed() {
//
//        if (isAllVessal.equals("4")) {
//            super.onBackPressed();
//            Intent mIntent = new Intent(mActivity, AllUniversalInvoiceActivity.class);
//            startActivity(mIntent);
//            finish();
//        } else {
//            super.onBackPressed();
//            Intent mIntent = new Intent(mActivity, AllInVoicesActivity.class);
//            startActivity(mIntent);
//            finish();
//        }
//    }
}
