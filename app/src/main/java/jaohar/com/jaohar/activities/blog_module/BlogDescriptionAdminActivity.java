package jaohar.com.jaohar.activities.blog_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.blog_adapter.Blog_Comments_Admin_Adapter;
import jaohar.com.jaohar.beans.blog_module.Blog_Details_Model;
import jaohar.com.jaohar.beans.blog_module.Blog_List_Model;
import jaohar.com.jaohar.beans.blog_module.Blog_Replies_Model;
import jaohar.com.jaohar.beans.blog_module.Blog_User_Detail_Model;
import jaohar.com.jaohar.interfaces.blog_module.BlogListClickInterface;
import jaohar.com.jaohar.interfaces.blog_module.BlogRepliesdelInterface;
import jaohar.com.jaohar.interfaces.blog_module.DeleteBlogCommentsInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class BlogDescriptionAdminActivity extends BaseActivity implements BlogListClickInterface, DeleteBlogCommentsInterface, BlogRepliesdelInterface {

    /**
     * set Activity
     **/
    Activity mActivity = BlogDescriptionAdminActivity.this;

    /**
     * set Activity TAG
     **/
    String TAG = BlogDescriptionAdminActivity.this.getClass().getSimpleName();

    /**
     * Widgets AND Adapters,Strings, Array List,Boolean and Integers
     **/
    @BindView(R.id.commentsRV)
    RecyclerView commentsRV;
    @BindView(R.id.commentsET)
    EditText commentsET;
    @BindView(R.id.blogImageIV)
    ImageView blogImageIV;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgRight)
    ImageView imgRight;
    @BindView(R.id.sendIMG)
    ImageView sendIMG;
    @BindView(R.id.blogHeadingTV)
    TextView blogHeadingTV;
    @BindView(R.id.blogDateTV)
    TextView blogDateTV;
    @BindView(R.id.blogContentDetailTV)
    TextView blogContentDetailTV;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.editMessageText)
    TextView editMessageText;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.imgRightLL)
    RelativeLayout imgRightLL;
    @BindView(R.id.messageReplyRL)
    RelativeLayout messageReplyRL;
    @BindView(R.id.crossRL)
    RelativeLayout crossRL;
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;
    Blog_List_Model mModel;

    ArrayList<Blog_Details_Model> mBlogDetailsArray = new ArrayList<>();
    ArrayList<Blog_Details_Model> mLoadMore = new ArrayList<Blog_Details_Model>();
    Blog_Comments_Admin_Adapter mAdapter;
    BlogListClickInterface mInterfaceData;
    String strReplyID = "", strType = "";
    DeleteBlogCommentsInterface mDeleteInterface;
    BlogRepliesdelInterface mDeleteRepliesInterface;

    boolean isSwipeRefresh = false;
    static int page_no = 0;
    private String strLastPage = "FALSE", strUserID = "";

    /**
     * Activity Default Method
     **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
        setContentView(R.layout.activity_blog_description_admin);
        ButterKnife.bind(this);
        mInterfaceData = this;
        mDeleteInterface = this;
        mDeleteRepliesInterface = this;
    }

    /*
     *SetUp IDs to Views
     **/
    @Override
    protected void setViewsIDs() {

        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);

        /* set tool bar */
        txtCenter.setText(getString(R.string.blog_details));
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        imgRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_share_blog));

        /*Retrive Data From Previous Activity*/
        if (getIntent().getSerializableExtra("model") != null) {
            mModel = (Blog_List_Model) getIntent().getSerializableExtra("model");
            blogHeadingTV.setText(mModel.getTitle());
            blogContentDetailTV.setText(mModel.getContent());

            /*
             * Convert Unix Time To Date
             **/
            long unix_seconds = Long.parseLong(mModel.getCreation_date());
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(unix_seconds * 1000L);
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            String date = format.format(cal.getTime());
            blogDateTV.setText(" " + date);
            if (!mModel.getImage().equals("")) {
                blogImageIV.setVisibility(View.VISIBLE);
                Glide.with(mActivity).asBitmap().load(mModel.getImage()).into(blogImageIV);
            } else {

            }

            strUserID = JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");


            /*
             * Getting All Comments Array
             * */

            if (Utilities.isNetworkAvailable(mActivity) == false) {
                showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
            } else {
                mBlogDetailsArray.clear();
                page_no = 1;
                executeAPI();
            }

            swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                           if (strLastPage.equals("FALSE")) {
                                swipeToRefresh.setRefreshing(false);
                                isSwipeRefresh = true;

                                page_no++;
                                executeAPI();
                            } else {
                                isSwipeRefresh = false;
                                swipeToRefresh.setRefreshing(false);
                            }
                        }
                    }, 500);

                }
            });


        }
    }


   @OnClick({R.id.llLeftLL,R.id.imgRightLL,R.id.crossRL,R.id.sendIMG})
   public  void onViewCLicked(View view)
   {
       switch (view.getId())
       {
           case R.id.llLeftLL:
               onBackPressed();
               break;
           case R.id.imgRightLL:
               performRightllClick();
               break;
           case R.id.crossRL:
               performCrossCLick();
               break;
           case R.id.sendIMG:
               performSendClcik();
               break;

       }
   }




    /*
     *SetUp Clicks to Views
     **/

    private void performRightllClick() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, "Jaohar Blogs");
        share.putExtra(Intent.EXTRA_TEXT, mModel.getShare_url());
        startActivity(Intent.createChooser(share, "Share text to..."));

    }
    private void performCrossCLick() {
        strReplyID = "";
        strType = "";
        messageReplyRL.setVisibility(View.GONE);
        editMessageText.setText("");
    }

    private void performSendClcik() {
        if (commentsET.getText().toString().trim().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_text));
        } else {
            if (Utilities.isNetworkAvailable(mActivity) == false) {
                showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
            } else {

                executeAddCommentAPI(strType);
            }
        }
    }




    /* *
     * Execute API for getting GetAllBlogCommentFront
     * @param
     * @user_id
     * @blog_id
     * @page_no
     * */
    public void executeAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllBlogCommentFront(strUserID,mModel.getBlog_id(), String.valueOf(page_no));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLADDResponce***" + response.body().toString());
                try {
                    JSONObject mJsonDATA = new JSONObject(response.body().toString());
                    if (mJsonDATA.getString("status").equals("1")) {
                        if (isSwipeRefresh) {
                            isSwipeRefresh = false;
                            swipeToRefresh.setRefreshing(false);
                        }
                        parseResponce(mJsonDATA);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());

            }
        });
            }


    /*
     * Parse Comments Data
     * */
    private void parseResponce(JSONObject mJsonDATA) {
        try {
            JSONObject mjsonDATA = mJsonDATA.getJSONObject("data");
            strLastPage = mjsonDATA.getString("last_page");
            JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_blogs");

            if (mjsonArrayData.length() > 0) {

                ArrayList<Blog_Details_Model> mTempArray = new ArrayList<>();
                mTempArray.clear();
                for (int i = 0; i < mjsonArrayData.length(); i++) {

                    JSONObject mJsonBlogsDATA = mjsonArrayData.getJSONObject(i);

                    Blog_Details_Model mDetailsMOdel = new Blog_Details_Model();

                    if (!mJsonBlogsDATA.getString("comment_id").equals("")) {
                        mDetailsMOdel.setComment_id(mJsonBlogsDATA.getString("comment_id"));
                    }
                    if (!mJsonBlogsDATA.getString("blog_id").equals("")) {
                        mDetailsMOdel.setBlog_id(mJsonBlogsDATA.getString("blog_id"));
                    }
                    if (!mJsonBlogsDATA.getString("reply_id").equals("")) {
                        mDetailsMOdel.setReply_id(mJsonBlogsDATA.getString("reply_id"));
                    }
                    if (!mJsonBlogsDATA.getString("message").equals("")) {
                        mDetailsMOdel.setMessage(mJsonBlogsDATA.getString("message"));
                    }
                    if (!mJsonBlogsDATA.getString("posted_by").equals("")) {
                        mDetailsMOdel.setPosted_by(mJsonBlogsDATA.getString("posted_by"));
                    }
                    if (!mJsonBlogsDATA.getString("posted_on").equals("")) {
                        mDetailsMOdel.setPosted_on(mJsonBlogsDATA.getString("posted_on"));
                    }

                    JSONObject mUserDetails = mJsonBlogsDATA.getJSONObject("user_detail");
                    Blog_User_Detail_Model mUserDetailModel = new Blog_User_Detail_Model();
                    if (!mUserDetails.getString("id").equals("")) {
                        mUserDetailModel.setId(mUserDetails.getString("id"));
                    }
                    if (!mUserDetails.getString("email").equals("")) {
                        mUserDetailModel.setEmail(mUserDetails.getString("email"));
                    }
                    if (!mUserDetails.getString("password").equals("")) {
                        mUserDetailModel.setPassword(mUserDetails.getString("password"));
                    }
                    if (!mUserDetails.getString("role").equals("")) {
                        mUserDetailModel.setRole(mUserDetails.getString("role"));
                    }
                    if (!mUserDetails.getString("status").equals("")) {
                        mUserDetailModel.setStatus(mUserDetails.getString("status"));
                    }
                    if (!mUserDetails.getString("created").equals("")) {
                        mUserDetailModel.setCreated(mUserDetails.getString("created"));
                    }
                    if (!mUserDetails.getString("enabled").equals("")) {
                        mUserDetailModel.setEnabled(mUserDetails.getString("enabled"));
                    }
                    if (!mUserDetails.getString("company_name").equals("")) {
                        mUserDetailModel.setCompany_name(mUserDetails.getString("company_name"));
                    }
                    if (!mUserDetails.getString("first_name").equals("")) {
                        mUserDetailModel.setFirst_name(mUserDetails.getString("first_name"));
                    }
                    if (!mUserDetails.getString("last_name").equals("")) {
                        mUserDetailModel.setLast_name(mUserDetails.getString("last_name"));
                    }
                    if (!mUserDetails.getString("job").equals("")) {
                        mUserDetailModel.setJob(mUserDetails.getString("job"));
                    }
                    if (!mUserDetails.getString("image").equals("")) {
                        mUserDetailModel.setImage(mUserDetails.getString("image"));
                    }
                    if (!mUserDetails.getString("chat_approve").equals("")) {
                        mUserDetailModel.setChat_approve(mUserDetails.getString("chat_approve"));
                    }
                    if (!mUserDetails.getString("chat_status").equals("")) {
                        mUserDetailModel.setChat_status(mUserDetails.getString("chat_status"));
                    }
                    if (!mUserDetails.getString("chat_support_approve").equals("")) {
                        mUserDetailModel.setChat_approve(mUserDetails.getString("chat_support_approve"));
                    }


                    mDetailsMOdel.setmUser_Detail_Model(mUserDetailModel);


                    JSONArray mJsonBlogRepliesArray = mJsonBlogsDATA.getJSONArray("blog_replies");
                    if (mJsonBlogRepliesArray.length() > 0) {
                        ArrayList<Blog_Replies_Model> mRepliesArray = new ArrayList<>();
                        for (int j = 0; j < mJsonBlogRepliesArray.length(); j++) {
                            Blog_Replies_Model mBlogRepliesModel = new Blog_Replies_Model();
                            JSONObject mJsonRepliesData = mJsonBlogRepliesArray.getJSONObject(j);
                            if (!mJsonRepliesData.getString("comment_id").equals("")) {
                                mBlogRepliesModel.setComment_id(mJsonRepliesData.getString("comment_id"));
                            }
                            if (!mJsonRepliesData.getString("blog_id").equals("")) {
                                mBlogRepliesModel.setBlog_id(mJsonRepliesData.getString("blog_id"));
                            }
                            if (!mJsonRepliesData.getString("reply_id").equals("")) {
                                mBlogRepliesModel.setReply_id(mJsonRepliesData.getString("reply_id"));
                            }
                            if (!mJsonRepliesData.getString("user_id").equals("")) {
                                mBlogRepliesModel.setUser_id(mJsonRepliesData.getString("user_id"));
                            }
                            if (!mJsonRepliesData.getString("message").equals("")) {
                                mBlogRepliesModel.setMessage(mJsonRepliesData.getString("message"));
                            }
                            if (!mJsonRepliesData.getString("posted_by").equals("")) {
                                mBlogRepliesModel.setPosted_by(mJsonRepliesData.getString("posted_by"));
                            }
                            if (!mJsonRepliesData.getString("posted_on").equals("")) {
                                mBlogRepliesModel.setPosted_on(mJsonRepliesData.getString("posted_on"));
                            }

                            JSONObject mUserReplyDetails = mJsonRepliesData.getJSONObject("user_detail");
                            Blog_User_Detail_Model mUserDetailReplyModel = new Blog_User_Detail_Model();
                            if (!mUserReplyDetails.getString("id").equals("")) {
                                mUserDetailReplyModel.setId(mUserReplyDetails.getString("id"));
                            }
                            if (!mUserReplyDetails.getString("email").equals("")) {
                                mUserDetailReplyModel.setEmail(mUserReplyDetails.getString("email"));
                            }
                            if (!mUserReplyDetails.getString("password").equals("")) {
                                mUserDetailReplyModel.setPassword(mUserReplyDetails.getString("password"));
                            }
                            if (!mUserReplyDetails.getString("role").equals("")) {
                                mUserDetailReplyModel.setRole(mUserReplyDetails.getString("role"));
                            }
                            if (!mUserReplyDetails.getString("status").equals("")) {
                                mUserDetailReplyModel.setStatus(mUserReplyDetails.getString("status"));
                            }
                            if (!mUserReplyDetails.getString("created").equals("")) {
                                mUserDetailReplyModel.setCreated(mUserReplyDetails.getString("created"));
                            }
                            if (!mUserReplyDetails.getString("enabled").equals("")) {
                                mUserDetailReplyModel.setEnabled(mUserReplyDetails.getString("enabled"));
                            }
                            if (!mUserReplyDetails.getString("company_name").equals("")) {
                                mUserDetailReplyModel.setCompany_name(mUserReplyDetails.getString("company_name"));
                            }
                            if (!mUserReplyDetails.getString("first_name").equals("")) {
                                mUserDetailReplyModel.setFirst_name(mUserReplyDetails.getString("first_name"));
                            }
                            if (!mUserReplyDetails.getString("last_name").equals("")) {
                                mUserDetailReplyModel.setLast_name(mUserReplyDetails.getString("last_name"));
                            }
                            if (!mUserReplyDetails.getString("job").equals("")) {
                                mUserDetailReplyModel.setJob(mUserReplyDetails.getString("job"));
                            }
                            if (!mUserReplyDetails.getString("image").equals("")) {
                                mUserDetailReplyModel.setImage(mUserReplyDetails.getString("image"));
                            }
                            if (!mUserReplyDetails.getString("chat_approve").equals("")) {
                                mUserDetailReplyModel.setChat_approve(mUserReplyDetails.getString("chat_approve"));
                            }
                            if (!mUserReplyDetails.getString("chat_status").equals("")) {
                                mUserDetailReplyModel.setChat_status(mUserReplyDetails.getString("chat_status"));
                            }
                            if (!mUserReplyDetails.getString("chat_support_approve").equals("")) {
                                mUserDetailReplyModel.setChat_approve(mUserReplyDetails.getString("chat_support_approve"));
                            }
                            mBlogRepliesModel.setmUser_Detail_Model(mUserDetailReplyModel);
                            mRepliesArray.add(mBlogRepliesModel);
                        }

                        /*
                         * Sort ArrayList according to time
                         * */
                        Collections.sort(mRepliesArray,
                                new Comparator<Blog_Replies_Model>() {
                                    public int compare(Blog_Replies_Model o1,
                                                       Blog_Replies_Model o2) {
                                        if (Long.parseLong(o1.getPosted_on()) ==
                                                Long.parseLong(o2.getPosted_on())) {
                                            return 1;
                                        } else if (Long.parseLong(o1.getPosted_on()) <
                                                Long.parseLong(o2.getPosted_on())) {
                                            return -1;
                                        }
                                        return 0;
                                    }
                                });

                        mDetailsMOdel.setmBlogRepliesModel(mRepliesArray);
                    }
                    mTempArray.add(mDetailsMOdel);
                }


                mBlogDetailsArray.addAll(mTempArray);


                /*
                 * Sort ArrayList according to time
                 * */
                Collections.sort(mBlogDetailsArray,
                        new Comparator<Blog_Details_Model>() {
                            public int compare(Blog_Details_Model o1,
                                               Blog_Details_Model o2) {
                                if (Long.parseLong(o1.getPosted_on()) ==
                                        Long.parseLong(o2.getPosted_on())) {
                                    return 1;
                                } else if (Long.parseLong(o1.getPosted_on()) <
                                        Long.parseLong(o2.getPosted_on())) {
                                    return -1;
                                }
                                return 0;
                            }
                        });


                if (page_no == 1) {
                    /*SetUp Adapter*/
                    setUpAdapter();
                } else {
                    mAdapter.notifyDataSetChanged();
                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    /* *
     * Execute API for getting AddBlogComment
     * @param
     * @user_id
     * @blog_id
     * @comment
     * @reply_id (When add reply)
     * */
    public void executeAddCommentAPI(String strTypeMain) {
        AlertDialogManager.showProgressDialog(mActivity);


        String replyNew="";
        if (strTypeMain.equals("reply")) {
            replyNew=strReplyID;
        } else if (strType.equals("")) {
            replyNew="";
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.addBlogComment(strUserID,mModel.getBlog_id(), commentsET.getText().toString().trim(),replyNew);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    /*
                         * Getting All Comments Array
                         * */
                        page_no = 1;
                        strType = "";
                        mBlogDetailsArray.clear();
                        messageReplyRL.setVisibility(View.GONE);
                        editMessageText.setText("");
                        commentsET.setText("");
                        executeAPI();
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                    }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());

            }
        });


       }


    //Setting UpAdapter
    private void setUpAdapter() {
        Log.e(TAG, "Size====== " + mBlogDetailsArray.size());

        commentsRV.setNestedScrollingEnabled(false);
        commentsRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new Blog_Comments_Admin_Adapter(mActivity, mBlogDetailsArray, mInterfaceData, mDeleteInterface, mDeleteRepliesInterface);

        commentsRV.setAdapter(mAdapter);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @Override
    public void BlogInterface(Blog_Details_Model mModel, String strClickType) {
        strReplyID = mModel.getComment_id();
        if (strClickType.equals("reply")) {

            messageReplyRL.setVisibility(View.VISIBLE);
            editMessageText.setText("Replying to " + mModel.getmUser_Detail_Model().getFirst_name() + ": " + html2text(mModel.getMessage()));
            strType = "reply";
        } else {
            deleteConfirmDialog(getResources().getString(R.string.are_you_sure_want_to_delete_));
        }
    }

    @Override
    public void mDeleteBlogInterface(final Blog_Details_Model mModel, View view) {
        PopupMenu popup = new PopupMenu(mActivity, view);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.del_btn:


                        // EX : call intent if you want to swich to other activity
                        break;
                }
                return false;
            }
        });
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.option_menu_delete_blog, popup.getMenu());
        popup.show();
    }

    /* *
     * PopUp to Delete or Cancel the Message
     * */
    public void deleteConfirmDialog(String strMessage) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /* *
                 * Execute API to Delete Blog Comment
                 * */
                executedeleteAPI();
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    /* *
     * Execute API for getting DeleteBlogComment
     * @param
     * @user_id
     * @comment_id
     * */
    public void executedeleteAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
          ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteBlogComment(strUserID,strReplyID);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLADDResponce***" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    /*
                         * Getting All Comments Array
                         * */
                        strType = "";
                        mBlogDetailsArray.clear();
                        mAdapter.notifyDataSetChanged();
                        messageReplyRL.setVisibility(View.GONE);
                        editMessageText.setText("");
                        commentsET.setText("");
                        executeAPI();
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                    }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());

            }
        });


        }


    @Override
    public void mBlogRepliesInterface(Blog_Replies_Model mModel) {
        strReplyID = mModel.getComment_id();
        deleteConfirmDialog(getResources().getString(R.string.are_you_sure_want_to_delete_));


    }
}