package jaohar.com.jaohar.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.utils.Utilities;

public class ZoomableActivity extends AppCompatActivity {
    NestedScrollView mScreenShotScrollView;
    RelativeLayout parent;
    Button btnSubmit;
    Bitmap mBitmap;
    ImageView screenshotImageview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoomable);

        mScreenShotScrollView =  findViewById(R.id.mScreenShotScrollView);
        btnSubmit =findViewById(R.id.btnSubmit);
        parent =  findViewById(R.id.parent);
        screenshotImageview =  findViewById(R.id.screenshotImageview);

        setClickListner();
    }

    private void setClickListner() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBitmap = Utilities.getBitmapFromView(v, mScreenShotScrollView.getHeight(), mScreenShotScrollView.getWidth());
                screenshotImageview.setImageBitmap(mBitmap);
                parent.setVisibility(View.GONE);
            }
        });
    }


}
