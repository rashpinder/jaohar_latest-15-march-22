package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.EditInvoiceVesselActivity;
import jaohar.com.jaohar.adapters.InvoiceVesselsAdapter;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.InvoiceVesselDeleteInterface;
import jaohar.com.jaohar.interfaces.InvoiceVesselEditInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class SearchInvoiceVesselsActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = SearchInvoiceVesselsActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = SearchInvoiceVesselsActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.cancelTV)
    TextView cancelTV;
    @BindView(R.id.invoicesRV)
    RecyclerView vesselsRV;

    ArrayList<VesselSearchInvoiceModel> vesselArrayList = new ArrayList<VesselSearchInvoiceModel>();
    InvoiceVesselsAdapter mInvoiceVesselsAdapter;
    ArrayList<VesselSearchInvoiceModel> filteredList = new ArrayList<VesselSearchInvoiceModel>();

    InvoiceVesselDeleteInterface mInvoiceVesselDeleteInterface = new InvoiceVesselDeleteInterface() {
        @Override
        public void invoiceVesselDelete(VesselSearchInvoiceModel mModel, int position) {
            deleteConfirmDialog(mModel, position);
        }
    };

    InvoiceVesselEditInterface mInvoiceVesselEditInterface = new InvoiceVesselEditInterface() {
        @Override
        public void invoiceVesselEdit(VesselSearchInvoiceModel mModel) {
            Intent mIntent = new Intent(mActivity, EditInvoiceVesselActivity.class);
            mIntent.putExtra("Model", mModel);
            mActivity.startActivity(mIntent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_invoice_vessels);

        setStatusBar();

        getIntentData();

        ButterKnife.bind(this);
    }

    private void getIntentData() {
        vesselArrayList = (ArrayList<VesselSearchInvoiceModel>) getIntent().getSerializableExtra("QuestionListExtra");
    }

    @Override
    protected void setClickListner() {
        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                s = s.toString().toLowerCase();
                filteredList.clear();
                for (int k = 0; k < vesselArrayList.size(); k++) {
                    final String text = vesselArrayList.get(k).getVessel_name().toLowerCase();
                    if (text.contains(s)) {
                        filteredList.add(vesselArrayList.get(k));
                    }
                }
                vesselsRV.setLayoutManager(new LinearLayoutManager(mActivity));
                mInvoiceVesselsAdapter = new InvoiceVesselsAdapter(mActivity, filteredList, mInvoiceVesselDeleteInterface, mInvoiceVesselEditInterface);
                vesselsRV.setAdapter(mInvoiceVesselsAdapter);
                mInvoiceVesselsAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setAdapter() {
        Log.e(TAG, "VesselsAdapter: " + vesselArrayList.size());
        vesselsRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mInvoiceVesselsAdapter = new InvoiceVesselsAdapter(mActivity, vesselArrayList, mInvoiceVesselDeleteInterface, mInvoiceVesselEditInterface);
        vesselsRV.setAdapter(mInvoiceVesselsAdapter);
    }

    public void deleteConfirmDialog(final VesselSearchInvoiceModel mModel, int position) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_vessel));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mModel, position);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    public void executeDeleteAPI(VesselSearchInvoiceModel model, int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteInvoiceVesselsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), model.getVessel_id());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    vesselArrayList.remove(position);
                    filteredList.remove(position);

                    if (mInvoiceVesselsAdapter != null)
                        mInvoiceVesselsAdapter.notifyDataSetChanged();

                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }
}