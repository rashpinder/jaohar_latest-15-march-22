package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;

import androidx.core.content.FileProvider;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.krishna.fileloader.FileLoader;
import com.krishna.fileloader.listener.FileRequestListener;
import com.krishna.fileloader.pojo.FileResponse;
import com.krishna.fileloader.request.FileLoadRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.InVoicesModel;

public class DetailUniversalActivity extends BaseActivity {
    Activity mActivity = DetailUniversalActivity.this;
    String TAG = DetailUniversalActivity.this.getClass().getSimpleName();
    InVoicesModel mInVoicesModel;
    private ImageView imgBack;
    private LinearLayout llLeftLL;
    private TextView txtCenter, txtRight;
    private RelativeLayout imgRightLL;
    private WebView mWebView;
    private ProgressBar progressbar1;
    private String strPDF;
    private DownloadManager downloadManager;
    private long downloadReference;
    File mFinalFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_universal);
        if (getIntent() != null) {
            mInVoicesModel = (InVoicesModel) getIntent().getSerializableExtra("Model");
            strPDF = mInVoicesModel.getPdf();
        }
        if (!strPDF.equals("")) {
//            String extStorageDirectory = Environment.getExternalStorageDirectory()
//                    .toString();
//            File folder = new File(extStorageDirectory, "pdf");
//            folder.mkdir();
//            File file = new File(folder, "Read.pdf");
//            try {
//                file.createNewFile();
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            }
//           DownloadFile("http://122.248.233.68/pvfiles/Guide-2.pdf", file);

            downLoadFileFromUrl(strPDF);
        } else {
            Toast.makeText(mActivity, "No PDF", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = findViewById(R.id.llLeftLL);
        mWebView = findViewById(R.id.mWebView);
        progressbar1 = (ProgressBar) findViewById(R.id.progressbar1);
        txtCenter = findViewById(R.id.txtCenter);
        imgRightLL = findViewById(R.id.imgRightLL);
        txtRight = findViewById(R.id.txtRight);
        imgRightLL.setVisibility(View.VISIBLE);
        txtCenter.setText(getResources().getString(R.string.detail_universal_invoice));
        txtRight.setText(getResources().getString(R.string.share));
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);


        if (!strPDF.equals("")) {
            progressbar1.setVisibility(View.VISIBLE);
            // To Show PDF IN WebView
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setBuiltInZoomControls(true);
            mWebView.getSettings().setLoadWithOverviewMode(true);
            mWebView.getSettings().setUseWideViewPort(true);
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    progressbar1.setVisibility(View.VISIBLE);
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, final String url) {
                    progressbar1.setVisibility(View.GONE);
                }
            });


            mWebView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + strPDF);
        } else {
            Toast.makeText(mActivity, "No PDF", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentShareFile = new Intent(Intent.ACTION_SEND);
                Uri photoURI = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".fileprovider", mFinalFile);
                Log.e(TAG, "onClick: " + photoURI);

                intentShareFile.setType("application/pdf");

                intentShareFile.putExtra(Intent.EXTRA_STREAM, photoURI);
                intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                        mInVoicesModel.getInvoice_number());
                intentShareFile.putExtra(Intent.EXTRA_TEXT, "UNIVERSAL INVOICE PDF PLEASE CHECK :-");
                startActivity(Intent.createChooser(intentShareFile, "Share File"));
            }
        });
    }

    private void downLoadFileFromUrl(String pdfUrl) {
        FileLoader.with(this)
                .load(pdfUrl, true) //2nd parameter is optioal, pass true to force load from network
                .fromDirectory("BrillWill", FileLoader.DIR_INTERNAL)
                .asFile(new FileRequestListener<File>() {
                    @Override
                    public void onLoad(FileLoadRequest request, FileResponse<File> response) {
                        mFinalFile = response.getBody();
                        // do something with the file
                        Log.e(TAG, "==FILE==" + mFinalFile);

                    }

                    @Override
                    public void onError(FileLoadRequest request, Throwable t) {
                        Log.e(TAG, "==ERROR==");
                    }
                });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
