package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.invoices_module.ManageUtilitiesAdapter;
import jaohar.com.jaohar.beans.ManagerModule.ManagerModulesModel;
import jaohar.com.jaohar.interfaces.managermodules.ClickManagerModulesInterface;

public class ManageUtilitiesActivity extends BaseActivity implements ClickManagerModulesInterface {
    Activity mActivity = ManageUtilitiesActivity.this;
    String TAG = ManageUtilitiesActivity.this.getClass().getSimpleName();
    //WIDGETS
    ImageView imgBack, imgRight;
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    TextView txtCenter, txtRight;
    RecyclerView tabListRV;

    Button btnVesselStatus, btnVesselTypes, btnAllNews,
            btnAllInternalNews, btnAddLinksToHome,
            btnAddLinksToHomeForStaff, btnBlogs, btnModules;

    List<ManagerModulesModel> ManagerModulesModelList = new ArrayList<>();
    ClickManagerModulesInterface clickManagerModulesInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_manage_utilities);

        /* initialize ids */
        setViewsIDs();

        /* set click interface */
        clickManagerModulesInterface = this;

        /* set all dynamic additions list */
        setModulesList();
    }

    public void setViewsIDs() {
        /*SET UP TOOLBAR*/
        imgRight = findViewById(R.id.imgRight);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.GONE);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        btnVesselStatus = (Button) findViewById(R.id.btnVesselStatus);
        btnVesselTypes = (Button) findViewById(R.id.btnVesselTypes);
        btnAllNews = (Button) findViewById(R.id.btnAllNews);
        btnAllInternalNews = (Button) findViewById(R.id.btnAllInternalNews);
        btnAddLinksToHome = (Button) findViewById(R.id.btnAddLinksToHome);
        btnAddLinksToHomeForStaff = (Button) findViewById(R.id.btnAddLinksToHomeForStaff);
        btnBlogs = (Button) findViewById(R.id.btnBlogs);
        btnModules = (Button) findViewById(R.id.btnModules);
        tabListRV = findViewById(R.id.tabListRV);

        /* set tool bar */
        txtCenter.setText(getString(R.string.mng_utitlities));
//        imgRight.setImageResource(R.drawable.ic_magnifying_glass);
    }

    private void setModulesList() {
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.all_company), R.drawable.ic_in_add_company));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.vessel), R.drawable.ic_inv_vessel));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.currency), R.drawable.ic_in_currency));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.stamp), R.drawable.ic_in_stamp));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.sign), R.drawable.ic_in_sign));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.bank), R.drawable.ic_in_bank));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.in_company), R.drawable.ic_in_company_inv));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.mail_sign), R.drawable.ic_in_mail));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.benificiary), R.drawable.ic_in_beneficiary));

        /* set modules adapter */
        setAdapter();
    }

    private void setAdapter() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 2);
        tabListRV.setLayoutManager(gridLayoutManager);
        ManageUtilitiesAdapter mAdapter = new ManageUtilitiesAdapter(mActivity, ManagerModulesModelList, clickManagerModulesInterface);
        tabListRV.setAdapter(mAdapter);
    }

    @Override
    public void mClickManagerModulesInterface(int position, String name) {
        if (name.equalsIgnoreCase(getString(R.string.all_company))) {
            startActivity(new Intent(mActivity, AllCompaniesActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.vessel))) {
            startActivity(new Intent(mActivity, InvVesselActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.currency))) {
            startActivity(new Intent(mActivity, CurrencyActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.stamp))) {
            startActivity(new Intent(mActivity, StampActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.sign))) {
            startActivity(new Intent(mActivity, SignActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.bank))) {
            startActivity(new Intent(mActivity, BankActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.in_company))) {
            startActivity(new Intent(mActivity, InvoiceCompanyActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.mail_sign))) {
            startActivity(new Intent(mActivity, MailSignActivity.class));
            overridePendingTransitionEnter();
        }else if (name.equalsIgnoreCase(getString(R.string.benificiary))) {
            startActivity(new Intent(mActivity, BeneficiaryActivity.class));
            overridePendingTransitionEnter();
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        btnVesselStatus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(mActivity, AllStatusActivity.class));
//                overridePendingTransitionEnter();
//            }
//        });
//        btnVesselTypes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(mActivity, AllVesselTypesActivity.class));
//                overridePendingTransitionEnter();
//            }
//        });
//        btnAllNews.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(mActivity, AllNewsActivity.class));
//                overridePendingTransitionEnter();
//            }
//        });
//        btnAllInternalNews.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(mActivity, AllInternalNewsActivity.class));
//                overridePendingTransitionEnter();
//            }
//        });
//        btnAddLinksToHome.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(mActivity, AllLinkAdminActivity.class));
//                overridePendingTransitionEnter();
//            }
//        });
//        btnAddLinksToHomeForStaff.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(mActivity, AllLinkStaffActivity.class));
//                overridePendingTransitionEnter();
//            }
//        });
//
//        btnBlogs.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(mActivity, BlogListAdminActivity.class));
//                overridePendingTransitionEnter();
//            }
//        });
//
//        btnModules.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(mActivity, AdminModuleListActivity.class));
//                overridePendingTransitionEnter();
//
//            }
//        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }
}
