package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.fonts.ButtonPoppinsMedium;
import jaohar.com.jaohar.models.ContactByIdModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class AddInvCompanyActivity extends BaseActivity {
    private final Activity mActivity = AddInvCompanyActivity.this;
    /**
     * Widgets
     */
    @BindView(R.id.editInvCompanyNameET)
    EditText editInvCompanyNameET;
    @BindView(R.id.editCompanyET)
    EditText editCompanyET;
    @BindView(R.id.editInvoicePrefixET)
    EditText editInvoicePrefixET;
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editAddressLine1ET)
    EditText editAddressLine1ET;
    @BindView(R.id.editAddressLine2ET)
    EditText editAddressLine2ET;
    @BindView(R.id.editAddressLine3ET)
    EditText editAddressLine3ET;
    @BindView(R.id.editRegistryNoET)
    EditText editRegistryNoET;
    @BindView(R.id.editFiscalCodeET)
    EditText editFiscalCodeET;
    @BindView(R.id.editImoNumET)
    EditText editImoNumET;
    @BindView(R.id.editVatNumET)
    EditText editVatNumET;
    @BindView(R.id.editTicNumET)
    EditText editTicNumET;
    @BindView(R.id.editLogoTextET)
    EditText editLogoTextET;
    @BindView(R.id.editSignTextET)
    EditText editSignTextET;
    @BindView(R.id.editPdfPassET)
    EditText editPdfPassET;
    @BindView(R.id.btnAddComapnyB)
    ButtonPoppinsMedium btnAddComapnyB;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    private String strContactID = "", strUSERID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_inv_comapny);
        ButterKnife.bind(this);
        if (JaoharConstants.is_Staff_FragmentClick == true) {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
        }
    }


    @OnClick({R.id.llLeftLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (Utilities.isNetworkAvailable(mActivity) == false) {
//            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
//        } else {
//            executeAPIforGetting();
//        }
    }


}
