package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.invoices_module.AllCompaniesAdapter;

import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.interfaces.CompaniesClickInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AllCompaniesActivity extends BaseActivity {
    Activity mActivity = AllCompaniesActivity.this;
    String TAG = AllCompaniesActivity.this.getClass().getSimpleName();
    //Toolbar
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    ImageView imgBack;
    CompaniesModel mModel;
    TextView txtCenter;
    EditText editSearchET;
    ImageView imgSearch, imgAdd;
    ArrayList<String> mArrayListCompanys = new ArrayList<String>();
String user_id;
    //RecyclerView
    RecyclerView allCompaniesRV;
    ArrayList<CompaniesModel> mArrayList = new ArrayList<CompaniesModel>();
//    ArrayList<CompaniesModel> mArrayList = new ArrayList<CompaniesModel>();
    AllCompaniesAdapter mCompaniesAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_companies);
        setStatusBar();
        setViewsIDs();
        setClickListner();
//        if (JaoharConstants.is_Staff_FragmentClick == true) {
//            user_id = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        } else {
//            user_id = JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
//        }
        if(JaoharPreference.readString(mActivity, JaoharPreference.Role, null).equals("Admin")){
            user_id=JaoharPreference.readString(mActivity,JaoharPreference.ADMIN_ID,null);
        }
        else{
            user_id=JaoharPreference.readString(mActivity,JaoharPreference.STAFF_ID,null);
        }

    }


    CompaniesClickInterface mOnClickInterface = new CompaniesClickInterface() {
        @Override
        public void mOnClickInterface(CompaniesModel mArrayList, int position) {
            PerformOptionsClick(mArrayList,position);
        }

    };

    private void PerformOptionsClick(CompaniesModel mArrayList,final int position) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_invoice_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout downloadRL = view.findViewById(R.id.downloadRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);
        View copyView = view.findViewById(R.id.copyView);
        View mailView = view.findViewById(R.id.mailView);
        View downloadView = view.findViewById(R.id.downloadView);
        copyRL.setVisibility(View.GONE);
        viewRL.setVisibility(View.VISIBLE);
        mailRL.setVisibility(View.GONE);
        downloadRL.setVisibility(View.GONE);
        copyRL.setVisibility(View.GONE);
        copyView.setVisibility(View.GONE);
        mailView.setVisibility(View.GONE);
        downloadView.setVisibility(View.GONE);

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, EditCompanyActivity.class);
                mIntent.putExtra("Model", (Serializable) mArrayList);
                startActivity(mIntent);
                overridePendingTransitionEnter();
                dialog.dismiss();
            }
        });
        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, ViewCompanyActivity.class);
                startActivity(mIntent);
                overridePendingTransitionEnter();
                dialog.dismiss();
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteConfirmDialog(mArrayList, position);
                dialog.dismiss();
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            executeCompaniesAPI();
        }
    }

    protected void setViewsIDs() {
        editSearchET = findViewById(R.id.editSearchET);
        txtCenter = findViewById(R.id.txtCenter);
        llLeftLL = findViewById(R.id.llLeftLL);
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        allCompaniesRV = findViewById(R.id.allCompaniesRV);
        imgAdd = findViewById(R.id.imgAdd);
        imgSearch = findViewById(R.id.imgSearch);
    }

    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, AddCompanyActivity.class);
                startActivity(mIntent);
                overridePendingTransitionEnter();
            }
        });
    }


    public void executeCompaniesAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
            mArrayListCompanys.clear();
            ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            Call<JsonObject> call1 = mApiInterface.getAlCompaniesRequest(user_id);
            call1.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                    AlertDialogManager.hideProgressDialog();
                    parseResponse12(response.body().toString());
                    }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    AlertDialogManager.hideProgressDialog();
                    Log.e(TAG, "******error*****" + t.getMessage());
                }
            });
        }


    private void parseResponse12(String response) {

        try {
            JSONObject mJsonObject = new JSONObject(response);
            String strStatus = mJsonObject.getString("status");
            if (strStatus.equals("1")) {
                JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJson = mJsonArray.getJSONObject(i);
                    jaohar.com.jaohar.beans.CompaniesModel mModel = new jaohar.com.jaohar.beans.CompaniesModel();

                    if (!mJson.isNull("id")) {
                        mModel.setId(mJson.getString("id"));
                    }
                    if (!mJson.isNull("company_name")) {
                        mModel.setCompany_name(mJson.getString("company_name"));
                        mArrayListCompanys.add(mJson.getString("company_name"));
                    }
                    if (!mJson.isNull("Address1")) {
                        mModel.setAddress1(mJson.getString("Address1"));
                    }
                    if (!mJson.isNull("Address2")) {
                        mModel.setAddress2(mJson.getString("Address2"));
                    }
                    if (!mJson.isNull("Address3")) {
                        mModel.setAddress3(mJson.getString("Address3"));
                    }
                    if (!mJson.isNull("Address4")) {
                        mModel.setAddress4(mJson.getString("Address4"));
                    }
                    if (!mJson.isNull("Address5")) {
                        mModel.setAddress5(mJson.getString("Address5"));
                    }
                    mArrayList.add(mModel);
                }
                /*SetAdapter*/
                setAdapter();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//
//        imgSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(mActivity, SearchInvoiceCurrencyActivity.class);
//                intent.putExtra("QuestionListExtra", mArrayList);
//                startActivityForResult(intent, 777);
//                overridePendingTransitionEnter();
//            }
//        });

//        editSearchET.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                charSequence = charSequence.toString().toLowerCase();
//                filteredList.clear();
//                for (int k = 0; k < mArrayList.size(); k++) {
//                    final String text = mArrayList.get(k).getAlias_name().toLowerCase();
//                    if (text.contains(charSequence)) {
//                        filteredList.add(mArrayList.get(k));
//                    }
//                }
//                allCompaniesRV.setLayoutManager(new LinearLayoutManager(mActivity));
//                mCompaniesAdapter = new CurrenciesAdapter(mActivity, filteredList, mDeleteCurrencyInterface, mEditCurrencyInterface);
//                allCompaniesRV.setAdapter(mCompaniesAdapter);
//                mCompaniesAdapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private void setAdapter() {
        Log.e(TAG, "Companies: " + mArrayList.size());
        allCompaniesRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mCompaniesAdapter = new AllCompaniesAdapter(mActivity,mArrayList,mOnClickInterface);
        allCompaniesRV.setAdapter(mCompaniesAdapter);
    }


    public void deleteConfirmDialog(final CompaniesModel mCompaniesModel, int position) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_company));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mCompaniesModel, position);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    private void executeDeleteAPI(CompaniesModel mCompaniesModel, int position) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteCompaniesRequest(user_id, mCompaniesModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
                          @Override
                          public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              StatusMsgModel mModel = response.body();
                              if (mModel.getStatus() == 1) {
                                  mArrayList.remove(position);

                                  if (mCompaniesAdapter != null)
                                      mCompaniesAdapter.notifyDataSetChanged();

                              } else if (mModel.getStatus() == 100) {
                                  AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                              } else {
                                  AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check that it is the Activity with an OK result
        if (requestCode == 777) {
            if (data != null && data.getSerializableExtra("Model") != null) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", data.getSerializableExtra("Model"));
                mActivity.setResult(777, returnIntent);
                mActivity.finish();
            }
        }
    }
}
