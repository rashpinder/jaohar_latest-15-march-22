package jaohar.com.jaohar.activities.invoices_module;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.fonts.ButtonPoppinsMedium;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;

public class EditBeneficiaryActivity extends BaseActivity {
    private final Activity mActivity = EditBeneficiaryActivity.this;
    /**
     * Widgets
     */
    @BindView(R.id.beneficiaryNameET)
    EditText beneficiaryNameET;
    @BindView(R.id.beneficiaryAddressET)
    EditText beneficiaryAddressET;
    @BindView(R.id.editCountryET)
    EditText editCountryET;
    @BindView(R.id.editBankNameET)
    EditText editBankNameET;
    @BindView(R.id.editIBanRonET)
    EditText editIBanRonET;
    @BindView(R.id.editIBanUsdET)
    EditText editIBanUsdET;
    @BindView(R.id.editIBanEurET)
    EditText editIBanEurET;
    @BindView(R.id.editIBanGbpET)
    EditText editIBanGbpET;
    @BindView(R.id.editSwiftET)
    EditText editSwiftET;
    @BindView(R.id.savebtn)
    ButtonPoppinsMedium savebtn;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    private String strContactID = "", strUSERID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_beneficiary);
        ButterKnife.bind(this);
        if (JaoharConstants.is_Staff_FragmentClick == true) {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else {
            strUSERID = JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
        }
    }


    @OnClick({R.id.llLeftLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }}
