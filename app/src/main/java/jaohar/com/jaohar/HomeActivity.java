package jaohar.com.jaohar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddMailingListActivity;
import jaohar.com.jaohar.activities.LoginActivity;
import jaohar.com.jaohar.activities.ManagerActivity;
import jaohar.com.jaohar.activities.VesselForSaleActivity;
import jaohar.com.jaohar.activities.invoices_module.ManageUtilitiesActivity;
import jaohar.com.jaohar.adapters.modules_adapter.SideMenuModulesAdapter;
import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.fragments.AboutFragment;
import jaohar.com.jaohar.fragments.BlogFragment;
import jaohar.com.jaohar.fragments.ContactUsFragment;
import jaohar.com.jaohar.fragments.FavoriteVesselFragment;
import jaohar.com.jaohar.fragments.HomeFragment;
import jaohar.com.jaohar.fragments.LoginFragment;
import jaohar.com.jaohar.fragments.LoginUserFragment;
import jaohar.com.jaohar.fragments.NotificationFragment;
import jaohar.com.jaohar.fragments.OpenLinkFragment;
import jaohar.com.jaohar.fragments.OurHistoryFragment;
import jaohar.com.jaohar.fragments.OurOfficesFragment;
import jaohar.com.jaohar.fragments.ServicesFragment;
import jaohar.com.jaohar.fragments.StaffFragment;
import jaohar.com.jaohar.fragments.SupportFragment;
import jaohar.com.jaohar.fragments.UserProfileFragment;
import jaohar.com.jaohar.fragments.VesselsForSaleFragment;
import jaohar.com.jaohar.fragments.WebLoginFragment;
import jaohar.com.jaohar.fragments.sidemenufromapifragments.AddressBookListStaffFragment;
import jaohar.com.jaohar.fragments.sidemenufromapifragments.AllInVoicesFragment;
import jaohar.com.jaohar.fragments.sidemenufromapifragments.AllInternalNewsFragment;
import jaohar.com.jaohar.fragments.sidemenufromapifragments.AllNewsFragment;
import jaohar.com.jaohar.fragments.sidemenufromapifragments.AllUniversalInvoiceFragment;
import jaohar.com.jaohar.fragments.sidemenufromapifragments.AllVesselsFragment;
import jaohar.com.jaohar.fragments.sidemenufromapifragments.ChatUsersListFragment;
import jaohar.com.jaohar.fragments.sidemenufromapifragments.EducationalBlogsFragment;
import jaohar.com.jaohar.fragments.sidemenufromapifragments.ForumListFragment;
import jaohar.com.jaohar.fragments.sidemenufromapifragments.MaillingStaffFragment;
import jaohar.com.jaohar.fragments.sidemenufromapifragments.UkOfficeForumListFragment;
import jaohar.com.jaohar.interfaces.modules.ClickSideMenuModuleInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.models.profileandtabsmodels.ModulesItem;
import jaohar.com.jaohar.models.profileandtabsmodels.ProfileResponse;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import jaohar.com.jaohar.views.SimpleSideDrawer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {

    Activity mActivity = HomeActivity.this;
    String TAG = HomeActivity.this.getClass().getSimpleName();

    /* Widgets */
    private RecyclerView modulesRV;
    private LinearLayout modulesLL;
    private ProgressBar modulesProgressBar;

    public static LinearLayout llLeftLL, spinnerLL;
    public static RelativeLayout imgRightLL, notificationCountRL, notificationCountBgRl;
    public static TextView txtCenter, textLL, notificationCountTV;
    public static ImageView imgBack, imgRight, jaoharlogoImg, imgAdd, imgDraft,imgManageUtilitiesIV;
    public static RelativeLayout mainRL;
    public static LinearLayout bottomMainLL;
    public static Spinner selectSpinner;
    public static TextView signTV, nameTV, typetv, roletv, badgesTV, badgesTVUser;
    public static LinearLayout profileLL, toolBarMainLL;
    public static LinearLayout profileClickLL;
    public static CircleImageView profilePICIMG;
    public RelativeLayout notificationRL, notificationRLUser;
    public SimpleSideDrawer mSimpleSideDrawer;
    public LinearLayout homeLL, aboutLL, servicesLL, ourOfficesLL, vesselesForSaleLL, vesselesForSaleLLUser, outHistoryLL,
            contactUsLL, staffLL, blogsLL, blogsLLUser, supportLL, liveSupportLL, webLoginLL, FavoriteVesselLL;
    public TextView txtHomeTV, txtAboutTV, txtServicesTV, txtVessselForSaleTV, txtOurOfficsTV, txtStaffTV,
            txtContactUsTV, txtFavoriteVesselTV, notificationTV, notificationTVUser, txtblogsTV, txtblogsTVUser,
            txtSupportTV, webLoginTV, txtVessselForSaleTVUser, txtliveSupportTV;
    public ImageView homeIv, aboutIv, servicesIv, vesselforsaleIv, officeIv, staffIv, contactIv, favoritevesselIv,
            imgBell, imgBellUser, blogIv, blogIvUser, supportIv, webIv, vesselforsaleIvUser, liveSupportIv;
    public ImageView imgfbIV, imgtwiIV, imginstIV;
    public String strLoginFragment = "";
    private LinearLayout signLL, blogLL;
    private String refreshedToken = "", mModuleType = "";
    private String strUserId = "";
    private List<ModulesItem> mModulesItemList = new ArrayList<>();
    private SideMenuModulesAdapter mSideMenuModulesAdapter;
    private String strUnReadForum = "", strUnReadUK = "", unread_chat = "", unread_notifications = "";

    /********* Replace Fragment In Activity **********/
    public static void switchFragment(FragmentActivity activity, Fragment fragment, String TAG, boolean addToStack, Bundle bundle) {
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.layoutContainerLL, fragment, TAG);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }

    public static void homeToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        bottomMainLL.setVisibility(View.VISIBLE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu_white_icon);
        txtCenter.setText("Home");
        imgRightLL.setVisibility(View.GONE);
        imgRight.setVisibility(View.GONE);
        jaoharlogoImg.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
    }

    @SuppressLint("ResourceAsColor")
    public static void aboutToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        txtCenter.setText("About Us");
        toolBarMainLL.setBackgroundColor(R.color.white);
        imgRightLL.setVisibility(View.INVISIBLE);
        imgRight.setVisibility(View.GONE);
        jaoharlogoImg.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
    }

    @SuppressLint("ResourceAsColor")
    public static void servicesToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        toolBarMainLL.setBackgroundColor(R.color.white);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        txtCenter.setText("Services");
        imgRightLL.setVisibility(View.INVISIBLE);
        imgRight.setVisibility(View.GONE);
        jaoharlogoImg.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
    }

    public void vesselsForSaleToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        txtCenter.setText("Vessels For Sale");
        imgRightLL.setVisibility(View.VISIBLE);
        imgRight.setVisibility(View.VISIBLE);
        imgRight.setImageResource(R.drawable.ic_magnifying_glass);
        jaoharlogoImg.setVisibility(View.VISIBLE);
        imgRight.setPadding(5, 5, 5, 5);
        spinnerLL.setVisibility(View.GONE);
    }

    public static void FavvesselsToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        txtCenter.setText("Favorite Vessels");
        imgRightLL.setVisibility(View.INVISIBLE);
        imgRight.setVisibility(View.VISIBLE);
        jaoharlogoImg.setVisibility(View.VISIBLE);
        spinnerLL.setVisibility(View.GONE);
        imgRight.setPadding(5, 5, 5, 5);
    }

    @SuppressLint("ResourceAsColor")
    public static void ourOfficesToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        txtCenter.setText("Our Offices");
        toolBarMainLL.setBackgroundColor(R.color.white);
        jaoharlogoImg.setVisibility(View.GONE);
        txtCenter.setTextColor(R.color.black);
        imgRightLL.setVisibility(View.INVISIBLE);
        imgRight.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
    }

    @SuppressLint("ResourceAsColor")
    public static void ourHistoryToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        txtCenter.setText("Our History");
        toolBarMainLL.setBackgroundColor(R.color.white);
        jaoharlogoImg.setVisibility(View.GONE);
        txtCenter.setTextColor(R.color.black);
        imgRightLL.setVisibility(View.GONE);
        imgRight.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
    }

    @SuppressLint("ResourceAsColor")
    public static void contactUsToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        txtCenter.setText("Contact Us");
        toolBarMainLL.setBackgroundColor(R.color.white);
        jaoharlogoImg.setVisibility(View.GONE);
        txtCenter.setTextColor(R.color.black);
        imgRightLL.setVisibility(View.INVISIBLE);
        imgRight.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
    }

    @SuppressLint("ResourceAsColor")
    public static void notificationToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.VISIBLE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        txtCenter.setText("Notifications");
        toolBarMainLL.setBackgroundColor(R.color.white);
        txtCenter.setTextColor(R.color.black);
        imgRightLL.setVisibility(View.GONE);
        imgRight.setVisibility(View.GONE);
        jaoharlogoImg.setVisibility(View.GONE);
        imgRight.setPadding(5, 5, 5, 5);
        spinnerLL.setVisibility(View.GONE);
    }

    @SuppressLint("ResourceAsColor")
    public static void profileToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        toolBarMainLL.setBackgroundColor(R.color.white);
        txtCenter.setTextColor(R.color.black);
        imgRightLL.setVisibility(View.VISIBLE);
        imgRight.setVisibility(View.VISIBLE);
        jaoharlogoImg.setVisibility(View.GONE);
        imgRight.setPadding(4, 4, 4, 4);
        spinnerLL.setVisibility(View.VISIBLE);
    }

    public static void loginToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        imgRightLL.setVisibility(View.INVISIBLE);
        imgRight.setVisibility(View.GONE);
        jaoharlogoImg.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
    }

    @SuppressLint("ResourceAsColor")
    public static void resetToolbar() {
        imgAdd.setVisibility(View.GONE);
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        imgRightLL.setVisibility(View.GONE);
        imgRight.setVisibility(View.GONE);
        txtCenter.setTextColor(R.color.black);
        jaoharlogoImg.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
        imgDraft.setVisibility(View.GONE);
    }

    @SuppressLint("ResourceAsColor")
    public static void ToolbarWithSearch() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        txtCenter.setText("");
        imgRightLL.setVisibility(View.VISIBLE);
        imgRight.setVisibility(View.VISIBLE);
        imgRight.setImageResource(R.drawable.ic_magnifying_glass);
        imgRight.setPadding(5, 5, 5, 5);
        spinnerLL.setVisibility(View.GONE);
    }

    public static void staffToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        txtCenter.setText("Staff");
        imgRight.setImageResource(R.drawable.ic_options);
        imgRightLL.setVisibility(View.VISIBLE);
        imgRight.setVisibility(View.VISIBLE);
        jaoharlogoImg.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
    }

    public static void blogToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        txtCenter.setText("Blogs");
        imgRightLL.setVisibility(View.GONE);
        imgRight.setVisibility(View.GONE);
        jaoharlogoImg.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
    }

    public static void socialMediaToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        imgRightLL.setVisibility(View.INVISIBLE);
        imgRight.setVisibility(View.GONE);
        jaoharlogoImg.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
    }

    public static void WebLoginToolbar() {
        resetToolbar();
        notificationCountRL.setVisibility(View.GONE);
        llLeftLL.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.menu);
        imgRightLL.setVisibility(View.INVISIBLE);
        imgRight.setVisibility(View.GONE);
        jaoharlogoImg.setVisibility(View.GONE);
        imgRight.setPadding(0, 0, 0, 0);
        spinnerLL.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        getPushToken();
        Log.e(TAG, "********TOKEN*******" + refreshedToken);
        sendingMassPushToken();

        //Set Toolbar
        setToolbar();
        //Set Navigation Drawer
        setNavigationDrawer();

        setViewsDATA();

        setClickListner();

        getIntentData();
    }

    private void getIntentData() {
        if (getIntent() != null
                && getIntent().getStringExtra(JaoharConstants.LOGIN_TYPE) != null
                && getIntent().getStringExtra(JaoharConstants.LOGIN_TYPE).equals(JaoharConstants.STAFF)) {
            if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                setSideMenu("home");
                executeStaffClick();
            }
        }
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    refreshedToken = task.getResult();
                    Log.e(TAG, "**Push Token**" + refreshedToken);
                });
    }

    private void setToolbar() {
        notificationCountBgRl = findViewById(R.id.notificationCountBgRl);
        notificationCountTV = findViewById(R.id.notificationCountTV);
        notificationCountRL = findViewById(R.id.notificationCountRL);
        imgRightLL = findViewById(R.id.imgRightLL);
        llLeftLL = findViewById(R.id.llLeftLL);
        imgRight = findViewById(R.id.imgRight);
        imgBack = findViewById(R.id.imgBack);
        txtCenter = findViewById(R.id.txtCenter);
        textLL = findViewById(R.id.textLL);
        jaoharlogoImg = findViewById(R.id.jaoharlogoImg);
        spinnerLL = findViewById(R.id.spinnerLL);
        selectSpinner = findViewById(R.id.selectSpinner);
    }

    private void setNavigationDrawer() {
        mSimpleSideDrawer = new SimpleSideDrawer(mActivity);
        mSimpleSideDrawer.setLeftBehindContentView(R.layout.activity_behind_left_simple);

        modulesRV = findViewById(R.id.modulesRV);
        modulesLL = findViewById(R.id.modulesLL);
        modulesProgressBar = findViewById(R.id.modulesProgressBar);

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                    modulesRV.setVisibility(View.VISIBLE);
                    modulesLL.setVisibility(View.GONE);
                    executeSideMenuModulesAPI();
                } else {
                    modulesRV.setVisibility(View.GONE);
                    modulesLL.setVisibility(View.VISIBLE);
                    liveSupportLL.setVisibility(View.GONE);
                }

                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    FavoriteVesselLL.setVisibility(View.VISIBLE);

                    vesselesForSaleLLUser.setVisibility(View.VISIBLE);
                    vesselesForSaleLL.setVisibility(View.GONE);

                    notificationRLUser.setVisibility(View.VISIBLE);
                    notificationRL.setVisibility(View.GONE);

                    blogsLLUser.setVisibility(View.VISIBLE);
                    blogsLL.setVisibility(View.GONE);

                    liveSupportLL.setVisibility(View.VISIBLE);

                    staffLL.setVisibility(View.GONE);
                } else {

                    vesselesForSaleLLUser.setVisibility(View.GONE);
                    vesselesForSaleLL.setVisibility(View.VISIBLE);

                    notificationRLUser.setVisibility(View.GONE);
                    notificationRL.setVisibility(View.VISIBLE);

                    blogsLLUser.setVisibility(View.GONE);
                    blogsLL.setVisibility(View.VISIBLE);

                    liveSupportLL.setVisibility(View.GONE);

                    staffLL.setVisibility(View.VISIBLE);
                }

                if (JaoharConstants.IS_BACK == true) {
                    Utilities.hideKeyBoad(mActivity, llLeftLL);
                    JaoharConstants.IS_BACK = false;
                    FragmentManager fm = getSupportFragmentManager();
                    fm.popBackStack();
                    mainRL.setBackground(getResources().getDrawable(R.drawable.home_bg));
                    bottomMainLL.setVisibility(View.VISIBLE);
                    homeToolbar();
                } else {
                    Utilities.hideKeyBoad(mActivity, llLeftLL);
                    mSimpleSideDrawer.openLeftSide();
                }
            }
        });
    }

    public void sendingMassPushToken() {
        if (!JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "").equals("")) {
            strUserId = JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "");
        } else {
            strUserId = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.sendMassPushRequest(strUserId, refreshedToken, "Android");
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                Log.e(TAG, "*****Response****" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    String strStatus = String.valueOf(mModel.getStatus());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    protected void setViewsDATA() {
        profilePICIMG = findViewById(R.id.profilePICIMG);
        imgAdd = findViewById(R.id.imgAdd);
        imgManageUtilitiesIV = findViewById(R.id.imgManageUtilitiesIV);
        imgDraft = findViewById(R.id.imgDraft);

        /* navigation text ids */
        txtHomeTV = findViewById(R.id.txtHomeTV);
        txtAboutTV = findViewById(R.id.txtAboutTV);
        txtServicesTV = findViewById(R.id.txtServicesTV);
        txtVessselForSaleTV = findViewById(R.id.txtVessselForSaleTV);
        txtVessselForSaleTVUser = findViewById(R.id.txtVessselForSaleTVUser);
        txtOurOfficsTV = findViewById(R.id.txtOurOfficsTV);
        txtStaffTV = findViewById(R.id.txtStaffTV);
        txtContactUsTV = findViewById(R.id.txtContactUsTV);
        txtFavoriteVesselTV = findViewById(R.id.txtFavoriteVesselTV);
        notificationTV = findViewById(R.id.notificationTV);
        notificationTVUser = findViewById(R.id.notificationTVUser);
        txtblogsTV = findViewById(R.id.txtblogsTV);
        txtblogsTVUser = findViewById(R.id.txtblogsTVUser);
        txtSupportTV = findViewById(R.id.txtSupportTV);
        webLoginTV = findViewById(R.id.webLoginTV);
        txtliveSupportTV = findViewById(R.id.txtliveSupportTV);

        /* navigation image ids */
        homeIv = findViewById(R.id.homeIv);
        aboutIv = findViewById(R.id.aboutIv);
        servicesIv = findViewById(R.id.servicesIv);
        vesselforsaleIv = findViewById(R.id.vesselforsaleIv);
        vesselforsaleIvUser = findViewById(R.id.vesselforsaleIvUser);
        officeIv = findViewById(R.id.officeIv);
        staffIv = findViewById(R.id.staffIv);
        contactIv = findViewById(R.id.contactIv);
        favoritevesselIv = findViewById(R.id.favoritevesselIv);
        imgBell = findViewById(R.id.imgBell);
        imgBellUser = findViewById(R.id.imgBellUser);
        blogIv = findViewById(R.id.blogIv);
        blogIvUser = findViewById(R.id.blogIvUser);
        supportIv = findViewById(R.id.supportIv);
        webIv = findViewById(R.id.webIv);
        liveSupportIv = findViewById(R.id.liveSupportIv);

        /* navigation layout ids */
        FavoriteVesselLL = findViewById(R.id.FavoriteVesselLL);
        homeLL = findViewById(R.id.homeLL);
        webLoginLL = findViewById(R.id.webLoginLL);
        aboutLL = findViewById(R.id.aboutLL);
        servicesLL = findViewById(R.id.servicesLL);
        ourOfficesLL = findViewById(R.id.ourOfficesLL);
        vesselesForSaleLL = findViewById(R.id.vesselesForSaleLL);
        vesselesForSaleLLUser = findViewById(R.id.vesselesForSaleLLUser);
        outHistoryLL = findViewById(R.id.outHistoryLL);
        contactUsLL = findViewById(R.id.contactUsLL);
        staffLL = findViewById(R.id.staffLL);
        blogsLL = findViewById(R.id.blogsLL);
        blogsLLUser = findViewById(R.id.blogsLLUser);
        supportLL = findViewById(R.id.supportLL);
        liveSupportLL = findViewById(R.id.liveSupportLL);

        imgfbIV = findViewById(R.id.imgfbIV);
        imgtwiIV = findViewById(R.id.imgtwiIV);
        imginstIV = findViewById(R.id.imginstIV);
        toolBarMainLL = findViewById(R.id.toolBarMainLL);
        mainRL = findViewById(R.id.mainRL);
        bottomMainLL = findViewById(R.id.bottomMainLL);
        notificationRL = findViewById(R.id.notificationRL);
        notificationRLUser = findViewById(R.id.notificationRLUser);
        signLL = findViewById(R.id.signLL);
        blogLL = findViewById(R.id.blogLL);
        signTV = findViewById(R.id.signTV);
        nameTV = findViewById(R.id.nameTV);
        typetv = findViewById(R.id.typetv);
        roletv = findViewById(R.id.roletv);
        badgesTV = findViewById(R.id.badgesTV);
        badgesTVUser = findViewById(R.id.badgesTVUser);
        profileLL = findViewById(R.id.profileLL);
        profileClickLL = findViewById(R.id.profileClickLL);

        /* Check to Show And Hide View */
        if (JaoharPreference.readString(mActivity, JaoharPreference.first_name, "").equals("")) {
            profileLL.setVisibility(View.GONE);
        } else {
            profileLL.setVisibility(View.VISIBLE);
            nameTV.setText(JaoharPreference.readString(mActivity, JaoharPreference.full_name, ""));
            if (JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ROLE, "").equals("staff")) {
                typetv.setText("Romania");
                roletv.setVisibility(View.VISIBLE);
                roletv.setText("Staff");
                typetv.setVisibility(View.VISIBLE);
            } else if (JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ROLE, "").equals("office")) {
                typetv.setText("UK Office");
                roletv.setVisibility(View.VISIBLE);
                roletv.setText("Staff");
                typetv.setVisibility(View.VISIBLE);
            } else if (JaoharPreference.readString(mActivity, JaoharPreference.Role, "").equals(JaoharPreference.readString(mActivity, JaoharPreference.USER_ROLE, ""))) {
                roletv.setVisibility(View.VISIBLE);
                roletv.setText("User");
                typetv.setVisibility(View.GONE);
            }

            if (!JaoharPreference.readString(mActivity, JaoharPreference.image, "").equals("")) {
                Glide.with(mActivity).load(JaoharPreference.readString(mActivity, JaoharPreference.image, "")).into(profilePICIMG);
            } else {
                profilePICIMG.setImageResource(R.drawable.profile);
            }
        }

        if (JaoharPreference.readString(mActivity, JaoharPreference.first_name, "").equals("")) {
            signTV.setText(getString(R.string.sign_in));
        } else {
            signTV.setText("Hi, " + JaoharPreference.readString(mActivity, JaoharPreference.first_name, ""));
        }

        /* Set Default Fragment */
        if (getIntent() != null) {
            if (getIntent().getStringExtra(JaoharConstants.LOGIN).equals(JaoharConstants.LOGIN)) {
                strLoginFragment = getIntent().getStringExtra(JaoharConstants.LOGIN);
                loginToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new LoginFragment(), JaoharConstants.LOGIN_FRAGMENT, false, null);
            } else if (getIntent().getStringExtra(JaoharConstants.LOGIN).equals("Home")) {
                homeToolbar();
                bottomMainLL.setVisibility(View.VISIBLE);
                mainRL.setBackground(getResources().getDrawable(R.drawable.home_bg));
                txtCenter.setVisibility(View.GONE);
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.trasperant));
                switchFragment((FragmentActivity) mActivity, new HomeFragment(), JaoharConstants.HOME_FRAGMENT, false, null);
            } else if (getIntent().getStringExtra(JaoharConstants.LOGIN).equals("mass_push")) {
                setNotificationTab();
            } else if (getIntent().getStringExtra(JaoharConstants.LOGIN).equals("StaffHome")) {
                staffToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new StaffFragment(), JaoharConstants.STAFF_FRAGMENT, false, null);
            } else if (getIntent().getStringExtra(JaoharConstants.LOGIN).equals("Staff")) {
                staffToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new StaffFragment(), JaoharConstants.STAFF_FRAGMENT, false, null);
            } else if (getIntent().getStringExtra(JaoharConstants.LOGIN).equals("VesselForSale")) {
                vesselsForSaleToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new VesselsForSaleFragment(), JaoharConstants.VESSELES_FOR_SALE_FRAGMENT, false, null);
            } else if (getIntent().getStringExtra(JaoharConstants.LOGIN).equals("VesselForSale") && getIntent().getStringExtra(JaoharConstants.VESSELS_FOR_SALE_ACTIVITY).equals("VesselForSaleActivity")) {
                startActivity(new Intent(mActivity, VesselForSaleActivity.class));
                overridePendingTransitionEnter();
            } else if (getIntent().getStringExtra(JaoharConstants.LOGIN).equals("SignUP") && getIntent().getStringExtra("TYPE").equals(JaoharConstants.STAFF)) {
                loginToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                Bundle mBundle = new Bundle();
                mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.STAFF);
                switchFragment((FragmentActivity) mActivity, new LoginFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
            } else if (getIntent().getStringExtra(JaoharConstants.LOGIN).equals("BlogHOME")) {
                JaoharConstants.is_Staff_FragmentClick = false;
                blogToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new BlogFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);
            } else if (getIntent().getStringExtra(JaoharConstants.LOGIN).equals("SignUP") && getIntent().getStringExtra("TYPE").equals(JaoharConstants.USER)) {
                VesselesModel mVesselesModel = (VesselesModel) getIntent().getSerializableExtra("Model");
                loginToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                Bundle mBundle = new Bundle();
                mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.USER);
                mBundle.putSerializable("Model", mVesselesModel);
                switchFragment((FragmentActivity) mActivity, new LoginFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
            } else if (JaoharConstants.is_Staff_FragmentClick == true) {
                JaoharConstants.is_Staff_FragmentClick = false;
                staffToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new StaffFragment(), JaoharConstants.STAFF_FRAGMENT, false, null);
            } else if (JaoharConstants.is_VessalsForSale == true) {
                JaoharConstants.is_VessalsForSale = false;
                vesselsForSaleToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new VesselsForSaleFragment(), JaoharConstants.VESSELES_FOR_SALE_FRAGMENT, false, null);
            } else {
                homeToolbar();
                bottomMainLL.setVisibility(View.VISIBLE);
                mainRL.setBackground(getResources().getDrawable(R.drawable.home_bg));
                txtCenter.setVisibility(View.GONE);
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.trasperant));
                switchFragment((FragmentActivity) mActivity, new HomeFragment(), JaoharConstants.HOME_FRAGMENT, false, null);
            }
        }
    }

    protected void setClickListner() {
        homeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                    setSideMenu("home");
                    executeStaffClick();
                } else {
                    executeHomeClick();
                }
            }
        });

        FavoriteVesselLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* change text and icon color */
                setSideMenu("fav vessels");

                mSimpleSideDrawer.closeLeftSide();
                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    JaoharConstants.is_Staff_FragmentClick = false;
                    txtCenter.setText(getString(R.string.favorite_vessels));
                    FavvesselsToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    switchFragment((FragmentActivity) mActivity, new FavoriteVesselFragment(), JaoharConstants.FavoriteVessels, false, null);
                } else {
                    imgRightLL.setVisibility(View.GONE);
                    imgRight.setVisibility(View.GONE);
                    loginToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    Bundle mBundle = new Bundle();
                    mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.USER);
                    switchFragment((FragmentActivity) mActivity, new LoginUserFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
                }
            }
        });

        aboutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("about");

                mSimpleSideDrawer.closeLeftSide();
                txtCenter.setText(getString(R.string.about_us));
                aboutToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new AboutFragment(), JaoharConstants.ABOUT_FRAGMENT, false, null);
            }
        });

        servicesLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("services");

                mSimpleSideDrawer.closeLeftSide();
                txtCenter.setText(getString(R.string.services));
                servicesToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new ServicesFragment(), JaoharConstants.SERVICES_FRAGMENT, false, null);
            }
        });

        ourOfficesLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("our offices");

                mSimpleSideDrawer.closeLeftSide();
                txtCenter.setText(getString(R.string.our_offfices));
                ourOfficesToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new OurOfficesFragment(), JaoharConstants.OUR_OFFICES_FRAGMENT, false, null);
            }
        });

        outHistoryLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSimpleSideDrawer.closeLeftSide();
                txtCenter.setText(getString(R.string.our_history));
                ourHistoryToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new OurHistoryFragment(), JaoharConstants.OUR_HISTORY_FRAGMENT, false, null);
            }
        });

        contactUsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("contact us");

                mSimpleSideDrawer.closeLeftSide();
                txtCenter.setText(getString(R.string.contact_us));
                contactUsToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new ContactUsFragment(), JaoharConstants.CONTACTUS_FRAGMENT, false, null);
            }
        });

        staffLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSideMenu("staff");
                executeStaffClick();
            }
        });

        blogLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("blogs");

                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    JaoharConstants.is_Staff_FragmentClick = false;
                    blogToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    switchFragment((FragmentActivity) mActivity, new BlogFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);
                } else {
                    imgRightLL.setVisibility(View.GONE);
                    imgRight.setVisibility(View.GONE);
                    loginToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    Bundle mBundle = new Bundle();
                    mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.STAFF);
                    switchFragment((FragmentActivity) mActivity, new LoginFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
                }
            }
        });

        blogsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("blogs");

                mSimpleSideDrawer.closeLeftSide();

                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    JaoharConstants.is_Staff_FragmentClick = false;
                    blogToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    switchFragment((FragmentActivity) mActivity, new BlogFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);
                } else {
                    imgRightLL.setVisibility(View.GONE);
                    imgRight.setVisibility(View.GONE);
                    loginToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    Bundle mBundle = new Bundle();
                    mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.STAFF);
                    switchFragment((FragmentActivity) mActivity, new LoginFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
                }
            }
        });

        blogsLLUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("blogs");

                mSimpleSideDrawer.closeLeftSide();

                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    JaoharConstants.is_Staff_FragmentClick = false;
                    blogToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    switchFragment((FragmentActivity) mActivity, new BlogFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);
                } else {
                    imgRightLL.setVisibility(View.GONE);
                    imgRight.setVisibility(View.GONE);
                    loginToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    Bundle mBundle = new Bundle();
                    mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.STAFF);
                    switchFragment((FragmentActivity) mActivity, new LoginFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
                }
            }
        });

        vesselesForSaleLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("vessels for sale");

                mSimpleSideDrawer.closeLeftSide();
                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    txtCenter.setText(getString(R.string.vessel_for_sale11));
                    vesselsForSaleToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    JaoharConstants.IsAllVesselsDetailBack = false;
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    switchFragment((FragmentActivity) mActivity, new VesselsForSaleFragment(), JaoharConstants.VESSELES_FOR_SALE_FRAGMENT, false, null);
                } else {
                    imgRightLL.setVisibility(View.GONE);
                    imgRight.setVisibility(View.GONE);
                    loginToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    Bundle mBundle = new Bundle();
                    mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.USER);
                    switchFragment((FragmentActivity) mActivity, new LoginUserFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
                }
            }
        });

        vesselesForSaleLLUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("vessels for sale");

                mSimpleSideDrawer.closeLeftSide();
                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    txtCenter.setText(getString(R.string.vessel_for_sale11));
                    vesselsForSaleToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    JaoharConstants.IsAllVesselsDetailBack = false;
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    switchFragment((FragmentActivity) mActivity, new VesselsForSaleFragment(), JaoharConstants.VESSELES_FOR_SALE_FRAGMENT, false, null);
                } else {
                    imgRightLL.setVisibility(View.GONE);
                    imgRight.setVisibility(View.GONE);
                    loginToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    Bundle mBundle = new Bundle();
                    mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.USER);
                    switchFragment((FragmentActivity) mActivity, new LoginUserFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
                }
            }
        });

        supportLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* change text and icon color */
                setSideMenu("support");

                resetToolbar();
                mSimpleSideDrawer.closeLeftSide();
                txtCenter.setText(getString(R.string.support_page));
                txtCenter.setVisibility(View.VISIBLE);
                imgRightLL.setVisibility(View.INVISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new SupportFragment(), JaoharConstants.SUPPORT_FRAGMENT, false, null);
            }
        });

        liveSupportLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* change text and icon color */
                setSideMenu("live support");

                resetToolbar();

                showToast(mActivity, "Coming soon");
            }
        });

        imgfbIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSimpleSideDrawer.closeLeftSide();
                Bundle mBundle = new Bundle();
                mBundle.putString("LINK", JaoharConstants.FACEBOOK_PAGE_LINK);
                socialMediaToolbar();
                txtCenter.setText(getString(R.string.facebook));
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new OpenLinkFragment(), JaoharConstants.OPENLINK_FRAGMENT, false, mBundle);
            }
        });

        imgtwiIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSimpleSideDrawer.closeLeftSide();
                Bundle mBundle = new Bundle();
                mBundle.putString("LINK", JaoharConstants.TWITTER_PAGE_LINK);
                socialMediaToolbar();
                txtCenter.setText(getString(R.string.twitter));
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new OpenLinkFragment(), JaoharConstants.OPENLINK_FRAGMENT, false, mBundle);
            }
        });

        imginstIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSimpleSideDrawer.closeLeftSide();
                Bundle mBundle = new Bundle();
                mBundle.putString("LINK", JaoharConstants.INSTAGRAM_PAGE_LINK);
                socialMediaToolbar();
                txtCenter.setText(getString(R.string.instagram));
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new OpenLinkFragment(), JaoharConstants.OPENLINK_FRAGMENT, false, mBundle);
            }
        });

        webLoginLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("web login");

                WebLoginToolbar();
                mSimpleSideDrawer.closeLeftSide();
                txtCenter.setText(getString(R.string.weblogin));
                txtCenter.setTextColor(getResources().getColor(R.color.black));
                txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.white));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment((FragmentActivity) mActivity, new WebLoginFragment(), JaoharConstants.SUPPORT_FRAGMENT, false, null);
            }
        });

        signLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (JaoharPreference.readString(mActivity, JaoharPreference.first_name, "").equals("")) {
                    signTV.setText(getString(R.string.sign_in));
                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, JaoharConstants.USER);
                    mActivity.startActivity(mIntent);
                } else {
                    signTV.setText("Hi, " + JaoharPreference.readString(mActivity, JaoharPreference.first_name, ""));
                }
            }
        });

        profileClickLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSimpleSideDrawer.closeLeftSide();
                String strURL = JaoharPreference.readString(mActivity, JaoharPreference.Role, "");
                Log.e(TAG, "Data== " + strURL);

                setSideMenu("");

                if (JaoharPreference.readString(mActivity, JaoharPreference.Role, "").equals(JaoharPreference.readString(mActivity, JaoharPreference.USER_ROLE, ""))) {
                    JaoharConstants.BOOLEAN_USER_LOGOUT = true;

                    if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                        JaoharConstants.BOOLEAN_USER_LOGOUT = true;
                        txtCenter.setText(getString(R.string.profile));
                        profileToolbar();
                        txtCenter.setVisibility(View.VISIBLE);
                        mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                        switchFragment((FragmentActivity) mActivity, new UserProfileFragment(), JaoharConstants.NotificationFragment, false, null);
                    } else {
                        JaoharConstants.BOOLEAN_STAFF_LOGOUT = true;
                        imgRightLL.setVisibility(View.GONE);
                        imgRight.setVisibility(View.GONE);
                        profileToolbar();
                        spinnerLL.setVisibility(View.GONE);
                        txtCenter.setVisibility(View.VISIBLE);
                        mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                        Bundle mBundle = new Bundle();
                        mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.STAFF);
                        switchFragment((FragmentActivity) mActivity, new UserProfileFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
                    }

                } else {
                    JaoharConstants.BOOLEAN_STAFF_LOGOUT = true;

                    if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                        JaoharConstants.BOOLEAN_USER_LOGOUT = true;
                        txtCenter.setText(getString(R.string.profile));
                        profileToolbar();
                        txtCenter.setVisibility(View.VISIBLE);
                        mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                        switchFragment((FragmentActivity) mActivity, new UserProfileFragment(), JaoharConstants.NotificationFragment, false, null);
                    } else {
                        JaoharConstants.BOOLEAN_STAFF_LOGOUT = true;
                        imgRightLL.setVisibility(View.GONE);
                        imgRight.setVisibility(View.GONE);
                        profileToolbar();
                        txtCenter.setVisibility(View.VISIBLE);
                        spinnerLL.setVisibility(View.GONE);
                        mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                        switchFragment((FragmentActivity) mActivity, new UserProfileFragment(), JaoharConstants.NotificationFragment, false, null);
                    }
                }
            }
        });

        notificationRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("notifications");

                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    JaoharConstants.is_Staff_FragmentClick = false;
                    txtCenter.setText(getString(R.string.notification));
                    notificationToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    switchFragment((FragmentActivity) mActivity, new NotificationFragment(), JaoharConstants.NotificationFragment, false, null);
                } else {
                    imgRightLL.setVisibility(View.GONE);
                    imgRight.setVisibility(View.GONE);
                    loginToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    Bundle mBundle = new Bundle();
                    mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.STAFF);
                    switchFragment((FragmentActivity) mActivity, new LoginFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
                }
                mSimpleSideDrawer.closeLeftSide();
            }
        });

        notificationRLUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* change text and icon color */
                setSideMenu("notifications");

                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    JaoharConstants.is_Staff_FragmentClick = false;
                    txtCenter.setText(getString(R.string.notification));
                    notificationToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    switchFragment((FragmentActivity) mActivity, new NotificationFragment(), JaoharConstants.NotificationFragment, false, null);
                } else {
                    imgRightLL.setVisibility(View.GONE);
                    imgRight.setVisibility(View.GONE);
                    loginToolbar();
                    txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    Bundle mBundle = new Bundle();
                    mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.STAFF);
                    switchFragment((FragmentActivity) mActivity, new LoginFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
                }
                mSimpleSideDrawer.closeLeftSide();
            }
        });

        imgManageUtilitiesIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, ManageUtilitiesActivity.class);
                mActivity.startActivity(mIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        UpdateOnlineStatus();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult Result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (Result != null) {
            if (Result.getContents() == null) {
                Log.d("MainActivity", "cancelled scan");
                Toast.makeText(this, "cancelled", Toast.LENGTH_SHORT).show();
            } else {
                Log.d("MainActivity", "Scanned");
                webLoginApi(Result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.layoutContainerLL);
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    /*
     *Execute WebLogin API using  WebLogin
     *@param
     *@user_id,code
     * */
    private void webLoginApi(String contents) {
        String strUserId = "";
        if (!JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "").equals("")) {
            strUserId = JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "");
        } else {
            strUserId = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.webLoginRequest(contents, strUserId);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                Log.e(TAG, "*****ResponseWebLogin****" + response);
                mSimpleSideDrawer.closeLeftSide();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
            txtCenter.setVisibility(View.GONE);
            toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.trasperant));
            homeToolbar();
            bottomMainLL.setVisibility(View.VISIBLE);
            mainRL.setBackground(getResources().getDrawable(R.drawable.home_bg));
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            finishAffinity();
        }
    }

    private void UpdateOnlineStatus() {
        String strUserId = "";
        if (!JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "").equals("")) {
            strUserId = JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "");
        } else {
            strUserId = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.updateOnLineStatusRequest(strUserId, "1");
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                Log.e(TAG, "*****Response****" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    String strStatus = String.valueOf(mModel.getStatus());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void setNotificationTab() {
        /* change text and icon color */
        setSideMenu("notifications");

        if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
            JaoharConstants.is_Staff_FragmentClick = false;
            txtCenter.setText(getString(R.string.notification));
            notificationToolbar();
            txtCenter.setVisibility(View.VISIBLE);
            mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
            switchFragment((FragmentActivity) mActivity, new NotificationFragment(), JaoharConstants.NotificationFragment, false, null);
        } else {
            imgRightLL.setVisibility(View.GONE);
            imgRight.setVisibility(View.GONE);
            loginToolbar();
            txtCenter.setVisibility(View.VISIBLE);
            mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
            Bundle mBundle = new Bundle();
            mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.STAFF);
            switchFragment((FragmentActivity) mActivity, new LoginFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
        }
        mSimpleSideDrawer.closeLeftSide();
    }

    /* execute API to get side menu data */
    private void executeSideMenuModulesAPI() {
        modulesProgressBar.setVisibility(View.VISIBLE);
        String user_id = "";
        if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
            if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                user_id = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
            } else {
                user_id = JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "");
            }
        } else {
            user_id = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        }

        ApiInterface api = ApiClient.getApiClient().create(ApiInterface.class);
        api.getUserProfileRequest(user_id).enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                modulesProgressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    Log.e(TAG, "****Response****" + response.body().toString());

                    ProfileResponse mModel = response.body();
                    if (mModel.getStatus().equals("1")) {

                        if (JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ROLE, "").equals("staff")) {
                            typetv.setText("Romania");
                            roletv.setVisibility(View.VISIBLE);
                            roletv.setText("Staff");
                            typetv.setVisibility(View.VISIBLE);
                        } else if (JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ROLE, "").equals("office")) {
                            typetv.setText("UK Office");
                            roletv.setVisibility(View.VISIBLE);
                            roletv.setText("Staff");
                            typetv.setVisibility(View.VISIBLE);
                        } else if (JaoharPreference.readString(mActivity, JaoharPreference.Role, "").equals(JaoharPreference.readString(mActivity, JaoharPreference.USER_ROLE, ""))) {
                            roletv.setVisibility(View.VISIBLE);
                            roletv.setText("User");
                            typetv.setVisibility(View.GONE);
                        }

                        mModulesItemList = mModel.getData().getModules();

                        strUnReadForum = String.valueOf(mModel.getData().getUnreadForumMessageCount());
                        strUnReadUK = String.valueOf(mModel.getData().getUnreadUkForumMessageCount());
                        unread_notifications = String.valueOf(mModel.getData().getUnreadNotify());
                        unread_chat = String.valueOf(mModel.getData().getUnreadChat());

                        setSideMenuModulesAdapter();
                    } else {
                        showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                modulesProgressBar.setVisibility(View.GONE);
                Log.e(TAG, "****Error****" + t.toString());
            }
        });
    }

    private void setSideMenuModulesAdapter() {
        modulesRV.setNestedScrollingEnabled(false);
        modulesRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mSideMenuModulesAdapter = new SideMenuModulesAdapter(mActivity, mModulesItemList,
                clickSideMenuModuleInterface, strUnReadForum, strUnReadUK,
                unread_chat, unread_notifications);
        modulesRV.setAdapter(mSideMenuModulesAdapter);
    }

    ClickSideMenuModuleInterface clickSideMenuModuleInterface = new ClickSideMenuModuleInterface() {
        @Override
        public void mClickSideMenuModuleInterface(int position, ModulesItem mModule) {
            mModuleType = mModule.getType();
            JaoharConstants.is_Staff_FragmentClick = true;
            mainRL.setBackgroundColor(getResources().getColor(R.color.white));
            toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
            setSideMenu("");
            if (mModuleType.equals("vessel")) {
                mSimpleSideDrawer.closeLeftSide();
                vesselsForSaleToolbar();
                imgAdd.setVisibility(View.VISIBLE);
                txtCenter.setVisibility(View.VISIBLE);
                imgManageUtilitiesIV.setVisibility(View.GONE);

                switchFragment((FragmentActivity) mActivity, new AllVesselsFragment(), JaoharConstants.VESSELES_FOR_SALE_FRAGMENT, false, null);
            } else if (mModuleType.equals("invoice")) {
                mSimpleSideDrawer.closeLeftSide();
                ToolbarWithSearch();
                imgAdd.setVisibility(View.VISIBLE);
                imgManageUtilitiesIV.setVisibility(View.GONE);
                txtCenter.setVisibility(View.VISIBLE);
                imgDraft.setVisibility(View.VISIBLE);
                switchFragment((FragmentActivity) mActivity, new AllInVoicesFragment(), JaoharConstants.VESSELES_FOR_SALE_FRAGMENT, false, null);
            } else if (mModuleType.equals("universal_invoice")) {
                mSimpleSideDrawer.closeLeftSide();
                ToolbarWithSearch();
                imgAdd.setVisibility(View.VISIBLE);
                txtCenter.setVisibility(View.VISIBLE);
                imgManageUtilitiesIV.setVisibility(View.GONE);
                switchFragment((FragmentActivity) mActivity, new AllUniversalInvoiceFragment(), JaoharConstants.VESSELES_FOR_SALE_FRAGMENT, false, null);
            } else if (mModuleType.equals("address_book")) {
                mSimpleSideDrawer.closeLeftSide();
                resetToolbar();
                imgManageUtilitiesIV.setVisibility(View.GONE);
                imgRightLL.setVisibility(View.VISIBLE);
                imgRight.setVisibility(View.VISIBLE);
                imgRight.setImageResource(R.drawable.plus_symbol);
                imgRight.setPadding(5, 5, 5, 5);

                txtCenter.setVisibility(View.VISIBLE);

                switchFragment((FragmentActivity) mActivity, new AddressBookListStaffFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);

            } else if (mModuleType.equals("mailing_list")) {
                mSimpleSideDrawer.closeLeftSide();
                resetToolbar();
                if(JaoharPreference.readString(mActivity, JaoharPreference.Role, null).equals("admin")){
                    imgRightLL.setVisibility(View.VISIBLE);
                    imgManageUtilitiesIV.setVisibility(View.GONE);
                    imgRight.setVisibility(View.VISIBLE);
                    imgRight.setImageResource(R.drawable.plus_symbol);
                    imgRight.setVisibility(View.VISIBLE);
                    imgRight.setPadding(5, 5, 5, 5);

                    txtCenter.setVisibility(View.VISIBLE);

                    imgRightLL.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent mIntent = new Intent(mActivity, AddMailingListActivity.class);
                            mIntent.putExtra("isAddClick", "true");
                            startActivity(mIntent);
                        }
                    });
                }
                else{
                    imgRightLL.setVisibility(View.VISIBLE);
                    imgRight.setVisibility(View.VISIBLE);
                    imgRight.setImageResource(R.drawable.add_icon);
                    imgRight.setVisibility(View.GONE);
                    imgManageUtilitiesIV.setVisibility(View.GONE);
                    imgRight.setPadding(5, 5, 5, 5);
                    txtCenter.setVisibility(View.VISIBLE);
                }

                switchFragment((FragmentActivity) mActivity, new MaillingStaffFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);

            } else if (mModuleType.equals("staff_forum")) {
                mSimpleSideDrawer.closeLeftSide();
                resetToolbar();
                imgManageUtilitiesIV.setVisibility(View.GONE);
                imgRightLL.setVisibility(View.VISIBLE);
                imgRight.setVisibility(View.VISIBLE);
                imgRight.setImageResource(R.drawable.plus_symbol);
                imgRight.setPadding(5, 5, 5, 5);

                txtCenter.setVisibility(View.VISIBLE);

                switchFragment((FragmentActivity) mActivity, new ForumListFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);

            } else if (mModuleType.equals("fund_transfer")) {
                Toast.makeText(mActivity, "In Progress ...", Toast.LENGTH_SHORT).show();
            } else if (mModuleType.equals("blog")) {
                mSimpleSideDrawer.closeLeftSide();
                blogToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                imgManageUtilitiesIV.setVisibility(View.GONE);
                switchFragment((FragmentActivity) mActivity, new BlogFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);
            } else if (mModuleType.equals("chat_support")) {
                Toast.makeText(mActivity, "In Progress ...", Toast.LENGTH_SHORT).show();
            } else if (mModuleType.equals("chat")) {
                mSimpleSideDrawer.closeLeftSide();
                ToolbarWithSearch();
                imgManageUtilitiesIV.setVisibility(View.GONE);
                txtCenter.setVisibility(View.VISIBLE);

                switchFragment((FragmentActivity) mActivity, new ChatUsersListFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);
            } else if (mModuleType.equals("manager")) {
                resetToolbar();
                imgManageUtilitiesIV.setVisibility(View.GONE);
                startActivity(new Intent(mActivity, ManagerActivity.class));
            } else if (mModuleType.equals("forum") || mModuleType.equals("uk_forum")) {
                mSimpleSideDrawer.closeLeftSide();
                resetToolbar();
                imgManageUtilitiesIV.setVisibility(View.GONE);
                imgRightLL.setVisibility(View.VISIBLE);
                imgRight.setVisibility(View.VISIBLE);
                imgRight.setImageResource(R.drawable.plus_symbol);
                imgRight.setPadding(5, 5, 5, 5);

                txtCenter.setVisibility(View.VISIBLE);

                switchFragment((FragmentActivity) mActivity, new UkOfficeForumListFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);
            } else if (mModuleType.equals("educational_blogs")) {
                mSimpleSideDrawer.closeLeftSide();
                ToolbarWithSearch();
                imgAdd.setVisibility(View.GONE);
                txtCenter.setVisibility(View.VISIBLE);
                imgManageUtilitiesIV.setVisibility(View.GONE);
                switchFragment((FragmentActivity) mActivity, new EducationalBlogsFragment(), JaoharConstants.VESSELES_FOR_SALE_FRAGMENT, false, null);

            } else if (mModuleType.equals("user_news")) {

                mSimpleSideDrawer.closeLeftSide();
                resetToolbar();
                imgManageUtilitiesIV.setVisibility(View.GONE);
                imgRightLL.setVisibility(View.VISIBLE);
                imgRight.setVisibility(View.VISIBLE);
                imgRight.setImageResource(R.drawable.plus_symbol);
                imgRight.setPadding(5, 5, 5, 5);

                txtCenter.setVisibility(View.VISIBLE);

                switchFragment((FragmentActivity) mActivity, new AllNewsFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);

//                startActivity(new Intent(mActivity, AllNewsActivity.class));
            } else if (mModuleType.equals("internal_news")) {

                mSimpleSideDrawer.closeLeftSide();
                resetToolbar();
                imgManageUtilitiesIV.setVisibility(View.GONE);
                imgRightLL.setVisibility(View.VISIBLE);
                imgRight.setVisibility(View.VISIBLE);
                imgRight.setImageResource(R.drawable.plus_symbol);
                imgRight.setPadding(5, 5, 5, 5);

                txtCenter.setVisibility(View.VISIBLE);

                switchFragment((FragmentActivity) mActivity, new AllInternalNewsFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);

//                startActivity(new Intent(mActivity, AllInternalNewsActivity.class));
            } else if (mModuleType.equals("notifications")) {
                mSimpleSideDrawer.closeLeftSide();
                notificationToolbar();
                txtCenter.setText(getString(R.string.notification));
                txtCenter.setVisibility(View.VISIBLE);
                imgManageUtilitiesIV.setVisibility(View.GONE);
                switchFragment((FragmentActivity) mActivity, new NotificationFragment(), JaoharConstants.NotificationFragment, false, null);
            } else if (mModuleType.equals("favourite")) {
                mSimpleSideDrawer.closeLeftSide();
                FavvesselsToolbar();
                txtCenter.setText(getString(R.string.favorite_vessels));
                txtCenter.setVisibility(View.VISIBLE);
                imgManageUtilitiesIV.setVisibility(View.GONE);
                switchFragment((FragmentActivity) mActivity, new FavoriteVesselFragment(), JaoharConstants.FavoriteVessels, false, null);
            } else {
                if (mModule.getModuleName().equals("Educational Blogs")) {
                    mSimpleSideDrawer.closeLeftSide();
                    ToolbarWithSearch();
                    imgAdd.setVisibility(View.VISIBLE);
                    txtCenter.setVisibility(View.VISIBLE);
                    switchFragment((FragmentActivity) mActivity, new EducationalBlogsFragment(), JaoharConstants.VESSELES_FOR_SALE_FRAGMENT, false, null);

//                    JaoharConstants.is_Staff_FragmentClick = true;
//                    startActivity(new Intent(mActivity, EducationalBlogsActivity.class));
                }
            }
        }
    };

    private void executeHomeClick() {
        /* change text and icon color */
        setSideMenu("home");

        if (mSideMenuModulesAdapter != null)
            mSideMenuModulesAdapter.setHomeClick();

        mSimpleSideDrawer.closeLeftSide();
        txtCenter.setText(getString(R.string.home));
        homeToolbar();
        mainRL.setBackground(getResources().getDrawable(R.drawable.home_bg));
        txtCenter.setVisibility(View.GONE);
        toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.trasperant));
        switchFragment((FragmentActivity) mActivity, new HomeFragment(), JaoharConstants.HOME_FRAGMENT, false, null);
    }

    private void executeStaffClick() {
        /* change text and icon color */
        mSimpleSideDrawer.closeLeftSide();
        if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
            txtCenter.setText(getString(R.string.staff));
            imgRightLL.setVisibility(View.VISIBLE);
            imgRight.setVisibility(View.VISIBLE);
            imgRight.setImageResource(R.drawable.ic_options);
            staffToolbar();
            txtCenter.setVisibility(View.VISIBLE);
            mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
            switchFragment((FragmentActivity) mActivity, new StaffFragment(), JaoharConstants.STAFF_FRAGMENT, false, null);
        } else {
            imgRightLL.setVisibility(View.GONE);
            imgRight.setVisibility(View.GONE);
            loginToolbar();
            txtCenter.setVisibility(View.VISIBLE);
            mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
            Bundle mBundle = new Bundle();
            mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.STAFF);
            switchFragment((FragmentActivity) mActivity, new LoginFragment(), JaoharConstants.LOGIN_FRAGMENT, false, mBundle);
        }
    }

    private void setSideMenu(String type) {
        txtStaffTV.setTextColor(getResources().getColor(R.color.black));
        staffIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        txtAboutTV.setTextColor(getResources().getColor(R.color.black));
        aboutIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        txtHomeTV.setTextColor(getResources().getColor(R.color.black));
        homeIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        txtServicesTV.setTextColor(getResources().getColor(R.color.black));
        servicesIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        txtOurOfficsTV.setTextColor(getResources().getColor(R.color.black));
        officeIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        txtContactUsTV.setTextColor(getResources().getColor(R.color.black));
        contactIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        txtblogsTV.setTextColor(getResources().getColor(R.color.black));
        txtblogsTVUser.setTextColor(getResources().getColor(R.color.black));
        blogIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        blogIvUser.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        txtVessselForSaleTV.setTextColor(getResources().getColor(R.color.black));
        txtVessselForSaleTVUser.setTextColor(getResources().getColor(R.color.black));
        vesselforsaleIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        vesselforsaleIvUser.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        txtSupportTV.setTextColor(getResources().getColor(R.color.black));
        supportIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        webLoginTV.setTextColor(getResources().getColor(R.color.black));
        webIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        notificationTV.setTextColor(getResources().getColor(R.color.black));
        notificationTVUser.setTextColor(getResources().getColor(R.color.black));
        imgBell.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        imgBellUser.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        txtFavoriteVesselTV.setTextColor(getResources().getColor(R.color.black));
        favoritevesselIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        txtliveSupportTV.setTextColor(getResources().getColor(R.color.black));
        liveSupportIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));

        if (type.equals("home")) {
            JaoharPreference.writeString(mActivity, JaoharConstants.SIDE_MENU_CLICKED_ID, "");
            txtHomeTV.setTextColor(getResources().getColor(R.color.blue));
            homeIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("about")) {
            txtAboutTV.setTextColor(getResources().getColor(R.color.blue));
            aboutIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("services")) {
            txtServicesTV.setTextColor(getResources().getColor(R.color.blue));
            servicesIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("our offices")) {
            txtOurOfficsTV.setTextColor(getResources().getColor(R.color.blue));
            officeIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("vessels for sale")) {
            txtVessselForSaleTV.setTextColor(getResources().getColor(R.color.blue));
            txtVessselForSaleTVUser.setTextColor(getResources().getColor(R.color.blue));
            vesselforsaleIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
            vesselforsaleIvUser.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("contact us")) {
            txtContactUsTV.setTextColor(getResources().getColor(R.color.blue));
            contactIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("staff")) {
            txtStaffTV.setTextColor(getResources().getColor(R.color.blue));
            staffIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("support")) {
            txtSupportTV.setTextColor(getResources().getColor(R.color.blue));
            supportIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("blogs")) {
            txtblogsTV.setTextColor(getResources().getColor(R.color.blue));
            txtblogsTVUser.setTextColor(getResources().getColor(R.color.blue));
            blogIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
            blogIvUser.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("notifications")) {
            notificationTV.setTextColor(getResources().getColor(R.color.blue));
            notificationTVUser.setTextColor(getResources().getColor(R.color.blue));
            imgBell.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
            imgBellUser.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("web login")) {
            webLoginTV.setTextColor(getResources().getColor(R.color.blue));
            webIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("fav vessels")) {
            txtFavoriteVesselTV.setTextColor(getResources().getColor(R.color.blue));
            favoritevesselIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (type.equals("live support")) {
            txtliveSupportTV.setTextColor(getResources().getColor(R.color.blue));
            liveSupportIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        }
    }
}
