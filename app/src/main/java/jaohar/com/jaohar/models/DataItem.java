package jaohar.com.jaohar.models;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("added_by")
	private String addedBy;

	@SerializedName("Address4")
	private String address4;

	@SerializedName("enable")
	private String enable;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("Address5")
	private String address5;

	@SerializedName("modification_date")
	private String modificationDate;

	@SerializedName("Address2")
	private String address2;

	@SerializedName("Address3")
	private String address3;

	@SerializedName("id")
	private String id;

	@SerializedName("Address1")
	private String address1;

	public String getAddedBy(){
		return addedBy;
	}

	public String getAddress4(){
		return address4;
	}

	public String getEnable(){
		return enable;
	}

	public String getCompanyName(){
		return companyName;
	}

	public String getAddress5(){
		return address5;
	}

	public String getModificationDate(){
		return modificationDate;
	}

	public String getAddress2(){
		return address2;
	}

	public String getAddress3(){
		return address3;
	}

	public String getId(){
		return id;
	}

	public String getAddress1(){
		return address1;
	}
}