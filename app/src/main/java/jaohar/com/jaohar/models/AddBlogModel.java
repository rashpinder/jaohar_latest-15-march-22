package jaohar.com.jaohar.models;

import com.google.gson.annotations.SerializedName;

public class AddBlogModel {
    @SerializedName("status")
    int status;

    @SerializedName("message")
    String message;

    @SerializedName("id")
    int id;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
