package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankData {

    @SerializedName("bank_id")
    @Expose
    private String bankId;
    @SerializedName("beneficiary")
    @Expose
    private String beneficiary;
    @SerializedName("beneficiary_add")
    @Expose
    private String beneficiaryAdd;
    @SerializedName("beneficiary_country")
    @Expose
    private String beneficiaryCountry;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("iban_ron")
    @Expose
    private String ibanRon;
    @SerializedName("iban_usd")
    @Expose
    private String ibanUsd;
    @SerializedName("iban_eur")
    @Expose
    private String ibanEur;
    @SerializedName("iban_gbp")
    @Expose
    private String ibanGbp;
    @SerializedName("swift")
    @Expose
    private String swift;
    @SerializedName("added_by")
    @Expose
    private String addedBy;
    @SerializedName("modification_date")
    @Expose
    private String modificationDate;
    @SerializedName("enable")
    @Expose
    private String enable;

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }

    public String getBeneficiaryAdd() {
        return beneficiaryAdd;
    }

    public void setBeneficiaryAdd(String beneficiaryAdd) {
        this.beneficiaryAdd = beneficiaryAdd;
    }

    public String getBeneficiaryCountry() {
        return beneficiaryCountry;
    }

    public void setBeneficiaryCountry(String beneficiaryCountry) {
        this.beneficiaryCountry = beneficiaryCountry;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getIbanRon() {
        return ibanRon;
    }

    public void setIbanRon(String ibanRon) {
        this.ibanRon = ibanRon;
    }

    public String getIbanUsd() {
        return ibanUsd;
    }

    public void setIbanUsd(String ibanUsd) {
        this.ibanUsd = ibanUsd;
    }

    public String getIbanEur() {
        return ibanEur;
    }

    public void setIbanEur(String ibanEur) {
        this.ibanEur = ibanEur;
    }

    public String getIbanGbp() {
        return ibanGbp;
    }

    public void setIbanGbp(String ibanGbp) {
        this.ibanGbp = ibanGbp;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    @Override
    public String toString() {
        return  bankName;
    }
}
