package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links {

    @SerializedName("Show vessel in marine traffic")
    @Expose
    private String showVesselInMarineTraffic;
    @SerializedName("Ship Info")
    @Expose
    private String shipInfo;
    @SerializedName("Shipspotting Photos")
    @Expose
    private String shipspottingPhotos;
    @SerializedName("Vessel Tracker Position")
    @Expose
    private String vesselTrackerPosition;

    public String getShowVesselInMarineTraffic() {
        return showVesselInMarineTraffic;
    }

    public void setShowVesselInMarineTraffic(String showVesselInMarineTraffic) {
        this.showVesselInMarineTraffic = showVesselInMarineTraffic;
    }

    public String getShipInfo() {
        return shipInfo;
    }

    public void setShipInfo(String shipInfo) {
        this.shipInfo = shipInfo;
    }

    public String getShipspottingPhotos() {
        return shipspottingPhotos;
    }

    public void setShipspottingPhotos(String shipspottingPhotos) {
        this.shipspottingPhotos = shipspottingPhotos;
    }

    public String getVesselTrackerPosition() {
        return vesselTrackerPosition;
    }

    public void setVesselTrackerPosition(String vesselTrackerPosition) {
        this.vesselTrackerPosition = vesselTrackerPosition;
    }

}
