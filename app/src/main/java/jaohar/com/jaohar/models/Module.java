package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Module {

    @SerializedName("module_id")
    @Expose
    private String moduleId;
    @SerializedName("access_type")
    @Expose
    private String accessType;
    @SerializedName("module_name")
    @Expose
    private String moduleName;
    @SerializedName("module_link")
    @Expose
    private String moduleLink;
    @SerializedName("module_image")
    @Expose
    private String moduleImage;
    @SerializedName("app_image")
    @Expose
    private String appImage;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("access_to")
    @Expose
    private String accessTo;
    @SerializedName("disable")
    @Expose
    private String disable;
    @SerializedName("creation_date")
    @Expose
    private String creationDate;

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleLink() {
        return moduleLink;
    }

    public void setModuleLink(String moduleLink) {
        this.moduleLink = moduleLink;
    }

    public String getModuleImage() {
        return moduleImage;
    }

    public void setModuleImage(String moduleImage) {
        this.moduleImage = moduleImage;
    }

    public String getAppImage() {
        return appImage;
    }

    public void setAppImage(String appImage) {
        this.appImage = appImage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccessTo() {
        return accessTo;
    }

    public void setAccessTo(String accessTo) {
        this.accessTo = accessTo;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

}