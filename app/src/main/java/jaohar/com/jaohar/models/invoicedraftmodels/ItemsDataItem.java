package jaohar.com.jaohar.models.invoicedraftmodels;

import com.google.gson.annotations.SerializedName;

public class ItemsDataItem{

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("item_id")
	private String itemId;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("price")
	private String price;

	@SerializedName("TotalAmountUnit")
	private int totalAmountUnit;

	@SerializedName("arraylistDiscount")
	private String arraylistDiscount;

	@SerializedName("description")
	private String description;

	@SerializedName("TotalAmount")
	private int totalAmount;

	@SerializedName("TotalunitPrice")
	private String totalunitPrice;

	@SerializedName("item_serial_no")
	private String itemSerialNo;

	@SerializedName("status")
	private String status;

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setItemId(String itemId){
		this.itemId = itemId;
	}

	public String getItemId(){
		return itemId;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setTotalAmountUnit(int totalAmountUnit){
		this.totalAmountUnit = totalAmountUnit;
	}

	public int getTotalAmountUnit(){
		return totalAmountUnit;
	}

	public void setArraylistDiscount(String arraylistDiscount){
		this.arraylistDiscount = arraylistDiscount;
	}

	public String getArraylistDiscount(){
		return arraylistDiscount;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setTotalAmount(int totalAmount){
		this.totalAmount = totalAmount;
	}

	public int getTotalAmount(){
		return totalAmount;
	}

	public void setTotalunitPrice(String totalunitPrice){
		this.totalunitPrice = totalunitPrice;
	}

	public String getTotalunitPrice(){
		return totalunitPrice;
	}

	public void setItemSerialNo(String itemSerialNo){
		this.itemSerialNo = itemSerialNo;
	}

	public String getItemSerialNo(){
		return itemSerialNo;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}