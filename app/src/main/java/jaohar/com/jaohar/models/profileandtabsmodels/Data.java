package jaohar.com.jaohar.models.profileandtabsmodels;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("chat_status")
	private String chatStatus;

	@SerializedName("role")
	private String role;

	@SerializedName("scroll_text")
	private String scrollText;

	@SerializedName("unread_chat")
	private int unreadChat;

	@SerializedName("total_unread_forum_message_count")
	private int totalUnreadForumMessageCount;

	@SerializedName("bio")
	private String bio;

	@SerializedName("enabled")
	private String enabled;

	@SerializedName("unread_forum_message_count")
	private int unreadForumMessageCount;

	@SerializedName("password")
	private String password;

	@SerializedName("login_qrcode")
	private String loginQrcode;

	@SerializedName("chat_approve")
	private String chatApprove;

	@SerializedName("id")
	private String id;

	@SerializedName("cover_image")
	private String coverImage;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("all_news")
	private List<AllNewsItem> allNews;

	@SerializedName("email")
	private String email;

	@SerializedName("unread_uk_forum_message_count")
	private int unreadUkForumMessageCount;

	@SerializedName("image")
	private String image;

	@SerializedName("chat_support_approve")
	private String chatSupportApprove;

	@SerializedName("created")
	private String created;

	@SerializedName("app_login")
	private String appLogin;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("desktop_notify")
	private String desktopNotify;

	@SerializedName("modules")
	private List<ModulesItem> modules;

	@SerializedName("deleted")
	private String deleted;

	@SerializedName("online_state")
	private String onlineState;

	@SerializedName("web_login")
	private String webLogin;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("unread_notify")
	private int unreadNotify;

	@SerializedName("job")
	private String job;

	@SerializedName("status")
	private String status;

	public void setChatStatus(String chatStatus){
		this.chatStatus = chatStatus;
	}

	public String getChatStatus(){
		return chatStatus;
	}

	public void setRole(String role){
		this.role = role;
	}

	public String getRole(){
		return role;
	}

	public void setScrollText(String scrollText){
		this.scrollText = scrollText;
	}

	public String getScrollText(){
		return scrollText;
	}

	public void setUnreadChat(int unreadChat){
		this.unreadChat = unreadChat;
	}

	public int getUnreadChat(){
		return unreadChat;
	}

	public void setTotalUnreadForumMessageCount(int totalUnreadForumMessageCount){
		this.totalUnreadForumMessageCount = totalUnreadForumMessageCount;
	}

	public int getTotalUnreadForumMessageCount(){
		return totalUnreadForumMessageCount;
	}

	public void setBio(String bio){
		this.bio = bio;
	}

	public String getBio(){
		return bio;
	}

	public void setEnabled(String enabled){
		this.enabled = enabled;
	}

	public String getEnabled(){
		return enabled;
	}

	public void setUnreadForumMessageCount(int unreadForumMessageCount){
		this.unreadForumMessageCount = unreadForumMessageCount;
	}

	public int getUnreadForumMessageCount(){
		return unreadForumMessageCount;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setLoginQrcode(String loginQrcode){
		this.loginQrcode = loginQrcode;
	}

	public String getLoginQrcode(){
		return loginQrcode;
	}

	public void setChatApprove(String chatApprove){
		this.chatApprove = chatApprove;
	}

	public String getChatApprove(){
		return chatApprove;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCoverImage(String coverImage){
		this.coverImage = coverImage;
	}

	public String getCoverImage(){
		return coverImage;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setAllNews(List<AllNewsItem> allNews){
		this.allNews = allNews;
	}

	public List<AllNewsItem> getAllNews(){
		return allNews;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setUnreadUkForumMessageCount(int unreadUkForumMessageCount){
		this.unreadUkForumMessageCount = unreadUkForumMessageCount;
	}

	public int getUnreadUkForumMessageCount(){
		return unreadUkForumMessageCount;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setChatSupportApprove(String chatSupportApprove){
		this.chatSupportApprove = chatSupportApprove;
	}

	public String getChatSupportApprove(){
		return chatSupportApprove;
	}

	public void setCreated(String created){
		this.created = created;
	}

	public String getCreated(){
		return created;
	}

	public void setAppLogin(String appLogin){
		this.appLogin = appLogin;
	}

	public String getAppLogin(){
		return appLogin;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setDesktopNotify(String desktopNotify){
		this.desktopNotify = desktopNotify;
	}

	public String getDesktopNotify(){
		return desktopNotify;
	}

	public void setModules(List<ModulesItem> modules){
		this.modules = modules;
	}

	public List<ModulesItem> getModules(){
		return modules;
	}

	public void setDeleted(String deleted){
		this.deleted = deleted;
	}

	public String getDeleted(){
		return deleted;
	}

	public void setOnlineState(String onlineState){
		this.onlineState = onlineState;
	}

	public String getOnlineState(){
		return onlineState;
	}

	public void setWebLogin(String webLogin){
		this.webLogin = webLogin;
	}

	public String getWebLogin(){
		return webLogin;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setUnreadNotify(int unreadNotify){
		this.unreadNotify = unreadNotify;
	}

	public int getUnreadNotify(){
		return unreadNotify;
	}

	public void setJob(String job){
		this.job = job;
	}

	public String getJob(){
		return job;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}