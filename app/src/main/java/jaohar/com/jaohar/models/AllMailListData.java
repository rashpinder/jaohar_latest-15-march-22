package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllMailListData {

    @SerializedName("all_mail_list_contacts")
    @Expose
    private ArrayList<AllMailListContact> allMailListContacts = null;
    @SerializedName("last_page")
    @Expose
    private String lastPage;

    public ArrayList<AllMailListContact> getAllMailListContacts() {
        return allMailListContacts;
    }

    public void setAllMailListContacts(ArrayList<AllMailListContact> allMailListContacts) {
        this.allMailListContacts = allMailListContacts;
    }

    public String getLastPage() {
        return lastPage;
    }

    public void setLastPage(String lastPage) {
        this.lastPage = lastPage;
    }

}
