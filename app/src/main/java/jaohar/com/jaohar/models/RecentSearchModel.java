package jaohar.com.jaohar.models;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecentSearchModel {
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private List<String> data = null;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<String> getData() {
            return data;
        }

        public void setData(List<String> data) {
            this.data = data;
        }

    }
//
//    @SerializedName("status")
//    @Expose
//    private String status;
//    @SerializedName("message")
//    @Expose
//    private String message;
//    @SerializedName("data")
//    @Expose
//    private ArrayList<GetVesselTypeData> data = null;
////    @SerializedName("data")
////    @Expose
////    private List<String> data = null;
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public ArrayList<GetVesselTypeData> getData() {
//        return data;
//    }
//
//    public void setData(ArrayList<GetVesselTypeData> data) {
//        this.data = data;
//    }
//
////    public List<String> getData() {
////        return data;
////    }
////
////    public void setData(List<String> data) {
////        this.data = data;
////    }

//}