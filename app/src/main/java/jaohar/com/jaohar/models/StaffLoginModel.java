package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StaffLoginModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("support_access")
    @Expose
    private Boolean supportAccess;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Boolean getSupportAccess() {
        return supportAccess;
    }

    public void setSupportAccess(Boolean supportAccess) {
        this.supportAccess = supportAccess;
    }

    public class Data {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("role")
        @Expose
        private String role;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("enabled")
        @Expose
        private String enabled;
        @SerializedName("company_name")
        @Expose
        private String companyName;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("job")
        @Expose
        private String job;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("cover_image")
        @Expose
        private String coverImage;
        @SerializedName("bio")
        @Expose
        private String bio;
        @SerializedName("chat_approve")
        @Expose
        private String chatApprove;
        @SerializedName("chat_status")
        @Expose
        private String chatStatus;
        @SerializedName("chat_support_approve")
        @Expose
        private String chatSupportApprove;
        @SerializedName("login_qrcode")
        @Expose
        private String loginQrcode;
        @SerializedName("desktop_notify")
        @Expose
        private String desktopNotify;
        @SerializedName("app_notify")
        @Expose
        private String appNotify;
        @SerializedName("deleted")
        @Expose
        private String deleted;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getEnabled() {
            return enabled;
        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getJob() {
            return job;
        }

        public void setJob(String job) {
            this.job = job;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCoverImage() {
            return coverImage;
        }

        public void setCoverImage(String coverImage) {
            this.coverImage = coverImage;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getChatApprove() {
            return chatApprove;
        }

        public void setChatApprove(String chatApprove) {
            this.chatApprove = chatApprove;
        }

        public String getChatStatus() {
            return chatStatus;
        }

        public void setChatStatus(String chatStatus) {
            this.chatStatus = chatStatus;
        }

        public String getChatSupportApprove() {
            return chatSupportApprove;
        }

        public void setChatSupportApprove(String chatSupportApprove) {
            this.chatSupportApprove = chatSupportApprove;
        }

        public String getLoginQrcode() {
            return loginQrcode;
        }

        public void setLoginQrcode(String loginQrcode) {
            this.loginQrcode = loginQrcode;
        }

        public String getDesktopNotify() {
            return desktopNotify;
        }

        public void setDesktopNotify(String desktopNotify) {
            this.desktopNotify = desktopNotify;
        }

        public String getAppNotify() {
            return appNotify;
        }

        public void setAppNotify(String appNotify) {
            this.appNotify = appNotify;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }

    }

}