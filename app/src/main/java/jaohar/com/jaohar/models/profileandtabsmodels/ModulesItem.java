package jaohar.com.jaohar.models.profileandtabsmodels;

import com.google.gson.annotations.SerializedName;

public class ModulesItem{

	@SerializedName("access_type")
	private String accessType;

	@SerializedName("module_id")
	private String moduleId;

	@SerializedName("disable")
	private String disable;

	@SerializedName("module_image")
	private String moduleImage;

	@SerializedName("module_link")
	private String moduleLink;

	@SerializedName("access_to")
	private String accessTo;

	@SerializedName("module_name")
	private String moduleName;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("type")
	private String type;

	@SerializedName("sort_order")
	private String sortOrder;

	@SerializedName("app_image")
	private String appImage;

	@SerializedName("app_selected_image")
	private String appSelectedImage;

	public void setAccessType(String accessType){
		this.accessType = accessType;
	}

	public String getAccessType(){
		return accessType;
	}

	public void setModuleId(String moduleId){
		this.moduleId = moduleId;
	}

	public String getModuleId(){
		return moduleId;
	}

	public void setDisable(String disable){
		this.disable = disable;
	}

	public String getDisable(){
		return disable;
	}

	public void setModuleImage(String moduleImage){
		this.moduleImage = moduleImage;
	}

	public String getModuleImage(){
		return moduleImage;
	}

	public void setModuleLink(String moduleLink){
		this.moduleLink = moduleLink;
	}

	public String getModuleLink(){
		return moduleLink;
	}

	public void setAccessTo(String accessTo){
		this.accessTo = accessTo;
	}

	public String getAccessTo(){
		return accessTo;
	}

	public void setModuleName(String moduleName){
		this.moduleName = moduleName;
	}

	public String getModuleName(){
		return moduleName;
	}

	public void setCreationDate(String creationDate){
		this.creationDate = creationDate;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setSortOrder(String sortOrder){
		this.sortOrder = sortOrder;
	}

	public String getSortOrder(){
		return sortOrder;
	}

	public void setAppImage(String appImage){
		this.appImage = appImage;
	}

	public String getAppImage(){
		return appImage;
	}

	public void setAppSelectedImage(String appSelectedImage){
		this.appSelectedImage = appSelectedImage;
	}

	public String getAppSelectedImage(){
		return appSelectedImage;
	}
}