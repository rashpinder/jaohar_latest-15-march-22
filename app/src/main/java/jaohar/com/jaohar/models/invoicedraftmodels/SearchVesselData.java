package jaohar.com.jaohar.models.invoicedraftmodels;

import com.google.gson.annotations.SerializedName;

public class SearchVesselData{

	@SerializedName("IMO_no")
	private String iMONo;

	@SerializedName("flag")
	private String flag;

	@SerializedName("added_by")
	private String addedBy;

	@SerializedName("enable")
	private String enable;

	@SerializedName("modification_date")
	private String modificationDate;

	@SerializedName("vessel_id")
	private String vesselId;

	@SerializedName("vessel_name")
	private String vesselName;

	public void setIMONo(String iMONo){
		this.iMONo = iMONo;
	}

	public String getIMONo(){
		return iMONo;
	}

	public void setFlag(String flag){
		this.flag = flag;
	}

	public String getFlag(){
		return flag;
	}

	public void setAddedBy(String addedBy){
		this.addedBy = addedBy;
	}

	public String getAddedBy(){
		return addedBy;
	}

	public void setEnable(String enable){
		this.enable = enable;
	}

	public String getEnable(){
		return enable;
	}

	public void setModificationDate(String modificationDate){
		this.modificationDate = modificationDate;
	}

	public String getModificationDate(){
		return modificationDate;
	}

	public void setVesselId(String vesselId){
		this.vesselId = vesselId;
	}

	public String getVesselId(){
		return vesselId;
	}

	public void setVesselName(String vesselName){
		this.vesselName = vesselName;
	}

	public String getVesselName(){
		return vesselName;
	}
}