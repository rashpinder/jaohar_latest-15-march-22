package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//public class Datum {
//
//    @SerializedName("id")
//    @Expose
//    private String id;
//    @SerializedName("news")
//    @Expose
//    private String news;
//    @SerializedName("type")
//    @Expose
//    private String type;
//    @SerializedName("photo")
//    @Expose
//    private String photo;
//    @SerializedName("created_at")
//    @Expose
//    private String createdAt;
//    @SerializedName("disable")
//    @Expose
//    private String disable;
//    @SerializedName("news_text")
//    @Expose
//    private String newsText;
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getNews() {
//        return news;
//    }
//
//    public void setNews(String news) {
//        this.news = news;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public String getPhoto() {
//        return photo;
//    }
//
//    public void setPhoto(String photo) {
//        this.photo = photo;
//    }
//
//    public String getCreatedAt() {
//        return createdAt;
//    }
//
//    public void setCreatedAt(String createdAt) {
//        this.createdAt = createdAt;
//    }
//
//    public String getDisable() {
//        return disable;
//    }
//
//    public void setDisable(String disable) {
//        this.disable = disable;
//    }
//
//    public String getNewsText() {
//        return newsText;
//    }
//
//    public void setNewsText(String newsText) {
//        this.newsText = newsText;
//    }
//
//}

public class AllNewsModel implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

}