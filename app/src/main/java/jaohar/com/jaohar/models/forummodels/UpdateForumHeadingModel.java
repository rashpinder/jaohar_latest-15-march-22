package jaohar.com.jaohar.models.forummodels;

import com.google.gson.annotations.SerializedName;

public class UpdateForumHeadingModel {

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public String getMessage(){
		return message;
	}

	public int getStatus(){
		return status;
	}
}