package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AllAdminBySearchModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<AllUser> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AllUser> getData() {
        return data;
    }

    public void setData(ArrayList<AllUser> data) {
        this.data = data;
    }

}
