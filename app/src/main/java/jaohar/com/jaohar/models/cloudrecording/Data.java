package jaohar.com.jaohar.models.cloudrecording;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("resourceId")
	private String resourceId;

	@SerializedName("code")
	private int code;

	@SerializedName("sid")
	private String sid;

	public void setResourceId(String resourceId){
		this.resourceId = resourceId;
	}

	public String getResourceId(){
		return resourceId;
	}

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setSid(String sid){
		this.sid = sid;
	}

	public String getSid(){
		return sid;
	}
}