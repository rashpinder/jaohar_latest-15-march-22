package jaohar.com.jaohar.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OwnersModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

public class Datum {

    @SerializedName("owner_id")
    @Expose
    private String ownerId;
    @SerializedName("owner_name")
    @Expose
    private String ownerName;
    @SerializedName("owner_company")
    @Expose
    private String ownerCompany;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("address3")
    @Expose
    private String address3;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("registry_no")
    @Expose
    private String registryNo;
    @SerializedName("fiscal_code")
    @Expose
    private String fiscalCode;
    @SerializedName("imo_no")
    @Expose
    private String imoNo;
    @SerializedName("vat_no")
    @Expose
    private String vatNo;
    @SerializedName("tic_no")
    @Expose
    private String ticNo;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("header")
    @Expose
    private String header;
    @SerializedName("footer")
    @Expose
    private String footer;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("head_color")
    @Expose
    private String headColor;
    @SerializedName("table_head_color")
    @Expose
    private String tableHeadColor;
    @SerializedName("table_row_color")
    @Expose
    private String tableRowColor;
    @SerializedName("gradient_color")
    @Expose
    private String gradientColor;
    @SerializedName("prefix")
    @Expose
    private String prefix;
    @SerializedName("signature")
    @Expose
    private String signature;
    @SerializedName("logo_text")
    @Expose
    private String logoText;
    @SerializedName("logo_text_color")
    @Expose
    private String logoTextColor;
    @SerializedName("added_by")
    @Expose
    private String addedBy;
    @SerializedName("modification_date")
    @Expose
    private String modificationDate;
    @SerializedName("enable")
    @Expose
    private String enable;

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerCompany() {
        return ownerCompany;
    }

    public void setOwnerCompany(String ownerCompany) {
        this.ownerCompany = ownerCompany;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistryNo() {
        return registryNo;
    }

    public void setRegistryNo(String registryNo) {
        this.registryNo = registryNo;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getImoNo() {
        return imoNo;
    }

    public void setImoNo(String imoNo) {
        this.imoNo = imoNo;
    }

    public String getVatNo() {
        return vatNo;
    }

    public void setVatNo(String vatNo) {
        this.vatNo = vatNo;
    }

    public String getTicNo() {
        return ticNo;
    }

    public void setTicNo(String ticNo) {
        this.ticNo = ticNo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getHeadColor() {
        return headColor;
    }

    public void setHeadColor(String headColor) {
        this.headColor = headColor;
    }

    public String getTableHeadColor() {
        return tableHeadColor;
    }

    public void setTableHeadColor(String tableHeadColor) {
        this.tableHeadColor = tableHeadColor;
    }

    public String getTableRowColor() {
        return tableRowColor;
    }

    public void setTableRowColor(String tableRowColor) {
        this.tableRowColor = tableRowColor;
    }

    public String getGradientColor() {
        return gradientColor;
    }

    public void setGradientColor(String gradientColor) {
        this.gradientColor = gradientColor;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getLogoText() {
        return logoText;
    }

    public void setLogoText(String logoText) {
        this.logoText = logoText;
    }

    public String getLogoTextColor() {
        return logoTextColor;
    }

    public void setLogoTextColor(String logoTextColor) {
        this.logoTextColor = logoTextColor;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

}}
