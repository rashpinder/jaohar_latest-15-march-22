package jaohar.com.jaohar.models.chatmodels.chatusers;

import com.google.gson.annotations.SerializedName;

public class AllUsersItem{

	@SerializedName("room_id")
	private String roomId;

	@SerializedName("image")
	private String image;

	@SerializedName("role")
	private String role;

	@SerializedName("receiver_id")
	private String receiverId;

	@SerializedName("unread_chat")
	private String unreadChat;

	@SerializedName("last_message")
	private String lastMessage;

	@SerializedName("message")
	private String message;

	@SerializedName("sender_id")
	private String senderId;

	@SerializedName("room_status")
	private String roomStatus;

	@SerializedName("message_time")
	private String messageTime;

	@SerializedName("request_time")
	private String requestTime;

	@SerializedName("online_state")
	private String onlineState;

	@SerializedName("group_id")
	private String groupId;

	@SerializedName("admin_id")
	private String adminId;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	@SerializedName("room_type")
	private String roomType;

	public void setRoomId(String roomId){
		this.roomId = roomId;
	}

	public String getRoomId(){
		return roomId;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setRole(String role){
		this.role = role;
	}

	public String getRole(){
		return role;
	}

	public void setReceiverId(String receiverId){
		this.receiverId = receiverId;
	}

	public String getReceiverId(){
		return receiverId;
	}

	public void setUnreadChat(String unreadChat){
		this.unreadChat = unreadChat;
	}

	public String getUnreadChat(){
		return unreadChat;
	}

	public void setLastMessage(String lastMessage){
		this.lastMessage = lastMessage;
	}

	public String getLastMessage(){
		return lastMessage;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setSenderId(String senderId){
		this.senderId = senderId;
	}

	public String getSenderId(){
		return senderId;
	}

	public void setRoomStatus(String roomStatus){
		this.roomStatus = roomStatus;
	}

	public String getRoomStatus(){
		return roomStatus;
	}

	public void setMessageTime(String messageTime){
		this.messageTime = messageTime;
	}

	public String getMessageTime(){
		return messageTime;
	}

	public void setRequestTime(String requestTime){
		this.requestTime = requestTime;
	}

	public String getRequestTime(){
		return requestTime;
	}

	public void setOnlineState(String onlineState){
		this.onlineState = onlineState;
	}

	public String getOnlineState(){
		return onlineState;
	}

	public void setGroupId(String groupId){
		this.groupId = groupId;
	}

	public String getGroupId(){
		return groupId;
	}

	public void setAdminId(String adminId){
		this.adminId = adminId;
	}

	public String getAdminId(){
		return adminId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setRoomType(String roomType){
		this.roomType = roomType;
	}

	public String getRoomType(){
		return roomType;
	}
}