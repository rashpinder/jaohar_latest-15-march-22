package jaohar.com.jaohar.models.invoicedraftmodels;

import com.google.gson.annotations.SerializedName;

public class SignData{

	@SerializedName("sign_name")
	private String signName;

	@SerializedName("sign_id")
	private String signId;

	@SerializedName("sign_image")
	private String signImage;

	@SerializedName("added_by")
	private String addedBy;

	@SerializedName("enable")
	private String enable;

	@SerializedName("modification_date")
	private String modificationDate;

	public void setSignName(String signName){
		this.signName = signName;
	}

	public String getSignName(){
		return signName;
	}

	public void setSignId(String signId){
		this.signId = signId;
	}

	public String getSignId(){
		return signId;
	}

	public void setSignImage(String signImage){
		this.signImage = signImage;
	}

	public String getSignImage(){
		return signImage;
	}

	public void setAddedBy(String addedBy){
		this.addedBy = addedBy;
	}

	public String getAddedBy(){
		return addedBy;
	}

	public void setEnable(String enable){
		this.enable = enable;
	}

	public String getEnable(){
		return enable;
	}

	public void setModificationDate(String modificationDate){
		this.modificationDate = modificationDate;
	}

	public String getModificationDate(){
		return modificationDate;
	}
}