package jaohar.com.jaohar.models.chatmodels;

import com.google.gson.annotations.SerializedName;

public class UserDetail{

	@SerializedName("image")
	private String image;

	@SerializedName("chat_status")
	private String chatStatus;

	@SerializedName("role")
	private String role;

	@SerializedName("chat_support_approve")
	private String chatSupportApprove;

	@SerializedName("created")
	private String created;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("bio")
	private String bio;

	@SerializedName("desktop_notify")
	private String desktopNotify;

	@SerializedName("enabled")
	private String enabled;

	@SerializedName("password")
	private String password;

	@SerializedName("deleted")
	private String deleted;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("login_qrcode")
	private String loginQrcode;

	@SerializedName("app_notify")
	private String appNotify;

	@SerializedName("chat_approve")
	private String chatApprove;

	@SerializedName("id")
	private String id;

	@SerializedName("cover_image")
	private String coverImage;

	@SerializedName("job")
	private String job;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("email")
	private String email;

	@SerializedName("status")
	private String status;

	public String getImage(){
		return image;
	}

	public String getChatStatus(){
		return chatStatus;
	}

	public String getRole(){
		return role;
	}

	public String getChatSupportApprove(){
		return chatSupportApprove;
	}

	public String getCreated(){
		return created;
	}

	public String getLastName(){
		return lastName;
	}

	public String getBio(){
		return bio;
	}

	public String getDesktopNotify(){
		return desktopNotify;
	}

	public String getEnabled(){
		return enabled;
	}

	public String getPassword(){
		return password;
	}

	public String getDeleted(){
		return deleted;
	}

	public String getCompanyName(){
		return companyName;
	}

	public String getLoginQrcode(){
		return loginQrcode;
	}

	public String getAppNotify(){
		return appNotify;
	}

	public String getChatApprove(){
		return chatApprove;
	}

	public String getId(){
		return id;
	}

	public String getCoverImage(){
		return coverImage;
	}

	public String getJob(){
		return job;
	}

	public String getFirstName(){
		return firstName;
	}

	public String getEmail(){
		return email;
	}

	public String getStatus(){
		return status;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setChatStatus(String chatStatus) {
		this.chatStatus = chatStatus;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setChatSupportApprove(String chatSupportApprove) {
		this.chatSupportApprove = chatSupportApprove;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public void setDesktopNotify(String desktopNotify) {
		this.desktopNotify = desktopNotify;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setLoginQrcode(String loginQrcode) {
		this.loginQrcode = loginQrcode;
	}

	public void setAppNotify(String appNotify) {
		this.appNotify = appNotify;
	}

	public void setChatApprove(String chatApprove) {
		this.chatApprove = chatApprove;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setCoverImage(String coverImage) {
		this.coverImage = coverImage;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}