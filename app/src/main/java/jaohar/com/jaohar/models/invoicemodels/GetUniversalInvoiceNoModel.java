package jaohar.com.jaohar.models.invoicemodels;

import com.google.gson.annotations.SerializedName;

public class GetUniversalInvoiceNoModel{

	@SerializedName("data")
	private int data;

	@SerializedName("prefix")
	private String prefix;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public int getData(){
		return data;
	}

	public String getPrefix(){
		return prefix;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}