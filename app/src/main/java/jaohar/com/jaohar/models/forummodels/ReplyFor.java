package jaohar.com.jaohar.models.forummodels;

import com.google.gson.annotations.SerializedName;

public class ReplyFor{

	@SerializedName("room_id")
	private String roomId;

	@SerializedName("image5")
	private String image5;

	@SerializedName("image")
	private String image;

	@SerializedName("image6")
	private String image6;

	@SerializedName("image3")
	private String image3;

	@SerializedName("image4")
	private String image4;

	@SerializedName("image9")
	private String image9;

	@SerializedName("read_by")
	private String readBy;

	@SerializedName("edited")
	private String edited;

	@SerializedName("image7")
	private String image7;

	@SerializedName("user_detail")
	private UserDetail userDetail;

	@SerializedName("image8")
	private String image8;

	@SerializedName("edit_refresh")
	private String editRefresh;

	@SerializedName("message_id")
	private String messageId;

	@SerializedName("creation_date")
	private String creationDate;

	@SerializedName("message")
	private String message;

	@SerializedName("image2")
	private String image2;

	@SerializedName("forum_id")
	private String forumId;

	@SerializedName("image10")
	private String image10;

	@SerializedName("edit_refresh_app")
	private String editRefreshApp;

	@SerializedName("reply_id")
	private String replyId;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("enable")
	private String enable;

	@SerializedName("image_count")
	private String imageCount;

	public String getRoomId(){
		return roomId;
	}

	public String getImage5(){
		return image5;
	}

	public String getImage(){
		return image;
	}

	public String getImage6(){
		return image6;
	}

	public String getImage3(){
		return image3;
	}

	public String getImage4(){
		return image4;
	}

	public String getImage9(){
		return image9;
	}

	public String getReadBy(){
		return readBy;
	}

	public String getEdited(){
		return edited;
	}

	public String getImage7(){
		return image7;
	}

	public UserDetail getUserDetail(){
		return userDetail;
	}

	public String getImage8(){
		return image8;
	}

	public String getEditRefresh(){
		return editRefresh;
	}

	public String getMessageId(){
		return messageId;
	}

	public String getCreationDate(){
		return creationDate;
	}

	public String getMessage(){
		return message;
	}

	public String getImage2(){
		return image2;
	}

	public String getForumId(){
		return forumId;
	}

	public String getImage10(){
		return image10;
	}

	public String getEditRefreshApp(){
		return editRefreshApp;
	}

	public String getReplyId(){
		return replyId;
	}

	public String getUserId(){
		return userId;
	}

	public String getEnable(){
		return enable;
	}

	public String getImageCount(){
		return imageCount;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public void setImage5(String image5) {
		this.image5 = image5;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setImage6(String image6) {
		this.image6 = image6;
	}

	public void setImage3(String image3) {
		this.image3 = image3;
	}

	public void setImage4(String image4) {
		this.image4 = image4;
	}

	public void setImage9(String image9) {
		this.image9 = image9;
	}

	public void setReadBy(String readBy) {
		this.readBy = readBy;
	}

	public void setEdited(String edited) {
		this.edited = edited;
	}

	public void setImage7(String image7) {
		this.image7 = image7;
	}

	public void setUserDetail(UserDetail userDetail) {
		this.userDetail = userDetail;
	}

	public void setImage8(String image8) {
		this.image8 = image8;
	}

	public void setEditRefresh(String editRefresh) {
		this.editRefresh = editRefresh;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setImage2(String image2) {
		this.image2 = image2;
	}

	public void setForumId(String forumId) {
		this.forumId = forumId;
	}

	public void setImage10(String image10) {
		this.image10 = image10;
	}

	public void setEditRefreshApp(String editRefreshApp) {
		this.editRefreshApp = editRefreshApp;
	}

	public void setReplyId(String replyId) {
		this.replyId = replyId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	public void setImageCount(String imageCount) {
		this.imageCount = imageCount;
	}
}