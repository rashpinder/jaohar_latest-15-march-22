package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CompanyData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("news")
    @Expose
    private String news;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("staff_news")
    @Expose
    private String staffNews;
    @SerializedName("staff_photo")
    @Expose
    private String staffPhoto;
    @SerializedName("staff_news_status")
    @Expose
    private String staffNewsStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("disable")
    @Expose
    private String disable;
    @SerializedName("news_text")
    @Expose
    private String newsText;
    @SerializedName("staff_news_text")
    @Expose
    private String staffNewsText;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStaffNews() {
        return staffNews;
    }

    public void setStaffNews(String staffNews) {
        this.staffNews = staffNews;
    }

    public String getStaffPhoto() {
        return staffPhoto;
    }

    public void setStaffPhoto(String staffPhoto) {
        this.staffPhoto = staffPhoto;
    }

    public String getStaffNewsStatus() {
        return staffNewsStatus;
    }

    public void setStaffNewsStatus(String staffNewsStatus) {
        this.staffNewsStatus = staffNewsStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

    public String getNewsText() {
        return newsText;
    }

    public void setNewsText(String newsText) {
        this.newsText = newsText;
    }

    public String getStaffNewsText() {
        return staffNewsText;
    }

    public void setStaffNewsText(String staffNewsText) {
        this.staffNewsText = staffNewsText;
    }

}
