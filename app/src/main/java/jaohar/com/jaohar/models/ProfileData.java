package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("enabled")
    @Expose
    private String enabled;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("job")
    @Expose
    private String job;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("cover_image")
    @Expose
    private String coverImage;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("chat_approve")
    @Expose
    private String chatApprove;
    @SerializedName("chat_status")
    @Expose
    private String chatStatus;
    @SerializedName("chat_support_approve")
    @Expose
    private String chatSupportApprove;
    @SerializedName("login_qrcode")
    @Expose
    private String loginQrcode;
    @SerializedName("desktop_notify")
    @Expose
    private String desktopNotify;
    @SerializedName("app_notify")
    @Expose
    private String appNotify;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("online_state")
    @Expose
    private String onlineState;
    @SerializedName("unread_chat")
    @Expose
    private Integer unreadChat;
    @SerializedName("unread_notify")
    @Expose
    private Integer unreadNotify;
    @SerializedName("all_news")
    @Expose
    private List<AllNews> allNews = null;
    @SerializedName("scroll_text")
    @Expose
    private String scrollText;
    @SerializedName("unread_forum_message_count")
    @Expose
    private Integer unreadForumMessageCount;
    @SerializedName("unread_uk_forum_message_count")
    @Expose
    private Integer unreadUkForumMessageCount;
    @SerializedName("total_unread_forum_message_count")
    @Expose
    private Integer totalUnreadForumMessageCount;
    @SerializedName("modules")
    @Expose
    private List<Module> modules = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getChatApprove() {
        return chatApprove;
    }

    public void setChatApprove(String chatApprove) {
        this.chatApprove = chatApprove;
    }

    public String getChatStatus() {
        return chatStatus;
    }

    public void setChatStatus(String chatStatus) {
        this.chatStatus = chatStatus;
    }

    public String getChatSupportApprove() {
        return chatSupportApprove;
    }

    public void setChatSupportApprove(String chatSupportApprove) {
        this.chatSupportApprove = chatSupportApprove;
    }

    public String getLoginQrcode() {
        return loginQrcode;
    }

    public void setLoginQrcode(String loginQrcode) {
        this.loginQrcode = loginQrcode;
    }

    public String getDesktopNotify() {
        return desktopNotify;
    }

    public void setDesktopNotify(String desktopNotify) {
        this.desktopNotify = desktopNotify;
    }

    public String getAppNotify() {
        return appNotify;
    }

    public void setAppNotify(String appNotify) {
        this.appNotify = appNotify;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getOnlineState() {
        return onlineState;
    }

    public void setOnlineState(String onlineState) {
        this.onlineState = onlineState;
    }

    public Integer getUnreadChat() {
        return unreadChat;
    }

    public void setUnreadChat(Integer unreadChat) {
        this.unreadChat = unreadChat;
    }

    public Integer getUnreadNotify() {
        return unreadNotify;
    }

    public void setUnreadNotify(Integer unreadNotify) {
        this.unreadNotify = unreadNotify;
    }

    public List<AllNews> getAllNews() {
        return allNews;
    }

    public void setAllNews(List<AllNews> allNews) {
        this.allNews = allNews;
    }

    public String getScrollText() {
        return scrollText;
    }

    public void setScrollText(String scrollText) {
        this.scrollText = scrollText;
    }

    public Integer getUnreadForumMessageCount() {
        return unreadForumMessageCount;
    }

    public void setUnreadForumMessageCount(Integer unreadForumMessageCount) {
        this.unreadForumMessageCount = unreadForumMessageCount;
    }

    public Integer getUnreadUkForumMessageCount() {
        return unreadUkForumMessageCount;
    }

    public void setUnreadUkForumMessageCount(Integer unreadUkForumMessageCount) {
        this.unreadUkForumMessageCount = unreadUkForumMessageCount;
    }

    public Integer getTotalUnreadForumMessageCount() {
        return totalUnreadForumMessageCount;
    }

    public void setTotalUnreadForumMessageCount(Integer totalUnreadForumMessageCount) {
        this.totalUnreadForumMessageCount = totalUnreadForumMessageCount;
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

}
