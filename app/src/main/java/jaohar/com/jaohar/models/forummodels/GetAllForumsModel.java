package jaohar.com.jaohar.models.forummodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAllForumsModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class AllForum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("forum_room_id")
        @Expose
        private String forumRoomId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("vessel_id")
        @Expose
        private String vesselId;
        @SerializedName("room_id")
        @Expose
        private String roomId;
        @SerializedName("heading")
        @Expose
        private String heading;
        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("creation_date")
        @Expose
        private String creationDate;
        @SerializedName("disable")
        @Expose
        private String disable;
        @SerializedName("reactive")
        @Expose
        private String reactive;
        @SerializedName("access_type")
        @Expose
        private String accessType;
        @SerializedName("user_access_to")
        @Expose
        private String userAccessTo;
        @SerializedName("read_by")
        @Expose
        private String readBy;
        @SerializedName("message_time")
        @Expose
        private String messageTime;
        @SerializedName("vessel_record_id")
        @Expose
        private String vesselRecordId;
        @SerializedName("vessel_name")
        @Expose
        private String vesselName;
        @SerializedName("IMO_number")
        @Expose
        private String iMONumber;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("pic")
        @Expose
        private String pic;
        @SerializedName("total_messages")
        @Expose
        private Integer totalMessages;
        @SerializedName("unread_messages")
        @Expose
        private Integer unreadMessages;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getForumRoomId() {
            return forumRoomId;
        }

        public void setForumRoomId(String forumRoomId) {
            this.forumRoomId = forumRoomId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getVesselId() {
            return vesselId;
        }

        public void setVesselId(String vesselId) {
            this.vesselId = vesselId;
        }

        public String getRoomId() {
            return roomId;
        }

        public void setRoomId(String roomId) {
            this.roomId = roomId;
        }

        public String getHeading() {
            return heading;
        }

        public void setHeading(String heading) {
            this.heading = heading;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getDisable() {
            return disable;
        }

        public void setDisable(String disable) {
            this.disable = disable;
        }

        public String getReactive() {
            return reactive;
        }

        public void setReactive(String reactive) {
            this.reactive = reactive;
        }

        public String getAccessType() {
            return accessType;
        }

        public void setAccessType(String accessType) {
            this.accessType = accessType;
        }

        public String getUserAccessTo() {
            return userAccessTo;
        }

        public void setUserAccessTo(String userAccessTo) {
            this.userAccessTo = userAccessTo;
        }

        public String getReadBy() {
            return readBy;
        }

        public void setReadBy(String readBy) {
            this.readBy = readBy;
        }

        public String getMessageTime() {
            return messageTime;
        }

        public void setMessageTime(String messageTime) {
            this.messageTime = messageTime;
        }

        public String getVesselRecordId() {
            return vesselRecordId;
        }

        public void setVesselRecordId(String vesselRecordId) {
            this.vesselRecordId = vesselRecordId;
        }

        public String getVesselName() {
            return vesselName;
        }

        public void setVesselName(String vesselName) {
            this.vesselName = vesselName;
        }

        public String getIMONumber() {
            return iMONumber;
        }

        public void setIMONumber(String iMONumber) {
            this.iMONumber = iMONumber;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public Integer getTotalMessages() {
            return totalMessages;
        }

        public void setTotalMessages(Integer totalMessages) {
            this.totalMessages = totalMessages;
        }

        public Integer getUnreadMessages() {
            return unreadMessages;
        }

        public void setUnreadMessages(Integer unreadMessages) {
            this.unreadMessages = unreadMessages;
        }

    }

    public class Data {

        @SerializedName("all_forums")
        @Expose
        private List<AllForum> allForums = null;
        @SerializedName("last_page")
        @Expose
        private String lastPage;

        public List<AllForum> getAllForums() {
            return allForums;
        }

        public void setAllForums(List<AllForum> allForums) {
            this.allForums = allForums;
        }

        public String getLastPage() {
            return lastPage;
        }

        public void setLastPage(String lastPage) {
            this.lastPage = lastPage;
        }

    }
}
