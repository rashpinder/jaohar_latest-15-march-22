package jaohar.com.jaohar.models.forummodels;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("forum_detail")
	private ForumDetail forumDetail;

	public ForumDetail getForumDetail(){
		return forumDetail;
	}
}