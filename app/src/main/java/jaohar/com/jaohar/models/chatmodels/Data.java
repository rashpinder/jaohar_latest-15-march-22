package jaohar.com.jaohar.models.chatmodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data{

	@SerializedName("all_chats")
	private List<AllChatsItem> allChats;

	@SerializedName("last_page")
	private String lastPage;

	public List<AllChatsItem> getAllChats(){
		return allChats;
	}

	public String getLastPage(){
		return lastPage;
	}
}