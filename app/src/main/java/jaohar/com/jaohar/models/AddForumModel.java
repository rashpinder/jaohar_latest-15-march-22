package jaohar.com.jaohar.models;

public class AddForumModel {
        private int status;

        private String message;

        private int id;

        public void setStatus(int status){
            this.status = status;
        }
        public int getStatus(){
            return this.status;
        }
        public void setMessage(String message){
            this.message = message;
        }
        public String getMessage(){
            return this.message;
        }
        public void setId(int id){
            this.id = id;
        }
        public int getId(){
            return this.id;
        }

}
