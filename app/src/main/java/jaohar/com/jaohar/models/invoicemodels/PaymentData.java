package jaohar.com.jaohar.models.invoicemodels;

import com.google.gson.annotations.SerializedName;

public class PaymentData{

	@SerializedName("total")
	private String total;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("due")
	private String due;

	@SerializedName("payment_id")
	private String paymentId;

	@SerializedName("sub_total")
	private String subTotal;

	@SerializedName("VAT")
	private String vAT;

	@SerializedName("paid")
	private String paid;

	@SerializedName("vat_price")
	private String vatPrice;

	@SerializedName("status")
	private String status;

	public void setTotal(String total){
		this.total = total;
	}

	public String getTotal(){
		return total;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setDue(String due){
		this.due = due;
	}

	public String getDue(){
		return due;
	}

	public void setPaymentId(String paymentId){
		this.paymentId = paymentId;
	}

	public String getPaymentId(){
		return paymentId;
	}

	public void setSubTotal(String subTotal){
		this.subTotal = subTotal;
	}

	public String getSubTotal(){
		return subTotal;
	}

	public void setVAT(String vAT){
		this.vAT = vAT;
	}

	public String getVAT(){
		return vAT;
	}

	public void setPaid(String paid){
		this.paid = paid;
	}

	public String getPaid(){
		return paid;
	}

	public void setVatPrice(String vatPrice){
		this.vatPrice = vatPrice;
	}

	public String getVatPrice(){
		return vatPrice;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}