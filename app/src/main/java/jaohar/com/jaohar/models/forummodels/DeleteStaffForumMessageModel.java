package jaohar.com.jaohar.models.forummodels;

import com.google.gson.annotations.SerializedName;

public class DeleteStaffForumMessageModel{

	@SerializedName("append_data")
	private AppendData appendData;

	@SerializedName("update")
	private boolean update;

	@SerializedName("id")
	private String id;

	@SerializedName("message")
	private String message;

	@SerializedName("append_full_screen_data")
	private AppendFullScreenData appendFullScreenData;

	@SerializedName("status")
	private int status;

	public AppendData getAppendData(){
		return appendData;
	}

	public boolean isUpdate(){
		return update;
	}

	public String getId(){
		return id;
	}

	public String getMessage(){
		return message;
	}

	public AppendFullScreenData getAppendFullScreenData(){
		return appendFullScreenData;
	}

	public int getStatus(){
		return status;
	}
}