package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AdminRoleData {

    @SerializedName("all_users")
    @Expose
    private ArrayList<AllUser> allUsers = null;

    @SerializedName("last_page")
    @Expose
    private String lastPage;

    public ArrayList<AllUser> getAllUsers() {
        return allUsers;
    }

    public void setAllUsers(ArrayList<AllUser> allUsers) {
        this.allUsers = allUsers;
    }

    public String getLastPage() {
        return lastPage;
    }

    public void setLastPage(String lastPage) {
        this.lastPage = lastPage;
    }
}
