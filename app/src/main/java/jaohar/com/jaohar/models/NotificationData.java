package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NotificationData {

    @SerializedName("all_notifications")
    @Expose
    private ArrayList<AllNotification> allNotifications = null;
    @SerializedName("last_page")
    @Expose
    private String lastPage;

    public ArrayList<AllNotification> getAllNotifications() {
        return allNotifications;
    }

    public void setAllNotifications(ArrayList<AllNotification> allNotifications) {
        this.allNotifications = allNotifications;
    }

    public String getLastPage() {
        return lastPage;
    }

    public void setLastPage(String lastPage) {
        this.lastPage = lastPage;
    }

}
