package jaohar.com.jaohar.models;

import java.io.Serializable;

public class Data implements Serializable {
	private String id;
	private String email;
	private String password;
	private String role;
	private String status;
	private String created;
	private String enabled;
	private String companyName;
	private String firstName;
	private String lastName;
	private String job;
	private String image;
	private String coverImage;
	private String bio;
	private String chatApprove;
	private String chatStatus;
	private String chatSupportApprove;
	private String loginQrcode;
	private String desktopNotify;
	private String appNotify;
	private String deleted;

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setRole(String role){
		this.role = role;
	}

	public String getRole(){
		return role;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setCreated(String created){
		this.created = created;
	}

	public String getCreated(){
		return created;
	}

	public void setEnabled(String enabled){
		this.enabled = enabled;
	}

	public String getEnabled(){
		return enabled;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setJob(String job){
		this.job = job;
	}

	public String getJob(){
		return job;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setCoverImage(String coverImage){
		this.coverImage = coverImage;
	}

	public String getCoverImage(){
		return coverImage;
	}

	public void setBio(String bio){
		this.bio = bio;
	}

	public String getBio(){
		return bio;
	}

	public void setChatApprove(String chatApprove){
		this.chatApprove = chatApprove;
	}

	public String getChatApprove(){
		return chatApprove;
	}

	public void setChatStatus(String chatStatus){
		this.chatStatus = chatStatus;
	}

	public String getChatStatus(){
		return chatStatus;
	}

	public void setChatSupportApprove(String chatSupportApprove){
		this.chatSupportApprove = chatSupportApprove;
	}

	public String getChatSupportApprove(){
		return chatSupportApprove;
	}

	public void setLoginQrcode(String loginQrcode){
		this.loginQrcode = loginQrcode;
	}

	public String getLoginQrcode(){
		return loginQrcode;
	}

	public void setDesktopNotify(String desktopNotify){
		this.desktopNotify = desktopNotify;
	}

	public String getDesktopNotify(){
		return desktopNotify;
	}

	public void setAppNotify(String appNotify){
		this.appNotify = appNotify;
	}

	public String getAppNotify(){
		return appNotify;
	}

	public void setDeleted(String deleted){
		this.deleted = deleted;
	}

	public String getDeleted(){
		return deleted;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",password = '" + password + '\'' + 
			",role = '" + role + '\'' + 
			",status = '" + status + '\'' + 
			",created = '" + created + '\'' + 
			",enabled = '" + enabled + '\'' + 
			",company_name = '" + companyName + '\'' + 
			",first_name = '" + firstName + '\'' + 
			",last_name = '" + lastName + '\'' + 
			",job = '" + job + '\'' + 
			",image = '" + image + '\'' + 
			",cover_image = '" + coverImage + '\'' + 
			",bio = '" + bio + '\'' + 
			",chat_approve = '" + chatApprove + '\'' + 
			",chat_status = '" + chatStatus + '\'' + 
			",chat_support_approve = '" + chatSupportApprove + '\'' + 
			",login_qrcode = '" + loginQrcode + '\'' + 
			",desktop_notify = '" + desktopNotify + '\'' + 
			",app_notify = '" + appNotify + '\'' + 
			",deleted = '" + deleted + '\'' + 
			"}";
		}
}