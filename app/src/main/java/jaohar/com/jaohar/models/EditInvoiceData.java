package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EditInvoiceData {

    @SerializedName("all_data")
    @Expose
    public AllInvoiceData allData;
    @SerializedName("sign_data")
    @Expose
    private Signdata signData;
    @SerializedName("stamp_data")
    @Expose
    private StampsData stampData;
    @SerializedName("bank_data")
    @Expose
    private BankData bankData;
    @SerializedName("search_vessel_data")
    @Expose
    private SearchVesselData searchVesselData;
    @SerializedName("search_company_data")
    @Expose
    private SearchCompanyData searchCompanyData;
    @SerializedName("payment_data")
    @Expose
    private PaymentData paymentData;
    @SerializedName("items_data")
    @Expose
    private ArrayList<ItemsDatum> itemsData = null;
//    @SerializedName("itemdiscount")
//    @Expose
//    private String itemdiscount;
    @SerializedName("itemdiscount")
    @Expose
    private ItemDiscount itemdiscount;

    public AllInvoiceData getAllData() {
        return allData;
    }

    public void setAllData(AllInvoiceData allData) {
        this.allData = allData;
    }

    public Signdata getSignData() {
        return signData;
    }

    public void setSignData(Signdata signData) {
        this.signData = signData;
    }

    public StampsData getStampData() {
        return stampData;
    }

    public void setStampData(StampsData stampData) {
        this.stampData = stampData;
    }

    public BankData getBankData() {
        return bankData;
    }

    public void setBankData(BankData bankData) {
        this.bankData = bankData;
    }

    public SearchVesselData getSearchVesselData() {
        return searchVesselData;
    }

    public void setSearchVesselData(SearchVesselData searchVesselData) {
        this.searchVesselData = searchVesselData;
    }

    public SearchCompanyData getSearchCompanyData() {
        return searchCompanyData;
    }

    public void setSearchCompanyData(SearchCompanyData searchCompanyData) {
        this.searchCompanyData = searchCompanyData;
    }

    public PaymentData getPaymentData() {
        return paymentData;
    }

    public void setPaymentData(PaymentData paymentData) {
        this.paymentData = paymentData;
    }

    public ArrayList<ItemsDatum> getItemsData() {
        return itemsData;
    }

    public void setItemsData(ArrayList<ItemsDatum> itemsData) {
        this.itemsData = itemsData;
    }

//    public String getItemdiscount() {
//        return itemdiscount;
//    }
//
//    public void setItemdiscount(String itemdiscount) {
//        this.itemdiscount = itemdiscount;
//    }

}
