package jaohar.com.jaohar.models.chatmodels;

import com.google.gson.annotations.SerializedName;

public class AppendData{

	@SerializedName("room_id")
	private String roomId;

	@SerializedName("message_type")
	private String messageType;

	@SerializedName("sender_id")
	private String senderId;

	@SerializedName("chat_id")
	private String chatId;

	public String getRoomId(){
		return roomId;
	}

	public String getMessageType(){
		return messageType;
	}

	public String getSenderId(){
		return senderId;
	}

	public String getChatId(){
		return chatId;
	}
}