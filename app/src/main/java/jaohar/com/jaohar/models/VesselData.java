package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VesselData {

    @SerializedName("record_id")
    @Expose
    private String recordId;
    @SerializedName("IMO_number")
    @Expose
    private String iMONumber;
    @SerializedName("owner_detail")
    @Expose
    private String ownerDetail;
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("vessel_name")
    @Expose
    private String vesselName;
    @SerializedName("vessel_type")
    @Expose
    private String vesselType;
    @SerializedName("year_built")
    @Expose
    private String yearBuilt;
    @SerializedName("place_of_built")
    @Expose
    private String placeOfBuilt;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("capacity")
    @Expose
    private String capacity;
    @SerializedName("loa")
    @Expose
    private String loa;
    @SerializedName("breadth")
    @Expose
    private String breadth;
    @SerializedName("depth")
    @Expose
    private String depth;
    @SerializedName("geared")
    @Expose
    private String geared;
    @SerializedName("price_idea")
    @Expose
    private String priceIdea;
    @SerializedName("vessel_location")
    @Expose
    private String vesselLocation;
    @SerializedName("full_description")
    @Expose
    private String fullDescription;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("teu")
    @Expose
    private String teu;
    @SerializedName("no_passengers")
    @Expose
    private String noPassengers;
    @SerializedName("liquid")
    @Expose
    private String liquid;
    @SerializedName("draft")
    @Expose
    private String draft;
    @SerializedName("builder")
    @Expose
    private String builder;
    @SerializedName("gross_tonnage")
    @Expose
    private String grossTonnage;
    @SerializedName("net_tonnage")
    @Expose
    private String netTonnage;
    @SerializedName("gas")
    @Expose
    private String gas;
    @SerializedName("lenght_overall")
    @Expose
    private String lenghtOverall;
    @SerializedName("draught")
    @Expose
    private String draught;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("machinery_detail")
    @Expose
    private String machineryDetail;
    @SerializedName("ldt")
    @Expose
    private String ldt;
    @SerializedName("tcm")
    @Expose
    private String tcm;
    @SerializedName("photo1")
    @Expose
    private String photo1;
    @SerializedName("photo2")
    @Expose
    private String photo2;
    @SerializedName("photo3")
    @Expose
    private String photo3;
    @SerializedName("photo4")
    @Expose
    private String photo4;
    @SerializedName("photo5")
    @Expose
    private String photo5;
    @SerializedName("photo6")
    @Expose
    private String photo6;
    @SerializedName("photo7")
    @Expose
    private String photo7;
    @SerializedName("photo8")
    @Expose
    private String photo8;
    @SerializedName("photo9")
    @Expose
    private String photo9;
    @SerializedName("photo10")
    @Expose
    private String photo10;
    @SerializedName("photo11")
    @Expose
    private String photo11;
    @SerializedName("photo12")
    @Expose
    private String photo12;
    @SerializedName("photo13")
    @Expose
    private String photo13;
    @SerializedName("photo14")
    @Expose
    private String photo14;
    @SerializedName("photo15")
    @Expose
    private String photo15;
    @SerializedName("photo16")
    @Expose
    private String photo16;
    @SerializedName("photo17")
    @Expose
    private String photo17;
    @SerializedName("photo18")
    @Expose
    private String photo18;
    @SerializedName("photo19")
    @Expose
    private String photo19;
    @SerializedName("photo20")
    @Expose
    private String photo20;
    @SerializedName("photo21")
    @Expose
    private String photo21;
    @SerializedName("photo22")
    @Expose
    private String photo22;
    @SerializedName("photo23")
    @Expose
    private String photo23;
    @SerializedName("photo24")
    @Expose
    private String photo24;
    @SerializedName("photo25")
    @Expose
    private String photo25;
    @SerializedName("photo26")
    @Expose
    private String photo26;
    @SerializedName("photo27")
    @Expose
    private String photo27;
    @SerializedName("photo28")
    @Expose
    private String photo28;
    @SerializedName("photo29")
    @Expose
    private String photo29;
    @SerializedName("photo30")
    @Expose
    private String photo30;
    @SerializedName("photo31")
    @Expose
    private String photo31;
    @SerializedName("photo32")
    @Expose
    private String photo32;
    @SerializedName("photo33")
    @Expose
    private String photo33;
    @SerializedName("photo34")
    @Expose
    private String photo34;
    @SerializedName("photo35")
    @Expose
    private String photo35;
    @SerializedName("photo36")
    @Expose
    private String photo36;
    @SerializedName("photo37")
    @Expose
    private String photo37;
    @SerializedName("photo38")
    @Expose
    private String photo38;
    @SerializedName("photo39")
    @Expose
    private String photo39;
    @SerializedName("photo40")
    @Expose
    private String photo40;
    @SerializedName("photo41")
    @Expose
    private String photo41;
    @SerializedName("photo42")
    @Expose
    private String photo42;
    @SerializedName("photo43")
    @Expose
    private String photo43;
    @SerializedName("photo44")
    @Expose
    private String photo44;
    @SerializedName("photo45")
    @Expose
    private String photo45;
    @SerializedName("photo46")
    @Expose
    private String photo46;
    @SerializedName("photo47")
    @Expose
    private String photo47;
    @SerializedName("photo48")
    @Expose
    private String photo48;
    @SerializedName("photo49")
    @Expose
    private String photo49;
    @SerializedName("photo50")
    @Expose
    private String photo50;
    @SerializedName("date_entered")
    @Expose
    private String dateEntered;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("bale")
    @Expose
    private String bale;
    @SerializedName("grain")
    @Expose
    private String grain;
    @SerializedName("date_for_vessel")
    @Expose
    private String dateForVessel;
    @SerializedName("vessel_added_by")
    @Expose
    private String vesselAddedBy;
    @SerializedName("document1")
    @Expose
    private String document1;
    @SerializedName("document2")
    @Expose
    private String document2;
    @SerializedName("document3")
    @Expose
    private String document3;
    @SerializedName("document4")
    @Expose
    private String document4;
    @SerializedName("document5")
    @Expose
    private String document5;
    @SerializedName("document6")
    @Expose
    private String document6;
    @SerializedName("document7")
    @Expose
    private String document7;
    @SerializedName("document8")
    @Expose
    private String document8;
    @SerializedName("document9")
    @Expose
    private String document9;
    @SerializedName("document10")
    @Expose
    private String document10;
    @SerializedName("document11")
    @Expose
    private String document11;
    @SerializedName("document12")
    @Expose
    private String document12;
    @SerializedName("document13")
    @Expose
    private String document13;
    @SerializedName("document14")
    @Expose
    private String document14;
    @SerializedName("document15")
    @Expose
    private String document15;
    @SerializedName("document16")
    @Expose
    private String document16;
    @SerializedName("document17")
    @Expose
    private String document17;
    @SerializedName("document18")
    @Expose
    private String document18;
    @SerializedName("document19")
    @Expose
    private String document19;
    @SerializedName("document20")
    @Expose
    private String document20;
    @SerializedName("document21")
    @Expose
    private String document21;
    @SerializedName("document22")
    @Expose
    private String document22;
    @SerializedName("document23")
    @Expose
    private String document23;
    @SerializedName("document24")
    @Expose
    private String document24;
    @SerializedName("document25")
    @Expose
    private String document25;
    @SerializedName("document26")
    @Expose
    private String document26;
    @SerializedName("document27")
    @Expose
    private String document27;
    @SerializedName("document28")
    @Expose
    private String document28;
    @SerializedName("document29")
    @Expose
    private String document29;
    @SerializedName("document30")
    @Expose
    private String document30;
    @SerializedName("vessel_add_time")
    @Expose
    private String vesselAddTime;
    @SerializedName("modified_by")
    @Expose
    private String modifiedBy;
    @SerializedName("modification_date")
    @Expose
    private String modificationDate;
    @SerializedName("admin_remark")
    @Expose
    private String adminRemark;
    @SerializedName("enable")
    @Expose
    private String enable;
    @SerializedName("mmsi_no")
    @Expose
    private String mmsiNo;
    @SerializedName("off_market")
    @Expose
    private String offMarket;
    @SerializedName("document1name")
    @Expose
    private String document1name;
    @SerializedName("remarks")
    @Expose
    private List<Remark> remarks = null;
    @SerializedName("document2name")
    @Expose
    private String document2name;
    @SerializedName("document3name")
    @Expose
    private String document3name;
    @SerializedName("document4name")
    @Expose
    private String document4name;
    @SerializedName("document5name")
    @Expose
    private String document5name;
    @SerializedName("document6name")
    @Expose
    private String document6name;
    @SerializedName("document7name")
    @Expose
    private String document7name;
    @SerializedName("document8name")
    @Expose
    private String document8name;
    @SerializedName("document9name")
    @Expose
    private String document9name;
    @SerializedName("document10name")
    @Expose
    private String document10name;
    @SerializedName("document11name")
    @Expose
    private String document11name;
    @SerializedName("document12name")
    @Expose
    private String document12name;
    @SerializedName("document13name")
    @Expose
    private String document13name;
    @SerializedName("document14name")
    @Expose
    private String document14name;
    @SerializedName("document15name")
    @Expose
    private String document15name;
    @SerializedName("document16name")
    @Expose
    private String document16name;
    @SerializedName("document17name")
    @Expose
    private String document17name;
    @SerializedName("document18name")
    @Expose
    private String document18name;
    @SerializedName("document19name")
    @Expose
    private String document19name;
    @SerializedName("document20name")
    @Expose
    private String document20name;
    @SerializedName("document21name")
    @Expose
    private String document21name;
    @SerializedName("document22name")
    @Expose
    private String document22name;
    @SerializedName("document23name")
    @Expose
    private String document23name;
    @SerializedName("document24name")
    @Expose
    private String document24name;
    @SerializedName("document25name")
    @Expose
    private String document25name;
    @SerializedName("document26name")
    @Expose
    private String document26name;
    @SerializedName("document27name")
    @Expose
    private String document27name;
    @SerializedName("document28name")
    @Expose
    private String document28name;
    @SerializedName("document29name")
    @Expose
    private String document29name;
    @SerializedName("document30name")
    @Expose
    private String document30name;
    @SerializedName("desc")
    @Expose
    private String desc;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getIMONumber() {
        return iMONumber;
    }

    public void setIMONumber(String iMONumber) {
        this.iMONumber = iMONumber;
    }

    public String getOwnerDetail() {
        return ownerDetail;
    }

    public void setOwnerDetail(String ownerDetail) {
        this.ownerDetail = ownerDetail;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getVesselType() {
        return vesselType;
    }

    public void setVesselType(String vesselType) {
        this.vesselType = vesselType;
    }

    public String getYearBuilt() {
        return yearBuilt;
    }

    public void setYearBuilt(String yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    public String getPlaceOfBuilt() {
        return placeOfBuilt;
    }

    public void setPlaceOfBuilt(String placeOfBuilt) {
        this.placeOfBuilt = placeOfBuilt;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getLoa() {
        return loa;
    }

    public void setLoa(String loa) {
        this.loa = loa;
    }

    public String getBreadth() {
        return breadth;
    }

    public void setBreadth(String breadth) {
        this.breadth = breadth;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getGeared() {
        return geared;
    }

    public void setGeared(String geared) {
        this.geared = geared;
    }

    public String getPriceIdea() {
        return priceIdea;
    }

    public void setPriceIdea(String priceIdea) {
        this.priceIdea = priceIdea;
    }

    public String getVesselLocation() {
        return vesselLocation;
    }

    public void setVesselLocation(String vesselLocation) {
        this.vesselLocation = vesselLocation;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTeu() {
        return teu;
    }

    public void setTeu(String teu) {
        this.teu = teu;
    }

    public String getNoPassengers() {
        return noPassengers;
    }

    public void setNoPassengers(String noPassengers) {
        this.noPassengers = noPassengers;
    }

    public String getLiquid() {
        return liquid;
    }

    public void setLiquid(String liquid) {
        this.liquid = liquid;
    }

    public String getDraft() {
        return draft;
    }

    public void setDraft(String draft) {
        this.draft = draft;
    }

    public String getBuilder() {
        return builder;
    }

    public void setBuilder(String builder) {
        this.builder = builder;
    }

    public String getGrossTonnage() {
        return grossTonnage;
    }

    public void setGrossTonnage(String grossTonnage) {
        this.grossTonnage = grossTonnage;
    }

    public String getNetTonnage() {
        return netTonnage;
    }

    public void setNetTonnage(String netTonnage) {
        this.netTonnage = netTonnage;
    }

    public String getGas() {
        return gas;
    }

    public void setGas(String gas) {
        this.gas = gas;
    }

    public String getLenghtOverall() {
        return lenghtOverall;
    }

    public void setLenghtOverall(String lenghtOverall) {
        this.lenghtOverall = lenghtOverall;
    }

    public String getDraught() {
        return draught;
    }

    public void setDraught(String draught) {
        this.draught = draught;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMachineryDetail() {
        return machineryDetail;
    }

    public void setMachineryDetail(String machineryDetail) {
        this.machineryDetail = machineryDetail;
    }

    public String getLdt() {
        return ldt;
    }

    public void setLdt(String ldt) {
        this.ldt = ldt;
    }

    public String getTcm() {
        return tcm;
    }

    public void setTcm(String tcm) {
        this.tcm = tcm;
    }

    public String getPhoto1() {
        return photo1;
    }

    public void setPhoto1(String photo1) {
        this.photo1 = photo1;
    }

    public String getPhoto2() {
        return photo2;
    }

    public void setPhoto2(String photo2) {
        this.photo2 = photo2;
    }

    public String getPhoto3() {
        return photo3;
    }

    public void setPhoto3(String photo3) {
        this.photo3 = photo3;
    }

    public String getPhoto4() {
        return photo4;
    }

    public void setPhoto4(String photo4) {
        this.photo4 = photo4;
    }

    public String getPhoto5() {
        return photo5;
    }

    public void setPhoto5(String photo5) {
        this.photo5 = photo5;
    }

    public String getPhoto6() {
        return photo6;
    }

    public void setPhoto6(String photo6) {
        this.photo6 = photo6;
    }

    public String getPhoto7() {
        return photo7;
    }

    public void setPhoto7(String photo7) {
        this.photo7 = photo7;
    }

    public String getPhoto8() {
        return photo8;
    }

    public void setPhoto8(String photo8) {
        this.photo8 = photo8;
    }

    public String getPhoto9() {
        return photo9;
    }

    public void setPhoto9(String photo9) {
        this.photo9 = photo9;
    }

    public String getPhoto10() {
        return photo10;
    }

    public void setPhoto10(String photo10) {
        this.photo10 = photo10;
    }

    public String getPhoto11() {
        return photo11;
    }

    public void setPhoto11(String photo11) {
        this.photo11 = photo11;
    }

    public String getPhoto12() {
        return photo12;
    }

    public void setPhoto12(String photo12) {
        this.photo12 = photo12;
    }

    public String getPhoto13() {
        return photo13;
    }

    public void setPhoto13(String photo13) {
        this.photo13 = photo13;
    }

    public String getPhoto14() {
        return photo14;
    }

    public void setPhoto14(String photo14) {
        this.photo14 = photo14;
    }

    public String getPhoto15() {
        return photo15;
    }

    public void setPhoto15(String photo15) {
        this.photo15 = photo15;
    }

    public String getPhoto16() {
        return photo16;
    }

    public void setPhoto16(String photo16) {
        this.photo16 = photo16;
    }

    public String getPhoto17() {
        return photo17;
    }

    public void setPhoto17(String photo17) {
        this.photo17 = photo17;
    }

    public String getPhoto18() {
        return photo18;
    }

    public void setPhoto18(String photo18) {
        this.photo18 = photo18;
    }

    public String getPhoto19() {
        return photo19;
    }

    public void setPhoto19(String photo19) {
        this.photo19 = photo19;
    }

    public String getPhoto20() {
        return photo20;
    }

    public void setPhoto20(String photo20) {
        this.photo20 = photo20;
    }

    public String getPhoto21() {
        return photo21;
    }

    public void setPhoto21(String photo21) {
        this.photo21 = photo21;
    }

    public String getPhoto22() {
        return photo22;
    }

    public void setPhoto22(String photo22) {
        this.photo22 = photo22;
    }

    public String getPhoto23() {
        return photo23;
    }

    public void setPhoto23(String photo23) {
        this.photo23 = photo23;
    }

    public String getPhoto24() {
        return photo24;
    }

    public void setPhoto24(String photo24) {
        this.photo24 = photo24;
    }

    public String getPhoto25() {
        return photo25;
    }

    public void setPhoto25(String photo25) {
        this.photo25 = photo25;
    }

    public String getPhoto26() {
        return photo26;
    }

    public void setPhoto26(String photo26) {
        this.photo26 = photo26;
    }

    public String getPhoto27() {
        return photo27;
    }

    public void setPhoto27(String photo27) {
        this.photo27 = photo27;
    }

    public String getPhoto28() {
        return photo28;
    }

    public void setPhoto28(String photo28) {
        this.photo28 = photo28;
    }

    public String getPhoto29() {
        return photo29;
    }

    public void setPhoto29(String photo29) {
        this.photo29 = photo29;
    }

    public String getPhoto30() {
        return photo30;
    }

    public void setPhoto30(String photo30) {
        this.photo30 = photo30;
    }

    public String getPhoto31() {
        return photo31;
    }

    public void setPhoto31(String photo31) {
        this.photo31 = photo31;
    }

    public String getPhoto32() {
        return photo32;
    }

    public void setPhoto32(String photo32) {
        this.photo32 = photo32;
    }

    public String getPhoto33() {
        return photo33;
    }

    public void setPhoto33(String photo33) {
        this.photo33 = photo33;
    }

    public String getPhoto34() {
        return photo34;
    }

    public void setPhoto34(String photo34) {
        this.photo34 = photo34;
    }

    public String getPhoto35() {
        return photo35;
    }

    public void setPhoto35(String photo35) {
        this.photo35 = photo35;
    }

    public String getPhoto36() {
        return photo36;
    }

    public void setPhoto36(String photo36) {
        this.photo36 = photo36;
    }

    public String getPhoto37() {
        return photo37;
    }

    public void setPhoto37(String photo37) {
        this.photo37 = photo37;
    }

    public String getPhoto38() {
        return photo38;
    }

    public void setPhoto38(String photo38) {
        this.photo38 = photo38;
    }

    public String getPhoto39() {
        return photo39;
    }

    public void setPhoto39(String photo39) {
        this.photo39 = photo39;
    }

    public String getPhoto40() {
        return photo40;
    }

    public void setPhoto40(String photo40) {
        this.photo40 = photo40;
    }

    public String getPhoto41() {
        return photo41;
    }

    public void setPhoto41(String photo41) {
        this.photo41 = photo41;
    }

    public String getPhoto42() {
        return photo42;
    }

    public void setPhoto42(String photo42) {
        this.photo42 = photo42;
    }

    public String getPhoto43() {
        return photo43;
    }

    public void setPhoto43(String photo43) {
        this.photo43 = photo43;
    }

    public String getPhoto44() {
        return photo44;
    }

    public void setPhoto44(String photo44) {
        this.photo44 = photo44;
    }

    public String getPhoto45() {
        return photo45;
    }

    public void setPhoto45(String photo45) {
        this.photo45 = photo45;
    }

    public String getPhoto46() {
        return photo46;
    }

    public void setPhoto46(String photo46) {
        this.photo46 = photo46;
    }

    public String getPhoto47() {
        return photo47;
    }

    public void setPhoto47(String photo47) {
        this.photo47 = photo47;
    }

    public String getPhoto48() {
        return photo48;
    }

    public void setPhoto48(String photo48) {
        this.photo48 = photo48;
    }

    public String getPhoto49() {
        return photo49;
    }

    public void setPhoto49(String photo49) {
        this.photo49 = photo49;
    }

    public String getPhoto50() {
        return photo50;
    }

    public void setPhoto50(String photo50) {
        this.photo50 = photo50;
    }

    public String getDateEntered() {
        return dateEntered;
    }

    public void setDateEntered(String dateEntered) {
        this.dateEntered = dateEntered;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBale() {
        return bale;
    }

    public void setBale(String bale) {
        this.bale = bale;
    }

    public String getGrain() {
        return grain;
    }

    public void setGrain(String grain) {
        this.grain = grain;
    }

    public String getDateForVessel() {
        return dateForVessel;
    }

    public void setDateForVessel(String dateForVessel) {
        this.dateForVessel = dateForVessel;
    }

    public String getVesselAddedBy() {
        return vesselAddedBy;
    }

    public void setVesselAddedBy(String vesselAddedBy) {
        this.vesselAddedBy = vesselAddedBy;
    }

    public String getDocument1() {
        return document1;
    }

    public void setDocument1(String document1) {
        this.document1 = document1;
    }

    public String getDocument2() {
        return document2;
    }

    public void setDocument2(String document2) {
        this.document2 = document2;
    }

    public String getDocument3() {
        return document3;
    }

    public void setDocument3(String document3) {
        this.document3 = document3;
    }

    public String getDocument4() {
        return document4;
    }

    public void setDocument4(String document4) {
        this.document4 = document4;
    }

    public String getDocument5() {
        return document5;
    }

    public void setDocument5(String document5) {
        this.document5 = document5;
    }

    public String getDocument6() {
        return document6;
    }

    public void setDocument6(String document6) {
        this.document6 = document6;
    }

    public String getDocument7() {
        return document7;
    }

    public void setDocument7(String document7) {
        this.document7 = document7;
    }

    public String getDocument8() {
        return document8;
    }

    public void setDocument8(String document8) {
        this.document8 = document8;
    }

    public String getDocument9() {
        return document9;
    }

    public void setDocument9(String document9) {
        this.document9 = document9;
    }

    public String getDocument10() {
        return document10;
    }

    public void setDocument10(String document10) {
        this.document10 = document10;
    }

    public String getDocument11() {
        return document11;
    }

    public void setDocument11(String document11) {
        this.document11 = document11;
    }

    public String getDocument12() {
        return document12;
    }

    public void setDocument12(String document12) {
        this.document12 = document12;
    }

    public String getDocument13() {
        return document13;
    }

    public void setDocument13(String document13) {
        this.document13 = document13;
    }

    public String getDocument14() {
        return document14;
    }

    public void setDocument14(String document14) {
        this.document14 = document14;
    }

    public String getDocument15() {
        return document15;
    }

    public void setDocument15(String document15) {
        this.document15 = document15;
    }

    public String getDocument16() {
        return document16;
    }

    public void setDocument16(String document16) {
        this.document16 = document16;
    }

    public String getDocument17() {
        return document17;
    }

    public void setDocument17(String document17) {
        this.document17 = document17;
    }

    public String getDocument18() {
        return document18;
    }

    public void setDocument18(String document18) {
        this.document18 = document18;
    }

    public String getDocument19() {
        return document19;
    }

    public void setDocument19(String document19) {
        this.document19 = document19;
    }

    public String getDocument20() {
        return document20;
    }

    public void setDocument20(String document20) {
        this.document20 = document20;
    }

    public String getDocument21() {
        return document21;
    }

    public void setDocument21(String document21) {
        this.document21 = document21;
    }

    public String getDocument22() {
        return document22;
    }

    public void setDocument22(String document22) {
        this.document22 = document22;
    }

    public String getDocument23() {
        return document23;
    }

    public void setDocument23(String document23) {
        this.document23 = document23;
    }

    public String getDocument24() {
        return document24;
    }

    public void setDocument24(String document24) {
        this.document24 = document24;
    }

    public String getDocument25() {
        return document25;
    }

    public void setDocument25(String document25) {
        this.document25 = document25;
    }

    public String getDocument26() {
        return document26;
    }

    public void setDocument26(String document26) {
        this.document26 = document26;
    }

    public String getDocument27() {
        return document27;
    }

    public void setDocument27(String document27) {
        this.document27 = document27;
    }

    public String getDocument28() {
        return document28;
    }

    public void setDocument28(String document28) {
        this.document28 = document28;
    }

    public String getDocument29() {
        return document29;
    }

    public void setDocument29(String document29) {
        this.document29 = document29;
    }

    public String getDocument30() {
        return document30;
    }

    public void setDocument30(String document30) {
        this.document30 = document30;
    }

    public String getVesselAddTime() {
        return vesselAddTime;
    }

    public void setVesselAddTime(String vesselAddTime) {
        this.vesselAddTime = vesselAddTime;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getAdminRemark() {
        return adminRemark;
    }

    public void setAdminRemark(String adminRemark) {
        this.adminRemark = adminRemark;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getMmsiNo() {
        return mmsiNo;
    }

    public void setMmsiNo(String mmsiNo) {
        this.mmsiNo = mmsiNo;
    }

    public String getOffMarket() {
        return offMarket;
    }

    public void setOffMarket(String offMarket) {
        this.offMarket = offMarket;
    }

    public String getDocument1name() {
        return document1name;
    }

    public void setDocument1name(String document1name) {
        this.document1name = document1name;
    }

    public List<Remark> getRemarks() {
        return remarks;
    }

    public void setRemarks(List<Remark> remarks) {
        this.remarks = remarks;
    }

    public String getDocument2name() {
        return document2name;
    }

    public void setDocument2name(String document2name) {
        this.document2name = document2name;
    }

    public String getDocument3name() {
        return document3name;
    }

    public void setDocument3name(String document3name) {
        this.document3name = document3name;
    }

    public String getDocument4name() {
        return document4name;
    }

    public void setDocument4name(String document4name) {
        this.document4name = document4name;
    }

    public String getDocument5name() {
        return document5name;
    }

    public void setDocument5name(String document5name) {
        this.document5name = document5name;
    }

    public String getDocument6name() {
        return document6name;
    }

    public void setDocument6name(String document6name) {
        this.document6name = document6name;
    }

    public String getDocument7name() {
        return document7name;
    }

    public void setDocument7name(String document7name) {
        this.document7name = document7name;
    }

    public String getDocument8name() {
        return document8name;
    }

    public void setDocument8name(String document8name) {
        this.document8name = document8name;
    }

    public String getDocument9name() {
        return document9name;
    }

    public void setDocument9name(String document9name) {
        this.document9name = document9name;
    }

    public String getDocument10name() {
        return document10name;
    }

    public void setDocument10name(String document10name) {
        this.document10name = document10name;
    }

    public String getDocument11name() {
        return document11name;
    }

    public void setDocument11name(String document11name) {
        this.document11name = document11name;
    }

    public String getDocument12name() {
        return document12name;
    }

    public void setDocument12name(String document12name) {
        this.document12name = document12name;
    }

    public String getDocument13name() {
        return document13name;
    }

    public void setDocument13name(String document13name) {
        this.document13name = document13name;
    }

    public String getDocument14name() {
        return document14name;
    }

    public void setDocument14name(String document14name) {
        this.document14name = document14name;
    }

    public String getDocument15name() {
        return document15name;
    }

    public void setDocument15name(String document15name) {
        this.document15name = document15name;
    }

    public String getDocument16name() {
        return document16name;
    }

    public void setDocument16name(String document16name) {
        this.document16name = document16name;
    }

    public String getDocument17name() {
        return document17name;
    }

    public void setDocument17name(String document17name) {
        this.document17name = document17name;
    }

    public String getDocument18name() {
        return document18name;
    }

    public void setDocument18name(String document18name) {
        this.document18name = document18name;
    }

    public String getDocument19name() {
        return document19name;
    }

    public void setDocument19name(String document19name) {
        this.document19name = document19name;
    }

    public String getDocument20name() {
        return document20name;
    }

    public void setDocument20name(String document20name) {
        this.document20name = document20name;
    }

    public String getDocument21name() {
        return document21name;
    }

    public void setDocument21name(String document21name) {
        this.document21name = document21name;
    }

    public String getDocument22name() {
        return document22name;
    }

    public void setDocument22name(String document22name) {
        this.document22name = document22name;
    }

    public String getDocument23name() {
        return document23name;
    }

    public void setDocument23name(String document23name) {
        this.document23name = document23name;
    }

    public String getDocument24name() {
        return document24name;
    }

    public void setDocument24name(String document24name) {
        this.document24name = document24name;
    }

    public String getDocument25name() {
        return document25name;
    }

    public void setDocument25name(String document25name) {
        this.document25name = document25name;
    }

    public String getDocument26name() {
        return document26name;
    }

    public void setDocument26name(String document26name) {
        this.document26name = document26name;
    }

    public String getDocument27name() {
        return document27name;
    }

    public void setDocument27name(String document27name) {
        this.document27name = document27name;
    }

    public String getDocument28name() {
        return document28name;
    }

    public void setDocument28name(String document28name) {
        this.document28name = document28name;
    }

    public String getDocument29name() {
        return document29name;
    }

    public void setDocument29name(String document29name) {
        this.document29name = document29name;
    }

    public String getDocument30name() {
        return document30name;
    }

    public void setDocument30name(String document30name) {
        this.document30name = document30name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
