package jaohar.com.jaohar.models;
import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;


        public class AllMailListContactsModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private AllMailListData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AllMailListData getData() {
        return data;
    }

    public void setData(AllMailListData data) {
        this.data = data;
    }

}
