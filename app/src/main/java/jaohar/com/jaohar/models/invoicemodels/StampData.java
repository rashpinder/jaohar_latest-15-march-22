package jaohar.com.jaohar.models.invoicemodels;

import com.google.gson.annotations.SerializedName;

public class StampData{

	@SerializedName("stamp_name")
	private String stampName;

	@SerializedName("stamp_id")
	private String stampId;

	@SerializedName("added_by")
	private String addedBy;

	@SerializedName("enable")
	private String enable;

	@SerializedName("modification_date")
	private String modificationDate;

	@SerializedName("stamp_image")
	private String stampImage;

	public void setStampName(String stampName){
		this.stampName = stampName;
	}

	public String getStampName(){
		return stampName;
	}

	public void setStampId(String stampId){
		this.stampId = stampId;
	}

	public String getStampId(){
		return stampId;
	}

	public void setAddedBy(String addedBy){
		this.addedBy = addedBy;
	}

	public String getAddedBy(){
		return addedBy;
	}

	public void setEnable(String enable){
		this.enable = enable;
	}

	public String getEnable(){
		return enable;
	}

	public void setModificationDate(String modificationDate){
		this.modificationDate = modificationDate;
	}

	public String getModificationDate(){
		return modificationDate;
	}

	public void setStampImage(String stampImage){
		this.stampImage = stampImage;
	}

	public String getStampImage(){
		return stampImage;
	}
}