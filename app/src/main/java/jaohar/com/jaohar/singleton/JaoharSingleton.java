package jaohar.com.jaohar.singleton;

import java.util.ArrayList;

import jaohar.com.jaohar.beans.AdminRoleModel;
import jaohar.com.jaohar.beans.DocumentModel;
import jaohar.com.jaohar.beans.ForumModule.ForumChatModel;
import jaohar.com.jaohar.beans.ForumModule.ForumUserTypeModel;
import jaohar.com.jaohar.beans.NewRequestModel;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.models.NewReqData;

/**
 * Created by Dharmani Apps on 11/9/2017.
 */

public class JaoharSingleton {

    String ForumType = "";

    private static final JaoharSingleton ourInstance = new JaoharSingleton();

    String SearchedVessel ="";
    String SearchedEducationalBlog ="";
    AllVessel mAllVessel = new AllVessel();
    DocumentModel mAllDoc = new DocumentModel();
    private ArrayList<AllVessel> vessaelesArrayList = new ArrayList<AllVessel>();
    private ArrayList<AdminRoleModel> adminRoleArrayList = new ArrayList<AdminRoleModel>();
    private ArrayList<NewReqData> newRequestArrayList = new ArrayList<NewReqData>();
    private ArrayList<VesselSearchInvoiceModel> vesselSearchArrayList = new ArrayList<VesselSearchInvoiceModel>();
    private ArrayList<ForumChatModel> mModelArrayList = new ArrayList<ForumChatModel>();
    ArrayList<DocumentModel> documentArrayList = new ArrayList<DocumentModel>();
    private ArrayList<ForumUserTypeModel> mUsersArrayList = new ArrayList<ForumUserTypeModel>();
    private ArrayList<String> mForumUsersList = new ArrayList<String>();
    private ArrayList<ChatUsersModel> mSelectedGroupUsersList = new ArrayList<ChatUsersModel>();
    private ArrayList<VesselesModel> SearchedVesselsModel = new ArrayList<VesselesModel>();
    private ArrayList<VesselesModel> AdvancedSearchedVesselsModel = new ArrayList<VesselesModel>();

    private JaoharSingleton() {
    }

    public AllVessel getmAllVessel() {
        return mAllVessel;
    }

    public void setmAllVessel(AllVessel mAllVessel) {
        this.mAllVessel = mAllVessel;
    }
  public void setmAllDocument(ArrayList<DocumentModel> documentArrayList) {
        this.documentArrayList = documentArrayList;
    }

    public ArrayList<DocumentModel> getDocumentArrayList(){
        return documentArrayList;
    }

    public ArrayList<ForumChatModel> getmModelArrayList() {
        return mModelArrayList;
    }

    public void setmModelArrayList(ArrayList<ForumChatModel> mModelArrayList) {
        this.mModelArrayList = mModelArrayList;
    }

    public static JaoharSingleton getInstance() {
        return ourInstance;
    }

    public ArrayList<NewReqData> getNewRequestArrayList() {
        return newRequestArrayList;
    }

    public ArrayList<AdminRoleModel> getAdminRoleArrayList() {
        return adminRoleArrayList;
    }

    public ArrayList<VesselSearchInvoiceModel> getVesselSearchArrayList() {
        return vesselSearchArrayList;
    }

    public ArrayList<AllVessel> getVessaelesArrayList() {
        return vessaelesArrayList;
    }

    public ArrayList<ForumUserTypeModel> getmUsersArrayList() {
        return mUsersArrayList;
    }

    public void setmUsersArrayList(ArrayList<ForumUserTypeModel> mUsersArrayList) {
        this.mUsersArrayList = mUsersArrayList;
    }

    public ArrayList<String> getmForumUsersList() {
        return mForumUsersList;
    }

    public void setmForumUsersList(ArrayList<String> mForumUsersList) {
        this.mForumUsersList = mForumUsersList;
    }

    public String getForumType() {
        return ForumType;
    }

    public void setForumType(String forumType) {
        ForumType = forumType;
    }

    public String getSearchedEducationalBlog() {
        return SearchedEducationalBlog;
    }

    public void setSearchedEducationalBlog(String searchedEducationalBlog) {
        SearchedEducationalBlog = searchedEducationalBlog;
    }

    public String getSearchedVessel() {
        return SearchedVessel;
    }

    public void setSearchedVessel(String searchedVessel) {
        SearchedVessel = searchedVessel;
    }

    public ArrayList<VesselesModel> getAdvancedSearchedVesselsModel() {
        return AdvancedSearchedVesselsModel;
    }

    public void setAdvancedSearchedVesselsModel(ArrayList<VesselesModel> advancedSearchedVesselsModel) {
        AdvancedSearchedVesselsModel = advancedSearchedVesselsModel;
    }

    public ArrayList<VesselesModel> getSearchedVesselsModel() {
        return SearchedVesselsModel;
    }

    public void setSearchedVesselsModel(ArrayList<VesselesModel> searchedVesselsModel) {
        SearchedVesselsModel = searchedVesselsModel;
    }

    public ArrayList<ChatUsersModel> getmSelectedGroupUsersList() {
        return mSelectedGroupUsersList;
    }

    public void setmSelectedGroupUsersList(ArrayList<ChatUsersModel> mSelectedGroupUsersList) {
        this.mSelectedGroupUsersList = mSelectedGroupUsersList;
    }
}
