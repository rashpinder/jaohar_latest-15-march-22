package jaohar.com.jaohar.interfaces.invoice_module;

import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.models.invoicedraftmodels.AllInvoicesItem;

public interface EditInvoiceDraftInterface {
    void mEditInvoiceDraft(InVoicesModel mModel);
}
