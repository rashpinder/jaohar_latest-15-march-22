package jaohar.com.jaohar.interfaces;


import jaohar.com.jaohar.models.StatusData;
import jaohar.com.jaohar.models.StatusModel;

/**
 * Created by Dharmani Apps on 3/13/2018.
 */

public interface EditStatusInterface {
    public void mEditStatusInterface(StatusData mStatusModel, int position);
}
