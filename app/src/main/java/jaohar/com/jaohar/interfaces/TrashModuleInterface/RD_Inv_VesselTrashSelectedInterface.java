package jaohar.com.jaohar.interfaces.TrashModuleInterface;

import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.models.SearchVesselData;

public interface RD_Inv_VesselTrashSelectedInterface {
//    void mRD_Inv_VesselTrashInterFace(VesselSearchInvoiceModel mModel , boolean isSelected);
    void mRD_Inv_VesselTrashInterFace(SearchVesselData mModel , boolean isSelected);
}
