package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.AddGalleryImagesModel;

/**
 * Created by dharmaniz on 2/7/18.
 */

public interface DeleteImagesVessels {
    public  void deleteImagesVessels(AddGalleryImagesModel mGalleryModel,int position);
}
