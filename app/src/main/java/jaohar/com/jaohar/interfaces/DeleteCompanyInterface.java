package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.CompaniesModel;

/**
 * Created by Dharmani Apps on 1/19/2018.
 */

public interface DeleteCompanyInterface {
    public void deleteCompany(CompaniesModel mCompaniesModel, int position);
}
