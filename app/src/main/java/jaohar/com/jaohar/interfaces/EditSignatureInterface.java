package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.SignatureModel;

/**
 * Created by Dharmani Apps on 1/19/2018.
 */

public interface EditSignatureInterface {
    public void editSignature(SignatureModel mSignatureModel);
}
