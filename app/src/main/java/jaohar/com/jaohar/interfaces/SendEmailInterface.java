package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.models.AllVessel;

/**
 * Created by Dharmani Apps on 12/29/2017.
 */

public interface SendEmailInterface {
//    public void sendEmail(VesselesModel mVesselesModel);
    public void sendEmail(AllVessel mVesselesModel);
}
