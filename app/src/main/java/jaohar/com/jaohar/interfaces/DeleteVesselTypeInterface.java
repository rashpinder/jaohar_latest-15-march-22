package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.VesselTypesModel;
import jaohar.com.jaohar.models.GetVesselTypeData;

/**
 * Created by Dharmani Apps on 3/13/2018.
 */

public interface DeleteVesselTypeInterface {
    public void mDeleteVesselTypeInterface(GetVesselTypeData mVesselTypesModel);
}
