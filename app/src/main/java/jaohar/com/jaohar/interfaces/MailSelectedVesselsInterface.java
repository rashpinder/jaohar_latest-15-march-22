package jaohar.com.jaohar.interfaces;

import java.util.ArrayList;

import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.models.AllVessel;

public interface MailSelectedVesselsInterface {
//    public void MailSelectedVessels(int position, String Vessel_id, ArrayList<String> Items,
//                                    VesselesModel mVesselesModel, boolean b);
    public void MailSelectedVessels(int position, String Vessel_id, ArrayList<String> Items,
                                    AllVessel mVesselesModel, boolean b);
}
