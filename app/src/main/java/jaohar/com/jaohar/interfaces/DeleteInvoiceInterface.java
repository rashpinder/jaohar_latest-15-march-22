package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.models.AllInvoiceModel;

/**
 * Created by Dharmani Apps on 1/30/2018.
 */

public interface DeleteInvoiceInterface {
    public void deleteInvoice(InVoicesModel mInVoicesModel);

}
