package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.CompaniesModel;

/**
 * Created by Dharmani Apps on 1/19/2018.
 */

public interface EditCompanyInterface {
    public void editCompany(CompaniesModel mCompaniesModel);
}
