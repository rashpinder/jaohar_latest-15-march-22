package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.AddTotalInvoiceDiscountModel;

/**
 * Created by dharmaniz on 25/5/18.
 */

public interface EditTotalDiscountItemdata {
    public void editTotalDiscountItemdata(AddTotalInvoiceDiscountModel mModel, int position,String strType);
}
