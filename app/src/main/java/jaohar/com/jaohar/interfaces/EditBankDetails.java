package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.models.BankData;

/**
 * Created by Dharmani Apps on 1/20/2018.
 */

public interface EditBankDetails {
    public void editBankDetails(BankModel mBankModel);
//    public void editBankDetails(BankData mBankModel);
}
