package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.MailContactListModel;
import jaohar.com.jaohar.models.AllMailListContact;

/**
 * Created by dharmaniz on 16/1/19.
 */

public interface EditContactsMailListInterface {
//    public  void editContactsMailListInterface(MailContactListModel mModel);
    public  void editContactsMailListInterface(AllMailListContact mModel);
}
