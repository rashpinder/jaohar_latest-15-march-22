package jaohar.com.jaohar.interfaces.TrashModuleInterface;

import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.models.GetAllBankTrashModel;

public interface Rec_Del_Selected_bank_Interface {
//    void mRec_Del_Selected_Interface(BankModel mModel , boolean isSelected);
    void mRec_Del_Selected_Interface(GetAllBankTrashModel.Datum mModel , boolean isSelected);
}
