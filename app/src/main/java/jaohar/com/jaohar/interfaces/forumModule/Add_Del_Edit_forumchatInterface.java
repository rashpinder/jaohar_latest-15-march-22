package jaohar.com.jaohar.interfaces.forumModule;

import android.view.View;

import jaohar.com.jaohar.models.forummodels.AllMessagesItem;

public interface Add_Del_Edit_forumchatInterface {
    void mAdd_Del_ChatInterface(AllMessagesItem mModel , View view, String strType, int position);
}
