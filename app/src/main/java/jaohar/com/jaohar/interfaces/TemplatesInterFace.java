package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.TemplatesInvoicModel;
import jaohar.com.jaohar.models.InvoiceDataModel;

/**
 * Created by dharmaniz on 22/3/19.
 */

public interface TemplatesInterFace {
//    void mTemplatesInterface(TemplatesInvoicModel mModel);
    void mTemplatesInterface(InvoiceDataModel mModel);
}
