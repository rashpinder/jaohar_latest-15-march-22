package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.LinkStaffModel;
import jaohar.com.jaohar.models.LinksToHomeData;

/**
 * Created by Dharmani Apps on 3/20/2018.
 */

public interface DeleteLinkStaffInterface {
//    public void mDeleteLinkAdmin(LinkStaffModel mModel);
    public void mDeleteLinkAdmin(LinksToHomeData mModel);
}
