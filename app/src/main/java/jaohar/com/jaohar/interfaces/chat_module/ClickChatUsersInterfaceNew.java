package jaohar.com.jaohar.interfaces.chat_module;

import java.util.ArrayList;

import jaohar.com.jaohar.models.chatmodels.chatusers.AllUsersItem;

public interface ClickChatUsersInterfaceNew {
    void mChatUsersListInterfaceNew(int position, ArrayList<AllUsersItem> modelArrayList);
}
