package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.NewsModel;
import jaohar.com.jaohar.models.AllNewsModel;
import jaohar.com.jaohar.models.Datum;

/**
 * Created by Dharmani Apps on 3/19/2018.
 */

public interface DeleteNewsInterface {
    public void mDeleteNews(Datum mNewsModel, int position);
}
