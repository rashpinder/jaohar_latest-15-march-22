package jaohar.com.jaohar.interfaces.blog_module;

import android.view.View;

import jaohar.com.jaohar.beans.blog_module.Blog_Details_Model;

public interface DeleteBlogCommentsInterface {
    void mDeleteBlogInterface(Blog_Details_Model mModel, View view);
}
