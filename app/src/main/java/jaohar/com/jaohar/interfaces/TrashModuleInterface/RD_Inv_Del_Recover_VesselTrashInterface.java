package jaohar.com.jaohar.interfaces.TrashModuleInterface;

import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.models.SearchVesselData;

public interface RD_Inv_Del_Recover_VesselTrashInterface {
//    void RD_Inv_RecoverVesselTrashInterface(VesselSearchInvoiceModel mModel, String strType);
    void RD_Inv_RecoverVesselTrashInterface(SearchVesselData mModel, String strType);
}
