package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.InvoiceAddItemModel;

/**
 * Created by Dharmani Apps on 2/22/2018.
 */

public interface EditInvoiceItemData {
     void editInvoiceItemData(InvoiceAddItemModel mModel,int position);
}
