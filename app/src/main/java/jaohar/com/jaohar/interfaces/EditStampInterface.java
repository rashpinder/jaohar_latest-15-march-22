package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.StampsModel;

/**
 * Created by Dharmani Apps on 1/19/2018.
 */

public interface EditStampInterface {
    public void editInterface(StampsModel mStampsModel);
}
