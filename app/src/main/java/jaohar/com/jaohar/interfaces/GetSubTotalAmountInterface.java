package jaohar.com.jaohar.interfaces;

/**
 * Created by Dharmani Apps on 1/10/2018.
 */

public interface GetSubTotalAmountInterface {
    public void getSubTotalAmount(long mSubTotal);
}
