package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.NewRequestModel;
import jaohar.com.jaohar.models.NewReqData;

/**
 * Created by Dharmani Apps on 12/29/2017.
 */

public interface ConfirmRejectRequestInterface {
//    public void getConfirmRequest(NewRequestModel mNewRequestModel,String type);
    public void getConfirmRequest(NewReqData mNewRequestModel, String type);
}
