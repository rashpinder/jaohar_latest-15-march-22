package jaohar.com.jaohar.interfaces;

/**
 * Created by Dharmani Apps on 1/30/2018.
 */

public interface AutoCompleteVesselInterface {
//    public void autoCompleteVessel(VesselSearchInvoiceModel mVesselSearchInvoiceModel);
    public void autoCompleteVessel(String strVesselName);
}
