package jaohar.com.jaohar.interfaces.forumModule;

import android.view.View;

import jaohar.com.jaohar.models.chatmodels.AllChatsItem;

public interface Add_Del_Edit_forumchatInterfaceNew {
    void mAdd_Del_ChatInterface(AllChatsItem mModel, View view, String strType, int position);
}
