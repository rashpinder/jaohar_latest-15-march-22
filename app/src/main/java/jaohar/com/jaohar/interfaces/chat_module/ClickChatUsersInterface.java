package jaohar.com.jaohar.interfaces.chat_module;

import java.util.ArrayList;

import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;

public interface ClickChatUsersInterface {
    void mChatUsersListInterface(int position, ArrayList<ChatUsersModel> modelArrayList);
}
