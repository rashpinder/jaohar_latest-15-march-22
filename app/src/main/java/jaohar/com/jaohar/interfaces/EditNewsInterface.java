package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.NewsModel;
import jaohar.com.jaohar.models.AllNewsModel;
import jaohar.com.jaohar.models.Datum;

/**
 * Created by Dharmani Apps on 3/19/2018.
 */

public interface EditNewsInterface {
    public void mEditNews(Datum mNewsModel);
}
