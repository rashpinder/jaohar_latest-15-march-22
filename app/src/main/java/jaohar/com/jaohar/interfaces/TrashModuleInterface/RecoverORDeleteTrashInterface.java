package jaohar.com.jaohar.interfaces.TrashModuleInterface;

import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.models.AllVessel;

public interface RecoverORDeleteTrashInterface {
//    void mRecoverAndDeleteTrash(VesselesModel mModel, String strType);
    void mRecoverAndDeleteTrash(AllVessel mModel, String strType);
}
