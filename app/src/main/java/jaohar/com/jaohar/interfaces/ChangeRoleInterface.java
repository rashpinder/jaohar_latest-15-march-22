package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.AdminRoleModel;
import jaohar.com.jaohar.models.AllUser;

/**
 * Created by Dharmani Apps on 12/29/2017.
 */

public interface ChangeRoleInterface {
     void getChangedRole(AllUser mAdminRoleModel);
     void getResstPassword(AllUser mAdminRoleModel);
}
