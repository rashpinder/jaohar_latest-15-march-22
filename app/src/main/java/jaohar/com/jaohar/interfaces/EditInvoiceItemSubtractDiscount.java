package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.DiscountModel;

/**
 * Created by dharmaniz on 24/5/18.
 */

public interface EditInvoiceItemSubtractDiscount {
    public void editInvoiceItemSubtractDiscount(DiscountModel mModel, int position, String strType);
}
