package jaohar.com.jaohar.interfaces.forum_module_admin;

import android.view.View;

import jaohar.com.jaohar.beans.ForumModule.ForumChatModel;

public interface Add_Del_Edit_forumchatInterfaceAdmin {
    void mAdd_Del_ChatInterface(ForumChatModel mModel , View view, String strType, int position);
}
