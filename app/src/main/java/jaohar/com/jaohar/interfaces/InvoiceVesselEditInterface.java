package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;

/**
 * Created by Dharmani Apps on 2/15/2018.
 */

public interface InvoiceVesselEditInterface {
    public void invoiceVesselEdit(VesselSearchInvoiceModel mModel);
}
