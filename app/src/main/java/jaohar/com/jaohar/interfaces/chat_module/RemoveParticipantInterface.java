package jaohar.com.jaohar.interfaces.chat_module;

import java.util.ArrayList;

import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;

public interface RemoveParticipantInterface {
    void mRemoveParticipantInterface(int position, ArrayList<ChatUsersModel> modelArrayList);
}
