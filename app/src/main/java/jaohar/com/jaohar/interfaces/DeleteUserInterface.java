package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.AdminRoleModel;
import jaohar.com.jaohar.models.AllUser;

/**
 * Created by Dharmani Apps on 12/29/2017.
 */

public interface DeleteUserInterface {
    public void deleteUser(AllUser mAdminRoleModel);
}
