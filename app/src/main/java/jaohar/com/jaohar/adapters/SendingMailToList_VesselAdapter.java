package jaohar.com.jaohar.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.MailingListModel;
import jaohar.com.jaohar.interfaces.SelectingListNameInterface;
import jaohar.com.jaohar.models.AllMailList;

public class SendingMailToList_VesselAdapter extends RecyclerView.Adapter<SendingMailToList_VesselAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<AllMailList> modelArrayList;
    private SelectingListNameInterface mInterface;


    public SendingMailToList_VesselAdapter(Activity mActivity, ArrayList<AllMailList> modelArrayList, SelectingListNameInterface mInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mInterface = mInterface;

    }

    @Override
    public SendingMailToList_VesselAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mail_list, parent, false);
        return new SendingMailToList_VesselAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SendingMailToList_VesselAdapter.ViewHolder holder, final int position) {
        final AllMailList tempValue = modelArrayList.get(position);


        holder.serialNOTV.setText(tempValue.getListName());

        holder.mainLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterface.mSelectingListnameInterface(tempValue);
            }
        });


    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView serialNOTV, listNameTV;
        public LinearLayout mainLL;


        ViewHolder(View itemView) {
            super(itemView);
            mainLL = itemView.findViewById(R.id.mainLL);
            listNameTV = itemView.findViewById(R.id.listNameTV);
            serialNOTV = itemView.findViewById(R.id.serialNOTV);


        }
    }
}
