package jaohar.com.jaohar.adapters.staff_module;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.AddressBookListStaffActivity;
import jaohar.com.jaohar.activities.AllInVoicesActivity;
import jaohar.com.jaohar.activities.AllInternalNewsActivity;
import jaohar.com.jaohar.activities.AllNewsActivity;
import jaohar.com.jaohar.activities.LoginActivity;
import jaohar.com.jaohar.activities.MaillingStaffActivity;
import jaohar.com.jaohar.activities.ManagerActivity;
import jaohar.com.jaohar.activities.chat_module.ChatUsersListActivity;
import jaohar.com.jaohar.activities.educational_blogs_module.EducationalBlogsActivity;
import jaohar.com.jaohar.activities.forum_module.ForumListActivity;
import jaohar.com.jaohar.activities.forum_module.UkOfficeForumListActivity;
import jaohar.com.jaohar.activities.universal_invoice_module.AllUniversalInvoiceActivity;
import jaohar.com.jaohar.beans.staff_module.Staff_Tab_Model;
import jaohar.com.jaohar.interfaces.blog_module.ClickStaffModuleInterface;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;

public class StaffTabAdapter extends RecyclerView.Adapter<StaffTabAdapter.ViewHolder> {
    private Activity mActivity;
    private String strUkForumCount = "", strForumCount = "", unread_chat = "", unread_notifications = "";
    private ArrayList<Staff_Tab_Model> modelArrayList;
    private ClickStaffModuleInterface clickStaffModuleInterface;

    public StaffTabAdapter(Activity mActivity, ArrayList<Staff_Tab_Model> modelArrayList,
                           String strForumCount, String strUkForumCount, String unread_chat,
                           String unread_notifications, ClickStaffModuleInterface clickStaffModuleInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.strForumCount = strForumCount;
        this.strUkForumCount = strUkForumCount;
        this.unread_chat = unread_chat;
        this.unread_notifications = unread_notifications;
        this.clickStaffModuleInterface = clickStaffModuleInterface;
    }

    @Override
    public StaffTabAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_staff_layout, parent, false);
        return new StaffTabAdapter.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final StaffTabAdapter.ViewHolder holder, final int position) {
        final Staff_Tab_Model tempValue = modelArrayList.get(position);
        String strName = tempValue.getModule_name();
        strName = strName.replaceAll(" ", "\n");

        holder.formTV.setText(strName);

        // Showing Badges According to Type
        if (tempValue.getType().equals("vessel")) {
            holder.badgesTV.setVisibility(View.GONE);

        } else if (tempValue.getType().equals("invoice")) {
            holder.badgesTV.setVisibility(View.GONE);

        } else if (tempValue.getType().equals("universal_invoice")) {
            holder.badgesTV.setVisibility(View.GONE);

        } else if (tempValue.getType().equals("address_book")) {
            holder.badgesTV.setVisibility(View.GONE);

        } else if (tempValue.getType().equals("mailing_list")) {
            holder.badgesTV.setVisibility(View.GONE);

        } else if (tempValue.getType().equals("staff_forum")) {
            if (!strForumCount.equals("")) {
                if (!strForumCount.equals("0")) {
                    holder.badgesTV.setVisibility(View.VISIBLE);
                    if (Integer.parseInt(strForumCount) > 99) {
                        holder.badgesTV.setText("99+");
                    } else {
                        holder.badgesTV.setText(strForumCount);
                    }
                } else {
                    holder.badgesTV.setVisibility(View.GONE);
                }
            } else {
                holder.badgesTV.setVisibility(View.GONE);
            }
        } else if (tempValue.getType().equals("notifications")) {
            if (!unread_notifications.equals("")) {
                if (!unread_notifications.equals("0")) {
                    holder.badgesTV.setVisibility(View.VISIBLE);
                    if (Integer.parseInt(unread_notifications) > 99) {
                        holder.badgesTV.setText("99+");
                    } else {
                        holder.badgesTV.setText(unread_notifications);
                    }
                } else {
                    holder.badgesTV.setVisibility(View.GONE);
                }
            } else {
                holder.badgesTV.setVisibility(View.GONE);
            }
        } else if (tempValue.getType().equals("fund_transfer")) {
            holder.badgesTV.setVisibility(View.GONE);

        } else if (tempValue.getType().equals("blog")) {
            holder.badgesTV.setVisibility(View.GONE);

        } else if (tempValue.getType().equals("chat_support")) {
            holder.badgesTV.setVisibility(View.GONE);

        } else if (tempValue.getType().equals("chat")) {
            holder.badgesTV.setVisibility(View.VISIBLE);
            if (!unread_chat.equals("")) {
                if (!unread_chat.equals("0")) {
                    holder.badgesTV.setVisibility(View.VISIBLE);
                    if (Integer.parseInt(unread_chat) > 99) {
                        holder.badgesTV.setText("99+");
                    } else {
                        holder.badgesTV.setText(unread_chat);
                    }
                } else {
                    holder.badgesTV.setVisibility(View.GONE);
                }
            } else {
                holder.badgesTV.setVisibility(View.GONE);
            }

        } else if (tempValue.getType().equals("manager")) {
            holder.badgesTV.setVisibility(View.GONE);

        } else if (tempValue.getType().equals("forum") || tempValue.getType().equals("uk_forum")) {
            if (!strUkForumCount.equals("")) {
                if (!strUkForumCount.equals("0")) {
                    holder.badgesTV.setVisibility(View.VISIBLE);
                    if (Integer.parseInt(strForumCount) > 99) {
                        holder.badgesTV.setText("99+");
                    } else {
                        holder.badgesTV.setText(strUkForumCount);
                    }
                } else {
                    holder.badgesTV.setVisibility(View.GONE);
                }
            } else {
                holder.badgesTV.setVisibility(View.GONE);
            }
        }


        if (!tempValue.getApp_image().equals("")) {
            Glide.with(mActivity).load(tempValue.getApp_image()).into(holder.itemIMG);
        }


        holder.clickLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tempValue.getType().equals("vessel")) {
                    JaoharConstants.IsAllVesselsDetailBack = false;
//                    mActivity.startActivity(new Intent(mActivity, AllVesselsActivity.class));
                } else if (tempValue.getType().equals("invoice")) {
                    mActivity.startActivity(new Intent(mActivity, AllInVoicesActivity.class));
                    mActivity.finish();
                } else if (tempValue.getType().equals("universal_invoice")) {
                    JaoharConstants.is_Staff_FragmentClick = true;
                    mActivity.startActivity(new Intent(mActivity, AllUniversalInvoiceActivity.class));
                    mActivity.finish();
                } else if (tempValue.getType().equals("address_book")) {
                    JaoharConstants.is_Staff_FragmentClick = true;
                    mActivity.startActivity(new Intent(mActivity, AddressBookListStaffActivity.class));
                } else if (tempValue.getType().equals("mailing_list")) {
                    mActivity.startActivity(new Intent(mActivity, MaillingStaffActivity.class));
                } else if (tempValue.getType().equals("staff_forum")) {
                    JaoharConstants.is_Staff_FragmentClick = true;
                    mActivity.startActivity(new Intent(mActivity, ForumListActivity.class));
                    mActivity.finish();
                } else if (tempValue.getType().equals("fund_transfer")) {
                    Toast.makeText(mActivity, "In Progress ...", Toast.LENGTH_SHORT).show();
                } else if (tempValue.getType().equals("blog")) {
                    clickStaffModuleInterface.mClickStaffModuleInterface(position, tempValue.getType());
                } else if (tempValue.getType().equals("chat_support")) {
                    Toast.makeText(mActivity, "In Progress ...", Toast.LENGTH_SHORT).show();
                } else if (tempValue.getType().equals("chat")) {
                    mActivity.startActivity(new Intent(mActivity, ChatUsersListActivity.class));
                } else if (tempValue.getType().equals("manager")) {
                    if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_ADMIN, false)) {
                        mActivity.startActivity(new Intent(mActivity, ManagerActivity.class));
                    } else {
                        Intent mIntent = new Intent(mActivity, LoginActivity.class);
                        mIntent.putExtra(JaoharConstants.LOGIN_TYPE, JaoharConstants.ADMIN);
                        mActivity.startActivity(mIntent);
                    }
                } else if (tempValue.getType().equals("forum") || tempValue.getType().equals("uk_forum")) {
                    JaoharConstants.is_Staff_FragmentClick = true;
                    mActivity.startActivity(new Intent(mActivity, UkOfficeForumListActivity.class));
                    mActivity.finish();
                } else if (tempValue.getType().equals("educational_blogs")) {
                    JaoharConstants.is_Staff_FragmentClick = true;
                    mActivity.startActivity(new Intent(mActivity, EducationalBlogsActivity.class));
                } else if (tempValue.getType().equals("user_news")) {
                    JaoharConstants.is_Staff_FragmentClick = true;
                    mActivity.startActivity(new Intent(mActivity, AllNewsActivity.class));
                } else if (tempValue.getType().equals("internal_news")) {
                    JaoharConstants.is_Staff_FragmentClick = true;
                    mActivity.startActivity(new Intent(mActivity, AllInternalNewsActivity.class));
                } else if (tempValue.getType().equals("notifications")) {
                    clickStaffModuleInterface.mClickStaffModuleInterface(position, tempValue.getType());
                } else if (tempValue.getType().equals("favourite")) {
                    clickStaffModuleInterface.mClickStaffModuleInterface(position, tempValue.getType());
                } else {
                    if (tempValue.getModule_name().equals("Educational Blogs")) {
                        JaoharConstants.is_Staff_FragmentClick = true;
                        mActivity.startActivity(new Intent(mActivity, EducationalBlogsActivity.class));
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView formTV, badgesTV;
        public ImageView itemIMG;
        public RelativeLayout clickLL;

        ViewHolder(View itemView) {
            super(itemView);
            badgesTV = (TextView) itemView.findViewById(R.id.badgesTV);
            formTV = (TextView) itemView.findViewById(R.id.formTV);
            itemIMG = (ImageView) itemView.findViewById(R.id.itemIMG);
            clickLL = (RelativeLayout) itemView.findViewById(R.id.clickLL);
        }
    }
}

