package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
//import com.bumptech.glide.request.animation.GlideAnimation;

import java.util.ArrayList;
import java.util.HashMap;

import jaohar.com.jaohar.interfaces.FavoriteVesselInterface;
import jaohar.com.jaohar.interfaces.MailSelectedVesselsInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.SendingMailToVesselInterface;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.AllNewsActivity;
import jaohar.com.jaohar.activities.DetailsActivity;
import jaohar.com.jaohar.activities.EditVesselActivity;
import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.interfaces.DeleteVesselsInterface;
import jaohar.com.jaohar.interfaces.OpenPopUpVesselInterface;
import jaohar.com.jaohar.interfaces.SendEmailInterface;
import jaohar.com.jaohar.interfaces.SendMultiVesselDataInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.utils.JaoharConstants;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class VesselesShipAdapter extends RecyclerView.Adapter<VesselesShipAdapter.ViewHolder> {
    DeleteVesselsInterface mDeleteVesselsInterface;
    private Activity mActivity;
//    private ArrayList<VesselesModel> modelArrayList;
    private ArrayList<AllVessel> modelArrayList;
    private SendEmailInterface mSendEmailInterface;
    private static boolean[] checkBoxState = null;
    boolean isFirstClick = false;
    private SendMultiVesselDataInterface mSendMultiVesselDataInterface;
    String strNormalText, strNewsTEXt, strPhotoURL;
    paginationforVesselsInterface mPagination;
    OpenPopUpVesselInterface mOpenPoUpInterface;
    SendingMailToVesselInterface mMailToVessel;
    private OnClickInterface onClickInterface;
    private FavoriteVesselInterface favoriteVesselInterface;
    private ArrayList<String> FavouriteItems = new ArrayList<>();
    public static ArrayList<String> Items = new ArrayList<>();
    private MailSelectedVesselsInterface mMailSelectedVesselsInterface;

//    private static HashMap<VesselesModel, Boolean> checkedForModel = new HashMap<VesselesModel, Boolean>();
    private static HashMap<AllVessel, Boolean> checkedForModel = new HashMap<AllVessel, Boolean>();

    public VesselesShipAdapter(Activity mActivity, ArrayList<AllVessel> modelArrayList, DeleteVesselsInterface mDeleteVesselsInterface,
                               SendEmailInterface mSendEmailInterface, SendMultiVesselDataInterface mSendMultiVesselDataInterface,
                               String strNormalText, String strNewsTEXt, String strPhotoURL, paginationforVesselsInterface mPagination,
                               OpenPopUpVesselInterface mOpenPoUpInterface, SendingMailToVesselInterface mMailToVessel,
                               OnClickInterface onClickInterface, FavoriteVesselInterface favoriteVesselInterface,
                               MailSelectedVesselsInterface mMailSelectedVesselsInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDeleteVesselsInterface = mDeleteVesselsInterface;
        this.mSendEmailInterface = mSendEmailInterface;
        this.mSendMultiVesselDataInterface = mSendMultiVesselDataInterface;
        this.strNormalText = strNormalText;
        this.strNewsTEXt = strNewsTEXt;
        this.strPhotoURL = strPhotoURL;
        this.mPagination = mPagination;
        this.mOpenPoUpInterface = mOpenPoUpInterface;
        this.mMailToVessel = mMailToVessel;
        this.onClickInterface = onClickInterface;
        this.favoriteVesselInterface = favoriteVesselInterface;
        this.mMailSelectedVesselsInterface = mMailSelectedVesselsInterface;
        checkBoxState = new boolean[modelArrayList.size()];
    }

    //    item_home
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_vessels, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final AllVessel tempValue = modelArrayList.get(position);

        // pagination for smooth scrooling
//        if (position >= modelArrayList.size() - 1) {
//            mPagination.mPaginationforVessels(true);
//        }

        if (checkBoxState != null) {
//          holder.imgMultiSelectIV .setChecked(checkBoxState[position]);
            holder.imgMultiSelectIV.setChecked(tempValue.getSelected());
        }

        holder.imgMultiSelectIV.setTag(position);

        if (position == 0) {
            holder.normalTextTV.setText(strNormalText);
        } else {
            holder.relativeLL.setVisibility(View.GONE);
        }

        holder.checkImg.setVisibility(View.VISIBLE);

        if (Items.contains(String.valueOf(position))) {
            holder.checkImg.setImageResource(R.drawable.ic_checked);
            mMailSelectedVesselsInterface.MailSelectedVessels(position, tempValue.getIMONumber(),
                    Items, tempValue, true);
        } else {
            holder.checkImg.setImageResource(R.drawable.ic_unchecked);
            mMailSelectedVesselsInterface.MailSelectedVessels(position, tempValue.getIMONumber(),
                    Items, tempValue, false);
        }

        holder.checkImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Items.contains(String.valueOf(position))) {
                    Items.remove(String.valueOf(position));
                    holder.checkImg.setImageResource(R.drawable.ic_unchecked);
                    mMailSelectedVesselsInterface.MailSelectedVessels(position, tempValue.getRecordId(),
                            Items, tempValue, true);
                } else {
                    Items.add(String.valueOf(position));
                    holder.checkImg.setImageResource(R.drawable.ic_checked);
                    mMailSelectedVesselsInterface.MailSelectedVessels(position, tempValue.getRecordId()
                            , Items, tempValue, false);
                }
            }
        });

        holder.relativeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpenPoUpInterface.mOpenPopUpVesselInterface(strNewsTEXt, strPhotoURL);
            }
        });

        holder.allnewsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.IS_SEE_ALL_NEWS_CLICK = true;
                Intent mIntent = new Intent(mActivity, AllNewsActivity.class);
                mActivity.startActivity(mIntent);
            }
        });

        holder.normalTextTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpenPoUpInterface.mOpenPopUpVesselInterface(strNewsTEXt, strPhotoURL);
            }
        });

        holder.linkIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpenPoUpInterface.mOpenPopUpVesselInterface(strNewsTEXt, strPhotoURL);
            }
        });

        if (tempValue.getPhoto1().length() > 0 && tempValue.getPhoto1().contains("http")) {

            Glide.with(mActivity).load(tempValue.getPhoto1()).into(holder.item_Image);
        } else {
            holder.item_Image.setImageResource(R.drawable.palace_holder);
        }

        /* checkBoxState has the value of checkBox ie true or false,
         * The position is used so that on scroll your selected checkBox maintain its state */
        if (tempValue.getVesselName().length() > 0) {
            holder.item_Title.setText(tempValue.getVesselName());
        }

        if (tempValue.getVesselType().length() > 0) {
            String strType = "DWT " + tempValue.getCapacity() + "/" + tempValue.getPriceIdea() + " " + tempValue.getCurrency();
            holder.item_Type.setText(strType);
        }
        if (tempValue.getCapacity().length() > 0) {
            String strCApicity = tempValue.getVesselType() + "/" + tempValue.getYearBuilt() + "/" + tempValue.getPlaceOfBuilt();
            holder.item_DateTime.setText(strCApicity);
        }

        holder.imgRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JaoharConstants.Is_click_form_All_Vessals = true;
                Intent mIntent = new Intent(mActivity, DetailsActivity.class);
                mIntent.putExtra("Model", tempValue);
                mIntent.putExtra("isEditShow", true);
                mActivity.startActivity(mIntent);
            }
        });
        System.out.println("Builder==" + tempValue.getBuilder());

        holder.item_EditIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, EditVesselActivity.class);
                mIntent.putExtra("Model", tempValue);
                mIntent.putExtra("Title", "Edit Vessels");
                mActivity.startActivity(mIntent);
            }
        });

        holder.item_MailDetailsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Send Mail Here for Single*/
                mSendEmailInterface.sendEmail(tempValue);
            }
        });

        holder.item_DeleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*To Delete Vessels Call Interface*/
                mDeleteVesselsInterface.mDeleteVessel(tempValue.getRecordId());
            }
        });

        holder.item_MailToListIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Send Mail Here To All Using Mail List*/
                mMailToVessel.mSendingMailVessel(tempValue);
            }
        });


        holder.imgMultiSelectIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.imgMultiSelectIV.isChecked()) {
                    tempValue.setSelected(true);
                    mSendMultiVesselDataInterface.sendMultipleVesselsData(tempValue, false);
                } else {
                    tempValue.setSelected(false);
                    mSendMultiVesselDataInterface.sendMultipleVesselsData(tempValue, true);
                }
            }
        });

        /*Vessels Status on Vessel Image*/
        if (tempValue.getStatus().equals("Sold")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_sold);
        } else if (tempValue.getStatus().equals("Withdrawn")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_withdrawn);
        } else if (tempValue.getStatus().equals("Committed")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_committed);
        } else if (tempValue.getStatus().equals("Scraped")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_scraped);
        } else if (tempValue.getStatus().equals("Hot Sale")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_hotsale);
        } else {
            holder.item_Image_Status.setVisibility(View.GONE);
        }

        holder.optionsImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickInterface.mOnClickInterface(position);
            }
        });

        if (tempValue.getFavoriteStatus().equals("0")) {
            holder.favImg.setImageResource(R.drawable.ic_bookmark_border);
            FavouriteItems.remove(String.valueOf(position));
        } else if (tempValue.getFavoriteStatus().equals("1")) {
            holder.favImg.setImageResource(R.drawable.ic_bookmark_yellow);
            FavouriteItems.add(String.valueOf(position));
        } else {
            holder.favImg.setImageResource(R.drawable.ic_bookmark_border);
        }

        holder.favImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FavouriteItems.contains(String.valueOf(position))) {
                    FavouriteItems.remove(String.valueOf(position));
                    favoriteVesselInterface.mFavoriteVesselInterface(position, "0", holder.favImg);
                } else {
                    FavouriteItems.add(String.valueOf(position));
                    favoriteVesselInterface.mFavoriteVesselInterface(position, "1", holder.favImg);
                }
            }
        });
    }

    public void ischecked(int position, boolean flag) {
        checkedForModel.put(this.modelArrayList.get(position), flag);
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView item_Title, item_Type, item_DateTime, txtEdit, txtMailTV, txtDelete, normalTextTV, allnewsTV;
        ImageView item_Image, checkImg;
        ImageView item_Image_Status;
        RelativeLayout layoutItemRL, itemClickRL, relativeLL, imgRL;
        CheckBox imgMultiSelectIV;
        ImageView item_DeleteIV, item_EditIV, item_MailDetailsIV, linkIMG, item_MailToListIV, favImg, optionsImg;

        ViewHolder(View itemView) {
            super(itemView);
            relativeLL = itemView.findViewById(R.id.relativeLL);
            normalTextTV = itemView.findViewById(R.id.normalTextTV);
            allnewsTV = itemView.findViewById(R.id.allnewsTV);
            item_EditIV = itemView.findViewById(R.id.item_EditIV);
            item_DeleteIV = itemView.findViewById(R.id.item_DeleteIV);
            item_MailDetailsIV = itemView.findViewById(R.id.item_MailDetailsIV);
            item_MailToListIV = itemView.findViewById(R.id.item_MailToListIV);
            item_Image = (ImageView) itemView.findViewById(R.id.item_Image);
            item_Image_Status = (ImageView) itemView.findViewById(R.id.item_Image_Status);
            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            imgMultiSelectIV = (CheckBox) itemView.findViewById(R.id.imgMultiSelectIV);
            item_DateTime = (TextView) itemView.findViewById(R.id.item_DateTime);
            item_Type = (TextView) itemView.findViewById(R.id.item_Type);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtMailTV = (TextView) itemView.findViewById(R.id.txtMailTV);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
            item_DeleteIV = (ImageView) itemView.findViewById(R.id.item_DeleteIV);
            item_EditIV = (ImageView) itemView.findViewById(R.id.item_EditIV);
            item_MailDetailsIV = (ImageView) itemView.findViewById(R.id.item_MailDetailsIV);
            layoutItemRL = (RelativeLayout) itemView.findViewById(R.id.layoutItemRL);
            itemClickRL = (RelativeLayout) itemView.findViewById(R.id.itemClickRL);
            linkIMG = (ImageView) itemView.findViewById(R.id.linkIMG);

            favImg = itemView.findViewById(R.id.favImg);
            optionsImg = itemView.findViewById(R.id.optionsImg);
            checkImg = itemView.findViewById(R.id.checkImg);
            imgRL = itemView.findViewById(R.id.imgRL);
        }
    }
}