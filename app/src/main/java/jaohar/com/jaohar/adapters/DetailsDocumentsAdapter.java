package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.OpenLinkActivity;
import jaohar.com.jaohar.beans.DocumentModel;
import jaohar.com.jaohar.interfaces.DeleteVesselTypeInterface;
import jaohar.com.jaohar.interfaces.DownloadDocInterface;
import jaohar.com.jaohar.interfaces.PdfInterface;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class DetailsDocumentsAdapter extends RecyclerView.Adapter<DetailsDocumentsAdapter.ViewHolder> {
    private Context mActivity;
    private ArrayList<DocumentModel> modelArrayList;
    private DownloadDocInterface mDownloadDocInterface;
    private PdfInterface mPdfInterface;

    public DetailsDocumentsAdapter(Activity mActivity, ArrayList<DocumentModel> modelArrayList,DownloadDocInterface mDownloadDocInterface,PdfInterface mPdfInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDownloadDocInterface = mDownloadDocInterface;
        this.mPdfInterface = mPdfInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_details_document, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final DocumentModel tempValue = modelArrayList.get(position);
        holder.txtItemsTV.setText(tempValue.getDocumentName());

        holder.txtItemsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempValue.getDocumentPath().contains(".pdf")|tempValue.getDocumentPath().contains(".txt")){
//                    mPdfInterface.pdfInterface(tempValue.getDocumentPath());
                    mDownloadDocInterface.mDownloadDocInterface(tempValue.getDocumentPath(),tempValue.getDocumentName());

                }
                else{
                    mDownloadDocInterface.mDownloadDocInterface(tempValue.getDocumentPath(),tempValue.getDocumentName());
                }

//                Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
//                mIntent.putExtra("TITLE", tempValue.getDocumentName());
//                mIntent.putExtra("LINK",tempValue.getDocumentPath());
//                mActivity.startActivity(mIntent);
            }
        });

        holder.pdfImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempValue.getDocumentPath().contains(".pdf")|tempValue.getDocumentPath().contains(".txt")){
                    mDownloadDocInterface.mDownloadDocInterface(tempValue.getDocumentPath(),tempValue.getDocumentName());
//                    mPdfInterface.pdfInterface(tempValue.getDocumentPath());
//                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
//                            Uri.parse(tempValue.getDocumentPath()));
//                    mActivity.startActivity(browserIntent);

                }
                else{
                    mDownloadDocInterface.mDownloadDocInterface(tempValue.getDocumentPath(),tempValue.getDocumentName());
                }

//                Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
//                mIntent.putExtra("TITLE", tempValue.getDocumentName());
//                mIntent.putExtra("LINK",tempValue.getDocumentPath());
//                mActivity.startActivity(mIntent);
            }
        });
    }



    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItemsTV;
        public ImageView imgRemoveIV, pdfImage;

        ViewHolder(View itemView) {
            super(itemView);
            imgRemoveIV = (ImageView) itemView.findViewById(R.id.imgRemoveIV);
            txtItemsTV = (TextView) itemView.findViewById(R.id.txtItemsTV);
            pdfImage = itemView.findViewById(R.id.pdfImage);
        }
    }
}