package jaohar.com.jaohar.adapters.invoices_module;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.interfaces.CompaniesClickInterface;

public class AllCompaniesAdapter extends RecyclerView.Adapter<AllCompaniesAdapter.ViewHolder> {
    private  Activity mActivity;
    private  ArrayList<CompaniesModel> mArrayList;
    private CompaniesClickInterface onClickInterface;

    public AllCompaniesAdapter(Activity mActivity, ArrayList<CompaniesModel> mArrayList, CompaniesClickInterface onClickInterface
    ) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.onClickInterface = onClickInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_all_companies, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        CompaniesModel tempValue = mArrayList.get(position);

        holder.companyNameTV.setText(tempValue.getCompany_name());
        holder.emailET.setText(tempValue.getAdded_by());
        holder.addressTV.setText(tempValue.getAddress1());
        holder.dateTV.setText(tempValue.getModification_date());

        holder.optionsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickInterface.mOnClickInterface(tempValue,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView companyNameTV, emailET, addressTV, dateTV, DateTV;
        public ImageView stampIV, optionsIV;

        ViewHolder(View itemView) {
            super(itemView);

            optionsIV = itemView.findViewById(R.id.optionsIV);
            stampIV = itemView.findViewById(R.id.stampIV);
            companyNameTV = itemView.findViewById(R.id.companyNameTV);
            DateTV = itemView.findViewById(R.id.DateTV);
            dateTV = itemView.findViewById(R.id.dateTV);
            emailET = itemView.findViewById(R.id.emailET);
            addressTV = itemView.findViewById(R.id.addressTV);
        }
    }
}