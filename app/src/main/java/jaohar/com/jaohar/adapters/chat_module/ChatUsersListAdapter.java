package jaohar.com.jaohar.adapters.chat_module;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chauthai.swipereveallayout.SwipeRevealLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RelativeTimeTextView;
import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;
import jaohar.com.jaohar.interfaces.chat_module.ChatMoreOptionsInterface;
import jaohar.com.jaohar.interfaces.chat_module.ChatUsersListInterface;
import jaohar.com.jaohar.interfaces.chat_module.ClickChatUsersInterface;

public class ChatUsersListAdapter extends RecyclerView.Adapter<ChatUsersListAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<ChatUsersModel> modelArrayList;
    private ChatUsersListInterface mPagination;
    private ClickChatUsersInterface clickChatUsersInterface;
    private ChatMoreOptionsInterface mChatMoreOptionsInterface;

    public ChatUsersListAdapter(Activity mActivity, ArrayList<ChatUsersModel> modelArrayList,
                                ChatUsersListInterface mPagination, ClickChatUsersInterface clickChatUsersInterface,
                                ChatMoreOptionsInterface mChatMoreOptionsInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mPagination = mPagination;
        this.clickChatUsersInterface = clickChatUsersInterface;
        this.mChatMoreOptionsInterface = mChatMoreOptionsInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_all_users_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        // pagination for smooth scrooling
        if (position >= modelArrayList.size() - 1) {
            mPagination.mChatUsersListInterface(true);
        }

        if (modelArrayList.get(position).getName() != null) {
            holder.NameTV.setText(modelArrayList.get(position).getName());
        }

        if (modelArrayList.get(position).getMessage() != null) {
            holder.messageTV.setText(html2text(modelArrayList.get(position).getMessage()));
        }

        /* set RelativeTimeTextView typeface */
        Typeface typeface = Typeface.createFromAsset(mActivity.getAssets(), "Poppins-Regular.ttf");
        holder.DayTV.setTypeface(typeface);

        if (modelArrayList.get(position).getMessage_time() != null && !modelArrayList.get(position).getMessage_time().equals("0")) {
//            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy");
            Date dateFormat = new Date((Long.parseLong(modelArrayList.get(position).getMessage_time()) * 1000));
            String weekday = sdf.format(dateFormat);

            /* get current date */
//            Date c = Calendar.getInstance().getTime();
//            System.out.println("Current time => " + c);
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyy");
            SimpleDateFormat timedf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
            String formattedDate = df.format(dateFormat);
            String formattedTime = timedf.format(dateFormat);

            if (weekday.equalsIgnoreCase(formattedDate)) {
                holder.DayTV.setText(formattedTime);
            } else {
                holder.DayTV.setText(weekday);
            }

        } else {
            holder.DayTV.setVisibility(View.GONE);
        }

        if (!modelArrayList.get(position).getUnread_chat().equals("") && !modelArrayList.get(position).getUnread_chat().equals("0")) {
            holder.totalMessagesTV.setVisibility(View.VISIBLE);
            holder.DayTV.setTextColor(Color.parseColor("#295fa3"));

            if (Integer.parseInt(modelArrayList.get(position).getUnread_chat()) > 9) {
                holder.totalMessagesTV.setText("9+");
            } else {
                holder.totalMessagesTV.setText(modelArrayList.get(position).getUnread_chat());
            }

        } else {
            holder.totalMessagesTV.setVisibility(View.GONE);
            holder.DayTV.setTextColor(Color.parseColor("#FF8E8D8D"));
        }

        if (modelArrayList.get(position).getImage() != null && modelArrayList.get(position).getImage().contains("http")) {
            if ((modelArrayList.get(position).getImage().contains("https"))) {
                if (modelArrayList.get(position).getGroup_id().equals("0")) {
                    Glide.with(mActivity).load(modelArrayList.get(position).getImage()).placeholder(R.drawable.ic_user).into(holder.userProfile);
                } else {
                    Glide.with(mActivity).load(modelArrayList.get(position).getImage()).placeholder(R.drawable.ic_user_group).into(holder.userProfile);
                }

            } else {
                if (modelArrayList.get(position).getGroup_id().equals("0")) {
                    Glide.with(mActivity).load(modelArrayList.get(position).getImage().replace("http://", "https://"))
                            .placeholder(R.drawable.ic_user)
                            .into(holder.userProfile);
                } else {
                    Glide.with(mActivity).load(modelArrayList.get(position).getImage().replace("http://", "https://"))
                            .placeholder(R.drawable.ic_user_group)
                            .into(holder.userProfile);
                }
            }
        } else {
            if (modelArrayList.get(position).getGroup_id().equals("0")) {
                holder.userProfile.setImageResource(R.drawable.ic_user);
            } else {
                holder.userProfile.setImageResource(R.drawable.ic_user_group);
            }
        }

        if (modelArrayList.get(position).getOnline_state().equals("Available")) {
            holder.item_Image_Status.setBackgroundResource(R.drawable.bg_green_status);
        } else {
            holder.item_Image_Status.setBackgroundResource(R.drawable.bg_red_status);
        }

        if (modelArrayList.get(position).getGroup_id().equals("0")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
        } else {
            holder.item_Image_Status.setVisibility(View.GONE);
        }

        holder.mainLayoutClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelArrayList.size() > 0) {
                    clickChatUsersInterface.mChatUsersListInterface(position, modelArrayList);
                }
            }
        });

        holder.moreLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mChatMoreOptionsInterface.mChatMoreOptionsInterface(position, modelArrayList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mainLayoutClick, moreLL;
        TextView NameTV, messageTV, totalMessagesTV;
        ImageView userProfile, item_Image_Status, editIV;
        RelativeTimeTextView DayTV;
        SwipeRevealLayout mSwipeRevealLayout;

        ViewHolder(View itemView) {
            super(itemView);

            mainLayoutClick = itemView.findViewById(R.id.mainLayoutClick);
            NameTV = itemView.findViewById(R.id.NameTV);
            messageTV = itemView.findViewById(R.id.messageTV);
            DayTV = itemView.findViewById(R.id.DayTV);
            totalMessagesTV = itemView.findViewById(R.id.totalMessagesTV);
            userProfile = itemView.findViewById(R.id.userProfile);
            item_Image_Status = itemView.findViewById(R.id.item_Image_Status);
            editIV = itemView.findViewById(R.id.editIV);
            moreLL = itemView.findViewById(R.id.moreLL);
            mSwipeRevealLayout = itemView.findViewById(R.id.mSwipeRevealLayout);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    //for converting html to text
    public CharSequence html2text(String html) {
        CharSequence trimmed = noTrailingwhiteLines(Html.fromHtml(html.replace("\n", "<br />")));
        return trimmed;
    }

    //for hiding extra white space
    private CharSequence noTrailingwhiteLines(CharSequence text) {

        if (text.length() > 0)
            while (text.charAt(text.length() - 1) == '\n') {
                text = text.subSequence(0, text.length() - 1);
            }
        return text;
    }
}
