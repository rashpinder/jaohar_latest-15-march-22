package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.daimajia.swipe.SwipeLayout;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.NewsModel;
import jaohar.com.jaohar.interfaces.DeleteNewsInterface;
import jaohar.com.jaohar.interfaces.EditNewsInterface;
import jaohar.com.jaohar.interfaces.OpenNewsPopUpInterFace;
import jaohar.com.jaohar.models.Datum;
import jaohar.com.jaohar.models.Datum;
import jaohar.com.jaohar.utils.JaoharConstants;

public class UserNewsAdapter extends RecyclerView.Adapter<UserNewsAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<Datum> modelArrayList;
    OpenNewsPopUpInterFace mOpenNewsPopUP;

    public UserNewsAdapter(Activity mActivity, ArrayList<Datum> modelArrayList, OpenNewsPopUpInterFace mOpenNewsPopUP) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mOpenNewsPopUP = mOpenNewsPopUP;
    }

    @Override
    public UserNewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_news_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final UserNewsAdapter.ViewHolder holder, final int position) {
        final Datum tempValue = modelArrayList.get(position);

        if (tempValue.getPhoto() != null) {
            Glide.with(mActivity).load(tempValue.getPhoto()).placeholder(R.drawable.palace_holder).into(holder.imgIV);
        }

        if (tempValue.getNewsText() != null) {
            holder.textNEWS.setText(tempValue.getNewsText());
        }

        holder.itemRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpenNewsPopUP.openNewsPopUpInterFace(tempValue.getNews(), tempValue.getPhoto());
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textNEWS;
        public ImageView imgIV;
        public RelativeLayout itemRL;

        ViewHolder(View itemView) {
            super(itemView);
            textNEWS = itemView.findViewById(R.id.textNEWS);
            imgIV = itemView.findViewById(R.id.imgIV);
            itemRL =itemView.findViewById(R.id.itemRL);
        }
    }
}
