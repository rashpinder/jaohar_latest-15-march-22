package jaohar.com.jaohar.adapters.trash_module_adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.Rec_Del_Selected_bank_Interface;

import jaohar.com.jaohar.interfaces.TrashModuleInterface.Recover_Del_Bank_Single_inteface;
import jaohar.com.jaohar.models.GetAllBankTrashModel;

public class DankTrashAdapter extends RecyclerView.Adapter<DankTrashAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<GetAllBankTrashModel.Datum> modelArrayList;

    private static HashMap<GetAllBankTrashModel.Datum, Boolean> checkedForModel = new HashMap<>();
    Recover_Del_Bank_Single_inteface mrecoverDelete;
    Rec_Del_Selected_bank_Interface mRecoverDeleteSelected;
    private static boolean[] checkBoxState = null;

    public DankTrashAdapter(Activity mActivity, ArrayList<GetAllBankTrashModel.Datum> modelArrayList,Rec_Del_Selected_bank_Interface mRecoverDeleteSelected ,Recover_Del_Bank_Single_inteface mrecoverDelete) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mrecoverDelete = mrecoverDelete;
        this.mRecoverDeleteSelected = mRecoverDeleteSelected;
        checkBoxState = new boolean[modelArrayList.size()];
    }

    @Override
    public DankTrashAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bank_trash, parent, false);
        return new DankTrashAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DankTrashAdapter.ViewHolder holder, final int position) {
        final GetAllBankTrashModel.Datum tempValue = modelArrayList.get(position);

        holder.itemNameTV.setText(tempValue.getBankName());
        holder.benificieryNameTV.setText(tempValue.getBeneficiary());
        holder.addressTV.setText(tempValue.getAddress1());
        holder.deletedBYTV.setText(tempValue.getAddedBy());

        holder.recoverIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mrecoverDelete.mRecover_Del_Bank_Interface(tempValue,"recover");

            }
        });
        holder.deleteIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mrecoverDelete.mRecover_Del_Bank_Interface(tempValue,"delete");
            }
        });

        holder.imgMultiSelectIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (
                        holder.imgMultiSelectIV.isChecked()) {
                    checkBoxState[position] = true;
                    ischecked(position,true);
                    mRecoverDeleteSelected.mRec_Del_Selected_Interface(tempValue,false);
                } else {
                    checkBoxState[position] = false;
                    ischecked(position,false);
                    mRecoverDeleteSelected.mRec_Del_Selected_Interface(tempValue,true);

                }

            }
        });
        /*if country is in checkedForCountry then set the checkBox to true */
        if (checkedForModel.get(tempValue) != null) {
            holder.imgMultiSelectIV.setChecked(checkedForModel.get(tempValue));
        }

        /*Set tag to all checkBox*/
        holder.imgMultiSelectIV.setTag(tempValue);


    }

    private void ischecked(int position, boolean flag) {
        checkedForModel.put(this.modelArrayList.get(position), flag);
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemNameTV, benificieryNameTV, addressTV,deletedBYTV;
        public ImageView recoverIMG, viewIMG, deleteIMG;
        CheckBox imgMultiSelectIV;

        ViewHolder(View itemView) {
            super(itemView);
            recoverIMG = (ImageView) itemView.findViewById(R.id.recoverIMG);
            viewIMG = (ImageView) itemView.findViewById(R.id.viewIMG);
            deleteIMG = (ImageView) itemView.findViewById(R.id.deleteIMG);

            itemNameTV = (TextView) itemView.findViewById(R.id.itemNameTV);
            benificieryNameTV = (TextView) itemView.findViewById(R.id.benificieryNameTV);
            addressTV = (TextView) itemView.findViewById(R.id.addressTV);
            deletedBYTV = (TextView) itemView.findViewById(R.id.deletedBYTV);
            imgMultiSelectIV = (CheckBox) itemView.findViewById(R.id.imgMultiSelectIV);
        }
    }
}
