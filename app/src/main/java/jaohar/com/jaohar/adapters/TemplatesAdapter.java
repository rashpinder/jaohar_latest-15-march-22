package jaohar.com.jaohar.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.TemplatesInvoicModel;
import jaohar.com.jaohar.interfaces.DeleteImagesVessels;
import jaohar.com.jaohar.interfaces.TemplatesInterFace;
import jaohar.com.jaohar.models.InvoiceDataModel;

/**
 * Created by dharmaniz on 22/3/19.
 */

public class TemplatesAdapter extends RecyclerView.Adapter<TemplatesAdapter.ViewHolder> {

    Context mContext;
    ArrayList<InvoiceDataModel> mArrayList;
    ArrayList<String> tempDeletedImages = new ArrayList<String>();
    private DeleteImagesVessels mDeleteImages;
    private TemplatesInterFace mInterFAcE;

    public TemplatesAdapter(Context mContext, ArrayList<InvoiceDataModel> mArrayList,TemplatesInterFace mInterFAcE) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
        this.mInterFAcE = mInterFAcE;


    }

    @Override
    public TemplatesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_vessel_images, parent, false);
        TemplatesAdapter.ViewHolder vH = new TemplatesAdapter.ViewHolder(v);
        return vH;
    }

    @Override
    public void onBindViewHolder(TemplatesAdapter.ViewHolder holder, final int position) {
        final InvoiceDataModel tempValues = mArrayList.get(position);
        if (tempValues.getTemplateImage().contains("http")) {
            Glide.with(mContext).load(tempValues.getTemplateImage())
                    .thumbnail(0.5f)
                    .into(holder.itemIV);
        }

     holder.itemIV.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        mInterFAcE.mTemplatesInterface(tempValues);
     }
            });
        holder.itemDeleteIV.setVisibility(View.GONE);
    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    private void loadImageFromStorage(String imgPath, ImageView imgImageView) {
        try {
            File f = new File(imgPath);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            imgImageView.setImageBitmap(b);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


    private Bitmap loadImageFromBitmap(String base64) {
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Bitmap original = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        original.compress(Bitmap.CompressFormat.PNG, 50, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));


        return decoded;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView itemDeleteIV;
        public ImageView itemIV;

        public ViewHolder(View itemView) {
            super(itemView);
            itemIV = (ImageView) itemView.findViewById(R.id.itemIV);
            itemDeleteIV = (ImageView) itemView.findViewById(R.id.itemDeleteIV);
        }
    }
}