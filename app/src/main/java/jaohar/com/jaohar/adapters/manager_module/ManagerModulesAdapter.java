package jaohar.com.jaohar.adapters.manager_module;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.ManagerModule.ManagerModulesModel;
import jaohar.com.jaohar.interfaces.managermodules.ClickManagerModulesInterface;

public class ManagerModulesAdapter extends RecyclerView.Adapter<ManagerModulesAdapter.ViewHolder> {
    private Activity mActivity;
    private List<ManagerModulesModel> modelArrayList;
    ClickManagerModulesInterface clickManagerModulesInterface;
    String BadgeCount;

    public ManagerModulesAdapter(Activity mActivity, List<ManagerModulesModel> modelArrayList, ClickManagerModulesInterface clickManagerModulesInterface,
                                 String BadgeCount) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.clickManagerModulesInterface = clickManagerModulesInterface;
        this.BadgeCount = BadgeCount;
    }

    @Override
    public ManagerModulesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_manager_module, parent, false);
        return new ManagerModulesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ManagerModulesAdapter.ViewHolder holder, final int position) {
        holder.formTV.setText(modelArrayList.get(position).getName());

        Glide.with(mActivity).load(modelArrayList.get(position).getImage()).into(holder.itemIMG);

        holder.clickLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickManagerModulesInterface.mClickManagerModulesInterface(position, modelArrayList.get(position).getName());
            }
        });

        if (!BadgeCount.equals("0") && modelArrayList.get(position).getName().equals(mActivity.getString(R.string.forum))) {
            holder.badgesTV.setVisibility(View.VISIBLE);
            holder.badgesTV.setText(BadgeCount);
        } else {
            holder.badgesTV.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView formTV, badgesTV;
        public ImageView itemIMG;
        public RelativeLayout clickLL;

        ViewHolder(View itemView) {
            super(itemView);
            badgesTV = itemView.findViewById(R.id.badgesTV);
            formTV = itemView.findViewById(R.id.formTV);
            itemIMG = itemView.findViewById(R.id.itemIMG);
            clickLL = itemView.findViewById(R.id.clickLL);
        }
    }
}
