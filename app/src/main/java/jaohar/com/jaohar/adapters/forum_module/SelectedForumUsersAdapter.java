package jaohar.com.jaohar.adapters.forum_module;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.ForumModule.ForumUserTypeModel;
import jaohar.com.jaohar.interfaces.forumModule.RemoveForumUserInterface;

public class SelectedForumUsersAdapter extends RecyclerView.Adapter<SelectedForumUsersAdapter.ViewHolder> {

    private Activity mActivity;
    ArrayList<ForumUserTypeModel> mArrayListData = new ArrayList<>();
    private ArrayList<String> SelectedItems = new ArrayList<>();
    RemoveForumUserInterface removeForumUserInterface;

    public SelectedForumUsersAdapter(Activity mActivity, ArrayList<ForumUserTypeModel> mArrayListData, RemoveForumUserInterface removeForumUserInterface) {
        this.mActivity = mActivity;
        this.mArrayListData = mArrayListData;
        this.removeForumUserInterface = removeForumUserInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_select_forum_users, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.forumUserName.setText(mArrayListData.get(position).getFirstname());

        if (mArrayListData.get(position).getImage() != null && mArrayListData.get(position).getImage().contains("http"))
            Glide.with(mActivity).load(mArrayListData.get(position).getImage()).into(holder.forumUserIV);

        holder.crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeForumUserInterface.mRemoveForumUserInterface(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayListData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView forumUserIV, crossIV;
        TextView forumUserName;

        ViewHolder(View itemView) {
            super(itemView);

            forumUserName = itemView.findViewById(R.id.forumUserName);
            forumUserIV = itemView.findViewById(R.id.forumUserIV);
            crossIV = itemView.findViewById(R.id.crossIV);
        }
    }
}