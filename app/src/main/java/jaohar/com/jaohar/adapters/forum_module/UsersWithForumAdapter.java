package jaohar.com.jaohar.adapters.forum_module;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.ForumModule.ForumUserTypeModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;

public class UsersWithForumAdapter extends RecyclerView.Adapter<UsersWithForumAdapter.ViewHolder> {

    private Activity mActivity;
    ArrayList<ForumUserTypeModel> mArrayListData = new ArrayList<>();
    ArrayList<ForumUserTypeModel> mUsersListData = new ArrayList<>();
    private ArrayList<String> SelectedItems = new ArrayList<>();

    public UsersWithForumAdapter(Activity mActivity, ArrayList<ForumUserTypeModel> mArrayListData) {
        this.mActivity = mActivity;
        this.mArrayListData = mArrayListData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_forum_users, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.forumUserName.setText(mArrayListData.get(position).getEmail());

        if (mArrayListData.get(position).getImage() != null && mArrayListData.get(position).getImage().contains("http"))
            Glide.with(mActivity).load(mArrayListData.get(position).getImage()).into(holder.forumUserIV);

        if (JaoharSingleton.getInstance().getmForumUsersList() != null) {
            SelectedItems = JaoharSingleton.getInstance().getmForumUsersList();
        }

        if (SelectedItems.contains(mArrayListData.get(position).getId())) {
            holder.tickIv.setImageResource(R.drawable.ic_tick);
            SelectedItems.add(mArrayListData.get(position).getId());
            mUsersListData.add(mArrayListData.get(position));
        } else {
            holder.tickIv.setImageResource(R.drawable.ic_circle);
            SelectedItems.remove(mArrayListData.get(position).getId());
            mUsersListData.remove(mArrayListData.get(position));
        }

        holder.tickIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SelectedItems.contains(mArrayListData.get(position).getId())) {
                    SelectedItems.remove(mArrayListData.get(position).getId());
                    mUsersListData.remove(mArrayListData.get(position));
                    holder.tickIv.setImageResource(R.drawable.ic_circle);

                } else {
                    SelectedItems.add(mArrayListData.get(position).getId());
                    mUsersListData.add(mArrayListData.get(position));
                    holder.tickIv.setImageResource(R.drawable.ic_tick);
                }
                JaoharSingleton.getInstance().setmForumUsersList(SelectedItems);
                JaoharSingleton.getInstance().setmUsersArrayList(mUsersListData);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayListData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView forumUserIV, tickIv;
        TextView forumUserName;

        ViewHolder(View itemView) {
            super(itemView);

            forumUserName = itemView.findViewById(R.id.forumUserName);
            forumUserIV = itemView.findViewById(R.id.forumUserIV);
            tickIv = itemView.findViewById(R.id.tickIv);
        }
    }
}