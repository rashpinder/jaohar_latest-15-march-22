package jaohar.com.jaohar.adapters.invoices_module;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.util.ArrayList;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.InvoiceDetailsActivity;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;

/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.ViewHolder> {
    private Activity mActivity;
    //    private ArrayList<InVoicesModel> modelArrayList;
    private OnClickInterface onClickInterface;

    public BankAdapter(Activity mActivity,OnClickInterface onClickInterface
    ) {
        this.mActivity = mActivity;
        this.onClickInterface = onClickInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bank_inv, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
//        final InVoicesModel tempValue = modelArrayList.get(position);

//        holder.inVoiceNumberTV.setText(tempValue.getInvoice_number());
//
//        if (tempValue.getmCompaniesModel() != null) {
//            holder.inVoiceCompanyNameTV.setText(html2text(tempValue.getmCompaniesModel().getCompany_name()));
//        }
//
//        holder.currencyNameTV.setText(tempValue.getInvoice_number());
//        String strDateFormat = null;
//
//
//        try {
//            strDateFormat = Utilities.gettingFormatTime(tempValue.getInvoice_date().replace("_", " "));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        holder.currencyCodeTV.setText(strDateFormat);
//
//        holder.inVoiceDateTV.setText(strDateFormat);
//
//        if (tempValue.getmVesselSearchInvoiceModel() != null) {
//            holder.addressTV.setText(html2text(tempValue.getmVesselSearchInvoiceModel().getVessel_name()));
//        }
//
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                JaoharConstants.Invoice_ID = tempValue.getInvoice_id();
//                Intent mIntent = new Intent(mActivity, InvoiceDetailsActivity.class);
//                mIntent.putExtra("Model", tempValue);
//                mActivity.startActivity(mIntent);
//            }
//        });
//
//        holder.layoutItemRL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                JaoharConstants.Invoice_ID = tempValue.getInvoice_id();
//                Intent mIntent = new Intent(mActivity, InvoiceDetailsActivity.class);
//                mIntent.putExtra("Model", tempValue);
//                mActivity.startActivity(mIntent);
//            }
//        });
//
        holder.optionsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickInterface.mOnClickInterface(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView currencyNameTV, currencyCodeTV, addressTV,emaillTV,DateTV;
        public ImageView stampIV,optionsIV;

        private TextView inVoiceNumberTV, inVoiceCompanyNameTV, inVoiceDateTV;

        ViewHolder(View itemView) {
            super(itemView);

            optionsIV = itemView.findViewById(R.id.optionsIV);
            stampIV = itemView.findViewById(R.id.stampIV);
            currencyNameTV = itemView.findViewById(R.id.currencyNameTV);
            DateTV = itemView.findViewById(R.id.DateTV);
            emaillTV = itemView.findViewById(R.id.emaillTV);
            currencyCodeTV = itemView.findViewById(R.id.currencyCodeTV);
            addressTV = itemView.findViewById(R.id.addressTV);
            inVoiceDateTV = itemView.findViewById(R.id.inVoiceDateTV);
            inVoiceNumberTV = itemView.findViewById(R.id.inVoiceNumberTV);
            inVoiceCompanyNameTV = itemView.findViewById(R.id.inVoiceCompanyNameTV);
        }
    }

    //for converting html to text
    public CharSequence html2text(String html) {
        CharSequence trimmed = noTrailingwhiteLines(Html.fromHtml(html.replace("\n", "<br />")));
        return trimmed;
    }

    //for hiding extra white space
    private CharSequence noTrailingwhiteLines(CharSequence text) {

        if (text.length() > 0)
            while (text.charAt(text.length() - 1) == '\n') {
                text = text.subSequence(0, text.length() - 1);
            }
        return text;
    }
}