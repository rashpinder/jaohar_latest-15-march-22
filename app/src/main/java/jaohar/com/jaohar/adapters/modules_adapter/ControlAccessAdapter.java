package jaohar.com.jaohar.adapters.modules_adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import java.util.ArrayList;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.staff_module.UserDetailAdminModel;
import jaohar.com.jaohar.interfaces.modules.SendMultipleUserInterface;


public class ControlAccessAdapter extends RecyclerView.Adapter<ControlAccessAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<UserDetailAdminModel> modelArrayList;
    SendMultipleUserInterface mInterfaceMultiSelect;

    public ControlAccessAdapter(Activity mActivity, ArrayList<UserDetailAdminModel> modelArrayList, SendMultipleUserInterface mInterfaceMultiSelect) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mInterfaceMultiSelect = mInterfaceMultiSelect;
    }

    @Override
    public ControlAccessAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_list_admin, parent, false);
        return new ControlAccessAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ControlAccessAdapter.ViewHolder holder, final int position) {
        final UserDetailAdminModel tempValue = modelArrayList.get(position);

        holder.emailTV.setText(tempValue.getEmail());
        holder.roleTV.setText(tempValue.getRole());

        /*
         * Check and un check according to API Responce
         * */
        holder.imgMultiSelectIV.setChecked(tempValue.getSelected());

         /*
         * Check and un check according to API Responce
         * */
        if (holder.imgMultiSelectIV.isChecked()) {
            tempValue.setSelected(true);
            mInterfaceMultiSelect.mSendMultipleUserInterface(tempValue, false, position);
        } else {
            tempValue.setSelected(false);
            mInterfaceMultiSelect.mSendMultipleUserInterface(tempValue, true, position);
        }


        holder.imgMultiSelectIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.imgMultiSelectIV.isChecked()) {
                    tempValue.setSelected(true);
                    mInterfaceMultiSelect.mSendMultipleUserInterface(tempValue, false, position);
                } else {
                    tempValue.setSelected(false);
                    mInterfaceMultiSelect.mSendMultipleUserInterface(tempValue, true, position);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView emailTV, roleTV;
        CheckBox imgMultiSelectIV;

        ViewHolder(View itemView) {
            super(itemView);
            emailTV =  itemView.findViewById(R.id.emailTV);
            roleTV =  itemView.findViewById(R.id.roleTV);
            imgMultiSelectIV =  itemView.findViewById(R.id.imgMultiSelectIV);
        }
    }
}
