package jaohar.com.jaohar.adapters.notification_module;

import android.app.Activity;
import android.content.Intent;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vincent.filepicker.Constant;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.NotificationDetailsActivity;
import jaohar.com.jaohar.activities.forum_module.ChatScreenForumActivity;
import jaohar.com.jaohar.activities.forum_module.UKOfficeChatScreenForumActivity;
import jaohar.com.jaohar.activities.forum_module_admin.ChatScreenAdminActivity;
import jaohar.com.jaohar.activities.forum_module_admin.ChatScreenUkOfficeAdminActivity;
import jaohar.com.jaohar.beans.notification_module.NotificationModel;
import jaohar.com.jaohar.fragments.HomeFragment;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<NotificationModel> modelArrayList;
    PaginationListForumAdapter mPaginationInterFace;
    int count = 0;
    String strUserRole = "";

    public NotificationAdapter(Activity mActivity, ArrayList<NotificationModel> modelArrayList, PaginationListForumAdapter mPaginationInterFace, String strUserRole) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mPaginationInterFace = mPaginationInterFace;
        this.strUserRole = strUserRole;
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_nototifcation_list, parent, false);
        return new NotificationAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.ViewHolder holder, final int position) {
        final NotificationModel tempValue = modelArrayList.get(position);


//     pagination for smooth scrooling
        if (position >= modelArrayList.size() - 1) {
            mPaginationInterFace.mPaginationforVessels(true);
        }

        if (tempValue.getNotify_type().equals("staff forum")) {
            String sourceString = "<b>" + tempValue.getUser_name() + "</b> " + " has posted a new message in " + "<b>" + tempValue.getForum_name();
            holder.messageTV.setText(Html.fromHtml(sourceString));
        } else if (tempValue.getNotify_type().equals("forum") || tempValue.getNotify_type().equals("uk_forum")) {
            String sourceString = "<b>" + tempValue.getUser_name() + "</b> " + " has posted a new message in " + "<b>" + tempValue.getForum_name();
            holder.messageTV.setText(Html.fromHtml(sourceString));
        } else {
            holder.messageTV.setText(tempValue.getNotify_message());
        }

        long unix_seconds = Long.parseLong(modelArrayList.get(position).getNotify_date());

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(unix_seconds * 1000L);

        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String date = format.format(cal.getTime());

        DateFormat formattime = new SimpleDateFormat("hh:mm");
        String time = formattime.format(cal.getTime());

        String date_time = date + " at " + time;

        holder.timeTV.setText(date_time);

        if (!tempValue.getUser_image().equals("")) {
            Glide.with(mActivity)
                    .load(tempValue.getUser_image())
                    .into(holder.picIMG);
        }

        holder.itemLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tempValue.getNotify_type().equals("staff forum")) {
                    if (strUserRole.equals("admin")) {
//                        Intent mIntent = new Intent(mActivity, ChatScreenAdminActivity.class);
//                        mIntent.putExtra("forum_id", tempValue.getForum_id());
//                        mIntent.putExtra("type", "staffAdmin");
//                        mActivity.startActivity(mIntent);
                    } else {
                        Intent mIntent = new Intent(mActivity, ChatScreenForumActivity.class);
                        mIntent.putExtra("forum_id", tempValue.getForum_id());
                        mActivity.startActivity(mIntent);
                    }

                } else if (tempValue.getNotify_type().equals("forum") || tempValue.getNotify_type().equals("uk_forum")) {
                    if (strUserRole.equals("admin")) {
                        Intent mIntent = new Intent(mActivity, ChatScreenUkOfficeAdminActivity.class);
                        mIntent.putExtra("forum_id", tempValue.getForum_id());
                        mIntent.putExtra("type", "staffAdmin");
                        mActivity.startActivity(mIntent);
                    } else {
                        Intent mIntent = new Intent(mActivity, UKOfficeChatScreenForumActivity.class);
                        mIntent.putExtra("forum_id", tempValue.getForum_id());
                        mActivity.startActivity(mIntent);
                    }
                } else {
                    mActivity.startActivity(new Intent(mActivity, NotificationDetailsActivity.class)
                    .putExtra("Notification",tempValue.getNotify_message()));
//                    HomeActivity.txtCenter.setText(mActivity.getString(R.string.home));
//                    Utilities.setStatusBarTransparent(mActivity);
//                    HomeActivity.homeToolbar();
//                    HomeActivity.mainRL.setBackground(mActivity.getResources().getDrawable(R.drawable.home_bg));
//                    HomeActivity.txtCenter.setVisibility(View.GONE);
//                    HomeActivity.toolBarMainLL.setBackgroundColor(mActivity.getResources().getColor(R.color.trasperant));
//                    HomeActivity.switchFragment((FragmentActivity) mActivity, new HomeFragment(), JaoharConstants.HOME_FRAGMENT, false, null);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView messageTV, timeTV;
        public ImageView picIMG;
        public LinearLayout itemLL;

        ViewHolder(View itemView) {
            super(itemView);
            messageTV = (TextView) itemView.findViewById(R.id.messageTV);
            timeTV = (TextView) itemView.findViewById(R.id.timeTV);
            picIMG = (ImageView) itemView.findViewById(R.id.picIMG);
            itemLL = (LinearLayout) itemView.findViewById(R.id.itemLL);
        }
    }
}

