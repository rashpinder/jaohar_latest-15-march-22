package jaohar.com.jaohar.adapters.invoices_module;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.OnClickInterface;

/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class InvVesselAdapter extends RecyclerView.Adapter<InvVesselAdapter.ViewHolder> {
    private Activity mActivity;
        private ArrayList<VesselSearchInvoiceModel> vesselArrayList;
    private OnClickInterface onClickInterface;

    public InvVesselAdapter(Activity mActivity, ArrayList<VesselSearchInvoiceModel> vesselArrayList, OnClickInterface onClickInterface) {
        this.mActivity = mActivity;
        this.vesselArrayList = vesselArrayList;
        this.onClickInterface = onClickInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_inv_vessel, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final VesselSearchInvoiceModel tempValue = vesselArrayList.get(position);

        holder.vesselNameTV.setText(tempValue.getVessel_name());
        holder.imoNoTV.setText(tempValue.getIMO_no());
        holder.flagTV.setText(tempValue.getFlag());
        holder.emailTV.setText(tempValue.getVessel_id());
        holder.dateTV.setText(tempValue.getVessel_id());

        holder.optionsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickInterface.mOnClickInterface(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView vesselNameTV, emailTV, flagTV,dateTV,imoNoTV;
        public ImageView stampIV,optionsIV;

        private TextView inVoiceNumberTV, inVoiceCompanyNameTV, inVoiceDateTV;

        ViewHolder(View itemView) {
            super(itemView);

            optionsIV = itemView.findViewById(R.id.optionsIV);
            stampIV = itemView.findViewById(R.id.stampIV);
            vesselNameTV = itemView.findViewById(R.id.vesselNameTV);
            imoNoTV = itemView.findViewById(R.id.imoNoTV);
            dateTV = itemView.findViewById(R.id.dateTV);
            emailTV = itemView.findViewById(R.id.emailTV);
            flagTV = itemView.findViewById(R.id.flagTV);
            inVoiceDateTV = itemView.findViewById(R.id.inVoiceDateTV);
            inVoiceNumberTV = itemView.findViewById(R.id.inVoiceNumberTV);
            inVoiceCompanyNameTV = itemView.findViewById(R.id.inVoiceCompanyNameTV);
        }
    }
//
//    //for converting html to text
//    public CharSequence html2text(String html) {
//        CharSequence trimmed = noTrailingwhiteLines(Html.fromHtml(html.replace("\n", "<br />")));
//        return trimmed;
//    }
//
//    //for hiding extra white space
//    private CharSequence noTrailingwhiteLines(CharSequence text) {
//
//        if (text.length() > 0)
//            while (text.charAt(text.length() - 1) == '\n') {
//                text = text.subSequence(0, text.length() - 1);
//            }
//        return text;
//    }
}