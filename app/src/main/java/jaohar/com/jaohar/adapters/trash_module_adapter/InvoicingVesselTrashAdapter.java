package jaohar.com.jaohar.adapters.trash_module_adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import jaohar.com.jaohar.interfaces.TrashModuleInterface.RD_Inv_Del_Recover_VesselTrashInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RD_Inv_VesselTrashSelectedInterface;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.models.SearchVesselData;


public class InvoicingVesselTrashAdapter extends RecyclerView.Adapter<InvoicingVesselTrashAdapter.ViewHolder> {
    Activity mActivity;
    ArrayList<SearchVesselData> modelArrayList;
    RD_Inv_VesselTrashSelectedInterface mRecoverDeleteSelected;
    RD_Inv_Del_Recover_VesselTrashInterface mrecoverDelete;
    private static boolean[] checkBoxState = null;
    private static HashMap<SearchVesselData, Boolean> checkedForModel = new HashMap<>();

    public InvoicingVesselTrashAdapter(Activity mActivity, ArrayList<SearchVesselData> modelArrayList, RD_Inv_VesselTrashSelectedInterface mRecoverDeleteSelected, RD_Inv_Del_Recover_VesselTrashInterface mrecoverDelete) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mRecoverDeleteSelected = mRecoverDeleteSelected;
        this.mrecoverDelete = mrecoverDelete;
        checkBoxState=new boolean[modelArrayList.size()];
    }

    @NonNull
    @Override
    public InvoicingVesselTrashAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inv_vessel_trash, parent, false);
        return new InvoicingVesselTrashAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final InvoicingVesselTrashAdapter.ViewHolder holder, final int position) {
        final SearchVesselData temValue = modelArrayList.get(position);
        holder.itemVesselNameTV.setText(temValue.getVesselName());
        holder.imo_NumberTV.setText(temValue.getIMONo());
        holder.flagTV.setText(temValue.getFlag());
        holder.deletedBYTV.setText(temValue.getDeleted_by());

        holder.recoverIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mrecoverDelete.RD_Inv_RecoverVesselTrashInterface(temValue,"recover");
            }
        });
        holder.deleteIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mrecoverDelete.RD_Inv_RecoverVesselTrashInterface(temValue,"delete");
            }
        });

        holder.imgMultiSelectIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (
                    holder.imgMultiSelectIV.isChecked()) {
                    checkBoxState[position] = true;
                    ischecked(position,true);
                    mRecoverDeleteSelected.mRD_Inv_VesselTrashInterFace(temValue,false);
                } else {
                    checkBoxState[position] = false;
                    ischecked(position,false);
                    mRecoverDeleteSelected.mRD_Inv_VesselTrashInterFace(temValue,true);

                }

            }
        });

        /*if country is in checkedForCountry then set the checkBox to true */
        if (checkedForModel.get(temValue) != null) {
            holder.imgMultiSelectIV.setChecked(checkedForModel.get(temValue));
        }

        /*Set tag to all checkBox*/
        holder.imgMultiSelectIV.setTag(temValue);
    }


    private void ischecked(int position,boolean flag )
    {
        checkedForModel.put(this.modelArrayList.get(position), flag);
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView recoverIMG, deleteIMG;
        TextView itemVesselNameTV, imo_NumberTV, flagTV, deletedBYTV;
        CheckBox imgMultiSelectIV;

        public ViewHolder(View itemView) {
            super(itemView);
            recoverIMG = itemView.findViewById(R.id.recoverIMG);
            deleteIMG = itemView.findViewById(R.id.deleteIMG);
            itemVesselNameTV = itemView.findViewById(R.id.itemVesselNameTV);
            imo_NumberTV = itemView.findViewById(R.id.imo_NumberTV);
            flagTV = itemView.findViewById(R.id.flagTV);
            deletedBYTV = itemView.findViewById(R.id.deletedBYTV);
            imgMultiSelectIV = itemView.findViewById(R.id.imgMultiSelectIV);
        }
    }
}
