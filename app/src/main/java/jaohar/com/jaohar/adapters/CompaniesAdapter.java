package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.CompaniesModel;
import jaohar.com.jaohar.interfaces.DeleteCompanyInterface;
import jaohar.com.jaohar.interfaces.EditCompanyInterface;

/**
 * Created by Dharmani Apps on 1/15/2018.
 */

public class CompaniesAdapter extends RecyclerView.Adapter<CompaniesAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<CompaniesModel> modelArrayList;
    private DeleteCompanyInterface mDeleteCompanyInterface;
    private EditCompanyInterface mEditCompanyInterface;

    public CompaniesAdapter(Activity mActivity, ArrayList<CompaniesModel> modelArrayList,DeleteCompanyInterface mDeleteCompanyInterface,EditCompanyInterface mEditCompanyInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDeleteCompanyInterface = mDeleteCompanyInterface;
        this.mEditCompanyInterface = mEditCompanyInterface;
    }

    @Override
    public CompaniesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_companies, null);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(CompaniesAdapter.ViewHolder holder, int position) {
        final CompaniesModel tempValue = modelArrayList.get(position);
        holder.item_Title.setText(tempValue.getCompany_name());

        holder.item_Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", tempValue);
                mActivity.setResult(222, returnIntent);
                mActivity.finish();
            }
        });
        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             mEditCompanyInterface.editCompany(tempValue);
            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              mDeleteCompanyInterface.deleteCompany(tempValue, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title,txtEdit,txtDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
        }
    }
}
