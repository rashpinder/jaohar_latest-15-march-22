package jaohar.com.jaohar.adapters.manager_module;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.ManagerModule.ManagerModulesModel;
import jaohar.com.jaohar.interfaces.managermodules.ClickManagerModulesInterface;

public class DynamicAdditionsAdapter extends RecyclerView.Adapter<DynamicAdditionsAdapter.ViewHolder> {
    private Activity mActivity;
    private List<ManagerModulesModel> modelArrayList;
    ClickManagerModulesInterface clickManagerModulesInterface;

    public DynamicAdditionsAdapter(Activity mActivity, List<ManagerModulesModel> modelArrayList, ClickManagerModulesInterface clickManagerModulesInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.clickManagerModulesInterface = clickManagerModulesInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dynamic_additions, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.formTV.setText(modelArrayList.get(position).getName());

        Glide.with(mActivity).load(modelArrayList.get(position).getImage()).into(holder.itemIMG);

        holder.clickLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickManagerModulesInterface.mClickManagerModulesInterface(position, modelArrayList.get(position).getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView formTV;
        public ImageView itemIMG;
        public RelativeLayout clickLL;

        ViewHolder(View itemView) {
            super(itemView);
            formTV = itemView.findViewById(R.id.formTV);
            itemIMG = itemView.findViewById(R.id.itemIMG);
            clickLL = itemView.findViewById(R.id.clickLL);
        }
    }
}
