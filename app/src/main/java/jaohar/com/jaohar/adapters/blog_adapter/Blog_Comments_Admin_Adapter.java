package jaohar.com.jaohar.adapters.blog_adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Vibrator;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.blog_module.Blog_Details_Model;
import jaohar.com.jaohar.beans.blog_module.Blog_Replies_Model;
import jaohar.com.jaohar.interfaces.blog_module.BlogListClickInterface;
import jaohar.com.jaohar.interfaces.blog_module.BlogRepliesdelInterface;
import jaohar.com.jaohar.interfaces.blog_module.DeleteBlogCommentsInterface;

public class Blog_Comments_Admin_Adapter extends RecyclerView.Adapter<Blog_Comments_Admin_Adapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<Blog_Details_Model> modelArrayList;
    BlogListClickInterface mInterfaceData;
    DeleteBlogCommentsInterface mDeleteInterface;
    Vibrator vibe;
    String strUserID = "";
    BlogRepliesdelInterface mDeleteRepliesInterface;

    public Blog_Comments_Admin_Adapter(Activity mActivity, ArrayList<Blog_Details_Model> modelArrayList, BlogListClickInterface mInterfaceData, DeleteBlogCommentsInterface mDeleteInterface, BlogRepliesdelInterface mDeleteRepliesInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mInterfaceData = mInterfaceData;
        this.mDeleteInterface = mDeleteInterface;
        this.mDeleteRepliesInterface = mDeleteRepliesInterface;

    }

    @Override
    public Blog_Comments_Admin_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_blog_comments, parent, false);
        return new Blog_Comments_Admin_Adapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final Blog_Comments_Admin_Adapter.ViewHolder holder, final int position) {
        final Blog_Details_Model tempValue = modelArrayList.get(position);
        vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);

        holder.deleteTV.setVisibility(View.VISIBLE);


        holder.messageTV.setText(tempValue.getMessage());

        long unix_seconds = Long.parseLong(tempValue.getPosted_on());
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(unix_seconds * 1000L);
        DateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
        String date = format.format(cal.getTime());

        holder.nameTV.setText(tempValue.getmUser_Detail_Model().getFirst_name());

        holder.dateTV.setText(date);


        //      holder.postedTimeTV.setText(" " +date);

        if (!tempValue.getmUser_Detail_Model().getImage().equals("")) {
            Glide.with(mActivity)
                    .load(tempValue.getmUser_Detail_Model().getImage())
                    .into(holder.profileMainImg);
        }


        holder.replyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterfaceData.BlogInterface(tempValue, "reply");
            }
        });

        holder.deleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterfaceData.BlogInterface(tempValue, "delete");
            }
        });
//        holder.mainLL.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                vibe.vibrate(150);
//                mDeleteInterface.mDeleteBlogInterface(tempValue, v);
//                return true;
//            }
//        });


        /* setUpInside Recycler view in Adapter*/
        if (tempValue.getmBlogRepliesModel() != null) {
            holder.recyclerDataRL.setVisibility(View.VISIBLE);
            setUpAdapter(holder.replyCommentRV, tempValue.getmBlogRepliesModel());
        } else {
            holder.recyclerDataRL.setVisibility(View.GONE);
        }

    }

    private void setUpAdapter(RecyclerView replyCommentRV, ArrayList<Blog_Replies_Model> blog_replies_models) {
        Blog_Replies_Admin_Adapter mAdapter;
        replyCommentRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setAutoMeasureEnabled(true);
        replyCommentRV.setNestedScrollingEnabled(false);
        replyCommentRV.setHasFixedSize(false);
        replyCommentRV.setLayoutManager(layoutManager);
        mAdapter = new Blog_Replies_Admin_Adapter(mActivity, blog_replies_models, mDeleteRepliesInterface);

        replyCommentRV.setAdapter(mAdapter);
    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV, messageTV, replyTV, deleteTV, dateTV;
        public de.hdodenhof.circleimageview.CircleImageView profileMainImg;
        public RecyclerView replyCommentRV;
        public RelativeLayout recyclerDataRL;
        public LinearLayout mainLL;

        ViewHolder(View itemView) {
            super(itemView);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            messageTV = (TextView) itemView.findViewById(R.id.messageTV);
            replyTV = (TextView) itemView.findViewById(R.id.replyTV);
            deleteTV = (TextView) itemView.findViewById(R.id.deleteTV);
            profileMainImg = itemView.findViewById(R.id.profileMainImg);
            replyCommentRV = itemView.findViewById(R.id.replyCommentRV);
            recyclerDataRL = itemView.findViewById(R.id.recyclerDataRL);
            mainLL = itemView.findViewById(R.id.mainLL);
            dateTV = itemView.findViewById(R.id.dateTV);
        }
    }
}