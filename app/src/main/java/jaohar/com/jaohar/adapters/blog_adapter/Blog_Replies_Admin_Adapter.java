package jaohar.com.jaohar.adapters.blog_adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.blog_module.Blog_Replies_Model;
import jaohar.com.jaohar.interfaces.blog_module.BlogRepliesdelInterface;

public class Blog_Replies_Admin_Adapter extends RecyclerView.Adapter<Blog_Replies_Admin_Adapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<Blog_Replies_Model> modelArrayList;
    String strUserID = "";
    BlogRepliesdelInterface mDeleteRepliesInterface;
    public Blog_Replies_Admin_Adapter(Activity mActivity, ArrayList<Blog_Replies_Model> modelArrayList,BlogRepliesdelInterface mDeleteRepliesInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDeleteRepliesInterface = mDeleteRepliesInterface;

    }

    @Override
    public Blog_Replies_Admin_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_blog_comments, parent, false);
        return new Blog_Replies_Admin_Adapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final Blog_Replies_Admin_Adapter.ViewHolder holder, final int position) {
        final Blog_Replies_Model tempValue = modelArrayList.get(position);



        // Maintain check to hide or show delete for users

            holder.deleteTV.setVisibility(View.VISIBLE);



        holder.replyCommentRV.setVisibility(View.GONE);
        holder.replyTV.setVisibility(View.GONE);

//        holder.viewVL.setVisibility(View.GONE);
        holder.recyclerDataRL.setVisibility(View.GONE);

        long unix_seconds = Long.parseLong(tempValue.getPosted_on());

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(unix_seconds * 1000L);

//                      DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        DateFormat format = new SimpleDateFormat("hh:mm a");

        String date = format.format(cal.getTime());
        holder.nameTV.setText(tempValue.getmUser_Detail_Model().getFirst_name() + " " + date);
        holder.messageTV.setText(tempValue.getMessage());
//        long unix_seconds = Long.parseLong(tempValue.getCreation_date());
//
//        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
//        cal.setTimeInMillis(unix_seconds * 1000L);
//
//        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
//        String date = format.format(cal.getTime());
//        holder.postedTimeTV.setText(" " +date);

        if (!tempValue.getmUser_Detail_Model().getImage().equals("")) {
            Glide.with(mActivity)
                    .load(tempValue.getmUser_Detail_Model().getImage())
                    .into(holder.profileMainImg);
        }

        holder.deleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDeleteRepliesInterface.mBlogRepliesInterface(tempValue);
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV, messageTV, replyTV, deleteTV;
        public de.hdodenhof.circleimageview.CircleImageView profileMainImg;
        public RecyclerView replyCommentRV;
//        public View viewVL;
        public RelativeLayout recyclerDataRL;
        public LinearLayout reply_DelLL;


        ViewHolder(View itemView) {
            super(itemView);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            messageTV = (TextView) itemView.findViewById(R.id.messageTV);
            replyTV = (TextView) itemView.findViewById(R.id.replyTV);
            deleteTV = itemView.findViewById(R.id.deleteTV);
            profileMainImg = itemView.findViewById(R.id.profileMainImg);
            replyCommentRV = itemView.findViewById(R.id.replyCommentRV);
//            viewVL = itemView.findViewById(R.id.viewVL);
            recyclerDataRL = itemView.findViewById(R.id.recyclerDataRL);
            reply_DelLL = itemView.findViewById(R.id.reply_DelLL);
        }
    }

}