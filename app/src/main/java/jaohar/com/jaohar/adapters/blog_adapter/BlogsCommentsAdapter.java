package jaohar.com.jaohar.adapters.blog_adapter;

import android.app.Activity;

import android.content.Context;
import android.os.Vibrator;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.blog_module.Blog_Details_Model;
import jaohar.com.jaohar.beans.blog_module.Blog_Replies_Model;
import jaohar.com.jaohar.interfaces.blog_module.BlogListClickInterface;
import jaohar.com.jaohar.interfaces.blog_module.BlogRepliesdelInterface;
import jaohar.com.jaohar.interfaces.blog_module.DeleteBlogCommentsInterface;
import jaohar.com.jaohar.utils.JaoharPreference;


public class BlogsCommentsAdapter extends RecyclerView.Adapter<BlogsCommentsAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<Blog_Details_Model> modelArrayList;
    BlogListClickInterface mInterfaceData;
    DeleteBlogCommentsInterface mDeleteInterface;
    Vibrator vibe;
    String strUserID = "";
    BlogRepliesdelInterface mDeleteRepliesInterface;

    public BlogsCommentsAdapter(Activity mActivity, ArrayList<Blog_Details_Model> modelArrayList, BlogListClickInterface mInterfaceData, DeleteBlogCommentsInterface mDeleteInterface, BlogRepliesdelInterface mDeleteRepliesInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mInterfaceData = mInterfaceData;
        this.mDeleteInterface = mDeleteInterface;
        this.mDeleteRepliesInterface = mDeleteRepliesInterface;

    }

    @Override
    public BlogsCommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_blog_comments, parent, false);
        return new BlogsCommentsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final BlogsCommentsAdapter.ViewHolder holder, final int position) {
        final Blog_Details_Model tempValue = modelArrayList.get(position);
        vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);

        if (JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "").equals("")) {
            strUserID = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else {
            strUserID = JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "");
        }

        // Maintain check to hide or show delete for users
        if (tempValue.getmUser_Detail_Model() != null) {
            if (tempValue.getmUser_Detail_Model().getId() != null && tempValue.getmUser_Detail_Model().getId().equals(strUserID)) {
                holder.deleteTV.setVisibility(View.VISIBLE);
            } else {
                holder.deleteTV.setVisibility(View.GONE);
            }

            if (tempValue.getmUser_Detail_Model().getFirst_name() != null)
                holder.nameTV.setText(tempValue.getmUser_Detail_Model().getFirst_name() + " " + tempValue.getmUser_Detail_Model().getLast_name());

            if (tempValue.getmUser_Detail_Model().getImage() != null)
                if (!tempValue.getmUser_Detail_Model().getImage().equals("")) {
                    Glide.with(mActivity)
                            .load(tempValue.getmUser_Detail_Model().getImage())
                            .into(holder.profileMainImg);
                }
        } else {
            holder.deleteTV.setVisibility(View.GONE);
            holder.nameTV.setText(tempValue.getPosted_by());
            Glide.with(mActivity)
                    .load(R.drawable.placeholder)
                    .into(holder.profileMainImg);
        }

        holder.messageTV.setText(tempValue.getMessage());

        long unix_seconds = Long.parseLong(tempValue.getPosted_on());
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(unix_seconds * 1000L);
        DateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
        String date = format.format(cal.getTime());


        holder.dateTV.setText(date);
        //      holder.postedTimeTV.setText(" " +date);


        holder.replyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterfaceData.BlogInterface(tempValue, "reply");
            }
        });

        holder.deleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterfaceData.BlogInterface(tempValue, "delete");
            }
        });
//        holder.mainLL.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                vibe.vibrate(150);
//                mDeleteInterface.mDeleteBlogInterface(tempValue, v);
//                return true;
//            }
//        });


        /* setUpInside Recycler view in Adapter*/
        if (tempValue.getmBlogRepliesModel() != null) {
            holder.recyclerDataRL.setVisibility(View.VISIBLE);
            setUpAdapter(holder.replyCommentRV, tempValue.getmBlogRepliesModel());
        } else {
            holder.recyclerDataRL.setVisibility(View.GONE);
        }

    }

    private void setUpAdapter(RecyclerView replyCommentRV, ArrayList<Blog_Replies_Model> blog_replies_models) {
        BlogRepliesAdapter mAdapter;
        replyCommentRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setAutoMeasureEnabled(true);
        replyCommentRV.setNestedScrollingEnabled(false);
        replyCommentRV.setHasFixedSize(false);
        replyCommentRV.setLayoutManager(layoutManager);
        mAdapter = new BlogRepliesAdapter(mActivity, blog_replies_models, mDeleteRepliesInterface);

        replyCommentRV.setAdapter(mAdapter);
    }

//    public void setUpAdapter(){
//        replyCommentRV.setNestedScrollingEnabled(false);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//        layoutManager.setAutoMeasureEnabled(true);
//        mRecyclerView.setNestedScrollingEnabled(false);
//        mRecyclerView.setHasFixedSize(false);
//        mRecyclerView.setLayoutManager(layoutManager);
//        mInVoiceItemsAdapter = new InVoiceItemsAdapter(mActivity, itemsArrayList, mDeleteItemInvoiceInterface, mEditInvoiceItemData);
//        mRecyclerView.setAdapter(mInVoiceItemsAdapter);
//    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV, messageTV, replyTV, deleteTV, dateTV;
        public de.hdodenhof.circleimageview.CircleImageView profileMainImg;
        public RecyclerView replyCommentRV;
        public RelativeLayout recyclerDataRL;
        public LinearLayout mainLL;

        ViewHolder(View itemView) {
            super(itemView);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            messageTV = (TextView) itemView.findViewById(R.id.messageTV);
            replyTV = (TextView) itemView.findViewById(R.id.replyTV);
            deleteTV = (TextView) itemView.findViewById(R.id.deleteTV);
            profileMainImg = itemView.findViewById(R.id.profileMainImg);
            replyCommentRV = itemView.findViewById(R.id.replyCommentRV);
            recyclerDataRL = itemView.findViewById(R.id.recyclerDataRL);
            mainLL = itemView.findViewById(R.id.mainLL);
            dateTV = itemView.findViewById(R.id.dateTV);
        }
    }
}