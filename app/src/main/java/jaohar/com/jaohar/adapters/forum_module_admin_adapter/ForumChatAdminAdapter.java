package jaohar.com.jaohar.adapters.forum_module_admin_adapter;

import static jaohar.com.jaohar.beans.ForumModule.ForumChatModel.Receiver;
import static jaohar.com.jaohar.beans.ForumModule.ForumChatModel.Sender;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.GalleryActivity;
import jaohar.com.jaohar.beans.ForumModule.ForumChatModel;
import jaohar.com.jaohar.interfaces.forum_module_admin.Add_Del_Edit_forumchatInterfaceAdmin;
import jaohar.com.jaohar.utils.Utilities;

public class ForumChatAdminAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    //    PaginationListForumAdapter mPaginationInterface;
    private Context context;
    int lastItemPosition = -1;
    private ArrayList<ForumChatModel> modelArrayList;
    private ArrayList<String> mImageArryList = new ArrayList<String>();
    Add_Del_Edit_forumchatInterfaceAdmin mAdd_Del_EditInteface;
    Vibrator vibe;
    String strType;

    public ForumChatAdminAdapter(Context context, ArrayList<ForumChatModel> modelArrayList, Add_Del_Edit_forumchatInterfaceAdmin mAdd_Del_EditInteface, String strType) {
        this.context = context;
        this.modelArrayList = modelArrayList;
        this.mAdd_Del_EditInteface = mAdd_Del_EditInteface;
        this.strType = strType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case Receiver:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_left_chat, parent, false);
                return new RecieverViewHolder(view);
            case Sender:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_right_chat, parent, false);
                return new SenderViewHolder(view);
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ForumChatModel object = modelArrayList.get(position);
//        holder.setIsRecyclable(false);
        vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (position > lastItemPosition) {
            // Scrolled Down
        } else {
//            mPaginationInterface.mPaginationforVessels(true);
            // Scrolled Up
        }
        lastItemPosition = position;
        if (object != null) switch (object.getIntType()) {
            /**************
             * TYPE_HEADER CASE
             * ****************/
            case Receiver:
//                      imagePicRight ,unReplyViewLL
                if (object.getReplyForModel() != null) {
                    ((RecieverViewHolder) holder).unReplyViewLL.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).replyLL.setVisibility(View.VISIBLE);
                    ((RecieverViewHolder) holder).ReplyViewLL.setVisibility(View.VISIBLE);
                    ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                    ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).imagePicRight.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).rightMessgeTv.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).leftChatReplyTV.setText(object.getReplyForModel().getMessage());
                    ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setText(object.getMessage());
                    ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setText(object.getMessage());

                    if (object.getImage_count().equals("1")) {
                        ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.GONE);


                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemReplyPICIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemReplyPICIV);
                            }

                        } else {
                            ((RecieverViewHolder) holder).itemReplyPICIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                    } else if (object.getImage_count().equals("2")) {
                        ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).image2ReplyPicLL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.GONE);

                        // For Reply Pic Layout for two image shown
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageReplyPic22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic22IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // For Reply Pic Layout for two image shown
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageReplyPic21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage2()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic21IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                    } else if (object.getImage_count().equals("3")) {
                        ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).image2ReplyPicLL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).image3ReplyPicLL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.GONE);
                        // For Reply Pic Layout for Three image shown
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic31IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // For Reply Pic Layout for Three image shown
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage2()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic32IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // For Reply Pic Layout for Three image shown
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((RecieverViewHolder) holder).itemImageReplyPic33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage3()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic33IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                    } else if (object.getImage_count().equals("4")) {
                        ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).image2ReplyPicLL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).image3ReplyPicLL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.GONE);

                        // For Reply Pic Layout for Four image shown
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic41IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // For Reply Pic Layout for Four image shown
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage2()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic42IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // For Reply Pic Layout for Four image shown
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage3()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic43IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // For Reply Pic Layout for Four image shown
                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage4()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic44IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                    } else if (Integer.parseInt(object.getImage_count()) >= 5) {
                        ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).image2ReplyPicLL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).image3ReplyPicLL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).blurReplyPicRL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.GONE);

                        int imageCount = Integer.parseInt(object.getImage_count()) - 4;
                        String strImageCount = "+" + imageCount;
                        ((RecieverViewHolder) holder).textCountReplyPicTv.setText(strImageCount);

                        // For Reply Pic Layout for Four image shown
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic41IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // For Reply Pic Layout for Four image shown
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage2()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic42IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // For Reply Pic Layout for Four image shown
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage3()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic43IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // For Reply Pic Layout for Four image shown
                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage4()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageReplyPic44IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                    } else {
                        ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).image2ReplyPicLL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).image3ReplyPicLL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).imageReplyPic4LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).blurReplyPicRL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.VISIBLE);
                    }

                    if (!object.getReplyForModel().getImage_count().equals("")) {
                        if (!object.getReplyForModel().getUserDetailForumModel().getImage().equals("")) {
                            Picasso.get().load(object.getReplyForModel().getUserDetailForumModel().getImage()).fit().centerCrop()
                                    .placeholder(R.drawable.palace_holder)
                                    .error(R.drawable.palace_holder)
                                    .into(((RecieverViewHolder) holder).imagePicLeftReply);
                        }
                        ((RecieverViewHolder) holder).lefttimeReplyTV.setText(object.getReplyForModel().getUserDetailForumModel().getFirst_name());

                        if (object.getReplyForModel().getImage_count().equals("0")) {
                            ((RecieverViewHolder) holder).imageRplyRL.setVisibility(View.GONE);
                        } else if (object.getReplyForModel().getImage_count().equals("1")) {
                            ((RecieverViewHolder) holder).blurPicRL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).imageRplyRL.setVisibility(View.VISIBLE);
                            // Image 1 Check
                            if (!Utilities.isDocAdded(object.getReplyForModel().getImage())) {
                                if (Utilities.isVideoAdded(object.getReplyForModel().getImage()))
                                    ((RecieverViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getReplyForModel().getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemMessagePicLeftReplyIV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }


                        } else if (Integer.parseInt(object.getReplyForModel().getImage_count()) >= 2) {
                            ((RecieverViewHolder) holder).imageRplyRL.setVisibility(View.VISIBLE);
                            ((RecieverViewHolder) holder).blurPicRL.setVisibility(View.VISIBLE);
                            int imageCount = Integer.parseInt(object.getReplyForModel().getImage_count()) - 1;
                            String strImageCount = "+" + imageCount;
                            ((RecieverViewHolder) holder).textCountPicTv.setText(strImageCount);

                            // Image 1 Check
                            if (!Utilities.isDocAdded(object.getReplyForModel().getImage())) {
                                if (Utilities.isVideoAdded(object.getReplyForModel().getImage()))
                                    ((RecieverViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getReplyForModel().getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemMessagePicLeftReplyIV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }


                        }
                    }
                } else {
                    ((RecieverViewHolder) holder).unReplyViewLL.setVisibility(View.VISIBLE);
                    ((RecieverViewHolder) holder).ReplyViewLL.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).replyLL.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.VISIBLE);
                    ((RecieverViewHolder) holder).rightMessgeTv.setVisibility(View.VISIBLE);
                    ((RecieverViewHolder) holder).imagePicRight.setVisibility(View.VISIBLE);
                }


                if (!object.getUserDetailForumModel().getImage().equals("")) {
                    Glide.with(context).load(object.getUserDetailForumModel().getImage()).into(((RecieverViewHolder) holder).imagePicRight);
                    Glide.with(context).load(object.getUserDetailForumModel().getImage()).into(((RecieverViewHolder) holder).imagePicRightReply1);
                }

                ((RecieverViewHolder) holder).rightMessgeTv.setText(object.getMessage());

                ((RecieverViewHolder) holder).menuRightLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, view, "receiver", position);
                    }
                });

                ((RecieverViewHolder) holder).unReplyViewLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            vibe.vibrate(150);
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);

                        }
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).multiImageRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            vibe.vibrate(150);
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);

                        }
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).ReplyViewLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            vibe.vibrate(150);
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);

                        }
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).itemMessagePicIV.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            vibe.vibrate(150);
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);

                        }
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).multiImageReplyPicRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            vibe.vibrate(150);
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);

                        }
                        return true;
                    }
                });


                ((RecieverViewHolder) holder).itemReplyPICIV.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            vibe.vibrate(150);
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);

                        }
                        return true;
                    }
                });

                // convert seconds to milliseconds
                if (!object.getCreation_date().equals("")) {
                    long unix_seconds = Long.parseLong(object.getCreation_date());

                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(unix_seconds * 1000L);

//                      DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
                    DateFormat format = new SimpleDateFormat("hh:mm a");

                    String date = format.format(cal.getTime());
                    ((RecieverViewHolder) holder).timeRightTV.setText(object.getUserDetailForumModel().getFirst_name() + " " + date);
                    ((RecieverViewHolder) holder).timeRightReplyTV.setText(object.getUserDetailForumModel().getFirst_name() + " " + date);
                }

                if (!object.getImage_count().equals("")) {
                    if (object.getImage_count().equals("1")) {

                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.GONE);

                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemMessagePicIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemMessagePicIV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemMessagePicIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                    } else if (object.getImage_count().equals("2")) {
                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage2LL.setVisibility(View.VISIBLE);

                        // Image 1 Check
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageRight21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight21IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image 2 Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageRight22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage2()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight22IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                    } else if (object.getImage_count().equals("3")) {
                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage3LL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).linearImage2LL.setVisibility(View.GONE);

                        // Image 1 Check
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageRight31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight31IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image 2 Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageRight32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage2()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight32IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image 3 Check
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((RecieverViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage3()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight33IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                    } else if (object.getImage_count().equals("4")) {
                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).linearImage4LL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage2LL.setVisibility(View.GONE);


                        // Image 1 Check

                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight41IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image 2 Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage2()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight42IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image 3 Check
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((RecieverViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage3()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight43IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image 4 Check
                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((RecieverViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage4()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight44IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                    } else if (Integer.parseInt(object.getImage_count()) >= 5) {
                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).linearImage4LL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).blurRL.setVisibility(View.VISIBLE);

                        int imageCount = Integer.parseInt(object.getImage_count()) - 4;
                        String strImageCount = "+" + imageCount;
                        ((RecieverViewHolder) holder).textCountTv.setText(strImageCount);


                        // Image 1 Check

                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight41IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image 2 Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage2()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight42IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image 3 Check
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((RecieverViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage3()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight43IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image 4 Check
                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((RecieverViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage4()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight44IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }
                    } else if (object.getImage_count().equals("0")) {
                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
                    }
                }
                ((RecieverViewHolder) holder).multiImageRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImage_count().equals("1")) {
                            // Image 1 Click
                            mImageArryList.add(object.getImage());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("2")) {
                            // Image 2 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("3")) {
                            // Image 3 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("4")) {
                            // Image 4 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("5")) {
                            // Image 5 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("6")) {
                            // Image 6 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("7")) {
                            // Image 7 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("8")) {
                            // Image 8 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("9")) {
                            // Image 9 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("10")) {
                            // Image 10 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            mImageArryList.add(object.getImage10());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        }


                    }
                });

                ((RecieverViewHolder) holder).multiImageReplyPicRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImage_count().equals("1")) {
                            // Image 1 Click
                            mImageArryList.add(object.getImage());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("2")) {
                            // Image 2 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("3")) {
                            // Image 3 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("4")) {
                            // Image 4 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("5")) {
                            // Image 5 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("6")) {
                            // Image 6 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("7")) {
                            // Image 7 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("8")) {
                            // Image 8 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("9")) {
                            // Image 9 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("10")) {
                            // Image 10 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            mImageArryList.add(object.getImage10());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        }


                    }
                });
                ((RecieverViewHolder) holder).itemMessagePicIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImage_count().equals("1")) {
                            mImageArryList.add(object.getImage());
                        }
                        Intent intent = new Intent(context, GalleryActivity.class);
                        intent.putStringArrayListExtra("LIST", mImageArryList);
                        context.startActivity(intent);

                    }
                });

                ((RecieverViewHolder) holder).itemReplyPICIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImage_count().equals("1")) {
                            mImageArryList.add(object.getImage());
                        }
                        Intent intent = new Intent(context, GalleryActivity.class);
                        intent.putStringArrayListExtra("LIST", mImageArryList);
                        context.startActivity(intent);

                    }
                });

                break;
            /******************
             * TYPE_ITEM CASE
             *****************/
            case Sender:

                // convert seconds to milliseconds
                if (!object.getCreation_date().equals("")) {
                    long unix_seconds = Long.parseLong(object.getCreation_date());


                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(unix_seconds * 1000L);
//                        DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
                    DateFormat format = new SimpleDateFormat("hh:mm a");

                    String date = format.format(cal.getTime());
                    ((SenderViewHolder) holder).timeTV.setText(date);
                    ((SenderViewHolder) holder).timeUnReplyTV.setText(date);
                    ((SenderViewHolder) holder).lefttimenormalTextTV.setText(date);
                    ((SenderViewHolder) holder).lefttimeReplyTV.setText(date);


                }
                if (!object.getImage_count().equals("")) {
                    if (object.getImage_count().equals("1")) {
                        ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);

                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemMessagePicLeftIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                                ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);
                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemMessagePicLeftIV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemMessagePicLeftIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                    } else if (object.getImage_count().equals("2")) {
                        ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage2LL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);

                        // Image One Check
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemImageRight21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight21IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }
// Image Two Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageRight22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight22IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                    } else if (object.getImage_count().equals("3")) {
                        ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage3LL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);

                        // Image One Check
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemImageRight31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight31IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image Two Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight33IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image Three  Check
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((SenderViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage3().equals("")) {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight33IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                    } else if (object.getImage_count().equals("4")) {
                        ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage4LL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);


                        // Image One Check
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight41IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image Two Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight42IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image Three  Check
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((SenderViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage3().equals("")) {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight43IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image Four  Check
                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((SenderViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage4().equals("")) {
                                    Picasso.get().load(object.getImage4()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight44IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                    } else if (Integer.parseInt(object.getImage_count()) >= 5) {
                        ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage4LL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).blurRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);
                        int imageCount = Integer.parseInt(object.getImage_count()) - 4;
                        String strImageCount = "+" + imageCount;
                        ((SenderViewHolder) holder).textCountTv.setText(strImageCount);
                        // Image One Check
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight41IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image Two Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight42IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image Three  Check
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((SenderViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage3().equals("")) {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight43IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image Four  Check
                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((SenderViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage4().equals("")) {
                                    Picasso.get().load(object.getImage4()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageRight44IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                    } else if (object.getImage_count().equals("0")) {
                        ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).textRL.setVisibility(View.VISIBLE);
                    }
                }
                ((SenderViewHolder) holder).leftChatTV.setText(object.getMessage());
                ((SenderViewHolder) holder).leftChatTextTV.setText(object.getMessage());
                if (!object.getUserDetailForumModel().getImage().equals("")) {
                    Picasso.get().load(object.getUserDetailForumModel().getImage()).fit().centerCrop()
                            .placeholder(R.drawable.palace_holder)
                            .error(R.drawable.palace_holder)
                            .into(((SenderViewHolder) holder).imagePicLeft);
                }

                if (object.getReplyForModel() != null) {
                    ((SenderViewHolder) holder).unReplyViewRL.setVisibility(View.GONE);
                    ((SenderViewHolder) holder).replyRL.setVisibility(View.VISIBLE);
                    ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                    ((SenderViewHolder) holder).leftChatTextRTV.setText(object.getMessage());
                    ((SenderViewHolder) holder).leftChatTextReplyPicTV.setText(object.getMessage());

                    // For reply view when image is One
                    if (object.getImage_count().equals("1")) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);

                        // For Reply One image send
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemMessageReplyPicLeftIV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                    }
//
                    // For reply view when image is two
                    if (object.getImage_count().equals("2")) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).imageReplyPic2LL.setVisibility(View.VISIBLE);


                        // For Reply two image send
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageReplyPic22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic22IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // For Reply two image send
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemImageReplyPic21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic21IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                    }

                    // For reply view when image is three
                    if (object.getImage_count().equals("3")) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).imageReplyPic2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic3LL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).itemImageReplyPic32IV.setVisibility(View.VISIBLE);
                        // For Reply three image send

                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic31IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic32IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((SenderViewHolder) holder).itemImageReplyPic33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage3().equals("")) {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic33IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                    }
                    // For reply view when image is Four
                    if (object.getImage_count().equals("4")) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).imageReplyPic2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic3LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);
                        // For Reply Four image send

                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic41IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic42IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage3().equals("")) {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic43IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage4().equals("")) {
                                    Picasso.get().load(object.getImage4()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic44IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                    }// For reply view when image is Five or more than Five
                    if (Integer.parseInt(object.getImage_count()) >= 5) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).imageReplyPic2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic3LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).blurReplyPicRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).textCountReplyPicTv.setVisibility(View.VISIBLE);
                        int imageCount = Integer.parseInt(object.getImage_count()) - 4;
                        String strImageCount = "+" + imageCount;
                        ((SenderViewHolder) holder).textCountReplyPicTv.setText(strImageCount);

// For Reply Four image send

                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic41IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic42IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage3().equals("")) {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic43IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage4().equals("")) {
                                    Picasso.get().load(object.getImage4()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic44IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }
                    }
                    if (object.getImage_count().equals("0")) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic3LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic4LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).blurReplyPicRL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).textCountReplyPicTv.setVisibility(View.GONE);
                    }

                    if (!object.getReplyForModel().getUserDetailForumModel().getImage().equals("")) {
                        Picasso.get().load(object.getReplyForModel().getUserDetailForumModel().getImage()).fit().centerCrop()
                                .placeholder(R.drawable.palace_holder)
                                .error(R.drawable.palace_holder)
                                .into(((SenderViewHolder) holder).imagePicLeftReply);


                    }


//                        =====================================
                    long unix_seconds = Long.parseLong(object.getReplyForModel().getCreation_date());

                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(unix_seconds * 1000L);
//                        DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
                    DateFormat format = new SimpleDateFormat("hh:mm a");

                    String date = format.format(cal.getTime());

//                        ((SenderViewHolder) holder).lefttimeReplyTV.setText(object.getReplyForModel().getUserDetailForumModel().getFirst_name()+" "+date);
                    ((SenderViewHolder) holder).lefttimeReplyTV.setText(object.getReplyForModel().getUserDetailForumModel().getFirst_name());
                    ((SenderViewHolder) holder).leftChatReplyTV.setText(object.getReplyForModel().getMessage());

                    if (!object.getReplyForModel().getImage_count().equals("")) {
                        if (!object.getReplyForModel().getImage_count().equals("0")) {
                            ((SenderViewHolder) holder).imageRplyRL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).leftChatReplyTV.setVisibility(View.VISIBLE);
                            if (object.getReplyForModel().getImage_count().equals("1")) {
                                ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setVisibility(View.VISIBLE);

                                if (!Utilities.isDocAdded(object.getReplyForModel().getImage())) {
                                    if (Utilities.isVideoAdded(object.getReplyForModel().getImage()))
                                        ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    else {

                                        if (!object.getReplyForModel().getImage().equals("")) {
                                            Picasso.get().load(object.getReplyForModel().getImage()).fit().centerCrop()
                                                    .placeholder(R.drawable.palace_holder)
                                                    .error(R.drawable.palace_holder)
                                                    .into(((SenderViewHolder) holder).itemMessagePicLeftReplyIV);
                                        }
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            } else if (Integer.parseInt(object.getReplyForModel().getImage_count()) >= 2) {
                                ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setVisibility(View.VISIBLE);
                                int imageCount = Integer.parseInt(object.getReplyForModel().getImage_count()) - 1;
                                String strImageCount = "+" + imageCount;
                                ((SenderViewHolder) holder).textCountPicTv.setText(strImageCount);


                                if (!Utilities.isDocAdded(object.getReplyForModel().getImage())) {
                                    if (Utilities.isVideoAdded(object.getReplyForModel().getImage()))
                                        ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    else {

                                        if (!object.getReplyForModel().getImage().equals("")) {
                                            Picasso.get().load(object.getReplyForModel().getImage()).fit().centerCrop()
                                                    .placeholder(R.drawable.palace_holder)
                                                    .error(R.drawable.palace_holder)
                                                    .into(((SenderViewHolder) holder).itemMessagePicLeftReplyIV);
                                        }
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            }
                        } else {
                            ((SenderViewHolder) holder).imageRplyRL.setVisibility(View.GONE);
                        }

                    }

                } else {
                    ((SenderViewHolder) holder).replyRL.setVisibility(View.GONE);
                    ((SenderViewHolder) holder).unReplyViewRL.setVisibility(View.VISIBLE);
                }

                ((SenderViewHolder) holder).multiImageLefftRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImage_count().equals("1")) {
                            mImageArryList.add(object.getImage());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("2")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("3")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("4")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("5")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("6")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("7")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("8")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("9")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("10")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            mImageArryList.add(object.getImage10());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        }
                    }
                });
                ((SenderViewHolder) holder).multiImageReplyPicRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImage_count().equals("1")) {
                            mImageArryList.add(object.getImage());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("2")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("3")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("4")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("5")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("6")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("7")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("8")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("9")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImage_count().equals("10")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            mImageArryList.add(object.getImage10());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        }
                    }
                });

                ((SenderViewHolder) holder).itemMessagePicLeftIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImage_count().equals("1")) {
                            mImageArryList.add(object.getImage());
                        }
                        Intent intent = new Intent(context, GalleryActivity.class);
                        intent.putStringArrayListExtra("LIST", mImageArryList);
                        context.startActivity(intent);
                    }
                });
                ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImage_count().equals("1")) {
                            mImageArryList.add(object.getImage());
                        }
                        Intent intent = new Intent(context, GalleryActivity.class);
                        intent.putStringArrayListExtra("LIST", mImageArryList);
                        context.startActivity(intent);
                    }
                });

                ((SenderViewHolder) holder).menuleftLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, view, "sender", position);
                    }
                });

                ((SenderViewHolder) holder).layoutClickLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                            vibe.vibrate(150);

                        }

                        return true;
                    }
                });
                ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                            vibe.vibrate(150);

                        }
                        return true;
                    }
                });
                ((SenderViewHolder) holder).multiImageReplyPicRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                            vibe.vibrate(150);

                        }
                        return true;
                    }
                });

                ((SenderViewHolder) holder).replyRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                            vibe.vibrate(150);

                        }
                        return true;
                    }
                });

                ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                            vibe.vibrate(150);

                        }
                        return true;
                    }
                });

                ((SenderViewHolder) holder).multiImageLefftRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                            vibe.vibrate(150);

                        }
                        return true;
                    }
                });

                ((SenderViewHolder) holder).itemMessagePicLeftIV.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (strType.equals("staffAdmin")) {
                            mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                            vibe.vibrate(150);

                        }
                        return true;
                    }
                });
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (modelArrayList != null) {
            ForumChatModel object = modelArrayList.get(position);
            if (object != null) {
                return object.getIntType();
            }
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        if (modelArrayList == null)
            return 0;
        return modelArrayList.size();
    }

    public static class RecieverViewHolder extends RecyclerView.ViewHolder {
        public TextView rightMessgeTv, textCountTv, lefttimeReplyTV, timeRightTV, timeRightReplyTV, textCountPicTv,
                rightMessgeReplyTextTv, leftChatReplyTV, textCountReplyPicTv, rightMessgUnReplyTextTv;
        public ImageView itemMessagePicIV, imagePicRight, itemImageRight41IV, itemImageRight42IV, itemImageRight43IV,
                itemImageRight44IV, itemImageRight21IV, itemMessagePicLeftReplyIV, itemImageRight22IV, itemImageRight31IV,
                itemImageRight32IV, itemImageRight33IV, itemReplyPICIV, itemImageReplyPic44IV,
                itemImageReplyPic22IV, itemImageReplyPic21IV, itemImageReplyPic33IV, itemImageReplyPic42IV, itemImageReplyPic43IV,
                itemImageReplyPic31IV, itemImageReplyPic32IV, itemImageReplyPic41IV;
        public RelativeLayout multiImageRL, blurRL, ReplyViewLL, unReplyViewLL, imageRplyRL, multiImageReplyPicRL, blurReplyPicRL, replyLL, blurPicRL;
        public de.hdodenhof.circleimageview.CircleImageView imagePicLeftReply, imagePicRightReply1;
        public LinearLayout linearImage4LL, linearImage3LL, linearImage2LL, menuRightLL, image2ReplyPicLL, image3ReplyPicLL, imageReplyPic4LL;


        public RecieverViewHolder(View view) {
            super(view);
            textCountTv = (TextView) view.findViewById(R.id.textCountTv);
            lefttimeReplyTV = (TextView) view.findViewById(R.id.lefttimeReplyTV);
            textCountReplyPicTv = (TextView) view.findViewById(R.id.textCountReplyPicTv);
            rightMessgeTv = (TextView) view.findViewById(R.id.rightMessgeTv);
            timeRightTV = (TextView) view.findViewById(R.id.timeRightTV);
            timeRightReplyTV = (TextView) view.findViewById(R.id.timeRightReplyTV);
            rightMessgeReplyTextTv = (TextView) view.findViewById(R.id.rightMessgeReplyTextTv);
            rightMessgUnReplyTextTv = (TextView) view.findViewById(R.id.rightMessgUnReplyTextTv);
            leftChatReplyTV = (TextView) view.findViewById(R.id.leftChatReplyTV);
            textCountPicTv = (TextView) view.findViewById(R.id.textCountPicTv);
            itemMessagePicIV = (ImageView) view.findViewById(R.id.itemMessagePicIV);

            imagePicRight = (ImageView) view.findViewById(R.id.imagePicRight);
            itemImageRight41IV = (ImageView) view.findViewById(R.id.itemImageRight41IV);
            itemImageRight42IV = (ImageView) view.findViewById(R.id.itemImageRight42IV);
            itemImageRight43IV = (ImageView) view.findViewById(R.id.itemImageRight43IV);


            itemImageRight31IV = (ImageView) view.findViewById(R.id.itemImageRight31IV);
            itemImageRight32IV = (ImageView) view.findViewById(R.id.itemImageRight32IV);
            itemImageRight33IV = (ImageView) view.findViewById(R.id.itemImageRight33IV);


            itemImageRight44IV = (ImageView) view.findViewById(R.id.itemImageRight44IV);

            itemImageReplyPic22IV = (ImageView) view.findViewById(R.id.itemImageReplyPic22IV);
            itemImageReplyPic21IV = (ImageView) view.findViewById(R.id.itemImageReplyPic21IV);
            itemReplyPICIV = (ImageView) view.findViewById(R.id.itemReplyPICIV);
            itemImageReplyPic31IV = (ImageView) view.findViewById(R.id.itemImageReplyPic31IV);
            itemImageReplyPic32IV = (ImageView) view.findViewById(R.id.itemImageReplyPic32IV);
            itemImageReplyPic33IV = (ImageView) view.findViewById(R.id.itemImageReplyPic33IV);
            itemImageReplyPic41IV = (ImageView) view.findViewById(R.id.itemImageReplyPic41IV);
            itemImageReplyPic42IV = (ImageView) view.findViewById(R.id.itemImageReplyPic42IV);
            itemImageReplyPic43IV = (ImageView) view.findViewById(R.id.itemImageReplyPic43IV);
            itemImageReplyPic44IV = (ImageView) view.findViewById(R.id.itemImageReplyPic44IV);
            multiImageRL = (RelativeLayout) view.findViewById(R.id.multiImageRL);
            blurReplyPicRL = (RelativeLayout) view.findViewById(R.id.blurReplyPicRL);
            imageRplyRL = (RelativeLayout) view.findViewById(R.id.imageRplyRL);


            blurRL = (RelativeLayout) view.findViewById(R.id.blurRL);
            blurPicRL = (RelativeLayout) view.findViewById(R.id.blurPicRL);

            ReplyViewLL = (RelativeLayout) view.findViewById(R.id.ReplyViewLL);
            unReplyViewLL = (RelativeLayout) view.findViewById(R.id.unReplyViewLL);
            multiImageReplyPicRL = (RelativeLayout) view.findViewById(R.id.multiImageReplyPicRL);
            image2ReplyPicLL = (LinearLayout) view.findViewById(R.id.image2ReplyPicLL);
            image3ReplyPicLL = (LinearLayout) view.findViewById(R.id.image3ReplyPicLL);
            replyLL = (RelativeLayout) view.findViewById(R.id.replyLL);
            menuRightLL = (LinearLayout) view.findViewById(R.id.menuRightLL);
            linearImage4LL = (LinearLayout) view.findViewById(R.id.linearImage4LL);
            linearImage3LL = (LinearLayout) view.findViewById(R.id.linearImage3LL);
            linearImage2LL = (LinearLayout) view.findViewById(R.id.linearImage2LL);
            imageReplyPic4LL = (LinearLayout) view.findViewById(R.id.imageReplyPic4LL);
            itemImageRight21IV = (ImageView) view.findViewById(R.id.itemImageRight21IV);
            itemImageRight22IV = (ImageView) view.findViewById(R.id.itemImageRight22IV);
            itemMessagePicLeftReplyIV = (ImageView) view.findViewById(R.id.itemMessagePicLeftReplyIV);
            imagePicLeftReply =  view.findViewById(R.id.imagePicLeftReply);
            imagePicRightReply1 = view.findViewById(R.id.imagePicRightReply1);
        }
    }


    public static class SenderViewHolder extends RecyclerView.ViewHolder {
        public TextView leftChatTV, timeTV, textCountTv, textCountPicTv, leftChatTextTV, leftChatTextRTV,
                timeUnReplyTV, lefttimenormalTextTV, leftChatReplyTV,
                leftChatTextReplyPicTV, textCountReplyPicTv, lefttimeReplyTV;
        public ImageView itemMessagePicLeftIV, imagePicLeft, itemImageRight41IV, itemImageRight42IV,
                itemImageRight43IV, itemImageRight44IV, itemImageRight31IV, itemImageRight32IV, itemMessagePicLeftReplyIV,
                itemImageRight33IV, itemImageRight21IV, itemImageRight22IV, imageMenuIMG, imagePicLeftReply, itemMessageReplyPicLeftIV, itemImageReplyPic31IV, itemImageRight41ReplyIV, itemImageReplyPic22IV, itemImageReplyPic21IV,
                itemImageReplyPic32IV, itemImageReplyPic33IV, itemImageReplyPic41IV, itemImageReplyPic42IV,
                itemImageReplyPic43IV, itemImageReplyPic44IV;
        public RelativeLayout multiImageLefftRL, blurRL, layoutClickLL,
                textRL, unReplyViewRL, replyRL, imageRplyRL, multiImageReplyPicRL, blurReplyPicRL;

        public LinearLayout linearImage4LL, linearImage3LL, linearImage2LL, leftChatWithIMGLL, menuleftLL, imageReplyPic2LL, imageReplyPic3LL, imageReplyPic4LL;

        public SenderViewHolder(View view) {
            super(view);
            leftChatTV = (TextView) view.findViewById(R.id.leftChatTV);
            timeTV = (TextView) view.findViewById(R.id.timeTV);
            timeUnReplyTV = (TextView) view.findViewById(R.id.timeUnReplyTV);
            lefttimenormalTextTV = (TextView) view.findViewById(R.id.lefttimenormalTextTV);
            textCountTv = (TextView) view.findViewById(R.id.textCountTv);
            textCountPicTv = (TextView) view.findViewById(R.id.textCountPicTv);
            leftChatTextTV = (TextView) view.findViewById(R.id.leftChatTextTV);
            leftChatTextRTV = (TextView) view.findViewById(R.id.leftChatTextRTV);
//            leftChatTextReplyPicTV = (TextView) view.findViewById(R.id.leftChatTextReplyPicTV);

            lefttimeReplyTV = (TextView) view.findViewById(R.id.lefttimeReplyTV);

            leftChatReplyTV = (TextView) view.findViewById(R.id.leftChatReplyTV);
            itemMessagePicLeftIV = (ImageView) view.findViewById(R.id.itemMessagePicLeftIV);
            imagePicLeftReply = (ImageView) view.findViewById(R.id.imagePicLeftReply);
            itemMessageReplyPicLeftIV = (ImageView) view.findViewById(R.id.itemMessageReplyPicLeftIV);
            itemImageRight41IV = (ImageView) view.findViewById(R.id.itemImageRight41IV);
            itemImageRight42IV = (ImageView) view.findViewById(R.id.itemImageRight42IV);
            itemImageRight43IV = (ImageView) view.findViewById(R.id.itemImageRight43IV);
            itemImageRight44IV = (ImageView) view.findViewById(R.id.itemImageRight44IV);

            itemImageRight31IV = (ImageView) view.findViewById(R.id.itemImageRight31IV);
            itemImageRight32IV = (ImageView) view.findViewById(R.id.itemImageRight32IV);
            itemImageRight33IV = (ImageView) view.findViewById(R.id.itemImageRight33IV);

            itemImageRight21IV = (ImageView) view.findViewById(R.id.itemImageRight21IV);
            itemImageRight22IV = (ImageView) view.findViewById(R.id.itemImageRight22IV);

            // For Reply Layout view
            itemImageReplyPic32IV = (ImageView) view.findViewById(R.id.itemImageReplyPic32IV);


            itemMessagePicLeftReplyIV = (ImageView) view.findViewById(R.id.itemMessagePicLeftReplyIV);


            itemImageReplyPic22IV = (ImageView) view.findViewById(R.id.itemImageReplyPic22IV);
            itemImageReplyPic21IV = (ImageView) view.findViewById(R.id.itemImageReplyPic21IV);
            itemImageReplyPic31IV = (ImageView) view.findViewById(R.id.itemImageReplyPic31IV);
            itemImageReplyPic33IV = (ImageView) view.findViewById(R.id.itemImageReplyPic33IV);
            itemImageReplyPic41IV = (ImageView) view.findViewById(R.id.itemImageReplyPic41IV);
            itemImageReplyPic44IV = (ImageView) view.findViewById(R.id.itemImageReplyPic44IV);
            itemImageReplyPic42IV = (ImageView) view.findViewById(R.id.itemImageReplyPic42IV);
            itemImageReplyPic43IV = (ImageView) view.findViewById(R.id.itemImageReplyPic43IV);
            textCountReplyPicTv = (TextView) view.findViewById(R.id.textCountReplyPicTv);

            imageMenuIMG = (ImageView) view.findViewById(R.id.imageMenuIMG);
            multiImageLefftRL = (RelativeLayout) view.findViewById(R.id.multiImageLefftRL);
            imageRplyRL = (RelativeLayout) view.findViewById(R.id.imageRplyRL);
            unReplyViewRL = (RelativeLayout) view.findViewById(R.id.unReplyViewRL);
            replyRL = (RelativeLayout) view.findViewById(R.id.replyRL);
            blurRL = (RelativeLayout) view.findViewById(R.id.blurRL);
            blurReplyPicRL = (RelativeLayout) view.findViewById(R.id.blurReplyPicRL);
            multiImageReplyPicRL = (RelativeLayout) view.findViewById(R.id.multiImageReplyPicRL);

            textRL = (RelativeLayout) view.findViewById(R.id.textRL);

            layoutClickLL = (RelativeLayout) view.findViewById(R.id.layoutClickLL);

            linearImage4LL = (LinearLayout) view.findViewById(R.id.linearImage4LL);
            linearImage3LL = (LinearLayout) view.findViewById(R.id.linearImage3LL);
            linearImage2LL = (LinearLayout) view.findViewById(R.id.linearImage2LL);
            leftChatWithIMGLL = (LinearLayout) view.findViewById(R.id.leftChatWithIMGLL);


            imageReplyPic2LL = (LinearLayout) view.findViewById(R.id.imageReplyPic2LL);
            imageReplyPic3LL = (LinearLayout) view.findViewById(R.id.imageReplyPic3LL);
            imageReplyPic4LL = (LinearLayout) view.findViewById(R.id.imageReplyPic4LL);
            menuleftLL = (LinearLayout) view.findViewById(R.id.menuleftLL);
        }
    }

}
