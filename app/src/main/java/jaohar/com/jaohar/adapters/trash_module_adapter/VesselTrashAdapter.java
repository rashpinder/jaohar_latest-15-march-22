package jaohar.com.jaohar.adapters.trash_module_adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
//import com.bumptech.glide.request.animation.GlideAnimation;

import java.util.ArrayList;
import java.util.HashMap;

import jaohar.com.jaohar.interfaces.DeleteVesselsInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteSelectedTrashInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverORDeleteTrashInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.models.AllVessel;


public class VesselTrashAdapter extends RecyclerView.Adapter<VesselTrashAdapter.ViewHolder> {
    private DeleteVesselsInterface mDeleteVesselsInterface;
    Activity mActivity;
    private ArrayList<AllVessel> modelArrayList;

    private static HashMap<AllVessel, Boolean> checkedForModel = new HashMap<>();
    private static boolean[] checkBoxState = null;
    RecoverORDeleteTrashInterface mRecoverDeleteInterface;
    RecoverDeleteSelectedTrashInterface mRecoverDELETEInterface;
    paginationforVesselsInterface mPagination;





    public VesselTrashAdapter(Activity mActivity, ArrayList<AllVessel> modelArrayList, RecoverORDeleteTrashInterface mRecoverDeleteInterface, paginationforVesselsInterface mPagination, RecoverDeleteSelectedTrashInterface mRecoverDELETEInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mRecoverDeleteInterface = mRecoverDeleteInterface;
        this.mPagination = mPagination;
        this.mRecoverDELETEInterface = mRecoverDELETEInterface;
        checkBoxState=new boolean[modelArrayList.size()];
    }

    @Override
    public VesselTrashAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.item_vessel_trash, parent, false);
                .inflate(R.layout.item_vessel_trash, parent, false);
        return new VesselTrashAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final VesselTrashAdapter.ViewHolder holder, final int position) {
        final AllVessel tempValue = modelArrayList.get(position);
        // pagination for smooth scrooling
        if (position >= modelArrayList.size()-1 ){
            mPagination.mPaginationforVessels(true);
        }
        holder.item_DeleteIV.setVisibility(View.VISIBLE);
        if(checkBoxState != null){
            holder.imgMultiSelectIV .setChecked(tempValue.getSelected());
        }
        holder.imgMultiSelectIV.setTag(position);
        if (tempValue.getPhoto1().length() > 0 && tempValue.getPhoto1().contains("http")) {
//            Glide.with(mActivity)
//                    .load(tempValue.getPhoto1())
//                    .asBitmap()
////                    .diskCacheStrategy(DiskCacheStrategy.NONE)
////                    .skipMemoryCache(true)
//                    .placeholder(R.drawable.palace_holder)
//                    .into(new SimpleTarget<Bitmap>() {
//                        @Override
//                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                            // you can do something with loaded bitmap here
////                            Bitmap scaleBitmap= Utilities.getScaledBitMapBaseOnScreenSize(resource,mActivity);
//                            if(resource!=null) {
//                                holder.item_Image.setImageBitmap(resource);
//                            }
//
//                        }
//                    });
            Glide.with(mActivity).load(tempValue.getPhoto1()).into(holder.item_Image);

//        holder.item_Image.setImageUrl(tempValue.getPhoto1(), JaoharApplication.getInstance().getImageLoader());
        } else {
            holder.item_Image.setImageResource(R.drawable.palace_holder);
        }

        /* checkBoxState has the value of checkBox ie true or false,
         * The position is used so that on scroll your selected checkBox maintain its state */
//if(position!=0){



//}


        if (tempValue.getVesselName().length() > 0) {
            holder.item_Title.setText(tempValue.getVesselName());

        }
        if (tempValue.getDeleted_by().length() > 0) {
            holder.item_DeletedBY.setText(tempValue.getDeleted_by());

        }


        if (tempValue.getVesselType().length() > 0) {
            String strType ="DWT " + tempValue.getCapacity() + "/" + tempValue.getPriceIdea() + " " + tempValue.getCurrency();
            holder.item_Type.setText(strType);
        }
        if (tempValue.getCapacity().length() > 0) {
            String strCApicity = tempValue.getVesselType() + "/" + tempValue.getYearBuilt() + "/" + tempValue.getPlaceOfBuilt();
            holder.item_DateTime.setText(strCApicity);
        }
holder.item_RecycleIV.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        mRecoverDeleteInterface.mRecoverAndDeleteTrash(tempValue,"recover");
    }
});holder.item_DeleteIV.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        mRecoverDeleteInterface.mRecoverAndDeleteTrash(tempValue,"delete");
    }
});





        holder.imgMultiSelectIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.imgMultiSelectIV.isChecked()) {
//                    checkBoxState[position] = true;
//                    ischecked(position,true);
                    tempValue.setSelected(true);
                    mRecoverDELETEInterface.mRecoverDeleteSelectedTrashInterface(tempValue,false);
                } else {
                    tempValue.setSelected(false);
//                    checkBoxState[position] = false;
//                    ischecked(position,false);
                    mRecoverDELETEInterface.mRecoverDeleteSelectedTrashInterface(tempValue,true);
                }

            }
        });
//        /*if country is in checkedForCountry then set the checkBox to true */
//        if (checkedForModel.get(tempValue) != null) {
//            holder.imgMultiSelectIV.setChecked(checkedForModel.get(tempValue));
//        }
//
//        /*Set tag to all checkBox*/
//        holder.imgMultiSelectIV.setTag(tempValue);


//        holder.item_mail.setText(HtmlCompat.fromHtml(mActivity, tempValue.getMail_data(), 0 , new URLImageParser( holder.item_mail, mActivity)));

        /*Vessels Status on Vessel Image*/
        if (tempValue.getStatus().equals("Sold")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.status_sold);
        } else if (tempValue.getStatus().equals("Withdrawn")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.status_withdrawn);
        } else if (tempValue.getStatus().equals("Committed")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.status_commited);
        } else if (tempValue.getStatus().equals("Scraped")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.scraped);
        } else if (tempValue.getStatus().equals("Hot Sale")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.icon_hot_sale);
        } else {
            holder.item_Image_Status.setVisibility(View.GONE);
        }

    }

    private void ischecked(int position,boolean flag )
    {
        checkedForModel.put(this.modelArrayList.get(position), flag);
    }
    @Override
    public int getItemCount() {
        return modelArrayList == null ? 0 : modelArrayList.size();

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView item_Title, item_Type,item_DateTime,item_DeletedBY;
        private ImageView item_Image;
        private ImageView item_Image_Status,item_RecycleIV,item_DeleteIV;
        private CheckBox imgMultiSelectIV;



        ViewHolder(View itemView) {
            super(itemView);
            item_Image =itemView.findViewById(R.id.item_Image);
            item_RecycleIV =itemView.findViewById(R.id.item_RecycleIV);
            item_DeleteIV =itemView.findViewById(R.id.item_DeleteIV);
            item_Image_Status =  itemView.findViewById(R.id.item_Image_Status);
            imgMultiSelectIV =  itemView.findViewById(R.id.imgMultiSelectIV);
            item_Title =  itemView.findViewById(R.id.item_Title);
            item_Type =itemView.findViewById(R.id.item_Type);
            item_DateTime =  itemView.findViewById(R.id.item_DateTime);
            item_DeletedBY =  itemView.findViewById(R.id.item_DeletedBY);



        }
    }
}
