package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.InvoiceVesselDeleteInterface;
import jaohar.com.jaohar.interfaces.InvoiceVesselEditInterface;

/**
 * Created by Dharmani Apps on 1/15/2018.
 */

public class InvoiceVesselsAdapter extends RecyclerView.Adapter<InvoiceVesselsAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<VesselSearchInvoiceModel> modelArrayList;
    private InvoiceVesselDeleteInterface mInvoiceVesselDeleteInterface;
    private InvoiceVesselEditInterface mInvoiceVesselEditInterface;

    public InvoiceVesselsAdapter(Activity mActivity, ArrayList<VesselSearchInvoiceModel> modelArrayList, InvoiceVesselDeleteInterface mInvoiceVesselDeleteInterface, InvoiceVesselEditInterface mInvoiceVesselEditInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mInvoiceVesselDeleteInterface = mInvoiceVesselDeleteInterface;
        this.mInvoiceVesselEditInterface = mInvoiceVesselEditInterface;
    }

    @Override
    public InvoiceVesselsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invoicevessels, null);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(InvoiceVesselsAdapter.ViewHolder holder, int position) {
        final VesselSearchInvoiceModel tempValue = modelArrayList.get(position);
        holder.item_Title.setText(tempValue.getVessel_name());

        holder.item_Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", tempValue);
                mActivity.setResult(666, returnIntent);
                mActivity.finish();
            }
        });

        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInvoiceVesselEditInterface.invoiceVesselEdit(tempValue);
            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInvoiceVesselDeleteInterface.invoiceVesselDelete(tempValue, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title, txtEdit, txtDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
        }
    }
}
