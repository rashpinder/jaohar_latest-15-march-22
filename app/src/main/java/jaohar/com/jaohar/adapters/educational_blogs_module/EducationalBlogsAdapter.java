package jaohar.com.jaohar.adapters.educational_blogs_module;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.educational_blogs_module.OpenEducationBlogActivity;
import jaohar.com.jaohar.beans.ForumModule.ForumModel;
import jaohar.com.jaohar.beans.educational_blogs_module.EducationalBlogsModel;
import jaohar.com.jaohar.interfaces.PaginationInterface;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.utils.JaoharConstants;

public class EducationalBlogsAdapter extends RecyclerView.Adapter<EducationalBlogsAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<EducationalBlogsModel> modelArrayList;
    PaginationInterface mPagination;
    String Type;

    public EducationalBlogsAdapter(Activity mActivity, ArrayList<EducationalBlogsModel> modelArrayList,
                                   PaginationInterface mPagination, String Type) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mPagination = mPagination;
        this.Type = Type;
    }

    @Override
    public EducationalBlogsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_educational_blogs_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(EducationalBlogsAdapter.ViewHolder holder, final int position) {
        final EducationalBlogsModel tempValue = modelArrayList.get(position);

        /* for serach 1 else 0 */
        if (Type.equals("0")) {
            // pagination for smooth scrooling
            if (position >= modelArrayList.size() - 1) {
                mPagination.mPaginationInterface(true);
            }
        }

        if (tempValue.getCreation_date() != null && !tempValue.getCreation_date().equals("")) {
            long dv = Long.valueOf(tempValue.getCreation_date()) * 1000;// its need to be in milisecond
            Date df = new java.util.Date(dv);
            String vv = new SimpleDateFormat("dd MMM, yyyy").format(df);
            holder.dateTV.setText(vv);
        }

        if (tempValue.getLink_title() != null && !tempValue.getLink_title().equals("")) {
            holder.titleTV.setText(tempValue.getLink_title());
        }

        if (tempValue.getLink_desc() != null && !tempValue.getLink_desc().equals("")) {
            holder.descriptionTV.setText(tempValue.getLink_desc());
        }

        if (tempValue.getMessage() != null && !tempValue.getMessage().equals("")) {
            holder.messageTV.setText(html2text(tempValue.getMessage()));
        }

        if (tempValue.getLink_image() != null && !tempValue.getLink_image().equals("")) {
            Glide.with(mActivity).load(tempValue.getLink_image()).into(holder.blogsImage);
        }

        holder.blogsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startActivity(new Intent(mActivity, OpenEducationBlogActivity.class)
                .putExtra(JaoharConstants.BLOG_LINK, tempValue.getLink()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView dateTV, messageTV, titleTV, descriptionTV;
        ImageView blogsImage;
        LinearLayout blogsLL;

        ViewHolder(View itemView) {
            super(itemView);

            dateTV = itemView.findViewById(R.id.dateTV);
            messageTV = itemView.findViewById(R.id.messageTV);
            titleTV = itemView.findViewById(R.id.titleTV);
            descriptionTV = itemView.findViewById(R.id.descriptionTV);
            blogsImage = itemView.findViewById(R.id.blogsImage);
            blogsLL = itemView.findViewById(R.id.blogsLL);
        }
    }

    //for converting html to text
    public CharSequence html2text(String html) {
        CharSequence trimmed = noTrailingwhiteLines(Html.fromHtml(html));
        return trimmed;
    }

    //for hiding extra white space
    private CharSequence noTrailingwhiteLines(CharSequence text) {

        if (text.length() > 0)
            while (text.charAt(text.length() - 1) == '\n') {
                text = text.subSequence(0, text.length() - 1);
            }
        return text;
    }
}
