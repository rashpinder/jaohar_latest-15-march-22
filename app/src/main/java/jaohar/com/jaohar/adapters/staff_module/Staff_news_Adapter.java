package jaohar.com.jaohar.adapters.staff_module;

import android.app.Activity;

import android.content.Intent;
import android.content.res.Resources;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import jaohar.com.jaohar.R;

import jaohar.com.jaohar.activities.AllInternalNewsActivity;
import jaohar.com.jaohar.beans.staff_module.Staff_news_model;
import jaohar.com.jaohar.interfaces.ChangeFragmentInterface;
import jaohar.com.jaohar.utils.MyTagHandler;


public class Staff_news_Adapter extends RecyclerView.Adapter<Staff_news_Adapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<Staff_news_model> modelArrayList;
    ChangeFragmentInterface changeFragmentInterface;

    //  calling constructor heregit init
    public Staff_news_Adapter(Activity mActivity, ArrayList<Staff_news_model> modelArrayList, ChangeFragmentInterface changeFragmentInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.changeFragmentInterface = changeFragmentInterface;
    }

    @Override
    public Staff_news_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news_staff, parent, false);
        return new Staff_news_Adapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final Staff_news_Adapter.ViewHolder holder, final int position) {
        final Staff_news_model tempValue = modelArrayList.get(position);

        if (tempValue.getCreated_at() != null && !tempValue.getCreated_at().equals(""))
            holder.dateTV.setText(tempValue.getCreated_at());

//        if (tempValue.getNews_text() != null && !tempValue.getNews_text().equals(""))
//            holder.newsTV.setText(tempValue.getNews_text());

        if (tempValue.getNews_image() != null && !tempValue.getNews_image().equals("")) {
            holder.imageNewsIMG.setVisibility(View.VISIBLE);
            Glide.with(mActivity).load(tempValue.getNews_image()).into(holder.imageNewsIMG);
        } else {
            holder.imageNewsIMG.setVisibility(View.GONE);
        }

        holder.newsWebviewTV.loadDataWithBaseURL(null, tempValue.getNews_text(), "text/html", "UTF-8", null);
        WebSettings webSettings = holder.newsWebviewTV.getSettings();
        Resources res = mActivity.getResources();
        webSettings.setDefaultFontSize((int)res.getDimension(R.dimen._3sdp));
        holder.newsWebviewTV.setBackgroundResource(R.color.internalNewsBG);
        holder.newsWebviewTV.setBackgroundColor(0x00000000);
        holder.newsWebviewTV.setPadding(2,0,2,0);

        holder.newsTV.setText(Html.fromHtml(tempValue.getNews_text(), null, new MyTagHandler()));

        holder.seeAllTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tempValue.getType().equals("mass")) {
                    changeFragmentInterface.mChangeFragmentInterface(position);
                } else if (tempValue.getType().equals("news")) {
                    Intent mIntent = new Intent(mActivity, AllInternalNewsActivity.class);
                    mActivity.startActivity(mIntent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView dateTV, newsTV, seeAllTV;
        public ImageView imageNewsIMG;
        WebView newsWebviewTV;

        ViewHolder(View itemView) {
            super(itemView);

            dateTV = itemView.findViewById(R.id.dateTV);
            newsTV = itemView.findViewById(R.id.newsTV);
            seeAllTV = itemView.findViewById(R.id.seeAllTV);
            imageNewsIMG = itemView.findViewById(R.id.imageNewsIMG);
            newsWebviewTV = itemView.findViewById(R.id.newsWebviewTV);
        }
    }
}

