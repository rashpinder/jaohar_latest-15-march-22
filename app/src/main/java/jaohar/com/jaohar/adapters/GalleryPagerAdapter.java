package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.otaliastudios.zoom.ZoomImageView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.forum_module.VideoPlayActivity;
import jaohar.com.jaohar.activities.webViewActivity;
import jaohar.com.jaohar.interfaces.chat_module.DownloadImageInterface;
import jaohar.com.jaohar.utils.Utilities;


/**
 * Created by Dharmani Apps on 8/19/2017.
 */

public class GalleryPagerAdapter extends PagerAdapter {
    String TAG = "GalleryPagerAdapter";
    Activity mContext;
    ArrayList<String> mImageArryList;
    LayoutInflater mLayoutInflater;
    DownloadImageInterface downloadImageInterface;

    public GalleryPagerAdapter(Activity context, ArrayList<String> mImageArryList, DownloadImageInterface downloadImageInterface) {
        this.mContext = context;
        this.mImageArryList = mImageArryList;
        this.downloadImageInterface = downloadImageInterface;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mImageArryList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.item_gallery_images, container, false);

        final ZoomImageView imgImage = itemView.findViewById(R.id.imgImage);
        final ImageView imgImage2 = itemView.findViewById(R.id.imgImage2);
        final String strUrl = mImageArryList.get(position);

        if (!Utilities.isDocAdded(strUrl)) {
            if (Utilities.isVideoAdded(strUrl)) {
                imgImage2.setVisibility(View.VISIBLE);
                imgImage.setVisibility(View.GONE);
                imgImage2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.video_icon));

                downloadImageInterface.mDownloadImageInterface(position, strUrl, "2");

            } else {
                RequestOptions requestOptions = new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                        .skipMemoryCache(true);
                Glide.with(mContext).asBitmap().apply(requestOptions).load(strUrl).into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        // you can do something with loaded bitmap here
                        imgImage.setImageBitmap(resource);
                    }
                });

                downloadImageInterface.mDownloadImageInterface(position, strUrl, "1");
            }

        } else {
            imgImage2.setVisibility(View.VISIBLE);
            imgImage.setVisibility(View.GONE);
            imgImage2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.icon_doc));
        }

        imgImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (strUrl.contains(".png")) {

                } else if (strUrl.contains(".jpg")) {

                } else if (strUrl.contains(".jpeg")) {

                } else {
                    if (Utilities.isVideoAdded(strUrl)) {
                        Intent intent = new Intent(mContext, VideoPlayActivity.class);
                        intent.putExtra("video_URL", strUrl);
                        mContext.startActivity(intent);
                        mContext.finish();
                    } else {
                        Intent intent = new Intent(mContext, webViewActivity.class);
                        intent.putExtra("news", strUrl);
                        mContext.startActivity(intent);
                        mContext.finish();
                    }
                }
            }
        });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}