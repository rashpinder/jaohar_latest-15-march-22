package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.StampsModel;
import jaohar.com.jaohar.interfaces.DeleteStampInterface;
import jaohar.com.jaohar.interfaces.EditStampInterface;

/**
 * Created by Dharmani Apps on 1/15/2018.
 */

public class StampsAdapter extends RecyclerView.Adapter<StampsAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<StampsModel> modelArrayList;
    private DeleteStampInterface mDeleteInterface;
    private EditStampInterface mEditInterface;

    public StampsAdapter(Activity mActivity, ArrayList<StampsModel> modelArrayList, DeleteStampInterface mDeleteInterface, EditStampInterface mEditInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDeleteInterface = mDeleteInterface;
        this.mEditInterface = mEditInterface;
    }

    @Override
    public StampsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stamps, null);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(StampsAdapter.ViewHolder holder, int position) {
        final StampsModel tempValue = modelArrayList.get(position);
        holder.item_Title.setText(tempValue.getStamp_name());

        holder.item_Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", tempValue);
                mActivity.setResult(333, returnIntent);
                mActivity.finish();
            }
        });
        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditInterface.editInterface(tempValue);
            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDeleteInterface.deleteInterface(tempValue, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title, txtEdit, txtDelete;


        public ViewHolder(View itemView) {
            super(itemView);

            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
        }
    }
}
