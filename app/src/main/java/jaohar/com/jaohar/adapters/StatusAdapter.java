package jaohar.com.jaohar.adapters;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.DeleteStatusInterface;
import jaohar.com.jaohar.interfaces.EditStatusInterface;
import jaohar.com.jaohar.models.StatusData;
import jaohar.com.jaohar.models.StatusModel;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<StatusData> modelArrayList;
    EditStatusInterface mEditStatusInterface;
    DeleteStatusInterface mDeleteStatusInterface;

    public StatusAdapter(Activity mActivity, ArrayList<StatusData> modelArrayList, EditStatusInterface mEditStatusInterface,
                         DeleteStatusInterface mDeleteStatusInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mEditStatusInterface = mEditStatusInterface;
        this.mDeleteStatusInterface = mDeleteStatusInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_status, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        StatusData tempValue = modelArrayList.get(position);
        holder.item_Title.setText(tempValue.getStatusName());
        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditStatusInterface.mEditStatusInterface(tempValue, position);
            }
        });
        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteStatusInterface.mDeleteStatusInterface(tempValue, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title, txtEdit, txtDelete;

        ViewHolder(View itemView) {
            super(itemView);

            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
        }
    }
}