package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.daimajia.swipe.SwipeLayout;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.DeleteInternalNewsInterface;
import jaohar.com.jaohar.interfaces.EditInternalNewsInterface;
import jaohar.com.jaohar.interfaces.OpenNewsPopUpInterFace;
import jaohar.com.jaohar.models.CompanyData;
import jaohar.com.jaohar.utils.JaoharConstants;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class InternalNewsAdapter extends RecyclerView.Adapter<InternalNewsAdapter.ViewHolder> {
    EditInternalNewsInterface mEditInternalNewsInterface;
    DeleteInternalNewsInterface mDeleteInternalNewsInterface;
    private final Activity mActivity;
    private final ArrayList<CompanyData> modelArrayList;
    OpenNewsPopUpInterFace mOpenNewsPopUP;

    public InternalNewsAdapter(Activity mActivity, ArrayList<CompanyData> modelArrayList, EditInternalNewsInterface mEditInternalNewsInterface, DeleteInternalNewsInterface mDeleteInternalNewsInterface, OpenNewsPopUpInterFace mOpenNewsPopUP) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mEditInternalNewsInterface = mEditInternalNewsInterface;
        this.mDeleteInternalNewsInterface = mDeleteInternalNewsInterface;
        this.mOpenNewsPopUP = mOpenNewsPopUP;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CompanyData tempValue = modelArrayList.get(position);

        if (JaoharConstants.IS_CLICK_FROM_COMPANY == true) {
                if (tempValue.getStaffNewsStatus().equals("0")) {
                    holder.txtTypeTV1.setText("Type:" + " " + "Inactive");
                    holder.txtTypeTV.setText("Type:" + " " + "Inactive");
                } else {
                    holder.txtTypeTV1.setText("Type:" + " " + "Active");
                    holder.txtTypeTV.setText("Type:" + " " + "Active");
                }

            if (tempValue.getCreatedAt().equals("") || tempValue.getCreatedAt() == null) {
                holder.txtDateTimeTV.setText(tempValue.getCreatedAt());
                holder.txtDateTimeTV1.setText(tempValue.getCreatedAt());
            }

            if (position % 2 == 0) {
                holder.firstRL.setVisibility(View.VISIBLE);
                holder.SecondLL.setVisibility(View.GONE);

                holder.view1.setVisibility(View.VISIBLE);
                holder.view2.setVisibility(View.GONE);
            } else {
                holder.firstRL.setVisibility(View.GONE);
                holder.SecondLL.setVisibility(View.VISIBLE);

                holder.view1.setVisibility(View.GONE);
                holder.view2.setVisibility(View.VISIBLE);
            }

            if (tempValue.getStaffPhoto().equals("") || tempValue.getStaffPhoto() == null) {
                holder.imgIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.palace_holder));
            } else if (!tempValue.getStaffPhoto().equals("")) {
                Glide.with(mActivity).load(tempValue.getStaffPhoto()).into(holder.imgIV);
            }
            if (tempValue.getStaffPhoto().equals("") || tempValue.getStaffPhoto() == null) {
                holder.imgIV1.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.palace_holder));
            } else if (!tempValue.getStaffPhoto().equals("")) {
                Glide.with(mActivity).load(tempValue.getStaffPhoto()).into(holder.imgIV1);
            }
            String strNews = tempValue.getStaffNewsText().trim();

            if (strNews != null && !strNews.equals("")) {
                holder.textNEWS.setText(html2text(strNews));
                holder.textNEWS1.setText(html2text(strNews));
            }


            if (JaoharConstants.IS_SEE_ALL_INTERVALS_NEWS_CLICK == true) {
                holder.editRL1.setVisibility(View.INVISIBLE);
                holder.editRL.setVisibility(View.INVISIBLE);
                holder.deleteRL1.setVisibility(View.INVISIBLE);
                holder.deleteRL.setVisibility(View.INVISIBLE);
            }

            holder.editRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mEditInternalNewsInterface.mEditNews(tempValue);
                }
            });
            holder.editRL1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mEditInternalNewsInterface.mEditNews(tempValue);
                }
            });


            holder.firstRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JaoharConstants.Is_click_form_NEWS = true;

                    mOpenNewsPopUP.openNewsPopUpInterFace(tempValue.getStaffNews(), tempValue.getStaffPhoto());
                }
            });

            holder.SecondLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JaoharConstants.Is_click_form_NEWS = true;
                    mOpenNewsPopUP.openNewsPopUpInterFace(tempValue.getStaffNews(), tempValue.getStaffPhoto());
                }
            });
            holder.firstRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JaoharConstants.Is_click_form_Internal_NEWS = true;

                    mOpenNewsPopUP.openNewsPopUpInterFace(tempValue.getStaffNews(), tempValue.getStaffPhoto());
                }
            });
            holder.SecondLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JaoharConstants.Is_click_form_Internal_NEWS = true;
                    mOpenNewsPopUP.openNewsPopUpInterFace(tempValue.getStaffNews(), tempValue.getStaffPhoto());

                }
            });

            holder.deleteRL1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDeleteInternalNewsInterface.mDeleteNews(tempValue);
                }
            });

            holder.deleteRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDeleteInternalNewsInterface.mDeleteNews(tempValue);
                }
            });
        } else {
            holder.txtTypeTV1.setText("Type:" + " " + tempValue.getType());
            holder.txtTypeTV.setText("Type:" + " " + tempValue.getType());
            holder.txtDateTimeTV.setText(tempValue.getCreatedAt());
            holder.txtDateTimeTV1.setText(tempValue.getCreatedAt());

            if (position % 2 == 0) {
                holder.firstRL.setVisibility(View.VISIBLE);
                holder.SecondLL.setVisibility(View.GONE);

                holder.view1.setVisibility(View.VISIBLE);
                holder.view2.setVisibility(View.GONE);

            } else {
                holder.firstRL.setVisibility(View.GONE);
                holder.SecondLL.setVisibility(View.VISIBLE);

                holder.view1.setVisibility(View.GONE);
                holder.view2.setVisibility(View.VISIBLE);
            }

            if (tempValue.getPhoto().equals("") || tempValue.getPhoto() == null) {
                holder.imgIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.palace_holder));
            } else if (!tempValue.getPhoto().equals("")) {
                Glide.with(mActivity).load(tempValue.getPhoto()).into(holder.imgIV);
            }
            if (tempValue.getPhoto().equals("") || tempValue.getPhoto() == null) {
                holder.imgIV1.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.palace_holder));
            } else if (!tempValue.getPhoto().equals("")) {
                Glide.with(mActivity).load(tempValue.getPhoto()).into(holder.imgIV1);
            }
            String strNews = tempValue.getNewsText().trim();
            if (strNews != null && !strNews.equals("")) {
                holder.textNEWS.setText(html2text(strNews));
                holder.textNEWS1.setText(html2text(strNews));
            }

            if (JaoharConstants.IS_SEE_ALL_INTERVALS_NEWS_CLICK == true) {
                holder.editRL1.setVisibility(View.INVISIBLE);
                holder.editRL.setVisibility(View.INVISIBLE);
                holder.deleteRL1.setVisibility(View.INVISIBLE);
                holder.deleteRL.setVisibility(View.INVISIBLE);
            }

            holder.editRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mEditInternalNewsInterface.mEditNews(tempValue);
                }
            });
            holder.editRL1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mEditInternalNewsInterface.mEditNews(tempValue);
                }
            });


            holder.firstRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JaoharConstants.Is_click_form_NEWS = true;

                    mOpenNewsPopUP.openNewsPopUpInterFace(tempValue.getNews(), tempValue.getPhoto());
                }
            });

            holder.SecondLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JaoharConstants.Is_click_form_NEWS = true;
                    mOpenNewsPopUP.openNewsPopUpInterFace(tempValue.getNews(), tempValue.getPhoto());
                }
            });
            holder.firstRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JaoharConstants.Is_click_form_Internal_NEWS = true;

                    mOpenNewsPopUP.openNewsPopUpInterFace(tempValue.getNews(), tempValue.getPhoto());
                }
            });
            holder.SecondLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JaoharConstants.Is_click_form_Internal_NEWS = true;
                    mOpenNewsPopUP.openNewsPopUpInterFace(tempValue.getNews(), tempValue.getPhoto());

                }
            });

            holder.deleteRL1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDeleteInternalNewsInterface.mDeleteNews(tempValue);
                }
            });

            holder.deleteRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDeleteInternalNewsInterface.mDeleteNews(tempValue);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textNEWS, textNEWS1, txtEdit, txtDelete, txtTypeTV, txtDateTimeTV, txtDateTimeTV1, txtTypeTV1;
        public RelativeLayout SecondLL, firstRL, editRL, editRL1, deleteRL1, deleteRL;
        public SwipeLayout swipe_audio_layout;
        public ImageView imgIV, imgIV1;
        public View view1, view2;

        ViewHolder(View itemView) {
            super(itemView);
            deleteRL1 = (RelativeLayout) itemView.findViewById(R.id.deleteRL1);
            deleteRL = (RelativeLayout) itemView.findViewById(R.id.deleteRL);
            editRL = (RelativeLayout) itemView.findViewById(R.id.editRL);
            editRL1 = (RelativeLayout) itemView.findViewById(R.id.editRL1);
            SecondLL = (RelativeLayout) itemView.findViewById(R.id.SecondLL);
            firstRL = (RelativeLayout) itemView.findViewById(R.id.firstRL);
            textNEWS = (TextView) itemView.findViewById(R.id.textNEWS);
            textNEWS1 = (TextView) itemView.findViewById(R.id.textNEWS1);
            txtTypeTV = (TextView) itemView.findViewById(R.id.txtTypeTV);
            txtTypeTV1 = (TextView) itemView.findViewById(R.id.txtTypeTV1);
            txtDateTimeTV = (TextView) itemView.findViewById(R.id.txtDateTimeTV);
            txtDateTimeTV1 = (TextView) itemView.findViewById(R.id.txtDateTimeTV1);
            imgIV = (ImageView) itemView.findViewById(R.id.imgIV);
            imgIV1 = (ImageView) itemView.findViewById(R.id.imgIV1);
            view1 = itemView.findViewById(R.id.view1);
            view2 = itemView.findViewById(R.id.view2);
        }
    }

    //for converting html to text
    public CharSequence html2text(String html) {
        CharSequence trimmed = noTrailingwhiteLines(Html.fromHtml(html));
        return trimmed;
    }

    //for hiding extra white space
    private CharSequence noTrailingwhiteLines(CharSequence text) {

        if (text != null && !text.equals(""))
            while (text.charAt(text.length() - 1) == '\n') {
                text = text.subSequence(0, text.length() - 1);
            }
        return text;
    }
}