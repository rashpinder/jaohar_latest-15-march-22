package jaohar.com.jaohar.adapters.chat_module;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RelativeTimeTextView;
import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;
import jaohar.com.jaohar.interfaces.chat_module.ChatUsersListInterface;
import jaohar.com.jaohar.interfaces.chat_module.ClickChatUsersInterface;

public class AllChatParticipantsAdapter extends RecyclerView.Adapter<AllChatParticipantsAdapter.ViewHolder> {
    private Activity mActivity;
    ArrayList<ChatUsersModel> modelArrayList;
    ChatUsersListInterface mPagination;
    ClickChatUsersInterface clickChatUsersInterface;

    public AllChatParticipantsAdapter(Activity mActivity, ArrayList<ChatUsersModel> modelArrayList,
                                      ChatUsersListInterface mPagination, ClickChatUsersInterface clickChatUsersInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mPagination = mPagination;
        this.clickChatUsersInterface = clickChatUsersInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_users_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.DayTV.setVisibility(View.GONE);
        holder.totalMessagesTV.setVisibility(View.GONE);

        // pagination for smooth scrooling
        if (position >= modelArrayList.size() - 1) {
            mPagination.mChatUsersListInterface(true);
        }

        if (modelArrayList.get(position).getFirst_name() != null) {
            holder.NameTV.setText(modelArrayList.get(position).getFirst_name() + " " + modelArrayList.get(position).getLast_name());
        }

        if (modelArrayList.get(position).getMessage() != null) {
            holder.messageTV.setText(modelArrayList.get(position).getMessage());
        }

        if (modelArrayList.get(position).getImage() != null) {

            if ((modelArrayList.get(position).getImage().contains("https"))) {
                Glide.with(mActivity).load(modelArrayList.get(position).getImage()).placeholder(R.drawable.ic_user).into(holder.userProfile);
            } else {
                Glide.with(mActivity).load(modelArrayList.get(position).getImage().replace("http://", "https://"))
                        .placeholder(R.drawable.ic_user)
                        .into(holder.userProfile);
            }
        }

        if (modelArrayList.get(position).getOnline_state().equals("Available")) {
            holder.item_Image_Status.setBackgroundResource(R.drawable.bg_green_status);
        } else if (modelArrayList.get(position).getOnline_state().equals("Offline")) {
            holder.item_Image_Status.setBackgroundResource(R.drawable.bg_red_status);
        } else {
            holder.item_Image_Status.setVisibility(View.GONE);
        }

        holder.mainLayoutClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickChatUsersInterface.mChatUsersListInterface(position, modelArrayList);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mainLayoutClick;
        TextView NameTV, messageTV, totalMessagesTV;
        ImageView userProfile, item_Image_Status;
        RelativeTimeTextView DayTV;

        ViewHolder(View itemView) {
            super(itemView);

            mainLayoutClick = itemView.findViewById(R.id.mainLayoutClick);
            NameTV = itemView.findViewById(R.id.NameTV);
            messageTV = itemView.findViewById(R.id.messageTV);
            DayTV = itemView.findViewById(R.id.DayTV);
            totalMessagesTV = itemView.findViewById(R.id.totalMessagesTV);
            userProfile = itemView.findViewById(R.id.userProfile);
            item_Image_Status = itemView.findViewById(R.id.item_Image_Status);
        }
    }
}
