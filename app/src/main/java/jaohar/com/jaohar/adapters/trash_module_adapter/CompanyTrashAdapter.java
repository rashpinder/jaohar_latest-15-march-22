package jaohar.com.jaohar.adapters.trash_module_adapter;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.HashMap;


import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteCompanyTrashInterface;

import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteSelectedInterface;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.models.CompaniesData;

public class CompanyTrashAdapter extends RecyclerView.Adapter<CompanyTrashAdapter.ViewHolder> {

        private Activity mActivity;
        private ArrayList<CompaniesData> modelArrayList;

        private static HashMap<CompaniesData, Boolean> checkedForModel = new HashMap<>();
        RecoverDeleteCompanyTrashInterface mrecoverDelete;
        RecoverDeleteSelectedInterface mRecoverDeleteSelected;
        private static boolean[] checkBoxState = null;

         public CompanyTrashAdapter(Activity mActivity, ArrayList<CompaniesData> modelArrayList, RecoverDeleteCompanyTrashInterface mrecoverDelete, RecoverDeleteSelectedInterface mRecoverDeleteSelected) {
            this.mActivity = mActivity;
            this.modelArrayList = modelArrayList;

            this.mrecoverDelete = mrecoverDelete;
            this.mRecoverDeleteSelected = mRecoverDeleteSelected;
            checkBoxState=new boolean[modelArrayList.size()];
        }

        @Override
        public CompanyTrashAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_company_trash, parent, false);
            return new CompanyTrashAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final CompanyTrashAdapter.ViewHolder holder, final int position) {
            final CompaniesData tempValue = modelArrayList.get(position);

            holder.itemcompanyNameTV.setText(tempValue.getCompanyName());
            holder.deletedBYTV.setText(tempValue.getDeletedBy());
            holder.address1TV.setText(tempValue.getAddress1());
            holder.address2TV.setText(tempValue.getAddress2());
            holder.address3TV.setText(tempValue.getAddress3());


            holder.recoverIMG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mrecoverDelete.mRecoverDelete(tempValue,"recover");
                }
            });
            holder.deleteIMG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mrecoverDelete.mRecoverDelete(tempValue,"delete");
                }
            });

            holder.imgMultiSelectIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (
                        holder.imgMultiSelectIV.isChecked()) {
                        checkBoxState[position] = true;
                        ischecked(position,true);
                        mRecoverDeleteSelected.mrecoverDeletedCheckInterface(tempValue,false);
                    } else {
                        checkBoxState[position] = false;
                        ischecked(position,false);
                        mRecoverDeleteSelected.mrecoverDeletedCheckInterface(tempValue,true);

                    }

                }
            });
            /*if country is in checkedForCountry then set the checkBox to true */
            if (checkedForModel.get(tempValue) != null) {
                holder.imgMultiSelectIV.setChecked(checkedForModel.get(tempValue));
            }

            /*Set tag to all checkBox*/
            holder.imgMultiSelectIV.setTag(tempValue);


        }

        private void ischecked(int position,boolean flag )
        {
            checkedForModel.put(this.modelArrayList.get(position), flag);
        }

        @Override
        public int getItemCount() {
            return modelArrayList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public TextView itemcompanyNameTV, address1TV, address2TV, address3TV,deletedBYTV;
            public ImageView recoverIMG, viewIMG, deleteIMG;
            CheckBox imgMultiSelectIV;

            ViewHolder(View itemView) {
                super(itemView);
                recoverIMG = (ImageView) itemView.findViewById(R.id.recoverIMG);
                viewIMG = (ImageView) itemView.findViewById(R.id.viewIMG);
                deleteIMG = (ImageView) itemView.findViewById(R.id.deleteIMG);

                itemcompanyNameTV = (TextView) itemView.findViewById(R.id.itemcompanyNameTV);
                address1TV = (TextView) itemView.findViewById(R.id.address1TV);
                address2TV = (TextView) itemView.findViewById(R.id.address2TV);
                address3TV = (TextView) itemView.findViewById(R.id.address3TV);
                deletedBYTV = (TextView) itemView.findViewById(R.id.deletedBYTV);

                imgMultiSelectIV = (CheckBox) itemView.findViewById(R.id.imgMultiSelectIV);
            }
        }
}
