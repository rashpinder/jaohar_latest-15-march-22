package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.interfaces.AutoCompleteVesselInterface;


/**
 * Created by Dharmani Apps on 12/8/2017.
 */

public class AutoCompleteVesselSearchAdapter extends ArrayAdapter<VesselSearchInvoiceModel> {

    final String TAG = "AutocompleteCustomArrayAdapter.java";

    Activity mActivity;
    int layoutResourceId;
    ArrayList<VesselSearchInvoiceModel> mArrayList;
    AutoCompleteVesselInterface mAutoCompleteVesselInterface;
    public AutoCompleteVesselSearchAdapter(Activity mActivity, int layoutResourceId, ArrayList<VesselSearchInvoiceModel> mArrayList, AutoCompleteVesselInterface mAutoCompleteVesselInterface) {
        super(mActivity, layoutResourceId, mArrayList);
        this.layoutResourceId = layoutResourceId;
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mAutoCompleteVesselInterface = mAutoCompleteVesselInterface;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        try{

            /*
             * The convertView argument is essentially a "ScrapView" as described is Lucas post
             * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
             * It will have a non-null value when ListView is asking you recycle the row layout.
             * So, when convertView is not null, you should simply update its contents instead of inflating a new row layout.
             */
            if(convertView==null){
                // inflate the layout
                LayoutInflater inflater = mActivity.getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }

            // object item based on the position
            final VesselSearchInvoiceModel objectItem = mArrayList.get(position);

//            // get the TextView and then set the text (item name) and tag (item ID) values
//            final TextView textViewItem = (TextView) convertView.findViewById(R.id.textViewItem);
//            textViewItem.setText(objectItem.getVessel_name());
//
//
//            textViewItem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mAutoCompleteVesselInterface.autoCompleteVessel(textViewItem.getText().toString());
//                }
//            });


        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

            return convertView;

    }
}