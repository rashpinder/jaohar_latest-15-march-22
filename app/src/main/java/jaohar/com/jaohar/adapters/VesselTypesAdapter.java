package jaohar.com.jaohar.adapters;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.VesselTypesModel;
import jaohar.com.jaohar.interfaces.DeleteVesselTypeInterface;
import jaohar.com.jaohar.interfaces.EditVesselTypeInterface;
import jaohar.com.jaohar.models.GetVesselTypeData;


/**
 * Created by Dharmani Apps on 7/11/2017.
 **/

public class VesselTypesAdapter extends RecyclerView.Adapter<VesselTypesAdapter.ViewHolder> {
    Activity mActivity;
    private ArrayList<GetVesselTypeData> modelArrayList;
    private EditVesselTypeInterface mEditVesselTypeInterface;
    private DeleteVesselTypeInterface mDeleteVesselTypeInterface;

    public VesselTypesAdapter(Activity mActivity, ArrayList<GetVesselTypeData> modelArrayList, EditVesselTypeInterface mEditVesselTypeInterface, DeleteVesselTypeInterface mDeleteVesselTypeInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mEditVesselTypeInterface = mEditVesselTypeInterface;
        this.mDeleteVesselTypeInterface = mDeleteVesselTypeInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_status, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final GetVesselTypeData tempValue = modelArrayList.get(position);
        holder.item_Title.setText(tempValue.getName());
        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditVesselTypeInterface.mEditVesselTypeInterface(tempValue);
            }
        });
        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteVesselTypeInterface.mDeleteVesselTypeInterface(tempValue);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title, txtEdit, txtDelete;

        ViewHolder(View itemView) {
            super(itemView);

            item_Title = itemView.findViewById(R.id.item_Title);
            txtEdit = itemView.findViewById(R.id.txtEdit);
            txtDelete = itemView.findViewById(R.id.txtDelete);

        }
    }
}