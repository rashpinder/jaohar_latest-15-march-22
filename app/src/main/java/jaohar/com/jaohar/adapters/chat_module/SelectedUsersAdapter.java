package jaohar.com.jaohar.adapters.chat_module;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;
import jaohar.com.jaohar.interfaces.chat_module.RemoveParticipantInterface;

public class SelectedUsersAdapter extends RecyclerView.Adapter<SelectedUsersAdapter.ViewHolder> {
    private Activity mActivity;
    ArrayList<ChatUsersModel> mArrayList;
    String type;
    RemoveParticipantInterface removeParticipantInterface;

    public SelectedUsersAdapter(Activity mActivity, ArrayList<ChatUsersModel> mArrayList, String type,
                                RemoveParticipantInterface removeParticipantInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.type = type;
        this.removeParticipantInterface = removeParticipantInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_users_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        ChatUsersModel model = mArrayList.get(position);

        if (model.getFirst_name() != null)
            holder.nameTV.setText(model.getFirst_name() + " " + model.getLast_name());

        if (model.getImage() != null)
            Glide.with(mActivity).load(model.getImage()).placeholder(R.drawable.ic_user).into(holder.userIV);

        if (type.equals("1")) {
            holder.removeIV.setVisibility(View.VISIBLE);
        } else {
            holder.removeIV.setVisibility(View.GONE);
        }

        holder.removeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mArrayList.remove(position);
                notifyDataSetChanged();
                removeParticipantInterface.mRemoveParticipantInterface(position, mArrayList);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView userIV, removeIV;
        TextView nameTV;

        ViewHolder(View itemView) {
            super(itemView);

            userIV = itemView.findViewById(R.id.userIV);
            removeIV = itemView.findViewById(R.id.removeIV);
            nameTV = itemView.findViewById(R.id.nameTV);
        }
    }
}
