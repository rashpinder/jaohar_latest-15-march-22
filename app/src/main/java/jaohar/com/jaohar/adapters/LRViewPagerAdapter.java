package jaohar.com.jaohar.adapters;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;


public class LRViewPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    int layoutID[] = {};


    public LRViewPagerAdapter(Context context, int layoutID[]) {
        mContext = context;
        this.layoutID = layoutID;

        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemPosition(Object object) {
        if (Arrays.asList(object).contains((View) object)) {
            return Arrays.asList(object).indexOf((View) object);
        } else {
            return POSITION_NONE;
        }
    }

    @Override
    public int getCount() {
        return layoutID.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(layoutID[position], container,
                false);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


}
