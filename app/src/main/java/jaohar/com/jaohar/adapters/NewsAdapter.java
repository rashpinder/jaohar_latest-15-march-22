package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.daimajia.swipe.SwipeLayout;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.DeleteNewsInterface;
import jaohar.com.jaohar.interfaces.EditNewsInterface;
import jaohar.com.jaohar.interfaces.OpenNewsPopUpInterFace;
import jaohar.com.jaohar.models.AllNewsModel;
import jaohar.com.jaohar.models.Datum;
import jaohar.com.jaohar.utils.JaoharConstants;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    EditNewsInterface mEditNewsInterface;
    DeleteNewsInterface mDeleteNewsInterface;
    private Activity mActivity;
    private ArrayList<Datum> modelArrayList;
    OpenNewsPopUpInterFace mOpenNewsPopUP;

    public NewsAdapter(Activity mActivity, ArrayList<Datum> modelArrayList, EditNewsInterface mEditNewsInterface, DeleteNewsInterface mDeleteNewsInterface, OpenNewsPopUpInterFace mOpenNewsPopUP) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mEditNewsInterface = mEditNewsInterface;
        this.mDeleteNewsInterface = mDeleteNewsInterface;
        this.mOpenNewsPopUP = mOpenNewsPopUP;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item, parent, false);
        return new ViewHolder(v);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Datum tempValue = modelArrayList.get(position);
        holder.txtTypeTV1.setText("Type:" + " " + tempValue.getType());
        holder.txtTypeTV.setText("Type:" + " " + tempValue.getType());
        holder.txtDateTimeTV.setText(tempValue.getCreatedAt());
        holder.txtDateTimeTV1.setText(tempValue.getCreatedAt());

        if (position % 2 == 0) {
            holder.firstRL.setVisibility(View.VISIBLE);
            holder.SecondLL.setVisibility(View.GONE);

            holder.view1.setVisibility(View.VISIBLE);
            holder.view2.setVisibility(View.GONE);

        } else {
            holder.firstRL.setVisibility(View.GONE);
            holder.SecondLL.setVisibility(View.VISIBLE);

            holder.view1.setVisibility(View.GONE);
            holder.view2.setVisibility(View.VISIBLE);
        }
        if (tempValue.getPhoto().equals("")) {
            holder.imgIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.palace_holder));
        } else if (!tempValue.getPhoto().equals("")) {
//            Glide.with(mActivity)
//                    .load(tempValue.getPhoto())
//                    .asBitmap()
//                    .placeholder(R.drawable.palace_holder)
//                    .into(new SimpleTarget<Bitmap>() {
//                        @Override
//                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                            // you can do something with loaded bitmap here
//                            holder.imgIV.setImageBitmap(resource);
//                        }
//                    });
            Glide.with(mActivity).load(tempValue.getPhoto()).into(holder.imgIV);
        }
        if (tempValue.getPhoto().equals("")) {
            holder.imgIV1.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.palace_holder));
        } else if (!tempValue.getPhoto().equals("")) {
//            Glide.with(mActivity)
//                    .load(tempValue.getPhoto())
//                    .asBitmap()
//                    .placeholder(R.drawable.palace_holder)
//                    .into(new SimpleTarget<Bitmap>() {
//                        @Override
//                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                            // you can do something with loaded bitmap here
//                            holder.imgIV1.setImageBitmap(resource);
//                        }
//                    });
            Glide.with(mActivity).load(tempValue.getPhoto()).into(holder.imgIV1);
        }

        String strNews = tempValue.getNewsText().trim();
        holder.textNEWS.setText(strNews);
        holder.textNEWS1.setText(strNews);

        if (JaoharConstants.IS_SEE_ALL_NEWS_CLICK == true) {
            holder.editRL1.setVisibility(View.INVISIBLE);
            holder.editRL.setVisibility(View.INVISIBLE);
            holder.deleteRL1.setVisibility(View.INVISIBLE);
            holder.deleteRL.setVisibility(View.INVISIBLE);

        } else if (JaoharConstants.IS_CLICK_FROM_VESSELS_FOR_SALE == true) {
            holder.editRL1.setVisibility(View.INVISIBLE);
            holder.editRL.setVisibility(View.INVISIBLE);
            holder.deleteRL1.setVisibility(View.INVISIBLE);
            holder.deleteRL.setVisibility(View.INVISIBLE);

        } else if (JaoharConstants.IS_ACTIVITY_CLICK == true) {
            holder.editRL1.setVisibility(View.INVISIBLE);
            holder.editRL.setVisibility(View.INVISIBLE);
            holder.deleteRL1.setVisibility(View.INVISIBLE);
            holder.deleteRL.setVisibility(View.INVISIBLE);

        }
        holder.editRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditNewsInterface.mEditNews(tempValue);
            }
        });
        holder.editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditNewsInterface.mEditNews(tempValue);
            }
        });
        holder.deleteRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteNewsInterface.mDeleteNews(tempValue, position);
            }
        });
        holder.deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteNewsInterface.mDeleteNews(tempValue,position);
            }
        });
        holder.firstRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Is_click_form_NEWS = true;
                mOpenNewsPopUP.openNewsPopUpInterFace(tempValue.getNews(), tempValue.getPhoto());
            }
        });
        holder.SecondLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Is_click_form_NEWS = true;
                mOpenNewsPopUP.openNewsPopUpInterFace(tempValue.getNews(), tempValue.getPhoto());
            }
        });
    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textNEWS, textNEWS1, txtEdit, txtDelete, txtTypeTV, txtDateTimeTV, txtDateTimeTV1, txtTypeTV1;
        public RelativeLayout SecondLL, firstRL, editRL, editRL1, deleteRL1, deleteRL;
        public SwipeLayout swipe_audio_layout;
        public ImageView imgIV, imgIV1;
        public View view1, view2;

        ViewHolder(View itemView) {
            super(itemView);
            deleteRL1 = (RelativeLayout) itemView.findViewById(R.id.deleteRL1);
            deleteRL = (RelativeLayout) itemView.findViewById(R.id.deleteRL);
            editRL = (RelativeLayout) itemView.findViewById(R.id.editRL);
            editRL1 = (RelativeLayout) itemView.findViewById(R.id.editRL1);
            SecondLL = (RelativeLayout) itemView.findViewById(R.id.SecondLL);
            firstRL = (RelativeLayout) itemView.findViewById(R.id.firstRL);

            textNEWS = (TextView) itemView.findViewById(R.id.textNEWS);
            textNEWS1 = (TextView) itemView.findViewById(R.id.textNEWS1);

            txtTypeTV = (TextView) itemView.findViewById(R.id.txtTypeTV);
            txtTypeTV1 = (TextView) itemView.findViewById(R.id.txtTypeTV1);
            txtDateTimeTV = (TextView) itemView.findViewById(R.id.txtDateTimeTV);
            txtDateTimeTV1 = (TextView) itemView.findViewById(R.id.txtDateTimeTV1);

            imgIV = (ImageView) itemView.findViewById(R.id.imgIV);
            imgIV1 = (ImageView) itemView.findViewById(R.id.imgIV1);

            view1 = itemView.findViewById(R.id.view1);
            view2 = itemView.findViewById(R.id.view2);
        }
    }
}