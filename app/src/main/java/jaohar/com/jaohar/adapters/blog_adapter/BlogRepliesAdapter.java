package jaohar.com.jaohar.adapters.blog_adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.blog_module.Blog_Replies_Model;
import jaohar.com.jaohar.interfaces.blog_module.BlogRepliesdelInterface;
import jaohar.com.jaohar.utils.JaoharPreference;

public class BlogRepliesAdapter extends RecyclerView.Adapter<BlogRepliesAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<Blog_Replies_Model> modelArrayList;
    String strUserID = "";
    BlogRepliesdelInterface mDeleteRepliesInterface;
    public BlogRepliesAdapter(Activity mActivity, ArrayList<Blog_Replies_Model> modelArrayList,BlogRepliesdelInterface mDeleteRepliesInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDeleteRepliesInterface = mDeleteRepliesInterface;

    }

    @Override
    public BlogRepliesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_blog_comments, parent, false);
        return new BlogRepliesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final BlogRepliesAdapter.ViewHolder holder, final int position) {
        final Blog_Replies_Model tempValue = modelArrayList.get(position);

        if (JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "").equals("")) {
            strUserID = JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
        } else {
            strUserID = JaoharPreference.readString(mActivity, JaoharPreference.USER_ID, "");
        }

        // Maintain check to hide or show delete for users
        if (tempValue.getmUser_Detail_Model().getId().equals(strUserID)) {
            holder.deleteTV.setVisibility(View.VISIBLE);
        } else {
            holder.deleteTV.setVisibility(View.GONE);
        }


        holder.replyCommentRV.setVisibility(View.GONE);
        holder.replyTV.setVisibility(View.GONE);

//        holder.viewVL.setVisibility(View.GONE);
        holder.recyclerDataRL.setVisibility(View.GONE);

        long unix_seconds = Long.parseLong(tempValue.getPosted_on());

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(unix_seconds * 1000L);

//                      DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        DateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");

        String date = format.format(cal.getTime());
        holder.nameTV.setText(tempValue.getmUser_Detail_Model().getFirst_name() + " " + tempValue.getmUser_Detail_Model().getLast_name());
        holder.messageTV.setText(tempValue.getMessage());

        holder.dateTV.setText(date);

//        long unix_seconds = Long.parseLong(tempValue.getCreation_date());
//
//        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
//        cal.setTimeInMillis(unix_seconds * 1000L);
//
//        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
//        String date = format.format(cal.getTime());
//        holder.postedTimeTV.setText(" " +date);

        if (!tempValue.getmUser_Detail_Model().getImage().equals("")) {
            Glide.with(mActivity)
                    .load(tempValue.getmUser_Detail_Model().getImage())
                    .into(holder.profileMainImg);
        }

        holder.deleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDeleteRepliesInterface.mBlogRepliesInterface(tempValue);
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV, messageTV, replyTV, deleteTV, dateTV;
        public de.hdodenhof.circleimageview.CircleImageView profileMainImg;
        public RecyclerView replyCommentRV;
//        public View viewVL;
        public RelativeLayout recyclerDataRL;
        public LinearLayout reply_DelLL;


        ViewHolder(View itemView) {
            super(itemView);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            messageTV = (TextView) itemView.findViewById(R.id.messageTV);
            replyTV = (TextView) itemView.findViewById(R.id.replyTV);
            deleteTV = itemView.findViewById(R.id.deleteTV);
            profileMainImg = itemView.findViewById(R.id.profileMainImg);
            replyCommentRV = itemView.findViewById(R.id.replyCommentRV);
//            viewVL = itemView.findViewById(R.id.viewVL);
            recyclerDataRL = itemView.findViewById(R.id.recyclerDataRL);
            reply_DelLL = itemView.findViewById(R.id.reply_DelLL);
            dateTV = itemView.findViewById(R.id.dateTV);
        }
    }

}