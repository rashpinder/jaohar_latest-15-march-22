package jaohar.com.jaohar.adapters.forum_module_admin_adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Callback;
import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.ForumModule.ForumModel;
import jaohar.com.jaohar.interfaces.forumModule.ForumItemClickInterace;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.interfaces.forum_module_admin.CloseAndDeleteForumInterface;


public class ClosedForumListAdapter extends RecyclerView.Adapter<ClosedForumListAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<ForumModel> modelArrayList;
    PaginationListForumAdapter mPagination;
    ForumItemClickInterace mInterfaceForum;
    CloseAndDeleteForumInterface mCloseAndDelForum;

    public ClosedForumListAdapter(Activity mActivity, ArrayList<ForumModel> modelArrayList, PaginationListForumAdapter mPagination, ForumItemClickInterace mInterfaceForum,CloseAndDeleteForumInterface mCloseAndDelForum) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mPagination = mPagination;
        this.mInterfaceForum = mInterfaceForum;
        this.mCloseAndDelForum = mCloseAndDelForum;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_forum_admin, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ForumModel tempValue = modelArrayList.get(position);
        holder.setIsRecyclable(false);

        // pagination for smooth scrooling
        if (position >= modelArrayList.size() - 1) {
            mPagination.mPaginationforVessels(true);
        }

        holder.vesselNameTV.setText(tempValue.getVessel_name());

        if (!tempValue.getUnread_messages().equals("0")) {
            holder.totalMessagesTV.setText(tempValue.getUnread_messages());
        } else {
            holder.totalMessagesTV.setVisibility(View.GONE);
        }

        holder.imoNumTV.setText(tempValue.getIMO_number());

        if (tempValue.getPic() != null && tempValue.getPic().contains("http")) {
//            Picasso.with(mActivity)
//                    .load(modelArrayList.get(position).getPic())
//                    .placeholder(R.drawable.placeholder)
//                    .into(holder.forumIMG, getCallBack(holder.forumIMG));
            Glide.with(mActivity)
                    .load(modelArrayList.get(position).getPic())
                    .into(holder.forumIMG);
        } else {
            holder.forumIMG.setImageDrawable(mActivity.getDrawable(R.drawable.palace_holder));
        }

        /* *
         * Setting Up Click for Viewing Forum
         * */
        holder.mainLayoutClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterfaceForum.ForumItemClick(tempValue);
            }
        });

        holder.txtEdit.setVisibility(View.GONE);

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCloseAndDelForum.mCloseAndDeleteInterFace(tempValue,"delete");
            }
        });

        /*Vessels Status on Vessel Image*/
        if (tempValue.getStatus().equals("Sold")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.status_sold);
        } else if (tempValue.getStatus().equals("Withdrawn")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.status_withdrawn);
        } else if (tempValue.getStatus().equals("Committed")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.status_commited);
        } else if (tempValue.getStatus().equals("Scraped")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.scraped);
        } else if (tempValue.getStatus().equals("Hot Sale")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.icon_hot_sale);
        } else {
            holder.item_Image_Status.setVisibility(View.GONE);
        }
    }

    public Callback getCallBack(final ImageView imageView) {
        return new Callback() {
            @Override
            public void onSuccess() {
                ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .75f, ScaleAnimation.RELATIVE_TO_SELF, .75f);
                scale.setDuration(1500);
                imageView.startAnimation(scale);
            }

            @Override
            public void onError(Exception e) {

            }

        };
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView vesselNameTV, totalMessagesTV, imoNumTV,txtEdit,txtDelete;
        public ImageView item_Image_Status;
        public LinearLayout mainLayoutClick;
        public CircleImageView forumIMG;


        ViewHolder(View itemView) {
            super(itemView);

            forumIMG = (CircleImageView) itemView.findViewById(R.id.forumIMG);

            vesselNameTV = (TextView) itemView.findViewById(R.id.vesselNameTV);
            totalMessagesTV = (TextView) itemView.findViewById(R.id.totalMessagesTV);
            imoNumTV = (TextView) itemView.findViewById(R.id.imoNumTV);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);

            mainLayoutClick = (LinearLayout) itemView.findViewById(R.id.mainLayoutClick);
            item_Image_Status = (ImageView) itemView.findViewById(R.id.item_Image_Status);

        }
    }
}