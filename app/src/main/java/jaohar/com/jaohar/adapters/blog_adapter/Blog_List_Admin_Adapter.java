package jaohar.com.jaohar.adapters.blog_adapter;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import jaohar.com.jaohar.R;

import jaohar.com.jaohar.beans.blog_module.Blog_List_Model;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.interfaces.blog_module.Blog_Admin_DeleteInterface;
import jaohar.com.jaohar.interfaces.blog_module.Blog_Item_Click_Interface;


public class Blog_List_Admin_Adapter extends RecyclerView.Adapter<Blog_List_Admin_Adapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<Blog_List_Model> modelArrayList;
    PaginationListForumAdapter mPagination;
    Blog_Item_Click_Interface mBlogClickInterface;
    Blog_Admin_DeleteInterface mDeleteInterface;

    public Blog_List_Admin_Adapter(Activity mActivity, ArrayList<Blog_List_Model> modelArrayList, PaginationListForumAdapter mPagination, Blog_Item_Click_Interface mBlogClickInterface, Blog_Admin_DeleteInterface mDeleteInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mPagination = mPagination;
        this.mBlogClickInterface = mBlogClickInterface;
        this.mDeleteInterface = mDeleteInterface;
    }

    @Override
    public Blog_List_Admin_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_blog_list_admin, parent, false);
        return new Blog_List_Admin_Adapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final Blog_List_Admin_Adapter.ViewHolder holder, final int position) {
        final Blog_List_Model tempValue = modelArrayList.get(position);
        holder.setIsRecyclable(false);

        holder.blogHeadingTV.setText(tempValue.getTitle());
        holder.blog_descriptionTV.setText(tempValue.getContent());
        long unix_seconds = Long.parseLong(tempValue.getCreation_date());

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(unix_seconds * 1000L);

        DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        String date = format.format(cal.getTime());
        holder.postedTimeTV.setText(" " + date);
        // pagination for smooth scrooling
        if (position >= modelArrayList.size() - 1) {
            mPagination.mPaginationforVessels(true);
        }
        if (!tempValue.getImage().equals("")) {
            Glide.with(mActivity)
                    .load(tempValue.getImage()).placeholder(R.drawable.blog_small_icon)
                    .into(holder.blogImgIV);
        } else {
            holder.blogImgIV.setImageResource(R.drawable.blog_small_icon);
        }

        holder.read_moreTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBlogClickInterface.BlogInterface(tempValue);
            }
        });


        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDeleteInterface.mBlog_Admin(tempValue, "edit");
            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDeleteInterface.mBlog_Admin(tempValue, "del");
            }
        });

    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView blogHeadingTV, blog_descriptionTV, postedTimeTV, read_moreTV, txtEdit, txtDelete;
        public ImageView blogImgIV;
        public LinearLayout mainLayoutClick;
        public RecyclerView replyCommentRV;

        ViewHolder(View itemView) {
            super(itemView);
            blogHeadingTV = (TextView) itemView.findViewById(R.id.blogHeadingTV);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
            read_moreTV = (TextView) itemView.findViewById(R.id.read_moreTV);
            blog_descriptionTV = (TextView) itemView.findViewById(R.id.blog_descriptionTV);
            postedTimeTV = (TextView) itemView.findViewById(R.id.postedTimeTV);
            mainLayoutClick = (LinearLayout) itemView.findViewById(R.id.mainLayoutClick);
            blogImgIV = (ImageView) itemView.findViewById(R.id.blogImgIV);
            replyCommentRV = (RecyclerView) itemView.findViewById(R.id.replyCommentRV);
        }
    }
}