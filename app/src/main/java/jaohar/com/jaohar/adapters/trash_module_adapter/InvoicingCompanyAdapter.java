package jaohar.com.jaohar.adapters.trash_module_adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.activities.universal_invoice_module.Edit_InvocingCompanyActivity;
import jaohar.com.jaohar.interfaces.DeleteInvoiceCompanyInterFace;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.InvoiceCompanyModel;
import jaohar.com.jaohar.models.OwnersModel;

/**
 * Created by dharmaniz on 15/3/19.
 */

public class InvoicingCompanyAdapter  extends RecyclerView.Adapter<InvoicingCompanyAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<OwnersModel.Datum> modelArrayList;
    DeleteInvoiceCompanyInterFace mDeleteCompany;

    public InvoicingCompanyAdapter(Activity mActivity, ArrayList<OwnersModel.Datum> modelArrayList, DeleteInvoiceCompanyInterFace mDeleteCompany) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDeleteCompany = mDeleteCompany;

    }

    @Override
    public InvoicingCompanyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stamps, null);
        return new InvoicingCompanyAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(InvoicingCompanyAdapter.ViewHolder holder, int position) {
        final OwnersModel.Datum tempValue = modelArrayList.get(position);
        holder.item_Title.setText(tempValue.getOwnerName());

        holder.item_Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("owner_ID", tempValue.getOwnerId());
                returnIntent.putExtra("owner_NAME", tempValue.getOwnerName());
                mActivity.setResult(111, returnIntent);
                mActivity.finish();
            }
        });
        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mIntent = new Intent(mActivity, Edit_InvocingCompanyActivity.class);
                mIntent.putExtra("owner_ID", tempValue.getOwnerId());
                mActivity.startActivity(mIntent);
//                mEditInterface.editInterface(tempValue);
            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDeleteCompany.mDeleteInvoiceInterface(tempValue);
//                mDeleteInterface.deleteInterface(tempValue);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title, txtEdit, txtDelete;


        public ViewHolder(View itemView) {
            super(itemView);

            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
        }
    }
}