package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.SelectFromEmailInterface;

/**
 * Created by dharmaniz on 5/2/19.
 */

public class ShowfromDATAEmailsAdapter extends RecyclerView.Adapter<ShowfromDATAEmailsAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<String> modelArrayList;
    private ArrayList<String> idd;
    SelectFromEmailInterface mSeletFromEmail;

    public ShowfromDATAEmailsAdapter(Activity mActivity, ArrayList<String> modelArrayList, SelectFromEmailInterface mSeletFromEmail, ArrayList<String> idd) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mSeletFromEmail = mSeletFromEmail;
        this.idd = idd;

    }

    @Override
    public ShowfromDATAEmailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mail_list, parent, false);
        return new ShowfromDATAEmailsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ShowfromDATAEmailsAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        holder.listNameTV.setVisibility(View.GONE);
        holder.serialNOTV.setText(modelArrayList.get(position).toString());

        holder.mainLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSeletFromEmail.mSelectEmail(modelArrayList.get(position),idd.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if(modelArrayList.size()>=10){
            return 10;
        }else {
            return modelArrayList.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView serialNOTV, listNameTV;
        public LinearLayout mainLL;

        ViewHolder(View itemView) {
            super(itemView);
            mainLL = (LinearLayout) itemView.findViewById(R.id.mainLL);
            serialNOTV = (TextView) itemView.findViewById(R.id.serialNOTV);
            listNameTV = (TextView) itemView.findViewById(R.id.listNameTV);

        }
    }
}