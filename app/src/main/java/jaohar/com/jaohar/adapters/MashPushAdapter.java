package jaohar.com.jaohar.adapters;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.MassPushModel;
import jaohar.com.jaohar.fonts.TextViewPoppinsMedium;
import jaohar.com.jaohar.interfaces.DeleteMassPush;
import jaohar.com.jaohar.interfaces.EditInternalNewsInterface;
import jaohar.com.jaohar.interfaces.EditMassPushInterface;
import jaohar.com.jaohar.models.AllNotification;
import jaohar.com.jaohar.utils.Utilities;

/**
 * Created by dharmaniz on 24/10/18.
 */

public class MashPushAdapter extends RecyclerView.Adapter<MashPushAdapter.ViewHolder> {
    EditInternalNewsInterface mEditInternalNewsInterface;
    DeleteMassPush mDeleteMassPUSHInterface;
    EditMassPushInterface mEditInterFAce;
    private Activity mActivity;
    private ArrayList<AllNotification> modelArrayList;
    private String OUTPUT_DATE_FORMATE = "dd-MM-yyyy - hh:mm a";

    public MashPushAdapter(Activity mActivity, ArrayList<AllNotification> modelArrayList, DeleteMassPush mDeleteMassPUSHInterface, EditMassPushInterface mEditInterFAce) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mEditInterFAce = mEditInterFAce;
        this.mDeleteMassPUSHInterface = mDeleteMassPUSHInterface;
    }

    @Override
    public MashPushAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mass_push, parent, false);
        return new MashPushAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MashPushAdapter.ViewHolder holder, final int position) {
        final AllNotification tempValue = modelArrayList.get(position);


        holder.txtDateTV.setText(tempValue.getCreationDate());
        holder.txtTypeTV.setText(tempValue.getNotificationType());
        holder.txtRoleTV.setText(tempValue.getUserType());
        holder.txtMessageTV.setText(html2text(tempValue.getMessage()));

        holder.itemtxtConfirmTV.setText("EDIT");
        holder.itemtxtRejectTV.setText("DELETE");
        long my_string = Long.parseLong(tempValue.getCreationDate());

//        holder.itemNameTV.setText(Utilities.getDateFromUTCTimestamp(my_string, OUTPUT_DATE_FORMATE));

        holder.itemtxtConfirmTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditInterFAce.editMassPush(tempValue.getNotificationId());
            }
        });
        holder.itemtxtRejectTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteMassPUSHInterface.deleteMassPush(tempValue.getNotificationId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtDateTV, txtTypeTV, txtRoleTV, txtMessageTV, itemtxtConfirmTV, itemtxtRejectTV;

        ViewHolder(View itemView) {
            super(itemView);

            txtDateTV = itemView.findViewById(R.id.txtDateTV);
            txtTypeTV = itemView.findViewById(R.id.txtTypeTV);
            txtRoleTV = itemView.findViewById(R.id.txtRoleTV);
            txtMessageTV = itemView.findViewById(R.id.txtMessageTV);
            itemtxtConfirmTV = (TextView) itemView.findViewById(R.id.itemtxtConfirmTV);
            itemtxtRejectTV = (TextView) itemView.findViewById(R.id.itemtxtRejectTV);
        }
    }

    //for converting html to text
    public CharSequence html2text(String html) {
        CharSequence trimmed = noTrailingwhiteLines(Html.fromHtml(html));
        return trimmed;
    }

    //for hiding extra white space
    private CharSequence noTrailingwhiteLines(CharSequence text) {

        if (text != null && !text.equals(""))
            while (text.charAt(text.length() - 1) == '\n') {
                text = text.subSequence(0, text.length() - 1);
            }
        return text;
    }
}
