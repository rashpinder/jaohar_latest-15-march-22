package jaohar.com.jaohar.adapters.forum_module;

import static jaohar.com.jaohar.beans.ForumModule.ForumChatModel.Receiver;
import static jaohar.com.jaohar.beans.ForumModule.ForumChatModel.Sender;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Vibrator;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.GalleryActivity;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.forumModule.Add_Del_Edit_forumchatInterface;
import jaohar.com.jaohar.models.forummodels.AllMessagesItem;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;

public class ForumChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    int lastItemPosition = -1;
    private List<AllMessagesItem> modelArrayList;
    private ArrayList<String> mImageArryList = new ArrayList<String>();
    Add_Del_Edit_forumchatInterface mAdd_Del_EditInteface;
    OnClickInterface mOnClickInterface;
    Vibrator vibe;
    String strLastPage = "";

    public ForumChatAdapter(Context context, List<AllMessagesItem> modelArrayList,
                            Add_Del_Edit_forumchatInterface mAdd_Del_EditInteface,
                            String strLastPage, OnClickInterface mOnClickInterface) {
        this.context = context;
        this.modelArrayList = modelArrayList;
        this.mAdd_Del_EditInteface = mAdd_Del_EditInteface;
        this.strLastPage = strLastPage;
        this.mOnClickInterface = mOnClickInterface;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case Receiver:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_left_chat, parent, false);
                return new RecieverViewHolder(view);
            case Sender:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_right_chat, parent, false);
                return new SenderViewHolder(view);
        }
        return null;
    }

    public void updateLastPageData(String LastPage) {
        strLastPage = LastPage;
    }

    public static String formatToYesterdayOrToday(String datee) {
        Date dateTime = null;
        try {
            dateTime = new SimpleDateFormat("EEE hh:mma MMM d, yyyy").parse(datee);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("dd MMM, yyyy");
        String date = timeFormatter.format(dateTime);

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return "Today";
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday";
        } else {
            return date;
        }
    }

    public static String getDateFromUixTimeStamp(long unixSeconds) {
        // convert seconds to milliseconds
        Date date = new Date(unixSeconds * 1000L);
        // the format of your date
        SimpleDateFormat sdf = new SimpleDateFormat("EEE hh:mma MMM d, yyyy");
        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        String formattedDate = sdf.format(date);
        Log.e("TAG", "formattedDate: " + formattedDate);

        return formattedDate;
    }

    public static String getDateFromUixTimeStampFormat(long unixSeconds) {
        // convert seconds to milliseconds
        Date date = new Date(unixSeconds * 1000L);
        // the format of your date
        SimpleDateFormat sdf = new SimpleDateFormat("d MMM,yyyy");
        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        String formattedDate = sdf.format(date);
        Log.e("TAG", "formattedDate: " + formattedDate);

        return formattedDate;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final AllMessagesItem object = modelArrayList.get(position);

        mOnClickInterface.mOnClickInterface(position);

        vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (position > lastItemPosition) {
            // Scrolled Down
        } else {
            // Scrolled Up
        }
        lastItemPosition = position;
        if (object != null) switch (object.getIntType()) {
            /**************
             * TYPE_HEADER CASE
             * ****************/
            case Receiver:

                if (object != null) {
                    if (object.getCreationDate() != null && !object.getCreationDate().equals("")) {

                        ((RecieverViewHolder) holder).ChatDateTV.setText(formatToYesterdayOrToday(getDateFromUixTimeStamp(Long.parseLong(object.getCreationDate()))));

                        String thisDate = getDateFromUixTimeStampFormat(Long.parseLong(modelArrayList.get(position).getCreationDate()));
                        String nextDate = "";

                        if (position == 0) {
                            nextDate = thisDate;
                        } else {
                            if (modelArrayList.get(position - 1).getCreationDate() != null)
                                nextDate = getDateFromUixTimeStampFormat(Long.parseLong(modelArrayList.get(position - 1).getCreationDate()));
                        }

                        // enable section heading if it's the first one, or
                        // different from the previous one
                        if (nextDate.equals(thisDate)) {
                            if (strLastPage.equals("TRUE") && position == 0) {
                                ((RecieverViewHolder) holder).ChatDateTV.setVisibility(View.VISIBLE);
                            } else {
                                ((RecieverViewHolder) holder).ChatDateTV.setVisibility(View.GONE);
                            }
                        } else {
                            ((RecieverViewHolder) holder).ChatDateTV.setVisibility(View.VISIBLE);
                        }
                    }
                }

                if(object.getMessage() != null)
                ((RecieverViewHolder) holder).rightMessgeTv.setText(html2text(object.getMessage()));

                // convert seconds to milliseconds
                if (object.getCreationDate() != null && !object.getCreationDate().equals("")) {
                    long unix_seconds = Long.parseLong(object.getCreationDate());

                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(unix_seconds * 1000L);
                    DateFormat format = new SimpleDateFormat("hh:mm a");

                    String date = format.format(cal.getTime());
                    String strDATE = date.replace("am", "AM").replace("pm", "PM");

                    ((RecieverViewHolder) holder).timeUnSingleImageTV.setText(strDATE);
                    ((RecieverViewHolder) holder).timeReplySingleImageTV.setText(strDATE);
                    ((RecieverViewHolder) holder).leftChatImagesReplytimeTV.setText(strDATE);
                    ((RecieverViewHolder) holder).timeUnReplyMultipleImagesTV.setText(strDATE);
                    ((RecieverViewHolder) holder).leftChatTextReplytimeTV.setText(strDATE);
                }

                if (object.getEdited() != null) {
                    if (object.getEdited().equals("1")) {

                        ((RecieverViewHolder) holder).editedImage.setVisibility(View.VISIBLE);

                        /* to set edit icon according to the width of message */
                        if (object.getImageCount().equals("0") || object.getImageCount().equals("")) {

                            ((RecieverViewHolder) holder).rightMessgeTv.measure(0, 0);
                            int width = ((RecieverViewHolder) holder).rightMessgeTv.getMeasuredWidth();

                            if (width == context.getResources().getDimension(R.dimen._200sdp)
                                    || width > context.getResources().getDimension(R.dimen._200sdp)) {
                                LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._200sdp), LinearLayout.LayoutParams.WRAP_CONTENT);
                                ((RecieverViewHolder) holder).leftChatLL.setLayoutParams(layoutParams1);
//                                ((RecieverViewHolder) holder).unReplyTopLL.setLayoutParams(layoutParams1);
                            } else {
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                ((RecieverViewHolder) holder).leftChatLL.setLayoutParams(layoutParams);
//                                ((RecieverViewHolder) holder).unReplyTopLL.setLayoutParams(layoutParams);
                            }

                        } else {
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), LinearLayout.LayoutParams.WRAP_CONTENT);
                            ((RecieverViewHolder) holder).leftChatLL.setLayoutParams(layoutParams);
//                            ((RecieverViewHolder) holder).unReplyTopLL.setLayoutParams(layoutParams);
                        }

                        /* to set edit icon according to the width of message */
//                        if (object.getImageCount().equals("0") || object.getImageCount().equals("")) {
//
//                            ((RecieverViewHolder) holder).rightMessgeTv.measure(0, 0);
//                            int width = ((RecieverViewHolder) holder).rightMessgeTv.getMeasuredWidth();
//
//                            if (width == context.getResources().getDimension(R.dimen._200sdp)
//                                    || width > context.getResources().getDimension(R.dimen._200sdp)) {
//                                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._200sdp), RelativeLayout.LayoutParams.WRAP_CONTENT);
//                                layoutParams.addRule(RelativeLayout.RIGHT_OF, ((RecieverViewHolder) holder).imageUnReplyRL.getId());
//                                ((RecieverViewHolder) holder).unReplyTopLL.setLayoutParams(layoutParams);
//                            } else {
//                                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//                                layoutParams.addRule(RelativeLayout.RIGHT_OF, ((RecieverViewHolder) holder).imageUnReplyRL.getId());
//                                ((RecieverViewHolder) holder).unReplyTopLL.setLayoutParams(layoutParams);
//                            }
//
//                        } else {
//                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._200sdp), RelativeLayout.LayoutParams.WRAP_CONTENT);
//                            layoutParams.addRule(RelativeLayout.RIGHT_OF, ((RecieverViewHolder) holder).imageUnReplyRL.getId());
//                            ((RecieverViewHolder) holder).unReplyTopLL.setLayoutParams(layoutParams);
//                        }

                    } else {
                        ((RecieverViewHolder) holder).editedImage.setVisibility(View.GONE);
                    }
                }

                if (object.getReplyFor() != null) {
                    ((RecieverViewHolder) holder).unReplyViewLL.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).replyLL.setVisibility(View.VISIBLE);
                    ((RecieverViewHolder) holder).ReplyViewLL.setVisibility(View.VISIBLE);

                    ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                    /* show message layout with images for reply case*/
                    ((RecieverViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);


                    ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.GONE);

                    ((RecieverViewHolder) holder).imagePicRight.setVisibility(View.GONE);

                    ((RecieverViewHolder) holder).rightMessgeTv.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).leftChatLL.setVisibility(View.GONE);

                    if (object.getReplyFor().getUserDetail().getId().equals(JaoharPreference.readString(context, JaoharPreference.STAFF_ID, ""))) {
                        ((RecieverViewHolder) holder).lefttimeReplyTV.setText("You");
                        ((RecieverViewHolder) holder).imageReplyRL.setVisibility(View.GONE);
                    } else if (object.getReplyFor().getUserDetail().getId().equals(JaoharPreference.readString(context, JaoharPreference.ADMIN_ID, ""))) {
                        ((RecieverViewHolder) holder).lefttimeReplyTV.setText("You");
                        ((RecieverViewHolder) holder).imageReplyRL.setVisibility(View.GONE);
                    } else {
                        ((RecieverViewHolder) holder).lefttimeReplyTV.setText(object.getReplyFor().getUserDetail().getFirstName());
                        ((RecieverViewHolder) holder).imageReplyRL.setVisibility(View.VISIBLE);
                    }

                    if(object.getReplyFor().getMessage() != null) {
                        ((RecieverViewHolder) holder).messageReplyTV.setText(html2text(object.getReplyFor().getMessage()));
                        ((RecieverViewHolder) holder).leftChatReplyTV.setText(html2text(object.getReplyFor().getMessage()));
                    }

                    if(object.getMessage() != null){
                        ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setText(html2text(object.getMessage()));
                        ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setText(html2text(object.getMessage()));
                    }

                    if (object.getImageCount() != null) {
                        if (object.getImageCount().equals("1")) {
                            ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.VISIBLE);
                            /* show 1 image layout with time */
                            ((RecieverViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.VISIBLE);

                            /* hide multiple images layout incase of 1 image */
                            ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.GONE);

                            ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                            /* show message layout with images for reply case*/
                            ((RecieverViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.GONE);
                            /* hide replied message layout */
                            ((RecieverViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.GONE);

                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage()))
                                    ((RecieverViewHolder) holder).itemReplyPICIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemReplyPICIV);
                                }

                            } else {
                                ((RecieverViewHolder) holder).itemReplyPICIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("2")) {
                            ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.GONE);
                            /* hide 1 image layout with time */
                            ((RecieverViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.GONE);

                            ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                            /* set height of layout for two images */
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._80sdp));
                            ((RecieverViewHolder) holder).multiImageReplyPicRL.setLayoutParams(layoutParams);

                            /* show time in reply case for multiple images */
                            ((RecieverViewHolder) holder).leftChatImagesReplytimeTV.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).image2ReplyPicLL.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                            /* show message layout with images for reply case*/
                            ((RecieverViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);


                            ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.GONE);
                            /* hide replied message layout */
                            ((RecieverViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.GONE);

                            // For Reply Pic Layout for two image shown
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Glide.with(context)
                                            .load(object.getImage())
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic22IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // For Reply Pic Layout for two image shown
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Glide.with(context)
                                            .load(object.getImage2())
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic21IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("3")) {
                            ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.GONE);
                            /* hide 1 image layout with time */
                            ((RecieverViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.GONE);

                            ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                            /* set height of layout for two images */
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._80sdp));
                            ((RecieverViewHolder) holder).multiImageReplyPicRL.setLayoutParams(layoutParams);

                            /* show time in reply case for multiple images */
                            ((RecieverViewHolder) holder).leftChatImagesReplytimeTV.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).image2ReplyPicLL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).image3ReplyPicLL.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                            /* show message layout with images for reply case*/
                            ((RecieverViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);


                            ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.GONE);
                            /* hide replied message layout */
                            ((RecieverViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.GONE);


                            // For Reply Pic Layout for Three image shown
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic31IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // For Reply Pic Layout for Three image shown
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic32IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // For Reply Pic Layout for Three image shown
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic33IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else if (object.getImageCount().equals("4")) {
                            ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.GONE);
                            /* hide 1 image layout with time */
                            ((RecieverViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.GONE);

                            ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                            /* set height of layout for two images */
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._160sdp));
                            ((RecieverViewHolder) holder).multiImageReplyPicRL.setLayoutParams(layoutParams);

                            /* show time in reply case for multiple images */
                            ((RecieverViewHolder) holder).leftChatImagesReplytimeTV.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).image2ReplyPicLL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).image3ReplyPicLL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                            /* show message layout with images for reply case*/
                            ((RecieverViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.GONE);
                            /* hide replied message layout */
                            ((RecieverViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.GONE);


                            //for 4 image count == for hiding +1
                            ((RecieverViewHolder) holder).blurRL.setVisibility(View.GONE);

                            // For Reply Pic Layout for Four image shown
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic41IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // For Reply Pic Layout for Four image shown
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic42IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // For Reply Pic Layout for Four image shown
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic43IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // For Reply Pic Layout for Four image shown
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage4()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic44IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }


                        } else if (Integer.parseInt(object.getImageCount()) >= 5) {
                            ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.GONE);
                            /* hide 1 image layout with time */
                            ((RecieverViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.GONE);

                            ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                            /* set height of layout for two images */
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._160sdp));
                            ((RecieverViewHolder) holder).multiImageReplyPicRL.setLayoutParams(layoutParams);

                            /* show time in reply case for multiple images */
                            ((RecieverViewHolder) holder).leftChatImagesReplytimeTV.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).image2ReplyPicLL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).image3ReplyPicLL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);
                            ((RecieverViewHolder) holder).blurReplyPicRL.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                            /* show message layout with images for reply case*/
                            ((RecieverViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.GONE);
                            /* hide replied message layout */
                            ((RecieverViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.GONE);


                            int imageCount = Integer.parseInt(object.getImageCount()) - 4;
                            String strImageCount = "+" + imageCount;
                            ((RecieverViewHolder) holder).textCountReplyPicTv.setText(strImageCount);

                            // For Reply Pic Layout for Four image shown
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic41IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // For Reply Pic Layout for Four image shown
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic42IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // For Reply Pic Layout for Four image shown
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic43IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // For Reply Pic Layout for Four image shown
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4()))
                                    ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage4()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemImageReplyPic44IV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        } else {
                            ((RecieverViewHolder) holder).itemReplyPICIV.setVisibility(View.GONE);
                            /* hide 1 image layout with time */
                            ((RecieverViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.GONE);

                            ((RecieverViewHolder) holder).multiImageReplyPicRL.setVisibility(View.GONE);

                            ((RecieverViewHolder) holder).image2ReplyPicLL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).image3ReplyPicLL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).imageReplyPic4LL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).blurReplyPicRL.setVisibility(View.GONE);

                            ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.GONE);
                            /* hide message layout with images for reply case*/
                            ((RecieverViewHolder) holder).leftChatTextRLL.setVisibility(View.GONE);

                            ((RecieverViewHolder) holder).rightMessgUnReplyTextTv.setVisibility(View.VISIBLE);
                            /* show replied message layout */
                            ((RecieverViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.VISIBLE);
                        }
                    }

                    if (object.getMessage() != null && !object.getMessage().equals("")) {
                        ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.VISIBLE);
                        /* show message layout with images for reply case*/
                        ((RecieverViewHolder) holder).leftChatTextReplytimeTV.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);
                        /* show time in reply case for multiple images */
                        ((RecieverViewHolder) holder).leftChatImagesReplytimeTV.setVisibility(View.GONE);
                        /* hide time of one image if message is there */
                        ((RecieverViewHolder) holder).timeReplySingleImageTV.setVisibility(View.GONE);
                    } else {
                        ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.GONE);
                        /* hide message layout with images for reply case*/
                        ((RecieverViewHolder) holder).leftChatTextReplytimeTV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).leftChatTextRLL.setVisibility(View.GONE);
                        /* show time in reply case for multiple images */
                        ((RecieverViewHolder) holder).leftChatImagesReplytimeTV.setVisibility(View.VISIBLE);
                        /* hide time of one image if message is there */
                        ((RecieverViewHolder) holder).timeReplySingleImageTV.setVisibility(View.VISIBLE);
                    }

                    if (object.getReplyFor() != null) {
                        if (object.getReplyFor().getMessage() != null && !object.getReplyFor().getMessage().equals("")) {
                            ((RecieverViewHolder) holder).leftChatReplyTV.setVisibility(View.VISIBLE);
                            ((RecieverViewHolder) holder).leftChatReplyTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        } else {
                            ((RecieverViewHolder) holder).leftChatReplyTV.setText(R.string.photo);
                            ((RecieverViewHolder) holder).leftChatReplyTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_photo_camera, 0, 0, 0);
                        }
                    }
                    if (!object.getReplyFor().getImageCount().equals("")) {
                        if (!object.getReplyFor().getUserDetail().getImage().equals("")) {
                            Picasso.get().load(object.getReplyFor().getUserDetail().getImage()).fit().centerCrop()
                                    .placeholder(R.drawable.palace_holder)
                                    .error(R.drawable.palace_holder)
                                    .into(((RecieverViewHolder) holder).imagePicLeftReply);
                        }


                        if (object.getReplyFor().getImageCount().equals("0")) {
                            ((RecieverViewHolder) holder).imageRplyRL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).ReplyLL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).onlyMsgReplyLL.setVisibility(View.VISIBLE);


                        } else if (object.getReplyFor().getImageCount().equals("1")) {
                            ((RecieverViewHolder) holder).blurPicRL.setVisibility(View.GONE);
                            ((RecieverViewHolder) holder).imageRplyRL.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).ReplyLL.setVisibility(View.VISIBLE);
                            ((RecieverViewHolder) holder).onlyMsgReplyLL.setVisibility(View.GONE);


                            // Image 1 Check
                            if (!object.getReplyFor().getImage().isEmpty())
                                if (!Utilities.isDocAdded(object.getReplyFor().getImage())) {
                                    if (Utilities.isVideoAdded(object.getReplyFor().getImage()))
                                        ((RecieverViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    else {
                                        Picasso.get().load(object.getReplyFor().getImage()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((RecieverViewHolder) holder).itemMessagePicLeftReplyIV);
                                    }
                                } else {
                                    ((RecieverViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }


                        } else if (Integer.parseInt(object.getReplyFor().getImageCount()) >= 2) {
                            ((RecieverViewHolder) holder).imageRplyRL.setVisibility(View.VISIBLE);

                            ((RecieverViewHolder) holder).ReplyLL.setVisibility(View.VISIBLE);
                            ((RecieverViewHolder) holder).onlyMsgReplyLL.setVisibility(View.GONE);

                            ((RecieverViewHolder) holder).blurPicRL.setVisibility(View.VISIBLE);
                            int imageCount = Integer.parseInt(object.getReplyFor().getImageCount()) - 1;
                            String strImageCount = "+" + imageCount;
                            ((RecieverViewHolder) holder).textCountPicTv.setText(strImageCount);

                            // Image 1 Check
                            if (!Utilities.isDocAdded(object.getReplyFor().getImage())) {
                                if (Utilities.isVideoAdded(object.getReplyFor().getImage()))
                                    ((RecieverViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getReplyFor().getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemMessagePicLeftReplyIV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }
                        }
                    }
                } else {

                    ((RecieverViewHolder) holder).unReplyViewLL.setVisibility(View.VISIBLE);
                    ((RecieverViewHolder) holder).ReplyViewLL.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).replyLL.setVisibility(View.GONE);

                    ((RecieverViewHolder) holder).rightMessgeReplyTextTv.setVisibility(View.GONE);
                    /* show message layout with images for reply case*/
                    ((RecieverViewHolder) holder).leftChatTextRLL.setVisibility(View.GONE);

                    ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.VISIBLE);
                    ((RecieverViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.VISIBLE);

                    ((RecieverViewHolder) holder).rightMessgeTv.setVisibility(View.VISIBLE);
                    ((RecieverViewHolder) holder).leftChatLL.setVisibility(View.VISIBLE);

                    /* hide time with single image */
                    ((RecieverViewHolder) holder).timeUnSingleImageTV.setVisibility(View.GONE);
                    /* hide time with multiple images */
                    ((RecieverViewHolder) holder).timeUnReplyMultipleImagesTV.setVisibility(View.GONE);

                    ((RecieverViewHolder) holder).imagePicRight.setVisibility(View.VISIBLE);
                }

                if (object.getUserDetail() != null && !object.getUserDetail().getImage().equals("")) {
                    Glide.with(context).load(object.getUserDetail().getImage()).placeholder(R.drawable.ic_user).into(((RecieverViewHolder) holder).imagePicRight);
                    Glide.with(context).load(object.getUserDetail().getImage()).placeholder(R.drawable.ic_user).into(((RecieverViewHolder) holder).imagePicRightReply1);
                }

                if (object.getImageCount() != null && object.getImageCount().equals("0") || object.getImageCount().equals("")) {

                    if (object.getMessage() != null)
                        ((RecieverViewHolder) holder).rightMessgeTv.setText(html2text(object.getMessage()));

                    ((RecieverViewHolder) holder).rightMessgeTv.setGravity(Gravity.START);
                } else {

                    if (object.getMessage() != null)
                        ((RecieverViewHolder) holder).rightMessgeTv.setText(html2text(object.getMessage()));

                    ((RecieverViewHolder) holder).rightMessgeTv.setGravity(Gravity.START);
                }

                if (object.getMessage() != null && !object.getMessage().equals("")) {
                    ((RecieverViewHolder) holder).rightMessgeTv.setVisibility(View.VISIBLE);
                    ((RecieverViewHolder) holder).leftChatLL.setVisibility(View.VISIBLE);

                    /* hide time with single image */
                    ((RecieverViewHolder) holder).timeUnSingleImageTV.setVisibility(View.GONE);
                    /* hide time with multiple images */
                    ((RecieverViewHolder) holder).timeUnReplyMultipleImagesTV.setVisibility(View.GONE);

                } else {
                    ((RecieverViewHolder) holder).rightMessgeTv.setVisibility(View.GONE);
                    ((RecieverViewHolder) holder).leftChatLL.setVisibility(View.GONE);

                    /* show time with single image */
                    ((RecieverViewHolder) holder).timeUnSingleImageTV.setVisibility(View.VISIBLE);
                    /* show time with multiple images */
                    ((RecieverViewHolder) holder).timeUnReplyMultipleImagesTV.setVisibility(View.VISIBLE);
                }

                ((RecieverViewHolder) holder).menuRightLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, view, "receiver", position);
                    }
                });

                ((RecieverViewHolder) holder).unReplyViewLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        vibe.vibrate(150);
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).multiImageRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        vibe.vibrate(150);
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).ReplyViewLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        vibe.vibrate(150);
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).itemMessagePicIV.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        vibe.vibrate(150);
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).multiImageReplyPicRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        vibe.vibrate(150);
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        return true;
                    }
                });

                ((RecieverViewHolder) holder).itemReplyPICIV.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        vibe.vibrate(150);
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "receiver", position);
                        return true;
                    }
                });

                // convert seconds to milliseconds
                if (object.getCreationDate() != null && !object.getCreationDate().equals("")) {
                    long unix_seconds = Long.parseLong(object.getCreationDate());

                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(unix_seconds * 1000L);

//                      DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
//                    DateFormat format = new SimpleDateFormat("dd MMM, hh:mm a");
                    DateFormat format = new SimpleDateFormat("hh:mm a");

                    String date = format.format(cal.getTime());
                    String strDATE = date.replace("am", "PM").replace("pm", "PM");

//                    ((RecieverViewHolder) holder).timeRightTV.setText(object.getUserDetail().getFirst_name() + " " + strDATE);
//                    ((RecieverViewHolder) holder).timeRightReplyTV.setText(object.getUserDetail().getFirst_name() + " " + strDATE);

                    ((RecieverViewHolder) holder).timeRightTV.setText(strDATE);
                    ((RecieverViewHolder) holder).timeRightReplyTV.setText(strDATE);
                }

                if (object.getImageCount() != null && !object.getImageCount().equals("")) {
                    if (object.getImageCount().equals("1")) {

                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.VISIBLE);

                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.GONE);

                        if (!object.getImage().isEmpty())
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage()))
                                    ((RecieverViewHolder) holder).itemMessagePicIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((RecieverViewHolder) holder).itemMessagePicIV);
                                }
                            } else {
                                ((RecieverViewHolder) holder).itemMessagePicIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                        /* to manage corners of imageview */
                        if (!object.getMessage().equals("")) {
                            ((RecieverViewHolder) holder).itemMessagePicIV.setCornerRadius(
                                    (int) context.getResources().getDimension(R.dimen._8sdp),
                                    (int) context.getResources().getDimension(R.dimen._8sdp),
                                    0, 0);
                        } else {
                            ((RecieverViewHolder) holder).itemMessagePicIV.setCornerRadius(
                                    (int) context.getResources().getDimension(R.dimen._8sdp),
                                    (int) context.getResources().getDimension(R.dimen._8sdp),
                                    (int) context.getResources().getDimension(R.dimen._8sdp),
                                    (int) context.getResources().getDimension(R.dimen._8sdp));
                        }

                    } else if (object.getImageCount().equals("2")) {
                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.GONE);

                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.VISIBLE);
                        /* set height for just two images */
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._80sdp));
                        ((RecieverViewHolder) holder).multiImageRL.setLayoutParams(layoutParams);

                        ((RecieverViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage2LL.setVisibility(View.VISIBLE);
//                        ((RecieverViewHolder) holder).relativeImage2RL.setVisibility(View.VISIBLE);

                        // Image 1 Check
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageRight21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Glide.with(context)
                                        .load(object.getImage())
                                        .placeholder(R.drawable.palace_holder)
                                        .listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                Log.e("TAG", "Load failed", e);
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                return false;
                                            }
                                        })
                                        .into(((RecieverViewHolder) holder).itemImageRight21IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image 2 Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageRight21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Glide.with(context)
                                        .load(object.getImage2())
                                        .placeholder(R.drawable.palace_holder)
                                        .listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                Log.e("TAG", "Load failed", e);
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                return false;
                                            }
                                        })
                                        .into(((RecieverViewHolder) holder).itemImageRight22IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!object.getMessage().equals("")) {
                            ((RecieverViewHolder) holder).itemImageRight21IV.setCornerRadius(
                                    0, (int) context.getResources().getDimension(R.dimen._15sdp), 0, 0);

                            ((RecieverViewHolder) holder).itemImageRight22IV.setCornerRadius(
                                    (int) context.getResources().getDimension(R.dimen._15sdp), 0, 0, 0);

                        } else {
                            ((RecieverViewHolder) holder).itemImageRight21IV.setCornerRadius(
                                    0, (int) context.getResources().getDimension(R.dimen._15sdp),
                                    0, (int) context.getResources().getDimension(R.dimen._15sdp));

                            ((RecieverViewHolder) holder).itemImageRight22IV.setCornerRadius(
                                    (int) context.getResources().getDimension(R.dimen._15sdp), 0,
                                    (int) context.getResources().getDimension(R.dimen._15sdp), 0);
                        }

                    } else if (object.getImageCount().equals("3")) {
                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.GONE);

                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.VISIBLE);
                        /* set height for just three images */
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._80sdp));
                        ((RecieverViewHolder) holder).multiImageRL.setLayoutParams(layoutParams);


                        ((RecieverViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage3LL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
//                        ((RecieverViewHolder) holder).relativeImage2RL.setVisibility(View.GONE);

                        // Image 1 Check
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageRight31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight31IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image 2 Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageRight32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage2()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight32IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image 3 Check
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((RecieverViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage3()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight33IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!object.getMessage().equals("")) {
//                            ((RecieverViewHolder) holder).blur3IV.setCornerRadius(
//                                    0, (int) context.getResources().getDimension(R.dimen._15sdp), 0, 0);

                            ((RecieverViewHolder) holder).blur3IV.setBackgroundResource(R.drawable.bg_tras_with_message);

                            ((RecieverViewHolder) holder).itemImageRight32IV.setCornerRadius(
                                    0, (int) context.getResources().getDimension(R.dimen._15sdp), 0, 0);

                            ((RecieverViewHolder) holder).itemImageRight31IV.setCornerRadius(
                                    (int) context.getResources().getDimension(R.dimen._15sdp), 0, 0, 0);

                        } else {
//                            ((RecieverViewHolder) holder).blur3IV.setCornerRadius(
//                                    0, (int) context.getResources().getDimension(R.dimen._15sdp),
//                                    0, (int) context.getResources().getDimension(R.dimen._15sdp));
                            ((RecieverViewHolder) holder).blur3IV.setBackgroundResource(R.drawable.bg_tras_with_images);

                            ((RecieverViewHolder) holder).itemImageRight32IV.setCornerRadius(
                                    0, (int) context.getResources().getDimension(R.dimen._15sdp),
                                    0, (int) context.getResources().getDimension(R.dimen._15sdp));

                            ((RecieverViewHolder) holder).itemImageRight31IV.setCornerRadius(
                                    (int) context.getResources().getDimension(R.dimen._15sdp), 0,
                                    (int) context.getResources().getDimension(R.dimen._15sdp), 0);
                        }

                    } else if (object.getImageCount().equals("4")) {
                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.GONE);

                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.VISIBLE);
                        /* set height for four images */
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._160sdp));
                        ((RecieverViewHolder) holder).multiImageRL.setLayoutParams(layoutParams);

                        ((RecieverViewHolder) holder).linearImage4LL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
//                        ((RecieverViewHolder) holder).relativeImage2RL.setVisibility(View.GONE);

                        //for 4 image count == for hiding +1
                        ((RecieverViewHolder) holder).blurRL.setVisibility(View.GONE);

                        // Image 1 Check

                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight41IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image 2 Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage2()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight42IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image 3 Check
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((RecieverViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage3()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight43IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        // Image 4 Check
                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((RecieverViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage4()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight44IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!object.getMessage().equals("")) {

                            ((RecieverViewHolder) holder).itemImageRight43IV.setCornerRadius(
                                    0, 0, 0, 0);

                            ((RecieverViewHolder) holder).itemImageRight44IV.setCornerRadius(
                                    0, 0, 0, 0);

                        } else {
                            ((RecieverViewHolder) holder).itemImageRight43IV.setCornerRadius(0, 0,
                                    (int) context.getResources().getDimension(R.dimen._15sdp), 0);

                            ((RecieverViewHolder) holder).itemImageRight44IV.setCornerRadius(0, 0,
                                    0, (int) context.getResources().getDimension(R.dimen._15sdp));
                        }

                    } else if (Integer.parseInt(object.getImageCount()) >= 5) {
                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.GONE);

                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.VISIBLE);
                        /* set height for more than four images */
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._160sdp));
                        ((RecieverViewHolder) holder).multiImageRL.setLayoutParams(layoutParams);

                        ((RecieverViewHolder) holder).linearImage4LL.setVisibility(View.VISIBLE);
                        ((RecieverViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
//                        ((RecieverViewHolder) holder).relativeImage2RL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).blurRL.setVisibility(View.VISIBLE);

                        int imageCount = Integer.parseInt(object.getImageCount()) - 4;
                        String strImageCount = "+" + imageCount;
                        ((RecieverViewHolder) holder).textCountTv.setText(strImageCount);


                        // Image 1 Check

                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((RecieverViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight41IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image 2 Check
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((RecieverViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage2()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight42IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image 3 Check
                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((RecieverViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage3()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight43IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // Image 4 Check
                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((RecieverViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {
                                Picasso.get().load(object.getImage4()).fit().centerCrop()
                                        .placeholder(R.drawable.palace_holder)
                                        .error(R.drawable.palace_holder)
                                        .into(((RecieverViewHolder) holder).itemImageRight44IV);
                            }
                        } else {
                            ((RecieverViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!object.getMessage().equals("")) {

                            ((RecieverViewHolder) holder).blurIV.setCornerRadius(
                                    0, 0, 0, 0);

                            ((RecieverViewHolder) holder).itemImageRight43IV.setCornerRadius(
                                    0, 0, 0, 0);

                            ((RecieverViewHolder) holder).itemImageRight44IV.setCornerRadius(
                                    0, 0, 0, 0);

                        } else {

                            ((RecieverViewHolder) holder).blurIV.setCornerRadius(0, 0,
                                    0, (int) context.getResources().getDimension(R.dimen._15sdp));

                            ((RecieverViewHolder) holder).itemImageRight43IV.setCornerRadius(0, 0,
                                    (int) context.getResources().getDimension(R.dimen._15sdp), 0);

                            ((RecieverViewHolder) holder).itemImageRight44IV.setCornerRadius(0, 0,
                                    0, (int) context.getResources().getDimension(R.dimen._15sdp));
                        }

                    } else if (object.getImageCount().equals("0")) {
                        ((RecieverViewHolder) holder).itemMessagePicIV.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.GONE);

                        ((RecieverViewHolder) holder).multiImageRL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                        ((RecieverViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
//                        ((RecieverViewHolder) holder).relativeImage2RL.setVisibility(View.GONE);
                    }
                }

                ((RecieverViewHolder) holder).multiImageRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImageCount().equals("1")) {
                            // Image 1 Click
                            mImageArryList.add(object.getImage());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("2")) {
                            // Image 2 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("3")) {
                            // Image 3 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("4")) {
                            // Image 4 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("5")) {
                            // Image 5 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("6")) {
                            // Image 6 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("7")) {
                            // Image 7 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("8")) {
                            // Image 8 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("9")) {
                            // Image 9 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("10")) {
                            // Image 10 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            mImageArryList.add(object.getImage10());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        }
                    }
                });

                ((RecieverViewHolder) holder).multiImageReplyPicRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImageCount().equals("1")) {
                            // Image 1 Click
                            mImageArryList.add(object.getImage());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("2")) {
                            // Image 2 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("3")) {
                            // Image 3 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("4")) {
                            // Image 4 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("5")) {
                            // Image 5 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("6")) {
                            // Image 6 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("7")) {
                            // Image 7 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("8")) {
                            // Image 8 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("9")) {
                            // Image 9 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("10")) {
                            // Image 10 Click
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            mImageArryList.add(object.getImage10());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        }
                    }
                });

                ((RecieverViewHolder) holder).itemMessagePicIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImageCount().equals("1")) {
                            mImageArryList.add(object.getImage());
                        }
                        Intent intent = new Intent(context, GalleryActivity.class);
                        intent.putStringArrayListExtra("LIST", mImageArryList);
                        context.startActivity(intent);

                    }
                });

                ((RecieverViewHolder) holder).itemReplyPICIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImageCount().equals("1")) {
                            mImageArryList.add(object.getImage());
                        }
                        Intent intent = new Intent(context, GalleryActivity.class);
                        intent.putStringArrayListExtra("LIST", mImageArryList);
                        context.startActivity(intent);
                    }
                });

                break;
            /******************
             * TYPE_ITEM CASE
             *****************/
            case Sender:

                if (object != null) {
                    if (object.getCreationDate() != null && !object.getCreationDate().equals("")) {

                        ((SenderViewHolder) holder).ChatDateTV.setText(formatToYesterdayOrToday(getDateFromUixTimeStamp(Long.parseLong(object.getCreationDate()))));

                        String thisDate = getDateFromUixTimeStampFormat(Long.parseLong(modelArrayList.get(position).getCreationDate()));
                        String nextDate = "";

                        if (position == 0) {
                            nextDate = thisDate;
                        } else {
                            if (modelArrayList.get(position - 1).getCreationDate() != null)
                                nextDate = getDateFromUixTimeStampFormat(Long.parseLong(modelArrayList.get(position - 1).getCreationDate()));
                        }

                        // enable section heading if it's the first one, or
                        // different from the previous one
                        if (nextDate.equals(thisDate)) {
                            if (strLastPage.equals("TRUE") && position == 0) {
                                ((SenderViewHolder) holder).ChatDateTV.setVisibility(View.VISIBLE);
                            } else {
                                ((SenderViewHolder) holder).ChatDateTV.setVisibility(View.GONE);
                            }
                        } else {
                            ((SenderViewHolder) holder).ChatDateTV.setVisibility(View.VISIBLE);
                        }
                    }
                }

                // convert seconds to milliseconds
                if (object.getCreationDate() != null && !object.getCreationDate().equals("")) {
                    long unix_seconds = Long.parseLong(object.getCreationDate());

                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(unix_seconds * 1000L);

//                    DateFormat format = new SimpleDateFormat("dd MMM, hh:mm a");
                    DateFormat format = new SimpleDateFormat("hh:mm a");

                    String date = format.format(cal.getTime());
                    String strDATE = date.replace("am", "AM").replace("pm", "PM");
//                    ((SenderViewHolder) holder).timeTV.setText(strDATE);
                    ((SenderViewHolder) holder).timeUnReplyTV.setText(strDATE);
                    ((SenderViewHolder) holder).lefttimenormalTextTV.setText(strDATE);
                    ((SenderViewHolder) holder).timeUnSingleImageTV.setText(strDATE);
                    ((SenderViewHolder) holder).timeUnReplyMultipleImagesTV.setText(strDATE);
                    ((SenderViewHolder) holder).leftChatTextReplytimeTV.setText(strDATE);
                    ((SenderViewHolder) holder).timeReplySingleImageTV.setText(strDATE);
                }

                if (object.getMessage() != null) {
                    ((SenderViewHolder) holder).leftChatTV.setText(html2text(object.getMessage()));
                    ((SenderViewHolder) holder).leftChatTextTV.setText(html2text(object.getMessage()));
                }
                if (object.getEdited() != null) {
                    if (object.getEdited().equals("1")) {
                        ((SenderViewHolder) holder).editedImage.setVisibility(View.VISIBLE);

                        /* to set edit icon according to the width of message */
                        if (object.getImageCount().equals("0") || object.getImageCount().equals("")) {

                            ((SenderViewHolder) holder).leftChatTextTV.measure(0, 0);
                            int width = ((SenderViewHolder) holder).leftChatTextTV.getMeasuredWidth();

                            if (width == context.getResources().getDimension(R.dimen._200sdp)
                                    || width > context.getResources().getDimension(R.dimen._200sdp)) {
                                RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._200sdp), RelativeLayout.LayoutParams.WRAP_CONTENT);
                                ((SenderViewHolder) holder).textRL.setLayoutParams(layoutParams1);
                            } else {
                                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                ((SenderViewHolder) holder).textRL.setLayoutParams(layoutParams);
                            }

                        } else {
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._200sdp), RelativeLayout.LayoutParams.WRAP_CONTENT);
                            ((SenderViewHolder) holder).textRL.setLayoutParams(layoutParams);
                        }

                    } else {
                        ((SenderViewHolder) holder).editedImage.setVisibility(View.GONE);
                    }
                }
                if (object.getImageCount() != null) {
                    if (!object.getImageCount().equals("")) {
                        if (object.getImageCount().equals("1")) {
                            ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).timeUnReplyMultipleImagesTV.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).timeUnSingleImageTV.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.VISIBLE);

                            ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);

                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage()))
                                    ((SenderViewHolder) holder).itemMessagePicLeftIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {
                                    ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                                    ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);
                                    if (!object.getImage().equals("")) {
                                        Picasso.get().load(object.getImage()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemMessagePicLeftIV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemMessagePicLeftIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            if (!object.getMessage().equals("")) {
                                ((SenderViewHolder) holder).itemMessagePicLeftIV.setCornerRadius(
                                        (int) context.getResources().getDimension(R.dimen._8sdp),
                                        (int) context.getResources().getDimension(R.dimen._8sdp),
                                        0, 0);
                            } else {
                                ((SenderViewHolder) holder).itemMessagePicLeftIV.setCornerRadius(
                                        (int) context.getResources().getDimension(R.dimen._8sdp),
                                        (int) context.getResources().getDimension(R.dimen._8sdp),
                                        (int) context.getResources().getDimension(R.dimen._8sdp),
                                        (int) context.getResources().getDimension(R.dimen._8sdp));
                            }

                        } else if (object.getImageCount().equals("2")) {
                            ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).timeUnReplyMultipleImagesTV.setVisibility(View.VISIBLE);

                            ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).timeUnSingleImageTV.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).relativeImage4RL.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).relativeImage3RL.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage2LL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).relativeImage2RL.setVisibility(View.VISIBLE);

                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._80sdp));
                            ((SenderViewHolder) holder).multiImageLefftRL.setLayoutParams(layoutParams);

                            ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);

                            // Image One Check
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage()))
                                    ((SenderViewHolder) holder).itemImageRight21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage().equals("")) {
                                        Picasso.get().load(object.getImage()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight21IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // Image Two Check
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2()))
                                    ((SenderViewHolder) holder).itemImageRight22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage2().equals("")) {
                                        Picasso.get().load(object.getImage2()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight22IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            if (!object.getMessage().equals("")) {
                                ((SenderViewHolder) holder).itemImageRight21IV.setCornerRadius(
                                        0, (int) context.getResources().getDimension(R.dimen._15sdp), 0, 0);

                                ((SenderViewHolder) holder).itemImageRight22IV.setCornerRadius(
                                        (int) context.getResources().getDimension(R.dimen._15sdp), 0, 0, 0);

                            } else {
                                ((SenderViewHolder) holder).itemImageRight21IV.setCornerRadius(
                                        0, (int) context.getResources().getDimension(R.dimen._15sdp),
                                        0, (int) context.getResources().getDimension(R.dimen._15sdp));

                                ((SenderViewHolder) holder).itemImageRight22IV.setCornerRadius(
                                        (int) context.getResources().getDimension(R.dimen._15sdp), 0,
                                        (int) context.getResources().getDimension(R.dimen._15sdp), 0);
                            }

                        } else if (object.getImageCount().equals("3")) {
                            ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).timeUnReplyMultipleImagesTV.setVisibility(View.VISIBLE);

                            ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).timeUnSingleImageTV.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).relativeImage4RL.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage3LL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).relativeImage3RL.setVisibility(View.VISIBLE);

                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._80sdp));
                            ((SenderViewHolder) holder).multiImageLefftRL.setLayoutParams(layoutParams);

                            ((SenderViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).relativeImage2RL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);

                            // Image One Check
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage()))
                                    ((SenderViewHolder) holder).itemImageRight31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage().equals("")) {
                                        Picasso.get().load(object.getImage()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight31IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // Image Two Check
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2()))
                                    ((SenderViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage2().equals("")) {
                                        Picasso.get().load(object.getImage2()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight33IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }


                            // Image Three  Check
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3()))
                                    ((SenderViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage3().equals("")) {
                                        Picasso.get().load(object.getImage3()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight33IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            if (!object.getMessage().equals("")) {
//                                ((SenderViewHolder) holder).blur3IV.setCornerRadius(
//                                        0, (int) context.getResources().getDimension(R.dimen._15sdp), 0, 0);
                                ((RecieverViewHolder) holder).blur3IV.setBackgroundResource(R.drawable.bg_tras_with_message);

                                ((SenderViewHolder) holder).itemImageRight32IV.setCornerRadius(
                                        0, (int) context.getResources().getDimension(R.dimen._15sdp), 0, 0);

                                ((SenderViewHolder) holder).itemImageRight31IV.setCornerRadius(
                                        (int) context.getResources().getDimension(R.dimen._15sdp), 0, 0, 0);

                            } else {
//                                ((SenderViewHolder) holder).blur3IV.setCornerRadius(
//                                        0, (int) context.getResources().getDimension(R.dimen._15sdp),
//                                        0, (int) context.getResources().getDimension(R.dimen._15sdp));
                                ((RecieverViewHolder) holder).blur3IV.setBackgroundResource(R.drawable.bg_tras_with_images);

                                ((SenderViewHolder) holder).itemImageRight32IV.setCornerRadius(
                                        0, (int) context.getResources().getDimension(R.dimen._15sdp),
                                        0, (int) context.getResources().getDimension(R.dimen._15sdp));

                                ((SenderViewHolder) holder).itemImageRight31IV.setCornerRadius(
                                        (int) context.getResources().getDimension(R.dimen._15sdp), 0,
                                        (int) context.getResources().getDimension(R.dimen._15sdp), 0);
                            }

                        } else if (object.getImageCount().equals("4")) {
                            ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).timeUnReplyMultipleImagesTV.setVisibility(View.VISIBLE);

                            ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).timeUnSingleImageTV.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage4LL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).relativeImage4RL.setVisibility(View.VISIBLE);

                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._160sdp));
                            ((SenderViewHolder) holder).multiImageLefftRL.setLayoutParams(layoutParams);

                            ((SenderViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).relativeImage3RL.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).relativeImage2RL.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);
                            //for 4 image count == for hiding +1
                            ((SenderViewHolder) holder).blurRL.setVisibility(View.GONE);


                            // Image One Check
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage()))
                                    ((SenderViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage().equals("")) {
                                        Picasso.get().load(object.getImage()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight41IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // Image Two Check
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2()))
                                    ((SenderViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage2().equals("")) {
                                        Picasso.get().load(object.getImage2()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight42IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }


                            // Image Three  Check
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3()))
                                    ((SenderViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage3().equals("")) {
                                        Picasso.get().load(object.getImage3()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight43IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // Image Four  Check
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4()))
                                    ((SenderViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage4().equals("")) {
                                        Picasso.get().load(object.getImage4()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight44IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            if (!object.getMessage().equals("")) {

                                ((SenderViewHolder) holder).itemImageRight43IV.setCornerRadius(
                                        0, 0, 0, 0);

                                ((SenderViewHolder) holder).itemImageRight44IV.setCornerRadius(
                                        0, 0, 0, 0);

                            } else {
                                ((SenderViewHolder) holder).itemImageRight43IV.setCornerRadius(0, 0,
                                        (int) context.getResources().getDimension(R.dimen._15sdp), 0);

                                ((SenderViewHolder) holder).itemImageRight44IV.setCornerRadius(0, 0,
                                        0, (int) context.getResources().getDimension(R.dimen._15sdp));
                            }

                        } else if (Integer.parseInt(object.getImageCount()) >= 5) {
                            ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).timeUnReplyMultipleImagesTV.setVisibility(View.VISIBLE);

                            ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).timeUnSingleImageTV.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage4LL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).relativeImage4RL.setVisibility(View.VISIBLE);

                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._160sdp));
                            ((SenderViewHolder) holder).multiImageLefftRL.setLayoutParams(layoutParams);

                            ((SenderViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).relativeImage3RL.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).relativeImage2RL.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).blurRL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).textRL.setVisibility(View.GONE);
                            int imageCount = Integer.parseInt(object.getImageCount()) - 4;
                            String strImageCount = "+" + imageCount;
                            ((SenderViewHolder) holder).textCountTv.setText(strImageCount);

                            // Image One Check
                            if (!Utilities.isDocAdded(object.getImage())) {
                                if (Utilities.isVideoAdded(object.getImage()))
                                    ((SenderViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage().equals("")) {
                                        Picasso.get().load(object.getImage()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight41IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // Image Two Check
                            if (!Utilities.isDocAdded(object.getImage2())) {
                                if (Utilities.isVideoAdded(object.getImage2()))
                                    ((SenderViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage2().equals("")) {
                                        Picasso.get().load(object.getImage2()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight42IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // Image Three  Check
                            if (!Utilities.isDocAdded(object.getImage3())) {
                                if (Utilities.isVideoAdded(object.getImage3()))
                                    ((SenderViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage3().equals("")) {
                                        Picasso.get().load(object.getImage3()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight43IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            // Image Four  Check
                            if (!Utilities.isDocAdded(object.getImage4())) {
                                if (Utilities.isVideoAdded(object.getImage4()))
                                    ((SenderViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                else {

                                    if (!object.getImage4().equals("")) {
                                        Picasso.get().load(object.getImage4()).fit().centerCrop()
                                                .placeholder(R.drawable.palace_holder)
                                                .error(R.drawable.palace_holder)
                                                .into(((SenderViewHolder) holder).itemImageRight44IV);
                                    }
                                }
                            } else {
                                ((SenderViewHolder) holder).itemImageRight44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                            }

                            if (!object.getMessage().equals("")) {

                                ((SenderViewHolder) holder).blurIV.setCornerRadius(
                                        0, 0, 0, 0);

                                ((SenderViewHolder) holder).itemImageRight43IV.setCornerRadius(
                                        0, 0, 0, 0);

                                ((SenderViewHolder) holder).itemImageRight44IV.setCornerRadius(
                                        0, 0, 0, 0);

                            } else {

                                ((SenderViewHolder) holder).blurIV.setCornerRadius(0, 0,
                                        0, (int) context.getResources().getDimension(R.dimen._15sdp));

                                ((SenderViewHolder) holder).itemImageRight43IV.setCornerRadius(0, 0,
                                        (int) context.getResources().getDimension(R.dimen._15sdp), 0);

                                ((SenderViewHolder) holder).itemImageRight44IV.setCornerRadius(0, 0,
                                        0, (int) context.getResources().getDimension(R.dimen._15sdp));
                            }

                        } else if (object.getImageCount().equals("0")) {
                            ((SenderViewHolder) holder).multiImageLefftRL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).timeUnReplyMultipleImagesTV.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).itemMessagePicLeftIV.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).itemMessagePicLeftRL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).timeUnSingleImageTV.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage4LL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).relativeImage4RL.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage3LL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).relativeImage3RL.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).linearImage2LL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).relativeImage2RL.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).leftChatWithIMGLL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).textRL.setVisibility(View.VISIBLE);
                        }

                        if (object.getMessage() != null && !object.getMessage().equals("")) {
                            ((SenderViewHolder) holder).leftChatTV.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).leftChatLL.setVisibility(View.VISIBLE);

                            ((SenderViewHolder) holder).timeUnSingleImageTV.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).timeUnReplyMultipleImagesTV.setVisibility(View.GONE);
                        } else {
                            ((SenderViewHolder) holder).leftChatTV.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).leftChatLL.setVisibility(View.GONE);

                            ((SenderViewHolder) holder).timeUnSingleImageTV.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).timeUnReplyMultipleImagesTV.setVisibility(View.VISIBLE);
                        }
                    }
                }
                if (object.getReplyFor() != null) {
                    ((SenderViewHolder) holder).unReplyViewRL.setVisibility(View.GONE);
                    ((SenderViewHolder) holder).replyRL.setVisibility(View.VISIBLE);

                    ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                    ((SenderViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);
                    ((SenderViewHolder) holder).timeReplySingleImageTV.setVisibility(View.GONE);

                    if (object.getMessage() != null) {
                        ((SenderViewHolder) holder).leftChatTextRTV.setText(html2text(object.getMessage()));
                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setText(html2text(object.getMessage()));
                    }

                    // For reply view when image is One
                    if (object.getImageCount() != null && object.getImageCount().equals("1")) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.VISIBLE);


                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).timeReplySingleImageTV.setVisibility(View.GONE);

                        // For Reply One image send
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemMessageReplyPicLeftIV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                    }

                    // For reply view when image is two
                    if (object.getImageCount() != null && object.getImageCount().equals("2")) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._80sdp));
                        ((SenderViewHolder) holder).multiImageReplyPicRL.setLayoutParams(layoutParams);


                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).timeReplySingleImageTV.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).imageReplyPic2LL.setVisibility(View.VISIBLE);


                        // For Reply two image send
                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageReplyPic21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic21IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic21IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        // For Reply two image send
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemImageReplyPic22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic22IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic22IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                    }

                    // For reply view when image is three
                    if (object.getImageCount() != null && object.getImageCount().equals("3")) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._80sdp));
                        ((SenderViewHolder) holder).multiImageReplyPicRL.setLayoutParams(layoutParams);


                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).timeReplySingleImageTV.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).imageReplyPic2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic3LL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).itemImageReplyPic32IV.setVisibility(View.VISIBLE);
                        // For Reply three image send

                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic31IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic31IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic32IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic32IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((SenderViewHolder) holder).itemImageReplyPic33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage3().equals("")) {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic33IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic33IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                    }

                    // For reply view when image is Four
                    if (object.getImageCount() != null && object.getImageCount().equals("4")) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._160sdp));
                        ((SenderViewHolder) holder).multiImageReplyPicRL.setLayoutParams(layoutParams);


                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).timeReplySingleImageTV.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).imageReplyPic2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic3LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);

                        //for 4 image count == for hiding +1
                        ((SenderViewHolder) holder).blurRL.setVisibility(View.GONE);

                        // For Reply Four image send

                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic41IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic42IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage3().equals("")) {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic43IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage4().equals("")) {
                                    Picasso.get().load(object.getImage4()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic44IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }
                    }

                    // For reply view when image is Five or more than Five
                    if (object.getImageCount() != null && Integer.parseInt(object.getImageCount()) >= 5) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.VISIBLE);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen._180sdp), (int) context.getResources().getDimension(R.dimen._160sdp));
                        ((SenderViewHolder) holder).multiImageReplyPicRL.setLayoutParams(layoutParams);

                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).timeReplySingleImageTV.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).imageReplyPic2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic3LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic4LL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).blurReplyPicRL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).textCountReplyPicTv.setVisibility(View.VISIBLE);
                        int imageCount = Integer.parseInt(object.getImageCount()) - 4;
                        String strImageCount = "+" + imageCount;
                        ((SenderViewHolder) holder).textCountReplyPicTv.setText(strImageCount);

                        // For Reply Four image send
                        if (!Utilities.isDocAdded(object.getImage())) {
                            if (Utilities.isVideoAdded(object.getImage()))
                                ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage().equals("")) {
                                    Picasso.get().load(object.getImage()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic41IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic41IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }


                        if (!Utilities.isDocAdded(object.getImage2())) {
                            if (Utilities.isVideoAdded(object.getImage2()))
                                ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage2().equals("")) {
                                    Picasso.get().load(object.getImage2()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic42IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic42IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!Utilities.isDocAdded(object.getImage3())) {
                            if (Utilities.isVideoAdded(object.getImage3()))
                                ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage3().equals("")) {
                                    Picasso.get().load(object.getImage3()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic43IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic43IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }

                        if (!Utilities.isDocAdded(object.getImage4())) {
                            if (Utilities.isVideoAdded(object.getImage4()))
                                ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                            else {

                                if (!object.getImage4().equals("")) {
                                    Picasso.get().load(object.getImage4()).fit().centerCrop()
                                            .placeholder(R.drawable.palace_holder)
                                            .error(R.drawable.palace_holder)
                                            .into(((SenderViewHolder) holder).itemImageReplyPic44IV);
                                }
                            }
                        } else {
                            ((SenderViewHolder) holder).itemImageReplyPic44IV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                        }
                    }

                    if (object.getImageCount() != null && object.getImageCount().equals("0")) {
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).itemMessageReplyPicLeftRL.setVisibility(View.GONE);

                        ((SenderViewHolder) holder).multiImageReplyPicRL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextReplyPicLL.setVisibility(View.VISIBLE);

                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextRLL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).timeReplySingleImageTV.setVisibility(View.VISIBLE);

                        ((SenderViewHolder) holder).imageReplyPic2LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic3LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).imageReplyPic4LL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).blurReplyPicRL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).textCountReplyPicTv.setVisibility(View.GONE);
                    }

                    if (object.getReplyFor().getUserDetail().getImage()!= null &&
                            !object.getReplyFor().getUserDetail().getImage().equals("")) {
                        Picasso.get().load(object.getReplyFor().getUserDetail().getImage()).fit().centerCrop()
                                .placeholder(R.drawable.palace_holder)
                                .error(R.drawable.palace_holder)
                                .into(((SenderViewHolder) holder).imagePicLeftReply);
                    }

                    if (object.getMessage() != null && !object.getMessage().equals("")) {
                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).leftChatTextRLL.setVisibility(View.VISIBLE);
                        ((SenderViewHolder) holder).timeReplySingleImageTV.setVisibility(View.GONE);

                    } else {
                        ((SenderViewHolder) holder).leftChatTextRTV.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).leftChatTextRLL.setVisibility(View.GONE);
                        ((SenderViewHolder) holder).timeReplySingleImageTV.setVisibility(View.VISIBLE);
                    }

                    if (object.getReplyFor().getUserDetail().getId() != null) {
                        if (object.getReplyFor().getUserDetail().getId().equals(JaoharPreference.readString(context, JaoharPreference.STAFF_ID, ""))) {
                            ((SenderViewHolder) holder).lefttimeReplyTV.setText("You");
                            ((SenderViewHolder) holder).imageRL.setVisibility(View.GONE);
                        } else if (object.getReplyFor().getUserDetail().getId().equals(JaoharPreference.readString(context, JaoharPreference.ADMIN_ID, ""))) {
                            ((SenderViewHolder) holder).lefttimeReplyTV.setText("You");
                            ((SenderViewHolder) holder).imageRL.setVisibility(View.GONE);
                        } else {
                            ((SenderViewHolder) holder).lefttimeReplyTV.setText(object.getReplyFor().getUserDetail().getFirstName());
                            ((SenderViewHolder) holder).imageRL.setVisibility(View.VISIBLE);
                        }
                    }

                    if (object.getReplyFor().getMessage() != null) {
                        ((SenderViewHolder) holder).leftChatReplyTV.setText(html2text(object.getReplyFor().getMessage()));
                        ((SenderViewHolder) holder).messageReplyTV.setText(html2text(object.getReplyFor().getMessage()));
                    }
                    if (object.getReplyFor().getImageCount() != null &&
                            !object.getReplyFor().getImageCount().equals("")) {
                        if (!object.getReplyFor().getImageCount().equals("0")) {

                            ((SenderViewHolder) holder).ReplyLL.setVisibility(View.VISIBLE);

                            ((SenderViewHolder) holder).leftChatReplyTV.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).imageRplyRL.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).onlyMsgReplyLL.setVisibility(View.GONE);

                            if (object.getReplyFor().getImageCount().equals("1")) {

                                ((SenderViewHolder) holder).blurPicRL.setVisibility(View.GONE);
                                ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setVisibility(View.VISIBLE);

                                if (!Utilities.isDocAdded(object.getReplyFor().getImage())) {
                                    if (Utilities.isVideoAdded(object.getReplyFor().getImage()))
                                        ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    else {

                                        if (!object.getReplyFor().getImage().equals("")) {
                                            Picasso.get().load(object.getReplyFor().getImage()).fit().centerCrop()
                                                    .placeholder(R.drawable.palace_holder)
                                                    .error(R.drawable.palace_holder)
                                                    .into(((SenderViewHolder) holder).itemMessagePicLeftReplyIV);
                                        }
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            } else if (Integer.parseInt(object.getReplyFor().getImageCount()) >= 2) {
                                ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setVisibility(View.VISIBLE);
                                int imageCount = Integer.parseInt(object.getReplyFor().getImageCount()) - 1;
                                String strImageCount = "+" + imageCount;
                                ((SenderViewHolder) holder).textCountPicTv.setText(strImageCount);


                                if (!Utilities.isDocAdded(object.getReplyFor().getImage())) {
                                    if (Utilities.isVideoAdded(object.getReplyFor().getImage()))
                                        ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.video_icon));
                                    else {

                                        if (!object.getReplyFor().getImage().equals("")) {
                                            Picasso.get().load(object.getReplyFor().getImage()).fit().centerCrop()
                                                    .placeholder(R.drawable.palace_holder)
                                                    .error(R.drawable.palace_holder)
                                                    .into(((SenderViewHolder) holder).itemMessagePicLeftReplyIV);
                                        }
                                    }
                                } else {
                                    ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_doc));
                                }

                            }
                        } else {
                            ((SenderViewHolder) holder).imageRplyRL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).ReplyLL.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).onlyMsgReplyLL.setVisibility(View.VISIBLE);
                        }

                        if (object.getReplyFor().getMessage() != null && !object.getReplyFor().getMessage().equals("")) {
                            ((SenderViewHolder) holder).leftChatReplyTV.setVisibility(View.VISIBLE);
                            ((SenderViewHolder) holder).leftChatReplyTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        } else {
//                            ((SenderViewHolder) holder).leftChatReplyTV.setVisibility(View.GONE);
                            ((SenderViewHolder) holder).leftChatReplyTV.setText(R.string.photo);
                            ((SenderViewHolder) holder).leftChatReplyTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_photo_camera, 0, 0, 0);
                        }
                    }
                } else {
                    ((SenderViewHolder) holder).replyRL.setVisibility(View.GONE);
                    ((SenderViewHolder) holder).unReplyViewRL.setVisibility(View.VISIBLE);
                }

                ((SenderViewHolder) holder).multiImageLefftRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImageCount().equals("1")) {
                            mImageArryList.add(object.getImage());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("2")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("3")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("4")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("5")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("6")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("7")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("8")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("9")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("10")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            mImageArryList.add(object.getImage10());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        }
                    }
                });

                ((SenderViewHolder) holder).multiImageReplyPicRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImageCount().equals("1")) {
                            mImageArryList.add(object.getImage());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("2")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("3")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("4")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("5")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("6")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("7")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("8")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("9")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        } else if (object.getImageCount().equals("10")) {
                            mImageArryList.add(object.getImage());
                            mImageArryList.add(object.getImage2());
                            mImageArryList.add(object.getImage3());
                            mImageArryList.add(object.getImage4());
                            mImageArryList.add(object.getImage5());
                            mImageArryList.add(object.getImage6());
                            mImageArryList.add(object.getImage7());
                            mImageArryList.add(object.getImage8());
                            mImageArryList.add(object.getImage9());
                            mImageArryList.add(object.getImage10());
                            Intent intent = new Intent(context, GalleryActivity.class);
                            intent.putStringArrayListExtra("LIST", mImageArryList);
                            context.startActivity(intent);
                        }
                    }
                });

                ((SenderViewHolder) holder).itemMessagePicLeftIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImageCount().equals("1")) {
                            mImageArryList.add(object.getImage());
                        }
                        Intent intent = new Intent(context, GalleryActivity.class);
                        intent.putStringArrayListExtra("LIST", mImageArryList);
                        context.startActivity(intent);
                    }
                });

                ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mImageArryList.clear();
                        if (object.getImageCount().equals("1")) {
                            mImageArryList.add(object.getImage());
                        }
                        Intent intent = new Intent(context, GalleryActivity.class);
                        intent.putStringArrayListExtra("LIST", mImageArryList);
                        context.startActivity(intent);
                    }
                });

                ((SenderViewHolder) holder).menuleftLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, view, "sender", position);
                    }
                });

                ((SenderViewHolder) holder).layoutClickLL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).itemMessageReplyPicLeftIV.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).multiImageReplyPicRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).replyRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).itemMessagePicLeftReplyIV.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).multiImageLefftRL.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                ((SenderViewHolder) holder).itemMessagePicLeftIV.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mAdd_Del_EditInteface.mAdd_Del_ChatInterface(object, v, "sender", position);
                        vibe.vibrate(150);
                        return true;
                    }
                });

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (modelArrayList != null) {
            AllMessagesItem object = modelArrayList.get(position);
            if (object != null) {
                return object.getIntType();
            }
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        if (modelArrayList == null)
            return 0;
        return modelArrayList.size();
    }

    public static class RecieverViewHolder extends RecyclerView.ViewHolder {
        public TextView rightMessgeTv, textCountTv, lefttimeReplyTV, timeRightTV, timeRightReplyTV, textCountPicTv,
                rightMessgeReplyTextTv, leftChatReplyTV, textCountReplyPicTv, rightMessgUnReplyTextTv, timeUnSingleImageTV,
                timeUnReplyMultipleImagesTV, timeReplySingleImageTV, leftChatImagesReplytimeTV, leftChatTextReplytimeTV,
                messageReplyTV, ChatDateTV;

        public ImageView imagePicRight, itemImageRight41IV, itemImageRight42IV, itemMessagePicLeftReplyIV,
                itemImageRight33IV, itemReplyPICIV, itemImageReplyPic44IV, itemImageReplyPic22IV,
                itemImageReplyPic21IV, itemImageReplyPic33IV, itemImageReplyPic42IV, itemImageReplyPic43IV,
                itemImageReplyPic31IV, itemImageReplyPic32IV, itemImageReplyPic41IV, editedImage;

        public RoundedImageView itemMessagePicIV, itemImageRight21IV, itemImageRight22IV,
                itemImageRight32IV, itemImageRight31IV, itemImageRight43IV, itemImageRight44IV, blurIV;

        public RelativeLayout multiImageRL, blurRL, ReplyViewLL, unReplyViewLL, imageRplyRL, multiImageReplyPicRL,
                blurReplyPicRL, replyLL, blurPicRL, imageReplyRL, itemMessagePicLeftRL, itemMessageReplyPicLeftRL,
                imageUnReplyRL, ReplyLL, blur3IV;

        public ImageView imagePicLeftReply, imagePicRightReply1;

        public LinearLayout linearImage4LL, linearImage3LL, linearImage2LL, menuRightLL, image2ReplyPicLL, image3ReplyPicLL,
                imageReplyPic4LL, leftChatLL, leftChatTextReplyPicLL, leftChatTextRLL, onlyMsgReplyLL;

        LinearLayout unReplyTopLL;

        public RecieverViewHolder(View view) {
            super(view);

            ChatDateTV = view.findViewById(R.id.ChatDateTV);
            ReplyLL = view.findViewById(R.id.ReplyLL);
            messageReplyTV = view.findViewById(R.id.messageReplyTV);
            onlyMsgReplyLL = view.findViewById(R.id.onlyMsgReplyLL);
            imageUnReplyRL = view.findViewById(R.id.imageUnReplyRL);
            editedImage = view.findViewById(R.id.editedImage);
            leftChatTextReplytimeTV = view.findViewById(R.id.leftChatTextReplytimeTV);
            leftChatImagesReplytimeTV = view.findViewById(R.id.leftChatImagesReplytimeTV);
            timeReplySingleImageTV = view.findViewById(R.id.timeReplySingleImageTV);
            itemMessageReplyPicLeftRL = view.findViewById(R.id.itemMessageReplyPicLeftRL);
            leftChatTextRLL = view.findViewById(R.id.leftChatTextRLL);
            leftChatTextReplyPicLL = view.findViewById(R.id.leftChatTextReplyPicLL);
            timeUnReplyMultipleImagesTV = view.findViewById(R.id.timeUnReplyMultipleImagesTV);
            timeUnSingleImageTV = view.findViewById(R.id.timeUnSingleImageTV);
            itemMessagePicLeftRL = view.findViewById(R.id.itemMessagePicLeftRL);
            unReplyTopLL = view.findViewById(R.id.unReplyTopLL);
            leftChatLL = view.findViewById(R.id.leftChatLL);
            textCountTv = (TextView) view.findViewById(R.id.textCountTv);
            lefttimeReplyTV = (TextView) view.findViewById(R.id.lefttimeReplyTV);
            textCountReplyPicTv = (TextView) view.findViewById(R.id.textCountReplyPicTv);
            rightMessgeTv = (TextView) view.findViewById(R.id.rightMessgeTv);
            timeRightTV = (TextView) view.findViewById(R.id.timeRightTV);
            timeRightReplyTV = (TextView) view.findViewById(R.id.timeRightReplyTV);
            rightMessgeReplyTextTv = (TextView) view.findViewById(R.id.rightMessgeReplyTextTv);
            rightMessgUnReplyTextTv = (TextView) view.findViewById(R.id.rightMessgUnReplyTextTv);
            leftChatReplyTV = (TextView) view.findViewById(R.id.leftChatReplyTV);
            textCountPicTv = (TextView) view.findViewById(R.id.textCountPicTv);
            itemMessagePicIV = view.findViewById(R.id.itemMessagePicIV);

            imagePicRight = (ImageView) view.findViewById(R.id.imagePicRight);
            itemImageRight41IV = (ImageView) view.findViewById(R.id.itemImageRight41IV);
            itemImageRight42IV = (ImageView) view.findViewById(R.id.itemImageRight42IV);
            itemImageRight43IV = view.findViewById(R.id.itemImageRight43IV);
            itemImageRight44IV = view.findViewById(R.id.itemImageRight44IV);

            blurIV = view.findViewById(R.id.blurIV);
            blur3IV = view.findViewById(R.id.blur3IV);
            itemImageRight31IV = view.findViewById(R.id.itemImageRight31IV);
            itemImageRight32IV = view.findViewById(R.id.itemImageRight32IV);
            itemImageRight33IV = (ImageView) view.findViewById(R.id.itemImageRight33IV);

            itemImageReplyPic22IV = (ImageView) view.findViewById(R.id.itemImageReplyPic22IV);
            itemImageReplyPic21IV = (ImageView) view.findViewById(R.id.itemImageReplyPic21IV);
            itemReplyPICIV = (ImageView) view.findViewById(R.id.itemReplyPICIV);
            itemImageReplyPic31IV = (ImageView) view.findViewById(R.id.itemImageReplyPic31IV);
            itemImageReplyPic32IV = (ImageView) view.findViewById(R.id.itemImageReplyPic32IV);
            itemImageReplyPic33IV = (ImageView) view.findViewById(R.id.itemImageReplyPic33IV);
            itemImageReplyPic41IV = (ImageView) view.findViewById(R.id.itemImageReplyPic41IV);
            itemImageReplyPic42IV = (ImageView) view.findViewById(R.id.itemImageReplyPic42IV);
            itemImageReplyPic43IV = (ImageView) view.findViewById(R.id.itemImageReplyPic43IV);
            itemImageReplyPic44IV = (ImageView) view.findViewById(R.id.itemImageReplyPic44IV);
            multiImageRL = (RelativeLayout) view.findViewById(R.id.multiImageRL);
            blurReplyPicRL = (RelativeLayout) view.findViewById(R.id.blurReplyPicRL);
            imageRplyRL = (RelativeLayout) view.findViewById(R.id.imageRplyRL);

            imageReplyRL = view.findViewById(R.id.imageReplyRL);
            blurRL = (RelativeLayout) view.findViewById(R.id.blurRL);
            blurPicRL = (RelativeLayout) view.findViewById(R.id.blurPicRL);

            ReplyViewLL = (RelativeLayout) view.findViewById(R.id.ReplyViewLL);
            unReplyViewLL = (RelativeLayout) view.findViewById(R.id.unReplyViewLL);
            multiImageReplyPicRL = (RelativeLayout) view.findViewById(R.id.multiImageReplyPicRL);
            image2ReplyPicLL = (LinearLayout) view.findViewById(R.id.image2ReplyPicLL);
            image3ReplyPicLL = (LinearLayout) view.findViewById(R.id.image3ReplyPicLL);
            replyLL = (RelativeLayout) view.findViewById(R.id.replyLL);
            menuRightLL = (LinearLayout) view.findViewById(R.id.menuRightLL);
            linearImage4LL = (LinearLayout) view.findViewById(R.id.linearImage4LL);
            linearImage3LL = (LinearLayout) view.findViewById(R.id.linearImage3LL);
            linearImage2LL = (LinearLayout) view.findViewById(R.id.linearImage2LL);
            imageReplyPic4LL = (LinearLayout) view.findViewById(R.id.imageReplyPic4LL);
            itemImageRight21IV = view.findViewById(R.id.itemImageRight21IV);
            itemImageRight22IV = view.findViewById(R.id.itemImageRight22IV);
            itemMessagePicLeftReplyIV = (ImageView) view.findViewById(R.id.itemMessagePicLeftReplyIV);
            imagePicLeftReply = view.findViewById(R.id.imagePicLeftReply);
            imagePicRightReply1 = view.findViewById(R.id.imagePicRightReply1);
        }
    }

    public static class SenderViewHolder extends RecyclerView.ViewHolder {
        public TextView leftChatTV, timeTV, textCountTv, textCountPicTv, leftChatTextTV, leftChatTextRTV,
                timeUnReplyTV, lefttimenormalTextTV, leftChatReplyTV, leftChatTextReplytimeTV, timeReplySingleImageTV,
                leftChatTextReplyPicTV, textCountReplyPicTv, lefttimeReplyTV, timeUnSingleImageTV, timeUnReplyMultipleImagesTV,
                messageReplyTV, ChatDateTV;

        public ImageView imagePicLeft, itemImageRight41IV, itemImageRight42IV, itemMessagePicLeftReplyIV,
                itemImageRight33IV, imageMenuIMG, imagePicLeftReply, itemMessageReplyPicLeftIV,
                itemImageReplyPic31IV, itemImageRight41ReplyIV, itemImageReplyPic22IV, itemImageReplyPic21IV,
                itemImageReplyPic32IV, itemImageReplyPic33IV, itemImageReplyPic41IV, itemImageReplyPic42IV,
                itemImageReplyPic43IV, itemImageReplyPic44IV, editedImage;

        public RoundedImageView itemMessagePicLeftIV, itemImageRight21IV, itemImageRight22IV, blurIV,
                itemImageRight32IV, itemImageRight31IV, itemImageRight43IV, itemImageRight44IV;

        public RelativeLayout multiImageLefftRL, blurRL, layoutClickLL, imageRL, itemMessageReplyPicLeftRL,
                unReplyViewRL, replyRL, imageRplyRL, multiImageReplyPicRL, blurReplyPicRL, blurPicRL, itemMessagePicLeftRL,
                relativeImage2RL, relativeImage3RL, relativeImage4RL, ReplyLL, blur3IV;

        public LinearLayout linearImage4LL, linearImage3LL, linearImage2LL, leftChatWithIMGLL, menuleftLL, imageReplyPic2LL,
                imageReplyPic3LL, imageReplyPic4LL, leftChatLL, leftChatTextReplyPicLL, leftChatTextRLL, mainLL, textRL,
                onlyMsgReplyLL;

        public SenderViewHolder(View view) {
            super(view);

            ChatDateTV = view.findViewById(R.id.ChatDateTV);
            ReplyLL = view.findViewById(R.id.ReplyLL);
            onlyMsgReplyLL = view.findViewById(R.id.onlyMsgReplyLL);
            messageReplyTV = view.findViewById(R.id.messageReplyTV);
            mainLL = view.findViewById(R.id.mainLL);
            editedImage = view.findViewById(R.id.editedImage);
            timeReplySingleImageTV = view.findViewById(R.id.timeReplySingleImageTV);
            itemMessageReplyPicLeftRL = view.findViewById(R.id.itemMessageReplyPicLeftRL);
            leftChatTextReplytimeTV = view.findViewById(R.id.leftChatTextReplytimeTV);
            leftChatTextRLL = view.findViewById(R.id.leftChatTextRLL);
            leftChatTextReplyPicLL = view.findViewById(R.id.leftChatTextReplyPicLL);
            relativeImage4RL = view.findViewById(R.id.relativeImage4RL);
            relativeImage3RL = view.findViewById(R.id.relativeImage3RL);
            relativeImage2RL = view.findViewById(R.id.relativeImage2RL);
            itemMessagePicLeftRL = view.findViewById(R.id.itemMessagePicLeftRL);
            timeUnReplyMultipleImagesTV = view.findViewById(R.id.timeUnReplyMultipleImagesTV);
            timeUnSingleImageTV = view.findViewById(R.id.timeUnSingleImageTV);
            leftChatLL = view.findViewById(R.id.leftChatLL);
            leftChatTV = (TextView) view.findViewById(R.id.leftChatTV);
            timeTV = (TextView) view.findViewById(R.id.timeTV);
            timeUnReplyTV = (TextView) view.findViewById(R.id.timeUnReplyTV);
            lefttimenormalTextTV = (TextView) view.findViewById(R.id.lefttimenormalTextTV);
            textCountTv = (TextView) view.findViewById(R.id.textCountTv);
            textCountPicTv = (TextView) view.findViewById(R.id.textCountPicTv);
            leftChatTextTV = (TextView) view.findViewById(R.id.leftChatTextTV);
            leftChatTextRTV = (TextView) view.findViewById(R.id.leftChatTextRTV);
//            leftChatTextReplyPicTV = (TextView) view.findViewById(R.id.leftChatTextReplyPicTV);

            lefttimeReplyTV = (TextView) view.findViewById(R.id.lefttimeReplyTV);

            leftChatReplyTV = (TextView) view.findViewById(R.id.leftChatReplyTV);
            itemMessagePicLeftIV = view.findViewById(R.id.itemMessagePicLeftIV);
            imagePicLeftReply = (ImageView) view.findViewById(R.id.imagePicLeftReply);
            itemMessageReplyPicLeftIV = (ImageView) view.findViewById(R.id.itemMessageReplyPicLeftIV);
            itemImageRight41IV = (ImageView) view.findViewById(R.id.itemImageRight41IV);
            itemImageRight42IV = (ImageView) view.findViewById(R.id.itemImageRight42IV);
            itemImageRight43IV = view.findViewById(R.id.itemImageRight43IV);
            itemImageRight44IV = view.findViewById(R.id.itemImageRight44IV);

            itemImageRight31IV = view.findViewById(R.id.itemImageRight31IV);
            itemImageRight32IV = view.findViewById(R.id.itemImageRight32IV);
            blur3IV = view.findViewById(R.id.blur3IV);
            blurIV = view.findViewById(R.id.blurIV);
            itemImageRight33IV = (ImageView) view.findViewById(R.id.itemImageRight33IV);

            itemImageRight21IV = view.findViewById(R.id.itemImageRight21IV);
            itemImageRight22IV = view.findViewById(R.id.itemImageRight22IV);

            // For Reply Layout view
            itemImageReplyPic32IV = (ImageView) view.findViewById(R.id.itemImageReplyPic32IV);


            itemMessagePicLeftReplyIV = (ImageView) view.findViewById(R.id.itemMessagePicLeftReplyIV);


            itemImageReplyPic22IV = (ImageView) view.findViewById(R.id.itemImageReplyPic22IV);
            itemImageReplyPic21IV = (ImageView) view.findViewById(R.id.itemImageReplyPic21IV);
            itemImageReplyPic31IV = (ImageView) view.findViewById(R.id.itemImageReplyPic31IV);
            itemImageReplyPic33IV = (ImageView) view.findViewById(R.id.itemImageReplyPic33IV);
            itemImageReplyPic41IV = (ImageView) view.findViewById(R.id.itemImageReplyPic41IV);
            itemImageReplyPic44IV = (ImageView) view.findViewById(R.id.itemImageReplyPic44IV);
            itemImageReplyPic42IV = (ImageView) view.findViewById(R.id.itemImageReplyPic42IV);
            itemImageReplyPic43IV = (ImageView) view.findViewById(R.id.itemImageReplyPic43IV);
            textCountReplyPicTv = (TextView) view.findViewById(R.id.textCountReplyPicTv);
            blurPicRL = view.findViewById(R.id.blurPicRL);

            imageMenuIMG = (ImageView) view.findViewById(R.id.imageMenuIMG);
            multiImageLefftRL = (RelativeLayout) view.findViewById(R.id.multiImageLefftRL);
            imageRplyRL = (RelativeLayout) view.findViewById(R.id.imageRplyRL);
            unReplyViewRL = (RelativeLayout) view.findViewById(R.id.unReplyViewRL);
            replyRL = (RelativeLayout) view.findViewById(R.id.replyRL);
            blurRL = (RelativeLayout) view.findViewById(R.id.blurRL);
            blurReplyPicRL = (RelativeLayout) view.findViewById(R.id.blurReplyPicRL);
            multiImageReplyPicRL = (RelativeLayout) view.findViewById(R.id.multiImageReplyPicRL);

            textRL = view.findViewById(R.id.textRL);
            imageRL = view.findViewById(R.id.imageRL);

            layoutClickLL = view.findViewById(R.id.layoutClickLL);

            linearImage4LL = (LinearLayout) view.findViewById(R.id.linearImage4LL);
            linearImage3LL = (LinearLayout) view.findViewById(R.id.linearImage3LL);
            linearImage2LL = (LinearLayout) view.findViewById(R.id.linearImage2LL);
            leftChatWithIMGLL = (LinearLayout) view.findViewById(R.id.leftChatWithIMGLL);

            imageReplyPic2LL = (LinearLayout) view.findViewById(R.id.imageReplyPic2LL);
            imageReplyPic3LL = (LinearLayout) view.findViewById(R.id.imageReplyPic3LL);
            imageReplyPic4LL = (LinearLayout) view.findViewById(R.id.imageReplyPic4LL);
            menuleftLL = (LinearLayout) view.findViewById(R.id.menuleftLL);
        }
    }

    //for converting html to text
    public CharSequence html2text(String html) {
        CharSequence trimmed = noTrailingwhiteLines(Html.fromHtml(html.replace("\n", "<br />")));
        return trimmed;
    }

    //for hiding extra white space
    private CharSequence noTrailingwhiteLines(CharSequence text) {

        if (text.length() > 0)
            while (text.charAt(text.length() - 1) == '\n') {
                text = text.subSequence(0, text.length() - 1);
            }
        return text;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
