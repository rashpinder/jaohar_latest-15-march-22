package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.vincent.filepicker.Constant;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.OpenDynamicLinksActivity;
import jaohar.com.jaohar.adapters.forum_module.ForumListAdapter;
import jaohar.com.jaohar.beans.DynamicLinksModel;
import jaohar.com.jaohar.beans.ForumModule.ForumModel;
import jaohar.com.jaohar.interfaces.forumModule.ForumItemClickInterace;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.utils.JaoharConstants;

public class DynamicLinksAdapter extends RecyclerView.Adapter<DynamicLinksAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<DynamicLinksModel> modelArrayList;

    public DynamicLinksAdapter(Activity mActivity, ArrayList<DynamicLinksModel> modelArrayList) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
    }

    @Override
    public DynamicLinksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_links, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DynamicLinksAdapter.ViewHolder holder, final int position) {
        final DynamicLinksModel tempValue = modelArrayList.get(position);

        holder.vesselNameTV.setText(tempValue.getLink_name());

        if (tempValue.getLink_image() != null && tempValue.getLink_image().contains("http")) {

            if (modelArrayList.get(position).getLink_image().contains("https")) {
                Glide.with(mActivity)
                        .load(modelArrayList.get(position).getLink_image()).placeholder(R.drawable.palace_holder)
                        .into(holder.forumIMG);
            } else {
                Glide.with(mActivity).load(modelArrayList.get(position).getLink_image()
                        .replace("http://", "https://")).placeholder(R.drawable.palace_holder)
                        .into(holder.forumIMG);
            }

        } else {
            holder.forumIMG.setImageResource(R.drawable.palace_holder);
        }

        holder.vesselLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startActivity(new Intent(mActivity, OpenDynamicLinksActivity.class)
                        .putExtra("TITLE", tempValue.getLink_name())
                        .putExtra("LINK", tempValue.getLink_value()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView vesselNameTV;
        ImageView forumIMG;
        LinearLayout vesselLL;

        ViewHolder(View itemView) {
            super(itemView);
            vesselNameTV = itemView.findViewById(R.id.vesselNameTV);
            forumIMG = itemView.findViewById(R.id.forumIMG);
            vesselLL = itemView.findViewById(R.id.vesselLL);
        }
    }
}
