package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.interfaces.DeleteItemInvoiceInterface;
import jaohar.com.jaohar.interfaces.EditInvoiceItemData;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class InVoiceItemsAdapter extends RecyclerView.Adapter<InVoiceItemsAdapter.ViewHolder> {
    DeleteItemInvoiceInterface mDeleteItemInvoiceInterface;
    private Activity mActivity;
    private ArrayList<InvoiceAddItemModel> modelArrayList;
    EditInvoiceItemData mEditInvoiceItemData;

    public InVoiceItemsAdapter(Activity mActivity, ArrayList<InvoiceAddItemModel> modelArrayList,DeleteItemInvoiceInterface mDeleteItemInvoiceInterface,EditInvoiceItemData mEditInvoiceItemData) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDeleteItemInvoiceInterface = mDeleteItemInvoiceInterface;
        this.mEditInvoiceItemData = mEditInvoiceItemData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_invoice_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final InvoiceAddItemModel tempValue = modelArrayList.get(position);
//        holder.txtItemsTV.setText(tempValue.getQuantity() + ", " + tempValue.getUnitprice());
        holder.txtItemsTV.setText(tempValue.getUnitprice());

        holder.txtItemsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent mIntent = new Intent(mActivity, EditInvoiceItemActivity.class);
//                mIntent.putExtra("Model", tempValue);
//                mIntent.putExtra("Position", position);
//                mActivity.startActivity(mIntent);
                mEditInvoiceItemData.editInvoiceItemData(tempValue,position);
            }
        });

        holder.imgRemoveIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                modelArrayList.remove(position);
//                notifyDataSetChanged();
                mDeleteItemInvoiceInterface.deleteItemInvoice(tempValue);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItemsTV;
        public ImageView imgRemoveIV;

        ViewHolder(View itemView) {
            super(itemView);
            imgRemoveIV = (ImageView) itemView.findViewById(R.id.imgRemoveIV);
            txtItemsTV = (TextView) itemView.findViewById(R.id.txtItemsTV);
        }
    }
}