package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.models.GetVesselTypeData;

public class VesselRecentSearchAdapter extends RecyclerView.Adapter<VesselRecentSearchAdapter.ViewHolder> {
    Activity mActivity;
    private List<String> modelArrayList;
    OnClickInterface mOnClickInterface;

    public VesselRecentSearchAdapter(Activity mActivity, List<String> modelArrayList, OnClickInterface mOnClickInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mOnClickInterface = mOnClickInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_vesseltypes, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.item_Title.setText(modelArrayList.get(position));

        holder.item_Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnClickInterface.mOnClickInterface(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title;

        ViewHolder(View itemView) {
            super(itemView);

            item_Title = itemView.findViewById(R.id.item_Title);

        }
    }
}
