package jaohar.com.jaohar.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.PriviewModelItems;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class PreviewItemsAdapter extends RecyclerView.Adapter<PreviewItemsAdapter.ViewHolder> {
    //  private GetSubTotalAmountInterface mGetSubTotalAmountInterface;
    private static int staticPostion = 0;
    private static int staticSerialNum = 0;
    private Activity mActivity;
    private ArrayList<PriviewModelItems> modelArrayList;
    private String strCurrency;

    public PreviewItemsAdapter(Activity mActivity, ArrayList<PriviewModelItems> modelArrayList, String strCurrency) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
//        TAGSIZE
        System.out.println("HESHAV PASSI=====" + this.modelArrayList.size());


        this.strCurrency = strCurrency;
//        this.mGetSubTotalAmountInterface = mGetSubTotalAmountInterface;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pdf_item_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PriviewModelItems tempValue = modelArrayList.get(position);

        int strCOUNT = position+1;
        if (!tempValue.getStrDescription().equals("")) {
            holder.item_idTV.setText(""+strCOUNT);
        }

        if (!tempValue.getStrDescription().equals("")) {
            holder.item_DescriptionTV.setText(tempValue.getStrDescription());
        }
        if (!tempValue.getStrUnitPrice().equals("")) {
            holder.item_UnitPriceTV.setText("" + tempValue.getStrUnitPrice() + " " + strCurrency);
        }
        if (!tempValue.getStrAmount().equals("")) {

            holder.item_AmountTV.setText("" + tempValue.getStrAmount() + " " + strCurrency);
        }
        if (!tempValue.getStrQuantity().equals("")) {
            holder.item_QuantityTV.setText("" + tempValue.getStrQuantity());
        }
        if (position % 2 == 0) {
            holder.itemParentLL.setBackgroundResource(R.drawable.pdf_row_odd);
        } else {
            holder.itemParentLL.setBackgroundResource(R.drawable.pdf_row_even);
        }

    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_idTV, item_DescriptionTV, item_QuantityTV, item_UnitPriceTV, item_AmountTV;
        public LinearLayout itemParentLL, dynamicviewLL, dynamicvLL;

        ViewHolder(View itemView) {
            super(itemView);
            item_idTV = (TextView) itemView.findViewById(R.id.item_idTV);
            item_DescriptionTV = (TextView) itemView.findViewById(R.id.item_DescriptionTV);
            item_QuantityTV = (TextView) itemView.findViewById(R.id.item_QuantityTV);
            item_UnitPriceTV = (TextView) itemView.findViewById(R.id.item_UnitPriceTV);
            item_AmountTV = (TextView) itemView.findViewById(R.id.item_AmountTV);
            itemParentLL = (LinearLayout) itemView.findViewById(R.id.itemParentLL);
            dynamicviewLL = (LinearLayout) itemView.findViewById(R.id.dynamicviewLL);
            dynamicvLL = (LinearLayout) itemView.findViewById(R.id.dynamicvLL);
        }
    }
}