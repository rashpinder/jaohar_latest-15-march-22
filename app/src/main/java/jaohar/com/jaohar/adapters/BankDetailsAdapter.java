package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.BankModel;
import jaohar.com.jaohar.interfaces.DeleteBankDetails;
import jaohar.com.jaohar.interfaces.EditBankDetails;

/**
 * Created by Dharmani Apps on 1/15/2018.
 */

public class BankDetailsAdapter extends RecyclerView.Adapter<BankDetailsAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<BankModel> modelArrayList;
    private DeleteBankDetails mDeleteBankDetails;
    private EditBankDetails mEditBankDetails;

    public BankDetailsAdapter(Activity mActivity, ArrayList<BankModel> modelArrayList, DeleteBankDetails mDeleteBankDetails, EditBankDetails mEditBankDetails) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDeleteBankDetails = mDeleteBankDetails;
        this.mEditBankDetails = mEditBankDetails;
    }

    @Override
    public BankDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_companies, null);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(BankDetailsAdapter.ViewHolder holder, int position) {
        final BankModel tempValue = modelArrayList.get(position);
        holder.item_Title.setText(tempValue.getBenificiary());

        holder.item_Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", tempValue);
                mActivity.setResult(555, returnIntent);
                mActivity.finish();
            }
        });
        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            mEditBankDetails.editBankDetails(tempValue);
            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            mDeleteBankDetails.deleteBankDetail(tempValue);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title,txtEdit,txtDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
        }
    }
}
