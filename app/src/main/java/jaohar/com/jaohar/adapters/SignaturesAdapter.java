package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.SignatureModel;
import jaohar.com.jaohar.interfaces.DeleteSignInterface;
import jaohar.com.jaohar.interfaces.EditSignatureInterface;

/**
 * Created by Dharmani Apps on 1/15/2018.
 */

public class SignaturesAdapter extends RecyclerView.Adapter<SignaturesAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<SignatureModel> modelArrayList;
    private EditSignatureInterface mEditSignatureInterface;
    private DeleteSignInterface mDeleteSignInterface;

    public SignaturesAdapter(Activity mActivity, ArrayList<SignatureModel> modelArrayList, DeleteSignInterface mDeleteSignInterface, EditSignatureInterface mEditSignatureInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mEditSignatureInterface = mEditSignatureInterface;
        this.mDeleteSignInterface = mDeleteSignInterface;
    }

    @Override
    public SignaturesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stamps, null);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(SignaturesAdapter.ViewHolder holder, int position) {
        final SignatureModel tempValue = modelArrayList.get(position);
        holder.item_Title.setText(tempValue.getSignature_name());

        holder.item_Title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Model", tempValue);
                mActivity.setResult(444, returnIntent);
                mActivity.finish();
            }
        });
        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDeleteSignInterface.deleteSignature(tempValue, position);
            }
        });

        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditSignatureInterface.editSignature(tempValue);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title, txtEdit, txtDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
        }
    }
}
