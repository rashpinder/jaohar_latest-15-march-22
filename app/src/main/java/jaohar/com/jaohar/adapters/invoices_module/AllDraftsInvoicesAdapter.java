package jaohar.com.jaohar.adapters.invoices_module;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.interfaces.DeleteInternalNewsInterface;
import jaohar.com.jaohar.interfaces.EditInternalNewsInterface;
import jaohar.com.jaohar.interfaces.invoice_module.DeleteInvoiceDraftInterface;
import jaohar.com.jaohar.interfaces.invoice_module.EditInvoiceDraftInterface;
import jaohar.com.jaohar.models.invoicedraftmodels.AllInvoicesItem;

public class AllDraftsInvoicesAdapter extends RecyclerView.Adapter<AllDraftsInvoicesAdapter.ViewHolder> {
    private Activity mActivity;
//    private List<AllInvoicesItem> modelArrayList;
    private List<InVoicesModel> modelArrayList;
    private EditInvoiceDraftInterface mEditInvoiceDraftInterface;
    private DeleteInvoiceDraftInterface mDeleteInvoiceDraftInterface;

    public AllDraftsInvoicesAdapter(Activity mActivity, List<InVoicesModel> modelArrayList,
                                    EditInvoiceDraftInterface mEditInvoiceDraftInterface,
                                    DeleteInvoiceDraftInterface mDeleteInvoiceDraftInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mEditInvoiceDraftInterface = mEditInvoiceDraftInterface;
        this.mDeleteInvoiceDraftInterface = mDeleteInvoiceDraftInterface;
    }

    @Override
    public AllDraftsInvoicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_invoice_draft, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        InVoicesModel tempValue = modelArrayList.get(position);

        if (tempValue.getmCompaniesModel() != null && tempValue.getmCompaniesModel().getCompany_name() != null)
            holder.companyNameTV.setText(html2text(tempValue.getmCompaniesModel().getCompany_name()));

        if (tempValue.getmVesselSearchInvoiceModel() != null && tempValue.getmVesselSearchInvoiceModel().getVessel_name() != null)
            holder.vesselNameTV.setText(html2text(tempValue.getmVesselSearchInvoiceModel().getVessel_name()));

        if (tempValue.getInvoice_date() != null && tempValue.getInvoice_date() != null)
            holder.inVoiceDateTV.setText(tempValue.getInvoice_date());

        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditInvoiceDraftInterface.mEditInvoiceDraft(tempValue);
            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteInvoiceDraftInterface.mDeleteInvoiceDraft(tempValue, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView companyNameTV, vesselNameTV, inVoiceDateTV, txtEdit, txtDelete;

        ViewHolder(View itemView) {
            super(itemView);

            companyNameTV = itemView.findViewById(R.id.companyNameTV);
            vesselNameTV = itemView.findViewById(R.id.vesselNameTV);
            inVoiceDateTV = itemView.findViewById(R.id.inVoiceDateTV);
            txtEdit = itemView.findViewById(R.id.txtEdit);
            txtDelete = itemView.findViewById(R.id.txtDelete);
        }
    }

    //for converting html to text
    public CharSequence html2text(String html) {
        CharSequence trimmed = noTrailingwhiteLines(Html.fromHtml(html.replace("\n", "<br />")));
        return trimmed;
    }

    //for hiding extra white space
    private CharSequence noTrailingwhiteLines(CharSequence text) {

        if (text.length() > 0)
            while (text.charAt(text.length() - 1) == '\n') {
                text = text.subSequence(0, text.length() - 1);
            }
        return text;
    }
}
