package jaohar.com.jaohar.fragments.sidemenufromapifragments;

import static android.view.View.GONE;

import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.educational_blogs_module.EducationalBlogsActivity;
import jaohar.com.jaohar.activities.educational_blogs_module.SearchBlogsActivity;
import jaohar.com.jaohar.adapters.educational_blogs_module.EducationalBlogsAdapter;
import jaohar.com.jaohar.beans.educational_blogs_module.EducationalBlogsModel;
import jaohar.com.jaohar.fragments.BaseFragment;
import jaohar.com.jaohar.interfaces.PaginationInterface;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class EducationalBlogsFragment extends BaseFragment implements PaginationInterface {
    /*
     * Getting the Class Name
     * */
    String TAG = EducationalBlogsFragment.this.getClass().getSimpleName();

    /*unbinder*/
    Unbinder unbinder;

    /**
     * Widgets
     */
    @BindView(R.id.progressBottomPB)
    ProgressBar progressBottomPB;
    @BindView(R.id.usersRV)
    RecyclerView usersRV;

    int page_no = 1;
    String strLastPage = "TRUE";

    String strSearchText = "";
    /*
     * Setting Up Array List
     * */
    ArrayList<EducationalBlogsModel> modelArrayList = new ArrayList<>();

    /*
     * Setting Up Adapter
     * */
    EducationalBlogsAdapter mEducationalBlogsAdapter;

    PaginationInterface mPagination;

    public EducationalBlogsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_educational_blogs, container, false);

        setStatusBar();

        /* butterknife */
        unbinder = ButterKnife.bind(this, view);

        setViewsIDs();

        toolBarMainLL.setVisibility(View.VISIBLE);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);
        HomeActivity.txtCenter.setText(getResources().getString(R.string.educational_blogs));

        HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setImageResource(R.drawable.ic_magnifying_glass);

        JaoharSingleton.getInstance().setSearchedEducationalBlog("");

        return view;
    }

    /**
     * Set Widget IDS
     **/
    private void setViewsIDs() {
        HomeActivity.imgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SearchBlogsActivity.class));
            }
        });
    }

    public void setAdapter(String type) {
        usersRV.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mEducationalBlogsAdapter = new EducationalBlogsAdapter(getActivity(), modelArrayList, mPagination, type);
        usersRV.setAdapter(mEducationalBlogsAdapter);
    }

    /* *
     * Execute API for getting Educational blogs list
     * @param
     * @user_id
     * */
    public void executeAPI() {
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_no == 1) {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getEducationalPosts(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), String.valueOf(page_no));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                hideProgressDialog();
                modelArrayList.clear();
                Log.e(TAG, "***URLResponce***" + response.body());
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                parseResponce(response.body().toString());

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    void parseResponce(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            String strStatus = mJSonObject.getString("status");
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_blogs");
                if (mjsonArrayData != null) {
                    ArrayList<EducationalBlogsModel> mTempraryList = new ArrayList<>();
                    mTempraryList.clear();
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        EducationalBlogsModel mModel = new EducationalBlogsModel();
                        if (!mJsonDATA.getString("id").equals("")) {
                            mModel.setId(mJsonDATA.getString("id"));
                        }
                        if (!mJsonDATA.getString("file_name").equals("")) {
                            mModel.setFile_name(mJsonDATA.getString("file_name"));
                        }
                        if (!mJsonDATA.getString("link").equals("")) {
                            mModel.setLink(mJsonDATA.getString("link"));
                        }
                        if (!mJsonDATA.getString("link_title").equals("")) {
                            mModel.setLink_title(mJsonDATA.getString("link_title"));
                        }
                        if (!mJsonDATA.getString("link_desc").equals("")) {
                            mModel.setLink_desc(mJsonDATA.getString("link_desc"));
                        }
                        if (!mJsonDATA.getString("link_image").equals("")) {
                            mModel.setLink_image(mJsonDATA.getString("link_image"));
                        }
                        if (!mJsonDATA.getString("message").equals("")) {
                            mModel.setMessage(mJsonDATA.getString("message"));
                        }
                        if (!mJsonDATA.getString("creation_date").equals("")) {
                            mModel.setCreation_date(mJsonDATA.getString("creation_date"));
                        }
                        if (!mJsonDATA.getString("disable").equals("")) {
                            mModel.setDisable(mJsonDATA.getString("disable"));
                        }
//                        if (!mJsonDATA.getString("is_featured").equals("")) {
//                            mModel.setIs_featured(mJsonDATA.getString("is_featured"));
//                        }
                        if (!mJsonDATA.getString("sortOrder").equals("")) {
                            mModel.setSortOrder(mJsonDATA.getString("sortOrder"));
                        }
                        mTempraryList.add(mModel);
                    }
                    modelArrayList.addAll(mTempraryList);
                    if (page_no == 1) {
                        /* for serach 1 else 0 */
                        setAdapter("0");
                    } else {
                        mEducationalBlogsAdapter.notifyDataSetChanged();
                    }
                }
            } else {
                AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void mPaginationInterface(boolean isLastScroll) {
        if (isLastScroll == true) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        progressBottomPB.setVisibility(View.VISIBLE);
                        ++page_no;
                        executeAPI();
                    } else {
                        if (progressBottomPB != null) {
                            progressBottomPB.setVisibility(GONE);
                        }
                    }
                }
            }, 1500);
        }
    }

    /* *
     * Execute Search API for getting Educational blogs list
     * @param
     * @user_id
     * */
    public void executeSearchAPI() {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getEducationPostBySearch(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), strSearchText);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                modelArrayList.clear();
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                parseSearchResponce(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    void parseSearchResponce(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            String strStatus = String.valueOf(mJSonObject.getInt("status"));
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_blogs");

                ArrayList<EducationalBlogsModel> mTempraryList = new ArrayList<>();

                for (int i = 0; i < mjsonArrayData.length(); i++) {
                    JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                    EducationalBlogsModel mModel = new EducationalBlogsModel();
                    if (!mJsonDATA.getString("id").equals("")) {
                        mModel.setId(mJsonDATA.getString("id"));
                    }
                    if (!mJsonDATA.getString("file_name").equals("")) {
                        mModel.setFile_name(mJsonDATA.getString("file_name"));
                    }
                    if (!mJsonDATA.getString("link").equals("")) {
                        mModel.setLink(mJsonDATA.getString("link"));
                    }
                    if (!mJsonDATA.getString("link_title").equals("")) {
                        mModel.setLink_title(mJsonDATA.getString("link_title"));
                    }
                    if (!mJsonDATA.getString("link_desc").equals("")) {
                        mModel.setLink_desc(mJsonDATA.getString("link_desc"));
                    }
                    if (!mJsonDATA.getString("link_image").equals("")) {
                        mModel.setLink_image(mJsonDATA.getString("link_image"));
                    }
                    if (!mJsonDATA.getString("message").equals("")) {
                        mModel.setMessage(mJsonDATA.getString("message"));
                    }
                    if (!mJsonDATA.getString("creation_date").equals("")) {
                        mModel.setCreation_date(mJsonDATA.getString("creation_date"));
                    }
                    if (!mJsonDATA.getString("disable").equals("")) {
                        mModel.setDisable(mJsonDATA.getString("disable"));
                    }
                    if (mJsonDATA.has("is_featured") && !mJsonDATA.getString("is_featured").equals("")) {
                        mModel.setIs_featured(mJsonDATA.getString("is_featured"));
                    }
                    if (!mJsonDATA.getString("sortOrder").equals("")) {
                        mModel.setSortOrder(mJsonDATA.getString("sortOrder"));
                    }
                    mTempraryList.add(mModel);
                }
                modelArrayList.addAll(mTempraryList);
                /* for serach 1 else 0 */
                setAdapter("1");
            } else {
                AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mPagination = this;

        if (modelArrayList != null) {
            modelArrayList.clear();

            /* for search 1 else 0 */
            if (!JaoharSingleton.getInstance().getSearchedEducationalBlog().equals("")) {
                setAdapter("1");
            } else {
                setAdapter("0");
            }
        }

        if (!JaoharSingleton.getInstance().getSearchedEducationalBlog().equals("")) {
            strSearchText = JaoharSingleton.getInstance().getSearchedEducationalBlog();

            executeSearchAPI();
        } else {
            //execute API
            page_no = 1;
            executeAPI();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}