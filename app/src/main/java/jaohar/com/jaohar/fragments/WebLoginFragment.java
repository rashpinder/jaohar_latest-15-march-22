package jaohar.com.jaohar.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.google.zxing.Result;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class WebLoginFragment extends Fragment implements ZXingScannerView.ResultHandler {
    String TAG = WebLoginFragment.this.getClass().getSimpleName();
    /*
     * Bar Code scanner Implemented Methods
     * */
    private ZXingScannerView mScannerView;

    public WebLoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //set status bar
        getActivity().getWindow().setStatusBarColor(Color.WHITE);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        View view = inflater.inflate(R.layout.fragment_web_login, container, false);

        HomeActivity.imgRightLL.setVisibility(View.GONE);
        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.imgBack.setImageResource(R.drawable.menu);
        HomeActivity.toolBarMainLL.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);

        ViewGroup contentFrame = view.findViewById(R.id.frameLayout);
        mScannerView = new ZXingScannerView(getActivity());
        contentFrame.addView(mScannerView);

        openScanner();

        return view;
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.e(TAG, "***Title*** : " + rawResult.getText()); // Prints scan results
        Log.e(TAG, "***Code*** : " + rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)
        String strCodeFormat = rawResult.getBarcodeFormat().toString();
        String strGeneratedCode = rawResult.getText();
    }

    private void openScanner() {
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
