package jaohar.com.jaohar.fragments.sidemenufromapifragments;

import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddInternalNewsActivity;
import jaohar.com.jaohar.activities.AllInternalNewsActivity;
import jaohar.com.jaohar.activities.EditInternalNewsActivity;
import jaohar.com.jaohar.activities.GalleryActivity;
import jaohar.com.jaohar.adapters.InternalNewsAdapter;
import jaohar.com.jaohar.fragments.BaseFragment;
import jaohar.com.jaohar.interfaces.DeleteInternalNewsInterface;
import jaohar.com.jaohar.interfaces.EditInternalNewsInterface;
import jaohar.com.jaohar.interfaces.OpenNewsPopUpInterFace;
import jaohar.com.jaohar.models.CompanyData;
import jaohar.com.jaohar.models.GetCompanyNewsModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;
import jaohar.com.jaohar.views.PinchRecyclerView;
import retrofit2.Call;
import retrofit2.Callback;

public class AllInternalNewsFragment extends BaseFragment {

    String TAG = AllInternalNewsFragment.this.getClass().getSimpleName();

    /*unbinder*/
    Unbinder unbinder;

    //WIDGETS
    boolean isOpenDialogBOX = false;
    PinchRecyclerView newsRV;
    InternalNewsAdapter mNewsAdapter;
    ArrayList<CompanyData> mNewsArrayList = new ArrayList<CompanyData>();
    ArrayList<String> mImageArryList = new ArrayList<String>();

    EditInternalNewsInterface mEditInternalNewsInterface = new EditInternalNewsInterface() {
        @Override
        public void mEditNews(CompanyData mModel) {
            Intent mIntent = new Intent(getActivity(), EditInternalNewsActivity.class);
            mIntent.putExtra("Model", String.valueOf(mModel));
            startActivity(mIntent);
        }
    };

    OpenNewsPopUpInterFace mOpenNewsPopUP = new OpenNewsPopUpInterFace() {
        @Override
        public void openNewsPopUpInterFace(String strWebTxt, String strPhotoURL) {
            if (isOpenDialogBOX == true) {
                isOpenDialogBOX = false;
            } else {
                setUpNewsDialog(strWebTxt, strPhotoURL);
            }
        }
    };

    DeleteInternalNewsInterface mDeleteInternalNewsInterface = new DeleteInternalNewsInterface() {
        @Override
        public void mDeleteNews(CompanyData mModel) {
            deleteConfirmDialog(mModel);
        }
    };

    public AllInternalNewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_internal_news, container, false);

        //set status bar
        setStatusBar();

        /* butterknife */
        unbinder = ButterKnife.bind(this, view);

        setViewsIDs(view);
        setClickListner();

        toolBarMainLL.setVisibility(View.VISIBLE);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);
        HomeActivity.txtCenter.setText(getResources().getString(R.string.all_internal_news));

        HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setImageResource(R.drawable.plus_symbol);

        return view;
    }

    private void setViewsIDs(View view) {
        /*SET UP TOOLBAR*/
        newsRV = view.findViewById(R.id.newsRV);

//        if (JaoharConstants.IS_SEE_ALL_INTERVALS_NEWS_CLICK == true) {
//            imgRightLL.setVisibility(View.GONE);
//        }

        if (!Utilities.isNetworkAvailable(getActivity())) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            executeGettingAllNews();
        }
    }

    public void setUpNewsDialog(String strNormalTEXT, final String strPhotoURL) {

        final Dialog searchDialog = new Dialog(getActivity());
        if (!searchDialog.isShowing()) {
            isOpenDialogBOX = true;
            searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            searchDialog.setContentView(R.layout.activity_full_news_description);
            searchDialog.setCanceledOnTouchOutside(true);
            searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Window window = searchDialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            ImageView add_image1 = (ImageView) searchDialog.findViewById(R.id.add_image1);
            WebView showTXT = (WebView) searchDialog.findViewById(R.id.showTXT);
            final ProgressBar progressbar1 = searchDialog.findViewById(R.id.progressbar1);
            progressbar1.setVisibility(View.VISIBLE);
            if (!strPhotoURL.equals("")) {
                Glide.with(getActivity()).load(strPhotoURL).into(add_image1);
            }
            add_image1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!strPhotoURL.equals("")) {
                        mImageArryList.clear();
                        mImageArryList.add(strPhotoURL);
                        Intent intent = new Intent(getActivity(), GalleryActivity.class);
                        intent.putStringArrayListExtra("LIST1", mImageArryList);
                        startActivity(intent);
                        getActivity().finish();
                    }
                }
            });
            showTXT.getSettings().setJavaScriptEnabled(true);
            showTXT.getSettings().setLoadWithOverviewMode(true);
            showTXT.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    progressbar1.setVisibility(View.VISIBLE);
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, final String url) {
                    progressbar1.setVisibility(View.GONE);
                }

                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    super.onReceivedError(view, request, error);
                    progressbar1.setVisibility(View.GONE);
                }
            });

            showTXT.loadDataWithBaseURL(null, strNormalTEXT, "text/html", "UTF-8", null);
            WebSettings webSettings = showTXT.getSettings();
            Resources res = getActivity().getResources();
            webSettings.setDefaultFontSize((int) res.getDimension(R.dimen._3sdp));
            searchDialog.show();
        }
    }

    private void setClickListner() {
        HomeActivity.imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddInternalNewsActivity.class));
                overridePendingTransitionEnter(getActivity());
            }
        });
    }

    private void setAdapter() {
        newsRV.setNestedScrollingEnabled(false);
        newsRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mNewsAdapter = new InternalNewsAdapter(getActivity(), mNewsArrayList, mEditInternalNewsInterface, mDeleteInternalNewsInterface, mOpenNewsPopUP);
        newsRV.setAdapter(mNewsAdapter);
    }

    private void executeAllInternalNewsApi() {
        mNewsArrayList.clear();
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetCompanyNewsModel> call1 = mApiInterface.getInternalNewsRequest();
        call1.enqueue(new Callback<GetCompanyNewsModel>() {
                          @Override
                          public void onResponse(Call<GetCompanyNewsModel> call, retrofit2.Response<GetCompanyNewsModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetCompanyNewsModel mModel = response.body();
                              if (mModel.getStatus().equals("1")) {
                                  mNewsArrayList = mModel.getData();
                                  /*setAdapter*/
                                  setAdapter();
                              } else {
                                  AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetCompanyNewsModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    private void executeGetCompanyNewsApi() {
        mNewsArrayList.clear();
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetCompanyNewsModel> call1 = mApiInterface.getCompanyNewsRequest();
        call1.enqueue(new Callback<GetCompanyNewsModel>() {
                          @Override
                          public void onResponse(Call<GetCompanyNewsModel> call, retrofit2.Response<GetCompanyNewsModel> response) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "***URLResponce***" + response);
                              GetCompanyNewsModel mModel = response.body();
                              if (mModel.getStatus().equals("1")) {
                                  mNewsArrayList = mModel.getData();
                                  /*setAdapter*/
                                  setAdapter();
                              } else {
                                  AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<GetCompanyNewsModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    private void executeGettingAllNews() {
        mNewsArrayList.clear();
        AlertDialogManager.showProgressDialog(getActivity());
        if (JaoharConstants.IS_CLICK_FROM_COMPANY == true) {
            executeGetCompanyNewsApi();
        } else {
            executeAllInternalNewsApi();
        }
    }

    public void deleteConfirmDialog(final CompanyData mNewsModel) {
        final Dialog deleteConfirmDialog = new Dialog(getActivity());
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_internal_news));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mNewsModel);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    /*Execute Delete API*/
    private void executeDeleteAPI(CompanyData newsModel) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteInternalNewsRequest(newsModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    showAlertDialog(getActivity(), getActivity().getTitle().toString(), mModel.getMessage());
                } else if (mModel.getStatus() == 0) {
                    showAlertDialog(getActivity(), getActivity().getTitle().toString(), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                executeGettingAllNews();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}