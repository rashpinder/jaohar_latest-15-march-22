package jaohar.com.jaohar.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import jaohar.com.jaohar.activities.trash_module.BankTrashActivity;
import jaohar.com.jaohar.activities.trash_module.CompanyTrashActivity;
import jaohar.com.jaohar.activities.trash_module.CurrencyTrashActivity;
import jaohar.com.jaohar.activities.trash_module.InvoiceTrashActivity;
import jaohar.com.jaohar.activities.trash_module.UniversalInvoiceTrashActivity;
import jaohar.com.jaohar.activities.trash_module.VesselTrashActivity;
import jaohar.com.jaohar.R;

public class ManageUtilitiesFragment extends Fragment {
    String TAG = ManageUtilitiesFragment.this.getClass().getSimpleName();
    View rootView;
    private LinearLayout invoiceTrashLL, universal_invoice_trashLL, companyLL, currencyLL, vesselLL, signLL,
            stampLL, invoicing_companyLL, mailLL, newsLL, internal_news_trashLL,bankLL;
    /*
     * Initialize  Unbinder for butterknife;
     * */


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_manage_utilities, container, false);

        /*
         * Declare Widgets IDS
         * */
        setWidgetIDs(rootView);
        /*
         * Declare ClickListeners
         * */
        setClickListner();

        return rootView;
    }

    private void setWidgetIDs(View rootView) {
        invoiceTrashLL = rootView.findViewById(R.id.invoiceTrashLL);
        universal_invoice_trashLL = rootView.findViewById(R.id.universal_invoice_trashLL);
        companyLL = rootView.findViewById(R.id.companyLL);
        currencyLL = rootView.findViewById(R.id.currencyLL);
        vesselLL = rootView.findViewById(R.id.vesselLL);
        signLL = rootView.findViewById(R.id.signLL);
        stampLL = rootView.findViewById(R.id.stampLL);
        invoicing_companyLL = rootView.findViewById(R.id.invoicing_companyLL);
        mailLL = rootView.findViewById(R.id.mailLL);
        newsLL = rootView.findViewById(R.id.newsLL);
        internal_news_trashLL = rootView.findViewById(R.id.internal_news_trashLL);
        bankLL = rootView.findViewById(R.id.bankLL);


    }

    /*
     * Widget Click listener
     * */
    private void setClickListner() {
        invoiceTrashLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent =new Intent(getActivity(), InvoiceTrashActivity.class);
                mIntent.putExtra("isClick", "manageUtilities");
                startActivity(mIntent);
                getActivity().finish();

            }
        });
        universal_invoice_trashLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent =new Intent(getActivity(), UniversalInvoiceTrashActivity.class);
                mIntent.putExtra("isClick", "manageUtilities");
                startActivity(mIntent);
                getActivity().finish();
            }
        });
        companyLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent =new Intent(getActivity(), CompanyTrashActivity.class);
                mIntent.putExtra("isClick", "manageUtilities");
                startActivity(mIntent);
                getActivity().finish();

            }
        });
        currencyLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent =new Intent(getActivity(), CurrencyTrashActivity.class);
                mIntent.putExtra("isClick", "manageUtilities");
                startActivity(mIntent);
                getActivity().finish();
            }
        });
        vesselLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent =new Intent(getActivity(), VesselTrashActivity.class);
                mIntent.putExtra("isClick", "manageUtilities");
                startActivity(mIntent);
                getActivity().finish();

            }
        });

        bankLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent =new Intent(getActivity(), BankTrashActivity.class);
                mIntent.putExtra("isClick", "manageUtilities");
                startActivity(mIntent);
                getActivity().finish();

            }
        });
        signLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });
        stampLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });
        invoicing_companyLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });
        mailLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });
        newsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });
        internal_news_trashLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /*
     * Fragment Override method
     * #onDestroy
     * */
    public void onDestroy() {
        super.onDestroy();

    }


}
