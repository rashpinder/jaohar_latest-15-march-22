package jaohar.com.jaohar.fragments.sidemenufromapifragments;

import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddressBookListStaffActivity;
import jaohar.com.jaohar.activities.AdminRoleActivity;
import jaohar.com.jaohar.activities.DynamicAditionsActivity;
import jaohar.com.jaohar.activities.MailIstManagerActivity;
import jaohar.com.jaohar.activities.MassPushActivity;
import jaohar.com.jaohar.activities.NewRequestActivity;
import jaohar.com.jaohar.activities.UserProfileActivity;
import jaohar.com.jaohar.activities.VesselsShipsActivity;
import jaohar.com.jaohar.activities.forum_module_admin.AdminForumActivity;
import jaohar.com.jaohar.activities.trash_module.TrashActivity;
import jaohar.com.jaohar.adapters.manager_module.ManagerModulesAdapter;
import jaohar.com.jaohar.beans.ManagerModule.ManagerModulesModel;
import jaohar.com.jaohar.fragments.BaseFragment;
import jaohar.com.jaohar.interfaces.managermodules.ClickManagerModulesInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class ManagerFragment extends BaseFragment implements ClickManagerModulesInterface{
    String TAG = ManagerFragment.this.getClass().getSimpleName();

    /*unbinder*/
    Unbinder unbinder;

    //WIDGETS
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    CircleImageView imgRight, statusIMG;
    TextView txtCenter, badgesTV;
    RecyclerView tabListRV;
    LinearLayout homeLL, aboutLL, servicesLL, ourOfficesLL, ourHistory, contactUsLL, staffLL, adminAndRoleLL, newRequestsLL, vesselsShipsLL, logoutLL,
            dynamicAdditionLL, massPushLL, mailingListLL, addressBookLL, trashLL, forumLL;

    List<ManagerModulesModel> ManagerModulesModelList = new ArrayList<>();
    ClickManagerModulesInterface clickManagerModulesInterface;


    public ManagerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manager, container, false);

        /* butterknife */
        unbinder = ButterKnife.bind(this, view);

        //set status bar
        setStatusBar();
        
        /* set click interface */
        clickManagerModulesInterface = this;

        /* initialize all ids */
        setViewsIDs(view);

        toolBarMainLL.setVisibility(View.VISIBLE);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);
        HomeActivity.txtCenter.setText(getResources().getString(R.string.chat));

        HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setImageResource(R.drawable.plus_symbol);

        /* set all modules list */
        setModulesList();

        return view;
    }
    
    private void setModulesList() {
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.home), R.drawable.ic_home_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.admin_and_role), R.drawable.ic_admin_role_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.new_requests), R.drawable.ic_new_requests_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.vessels_ships), R.drawable.ic_vessels_ships_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.staff), R.drawable.ic_staff_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.dynamic_addition), R.drawable.ic_dynamic_additions_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.mass_push), R.drawable.ic_mass_push_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.about), R.drawable.ic_about_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.services), R.drawable.ic_services_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.our_offfices), R.drawable.ic_our_offices_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.our_history), R.drawable.ic_our_history_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.mailing_list), R.drawable.ic_mailing_list_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.contact_us), R.drawable.ic_contact_us_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.address_book_), R.drawable.ic_address_book_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.trash), R.drawable.ic_trash_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.fund_trash), R.drawable.ic_fund_trash_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.letter_trash), R.drawable.ic_letter_trash_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.forum), R.drawable.ic_form_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.chat), R.drawable.ic_chat_png));

        /* set modules adapter */
        setAdapter("0");
    }

    private void setAdapter(String BadgeCount) {
        tabListRV.setHasFixedSize(false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        tabListRV.setLayoutManager(gridLayoutManager);
        ManagerModulesAdapter mAdapter = new ManagerModulesAdapter(getActivity(), ManagerModulesModelList, clickManagerModulesInterface, BadgeCount);
        tabListRV.setAdapter(mAdapter);
    }

    public void setViewsIDs(View view) {
        /*SET UP TOOLBAR*/
        llLeftLL = view.findViewById(R.id.llLeftLL);
        imgRight = view.findViewById(R.id.imgRight);
        statusIMG = view.findViewById(R.id.statusIMG);
        imgRightLL = view.findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);
        txtCenter = view.findViewById(R.id.txtCenter);

        /*MAIN LAYOUT IDS*/
        homeLL = view.findViewById(R.id.homeLL);
        aboutLL = view.findViewById(R.id.aboutLL);
        servicesLL = view.findViewById(R.id.servicesLL);
        ourOfficesLL = view.findViewById(R.id.ourOfficesLL);
        ourHistory = view.findViewById(R.id.ourHistory);
        contactUsLL = view.findViewById(R.id.contactUsLL);
        staffLL = view.findViewById(R.id.staffLL);
        adminAndRoleLL = view.findViewById(R.id.adminAndRoleLL);
        newRequestsLL = view.findViewById(R.id.newRequestsLL);
        vesselsShipsLL = view.findViewById(R.id.vesselsShipsLL);
        trashLL = view.findViewById(R.id.trashLL);
        dynamicAdditionLL = view.findViewById(R.id.dynamicAdditionLL);
        massPushLL = view.findViewById(R.id.massPushLL);
        mailingListLL = view.findViewById(R.id.mailingListLL);
        addressBookLL = view.findViewById(R.id.addressBookLL);
        forumLL = view.findViewById(R.id.forumLL);
        badgesTV = view.findViewById(R.id.badgesTV);
        tabListRV = view.findViewById(R.id.tabListRV);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!Utilities.isNetworkAvailable(getActivity())) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            /*Execute Api*/
            gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
        }
    }

    private void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Is_click_form_Manager = true;
                JaoharConstants.BOOLEAN_USER_LOGOUT = false;
                JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
                Intent mIntent = new Intent(getActivity(), UserProfileActivity.class);
                startActivity(mIntent);
            }
        });

        addressBookLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddressBookListStaffActivity.class));
            }
        });

        homeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), HomeActivity.class);
                mIntent.putExtra(JaoharConstants.LOGIN, "Home");
                startActivity(mIntent);
                getActivity().finish();
                overridePendingTransitionExit(getActivity());
            }
        });

        aboutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });

        servicesLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });

        ourOfficesLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });

        ourHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });

        mailingListLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MailIstManagerActivity.class));
                overridePendingTransitionEnter(getActivity());
            }
        });

        contactUsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });

        forumLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), AdminForumActivity.class);
                mIntent.putExtra("isClick", "vesselClick");
                startActivity(mIntent);
                overridePendingTransitionEnter(getActivity());
            }
        });

        staffLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), HomeActivity.class);
                mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
                startActivity(mIntent);
                getActivity().finish();
                overridePendingTransitionExit(getActivity());
            }
        });

        adminAndRoleLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AdminRoleActivity.class));
                overridePendingTransitionEnter(getActivity());
            }
        });

        newRequestsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Is_click_form_Manager = true;
                startActivity(new Intent(getActivity(), NewRequestActivity.class));
                getActivity().finish();
                overridePendingTransitionEnter(getActivity());
            }
        });

        vesselsShipsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Is_click_form_Manager = true;
                startActivity(new Intent(getActivity(), VesselsShipsActivity.class));
                getActivity().finish();
                overridePendingTransitionEnter(getActivity());
            }
        });

        trashLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), TrashActivity.class);
                mIntent.putExtra("isClick", "vesselClick");
                startActivity(mIntent);
                overridePendingTransitionEnter(getActivity());
            }
        });

        dynamicAdditionLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), DynamicAditionsActivity.class));
                overridePendingTransitionEnter(getActivity());
            }
        });

        massPushLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MassPushActivity.class));
                overridePendingTransitionEnter(getActivity());
            }
        });
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Intent mIntent = new Intent(mActivity, HomeActivity.class);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
//        mActivity.startActivity(mIntent);
//        mActivity.getActivity().finish();
//    }

    //ADMIN_LOGOUT_API
    private void executeAPI() {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.adminLogoutRequest(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""), JaoharPreference.readString(getActivity(), JaoharPreference.FCM_REFRESHED_TOKEN, ""), "Android");
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    SharedPreferences preferences = JaoharPreference.getPreferences(getActivity());
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove(JaoharPreference.IS_LOGGED_IN_ADMIN);
                    editor.remove(JaoharPreference.ADMIN_ID);
                    editor.remove(JaoharPreference.ADMIN_EMAIL);
                    editor.remove(JaoharPreference.ADMIN_ROLE);
                    editor.commit();
                    Intent mIntent = new Intent(getActivity(), HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
                    getActivity().startActivity(mIntent);
                    getActivity().finish();
                    overridePendingTransitionEnter(getActivity());
                } else if (mModel.getStatus() == 0) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    /* execute get user profile api */
    private void gettingProfileDATA(String strLoginID) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getuserProfileRequest(strLoginID);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    if (strStatus.equals("1")) {
                        if (!mJsonObject.isNull("data")) {
                            JSONObject mJsonDATA = mJsonObject.getJSONObject("data");
                            if (!mJsonDATA.getString("image").equals("")) {
                                String strImage = mJsonDATA.getString("image");
                                Glide.with(getActivity()).load(strImage).into(imgRight);
                            } else {
                                imgRight.setImageResource(R.drawable.profile);
                            }

                            if (!mJsonDATA.getString("chat_status").equals("")) {
                                String strImage = mJsonDATA.getString("chat_status");
                                if (strImage.equals("Available")) {
                                    statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.green_status_icon));
                                } else if (strImage.equals("Busy")) {
                                    statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.red_status_icon));
                                } else if (strImage.equals("Invisible")) {
                                    statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.grey_status_icon));
                                }
                            }

                            if (!mJsonDATA.getString("total_unread_forum_message_count").equals("")) {
                                String strBadgesCount = mJsonDATA.getString("total_unread_forum_message_count");
                                if (strBadgesCount.equals("0")) {
                                    badgesTV.setVisibility(View.GONE);
                                } else {
                                    badgesTV.setVisibility(View.VISIBLE);
                                    badgesTV.setText(strBadgesCount);
                                    setAdapter(strBadgesCount);
                                }
                            } else {
                                badgesTV.setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    /* execute get user profile api */
    @Override
    public void mClickManagerModulesInterface(int position, String name) {
        if (name.equalsIgnoreCase(getString(R.string.home))) {
            Intent mIntent = new Intent(getActivity(), HomeActivity.class);
            mIntent.putExtra(JaoharConstants.LOGIN, "Home");
            startActivity(mIntent);
            getActivity().finish();
            overridePendingTransitionExit(getActivity());
        } else if (name.equalsIgnoreCase(getString(R.string.admin_and_role))) {
            startActivity(new Intent(getActivity(), AdminRoleActivity.class));
            overridePendingTransitionEnter(getActivity());
        } else if (name.equalsIgnoreCase(getString(R.string.new_requests))) {
            JaoharConstants.Is_click_form_Manager = true;
            startActivity(new Intent(getActivity(), NewRequestActivity.class));
            getActivity().finish();
            overridePendingTransitionEnter(getActivity());
        } else if (name.equalsIgnoreCase(getString(R.string.vessels_ships))) {
            JaoharConstants.Is_click_form_Manager = true;
            startActivity(new Intent(getActivity(), VesselsShipsActivity.class));
//            getActivity().finish();
            overridePendingTransitionEnter(getActivity());
        } else if (name.equalsIgnoreCase(getString(R.string.staff))) {
            Intent mIntent = new Intent(getActivity(), HomeActivity.class);
            mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
            startActivity(mIntent);
            getActivity().finish();
            overridePendingTransitionExit(getActivity());
        } else if (name.equalsIgnoreCase(getString(R.string.dynamic_addition))) {
            startActivity(new Intent(getActivity(), DynamicAditionsActivity.class));
            overridePendingTransitionEnter(getActivity());
        } else if (name.equalsIgnoreCase(getString(R.string.mass_push))) {
            startActivity(new Intent(getActivity(), MassPushActivity.class));
            overridePendingTransitionEnter(getActivity());
        } else if (name.equalsIgnoreCase(getString(R.string.about))) {
            Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.services))) {
            Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.our_offfices))) {
            Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.our_history))) {
            Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.mailing_list))) {
            startActivity(new Intent(getActivity(), MailIstManagerActivity.class));
            overridePendingTransitionEnter(getActivity());
        } else if (name.equalsIgnoreCase(getString(R.string.contact_us))) {
            Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.address_book_))) {
            startActivity(new Intent(getActivity(), AddressBookListStaffActivity.class));
        } else if (name.equalsIgnoreCase(getString(R.string.trash))) {
            Intent mIntent = new Intent(getActivity(), TrashActivity.class);
            mIntent.putExtra("isClick", "vesselClick");
            startActivity(mIntent);
            overridePendingTransitionEnter(getActivity());
        } else if (name.equalsIgnoreCase(getString(R.string.fund_trash))) {
            Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.letter_trash))) {
            Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.forum))) {
            Intent mIntent = new Intent(getActivity(), AdminForumActivity.class);
            mIntent.putExtra("isClick", "vesselClick");
            startActivity(mIntent);
            overridePendingTransitionEnter(getActivity());
        } else if (name.equalsIgnoreCase(getString(R.string.chat))) {
            Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}