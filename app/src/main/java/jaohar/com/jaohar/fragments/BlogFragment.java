package jaohar.com.jaohar.fragments;

import static android.view.View.GONE;
import static jaohar.com.jaohar.HomeActivity.switchFragment;
import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.blog_module.BlogDescriptionActivity;
import jaohar.com.jaohar.adapters.blog_adapter.BlogListAdapter;
import jaohar.com.jaohar.beans.blog_module.Blog_List_Model;
import jaohar.com.jaohar.interfaces.blog_module.Blog_Item_Click_Interface;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class BlogFragment extends Fragment implements PaginationListForumAdapter, Blog_Item_Click_Interface {
    /*
     * String TAG
     * */
    String TAG = BlogFragment.this.getClass().getSimpleName();

    /*
     * Set Up root View
     * */
    View rootView;

    /*unbinder*/
    Unbinder unbinder;

    /*
     * Widgets ,Arraylist And Adapter
     * */
    @BindView(R.id.blogLayoutBar)
    RelativeLayout blogLayoutBar;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtCenter)
    TextView txtCenter;

    RecyclerView blogRV;
    ArrayList<Blog_List_Model> modelArrayList = new ArrayList<>();
    BlogListAdapter mAdapter;
    int page_num = 1;
    PaginationListForumAdapter mPaination;
    String strLastPage = "FALSE";
    boolean isSwipeRefresh = false;
    SwipeRefreshLayout swipeToRefresh;
    ProgressBar progressBottomPB;
    Blog_Item_Click_Interface mBlogClickInterface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //set status bar
        getActivity().getWindow().setStatusBarColor(Color.WHITE);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_blog, container, false);

        /* butterknife */
        unbinder = ButterKnife.bind(this, rootView);

        setWidgetIDs(rootView);
        mPaination = this;
        mBlogClickInterface = this;

        /* check redirection of blogs screen */
        if (JaoharConstants.is_Staff_FragmentClick) {
            blogLayoutBar.setVisibility(View.VISIBLE);
            toolBarMainLL.setVisibility(GONE);
        } else {
            blogLayoutBar.setVisibility(GONE);
            toolBarMainLL.setVisibility(View.VISIBLE);

            HomeActivity.txtCenter.setText(getString(R.string.blogs));
            HomeActivity.bottomMainLL.setVisibility(View.GONE);
            HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
            HomeActivity.txtCenter.setGravity(Gravity.START);
        }

        return rootView;
    }

    private void setWidgetIDs(View rootView) {
        blogRV = rootView.findViewById(R.id.blogRV);
        progressBottomPB = rootView.findViewById(R.id.progressBottomPB);
        swipeToRefresh = rootView.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;

                modelArrayList.clear();
                page_num = 1;
                if (Utilities.isNetworkAvailable(getActivity()) == false) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    gettingBlogDATA();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        modelArrayList.clear();
        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            gettingBlogDATA();
        }
    }

    /*
     * API to Get All Blog list Using API
     * */
    private void gettingBlogDATA() {
        if (!strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_num == 1) {
                AlertDialogManager.showProgressDialog(getActivity());
                isSwipeRefresh = false;
                swipeToRefresh.setRefreshing(false);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllBlog(String.valueOf(page_num));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                try {
                    if (progressBottomPB != null) {
                        progressBottomPB.setVisibility(GONE);
                    }
                    if (isSwipeRefresh == true) {
                        isSwipeRefresh = false;
                        swipeToRefresh.setRefreshing(false);
                    }
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    if (strStatus.equals("1")) {
                        parseResponce(response.body().toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
            }
        });
    }


    /*
     * Data Parsing
     * */
    private void parseResponce(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
            strLastPage = mjsonDATA.getString("last_page");
            JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_blogs");
            if (mjsonArrayData != null) {
                ArrayList<Blog_List_Model> mTempraryList = new ArrayList<>();
                mTempraryList.clear();
                for (int i = 0; i < mjsonArrayData.length(); i++) {
                    JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                    Blog_List_Model mModel = new Blog_List_Model();
                    if (!mJsonDATA.getString("blog_id").equals("")) {
                        mModel.setBlog_id(mJsonDATA.getString("blog_id"));
                    }
                    if (!mJsonDATA.getString("category").equals("")) {
                        mModel.setCategory(mJsonDATA.getString("category"));
                    }
                    if (!mJsonDATA.getString("tag").equals("")) {
                        mModel.setTag(mJsonDATA.getString("tag"));
                    }

                    if (!mJsonDATA.getString("title").equals("")) {
                        mModel.setTitle(mJsonDATA.getString("title"));
                    }
                    if (!mJsonDATA.getString("image").equals("")) {
                        mModel.setImage(mJsonDATA.getString("image"));
                    }
                    if (!mJsonDATA.getString("content").equals("")) {
                        mModel.setContent(mJsonDATA.getString("content"));
                    }
                    if (!mJsonDATA.getString("created_by").equals("")) {
                        mModel.setCreated_by(mJsonDATA.getString("created_by"));
                    }
                    if (!mJsonDATA.getString("creation_date").equals("")) {
                        mModel.setCreation_date(mJsonDATA.getString("creation_date"));
                    }
                    if (!mJsonDATA.getString("share_url").equals("")) {
                        mModel.setShare_url(mJsonDATA.getString("share_url"));
                    }

                    mTempraryList.add(mModel);


                }

                modelArrayList.addAll(mTempraryList);

                if (page_num == 1) {
                    setAdapter();
                } else {
                    mAdapter.notifyDataSetChanged();
                }
//                    setAdapter();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     * Adapter
     **/
    private void setAdapter() {
        blogRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new BlogListAdapter(getActivity(), modelArrayList, mPaination, mBlogClickInterface);
        blogRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void mPaginationforVessels(boolean isLastScroll) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (strLastPage.equals("FALSE")) {
                    if (progressBottomPB != null) {
                        progressBottomPB.setVisibility(View.VISIBLE);
                    }
                    swipeToRefresh.setEnabled(true);
                    ++page_num;
                    if (Utilities.isNetworkAvailable(getActivity()) == false) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        gettingBlogDATA();
                    }
                } else {
                    if (progressBottomPB != null) {
                        progressBottomPB.setVisibility(GONE);
                    }
                }
            }
        }, 150);
    }

    @Override
    public void BlogInterface(Blog_List_Model mModel) {
        Intent mIntent = new Intent(getActivity(), BlogDescriptionActivity.class);
        mIntent.putExtra("model", mModel);
        startActivity(mIntent);
    }

    @OnClick({R.id.llLeftLL})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.llLeftLL:

                blogLayoutBar.setVisibility(GONE);
                toolBarMainLL.setVisibility(View.VISIBLE);

                JaoharConstants.is_Staff_FragmentClick = false;
                HomeActivity.staffToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                HomeActivity.mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment(getActivity(), new StaffFragment(), JaoharConstants.STAFF_FRAGMENT, false, null);

                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}
