package jaohar.com.jaohar.fragments.sidemenufromapifragments;

import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddAddessBookActivity;
import jaohar.com.jaohar.activities.AddressBookListStaffActivity;
import jaohar.com.jaohar.activities.EditAddressBookActivity;
import jaohar.com.jaohar.adapters.AddressBookAdapter;
import jaohar.com.jaohar.fragments.BaseFragment;
import jaohar.com.jaohar.interfaces.DeleteAddressBook;
import jaohar.com.jaohar.interfaces.EditAddressBookInterface;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.models.AddressBookListModel;
import jaohar.com.jaohar.models.AddressBookSearchModel;
import jaohar.com.jaohar.models.AllContact;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AddressBookListStaffFragment extends BaseFragment {

    String TAG = AddressBookListStaffFragment.this.getClass().getSimpleName();

    /*unbinder*/
    Unbinder unbinder;

    private String strLastPage = "FALSE", strUSERID = "";
    private RelativeLayout resetRL1;
    private EditText editSearchET;
    private RecyclerView addressBookRV;
    private SwipeRefreshLayout swipeToRefresh;
    private AddressBookAdapter mAdapter;
    private ArrayList<AllContact> mArrayList = new ArrayList<>();
    private ArrayList<AllContact> loadMoreArrayList = new ArrayList<>();
    private int page_no = 1;
    private boolean isSwipeRefresh = false;
    private final boolean isAdvanceSearch = false;
    private boolean isNormalSearch = false;

    ProgressBar progressBottomPB;

    DeleteAddressBook mDeleteAddressBook = new DeleteAddressBook() {
        @Override
        public void mDeleteAddressBook(String strContactID) {
            deleteConfirmDialog(strContactID);
        }
    };

    EditAddressBookInterface mEditInterface = new EditAddressBookInterface() {
        @Override
        public void mEditAddressBookInterface(String strContactID) {
            Intent mIntent = new Intent(getActivity(), EditAddressBookActivity.class);
            mIntent.putExtra("isAddClick", "false");
            mIntent.putExtra("contactID", strContactID);
            startActivity(mIntent);
        }
    };

    /**
     * Recycler View Pagination Adapter Interface
     **/
    PaginationListForumAdapter mPaginationInterFace = new PaginationListForumAdapter() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isNormalSearch == false) {
                            if (strLastPage.equals("FALSE")) {
                                progressBottomPB.setVisibility(View.VISIBLE);
                                ++page_no;
                                addressBookRV.stopScroll();
                                executeAPI(page_no);
                            }
                        }
                    }
                }, 1000);
            }
        }
    };

    public AddressBookListStaffFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_address_book_list_staff, container, false);

        /* butterknife */
        unbinder = ButterKnife.bind(this, view);

        setStatusBar();

        JaoharSingleton.getInstance().setSearchedVessel("");

        setViewsIDs(view);
        setClickListner();

        toolBarMainLL.setVisibility(View.VISIBLE);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);
        HomeActivity.txtCenter.setText(getResources().getString(R.string.address_book));

        HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setImageResource(R.drawable.plus_symbol);

        return view;
    }

    private void setViewsIDs(View view) {
        progressBottomPB = view.findViewById(R.id.progressBottomPB);

        editSearchET = view.findViewById(R.id.editSearchET);
        resetRL1 = view.findViewById(R.id.resetRL1);
        addressBookRV = view.findViewById(R.id.addressBookRV);

        swipeToRefresh = view.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                isNormalSearch = false;
                mArrayList.clear();
                loadMoreArrayList.clear();
                mAdapter.notifyDataSetChanged();
                page_no = 1;
                editSearchET.setText("");
                executeAPI(page_no);
            }
        });

        if (JaoharConstants.is_Staff_FragmentClick == true) {
            strUSERID = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
        } else {
            strUSERID = JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, "");
        }
    }

    private void setClickListner() {
        HomeActivity.imgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), AddAddessBookActivity.class);
                mIntent.putExtra("isAddClick", "true");
                startActivity(mIntent);
            }
        });

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (Utilities.isNetworkAvailable(getActivity()) == false) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        resetRL1.setVisibility(View.VISIBLE);
                        executeSearchAPI();
                    }

                    return true;
                }
                return false;
            }
        });

        resetRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isNormalSearch = false;
                mArrayList.clear();
                loadMoreArrayList.clear();
                page_no = 1;
                editSearchET.setText("");
                addressBookRV.stopScroll();
                mAdapter.notifyDataSetChanged();
                executeAPI(page_no);
            }
        });


        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    resetRL1.setVisibility(View.VISIBLE);
                } else {
                    resetRL1.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void executeSearchAPI() {
        mArrayList.clear();
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<AddressBookSearchModel> call1 = mApiInterface.searchAddressBookRequest(editSearchET.getText().toString().trim(), strUSERID);
        call1.enqueue(new Callback<AddressBookSearchModel>() {
            @Override
            public void onResponse(Call<AddressBookSearchModel> call, retrofit2.Response<AddressBookSearchModel> response) {
                AlertDialogManager.hideProgressDialog();
                AddressBookSearchModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    isNormalSearch = true;
//                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" +  mModel.getMessage());
                    mArrayList = mModel.getAllContactList();
                    setAdapter();
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddressBookSearchModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            if (strLastPage.equals("FALSE")) {
                editSearchET.setText("");
                page_no = 1;
                mArrayList.clear();
                loadMoreArrayList.clear();
                AlertDialogManager.showProgressDialog(getActivity());
                addressBookRV.stopScroll();

                executeAPI(page_no);
            }
        }
    }

    public void executeAPI(int page_no) {
        loadMoreArrayList.clear();
        if (strLastPage.equals("FALSE")) {
            progressBottomPB.setVisibility(View.GONE);
        } else {
            if (page_no == 1) {
                swipeToRefresh.setRefreshing(false);
                progressBottomPB.setVisibility(View.GONE);
            }
        }
        addressBookRV.stopScroll();
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<AddressBookListModel> call1 = mApiInterface.getAllAddressBookRequest(String.valueOf(page_no), strUSERID);
        call1.enqueue(new Callback<AddressBookListModel>() {
                          @Override
                          public void onResponse(Call<AddressBookListModel> call, retrofit2.Response<AddressBookListModel> response) {
                              Log.e(TAG, "******Response*****" + response);
                              resetRL1.setVisibility(View.GONE);

                              Log.e(TAG, "******Response*****" + response);
                              AlertDialogManager.hideProgressDialog();
                              progressBottomPB.setVisibility(View.GONE);
                              if (isSwipeRefresh) {
                                  swipeToRefresh.setRefreshing(false);
                              }

                              AddressBookListModel mModel = response.body();

                              if (mModel.getStatus().equals("1")) {
//                                  mArrayList = mModel.getData().getAllContactList();
                                  strLastPage = mModel.getData().getLastPage();
                                  if (page_no == 1) {
                                      mArrayList = mModel.getData().getAllContactList();
                                  } else if (page_no > 1) {
                                      loadMoreArrayList = mModel.getData().getAllContactList();
                                  }
                                  if (loadMoreArrayList.size() > 0) {
                                      mArrayList.addAll(loadMoreArrayList);
                                  }

                                  /*SetAdapter*/
                                  if (page_no == 1) {
                                      setAdapter();
                                  } else {
                                      mAdapter.notifyDataSetChanged();
                                  }
                              } else {
                                  AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                              }
                          }

                          @Override
                          public void onFailure(Call<AddressBookListModel> call, Throwable t) {
                              AlertDialogManager.hideProgressDialog();
                              Log.e(TAG, "******error*****" + t.getMessage());
                          }
                      }
        );
    }

    void setAdapter() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        addressBookRV.setLayoutManager(layoutManager);
        mAdapter = new AddressBookAdapter(getActivity(), mArrayList, mDeleteAddressBook, mEditInterface, mPaginationInterFace);
        addressBookRV.setAdapter(mAdapter);
    }

    private void deleteConfirmDialog(final String strContactID) {
        final Dialog deleteConfirmDialog = new Dialog(getActivity());
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_address));
        TextView txtConfirm = deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(strContactID);
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }

    private void executeDeleteAPI(String strContactID) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteAddressBookContactRequest(strContactID);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                    page_no = 1;
                    mArrayList.clear();
                    loadMoreArrayList.clear();
                    AlertDialogManager.showProgressDialog(getActivity());
                    executeAPI(page_no);
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}
