package jaohar.com.jaohar.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.utils.JaoharConstants;

/**
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class ContactUsFragment extends Fragment {
    String TAG = ContactUsFragment.this.getClass().getSimpleName();
    View rootView;
    String strIsBack = "";

    public ContactUsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            strIsBack = getArguments().getString(JaoharConstants.STR_BACK);
        }

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.toolBarMainLL.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.CENTER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //set status bar
        getActivity().getWindow().setStatusBarColor(Color.WHITE);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_contact_us, container, false);
        if (strIsBack.equals(JaoharConstants.STR_BACK)) {
            JaoharConstants.IS_BACK = true;
        }


        return rootView;
    }


}
