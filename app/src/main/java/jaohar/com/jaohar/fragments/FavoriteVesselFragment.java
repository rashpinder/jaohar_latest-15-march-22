package jaohar.com.jaohar.fragments;

import static android.view.View.GONE;
import static jaohar.com.jaohar.HomeActivity.switchFragment;
import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.DetailsActivity;
import jaohar.com.jaohar.activities.EditVesselActivity;
import jaohar.com.jaohar.activities.SearchVesselsActivity;
import jaohar.com.jaohar.activities.SendingMailActivity;
import jaohar.com.jaohar.activities.mail_list_all_activities.SendingMailToLIst_Vessels;
import jaohar.com.jaohar.activities.vessels_module.CopyVesselActivity;
import jaohar.com.jaohar.adapters.FavoriteVesselAdapter;
import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.interfaces.FavoriteVesselInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.models.GetFavoriteVesselsModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteVesselFragment extends Fragment {
    String TAG = FavoriteVesselFragment.this.getClass().getSimpleName();

    /*unbinder*/
    Unbinder unbinder;

    /*
     * Widgets
     * */
    @BindView(R.id.favVesselsLayoutBar)
    RelativeLayout favVesselsLayoutBar;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.imgRightRL)
    RelativeLayout imgRightRL;
    @BindView(R.id.searchImg)
    ImageView searchImg;

    private RecyclerView vesselsRV;
    private SwipeRefreshLayout swipeToRefresh;
    private FavoriteVesselAdapter mVesselesAdapter;
    private ArrayList<AllVessel> mArrayList = new ArrayList<>();
    private final ArrayList<VesselesModel> filteredList = new ArrayList<VesselesModel>();
    private ArrayList<AllVessel> loadMoreArrayList = new ArrayList<AllVessel>();
    private final boolean isSwipeRefresh = false;
    private final String strIsBack = "";
    private String UserId = "";
    private int page_no = 1;
    private ProgressBar progressBottomPB;
    private String strLastPage = "FALSE";

    paginationforVesselsInterface mPagination = new paginationforVesselsInterface() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                progressBottomPB.setVisibility(View.VISIBLE);

                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            executeAPI(page_no, UserId);
                        } else {
                            progressBottomPB.setVisibility(View.GONE);
                        }
                    }
                }, 1000);
            }
        }
    };

    FavoriteVesselInterface mFavoriteVesselInterface = new FavoriteVesselInterface() {
        @Override
        public void mFavoriteVesselInterface(int position, String status, ImageView imageView) {
            String vessel_id = mArrayList.get(position).getRecordId();
            executeUnFavoriteAPI(vessel_id, position, UserId);
        }
    };

    public FavoriteVesselFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //set status bar
        getActivity().getWindow().setStatusBarColor(Color.WHITE);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite_vessel, container, false);

        /* butterknife */
        unbinder = ButterKnife.bind(this, view);

        JaoharSingleton.getInstance().setSearchedVessel("");

        setWidgetIDs(view);
        setClickListner();

        /* check redirection of notification screen */
        if (JaoharConstants.is_Staff_FragmentClick) {

            favVesselsLayoutBar.setVisibility(View.VISIBLE);
            toolBarMainLL.setVisibility(GONE);

        } else {
            favVesselsLayoutBar.setVisibility(GONE);
            toolBarMainLL.setVisibility(View.VISIBLE);

            HomeActivity.bottomMainLL.setVisibility(View.GONE);
            HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
            HomeActivity.txtCenter.setGravity(Gravity.START);
            HomeActivity.txtCenter.setText(getResources().getString(R.string.favorite_vessels));

            HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
            HomeActivity.imgRight.setVisibility(View.VISIBLE);
            HomeActivity.imgRight.setImageResource(R.drawable.ic_magnifying_glass);
        }

        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                UserId = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
            } else {
                UserId = JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, "");
            }
        } else {
            UserId = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
        }

        return view;
    }

    private void setWidgetIDs(View v) {
        progressBottomPB = v.findViewById(R.id.progressBottomPB);
        vesselsRV = v.findViewById(R.id.vesselsRV);
        swipeToRefresh = (SwipeRefreshLayout) v.findViewById(R.id.swipeToRefresh);
    }

    private void setClickListner() {
        HomeActivity.imgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.is_VesselsFavorite = true;
                if (getActivity() != null) {
                    startActivity(new Intent(getActivity(), SearchVesselsActivity.class)
                            .putExtra(JaoharConstants.SearchType, JaoharConstants.FavoriteVessels));
                }
            }
        });

        imgRightRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.is_VesselsFavorite = true;
                if (getActivity() != null) {
                    startActivity(new Intent(getActivity(), SearchVesselsActivity.class)
                            .putExtra(JaoharConstants.SearchType, JaoharConstants.FavoriteVessels));
                }
            }
        });
    }

    private void setAdapter() {
        vesselsRV.setNestedScrollingEnabled(false);
        vesselsRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mVesselesAdapter = new FavoriteVesselAdapter(getActivity(), mArrayList, mPagination, new OnClickInterface() {
            @Override
            public void mOnClickInterface(int position) {
                PerformOptionsClick(position);
            }
        }, mFavoriteVesselInterface);
        vesselsRV.setAdapter(mVesselesAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mArrayList != null)
            mArrayList.clear();

        if (loadMoreArrayList != null)
            loadMoreArrayList.clear();

        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            page_no = 1;
            executeAPI(page_no, UserId);
        }
    }

    public void executeAPI(int page_no, String strUserId) {
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
            AlertDialogManager.hideProgressDialog();
        }
        if (page_no == 1) {
            progressBottomPB.setVisibility(View.GONE);
            AlertDialogManager.showProgressDialog(getActivity());
        }
        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetFavoriteVesselsModel> call1 = mApiInterface.getAllFavoriteVesselsRequest(String.valueOf(page_no), strUserId);
        call1.enqueue(new Callback<GetFavoriteVesselsModel>() {
            @Override
            public void onResponse(Call<GetFavoriteVesselsModel> call, retrofit2.Response<GetFavoriteVesselsModel> response) {
                Log.e(TAG, "******Response*****" + response);
                AlertDialogManager.hideProgressDialog();
                progressBottomPB.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
                GetFavoriteVesselsModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    strLastPage = mModel.getData().getLastPage();
                    if (page_no == 1) {
                        mArrayList = mModel.getData().getAllVessels();
                    } else if (page_no > 1) {
                        loadMoreArrayList = mModel.getData().getAllVessels();
                    }

                    if (loadMoreArrayList.size() > 0) {
                        mArrayList.addAll(loadMoreArrayList);
                    }

                    if (page_no == 1) {
                        /*SetAdapter*/
                        setAdapter();
                    } else {
                        mVesselesAdapter.notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(getActivity(), mModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetFavoriteVesselsModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void PerformOptionsClick(final int position) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_vessels_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout shareRL = view.findViewById(R.id.shareRL);
        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout copyRL = view.findViewById(R.id.copyRL);
        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout mailToListRL = view.findViewById(R.id.mailToListRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);
        View shareView = view.findViewById(R.id.shareView);
        View editView = view.findViewById(R.id.editView);
        View copyView = view.findViewById(R.id.copyView);
        View mailToListView = view.findViewById(R.id.mailToListView);
        ImageView mailImg = view.findViewById(R.id.mailImg);

        shareRL.setVisibility(View.GONE);
        editRL.setVisibility(View.GONE);
        copyRL.setVisibility(View.GONE);
        mailToListRL.setVisibility(View.GONE);
        shareView.setVisibility(View.GONE);
        editView.setVisibility(View.GONE);
        copyView.setVisibility(View.GONE);
        mailToListView.setVisibility(View.GONE);

        mailImg.setImageResource(R.drawable.ic_email_);

        shareRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareViaWhatsApp();
            }
        });

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), EditVesselActivity.class);
                mIntent.putExtra("Model", mArrayList);
                mIntent.putExtra("Title", "Edit Vessels");
                startActivity(mIntent);
            }
        });

        copyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), CopyVesselActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("Title", "Edit Vessels");
                startActivity(mIntent);
            }
        });

        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.Is_click_form_All_Vessals = true;
                Intent mIntent = new Intent(getActivity(), DetailsActivity.class);
                mIntent.putExtra("Model", mArrayList.get(position));
                mIntent.putExtra("isEditShow", true);
                startActivity(mIntent);
            }
        });

        mailToListRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JaoharConstants.IsAllVesselsClick = true;
                Intent mIntent = new Intent(getActivity(), SendingMailToLIst_Vessels.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), SendingMailActivity.class);
                mIntent.putExtra("vessalName", "ID " + mArrayList.get(position).getRecordId() + "  " + "-" + "  " + mArrayList.get(position).getVesselName() + ", IMO No. " + mArrayList.get(position).getIMONumber());
                mIntent.putExtra("recordID", mArrayList.get(position).getRecordId());
                startActivity(mIntent);
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void shareViaWhatsApp() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.whatsapp");
            shareIntent.putExtra("Intent.EXTRA_SUBJECT", "Jaohar");
            String shareMessage = "\nSee this Video\n";
            shareMessage = "https://play.google.com/store/apps/details?id=jaohar.com.jaohar&hl=en_IN&gl=US";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            Log.e(TAG, "****Exception****" + e.toString());
        }
    }

    /* *
     * Execute UnFavorite API for vessel
     * @param
     * @user_id
     * */
    public void executeUnFavoriteAPI(String vessel_id, final int position, String strUserId) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.unFavoriteVesselsRequest(vessel_id, strUserId);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    mArrayList.remove(position);
                    mVesselesAdapter.notifyDataSetChanged();
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.the_vessel_has_been_unfav));
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    @OnClick({R.id.llLeftLL})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.llLeftLL:

                favVesselsLayoutBar.setVisibility(GONE);
                toolBarMainLL.setVisibility(View.VISIBLE);

                JaoharConstants.is_Staff_FragmentClick = false;
                HomeActivity.staffToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                HomeActivity.mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment(getActivity(), new StaffFragment(), JaoharConstants.STAFF_FRAGMENT, false, null);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}
