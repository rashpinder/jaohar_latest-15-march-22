package jaohar.com.jaohar.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.forum_module.AddForumActivity;
import jaohar.com.jaohar.activities.forum_module_admin.AdminForumActivity;
import jaohar.com.jaohar.activities.forum_module_admin.ChatScreenUkOfficeAdminActivity;
import jaohar.com.jaohar.activities.forum_module_admin.ClosedForumUkOfficceActivity;
import jaohar.com.jaohar.adapters.forum_module_admin_adapter.Forum_list_admin_Adapter;
import jaohar.com.jaohar.beans.ForumModule.ForumModel;
import jaohar.com.jaohar.interfaces.forumModule.ForumItemClickInterace;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.interfaces.forum_module_admin.CloseAndDeleteForumInterface;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;

import static android.view.View.GONE;

public class UKforumAdminFragment extends Fragment {
    String TAG = UKforumAdminFragment.this.getClass().getSimpleName();
    View rootView;

    /**
     * Widgets
     */
    ProgressBar progressBottomPB;
    RecyclerView dataRV;
    SwipeRefreshLayout swipeToRefresh;

    LinearLayout llLeftLL;
    Button btnclosed;


    int page_no = 1;
    String strLastPage = "TRUE";
    String strForumId = "";
    boolean isSwipeRefresh = false;
    /*
     * Setting Up Array List
     * */
    ArrayList<ForumModel> mLoadMore = new ArrayList<>();
    ArrayList<ForumModel> modelArrayList = new ArrayList<>();

    /* *
     * Setting Up Adapter
     * */
    Forum_list_admin_Adapter mAdapter;


    /* *
     * Setting Up Interface For selecting Items
     * */
    ForumItemClickInterace mInterfaceForum = new ForumItemClickInterace() {
        @Override
        public void ForumItemClick(ForumModel mModel) {
            AdminForumActivity.isUKOfficeAddClick=true;
//            Toast.makeText(getActivity(), "Coming Soon...", Toast.LENGTH_SHORT).show();
            Intent mIntent = new Intent(getActivity(), ChatScreenUkOfficeAdminActivity.class);
            mIntent.putExtra("forum_id", mModel.getId());
            mIntent.putExtra("type", "staffAdmin");
            getActivity().startActivity(mIntent);
        }
    };

    /* *
     * Setting Up Interface For Deleting and Close the Forum Items
     * */
    CloseAndDeleteForumInterface mCloseAndDelForum = new CloseAndDeleteForumInterface() {
        @Override
        public void mCloseAndDeleteInterFace(ForumModel mModel, String strType) {
            strForumId =mModel.getId();
            if (strType.equals("close")) {
                deleteConfirmDialog("Are you sure want to close this forum?", "close");
            } else {
                deleteConfirmDialog("Are you sure want to delete this forum?", "delete");
            }
        }
    };


    /**
     * Recycler View Pagination Adapter Interface
     **/
    PaginationListForumAdapter mPaginationInterFace = new PaginationListForumAdapter() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            progressBottomPB.setVisibility(View.VISIBLE);
                            ++page_no;
                            mLoadMore.clear();
                            executeAPI();
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                        }
                    }
                }, 1500);
            }
        }
    };


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_staff_forum, container, false);
        /*
         * Declare Widgets IDS
         * */
        setWidgetIDs(rootView);
        /*
         * Declare ClickListeners
         * */
        setClickListner();

        return rootView;
    }

    private void setWidgetIDs(View rootView) {
        progressBottomPB = rootView.findViewById(R.id.progressBottomPB);
        dataRV = rootView.findViewById(R.id.dataRV);

        llLeftLL = rootView.findViewById(R.id.llLeftLL);
        btnclosed = rootView.findViewById(R.id.btnclosed);

        swipeToRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                modelArrayList.clear();
                mLoadMore.clear();
                page_no = 1;
                executeAPI();
            }
        });
        setAdapter();
    }

    private void setClickListner() {
        AdminForumActivity.imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdminForumActivity.isUKOfficeAddClick=true;
                Intent mIntent = new Intent(getActivity(), AddForumActivity.class);
                mIntent.putExtra("type","UKadmin");
                startActivity(mIntent);
            }
        });

        btnclosed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdminForumActivity.isUKOfficeAddClick=true;
                Intent mIntent = new Intent(getActivity(), ClosedForumUkOfficceActivity.class);
                startActivity(mIntent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute API For Getting List *//*
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(GONE);
            }
            modelArrayList.clear();
            mLoadMore.clear();
            page_no = 1;
            isSwipeRefresh = false;
            swipeToRefresh.setRefreshing(false);
            executeAPI();
        }
    }

    /* *
     * Execute API for getting Forum list
     * @param
     * @user_id
     * */
    public void executeAPI() {
        if (strLastPage.equals("FALSE")) {
            if (isSwipeRefresh == true) {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }else {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(View.VISIBLE);
                }
            }

        } else {
            if (page_no == 1) {
                if (isSwipeRefresh == true) {
                    if (progressBottomPB != null) {
                        progressBottomPB.setVisibility(GONE);
                    }
                }else {
                    if(AdminForumActivity.isUKOfficeAddClick==true){
                        AdminForumActivity.isUKOfficeAddClick=false;
                    }else {
                        AlertDialogManager.showProgressDialog(getActivity());
                        isSwipeRefresh = false;
                        swipeToRefresh.setRefreshing(false);
                        if (progressBottomPB != null) {
                            progressBottomPB.setVisibility(GONE);
                        }
                    }

                }
            }
        }
        String strUrl = JaoharConstants.GetAllForums + "?user_id=" + JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, "") + "&page_no=" + page_no;
        Log.e(TAG, "***URL***" + strUrl);
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                if (isSwipeRefresh == true) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }
                parseResponce(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    void parseResponce(String responce) {

        try {
            JSONObject mJSonObject = new JSONObject(responce);
            String strStatus = mJSonObject.getString("status");
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");
                strLastPage = mjsonDATA.getString("last_page");
                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("all_forums");
                if (mjsonArrayData != null) {
                    ArrayList<ForumModel> mTempArray = new ArrayList<>();
                    mTempArray.clear();
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        ForumModel mModel = new ForumModel();
                        if (!mJsonDATA.getString("id").equals("")) {
                            mModel.setId(mJsonDATA.getString("id"));
                        }
                        if (!mJsonDATA.getString("vessel_id").equals("")) {
                            mModel.setVessel_id(mJsonDATA.getString("vessel_id"));
                        }
                        if (!mJsonDATA.getString("user_id").equals("")) {
                            mModel.setUser_id(mJsonDATA.getString("user_id"));
                        }
                        if (!mJsonDATA.getString("creation_date").equals("")) {
                            mModel.setCreation_date(mJsonDATA.getString("creation_date"));
                        }
                        if (!mJsonDATA.getString("disable").equals("")) {
                            mModel.setDisable(mJsonDATA.getString("disable"));
                        }
                        if (!mJsonDATA.getString("vessel_record_id").equals("")) {
                            mModel.setVessel_record_id(mJsonDATA.getString("vessel_record_id"));
                        }
                        if (!mJsonDATA.getString("vessel_name").equals("")) {
                            mModel.setVessel_name(mJsonDATA.getString("vessel_name"));
                        }
                        if (!mJsonDATA.getString("IMO_number").equals("")) {
                            mModel.setIMO_number(mJsonDATA.getString("IMO_number"));
                        }
                        if (!mJsonDATA.getString("status").equals("")) {
                            mModel.setStatus(mJsonDATA.getString("status"));
                        }
                        if (!mJsonDATA.getString("pic").equals("")) {
                            mModel.setPic(mJsonDATA.getString("pic"));
                        }
                        if (!mJsonDATA.getString("total_messages").equals("")) {
                            mModel.setTotal_messages(mJsonDATA.getString("total_messages"));
                        }
                        if (!mJsonDATA.getString("unread_messages").equals("")) {
                            mModel.setUnread_messages(mJsonDATA.getString("unread_messages"));
                        }
                        mTempArray.add(mModel);
//                        if (page_no == 1) {
//                            modelArrayList.add(mModel);
//                        } else if (page_no > 1) {
//                            mLoadMore.add(mModel);
//                        }
                    }


                        modelArrayList.addAll(mTempArray);


                    mAdapter.notifyDataSetChanged();

                }

            } else {
                AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Setting Up Adapter
     **/
    private void setAdapter() {
        dataRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new Forum_list_admin_Adapter(getActivity(), modelArrayList, mPaginationInterFace, mInterfaceForum, mCloseAndDelForum);
        mAdapter.setHasStableIds(true);
        dataRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }


    public void deleteConfirmDialog(String strMessage, final String strType) {
        final Dialog deleteConfirmDialog = new Dialog(getActivity());
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                if (strType.equals("close")) {
                    /*Execute EMPTY API*/
                    if (Utilities.isNetworkAvailable(getActivity()) == false) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeCloseForum();
                    }
                } else {
                    /*Execute Delete API*/
                    if (Utilities.isNetworkAvailable(getActivity()) == false) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeDeleteForum();
                    }
                }
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    /* *
     * Execute API for Close Forum list
     * @param
     * @user_id
     * @forum_id
     * */
    private void executeCloseForum() {
//        https://root.jaohar.com/Staging/JaoharWebServicesNew/CloseStaffForum.php
//        forum_id , user_id
        AlertDialogManager.showProgressDialog(getActivity());
        String strUrl = JaoharConstants.CloseForum + "?user_id=" + JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, "") + "&forum_id=" + strForumId;
        Log.e(TAG, "***URL***" + strUrl);
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);

                try {
                    JSONObject mjsonData = new JSONObject(response);
                    if(mjsonData.getString("status").equals("1")){
                        if (progressBottomPB != null) {
                            progressBottomPB.setVisibility(GONE);
                        }
                        modelArrayList.clear();
                        mLoadMore.clear();
                        page_no = 1;
                        isSwipeRefresh = false;
                        swipeToRefresh.setRefreshing(false);
                        executeAPI();
                    }else {
                        AlertDialogManager.showAlertDialog(getActivity(),getResources().getString(R.string.app_name),mjsonData.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    /* *
     * Execute API for Delete Forum list
     * @param
     * @user_id
     * */
    private void executeDeleteForum() {
//      https://root.jaohar.com/Staging/JaoharWebServicesNew/DeleteStaffForum.php
//      user_id
        AlertDialogManager.showProgressDialog(getActivity());
        String strUrl = JaoharConstants.DeleteForum + "?forum_id=" + strForumId;
        Log.e(TAG, "***URL***" + strUrl);
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                try {
                    JSONObject mjsonData = new JSONObject(response);
                    if(mjsonData.getString("status").equals("1")){
                        if (progressBottomPB != null) {
                            progressBottomPB.setVisibility(GONE);
                        }
                        modelArrayList.clear();
                        mLoadMore.clear();
                        page_no = 1;
                        isSwipeRefresh = false;
                        swipeToRefresh.setRefreshing(false);
                        executeAPI();
                    }else {
                        AlertDialogManager.showAlertDialog(getActivity(),getResources().getString(R.string.app_name),mjsonData.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}
