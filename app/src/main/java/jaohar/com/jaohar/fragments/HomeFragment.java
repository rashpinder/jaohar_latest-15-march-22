package jaohar.com.jaohar.fragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

//import com.bumptech.glide.request.animation.GlideAnimation;

import java.util.Timer;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.LRViewPagerAdapter;
//import me.relex.circleindicator.CircleIndicator;

/**
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    final long DELAY_MS = 3000;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 5000; // time in milliseconds between successive task executions.
    String TAG = HomeFragment.this.getClass().getSimpleName();
    View rootView;
    ViewPager viewPager;
//    me.relex.circleindicator.CircleIndicator indicator;
    int currentPage = 0;
    Timer timer;
    int layoutArray[] = {R.layout.slide1, R.layout.slide2, R.layout.slide3};
    ImageView imgAbout, imgMarineServices, imgShipSale, imgContact;
    private LRViewPagerAdapter mPagerAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //set status bar
        Window window = getActivity().getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.parseColor("#7f8175"));
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        setWidgetIDs(rootView);
        HomeActivity.bottomMainLL.setVisibility(View.VISIBLE);

//        setClickListner();
        return rootView;
    }

    private void setWidgetIDs(View v) {
//        viewPager = (ViewPager) v.findViewById(R.id.viewPager);
////        indicator = (CircleIndicator) v.findViewById(R.id.indicator);
//
//        /*SetUp ViewPager with Indicator*/
//        setUpViewPagerWithIndicator();
//
//        imgAbout = (ImageView) v.findViewById(R.id.imgAbout);
//        imgMarineServices = (ImageView) v.findViewById(R.id.imgMarineServices);
//        imgShipSale = (ImageView) v.findViewById(R.id.imgShipSale);
//        imgContact = (ImageView) v.findViewById(R.id.imgContact);
    }

//    private void setUpViewPagerWithIndicator() {
//        mPagerAdapter = new LRViewPagerAdapter(getActivity(), layoutArray);
//        viewPager.setAdapter(mPagerAdapter);
////        indicator.setViewPager(viewPager);
//        /*After setting the adapter use the timer */
//        final Handler handler = new Handler();
//        final Runnable Update = new Runnable() {
//            public void run() {
////                if (currentPage == 4 - 1) {
////                    currentPage = 0;
////                }
//                viewPager.setCurrentItem(currentPage++, true);
//            }
//        };
//
//        timer = new Timer(); // This will create a new Thread
//        timer.schedule(new TimerTask() { // task to be scheduled
//
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, 3000,5000);
//    }

//    private void setClickListner() {
//        imgAbout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                HomeActivity.txtCenter.setText(getString(R.string.about_us));
//                Fragment mFragment = new AboutFragment();
//                Bundle mBundle = new Bundle();
//                mBundle.putString(JaoharConstants.STR_BACK, JaoharConstants.STR_BACK);
//                HomeActivity.aboutToolbarWithBack();
//                HomeActivity.switchFragment((FragmentActivity) getActivity(), mFragment, JaoharConstants.ABOUT_FRAGMENT, true,mBundle);
//            }
//        });
//        imgMarineServices.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                HomeActivity.txtCenter.setText(getString(R.string.services));
//                Fragment mFragment = new ServicesFragment();
//                Bundle mBundle = new Bundle();
//                mBundle.putString(JaoharConstants.STR_BACK, JaoharConstants.STR_BACK);
//                HomeActivity.servicesToolbarWithBack();
//                HomeActivity.switchFragment((FragmentActivity) getActivity(), mFragment, JaoharConstants.SERVICES_FRAGMENT, true,mBundle);
//            }
//        });
//        imgShipSale.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
//                    startActivity(new Intent(getActivity(), VesselForSaleActivity.class));
//                    JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, "");
//                } else {
//                    Intent mIntent = new Intent(getActivity(), LoginActivity.class);
//                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, "VesselForSaleActivity");
//                    getActivity().startActivity(mIntent);
//                }
//            }
//        });
//        imgContact.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                HomeActivity.txtCenter.setText(getString(R.string.contact_us));
//                Fragment mFragment = new ContactUsFragment();
//                Bundle mBundle = new Bundle();
//                mBundle.putString(JaoharConstants.STR_BACK, JaoharConstants.STR_BACK);
//                HomeActivity.contactUsToolbarWithBack();
//                HomeActivity.switchFragment((FragmentActivity) getActivity(), mFragment, JaoharConstants.CONTACTUS_FRAGMENT, true,mBundle);
//            }
//        });
//    }
//
//    private void gettingProfileDATA(String strLoginID){
//        String strUrl = JaoharConstants.GetUserProfile_API + "?user_id=" + strLoginID;
//        AlertDialogManager.showProgressDialog(getActivity());
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    if(strStatus.equals("1")){
//                        if(!mJsonObject.isNull("data")){
//                            JSONObject mJsonDATA = mJsonObject.getJSONObject("data");
//                            if(!mJsonDATA.getString("image").equals("")){
//                                String strImage = mJsonDATA.getString("image");
////                                Glide.with(getActivity())
////                                        .load(strImage)
////                                        .asBitmap()
////                                        .placeholder(R.drawable.palace_holder)
////                                        .into(new SimpleTarget<Bitmap>() {
////                                            @Override
////                                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
////                                                // you can do something with loaded bitmap here
////                                                HomeActivity.imgRight.setImageBitmap(resource);
////                                            }
////                                        });
//                                Glide.with(getActivity()).load(strImage).into(HomeActivity.imgRight);
//
//                            }
//                        }
//
//
//                    }
//
//                }
//                catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
//

}
