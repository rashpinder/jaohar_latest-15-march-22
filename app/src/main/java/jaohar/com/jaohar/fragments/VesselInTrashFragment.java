package jaohar.com.jaohar.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverDeleteSelectedTrashInterface;
import jaohar.com.jaohar.interfaces.TrashModuleInterface.RecoverORDeleteTrashInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.trash_module_adapter.VesselTrashAdapter;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.models.GetAllVesselTrashModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class VesselInTrashFragment extends Fragment {
    String TAG = VesselInTrashFragment.this.getClass().getSimpleName();
    View rootView;
    SwipeRefreshLayout swipeToRefresh;
    RecyclerView dataRV;
    private int page_no = 1;
    private String strLastPage = "FALSE";
    private VesselTrashAdapter mVesselsAdapter;
    private ImageView emtyTrashIV, recoverIV, deleteIV;
//    ArrayList<VesselesModel> loadMoreArrayList = new ArrayList<>();
//    ArrayList<VesselesModel> mArrayList = new ArrayList<>();
    ArrayList<AllVessel> loadMoreArrayList = new ArrayList<>();
    ArrayList<AllVessel> mArrayList = new ArrayList<>();
    String strVesselID = "";
    private boolean isSwipeRefresh = false;
    ProgressBar progressBottomPB;
    ArrayList<String> mRecordID = new ArrayList<>();
//    private ArrayList<VesselesModel> multiSelectArrayList = new ArrayList<VesselesModel>();
    private ArrayList<AllVessel> multiSelectArrayList = new ArrayList<AllVessel>();
    /*
     * To Recover and Delete Interface
     * */
    RecoverORDeleteTrashInterface mRecoverDeleteInterface = new RecoverORDeleteTrashInterface() {
        @Override
        public void mRecoverAndDeleteTrash(AllVessel mModel, String strType) {
            strVesselID = mModel.getRecordId();
            if (strType.equals("recover")) {
                deleteConfirmDialog(getString(R.string.recover_trash), "recover");
            } else {
                deleteConfirmDialog(getString(R.string.are_you_sure_want_to_delete), "delete");
            }
        }
    };


    /**
     * To get Selected and unSelected Vessel Interface
     */

    RecoverDeleteSelectedTrashInterface mRecoverDELETEInterface = new RecoverDeleteSelectedTrashInterface() {
        @Override
        public void mRecoverDeleteSelectedTrashInterface(AllVessel mVesselesModel, boolean b) {
            if (mVesselesModel != null) {
                if (!b) {

                    multiSelectArrayList.add(mVesselesModel);
                    String strID = mVesselesModel.getRecordId();
                    mRecordID.add(mVesselesModel.getRecordId());
                } else {

                    multiSelectArrayList.remove(mVesselesModel);
                    String strID = mVesselesModel.getRecordId();
                    mRecordID.remove(mVesselesModel.getRecordId());
                }

            }
        }
    };


    /*
     * Implement Pagination On Recycler View
     * */
    paginationforVesselsInterface mPagination = new paginationforVesselsInterface() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                progressBottomPB.setVisibility(View.VISIBLE);
                ++page_no;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            executeAPI(page_no);
                        } else {
                            progressBottomPB.setVisibility(View.GONE);
                        }
                    }
                }, 1000);

            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_vessel_in_trash, container, false);
        /*
         * Declare Widgets IDS
         * */
        setWidgetIDs(rootView);
        /*
         * Declare ClickListeners
         * */
        setClickListner();

        return rootView;
    }

    private void setWidgetIDs(View rootView) {
        dataRV = rootView.findViewById(R.id.dataRV);
        emtyTrashIV = rootView.findViewById(R.id.emtyTrashIV);
        recoverIV = rootView.findViewById(R.id.recoverIV);
        deleteIV = rootView.findViewById(R.id.deleteIV);
        progressBottomPB = rootView.findViewById(R.id.progressBottomPB);
        swipeToRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                mArrayList.clear();
                loadMoreArrayList.clear();
                page_no = 1;
                executeAPI(page_no);
            }
        });
//        swipeToRefresh=rootView.findViewById(R.id.swipeToRefresh);
    }

    private void setClickListner() {
        emtyTrashIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mArrayList.size() > 0) {
                    deleteConfirmDialog(getString(R.string.clear_trash), "empty");
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.no_vessel_found));

                }
            }
        });

        recoverIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mArrayList.size() > 0) {

                    if (Utilities.isNetworkAvailable(getActivity()) == false) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {

                        if(multiSelectArrayList.size()>0){
                            executeRecoverSelectedTrash();
                        }else {
                            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.there_is_no_selected_item));

                        }
                    }
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.no_vessel_found));

                }
            }
        });

        deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mArrayList.size() > 0) {
                    if (Utilities.isNetworkAvailable(getActivity()) == false) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        if(multiSelectArrayList.size()>0){
                            executeDeleteSelectedTrash();
                        }else {
                            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.there_is_no_selected_item));

                        }

                    }
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.no_vessel_found));

                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            if (strLastPage.equals("FALSE")) {
                page_no = 1;
                mArrayList.clear();
                loadMoreArrayList.clear();
                executeAPI(page_no);
            }

        }
    }


    /**
     * Implement API for Clear All Trash
     */
//    public void executeEmptyTrash() {
////        api : https://root.jaohar.com/Staging/JaoharWebServicesNew/EmptyVesselTrash.php
//        String strUrl = JaoharConstants.Empty_Vessel_Trash + "?user_id=" + JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), strMessage);
//                        mArrayList.clear();
//                        loadMoreArrayList.clear();
//                        executeAPI(1);
//                        mVesselsAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void executeEmptyTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.emptyVesselTrash(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, "")).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), mModel.getMessage());
                    mArrayList.clear();
                    loadMoreArrayList.clear();
                    executeAPI(1);
                    mVesselsAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), t.getMessage());
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeEmptyTrash() {
////        api : https://root.jaohar.com/Staging/JaoharWebServicesNew/EmptyVesselTrash.php
//        String strUrl = JaoharConstants.Empty_Vessel_Trash + "?user_id=" + JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), strMessage);
//                        mArrayList.clear();
//                        loadMoreArrayList.clear();
//                        executeAPI(1);
//                        mVesselsAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /*
     * Arrange Selected Array list and Get ID from that
     * */
    private ArrayList<String> getMultiVesselDetailsData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();
        Log.e(TAG, "ArrayLIST_DATA1============= " + mRecordID.size());
        for (int i = 0; i < mRecordID.size(); i++) {
            strData.add(mRecordID.get(i));
        }

        return strData;
    }

    /**
     * Implement API for Recover Vessel
     */

    private void executeRecoverTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.recoverSingleVesselTrash(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""),strVesselID).
                enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mArrayList.clear();
                    loadMoreArrayList.clear();
                    mRecordID.clear();
                    executeAPI(1);
                    mVesselsAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), t.getMessage());
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


//    public void executeRecoverTrash() {
////        api : https://root.jaohar.com/Staging/JaoharWebServicesNew/RecoverSingleVesselTrash.php
//        String strUrl = JaoharConstants.Recover_Single_Vessel_Trash + "?user_id=" + JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, "") + "&vessel_id=" + strVesselID;// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mArrayList.clear();
//                        loadMoreArrayList.clear();
//                        mRecordID.clear();
//                        executeAPI(1);
//                        mVesselsAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Implement API for Recover Selected Vessel
     */

    private Map<String, String> mParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
        params.put("vessel_ids", String.valueOf(getMultiVesselDetailsData()));
        Log.e("**PARAMS**",params.toString());
        return params;
    }

    public void executeRecoverSelectedTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.recoverSelectedVesselTrash(mParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    mArrayList.clear();
                    loadMoreArrayList.clear();
                    mRecordID.clear();
                    executeAPI(1);
                    mVesselsAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" +t.getMessage());

            }
        });
    }


//    public void executeRecoverSelectedTrash() {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.RecoverSelectedVesselTrash;
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("vessel_ids", getMultiVesselDetailsData());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.e("test", "******response*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String strStatus = jsonObject.getString("status");
//                    String strMessage = jsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mArrayList.clear();
//                        loadMoreArrayList.clear();
//                        mRecordID.clear();
//                        executeAPI(1);
//                        mVesselsAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("test", "******response*****" + error.toString());
//            }
//        }) {
//            /* *
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /**
     * Implement API for Delete Selected Vessel
     */
    private Map<String, String> mDeleteParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
        params.put("vessel_ids", String.valueOf(getMultiVesselDetailsData()));
        Log.e("**PARAMS**",params.toString());
        return params;
    }

    public void executeDeleteSelectedTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteSelectedVesselTrash(mDeleteParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    mArrayList.clear();
                    loadMoreArrayList.clear();
                    executeAPI(1);
                    mVesselsAdapter.notifyDataSetChanged();
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" +t.getMessage());

            }
        });
    }


//    public void executeDeleteSelectedTrash() {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.DeleteSelectedVesselTrash;
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("vessel_ids", getMultiVesselDetailsData());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.e("test", "******response*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String strStatus = jsonObject.getString("status");
//                    String strMessage = jsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mArrayList.clear();
//                        loadMoreArrayList.clear();
//                        executeAPI(1);
//                        mVesselsAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), strMessage);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("test", "******response*****" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

/**
     * Implement API for Delete Vessel
     */
    private void executeDeleteTrash() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteSingleVesselTrash(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""), strVesselID).
                enqueue(new Callback<StatusMsgModel>() {
                    @Override
                    public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                        AlertDialogManager.hideProgressDialog();
                        Log.e(TAG, "**RESPONSE**" + response);
                        StatusMsgModel mModel = response.body();
                        if (mModel.getStatus() == 1) {
                            mArrayList.clear();
                            loadMoreArrayList.clear();
                            executeAPI(1);
                            mVesselsAdapter.notifyDataSetChanged();
                        } else {
                            AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), mModel.getMessage());     }
                    }

                    @Override
                    public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                        AlertDialogManager.hideProgressDialog();
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), t.getMessage());
                        Log.e(TAG, "******error*****" + t.getMessage());
                    }
                });
    }



//    public void executeDeleteTrash() {
////        api : https://root.jaohar.com/Staging/JaoharWebServicesNew/RecoverSingleVesselTrash.php
//        String strUrl = JaoharConstants.Delete_Single_Vessel_Trash + "?user_id=" + JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, "") + "&vessel_id=" + strVesselID;// + "?page_no=" + page_no ;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        mArrayList.clear();
//                        loadMoreArrayList.clear();
//                        executeAPI(1);
//                        mVesselsAdapter.notifyDataSetChanged();
//                    } else {
//                        AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    /**
     * Implement API for Showing All Trash List With Pagination
     */

    public void executeAPI(int page_no) {
        if (page_no > 1) {
            swipeToRefresh.setRefreshing(false);
            progressBottomPB.setVisibility(View.VISIBLE);
        }
        if (page_no == 1) {
            swipeToRefresh.setRefreshing(false);
            progressBottomPB.setVisibility(View.GONE);
            AlertDialogManager.showProgressDialog(getActivity());

        }
        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);
        }

//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllVesselTrashModel> call1 = mApiInterface.getAllVesselsTrash(String.valueOf(page_no), JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<GetAllVesselTrashModel>() {
            @Override
            public void onResponse(Call<GetAllVesselTrashModel> call, retrofit2.Response<GetAllVesselTrashModel> response) {
                Log.e(TAG, "******Response*****" + response);
                AlertDialogManager.hideProgressDialog();
                loadMoreArrayList.clear();
                progressBottomPB.setVisibility(View.GONE);
                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }

                GetAllVesselTrashModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    strLastPage = mModel.getData().getLastPage();
                    if (page_no == 1) {
                        mArrayList = mModel.getData().getAllVessels();
                    } else if (page_no > 1) {
                        loadMoreArrayList = mModel.getData().getAllVessels();
                    }

                    if (loadMoreArrayList.size() > 0) {
                        mArrayList.addAll(loadMoreArrayList);
                    } else {

                    }

                    if (page_no == 1) {
                        /*SetAdapter*/
                        setAdapter();
                    } else {
                        mVesselsAdapter.notifyDataSetChanged();
                    }}
                else {
                    Toast.makeText(getActivity(), mModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetAllVesselTrashModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }
    
    
    
    
    
    
//    public void executeAPI(int page_no) {
////      IsVessalActive=0;
////      txtMailTV.setVisibility(View.GONE);
//        String strUrl = JaoharConstants.Get_All_Vessels_Trash + "?page_no=" + page_no + "&user_id=" + JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, "");// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
//        if (page_no > 1) {
//            swipeToRefresh.setRefreshing(false);
//            progressBottomPB.setVisibility(View.VISIBLE);
//        }
//        if (page_no == 1) {
//            swipeToRefresh.setRefreshing(false);
//            progressBottomPB.setVisibility(View.GONE);
//            AlertDialogManager.showProgressDialog(getActivity());
//
//        }
//        if (!isSwipeRefresh) {
//            progressBottomPB.setVisibility(View.GONE);
//        }
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    String strMessage = mJsonObject.getString("message");
//                    if (strStatus.equals("1")) {
//                        parseResponse(response);
//                    } else {
//                        AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), strMessage);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                progressBottomPB.setVisibility(View.GONE);
//                if (isSwipeRefresh) {
//                    swipeToRefresh.setRefreshing(false);
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


//    private void parseResponse(String response) {
////      mArrayList.clear();
//        loadMoreArrayList.clear();
//        try {
//            JSONObject mJsonObject1 = new JSONObject(response);
//
//
//            if (!mJsonObject1.isNull("data")) {
//                JSONObject mDataObject = mJsonObject1.getJSONObject("data");
//                JSONArray mJsonArray = mDataObject.getJSONArray("all_vessels");
//                strLastPage = mDataObject.getString("last_page");
//                for (int i = 0; i < mJsonArray.length(); i++) {
//                    JSONObject mJsonObject11 = mJsonArray.getJSONObject(i);
//                    VesselesModel mVesselesModel = new VesselesModel();
//                    if (!mJsonObject11.isNull("record_id")) {
//                        mVesselesModel.setRecord_ID(mJsonObject11.getString("record_id"));
//                    }
//                    System.out.println("Builder11==" + mJsonObject11.getString("builder"));
//                    if (!mJsonObject11.isNull("builder")) {
//                        System.out.println("Builder11==" + mJsonObject11.getString("builder"));
//                        mVesselesModel.setBuilder(mJsonObject11.getString("builder"));
//                    }
//                    if (!mJsonObject11.isNull("draught")) {
//                        mVesselesModel.setDraught(mJsonObject11.getString("draught"));
//                    }
//                    if (!mJsonObject11.isNull("deleted_by")) {
//                        mVesselesModel.setDeleted_by(mJsonObject11.getString("deleted_by"));
//                    }
//                    if (!mJsonObject11.isNull("gross_tonnage")) {
//                        mVesselesModel.setGross_tonnage(mJsonObject11.getString("gross_tonnage"));
//                    }
//                    if (!mJsonObject11.isNull("net_tonnage")) {
//                        mVesselesModel.setNet_tonnage(mJsonObject11.getString("net_tonnage"));
//                    }
//                    if (!mJsonObject11.isNull("teu")) {
//                        mVesselesModel.setTeu(mJsonObject11.getString("teu"));
//                    }
//                    if (!mJsonObject11.isNull("liquid")) {
//                        mVesselesModel.setLiquid(mJsonObject11.getString("liquid"));
//                    }
//                    if (!mJsonObject11.isNull("url")) {
//                        mVesselesModel.setUrl(mJsonObject11.getString("url"));
//                    }
//                    if (!mJsonObject11.isNull("gas")) {
//                        mVesselesModel.setGas(mJsonObject11.getString("gas"));
//                    }
//                    if (!mJsonObject11.isNull("no_passengers")) {
//                        mVesselesModel.setNo_passengers(mJsonObject11.getString("no_passengers"));
//                    }
//
//                    if (!mJsonObject11.isNull("IMO_number")) {
//                        mVesselesModel.setIMO_Number(mJsonObject11.getString("IMO_number"));
//                    }
//                    if (!mJsonObject11.isNull("short_description")) {
//                        mVesselesModel.setShort_Discription(mJsonObject11.getString("short_description"));
//                    }
//                    if (!mJsonObject11.isNull("vessel_name")) {
//                        mVesselesModel.setVessel_Name(mJsonObject11.getString("vessel_name"));
//                    }
//                    if (!mJsonObject11.isNull("vessel_type")) {
//                        mVesselesModel.setVessel_Type(mJsonObject11.getString("vessel_type"));
//                    }
//                    if (!mJsonObject11.isNull("year_built")) {
//                        mVesselesModel.setYear_Built(mJsonObject11.getString("year_built"));
//                    }
//                    if (!mJsonObject11.isNull("place_of_built")) {
//                        mVesselesModel.setPlace_of_Built(mJsonObject11.getString("place_of_built"));
//                    }
//                    if (!mJsonObject11.isNull("class")) {
//                        mVesselesModel.setmClass(mJsonObject11.getString("class"));
//                    }
//                    if (!mJsonObject11.isNull("flag")) {
//                        mVesselesModel.setFlag(mJsonObject11.getString("flag"));
//                    }
//                    if (!mJsonObject11.isNull("capacity")) {
//                        mVesselesModel.setDWT_Capacity(mJsonObject11.getString("capacity"));
//                    }
//                    if (!mJsonObject11.isNull("loa")) {
//                        mVesselesModel.setLOA_M(mJsonObject11.getString("loa"));
//                    }
//                    if (!mJsonObject11.isNull("breadth")) {
//                        mVesselesModel.setBreadth_M(mJsonObject11.getString("breadth"));
//                    }
//                    if (!mJsonObject11.isNull("depth")) {
//                        mVesselesModel.setDepth(mJsonObject11.getString("depth"));
//                    }
//                    if (!mJsonObject11.isNull("price_idea")) {
//                        mVesselesModel.setPrice_Idea(mJsonObject11.getString("price_idea"));
//                    }
//                    if (!mJsonObject11.isNull("vessel_location")) {
//                        mVesselesModel.setVessel_Location(mJsonObject11.getString("vessel_location"));
//                    }
//                    if (!mJsonObject11.isNull("full_description")) {
//                        mVesselesModel.setFull_Discription(mJsonObject11.getString("full_description"));
//                    }
//                    if (!mJsonObject11.isNull("currency")) {
//                        mVesselesModel.setCurrency(mJsonObject11.getString("currency"));
//                    }
//                    if (!mJsonObject11.isNull("photo1")) {
//                        mVesselesModel.setPhoto1(mJsonObject11.getString("photo1"));
//                    }
//                    if (!mJsonObject11.isNull("photo2")) {
//                        mVesselesModel.setPhoto2(mJsonObject11.getString("photo2"));
//                    }
//                    if (!mJsonObject11.isNull("photo3")) {
//                        mVesselesModel.setPhoto3(mJsonObject11.getString("photo3"));
//                    }
//                    if (!mJsonObject11.isNull("photo4")) {
//                        mVesselesModel.setPhoto4(mJsonObject11.getString("photo4"));
//                    }
//                    if (!mJsonObject11.isNull("photo5")) {
//                        mVesselesModel.setPhoto5(mJsonObject11.getString("photo5"));
//                    }
//                    if (!mJsonObject11.isNull("photo6")) {
//                        mVesselesModel.setPhoto6(mJsonObject11.getString("photo6"));
//                    }
//                    if (!mJsonObject11.isNull("photo7")) {
//                        mVesselesModel.setPhoto7(mJsonObject11.getString("photo7"));
//                    }
//                    if (!mJsonObject11.isNull("photo8")) {
//                        mVesselesModel.setPhoto8(mJsonObject11.getString("photo8"));
//                    }
//                    if (!mJsonObject11.isNull("photo9")) {
//                        mVesselesModel.setPhoto9(mJsonObject11.getString("photo9"));
//                    }
//                    if (!mJsonObject11.isNull("photo10")) {
//                        mVesselesModel.setPhoto10(mJsonObject11.getString("photo10"));
//                    }
//
//                    if (!mJsonObject11.isNull("date_entered")) {
//                        mVesselesModel.setDate_Entered(mJsonObject11.getString("date_entered"));
//                    }
//                    if (!mJsonObject11.isNull("status")) {
//                        mVesselesModel.setStatus(mJsonObject11.getString("status"));
//                    }
//                    if (!mJsonObject11.isNull("bale")) {
//                        mVesselesModel.setBale(mJsonObject11.getString("bale"));
//                    }
//                    if (!mJsonObject11.isNull("grain")) {
//                        mVesselesModel.setGrain(mJsonObject11.getString("grain"));
//                    }
//                    if (!mJsonObject11.isNull("date_for_vessel")) {
//                        mVesselesModel.setDate_for_Vessel(mJsonObject11.getString("date_for_vessel"));
//                    }
//                    if (!mJsonObject11.isNull("vessel_added_by")) {
//                        mVesselesModel.setVessel_added_by(mJsonObject11.getString("vessel_added_by"));
//                    }
//                    if (!mJsonObject11.isNull("document1")) {
//                        mVesselesModel.setDocument1(mJsonObject11.getString("document1"));
//                    }
//                    if (!mJsonObject11.isNull("document2")) {
//                        mVesselesModel.setDocument2(mJsonObject11.getString("document2"));
//                    }
//                    if (!mJsonObject11.isNull("document3")) {
//                        mVesselesModel.setDocument3(mJsonObject11.getString("document3"));
//                    }
//                    if (!mJsonObject11.isNull("document4")) {
//                        mVesselesModel.setDocument4(mJsonObject11.getString("document4"));
//                    }
//                    if (!mJsonObject11.isNull("document5")) {
//                        mVesselesModel.setDocument5(mJsonObject11.getString("document5"));
//                    }
//                    if (!mJsonObject11.isNull("document6")) {
//                        mVesselesModel.setDocument6(mJsonObject11.getString("document6"));
//                    }
//
//                    if (!mJsonObject11.isNull("document7")) {
//                        mVesselesModel.setDocument7(mJsonObject11.getString("document7"));
//                    }
//                    if (!mJsonObject11.isNull("document8")) {
//                        mVesselesModel.setDocument8(mJsonObject11.getString("document8"));
//                    }
//                    if (!mJsonObject11.isNull("document9")) {
//                        mVesselesModel.setDocument9(mJsonObject11.getString("document9"));
//                    }
//                    if (!mJsonObject11.isNull("document10")) {
//                        mVesselesModel.setDocument10(mJsonObject11.getString("document10"));
//                    }
//
//                    if (!mJsonObject11.isNull("vessel_add_time")) {
//                        mVesselesModel.setVessel_add_time(mJsonObject11.getString("vessel_add_time"));
//                    }
//                    if (!mJsonObject11.isNull("document1name")) {
//                        mVesselesModel.setDocument1name(mJsonObject11.getString("document1name"));
//                    }
//                    if (!mJsonObject11.isNull("document2name")) {
//                        mVesselesModel.setDocument2name(mJsonObject11.getString("document2name"));
//                    }
//                    if (!mJsonObject11.isNull("document3name")) {
//                        mVesselesModel.setDocument3name(mJsonObject11.getString("document3name"));
//                    }
//                    if (!mJsonObject11.isNull("document4name")) {
//                        mVesselesModel.setDocument4name(mJsonObject11.getString("document4name"));
//                    }
//                    if (!mJsonObject11.isNull("document5name")) {
//                        mVesselesModel.setDocument5name(mJsonObject11.getString("document5name"));
//                    }
//                    if (!mJsonObject11.isNull("document6name")) {
//                        mVesselesModel.setDocument6name(mJsonObject11.getString("document6name"));
//                    }
//                    if (!mJsonObject11.isNull("document7name")) {
//                        mVesselesModel.setDocument7name(mJsonObject11.getString("document7name"));
//                    }
//                    if (!mJsonObject11.isNull("document8name")) {
//                        mVesselesModel.setDocument8name(mJsonObject11.getString("document8name"));
//                    }
//                    if (!mJsonObject11.isNull("document9name")) {
//                        mVesselesModel.setDocument9name(mJsonObject11.getString("document9name"));
//                    }
//                    if (!mJsonObject11.isNull("document10name")) {
//                        mVesselesModel.setDocument10name(mJsonObject11.getString("document10name"));
//                    }
//
//                    if (!mJsonObject11.isNull("mail_data")) {
//                        String mail = mJsonObject11.getString("mail_data");
//                        mVesselesModel.setMail_data(mJsonObject11.getString("mail_data"));
//                    }
//                    if (page_no == 1) {
//                        mArrayList.add(mVesselesModel);
//                    } else if (page_no > 1) {
//                        loadMoreArrayList.add(mVesselesModel);
//                    }
//                }
//                if (loadMoreArrayList.size() > 0) {
//                    mArrayList.addAll(loadMoreArrayList);
//                }
//
//                /*SetAdapter*/
//                if (page_no == 1) {
//                    setAdapter();
//                } else {
//                    mVesselsAdapter.notifyDataSetChanged();
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    private void setAdapter() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        dataRV.setLayoutManager(layoutManager);
        mVesselsAdapter = new VesselTrashAdapter(getActivity(), mArrayList, mRecoverDeleteInterface, mPagination, mRecoverDELETEInterface);
        dataRV.setAdapter(mVesselsAdapter);
    }


    /*
     * Showing Delete and recover PopUP
     * */
    public void deleteConfirmDialog(String strMessage, final String strType) {
        final Dialog deleteConfirmDialog = new Dialog(getActivity());
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                if (strType.equals("empty")) {
                    /*Execute EMPTY API*/
                    if (Utilities.isNetworkAvailable(getActivity()) == false) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeEmptyTrash();
                    }
                } else if (strType.equals("delete")) {
                    /*Execute Delete API*/
                    if (Utilities.isNetworkAvailable(getActivity()) == false) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeDeleteTrash();
                    }
                } else {
                    /*Execute Recover API*/
                    if (Utilities.isNetworkAvailable(getActivity()) == false) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeRecoverTrash();
                    }
                }
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }
}
