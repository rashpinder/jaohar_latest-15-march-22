package jaohar.com.jaohar.fragments.sidemenufromapifragments;

import static android.view.View.GONE;

import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddInvoiceActivity;
import jaohar.com.jaohar.activities.forum_module.AddForumActivity;
import jaohar.com.jaohar.activities.forum_module.ChatScreenForumActivity;
import jaohar.com.jaohar.activities.forum_module.ForumListActivity;
import jaohar.com.jaohar.adapters.forum_module.ForumListAdapter;
import jaohar.com.jaohar.adapters.forum_module.ForumListNewAdapter;
import jaohar.com.jaohar.beans.ForumModule.ForumModel;
import jaohar.com.jaohar.fragments.BaseFragment;
import jaohar.com.jaohar.interfaces.forumModule.ForumItemClickNewInterace;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.models.forummodels.GetAllForumsModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class ForumListFragment extends BaseFragment {
    /*
     * set Activity TAG
     * */
    String TAG = ForumListFragment.this.getClass().getSimpleName();

    /*unbinder*/
    Unbinder unbinder;

    /**
     * Widgets
     */
    @BindView(R.id.progressBottomPB)
    ProgressBar progressBottomPB;
    @BindView(R.id.dataRV)
    RecyclerView dataRV;
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeToRefresh;

    int page_no = 1;
    String strLastPage = "TRUE";
    boolean isSwipeRefresh = false;

    /*
     * Setting Up Array List
     * */
    ArrayList<ForumModel> mLoadMore = new ArrayList<>();
    ArrayList<ForumModel> modelArrayList = new ArrayList<>();
    List<GetAllForumsModel.AllForum> mGetAllForumsList = new ArrayList<>();
    List<GetAllForumsModel.AllForum> mTempAllForumsList = new ArrayList<>();
    /*
     * Setting Up Adapter
     * */
    ForumListNewAdapter mAdapterNew;

    /* *
     * Setting Up Interface For selecting Items
     * */
    ForumItemClickNewInterace mInterfaceForumNew = new ForumItemClickNewInterace() {
        @Override
        public void ForumItemClick(GetAllForumsModel.AllForum mModel) {
            Intent mIntent = new Intent(getActivity(), ChatScreenForumActivity.class);
            mIntent.putExtra("forum_id", mModel.getId());
            mIntent.putExtra("room_id", mModel.getRoomId());
            getActivity().startActivity(mIntent);
        }
    };

    /**
     * Recycler View Pagination Adapter Interface
     **/
    PaginationListForumAdapter mPaginationInterFace = new PaginationListForumAdapter() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(View.VISIBLE);
                            }
                            ++page_no;
                            executeGetAllStaffForumsAPI();
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                        }
                    }
                }, 1000);
            }
        }
    };

    public ForumListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forum_list, container, false);

        //set status bar
        setStatusBar();

        /* butterknife */
        unbinder = ButterKnife.bind(this, view);

        setViewsIDs(view);

        toolBarMainLL.setVisibility(View.VISIBLE);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);
        HomeActivity.txtCenter.setText(getResources().getString(R.string.forum));

        HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setImageResource(R.drawable.plus_symbol);

        return view;
    }

    private void setViewsIDs(View view) {
        swipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                modelArrayList.clear();
                mLoadMore.clear();
                page_no = 1;
                executeGetAllStaffForumsAPI();
            }
        });

        HomeActivity.imgRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performCLick();
            }
        });
    }

    private void performCLick() {
        Intent mIntent = new Intent(getActivity(), AddForumActivity.class);
        mIntent.putExtra("type", "staff");
        startActivity(mIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute API For Getting List *//*
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(GONE);
            }
            modelArrayList.clear();
            mLoadMore.clear();
            page_no = 1;
            isSwipeRefresh = false;
            swipeToRefresh.setRefreshing(false);
            executeGetAllStaffForumsAPI();
        }
    }

    /* *
     * Execute API for getting Forum list
     * @param
     * @user_id
     * */
    public void executeGetAllStaffForumsAPI() {
        if (mTempAllForumsList != null) {
            mTempAllForumsList.clear();
        }
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_no == 1) {
                isSwipeRefresh = false;
                swipeToRefresh.setRefreshing(false);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllForumsModel> call1 = mApiInterface.getRomaniaForumListNew(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""), String.valueOf(page_no));
        call1.enqueue(new Callback<GetAllForumsModel>() {
            @Override
            public void onResponse(Call<GetAllForumsModel> call, retrofit2.Response<GetAllForumsModel> response) {
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                if (isSwipeRefresh == true) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }

                if (response.body() != null) {
                    if (response.body().getData() != null &&
                            response.body().getData().getLastPage() != null &&
                            !response.body().getData().getLastPage().equals("")) {
                        strLastPage = response.body().getData().getLastPage();
                    }

                    if (page_no == 1) {
                        mGetAllForumsList = response.body().getData().getAllForums();
                    } else if (page_no > 1) {
                        mTempAllForumsList = response.body().getData().getAllForums();
                    }

                    if (mTempAllForumsList.size() > 0) {
                        mGetAllForumsList.addAll(mTempAllForumsList);
                    }

                    if (page_no == 1) {
                        setAdapterNew();
                    } else {
                        mAdapterNew.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetAllForumsModel> call, Throwable t) {
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    /**
     * Setting Up Adapter
     **/
    private void setAdapterNew() {
        if (getActivity()!=null){
        dataRV.setLayoutManager(new LinearLayoutManager(getActivity()));}
        mAdapterNew = new ForumListNewAdapter(getActivity(), mGetAllForumsList, mPaginationInterFace, mInterfaceForumNew);
        dataRV.setAdapter(mAdapterNew);
        mAdapterNew.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}