package jaohar.com.jaohar.fragments;

import static android.view.View.GONE;
import static jaohar.com.jaohar.HomeActivity.switchFragment;
import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.notification_module.NotificationAdapter;
import jaohar.com.jaohar.beans.notification_module.NotificationModel;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.models.forummodels.UpdateForumHeadingModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends BaseFragment {
    /*
     * Strings TAG
     * */
    String TAG = NotificationFragment.this.getClass().getSimpleName();
    String strIsBack = "", UserId = "";

    /*unbinder*/
    Unbinder unbinder;

    /*
     * Widgets
     * */
    @BindView(R.id.notificationLayoutBar)
    RelativeLayout notificationLayoutBar;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.imgRightRL)
    RelativeLayout imgRightRL;
    @BindView(R.id.notificationCountRL)
    RelativeLayout notificationCountRL;
    @BindView(R.id.notificationCountBgRl)
    RelativeLayout notificationCountBgRl;
    @BindView(R.id.notificationCountTV)
    TextView notificationCountTV;

    SwipeRefreshLayout swipeToRefresh;
    RecyclerView dataRV;
    LinearLayout bottomLL;
    ProgressBar progressBottomPB;
    View rootView;
    NotificationAdapter mAdapter;
    boolean isSwipeRefresh = false;
    static int page_number = 0;
    String strLastPage = "TRUE", strUserRole = "";

    Integer notificationCount = 0;

    /*
     * ArrayList
     * */
    ArrayList<NotificationModel> mModelArray = new ArrayList<>();

    /**
     * Recycler View Pagination Adapter Interface
     **/
    PaginationListForumAdapter mPaginationInterFace = new PaginationListForumAdapter() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            progressBottomPB.setVisibility(View.VISIBLE);
                            ++page_number;
                            executeListAPI();
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                        }
                    }
                }, 1500);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            strIsBack = getArguments().getString(JaoharConstants.STR_BACK);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //set status bar
        getActivity().getWindow().setStatusBarColor(Color.WHITE);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_notification, container, false);

        /* butterknife */
        unbinder = ButterKnife.bind(this, rootView);

        if (strIsBack.equals(JaoharConstants.STR_BACK)) {
            JaoharConstants.IS_BACK = true;
        }

        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {

            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                UserId = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
            } else {
                UserId = JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, "");
            }

        } else {
            UserId = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
        }

        setViewData(rootView);
        ClickListeners();

        notificationLayoutBar.setVisibility(GONE);
        toolBarMainLL.setVisibility(View.VISIBLE);

        toolBarMainLL.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        HomeActivity.notificationCountRL.setVisibility(View.VISIBLE);
        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.badgesTV.setVisibility(GONE);
        HomeActivity.badgesTVUser.setVisibility(GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);

        HomeActivity.imgRightLL.setVisibility(View.INVISIBLE);
        HomeActivity.imgRight.setVisibility(View.VISIBLE);
        HomeActivity.imgRight.setImageResource(R.drawable.ic_magnifying_glass);

        return rootView;
    }

    private void ClickListeners() {
        HomeActivity.notificationCountRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeUpdateNotificationReadStatusAPI();
            }
        });

        imgRightRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeUpdateNotificationReadStatusAPI();
            }
        });
    }

    private void setViewData(View rootView) {
        dataRV = rootView.findViewById(R.id.dataRV);
        swipeToRefresh = rootView.findViewById(R.id.swipeToRefresh);
        bottomLL = rootView.findViewById(R.id.bottomLL);
        progressBottomPB = rootView.findViewById(R.id.progressBottomPB);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);

        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                mModelArray.clear();
                page_number = 1;
                executeListAPI();
            }
        });

        if (Utilities.isNetworkAvailable(getActivity())) {
            mModelArray.clear();
            page_number = 1;
            executeListAPI();
        } else {
            AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), getResources().getString(R.string.internetconnection));
        }
    }

    /*
     * Execute All List API GetNotificationList
     * @params
     * user_id
     * */
    private void executeListAPI() {
        if (page_number == 1) {
            if (isSwipeRefresh == true) {
                bottomLL.setVisibility(GONE);
                swipeToRefresh.setRefreshing(false);
            } else {
                bottomLL.setVisibility(GONE);
            }
        } else {
            swipeToRefresh.setRefreshing(false);
            bottomLL.setVisibility(View.VISIBLE);
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getNotificationsList(UserId, String.valueOf(page_number));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "******response*****" + response);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                if (isSwipeRefresh == true) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    parseResponce(mJsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
            }
        });
    }

    /*
     * Parse Responce
     * */
    private void parseResponce(JSONObject mJsonObject) {
        try {
            JSONObject mJsonDATA = mJsonObject.getJSONObject("data");
            if (!mJsonObject.isNull("unread_count") && !mJsonObject.getString("unread_count").equals("")) {
                notificationCount = mJsonObject.getInt("unread_count");
            }
            if (notificationCount > 0) {
                HomeActivity.notificationCountBgRl.setVisibility(View.VISIBLE);
                notificationCountBgRl.setVisibility(View.VISIBLE);
                HomeActivity.notificationCountTV.setText(String.valueOf(notificationCount));
                notificationCountTV.setText(String.valueOf(notificationCount));

                if (notificationCount > 99) {
                    HomeActivity.notificationCountTV.setText("99+");
                    notificationCountTV.setText(String.valueOf(notificationCount));
                }

            } else {
                HomeActivity.notificationCountBgRl.setVisibility(GONE);
                notificationCountBgRl.setVisibility(GONE);
            }

            strLastPage = mJsonDATA.getString("last_page");
            strUserRole = mJsonDATA.getString("user_role");
            JSONArray mjsonArrayData = mJsonDATA.getJSONArray("all_notifications");
            if (mjsonArrayData != null) {
                ArrayList<NotificationModel> mTempraryList = new ArrayList<>();
                mTempraryList.clear();
                for (int i = 0; i < mjsonArrayData.length(); i++) {
                    JSONObject mJsonMainData = mjsonArrayData.getJSONObject(i);
                    NotificationModel mModel = new NotificationModel();
                    if (!mJsonMainData.getString("user_image").equals("")) {
                        mModel.setUser_image(mJsonMainData.getString("user_image"));
                    }
                    if (!mJsonMainData.getString("notify_type").equals("")) {
                        mModel.setNotify_type(mJsonMainData.getString("notify_type"));
                    }
                    if (!mJsonMainData.getString("notify_id").equals("")) {
                        mModel.setNotify_id(mJsonMainData.getString("notify_id"));
                    }
                    if (!mJsonMainData.getString("forum_id").equals("")) {
                        mModel.setForum_id(mJsonMainData.getString("forum_id"));
                    }
                    if (!mJsonMainData.getString("notify_message").equals("")) {
                        mModel.setNotify_message(mJsonMainData.getString("notify_message"));
                    }
                    if (!mJsonMainData.getString("user_name").equals("")) {
                        mModel.setUser_name(mJsonMainData.getString("user_name"));
                    }
                    if (!mJsonMainData.getString("user_id").equals("")) {
                        mModel.setUser_id(mJsonMainData.getString("user_id"));
                    }
                    if (!mJsonMainData.getString("notify_date").equals("")) {
                        mModel.setNotify_date(mJsonMainData.getString("notify_date"));
                    }
                    if (!mJsonMainData.getString("forum_name").equals("")) {
                        mModel.setForum_name(mJsonMainData.getString("forum_name"));
                    }
                    mTempraryList.add(mModel);
                }

                mModelArray.addAll(mTempraryList);

                if (page_number == 1) {
                    setAdapter();
                } else {
                    mAdapter.notifyDataSetChanged();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        dataRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new NotificationAdapter(getActivity(), mModelArray, mPaginationInterFace, strUserRole);
        dataRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @OnClick({R.id.llLeftLL})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.llLeftLL:

                notificationLayoutBar.setVisibility(GONE);
                toolBarMainLL.setVisibility(View.VISIBLE);

                JaoharConstants.is_Staff_FragmentClick = false;
                HomeActivity.staffToolbar();
                txtCenter.setVisibility(View.VISIBLE);
                HomeActivity.mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment(getActivity(), new StaffFragment(), JaoharConstants.STAFF_FRAGMENT, false, null);
                break;
        }
    }

    private void executeUpdateNotificationReadStatusAPI() {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.UpdateNotificationReadStatusRequest(UserId).enqueue(new Callback<UpdateForumHeadingModel>() {
            @Override
            public void onResponse(Call<UpdateForumHeadingModel> call, Response<UpdateForumHeadingModel> response) {
                AlertDialogManager.hideProgressDialog();
                if (response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        HomeActivity.notificationCountBgRl.setVisibility(GONE);
                        notificationCountBgRl.setVisibility(GONE);
                    } else {
                        showAlertDialog(getActivity(), getResources().getString(R.string.app_name), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateForumHeadingModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}
