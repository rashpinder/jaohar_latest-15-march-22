package jaohar.com.jaohar.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.ForgotPasswordActivity;
import jaohar.com.jaohar.activities.OpenLinkActivity;
import jaohar.com.jaohar.activities.SignUpActivity;
import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.models.StaffLoginModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginFragment extends BaseFragment {
    String TAG = "LoginFragment";
    View rootView;
    EditText editUserNameET, editPasswordET;
    TextView txtForgotPwd, termsTV;
    LinearLayout llDontHaveAccountSignUpLL;
    Button btnLogin;
    String strLoginType = "";
    String refreshedToken = "";
    VesselesModel mVesselesModel;
    Spinner txtSpinnerTV;
    ArrayList<String> mArray;
    private long mLastClickTab1;

    String strType = "Romania Office";
    BottomSheetDialog mBottomSheetDialog;
    RelativeLayout romaniaOfficeRL, UkOfficeRL, cyprusOfficeRL, greeceOfficeRL, adminRL;

    public LoginFragment() {
        // Required empty public constructor
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getPushToken();

        Log.e(TAG, "*******TOKEN*******" + refreshedToken);

        if (getArguments() != null) {
            strLoginType = getArguments().getString(JaoharConstants.LOGIN_TYPE);
            if (strLoginType.equals(JaoharConstants.STAFF)) {
                HomeActivity.txtCenter.setText("(" + strType + ") \u2304");
                HomeActivity.txtCenter.setGravity(Gravity.CENTER);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, 0);
                HomeActivity.txtCenter.setLayoutParams(params);
                HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
            } else {
                HomeActivity.txtCenter.setGravity(Gravity.CENTER);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, 0);
                HomeActivity.txtCenter.setLayoutParams(params);
                HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
            }
        }
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                //handle token error
                Log.e(TAG, "**Get Instance Failed**", task.getException());
                return;
            }
            // Get new Instance ID token
            refreshedToken = task.getResult();
            Log.e(TAG, "**Push Token**" + refreshedToken);
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //set status bar
        getActivity().getWindow().setStatusBarColor(Color.WHITE);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_login, container, false);

        setWidgetIDs(rootView);

        addClicksToTermsAndPrivacyString();

        setClickListner();

        return rootView;
    }

    private void setWidgetIDs(View v) {
        termsTV = v.findViewById(R.id.termsTV);
        editUserNameET = v.findViewById(R.id.editUserNameET);
        editPasswordET = v.findViewById(R.id.editPasswordET);
        txtForgotPwd = v.findViewById(R.id.txtForgotPwd);
        llDontHaveAccountSignUpLL = v.findViewById(R.id.llDontHaveAccountSignUpLL);
        btnLogin = v.findViewById(R.id.btnLogin);
        HomeActivity.bottomMainLL.setVisibility(View.GONE);

        /* Bottom sheet Functionality */
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        @SuppressLint("InflateParams") View sheetView = getActivity().getLayoutInflater().inflate(R.layout.sign_up_bottom_sheet, null);
        mBottomSheetDialog.setContentView(sheetView);
        View bottomSheet = mBottomSheetDialog.findViewById(R.id.bottom_sheet);
        assert bottomSheet != null;
        adminRL = bottomSheet.findViewById(R.id.adminRL);
        romaniaOfficeRL = bottomSheet.findViewById(R.id.romaniaOfficeRL);
        UkOfficeRL = bottomSheet.findViewById(R.id.UkOfficeRL);
        cyprusOfficeRL = bottomSheet.findViewById(R.id.cyprusOfficeRL);
        greeceOfficeRL = bottomSheet.findViewById(R.id.greeceOfficeRL);

        adminRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strType = "Admin";
                HomeActivity.txtCenter.setText("(" + strType + ") \u2304");
                mBottomSheetDialog.dismiss();
            }
        });

        romaniaOfficeRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strType = "Romania Office";
                HomeActivity.txtCenter.setText("(" + strType + ") \u2304");
                mBottomSheetDialog.dismiss();
            }
        });

        UkOfficeRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strType = "UK Office";
                HomeActivity.txtCenter.setText("(" + strType + ") \u2304");
                mBottomSheetDialog.dismiss();
            }
        });

        cyprusOfficeRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strType = "Cyprus Office";
                HomeActivity.txtCenter.setText("(" + strType + ") \u2304");
                mBottomSheetDialog.dismiss();
            }
        });

        greeceOfficeRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strType = "Greece Office";
                HomeActivity.txtCenter.setText("(" + strType + ") \u2304");
                mBottomSheetDialog.dismiss();
            }
        });
    }

    private void setClickListner() {
        txtForgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (strLoginType.equals(JaoharConstants.STAFF)) {
                    Intent mIntent = new Intent(getActivity(), ForgotPasswordActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, strLoginType);
                    startActivity(mIntent);
                }
            }
        });

        llDontHaveAccountSignUpLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (strLoginType.equals(JaoharConstants.STAFF)) {
                    Intent mIntent = new Intent(getActivity(), SignUpActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, strLoginType);
                    startActivity(mIntent);
                }
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editUserNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (Utilities.isValidEmaillId(editUserNameET.getText().toString()) != true) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (editPasswordET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_password));
                } else {
                    if(getActivity()!=null){
                    if (Utilities.isNetworkAvailable(getActivity())) {
                        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                            logoutfromuser();
                        } else {
                            preventMultipleViewClick();
                            /*Execute Login API*/
                            executeAPI();
                        }
                    } else {
                        AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), getResources().getString(R.string.internetconnection));
                    }
                }}
            }
        });



        HomeActivity.txtCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null) {
                    if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                        mBottomSheetDialog.dismiss();
                    } else {
                        mBottomSheetDialog.show();
                    }
                }
            }
        });
    }

    private void addClicksToTermsAndPrivacyString() {
        SpannableString SpanString = new SpannableString(getString(R.string.by_logging_in));
        ClickableSpan termsAndCondition = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(getActivity(), OpenLinkActivity.class);
                mIntent.putExtra("TITLE", "Terms & Conditions");
                mIntent.putExtra("LINK", JaoharConstants.TERMS_AND_CONDITIONS);
                startActivity(mIntent);

            }
        };

        // Character starting from 31 - 44 is privacy policy.
        // Character starting from 52 - 69 is Terms and condition.

        ClickableSpan privacy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(getActivity(), OpenLinkActivity.class);
                mIntent.putExtra("TITLE", "Privacy Policy");
                mIntent.putExtra("LINK", JaoharConstants.PRIVACY_POLICY);
                startActivity(mIntent);

            }
        };

        SpanString.setSpan(termsAndCondition, 49, 67, 0);
        SpanString.setSpan(privacy, 30, 44, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 30, 44, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 49, 67, 0);
        SpanString.setSpan(new UnderlineSpan(), 30, 44, 0);
        SpanString.setSpan(new UnderlineSpan(), 49, 67, 0);

        termsTV.setMovementMethod(LinkMovementMethod.getInstance());
        termsTV.setText(SpanString, TextView.BufferType.SPANNABLE);
        termsTV.setSelected(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public void preventMultipleViewClick() {
        if (SystemClock.elapsedRealtime() - this.mLastClickTab1 >= (long) 4000) {
            this.mLastClickTab1 = SystemClock.elapsedRealtime();
        }
    }

    private Map<String, String> mParams() {
        Map<String, String> params = new HashMap<>();
        params.put("email", editUserNameET.getText().toString());
        params.put("password", editPasswordET.getText().toString());
        params.put("device_token", refreshedToken);
        params.put("device_type", "Android");
        params.put("role", strType);
        Log.e("**PARAMS**", params.toString());
        return params;
    }

    private void executeAPI() {
        if (getActivity()!=null){
            AlertDialogManager.showProgressDialog(getActivity());}
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StaffLoginModel> call1 = mApiInterface.loginStaffRequest(mParams());
        call1.enqueue(new Callback<StaffLoginModel>() {
            @Override
            public void onResponse(Call<StaffLoginModel> call, retrofit2.Response<StaffLoginModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StaffLoginModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    if (getActivity()!=null){
                    JaoharPreference.writeString(getActivity(), JaoharPreference.Role, mModel.getData().getRole());
                    JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_EMAIL, mModel.getData().getEmail());
                    JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ID, mModel.getData().getId());
                    JaoharPreference.writeString(getActivity(), JaoharPreference.BLOGUSER_ID, mModel.getData().getId());
                    JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ROLE, mModel.getData().getRole());
                    JaoharPreference.writeString(getActivity(), JaoharPreference.first_name, mModel.getData().getFirstName());
                    JaoharPreference.writeString(getActivity(), JaoharPreference.full_name, mModel.getData().getFirstName() + " " + mModel.getData().getLastName());
                    JaoharPreference.writeString(getActivity(), JaoharPreference.image, mModel.getData().getImage());

                    // Mainatain Check to Show And Hide View
                    if (JaoharPreference.readString(getActivity(), JaoharPreference.first_name, "").equals("")) {
                        HomeActivity.profileLL.setVisibility(View.GONE);
                    } else {
                        HomeActivity.profileLL.setVisibility(View.VISIBLE);
                        HomeActivity.nameTV.setText(JaoharPreference.readString(getActivity(), JaoharPreference.full_name, ""));
                        if (JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ROLE, "").equals("staff")) {
                            HomeActivity.typetv.setText("Romania");
                        } else if (JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ROLE, "").equals("office")) {
                            HomeActivity.typetv.setText("UK Office");
                        } else if (JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ROLE, "").equals("admin")) {
                            HomeActivity.typetv.setText("Admin");
                        }
                        Glide.with(getActivity()).load(JaoharPreference.readString(getActivity(), JaoharPreference.image, "")).into(HomeActivity.profilePICIMG);
                    }
                    if (strLoginType.equals(JaoharConstants.STAFF) && JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ROLE, "").equals(JaoharConstants.STAFF)) {
                        JaoharPreference.writeBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, true);
                        HomeActivity.txtCenter.setText(getString(R.string.staff));
                        HomeActivity.staffToolbar();
                        HomeActivity.imgRight.setVisibility(View.VISIBLE);
                        HomeActivity.switchFragment(getActivity(), new StaffFragment(), JaoharConstants.STAFF_FRAGMENT, false, null);
                        getActivity().getFragmentManager().popBackStack();
                    } else if (strLoginType.equals(JaoharConstants.STAFF) && JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ROLE, "").equals(JaoharConstants.USER)) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "Please enter Staff Credentials.");
                        editUserNameET.setText("");
                        editPasswordET.setText("");
                    } else if (mModel.getData().getRole().equals(JaoharConstants.ADMIN)) {
                        JaoharPreference.writeBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, true);
                        JaoharPreference.writeString(getActivity(), JaoharPreference.ADMIN_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.ADMIN_ID, mModel.getData().getId());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.ADMIN_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_ADMIN, true);
                        HomeActivity.txtCenter.setText(getString(R.string.staff));
                        HomeActivity.staffToolbar();
                        HomeActivity.imgRight.setVisibility(View.VISIBLE);
                        HomeActivity.switchFragment(getActivity(), new StaffFragment(), JaoharConstants.STAFF_FRAGMENT, false, null);
                        getActivity().getFragmentManager().popBackStack();
                    } else if (JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ROLE, "").equals(JaoharConstants.OFFICE)) {
                        JaoharPreference.writeBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, true);
                        HomeActivity.txtCenter.setText(getString(R.string.staff));
                        HomeActivity.staffToolbar();
                        HomeActivity.imgRight.setVisibility(View.VISIBLE);
                        HomeActivity.switchFragment(getActivity(), new StaffFragment(), JaoharConstants.STAFF_FRAGMENT, false, null);
                        getActivity().getFragmentManager().popBackStack();
                    }
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }}
            }

            @Override
            public void onFailure(Call<StaffLoginModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), t.getMessage());
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void logoutfromuser() {
        if (JaoharConstants.BOOLEAN_USER_LOGOUT == true) {
            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                    logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                } else {
                    logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                }
            } else {
                logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
            }
        } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT == true) {
            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                    logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                } else {
                    logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                }
            } else {
                logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
            }
        } else if (JaoharConstants.Is_click_form_Manager == true) {
            logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
        } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL == true) {
            logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
        }
    }

    private void logOutAPI(String strLoginID) {
        if (JaoharConstants.Is_click_form_Manager == true) {
            executeAdminLogoutApi(strLoginID);
        } else {
            executeLogoutApi(strLoginID);
        }
    }

    //ADMIN_LOGOUT_API
    private void executeAdminLogoutApi(String strLoginID) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.adminLogoutRequest(strLoginID, refreshedToken, "Android");
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    /*Execute Login API*/
                    executeAPI();
                } else {
                    AlertDialogManager.hideProgressDialog();
                    showAlertDialog(getActivity(), getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e("TAG", "******error*****" + t.getMessage());
            }
        });
    }

    private void executeLogoutApi(String strLoginID) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.logoutRequest(strLoginID, refreshedToken, "Android");
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {

                    /*Execute Login API*/
                    executeAPI();
                } else {
                    AlertDialogManager.hideProgressDialog();
                    showAlertDialog(getActivity(), getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e("TAG", "******error*****" + t.getMessage());
            }
        });
    }
}
