package jaohar.com.jaohar.fragments.sidemenufromapifragments;

import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.AddMailingListActivity;
import jaohar.com.jaohar.activities.MaillingStaffActivity;
import jaohar.com.jaohar.activities.SendMailingListActivity;
import jaohar.com.jaohar.activities.ShowMailingContactLISTActivity;
import jaohar.com.jaohar.adapters.MailingListStaffAdapter;
import jaohar.com.jaohar.fragments.BaseFragment;
import jaohar.com.jaohar.interfaces.SendingMulipleLISTID;
import jaohar.com.jaohar.models.AllMailList;
import jaohar.com.jaohar.models.GetAllMailListsModel;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import jaohar.com.jaohar.views.CustomNestedScrollView;
import retrofit2.Call;
import retrofit2.Callback;

public class MaillingStaffFragment extends BaseFragment {

    String TAG = MaillingStaffFragment.this.getClass().getSimpleName();

    /*unbinder*/
    Unbinder unbinder;

    TextView txtMailTV;
    RecyclerView mailingListRV;
    SwipeRefreshLayout swipeToRefresh;
    CustomNestedScrollView parentSV;
    boolean isSwipeRefresh = false;
    int page_no = 1;
    public String strLastPage = "FALSE";
    boolean isLoadMore = false;
    MailingListStaffAdapter mAdapter;
    ArrayList<AllMailList> mArrayList = new ArrayList();
    ArrayList<AllMailList> loadMoreArrayList = new ArrayList();
    ArrayList<String> mListID = new ArrayList();
    ProgressBar progressBottomPB;
    int IsActive = 0;
    ArrayList<String> strData = new ArrayList<>();

    private ArrayList<AllMailList> multiSelectArrayList = new ArrayList<AllMailList>();

    MailingListStaffAdapter.ClickManagerModulesInterface clickManagerModulesInterface = new MailingListStaffAdapter.ClickManagerModulesInterface() {
        @Override
        public void mClickManagerModulesInterface(int position, String strListId, String strListName) {
            PerformOptionsClick(position, strListId, strListName);
        }
    };

    private void PerformOptionsClick(final int position, final String strListID, final String strListNAME) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_mailing_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);

        if(JaoharPreference.readString(getActivity(), JaoharPreference.Role, null).equals("admin")){
            editRL.setVisibility(View.VISIBLE);
        deleteRL.setVisibility(View.VISIBLE);
        viewRL.setVisibility(View.VISIBLE);}
        else{
            editRL.setVisibility(View.GONE);
            deleteRL.setVisibility(View.GONE);
            viewRL.setVisibility(View.GONE);
        }

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                strLastPage = "FALSE";
                Intent mIntent = new Intent(getActivity(), AddMailingListActivity.class);
                mIntent.putExtra("isAddClick", "false");
                mIntent.putExtra("listID", strListID);
                mIntent.putExtra("listNAME", strListNAME);
                startActivity(mIntent);
            }
        });

        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(getActivity(), ShowMailingContactLISTActivity.class);
                mIntent.putExtra("listID", strListID);
                getActivity().startActivity(mIntent);
            }
        });

        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                /*Send Email*/
                Intent mIntent = new Intent(getActivity(), SendMailingListActivity.class);
                mIntent.putExtra("listID", strListID);
                mIntent.putExtra("type", "staff");
                getActivity().startActivity(mIntent);
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    SendingMulipleLISTID mMultimaIlInterface = new SendingMulipleLISTID() {
        @Override
        public void mSendingMultipleLIST(AllMailList mMailingLIST, boolean mDelete) {
            if (mMailingLIST != null) {

                if (mDelete) {
                    IsActive = IsActive + 1;
                    multiSelectArrayList.add(mMailingLIST);
                    String strID = mMailingLIST.getListId();
                    mListID.add(mMailingLIST.getListId());
                    if (IsActive > 0) {
                        txtMailTV.setVisibility(View.VISIBLE);
                    } else {
                        txtMailTV.setVisibility(View.GONE);
                    }
                } else {
                    IsActive = IsActive - 1;
                    multiSelectArrayList.remove(mMailingLIST);
                    String strID = mMailingLIST.getListId();
                    mListID.remove(mMailingLIST.getListId());
                    if (IsActive > 0) {
                        txtMailTV.setVisibility(View.VISIBLE);
                    } else {
                        txtMailTV.setVisibility(View.GONE);
                    }

                }


//                if (IsActive > 0) {
//                    txtMailTV.setVisibility(View.VISIBLE);
//                } else {
//                    txtMailTV.setVisibility(View.GONE);
//                }

            }
        }
    };

    public MaillingStaffFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mailling_staff, container, false);

        /* butterknife */
        unbinder = ButterKnife.bind(this, view);

        setStatusBar();

        JaoharSingleton.getInstance().setSearchedVessel("");

        setViewsIDs(view);
        setClickListner();

        toolBarMainLL.setVisibility(View.VISIBLE);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.START);
        HomeActivity.txtCenter.setText(getResources().getString(R.string.mailing_list));

        return view;
    }

    private void setViewsIDs(View view) {
        progressBottomPB = view.findViewById(R.id.progressBottomPB);
        mailingListRV = view.findViewById(R.id.mailingListRV);
        swipeToRefresh = view.findViewById(R.id.swipeToRefresh);
        txtMailTV = view.findViewById(R.id.txtMailTV);
        parentSV = view.findViewById(R.id.parentSV);

        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                page_no = 1;
                mArrayList.clear();
                loadMoreArrayList.clear();
                txtMailTV.setVisibility(View.GONE);
                if (Utilities.isNetworkAvailable(getActivity()) == false) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    executeAPI(page_no);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        txtMailTV.setVisibility(View.GONE);
        if (Utilities.isNetworkAvailable(getActivity()) == false) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            page_no = 1;
            mArrayList.clear();
            loadMoreArrayList.clear();
            executeAPI(page_no);
        }
    }

    private void setClickListner() {

        parentSV.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    progressBottomPB.setVisibility(View.VISIBLE);
                    isLoadMore = true;
                    ++page_no;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (strLastPage.equals("FALSE")) {
                                executeAPI(page_no);
                            } else {
                                progressBottomPB.setVisibility(View.GONE);
                            }
                        }
                    }, 1000);
                }
            }
        });

        txtMailTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getMultiDetailsData();
                IsActive=0;
                /*Send Email*/
                Intent mIntent = new Intent(getActivity(), SendMailingListActivity.class);
                mIntent.putExtra("listIDArray", strData);
                mIntent.putExtra("type", "staff");
                getActivity().startActivity(mIntent);

            }
        });
    }

    private ArrayList<String> getMultiDetailsData() {
        strData.clear();

        for (int i = 0; i < mListID.size(); i++) {
            strData.add(mListID.get(i));
        }
        mListID.clear();
        return strData;
    }

    public void executeAPI(int page_no) {

        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
            AlertDialogManager.hideProgressDialog();
        }
        if (page_no == 1) {
            progressBottomPB.setVisibility(View.GONE);
            AlertDialogManager.showProgressDialog(getActivity());
        }

        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllMailListsModel> call1 = mApiInterface.getAllMailsListRequest(String.valueOf(page_no), JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<GetAllMailListsModel>() {
            @Override
            public void onResponse(Call<GetAllMailListsModel> call, retrofit2.Response<GetAllMailListsModel> response) {
                AlertDialogManager.hideProgressDialog();
                GetAllMailListsModel mModel = response.body();
                progressBottomPB.setVisibility(View.GONE);

                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
                if (mModel.getStatus().equals("1")) {
                    strLastPage = mModel.getData().getLastPage();
                    if (page_no == 1) {
                        mArrayList = mModel.getData().getAllMailLists();
                    } else if (page_no > 1) {
                        loadMoreArrayList = mModel.getData().getAllMailLists();
                    }

                    if (loadMoreArrayList.size() > 0) {
                        mArrayList.addAll(loadMoreArrayList);
                    }
                    setAdapter();
                }                                                                                                                                                                                                                                                                                                                                                     else {

                }
            }

            @Override
            public void onFailure(Call<GetAllMailListsModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        mailingListRV.setNestedScrollingEnabled(false);
        mailingListRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new MailingListStaffAdapter(getActivity(), mArrayList, mMultimaIlInterface, clickManagerModulesInterface);
        mailingListRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}