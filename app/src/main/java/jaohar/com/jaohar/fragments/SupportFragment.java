package jaohar.com.jaohar.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;


public class SupportFragment extends Fragment {
    String TAG = "SupportFragment";
    View rootView;

    EditText editNameET, editEmailET, editMessageET;
    Button btnSendQuery;

    public SupportFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.imgBack.setImageResource(R.drawable.menu);
        HomeActivity.toolBarMainLL.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.CENTER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //set status bar
        getActivity().getWindow().setStatusBarColor(Color.WHITE);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_support, container, false);

        setViewsIDs(rootView);
        setClickListner();

        return rootView;
    }

    protected void setViewsIDs(View v) {
        editNameET = (EditText) v.findViewById(R.id.editNameET);
        editEmailET = (EditText) v.findViewById(R.id.editEmailET);
        editMessageET = (EditText) v.findViewById(R.id.editMessageET);
        btnSendQuery = (Button) v.findViewById(R.id.btnSendQuery);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    protected void setClickListner() {
        btnSendQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_name));
                } else if (Utilities.isValidEmaillId(editEmailET.getText().toString()) != true) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (editMessageET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.enter_message));
                } else {
                    if (Utilities.isNetworkAvailable(getActivity()) == false) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeAPI();
                    }
                }
            }
        });
    }

    private void executeAPI() {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.supportRequest(editNameET.getText().toString(),editEmailET.getText().toString(),editMessageET.getText().toString())
                .enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                    editEmailET.setText("");
                    editNameET.setText("");
                    editMessageET.setText("");
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
}
