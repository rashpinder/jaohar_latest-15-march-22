package jaohar.com.jaohar.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;

import androidx.databinding.adapters.LinearLayoutBindingAdapter;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.messaging.FirebaseMessaging;
//import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.webViewActivity;
import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;


public class SignUpFragment extends Fragment {
    String TAG = "LoginFragment";
    View rootView;
    EditText editUserNameET, editPasswordET, editConfirmPasswordET, editFirstNameET, editLastNameET, editCompanyET;
    Button btnSignUp;
    String strIsBack = "";
    String strLoginType = "";
    String strRoleArray[] = {};
    VesselesModel mVesselesModel;
    String refreshedToken = "";
    LinearLayout roleLL;
    Spinner txtSpinnerTV;
    ArrayList<String> mArray;
    String strType = "";

    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            strIsBack = getArguments().getString("IsBack");

            strLoginType = getArguments().getString(JaoharConstants.LOGIN_TYPE);

            if (strLoginType.equals(JaoharConstants.USER)) {
                mVesselesModel = (VesselesModel) getArguments().getSerializable("Model");
            }
        }
        HomeActivity.bottomMainLL.setVisibility(View.GONE);
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    refreshedToken = task.getResult();
                    Log.e(TAG, "**Push Token**" + refreshedToken);

                });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_signup, container, false);
        strRoleArray = getActivity().getResources().getStringArray(R.array.role_array);

        getPushToken();

        Log.e(TAG, "********TOKEN*******" + refreshedToken);

        if (strIsBack.equals("IsBack")) {
            JaoharConstants.IS_BACK = true;
            HomeActivity.imgBack.setImageResource(R.drawable.back);
            Utilities.hideKeyBoad(getActivity(), rootView);
        } else {
            HomeActivity.imgBack.setImageResource(R.drawable.menu);
        }

        setWidgetIDs(rootView);
        setClickListner();
        return rootView;
    }

    private void setWidgetIDs(View v) {
        editUserNameET = v.findViewById(R.id.editUserNameET);
        editPasswordET = v.findViewById(R.id.editPasswordET);
        txtSpinnerTV = v.findViewById(R.id.txtSpinnerTV);
        editConfirmPasswordET = v.findViewById(R.id.editConfirmPasswordET);
        editFirstNameET = v.findViewById(R.id.editFirstNameET);
        editLastNameET = v.findViewById(R.id.editLastNameET);
        editCompanyET = v.findViewById(R.id.editCompanyET);
        btnSignUp = v.findViewById(R.id.btnSignUp);
        roleLL = v.findViewById(R.id.roleLL);
        if (strLoginType.equals(JaoharConstants.STAFF)) {
            roleLL.setVisibility(View.VISIBLE);
        } else {
            roleLL.setVisibility(View.GONE);
        }
    }

    private void setClickListner() {
        // Creating adapter for spinner
        mArray = new ArrayList<String>();
        mArray.add("Romania Office");
        mArray.add("UK Office");
        mArray.add("Cyprus Office");
        mArray.add("Greece Office");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, mArray);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        txtSpinnerTV.setAdapter(dataAdapter);

        txtSpinnerTV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strType = adapterView.getItemAtPosition(i).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editFirstNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_first_name));
                } else if (editLastNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_last_name));
                } else if (editUserNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (Utilities.isValidEmaillId(editUserNameET.getText().toString()) == false) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (editPasswordET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_password));
                } else if (editConfirmPasswordET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_password_con));
                } else if (!editConfirmPasswordET.getText().toString().equals(editPasswordET.getText().toString())) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.password_should));
                } else {
                    /*Execute Login API*/
                    executeAPI();
                }
            }

        });
    }

    private Map<String, String> mParams() {
        Map<String, String> params = new HashMap<>();
        params.put("email", editUserNameET.getText().toString());
        params.put("role", strType);
        params.put("password", editPasswordET.getText().toString());
        params.put("device_token", refreshedToken);
        params.put("device_type", "Android");
        Log.e("**PARAMS**", params.toString());
        return params;
    }

    public void executeAPI() {
        if (strLoginType.equals(JaoharConstants.STAFF)) {
//            strAPIUrl = JaoharConstants.SIGN_UP_API;
        } else {
//            strAPIUrl = JaoharConstants.SIGN_UP_API;
            strType = "user";
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.signupAndroidRequest(mParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAdminDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());

            }
        });
    }


//    public void executeAPI() {
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        if (strLoginType.equals(JaoharConstants.STAFF)){
//            strAPIUrl = JaoharConstants.SIGN_UP_API;
//
//        }
//        else{
//            strAPIUrl = JaoharConstants.SIGN_UP_API;
//            strType="user";
//        }
//        try {
//            jsonObject.put("email", editUserNameET.getText().toString());
//            jsonObject.put("role", strType);
//            jsonObject.put("password", editPasswordET.getText().toString());
//            jsonObject.put("device_token", refreshedToken);
//            jsonObject.put("device_type", "Android");
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        AlertDialogManager.showProgressDialog(getActivity());
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + jsonObject.toString());
//                try {
//
//                    String strStatus = jsonObject.getString("status");
//                    if (strStatus.equals("1")) {
//                        showAdminDialog(getActivity(), getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******error*****" + error);
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    public void showAdminDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
               /* SharedPreferences preferences = mActivity.getSharedPreferences(JaoharPreference.PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                Intent mIntent = new Intent(mActivity,HomeActivity.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mIntent.putExtra(JaoharConstants.LOGIN,JaoharConstants.LOGIN);
                mActivity.startActivity(mIntent);
                mActivity.finish();*/

                HomeActivity.loginToolbar();
                Bundle mBundle = new Bundle();
                if (strLoginType.equals(JaoharConstants.STAFF))
                    mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.STAFF);
                else
                    mBundle.putString(JaoharConstants.LOGIN_TYPE, JaoharConstants.USER);
//                HomeActivity.switchFragment((FragmentActivity) mActivity, new LoginFragment(), JaoharConstants.LOGIN_FRAGMENT, true, mBundle);
//                getActivity().getFragmentManager().popBackStack();

                Intent mIntent = new Intent(mActivity, HomeActivity.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mIntent.putExtra(JaoharConstants.LOGIN, "SignUP");
                mIntent.putExtra("TYPE", strLoginType);
                mActivity.startActivity(mIntent);
                mActivity.finish();
            }
        });
        alertDialog.show();
    }


}
