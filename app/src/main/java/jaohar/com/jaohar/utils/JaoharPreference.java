package jaohar.com.jaohar.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class JaoharPreference {
    public static final String PREF_NAME = "App_PREF";
    public static final int MODE = Context.MODE_PRIVATE;

    public static final String FCM_REFRESHED_TOKEN = "fcm_refreshed_token";

    public static final String IS_LOGGED_IN_STAFF = "is_loggedin_staff";
    public static final String IS_LOGGED_IN_VESSELS = "is_loggedin_vessels";
    public static final String IS_LOGGED_IN_ADMIN = "is_loggedin_admin";

    public static final String USER_ID = "user_id";
    public static final String BLOGUSER_ID = "blog_user_id";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_ROLE = "user_role";
    public static final String first_name = "first_name";
    public static final String full_name = "full_name";
    public static final String image = "image";
    public static final String Role = "role";

    public static final String STAFF_ID = "staff_id";
    public static final String STAFF_EMAIL = "staff_email";
    public static final String STAFF_ROLE = "staff_role";

    public static final String ADMIN_ID = "admin_id";
    public static final String ADMIN_EMAIL = "admin_email";
    public static final String ADMIN_ROLE = "admin_role";


    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static boolean readBoolean(Context context, String key,
                                      boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();

    }

    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();

    }

    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).commit();
    }

    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).commit();
    }

    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    public static Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}
