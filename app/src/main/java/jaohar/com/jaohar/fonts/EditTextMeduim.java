package jaohar.com.jaohar.fonts;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextMeduim  extends EditText {
    public EditTextMeduim(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EditTextMeduim(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EditTextMeduim(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public EditTextMeduim(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        try {
            this.setTypeface(new CalibriMedium(context).getFontFamily());
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }
}

