package jaohar.com.jaohar.fonts;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

import androidx.annotation.RequiresApi;

public class EditTextPoppinsRegular extends EditText {
    public EditTextPoppinsRegular(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EditTextPoppinsRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EditTextPoppinsRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public EditTextPoppinsRegular(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsRegular(context).getFontFamily());
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }
}

