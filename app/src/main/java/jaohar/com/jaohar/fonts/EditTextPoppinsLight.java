package jaohar.com.jaohar.fonts;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

import androidx.annotation.RequiresApi;

public class EditTextPoppinsLight extends EditText {
    public EditTextPoppinsLight(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EditTextPoppinsLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EditTextPoppinsLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public EditTextPoppinsLight(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        try {
            this.setTypeface(new PoppinsLight(context).getFontFamily());
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }
}
