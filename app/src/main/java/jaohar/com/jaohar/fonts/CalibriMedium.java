package jaohar.com.jaohar.fonts;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Dharmani Apps on 5/9/2017.
 */

public class CalibriMedium {
    String path = "Calibri-Medium.ttf";
    Context mContext;
    public static Typeface mTypeface;

    public CalibriMedium(Context context){
        mContext = context;
    }

    public Typeface getFontFamily(){

        try{
            mTypeface = Typeface.createFromAsset(mContext.getAssets(),path);
        }catch (Exception e){
            e.printStackTrace();
        }
        return mTypeface;
    }
}
