package jaohar.com.jaohar.fonts;

import android.content.Context;
import android.graphics.Typeface;

public class ProximaRegular {
    String path = "ProximaNova-Regular.ttf";
    Context mContext;
    public static Typeface mTypeface;
    public ProximaRegular(Context context){
        mContext = context;
    }

    public Typeface getFontFamily(){
        try{
            if (mTypeface == null)
                mTypeface = Typeface.createFromAsset(mContext.getAssets(),path);
        }catch (Exception e){
            e.printStackTrace();
        }

        return mTypeface;
    }
}
