package jaohar.com.jaohar.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

public class ButtonMedium extends Button {
    /*
     * Getting Current Class Name
     * */

    private String mTag = ButtonMedium.this.getClass().getSimpleName();

    public ButtonMedium(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public ButtonMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public ButtonMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public ButtonMedium(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }
    /*
     * Apply font.
     * */

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new CalibriMedium(context).getFontFamily());
        } catch (Exception e) {
            Log.e(mTag, e.toString());
        }
    }
}
