package jaohar.com.jaohar.fonts;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Dharmani Apps on 5/9/2017.
 */

public class CalibriRegular {
    String path = "Calibri-Regular.ttf";
    Context mContext;
    public static Typeface mTypeface;

    public CalibriRegular(Context context) {
        mContext = context;
    }

    public Typeface getFontFamily() {

        try {
            if (mTypeface == null)
            mTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mTypeface;
    }
}
