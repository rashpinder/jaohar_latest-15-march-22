package jaohar.com.jaohar.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.beans.SendMailListModel;
import jaohar.com.jaohar.models.MailModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import retrofit2.Call;
import retrofit2.Callback;

public class MailingListServices extends Service {

    String strContent;
    String strSubject;
    String strEmail;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
//      Toast.makeText(this, " MyService Created ", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        String strSubject = "test";
        JaoharConstants.mArraySendMailListModel.clear();
//      Toast.makeText(this, "MyService Started", Toast.LENGTH_LONG).show();
        int sizeOFARRAY = JaoharConstants.mArrayLISTDATA.size();
        int a;
        for (a = 0; a < JaoharConstants.mArrayLISTDATA.size(); a++) {
            Log.e("test", "ArrayListSize*********:" + "DATA========================" + JaoharConstants.mArrayLISTDATA.get(a).getContent() + " " + JaoharConstants.mArrayLISTDATA.get(a).getEmail());
            sendEamilAPI(this, JaoharConstants.mArrayLISTDATA.get(a).getContent(), JaoharConstants.mArrayLISTDATA.get(a).getEmail(), JaoharConstants.strSubject);
        }
    }


    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("content", strContent);
        mMap.put("subject", strSubject);
        mMap.put("email", strEmail);
        return mMap;
    }

    private void sendEamilAPI(final MailingListServices mailingListServices, String strContent, String strEmail, String strSubject) {
        this.strContent = strContent;
        this.strEmail = strEmail;
        this.strSubject = strSubject;
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.sendMailRequest(mParam()).enqueue(new Callback<MailModel>() {
            @Override
            public void onResponse(Call<MailModel> call, retrofit2.Response<MailModel> response) {
                AlertDialogManager.hideProgressDialog();
                MailModel mModel = response.body();
                AlertDialogManager.hideProgressDialog();
                JaoharConstants.mArraySendMailListModel.add(mModel);
                JaoharConstants.Is_Value = true;
            }

            @Override
            public void onFailure(Call<MailModel> call, Throwable t) {

            }
        });
    }


//    private void sendEamilAPI(final MailingListServices mailingListServices, String strContent, String strEmail, String strSubject) {
////        http://root.jaohar.com/Staging/JaoharWebServicesNew/MailSend.php
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.Mail_Send;
//        try {
//            jsonObject.put("content", strContent);
//            jsonObject.put("subject", strSubject);
//            jsonObject.put("email", strEmail);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.e("test", "******response*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    SendMailListModel mailListModel = new SendMailListModel();
////                  Toast.makeText(mailingListServices, jsonObject.getString("success"), Toast.LENGTH_LONG).show();
//                    mailListModel.setSuccess(jsonObject.getString("success"));
//                    mailListModel.setEmail(jsonObject.getString("email"));
//                    JaoharConstants.mArraySendMailListModel.add(mailListModel);
//                    JaoharConstants.Is_Value = true;
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e("test", "******response*****" + error.toString());
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
//         Toast.makeText(this, "Servics Stopped", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
}
