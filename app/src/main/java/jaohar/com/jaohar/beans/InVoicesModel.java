package jaohar.com.jaohar.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Dharmani Apps on 11/30/2017.
 */

public class InVoicesModel implements Serializable {
    String created = "";
    String currency = "";
    String invoice_id = "";
    String invoice_date = "";
    String invoice_number = "";
    String modified = "";
    String search_company = "";
    String search_vessel = "";
    String sign_invoice = "";
    String stamp = "";
    String status = "";
    String term_days = "";
    String discription = "";
    String quantity = "";
    String item = "";
    String bank_details = "";
    String refrence1 = "";
    String refrence2 = "";
    String refrence3 = "";
    String payment_id = "";
    String invoice_added_on = "";
    String pdf = "";
    String pdf_name = "";
    String owner_id = "";
    String deleted_by = "";
    String inv_state = "";

    public String getInv_state() {
        return inv_state;
    }

    public void setInv_state(String inv_state) {
        this.inv_state = inv_state;
    }

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getPdf_name() {
        return pdf_name;
    }

    public void setPdf_name(String pdf_name) {
        this.pdf_name = pdf_name;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    ArrayList<InvoiceAddItemModel> mItemModelArrayList = new ArrayList<InvoiceAddItemModel>();

    CompaniesModel mCompaniesModel;
    StampsModel mStampsModel;
    SignatureModel mSignatureModel;
    BankModel mBankModel;
    VesselSearchInvoiceModel mVesselSearchInvoiceModel;
    CurrenciesModel mCurrenciesModel;
    PaymentModel mPaymentModel;


    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getInvoice_added_on() {
        return invoice_added_on;
    }

    public void setInvoice_added_on(String invoice_added_on) {
        this.invoice_added_on = invoice_added_on;
    }

    public PaymentModel getmPaymentModel() {
        return mPaymentModel;
    }

    public void setmPaymentModel(PaymentModel mPaymentModel) {
        this.mPaymentModel = mPaymentModel;
    }

    public CurrenciesModel getmCurrenciesModel() {
        return mCurrenciesModel;
    }

    public void setmCurrenciesModel(CurrenciesModel mCurrenciesModel) {
        this.mCurrenciesModel = mCurrenciesModel;
    }

    public VesselSearchInvoiceModel getmVesselSearchInvoiceModel() {
        return mVesselSearchInvoiceModel;
    }

    public void setmVesselSearchInvoiceModel(VesselSearchInvoiceModel mVesselSearchInvoiceModel) {
        this.mVesselSearchInvoiceModel = mVesselSearchInvoiceModel;
    }

    public String getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(String invoice_id) {
        this.invoice_id = invoice_id;
    }


    public String getBank_details() {
        return bank_details;
    }

    public void setBank_details(String bank_details) {
        this.bank_details = bank_details;
    }


    public String getRefrence1() {
        return refrence1;
    }

    public void setRefrence1(String refrence1) {
        this.refrence1 = refrence1;
    }

    public String getRefrence2() {
        return refrence2;
    }

    public void setRefrence2(String refrence2) {
        this.refrence2 = refrence2;
    }

    public String getRefrence3() {
        return refrence3;
    }

    public void setRefrence3(String refrence3) {
        this.refrence3 = refrence3;
    }

    public BankModel getmBankModel() {
        return mBankModel;
    }

    public void setmBankModel(BankModel mBankModel) {
        this.mBankModel = mBankModel;
    }


    public SignatureModel getmSignatureModel() {
        return mSignatureModel;
    }

    public void setmSignatureModel(SignatureModel mSignatureModel) {
        this.mSignatureModel = mSignatureModel;
    }

    public StampsModel getmStampsModel() {
        return mStampsModel;
    }

    public void setmStampsModel(StampsModel mStampsModel) {
        this.mStampsModel = mStampsModel;
    }

    public CompaniesModel getmCompaniesModel() {
        return mCompaniesModel;
    }

    public void setmCompaniesModel(CompaniesModel mCompaniesModel) {
        this.mCompaniesModel = mCompaniesModel;
    }

    public ArrayList<InvoiceAddItemModel> getmItemModelArrayList() {
        return mItemModelArrayList;
    }

    public void setmItemModelArrayList(ArrayList<InvoiceAddItemModel> mItemModelArrayList) {
        this.mItemModelArrayList = mItemModelArrayList;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getInvoice_date() {
        return invoice_date;
    }

    public void setInvoice_date(String invoice_date) {
        this.invoice_date = invoice_date;
    }

    public String getInvoice_number() {
        return invoice_number;
    }

    public void setInvoice_number(String invoice_number) {
        this.invoice_number = invoice_number;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getSearch_company() {
        return search_company;
    }

    public void setSearch_company(String search_company) {
        this.search_company = search_company;
    }

    public String getSearch_vessel() {
        return search_vessel;
    }

    public void setSearch_vessel(String search_vessel) {
        this.search_vessel = search_vessel;
    }

    public String getSign_invoice() {
        return sign_invoice;
    }

    public void setSign_invoice(String sign_invoice) {
        this.sign_invoice = sign_invoice;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTerm_days() {
        return term_days;
    }

    public void setTerm_days(String term_days) {
        this.term_days = term_days;
    }
}
