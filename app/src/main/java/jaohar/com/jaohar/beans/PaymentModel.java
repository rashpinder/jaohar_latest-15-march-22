package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 2/16/2018.
 */

public class PaymentModel implements Serializable {
    String  payment_id = "";
    String SubTotal = "";
    String VAT = "";
    String Total = "";
    String Paid = "";
    String BalanceDue = "";
    String VATPrice = "";


    public String getSubTotal() {
        return SubTotal;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getVATPrice() {

        return VATPrice;
    }

    public void setVATPrice(String VATPrice) {
        this.VATPrice = VATPrice;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getVAT() {
        return VAT;
    }

    public void setVAT(String VAT) {
        this.VAT = VAT;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    public String getPaid() {
        return Paid;
    }

    public void setPaid(String paid) {
        Paid = paid;
    }

    public String getBalanceDue() {
        return BalanceDue;
    }

    public void setBalanceDue(String balanceDue) {
        BalanceDue = balanceDue;
    }
}
