package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by dharmaniz on 29/1/19.
 */

public class SendMailListModel implements Serializable {
    private String success ="";
    private String email ="";

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
