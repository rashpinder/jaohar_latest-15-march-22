package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 1/16/2018.
 */

public class SignatureModel implements Serializable {
    String id = "";
    String signature_name = "";
    String signature_image = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSignature_name() {
        return signature_name;
    }

    public void setSignature_name(String signature_name) {
        this.signature_name = signature_name;
    }

    public String getSignature_image() {
        return signature_image;
    }

    public void setSignature_image(String signature_image) {
        this.signature_image = signature_image;
    }
}
