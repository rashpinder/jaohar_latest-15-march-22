package jaohar.com.jaohar.beans;

import android.graphics.Bitmap;
import android.net.Uri;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 5/29/2017.
 */

public class AddGalleryImagesModel implements Serializable {

    String strImageId = "";
    String strImagePath = "";
    String strImageName = "";
    String strImageBitmap = "";
    String strType = "";
    String strDocName = "";
    String strDocByteArray = "";
    Bitmap mBitmap ;
    Uri mUri ;
    byte byteArray[];
    String strImageCount = "";

    public Uri getmUri() {
        return mUri;
    }

    public void setmUri(Uri mUri) {
        this.mUri = mUri;
    }

    public String getStrType() {
        return strType;
    }

    public void setStrType(String strType) {
        this.strType = strType;
    }

    public String getStrDocName() {
        return strDocName;
    }

    public void setStrDocName(String strDocName) {
        this.strDocName = strDocName;
    }

    public String getStrDocByteArray() {
        return strDocByteArray;
    }

    public void setStrDocByteArray(String strDocByteArray) {
        this.strDocByteArray = strDocByteArray;
    }

    public String getStrImageCount() {
        return strImageCount;
    }

    public void setStrImageCount(String strImageCount) {
        this.strImageCount = strImageCount;
    }

    public byte[] getByteArray() {
        return byteArray;
    }

    public void setByteArray(byte[] byteArray) {
        this.byteArray = byteArray;
    }

    public Bitmap getmBitmap() {
        return mBitmap;
    }

    public void setmBitmap(Bitmap mBitmap) {
        this.mBitmap = mBitmap;
    }

    ;

    public String getStrImageBitmap() {
        return strImageBitmap;
    }

    public void setStrImageBitmap(String strImageBitmap) {
        this.strImageBitmap = strImageBitmap;
    }

    public String getStrImageId() {
        return strImageId;
    }

    public void setStrImageId(String strImageId) {
        this.strImageId = strImageId;
    }

    public String getStrImagePath() {
        return strImagePath;
    }

    public void setStrImagePath(String strImagePath) {
        this.strImagePath = strImagePath;
    }

    public String getStrImageName() {
        return strImageName;
    }

    public void setStrImageName(String strImageName) {
        this.strImageName = strImageName;
    }
}
