package jaohar.com.jaohar.beans.staff_module;

import java.io.Serializable;

public class UserDetailAdminModel implements Serializable {
    String id ="";
    String email ="";
    String module_access ="";
    String role ="";
    private boolean isSelected;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getModule_access() {
        return module_access;
    }

    public void setModule_access(String module_access) {
        this.module_access = module_access;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
