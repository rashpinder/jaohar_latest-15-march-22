package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by dharmaniz on 15/3/19.
 */

public class InvoiceCompanyModel implements Serializable {
    private String owner_id ="";
    private String owner_name ="";
    private String address1 ="";
    private String address2 ="";
    private String address3 ="";
    private String email ="";
    private String registry_no ="";
    private String fiscal_code ="";
    private String imo_no ="";
    private String phone ="";
    private String logo ="";
    private String header ="";
    private String footer ="";
    private String color ="";
    private String head_color ="";
    private String table_head_color ="";
    private String table_row_color ="";
    private String gradient_color ="";
    private String prefix ="";
    private String signature ="";
    private String logo_text ="";
    private String logo_text_color ="";
    private String added_by ="";
    private String modification_date ="";
    private String enable ="";

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistry_no() {
        return registry_no;
    }

    public void setRegistry_no(String registry_no) {
        this.registry_no = registry_no;
    }

    public String getFiscal_code() {
        return fiscal_code;
    }

    public void setFiscal_code(String fiscal_code) {
        this.fiscal_code = fiscal_code;
    }

    public String getImo_no() {
        return imo_no;
    }

    public void setImo_no(String imo_no) {
        this.imo_no = imo_no;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getHead_color() {
        return head_color;
    }

    public void setHead_color(String head_color) {
        this.head_color = head_color;
    }

    public String getTable_head_color() {
        return table_head_color;
    }

    public void setTable_head_color(String table_head_color) {
        this.table_head_color = table_head_color;
    }

    public String getTable_row_color() {
        return table_row_color;
    }

    public void setTable_row_color(String table_row_color) {
        this.table_row_color = table_row_color;
    }

    public String getGradient_color() {
        return gradient_color;
    }

    public void setGradient_color(String gradient_color) {
        this.gradient_color = gradient_color;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getLogo_text() {
        return logo_text;
    }

    public void setLogo_text(String logo_text) {
        this.logo_text = logo_text;
    }

    public String getLogo_text_color() {
        return logo_text_color;
    }

    public void setLogo_text_color(String logo_text_color) {
        this.logo_text_color = logo_text_color;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getModification_date() {
        return modification_date;
    }

    public void setModification_date(String modification_date) {
        this.modification_date = modification_date;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }
}
