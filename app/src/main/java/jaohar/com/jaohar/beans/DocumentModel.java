package jaohar.com.jaohar.beans;

import android.net.Uri;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 12/29/2017.
 */

public class DocumentModel implements Serializable {

    String id = "";
    byte byteArray[] = null;
    String documentPath = "";
    String documentName = "";
    String documentBase64 = "";
    String documunetCount = "";
    String documunetExtension = "";
    Uri mUri ;

    public String getDocumunetExtension() {
        return documunetExtension;
    }

    public void setDocumunetExtension(String documunetExtension) {
        this.documunetExtension = documunetExtension;
    }

    public Uri getmUri() {
        return mUri;
    }

    public void setmUri(Uri mUri) {
        this.mUri = mUri;
    }

    public String getDocumunetCount() {
        return documunetCount;
    }

    public void setDocumunetCount(String documunetCount) {
        this.documunetCount = documunetCount;
    }

    public String getId() {
        return id;
    }

    public String getDocumentBase64() {
        return documentBase64;
    }

    public void setDocumentBase64(String documentBase64) {
        this.documentBase64 = documentBase64;
    }

    public String getDocumentPath() {
        return documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public void setId(String id) {
        this.id = id;

    }


    public byte[] getByteArray() {
        return byteArray;

    }

    public void setByteArray(byte[] byteArray) {
        this.byteArray = byteArray;
    }
}
