package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 3/19/2018.
 */

public class InternalNewsModel implements Serializable {
String created_at = "",id = "",news = "",type = "",photo="",staff_news="",staff_photo="",staff_news_status="",news_text="",staff_news_text="";

    public String getNews_text() {
        return news_text;
    }

    public void setNews_text(String news_text) {
        this.news_text = news_text;
    }

    public String getStaff_news() {
        return staff_news;
    }

    public void setStaff_news(String staff_news) {
        this.staff_news = staff_news;
    }

    public String getStaff_photo() {
        return staff_photo;
    }

    public void setStaff_photo(String staff_photo) {
        this.staff_photo = staff_photo;
    }

    public String getStaff_news_status() {
        return staff_news_status;
    }

    public void setStaff_news_status(String staff_news_status) {
        this.staff_news_status = staff_news_status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStaff_news_text() {
        return staff_news_text;
    }

    public void setStaff_news_text(String staff_news_text) {
        this.staff_news_text = staff_news_text;
    }
}
