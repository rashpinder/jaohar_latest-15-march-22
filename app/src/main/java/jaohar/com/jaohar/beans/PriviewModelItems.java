package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by dharmaniz on 15/6/18.
 */

public class PriviewModelItems implements Serializable {
    String strItem = "";
    String strDescription = "";
    String strQuantity = "";
    String strUnitPrice = "";
    String strAmount = "";

    public String getStrItem() {
        return strItem;
    }

    public void setStrItem(String strItem) {
        this.strItem = strItem;
    }

    public String getStrDescription() {
        return strDescription;
    }

    public void setStrDescription(String strDescription) {
        this.strDescription = strDescription;
    }

    public String getStrQuantity() {
        return strQuantity;
    }

    public void setStrQuantity(String strQuantity) {
        this.strQuantity = strQuantity;
    }

    public String getStrUnitPrice() {
        return strUnitPrice;
    }

    public void setStrUnitPrice(String strUnitPrice) {
        this.strUnitPrice = strUnitPrice;
    }

    public String getStrAmount() {
        return strAmount;
    }

    public void setStrAmount(String strAmount) {
        this.strAmount = strAmount;
    }
}
