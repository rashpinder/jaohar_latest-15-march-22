package jaohar.com.jaohar.beans.blog_module;

import java.io.Serializable;

public class Blog_CategoriesModel implements Serializable {
    String id="";
    String category="";
    String creation_date="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }
}
