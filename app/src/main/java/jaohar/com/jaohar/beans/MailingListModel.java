package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by dharmaniz on 15/1/19.
 */

public class MailingListModel implements Serializable {
    private String list_id = "";
    private String list_name = "";
    private String added_by = "";
    private String modification_date = "";

    public String getList_id() {
        return list_id;
    }

    public void setList_id(String list_id) {
        this.list_id = list_id;
    }

    public String getList_name() {
        return list_name;
    }

    public void setList_name(String list_name) {
        this.list_name = list_name;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getModification_date() {
        return modification_date;
    }

    public void setModification_date(String modification_date) {
        this.modification_date = modification_date;
    }
}
