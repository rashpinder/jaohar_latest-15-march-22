package jaohar.com.jaohar.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dharmaniz on 7/5/18.
 */

public class DiscountModel  implements Serializable {
    String ADD_items = "";
    String ADD_quantity = "";
    String Add_description = "";
    String ADD_unitPrice = "";
    String Add_Value = "";
    String Add_Total = "";
    String Type = "";
    String Subtract_items = "";
    String Subtract_quantity = "";
    String Subtract_description = "";
    String Subtract_unitPrice = "";
    String Subtract_Value = "";
    String Subtract_Total = "";
    String addAndSubtractTotal = "";
    String strTotalEditAmount = "";
    String strtotalPercentValue ="";

    public String getStrtotalPercentValue() {
        return strtotalPercentValue;
    }

    public void setStrtotalPercentValue(String strtotalPercentValue) {
        this.strtotalPercentValue = strtotalPercentValue;
    }

    public String getStrTotalEditAmount() {
        return strTotalEditAmount;
    }

    public void setStrTotalEditAmount(String strTotalEditAmount) {
        this.strTotalEditAmount = strTotalEditAmount;
    }

    public String getAddAndSubtractTotal() {
        return addAndSubtractTotal;
    }

    public void setAddAndSubtractTotal(String addAndSubtractTotal) {
        this.addAndSubtractTotal = addAndSubtractTotal;
    }

    public String getADD_items() {
        return ADD_items;
    }

    public void setADD_items(String ADD_items) {
        this.ADD_items = ADD_items;
    }

    public String getADD_quantity() {
        return ADD_quantity;
    }

    public void setADD_quantity(String ADD_quantity) {
        this.ADD_quantity = ADD_quantity;
    }

    public String getAdd_description() {
        return Add_description;
    }

    public void setAdd_description(String add_description) {
        Add_description = add_description;
    }

    public String getADD_unitPrice() {
        return ADD_unitPrice;
    }

    public void setADD_unitPrice(String ADD_unitPrice) {
        this.ADD_unitPrice = ADD_unitPrice;
    }

    public String getAdd_Value() {
        return Add_Value;
    }

    public void setAdd_Value(String add_Value) {
        Add_Value = add_Value;
    }

//    public String getAdd_Total() {
//        return Add_Total;
//    }
//
//    public void setAdd_Total(String add_Total) {
//        Add_Total = add_Total;
//    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getSubtract_items() {
        return Subtract_items;
    }

    public void setSubtract_items(String subtract_items) {
        Subtract_items = subtract_items;
    }

    public String getSubtract_quantity() {
        return Subtract_quantity;
    }

    public void setSubtract_quantity(String subtract_quantity) {
        Subtract_quantity = subtract_quantity;
    }

    public String getSubtract_description() {
        return Subtract_description;
    }

    public void setSubtract_description(String subtract_description) {
        Subtract_description = subtract_description;
    }

    public String getSubtract_unitPrice() {
        return Subtract_unitPrice;
    }

    public void setSubtract_unitPrice(String subtract_unitPrice) {
        Subtract_unitPrice = subtract_unitPrice;
    }

    public String getSubtract_Value() {
        return Subtract_Value;
    }

    public void setSubtract_Value(String subtract_Value) {
        Subtract_Value = subtract_Value;
    }

//    public String getSubtract_Total() {
//        return Subtract_Total;
//    }
//
//    public void setSubtract_Total(String subtract_Total) {
//        Subtract_Total = subtract_Total;
//    }
}
