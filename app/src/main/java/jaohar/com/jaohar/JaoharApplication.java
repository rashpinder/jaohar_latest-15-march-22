package jaohar.com.jaohar;

import android.app.Application;
import android.provider.Settings;
import androidx.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.volley.LruBitmapCache;

/**
 * Created by Dharmani Apps on 11/9/2017.
 */

public class JaoharApplication extends Application {
    public static final String TAG = JaoharApplication.class.getSimpleName();
    public static final int CONNECTION_TIMEOUT = 30000 * 1000;//120 Seconds
    private static JaoharApplication mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static synchronized JaoharApplication getInstance() {
        return mInstance;
    }

    /*
     * Socket
     * */
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(JaoharConstants.SocketURL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        MultiDex.install(this);
        JaoharConstants.DEVICE_TOKEN = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e(TAG,"******Device Token*****"+ JaoharConstants.DEVICE_TOKEN);
    }

    /***************
     *Imaplement Volley
     ***************/
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setRetryPolicy(new DefaultRetryPolicy(
                CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(
                CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        req.setTag(TAG);
        getRequestQueue().add(req);
    }


}
